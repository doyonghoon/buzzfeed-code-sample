package com.frograms.watcha;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import br.com.goncalves.pugnotification.interfaces.ImageLoader;
import br.com.goncalves.pugnotification.interfaces.OnImageLoadingCompleted;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.PushNotification;
import com.frograms.watcha.utils.WLog;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService implements ImageLoader {

  public static final String SENDER_ID = "1082325189232";

  private static int NOTIFICATION_ID = 0;

  public GCMIntentService() {
    super(SENDER_ID);
  }

  private Context mContext;

  @Override protected void onRegistered(Context context, String registrationId) {
    WLog.e("Registration ID : " + registrationId);
    WatchaApp.initializeRetrofit();
  }

  @Override protected void onUnregistered(Context context, String registrationId) {
    WLog.d("onUnregistered Called");
  }

  @Override protected void onMessage(final Context context, Intent intent) {
    //Handler handler = new Handler(context.getMainLooper());
    //
    //Runnable runnable = new Runnable() {
    //
    //	@Override public void run() {
    //		mContext = context;
    //		WLog.i("onMessage Called");
    //		//PugNotification.with(context).load()
    //		//		.iden
    //		String[] str = new String[] {"line1", "line2", "line3", "line4"};
    //		PugNotification.with(mContext).load()
    //				.title("abc")
    //				.message("def")
    //				.bigTextStyle("ABC")
    //				.smallIcon(R.drawable.android_icon_96)
    //				.largeIcon(R.drawable.icon_512)
    //				//.inboxStyle(str, "title", "summary")
    //				.ticker("ticker")
    //				.color(android.R.color.background_light)
    //				.custom()
    //				.setImageLoader(GCMIntentService.this)
    //				.background("https://d12n550ohk75u3.cloudfront.net/ts/4d7f8363352d1f522de3.jpg?1429170406")
    //				.setPlaceholder(R.drawable.placeholder_profile)
    //				.build();
    //
    //		Intent intent = new Intent(mContext, NotificationActivity.class);
    //		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    //
    //		startActivity(intent);
    //	}
    //};
    //
    //handler.post(runnable);
    PushNotification pushNotification = new PushNotification(context, intent);
    pushNotification.show(NOTIFICATION_ID);
    NOTIFICATION_ID++;
  }

  @Override protected void onDeletedMessages(Context context, int total) {
    WLog.d("onDeleteMessage Called");
  }

  @Override protected void onError(Context contxt, String errorId) {
    WLog.d("onError Called " + errorId);
  }

  @Override protected boolean onRecoverableError(Context context, String errorId) {
    return super.onRecoverableError(context, errorId);
  }

  @Override public void load(String uri, final OnImageLoadingCompleted onImageLoadingCompleted) {
    Glide.with(mContext).load(uri).asBitmap().into(new SimpleTarget<Bitmap>() {

      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        onImageLoadingCompleted.imageLoadingCompleted(resource);
      }
    });
  }

  @Override public void load(int resId, final OnImageLoadingCompleted onImageLoadingCompleted) {
    Glide.with(mContext).load(resId).asBitmap().into(new SimpleTarget<Bitmap>() {

      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        onImageLoadingCompleted.imageLoadingCompleted(resource);
      }
    });
  }
}