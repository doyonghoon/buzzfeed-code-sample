package com.frograms.watcha.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import com.frograms.watcha.R;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemViewHolder extends RecyclerView.ViewHolder {

  private View mTopDivider;
  private View mBotDivider;

  public ItemViewHolder(View view) {
    super(view);
    mTopDivider = view.findViewById(R.id.top_divider);
    mBotDivider = view.findViewById(R.id.bot_divider);
  }

  public LinearLayout getFrame() {
    return (LinearLayout) itemView;
  }

  public ItemView getItemView() {
    ItemView view = (ItemView) getFrame().getChildAt(1);
    view.setVisibility(View.VISIBLE);
    return view;
  }

  public void remove() {
    mTopDivider.setVisibility(View.GONE);
    mBotDivider.setVisibility(View.GONE);
    getItemView().setVisibility(View.GONE);
  }

  public void isDividerVisible(boolean isTopVisible, boolean isBotVisible) {
    if (isTopVisible) {
      mTopDivider.setVisibility(View.VISIBLE);
    } else {
      mTopDivider.setVisibility(View.GONE);
    }

    if (isBotVisible) {
      mBotDivider.setVisibility(View.VISIBLE);
    } else {
      mBotDivider.setVisibility(View.GONE);
    }
  }
}
