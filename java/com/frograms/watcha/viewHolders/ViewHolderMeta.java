package com.frograms.watcha.viewHolders;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.view.itemViews.ItemAlertView;
import com.frograms.watcha.view.itemViews.ItemArticleSmallView;
import com.frograms.watcha.view.itemViews.ItemBannerLargeView;
import com.frograms.watcha.view.itemViews.ItemBannerView;
import com.frograms.watcha.view.itemViews.ItemButtonCompactView;
import com.frograms.watcha.view.itemViews.ItemChartLargeView;
import com.frograms.watcha.view.itemViews.ItemContentDraggableView;
import com.frograms.watcha.view.itemViews.ItemContentGridView;
import com.frograms.watcha.view.itemViews.ItemContentLargeView;
import com.frograms.watcha.view.itemViews.ItemContentRatableView;
import com.frograms.watcha.view.itemViews.ItemContentReActionView;
import com.frograms.watcha.view.itemViews.ItemContentSelectableView;
import com.frograms.watcha.view.itemViews.ItemContentSmallView;
import com.frograms.watcha.view.itemViews.ItemContentTutorialView;
import com.frograms.watcha.view.itemViews.ItemContentUserActionView;
import com.frograms.watcha.view.itemViews.ItemContentXLargeView;
import com.frograms.watcha.view.itemViews.ItemDeckGridView;
import com.frograms.watcha.view.itemViews.ItemDeckLargeView;
import com.frograms.watcha.view.itemViews.ItemDeckSelectableView;
import com.frograms.watcha.view.itemViews.ItemDeckXLargeView;
import com.frograms.watcha.view.itemViews.ItemEvaluateCategoryView;
import com.frograms.watcha.view.itemViews.ItemGalleryView;
import com.frograms.watcha.view.itemViews.ItemHeaderBoldView;
import com.frograms.watcha.view.itemViews.ItemHeaderButtonView;
import com.frograms.watcha.view.itemViews.ItemHeaderCenterView;
import com.frograms.watcha.view.itemViews.ItemHeaderContentView;
import com.frograms.watcha.view.itemViews.ItemHeaderView;
import com.frograms.watcha.view.itemViews.ItemLoadMoreView;
import com.frograms.watcha.view.itemViews.ItemMediaCompactView;
import com.frograms.watcha.view.itemViews.ItemMediaGridView;
import com.frograms.watcha.view.itemViews.ItemMediaSmallView;
import com.frograms.watcha.view.itemViews.ItemMediaView;
import com.frograms.watcha.view.itemViews.ItemPromotionXLargeView;
import com.frograms.watcha.view.itemViews.ItemReplySmallView;
import com.frograms.watcha.view.itemViews.ItemReplyView;
import com.frograms.watcha.view.itemViews.ItemScoreXLargeView;
import com.frograms.watcha.view.itemViews.ItemTagsView;
import com.frograms.watcha.view.itemViews.ItemTextCollapsibleView;
import com.frograms.watcha.view.itemViews.ItemTextEdgeAlignedView;
import com.frograms.watcha.view.itemViews.ItemUserActionLargeView;
import com.frograms.watcha.view.itemViews.ItemUserActionSmallView;
import com.frograms.watcha.view.itemViews.ItemUserActionXLargeView;
import com.frograms.watcha.view.itemViews.ItemUserSelectableView;
import com.frograms.watcha.view.itemViews.ItemUserSmallView;
import com.frograms.watcha.view.itemViews.ItemUsersCompactView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import java.lang.reflect.ParameterizedType;

public enum ViewHolderMeta {

  // etc
  ALERT(ItemAlertView.class),                                 // EDU 아이템 재정의 필요
  TAGS(ItemTagsView.class),                               // DONE
  GALLERY(ItemGalleryView.class),                         // DONE 디자인 끝
  CHART_LARGE(ItemChartLargeView.class),                  // DONE 디자인 끝
  ARTICLE_SMALL(ItemArticleSmallView.class),              // DONE 디자인 끝
  USERS_COMPACT(ItemUsersCompactView.class),              // DONE 디자인 끝
  BUTTON_COMPACT(ItemButtonCompactView.class),            // DONE 디자인 끝
  SCORE_XLARGE(ItemScoreXLargeView.class),
  PROMOTION_XLARGE(ItemPromotionXLargeView.class),        // DONE 디자인 끝
  REACTION(ItemContentReActionView.class),                // DONE 디자인 끝

  // TEXT
  TEXT_COLLAPSIBLE(ItemTextCollapsibleView.class),        // DONE 디자인 끝
  TEXT_EDGE_ALIGNED(ItemTextEdgeAlignedView.class),       // DONE 디자인 끝

  // HEADER
  HEADER(ItemHeaderView.class),                           // DONE 디자인 끝
  HEADER_BOLD(ItemHeaderBoldView.class),                  // DONE 디자인 끝
  HEADER_CENTER(ItemHeaderCenterView.class),              // DONE
  HEADER_CONTENT(ItemHeaderContentView.class),            // DONE
  HEADER_BUTTON(ItemHeaderButtonView.class),              // DONE 디자인 끝

  // USER
  USER_SMALL(ItemUserSmallView.class),                    // DONE 디자인 끝
  USER_SELECTABLE(ItemUserSelectableView.class),          // DONE 디자인 끝

  // CONTENT
  CONTENT_GRID(ItemContentGridView.class),                // DONE 디자인 끝
  CONTENT_SMALL(ItemContentSmallView.class),              // DONE 디자인 끝
  CONTENT_LARGE(ItemContentLargeView.class),              // DONE 디자인도 끝
  CONTENT_XLARGE(ItemContentXLargeView.class),            // DONE 디자인 끝
  CONTENT_RATABLE(ItemContentRatableView.class),          // DONE 디자인 끝
  CONTENT_TUTORIAL(ItemContentTutorialView.class),        // DONE 디자인 끝
  CONTENT_DRAGGABLE(ItemContentDraggableView.class),      // DONE 디자인 끝
  CONTENT_SELECTABLE(ItemContentSelectableView.class),    // DONE 디자인 끝
  CONTENT_USERACTION(ItemContentUserActionView.class),    // DONE 디자인 끝

  // USERACTION
  USERACTION_SMALL(ItemUserActionSmallView.class),        // DONE 디자인 끝
  USERACTION_LARGE(ItemUserActionLargeView.class),        // DONE 디자인 끝
  USERACTION_XLARGE(ItemUserActionXLargeView.class),      // DONE 디자인 끝

  // REPLY
  REPLY(ItemReplyView.class),
  REPLY_SMALL(ItemReplySmallView.class),

  // DECK
  DECK_GRID(ItemDeckGridView.class),                      // DONE 디자인 끝
  DECK_LARGE(ItemDeckLargeView.class),                    // DONE 디자인 끝
  DECK_XLARGE(ItemDeckXLargeView.class),                  // DONE 디자인 끝(더보기 추가해야됨 ㅠ)
  DECK_SELECTABLE(ItemDeckSelectableView.class),          // DONE 디자인 끝

  // MEDIA
  MEDIA(ItemMediaView.class),                             // DONE 디자인 끝
  MEDIA_GRID(ItemMediaGridView.class),                    // DONE 디자인 끝
  MEDIA_SMALL(ItemMediaSmallView.class),                  // DONE 디자인 끝
  EVALUATE_CATEGORY(ItemEvaluateCategoryView.class),                  // DONE 디자인 끝
  MEDIA_COMPACT(ItemMediaCompactView.class),              // DONE 디자인 끝

  // BANNER
  BANNER(ItemBannerView.class),                           // DONE 디자인 끝
  BANNER_LARGE(ItemBannerLargeView.class),                // DONE 디자인 끝

  // LOAD_MORE(서버에서 보내주는게 아니라 드로워에서 쓰려고 클라 자체 생산 TYPE)
  LOAD_MORE(ItemLoadMoreView.class);

  private Class<? extends ItemView> mItemViewClass;

  ViewHolderMeta(Class<? extends ItemView> itemView) {
    mItemViewClass = itemView;
  }

  public Class<? extends ItemView> getItemViewClass() {
    return mItemViewClass;
  }

  public Class<? extends Item> getItemType() {
    return (Class<? extends Item>) ((ParameterizedType) mItemViewClass.getGenericSuperclass()).getActualTypeArguments()[0];
  }

  public static boolean hasType(String type) {
    boolean result = false;
    if (!TextUtils.isEmpty(type)) {
      for (ViewHolderMeta h : values()) {
        if (h.name().toUpperCase().equals(type.toUpperCase())) {
          result = true;
          break;
        }
      }
    }
    return result;
  }

  @Nullable
  public static ViewHolderMeta getViewHolderMeta(Class<? extends ItemView> itemView) {
    if (itemView != null) {
      for (ViewHolderMeta m : values()) {
        if (m.getItemViewClass().equals(itemView)) {
          return m;
        }
      }
    }
    return null;
  }
}