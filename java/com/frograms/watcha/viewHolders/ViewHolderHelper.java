package com.frograms.watcha.viewHolders;

/**
 * Created by Larc21 on 2014. 11. 25..
 */
public class ViewHolderHelper {

  public enum CARD_TYPE {
    LIST,
    GRID
  }

  public static CARD_TYPE getCardType(String type) {
    return CARD_TYPE.valueOf(type);
  }

  public static ViewHolderMeta getItemType(String type) {
    if (ViewHolderMeta.hasType(type)) {
      return ViewHolderMeta.valueOf(type);
    }
    return null;
  }
}
