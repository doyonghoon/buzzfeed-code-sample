package com.frograms.watcha.database;

import com.frograms.watcha.model.User;

public class MainUserCache extends CacheBase<String, User> {

  public static final String KEY = "MainUser";

  @Override public void putData(User user) {
    if (user != null) {
      if (maxSize() <= size() && get(KEY) == null) {
        expandCacheSize(MIN_EXPAND_SIZE);
      }
      put(KEY, user);
    }
  }

  @Override protected int getMaxSize() {
    return 1;
  }
}
