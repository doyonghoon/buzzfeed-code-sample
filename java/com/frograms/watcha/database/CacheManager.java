package com.frograms.watcha.database;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.items.Comment;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.utils.WLog;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 데이터 싱크 관리하는 클래스.
 */
public class CacheManager {

  private static CacheManager singleton;
  private UserActionCache mUserActionCache;
  private CommentCache mCommentCache;
  private UserCache mUserCache;
  private MainUserCache mMainUserCache;
  private DeckCache mDeckCache;
  private TagCache mTagCache;

  public CacheManager() {
    mUserActionCache = new UserActionCache();
    mCommentCache = new CommentCache();
    mUserCache = new UserCache();
    mMainUserCache = new MainUserCache();
    mDeckCache = new DeckCache();
    mTagCache = new TagCache();
  }

  public static UserActionCache getUserActionCache() {
    return getInstance().mUserActionCache;
  }

  //public static void addUpdateListener(UserActionCache.OnUserActionUpdateListener listener) {
  //  if (listener != null) {
  //    getInstance().mUserActionCache.addUpdateListener(listener);
  //  }
  //}

  //public static void removeUpdateListener(UserActionCache.OnUserActionUpdateListener listener) {
  //  if (listener != null) {
  //    getInstance().mUserActionCache.removeUpdateListener(listener);
  //  }
  //}

  public static void addTagUpdateListener(TagCache.OnTagCacheUpdateListener listener) {
    if (listener != null) {
      getInstance().mTagCache.addUpdateListener(listener);
    }
  }

  public static void removeTagUpdateListener(TagCache.OnTagCacheUpdateListener listener) {
    if (listener != null) {
      getInstance().mTagCache.removeUpdateListener(listener);
    }
  }

  /**
   * 캐싱할 데이터들 인스턴스 생성때 같이 생성함.
   */
  public static CacheManager getInstance() {
    if (singleton == null) {
      singleton = new CacheManager();
    }
    return singleton;
  }

  public static TagCache getTagCache() {
    return getInstance().mTagCache;
  }

  public static void putTag(TagItems.Tag tag) {
    if (tag != null) {
      getInstance().mTagCache.putData(tag);
    }
  }

  public static void removeTag(TagItems.Tag tag) {
    if (tag != null) {
      getInstance().mTagCache.deleteDataByTag(tag);
    }
  }

  public static Map<String, TagItems.Tag> getTagSnapshot() {
    return getInstance().mTagCache.snapshot();
  }

  public static int[] getTagIds() {
    List<Integer> tagIds = new ArrayList<>();
    for (Map.Entry<String, TagItems.Tag> tag : getTagSnapshot().entrySet()) {
      if (TextUtils.isDigitsOnly(tag.getKey())) {
        tagIds.add(Integer.parseInt(tag.getKey()));
      }
    }
    int[] ids = new int[tagIds.size()];
    for (int i = 0; i < ids.length; i++) {
      ids[i] = tagIds.get(i);
    }
    return ids;
  }

  public static List<TagItems.Tag> getCachedTags() {
    List<TagItems.Tag> tags = new ArrayList<>();
    for (Map.Entry<String, TagItems.Tag> m : getTagSnapshot().entrySet()) {
      tags.add(m.getValue());
    }
    return tags;
  }

  public static int getTotalTagsCount() {
    return getInstance().mTagCache.size();
  }

  /**
   * 캐싱 해 놓을 내 평가정보.
   */
  public static void putMyUserAction(UserAction action) {
    if (action != null) {
      getInstance().mUserActionCache.putData(action);
    }
  }

  public static void deleteMyUserAction(String code) {
    if (code != null) {
      getInstance().mUserActionCache.deleteData(code);
    }
  }

  public static UserAction getMyUserAction(String contentCode) {
    return getInstance().mUserActionCache.get(contentCode);
  }

  public static void putComment(Comment comment) {
    if (comment != null) {
      getInstance().mCommentCache.putData(comment);
    }
  }

  public static void deleteComment(String code) {
    if (code != null) {
      getInstance().mCommentCache.deleteData(code);
    }
  }

  @Nullable public static Comment getComment(String code) {
    if (!TextUtils.isEmpty(code)) {
      return getInstance().mCommentCache.get(code);
    }
    return null;
  }

  public static void putUser(UserBase user) {
    if (user != null) {
      getInstance().mUserCache.putData(user);
    }
  }

  public static UserBase getUser(String code) {
    return getInstance().mUserCache.get(code);
  }

  public static void putDeck(DeckBase deck) {
    if (deck != null) {
      getInstance().mDeckCache.putData(deck);
    }
  }

  public static void deleteDeck(String code) {
    if (code != null) {
      getInstance().mDeckCache.deleteData(code);
    }
  }

  public static DeckBase getDeck(String code) {
    return getInstance().mDeckCache.get(code);
  }

  public static void putMainUser(User user) {
    if (user != null) {
      getInstance().mMainUserCache.putData(user);
    }
  }

  public static User getMainUser() {
    return getInstance().mMainUserCache.get(MainUserCache.KEY);
  }
}
