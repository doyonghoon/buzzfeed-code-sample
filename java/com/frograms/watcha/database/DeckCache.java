package com.frograms.watcha.database;

import com.frograms.watcha.model.items.bases.DeckBase;

/**
 * 평가정보 캐시 데이터 껍데기.
 */
public class DeckCache extends CacheBase<String, DeckBase> {

  @Override public void putData(DeckBase deck) {
    if (deck != null) {
      final String key = deck.getCode();
      if (maxSize() <= size() && get(key) == null) {
        expandCacheSize(MIN_EXPAND_SIZE);
      }
      put(key, deck);
    }
  }

  @Override protected int getMaxSize() {
    return 1000;
  }
}
