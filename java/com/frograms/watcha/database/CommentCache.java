package com.frograms.watcha.database;

import android.text.TextUtils;
import com.frograms.watcha.model.items.Comment;

/**
 * 평가정보 캐시 데이터 껍데기.
 */
public class CommentCache extends CacheBase<String, Comment> {

  @Override public void putData(Comment comment) {
    if (comment != null && !TextUtils.isEmpty(comment.getCode())) {
      final String key = comment.getCode();
      if (maxSize() <= size() && get(key) == null) {
        expandCacheSize(MIN_EXPAND_SIZE);
      }
      put(key, comment);
    }
  }

  @Override protected int getMaxSize() {
    return 500;
  }
}
