package com.frograms.watcha.database;

import android.content.Context;
import android.text.TextUtils;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.SearchSuggestionText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 최근 검색어를 기기에 저장해놓음.
 */
public class LatestSearchTextStore {

  private static final int MAX_COUNT = 5;
  private static final String KEY_LATEST_SEARCH_TEXT = "LatestSearchTextStore";

  public static List<String> getTexts(Context c) {
    String result = c.getSharedPreferences(KEY_LATEST_SEARCH_TEXT, Context.MODE_PRIVATE)
        .getString(KEY_LATEST_SEARCH_TEXT, null);
    if (!TextUtils.isEmpty(result)) {
      Type listType = new TypeToken<ArrayList<String>>() {
      }.getType();
      List<String> texts = WatchaApp.getInstance().getGson().fromJson(result, listType);
      return texts;
    } else {
      return new ArrayList<>();
    }
  }

  public static List<SearchSuggestionText> getSearchTexts(Context c) {
    List<String> ts = getTexts(c);
    List<SearchSuggestionText> result = new ArrayList<>();
    for (int i = 0; i < ts.size(); i++) {
      result.add(new SearchSuggestionText(ts.get(i), true, i == 0));
    }
    return result;
  }

  private static String toJson(List<String> texts) {
    if (texts == null) return null;
    JsonArray array = new JsonArray();
    for (String text : texts) {
      JsonPrimitive t = new JsonPrimitive(text);
      array.add(t);
    }
    return new Gson().toJson(array);
  }

  public static boolean addText(Context c, String text) {
    List<String> oldTexts = new ArrayList<>(getTexts(c));
    if (!oldTexts.contains(text)) {
      if (oldTexts.size() >= MAX_COUNT) {
        oldTexts.remove(oldTexts.size() - 1);
      }
      oldTexts.add(0, text);
      final String jsonString = toJson(oldTexts);
      return c.getSharedPreferences(KEY_LATEST_SEARCH_TEXT, Context.MODE_PRIVATE)
          .edit()
          .putString(KEY_LATEST_SEARCH_TEXT, jsonString)
          .commit();
    }
    return true;
  }
}
