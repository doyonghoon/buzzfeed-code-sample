package com.frograms.watcha.database;

import com.frograms.watcha.model.response.data.TagItems;
import java.util.ArrayList;
import java.util.List;

/**
 * 태그 관리.
 */
public class TagCache extends CacheBase<String, TagItems.Tag> {

  public interface OnTagCacheUpdateListener {
    void onAddTagCache(TagCache cache, TagItems.Tag tag);

    void onRemoveTagCache(TagCache cache, String key);
  }

  private List<OnTagCacheUpdateListener> mListeners = new ArrayList<>();

  public void addUpdateListener(OnTagCacheUpdateListener listener) {
    if (listener != null && !mListeners.contains(listener)) {
      mListeners.add(listener);
    }
  }

  public void removeUpdateListener(OnTagCacheUpdateListener listener) {
    if (listener != null && mListeners.contains(listener)) {
      mListeners.remove(listener);
    }
  }

  @Override public void putData(TagItems.Tag tag) {
    if (tag != null) {
      final String key = String.valueOf(tag.getId());
      if (maxSize() <= size() && get(key) == null) {
        expandCacheSize(MIN_EXPAND_SIZE);
      }
      put(key, tag);
      if (mListeners != null && !mListeners.isEmpty()) {
        for (OnTagCacheUpdateListener l : mListeners) {
          l.onAddTagCache(this, tag);
        }
      }
    }
  }

  @Override protected int getMaxSize() {
    return 1000;
  }

  @Override public void deleteData(String key) {
    super.deleteData(key);
    if (mListeners != null && !mListeners.isEmpty()) {
      for (OnTagCacheUpdateListener l : mListeners) {
        l.onRemoveTagCache(this, key);
      }
    }
  }

  public void deleteDataByTag(TagItems.Tag tag) {
    this.deleteData(String.valueOf(tag.getId()));
  }
}
