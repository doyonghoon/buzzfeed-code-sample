package com.frograms.watcha.database;

import com.frograms.watcha.model.items.UserAction;

/**
 * 평가정보 캐시 데이터 껍데기.
 */
public class UserActionCache extends CacheBase<String, UserAction> {

  //private List<OnUserActionUpdateListener> mListeners = new ArrayList<>();

  //public void addUpdateListener(OnUserActionUpdateListener listener) {
  //  if (listener != null && !mListeners.contains(listener)) {
  //    mListeners.add(listener);
  //  }
  //}

  //public void removeUpdateListener(OnUserActionUpdateListener listener) {
  //  if (listener != null && mListeners.contains(listener)) {
  //    mListeners.remove(listener);
  //  }
  //}

  @Override public void putData(UserAction action) {
    if (action != null) {
      final String key = action.getContentCode();
      if (maxSize() <= size() && get(key) == null) {
        expandCacheSize(MIN_EXPAND_SIZE);
      }
      put(key, action);
      callbackIfUpdated(action);
      //if (mListeners != null && !mListeners.isEmpty()) {
      //  for (OnUserActionUpdateListener l : mListeners) {
      //    l.onUpdateUserAction(action);
      //  }
      //}
    }
  }

  @Override protected int getMaxSize() {
    return 1000;
  }
}
