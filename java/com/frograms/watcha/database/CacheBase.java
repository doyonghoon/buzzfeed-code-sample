package com.frograms.watcha.database;

import android.util.LruCache;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 캐시 사이즈가 부족할때 넓히는 메소드가 있는 기본 캐싱 클래스.
 */
public abstract class CacheBase<K, V> extends LruCache<K, V> {

  public interface OnCacheUpdateListener<C extends CacheBase, K, V> {
    void onAddCache(C cache, V value);

    void onRemoveCache(CacheBase cache, K key);
  }

  private List<OnCacheUpdateListener> mListeners = null;

  protected static final int MIN_EXPAND_SIZE = 10;

  public CacheBase() {
    super(MIN_EXPAND_SIZE);
  }

  public abstract void putData(V value);

  protected abstract int getMaxSize();

  public <C extends CacheBase<K, V>> void addUpdateListener(
      OnCacheUpdateListener<C, K, V> listener) {
    if (mListeners == null) {
      mListeners = new ArrayList<>();
    }
    if (!mListeners.contains(listener)) {
      mListeners.add(listener);
    }
  }

  public <C extends CacheBase<K, V>> void removeUpdateListener(
      OnCacheUpdateListener<C, K, V> listener) {
    if (listener != null && mListeners.contains(listener)) {
      mListeners.remove(listener);
    }
  }

  public void deleteData(K key) {
    remove(key);
    callbackIfRemoved(key);
  }

  protected void expandCacheSize(int additionalSize) {
    if (!isOverflownMaxSize()) {
      Map<K, V> snapshot = snapshot();
      resize(size() + additionalSize);
      for (Map.Entry<K, V> s : snapshot.entrySet()) {
        put(s.getKey(), s.getValue());
      }
    }
  }

  private boolean isOverflownMaxSize() {
    return size() >= getMaxSize();
  }

  //protected <C extends CacheBase<K, V>> List<OnCacheUpdateListener<C, K, V>> getListeners() {
  //  return mListeners;
  //}

  protected <C extends CacheBase<K, V>> void callbackIfUpdated(V value) {
    if (mListeners != null && !mListeners.isEmpty()) {
      for (OnCacheUpdateListener l : mListeners) {
        l.onAddCache(this, value);
      }
    }
  }

  protected <C extends CacheBase<K, V>> void callbackIfRemoved(K key) {
    if (mListeners != null && !mListeners.isEmpty()) {
      for (OnCacheUpdateListener l : mListeners) {
        l.onRemoveCache(this, key);
      }
    }
  }
}
