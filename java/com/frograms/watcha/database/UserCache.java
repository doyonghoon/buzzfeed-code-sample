package com.frograms.watcha.database;

import com.frograms.watcha.model.items.UserBase;

public class UserCache extends CacheBase<String, UserBase> {

  @Override public void putData(UserBase user) {
    if (user != null) {
      final String key = user.getCode();
      if (maxSize() <= size() && get(key) == null) {
        expandCacheSize(MIN_EXPAND_SIZE);
      }
      put(key, user);
    }
  }

  @Override protected int getMaxSize() {
    return 1000;
  }
}
