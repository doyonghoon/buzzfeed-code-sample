package com.frograms.watcha.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.helpers.WLifeCycleManager;
import com.frograms.watcha.listeners.LifecycleCallback;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.RecycleUtils;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.drawers.WBottomSheetLayout;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

public class BaseActivity extends AppCompatActivity implements LifecycleCallback {

  /**
   * onActivityResult에서 -2로 넘겨주면, 다 종료되는 값.
   * {@code
   * if (resultCode == -2) {
   * setResult(-2);
   * finish();
   * }
   * }
   */
  public static final int RESULT_DESTROY = -2;

  protected FragmentTask mTask;

  protected WatchaApp mApp;

  private ToolbarHelper mToolbarHelper;

  public WBottomSheetLayout mBottomSheetLayout;
  private LinearLayout mHeaderView;
  private LinearLayout mUnderToolbarView;
  protected BaseFragment mFragment;

  private ActivityStarter.AnimationType mAnimType = null;
  private Bundle mBundle = null;
  private WLifeCycleManager mLifeCycleManager = null;

  /**
   * 뒤로가기 버튼 눌렀을때 이 값으로 액티비티를 종료하거나 그냥 놔둠.
   */
  private OnBackKeyListener mOnBackKeyListener;

  @Override public void onDestroy() {
    super.onDestroy();
    if (getToolbarHelper() != null) {
      getToolbarHelper().clearHelper();
      if (mToolbarHelper != null) {
        mToolbarHelper = null;
      }
    }

    try {
      RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    } catch (NullPointerException ignored) {
    }

    mTask = null;
    mApp = null;
    mFragment = null;
    mBundle = null;
  }

  public BaseFragment getFragment() {
    return this.mFragment;
  }

  public FragmentTask getTask() {
    return mTask;
  }

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity);
    setToolbarHelper();

    mUnderToolbarView = (LinearLayout) findViewById(R.id.under_toolbar);
    mBottomSheetLayout = (WBottomSheetLayout) findViewById(R.id.bottomsheet);

    mApp = (WatchaApp) getApplication();

    if (getIntent() != null) {
      if (getIntent().hasExtra(ActivityStarter.TASK)) {
        mTask = (FragmentTask) getIntent().getSerializableExtra(ActivityStarter.TASK);
      }

      if (getIntent().hasExtra(ActivityStarter.BUNDLE)) {
        mBundle = getIntent().getBundleExtra(ActivityStarter.BUNDLE);
        mAnimType = (ActivityStarter.AnimationType) mBundle.getSerializable(
            BundleSet.TRANSITION_ANIMATION_TYPE);
      }
    }

    if (mTask == null) {
      mTask = FragmentTask.HOME;
    }

    if (savedInstanceState == null) {
      try {
        mFragment = mTask.getFragmentClass().newInstance();
        if (mBundle != null) {
          mFragment.setBundle(mBundle);
        }

        setOnBackKeyListener(mFragment.getOnBackKeyListener());
      } catch (Exception localException) {
        localException.printStackTrace();
      }

      try {
        getSupportFragmentManager().beginTransaction()
            .add(R.id.activity_layout, mFragment)
            .commit();
      } catch (Exception localException) {
        localException.printStackTrace();
      }
    }
  }

  private void setToolbarHelper() {
    if (findViewById(R.id.toolbar) != null) {
      mToolbarHelper = ToolbarHelper.getInstance(this);
      mToolbarHelper.setToolbar();
    }

    if (findViewById(R.id.header) != null) {
      mHeaderView = (LinearLayout) findViewById(R.id.header);
    }
  }

  public ToolbarHelper getToolbarHelper() {
    if (mToolbarHelper == null) {
      setToolbarHelper();
    }
    return mToolbarHelper;
  }

  public WLifeCycleManager getLifeCycleManager() {
    if (mLifeCycleManager == null) {
      mLifeCycleManager = new WLifeCycleManager(this, this);
    }
    return mLifeCycleManager;
  }

  public LinearLayout getHeaderView() {
    return mHeaderView;
  }

  public LinearLayout getUnderToolbar() {
    return mUnderToolbarView;
  }

  @Override public boolean onPrepareOptionsMenu(Menu menu) {
    if (mFragment != null && menu.size() == 0) {
      mFragment.onMenuItemSet(menu);
    }

    return true;
  }

  @Override public void onBackPressed() {
    if (getToolbarHelper() != null) getToolbarHelper().clearHelper();

    if (mFragment == null) {
      super.onBackPressed();
      return;
    }

    if (!mFragment.alwaysFinishOnBackPressed()) {
      mFragment.onBackPressed(true);
    } else {
      super.onBackPressed();
    }
  }

  public void refresh() {
    mFragment.refresh();
  }

  private TwitterLoginButton mTwitterLoginButton;

  public void setTwitterButton(TwitterLoginButton b) {
    mTwitterLoginButton = b;
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    FacebookBroker.getInstance()
        .getCallbackManager()
        .onActivityResult(requestCode, resultCode, data);
    if (mTwitterLoginButton != null) {
      mTwitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    if (resultCode == RESULT_DESTROY) {
      setResult(RESULT_DESTROY);
      finish();
    }

    if (mFragment != null) {
      mFragment.onActivityResult(requestCode, resultCode, data);
      if (resultCode == RESULT_OK) {
        if (data != null) {
          mFragment.setIntent(data);
        }
      }
    }
  }

  @Override public void onStart() {
    super.onStart();
    //GAUtil.startGA(this);
  }

  @Override public void onStop() {
    super.onStop();
    getLifeCycleManager().onStop();
    //GAUtil.stopGA(this);
  }

  @Override public void onPause() {
    super.onPause();
    getLifeCycleManager().onPause();
  }

  @Override public void onResume() {
    super.onResume();
    getLifeCycleManager().onResume();
  }

  @Override public void onRestart() {
    super.onRestart();

    if (mFragment != null) {
      mFragment.notifyList();
    }
  }

  @Override public void finish() {
    if (mTask != null) {
      if (mTask == FragmentTask.HOME) {
        setResult(BaseActivity.RESULT_DESTROY);
      }
    }

    super.finish();

    if (mAnimType != null && mAnimType != ActivityStarter.AnimationType.NONE) {
      overridePendingTransition(mAnimType.getCloseAnimation().getInAnimationId(),
          mAnimType.getCloseAnimation().getOutAnimationId());
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    super.onOptionsItemSelected(item);
    return mFragment == null || mFragment.onOptionsItemSelected(item);
  }

  @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
    final boolean result = super.onKeyDown(keyCode, event);
    if (mBottomSheetLayout != null && mBottomSheetLayout.isSheetShowing() && keyCode == KeyEvent.KEYCODE_BACK) {
      mBottomSheetLayout.dismissSheet();
      return false;
    } else if (mOnBackKeyListener != null && keyCode == KeyEvent.KEYCODE_BACK) {
      return mOnBackKeyListener.onBackKeyDown();
    }
    return result;
  }

  private static boolean onConnectFbInBackground = false;

  /**
   * 뒤로가기 버튼 누를때 콜백해줄 리스너 등록.
   */
  public void setOnBackKeyListener(OnBackKeyListener listener) {
    mOnBackKeyListener = listener;
  }

  @Override public void onAppBackFromBackground() {
    WLog.i("앱이 백그라운드에서 실행됐다!");
    if (WatchaApp.getUser() != null
        && WatchaApp.getUser().isFbConnected()
        && !onConnectFbInBackground) {
      //onConnectFbInBackground = true;
      //FacebookBroker.getProfile(this, new OnProfileListener() {
      //  @Override
      //  public void onComplete(Profile response) {
      //    if (response.getId() != null) {
      //      onConnectFbInBackground = false;
      //    } else {
      //      FacebookBroker.connectFb(BaseActivity.this, new SettingHelper.OnSettingResponseListener() {
      //        @Override public void onSuccess(@NonNull QueryType queryType, @NonNull
      //        BaseResponse<User> result) {
      //          super.onSuccess(queryType, result);
      //          onConnectFbInBackground = false;
      //        }
      //      });
      //    }
      //  }
      //});
    }
  }

  @Override public void onAppGoesBackground() {
    WLog.i("앱이 백그라운드로 진입한다!");
  }

  @Override protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if(WatchaApp.getUser() != null){
      outState.putParcelable("USER", WatchaApp.getUser());
    }

    if (mFragment != null) getSupportFragmentManager().putFragment(outState, "frag", mFragment);
  }

  @Override protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);

    if (WatchaApp.getUser() == null) {
      WatchaApp.getInstance().setUser((User) savedInstanceState.getParcelable("USER"));
    }

    mFragment = (BaseFragment) getSupportFragmentManager().getFragment(savedInstanceState, "frag");
  }
}