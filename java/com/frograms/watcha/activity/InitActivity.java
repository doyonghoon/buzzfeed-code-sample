package com.frograms.watcha.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.WSessionManager;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.retrofit.RetrofitErrorHandler;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.WLog;

public class InitActivity extends Activity {

  private String to = null;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // 유저가 혹시 푸시 타고 들어왔을때 어디로 이동할지 설정함.
    if (getIntent() != null) {
      if (getIntent().hasExtra(BundleSet.PUSH_KEY)) {
        to = getIntent().getStringExtra(BundleSet.PUSH_KEY);
      }
    }

    //to = buildSchemeUri().toString();

    Handler handler = new Handler(Looper.getMainLooper());
    handler.postDelayed(new Runnable() {
      @Override public void run() {
        checkWatchaSession();
      }
    }, 1);
  }

  /**
   * 스키마 테스트할때 이거 쓰면 됨.
   */
  private Uri buildSchemeUri() {
    return new Uri.Builder().scheme("watcha").authority("staffmades").appendEncodedPath("awards")
        //.appendEncodedPath("edit")
        //.appendEncodedPath("evaluate")
        //.appendEncodedPath("popular")
        //.appendEncodedPath("5")
        //.appendEncodedPath("movies")
        //.appendEncodedPath("rating")
        .build();
  }

  /**
   * 기네스 세션이 존재하면 왓챠 유저이므로 피드페이지로 보내고 안그러면 로그인으로 보냄.
   * h˙
   * 왓챠 2.0때 사용하던 /api/v10/user_action.json 처럼 /api/session/validate.json 같은게 필요할듯.
   * {@link WSessionManager#storeWatchaSessionInLocalStorage(Context)} 함수는 로그인 과정이 완료되었을때 호출함.
   *
   * p.s. 세션을 관리하는 모든 과정은 그냥 {@link WSessionManager} 클래스 생성하고 몰아 넣는게 꺼내쓰기도 편하고 이해하기 좋을 거 같음.
   */
  private void checkWatchaSession() {
    boolean isFirstVisit = WatchaApp.getSessionManager().isFirstVisitInV3(this);

    if (!isFirstVisit) {
      final boolean hasSession = WatchaApp.getSessionManager().hasSession(this);
      FragmentTask fragment = hasSession ? FragmentTask.HOME : FragmentTask.INTRO;
      final ActivityStarter s =
          ActivityStarter.with(this, fragment).setAnimationType(ActivityStarter.AnimationType.NONE);
      if (hasSession) {
        // 세션이 있어도 유저 데이터를 받고 화면 이동 해야함..
        loadMyData(new ApiResponseListener<BaseResponse<User>>() {
          @Override
          public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
            WatchaApp.getInstance().setUser(result.getData());
            if (WatchaApp.getUser().isFbConnected()) {
              FacebookBroker.getAccessTokenWithGrantedPermissions(
                  AccessToken.getCurrentAccessToken(), InitActivity.this).subscribe();
            }
            if (!TextUtils.isEmpty(to)) {
              Intent schemeIntent = new Intent(InitActivity.this, SchemeActivity.class);
              schemeIntent.setData(Uri.parse(to));
              startActivity(schemeIntent);
            } else {
              s.start();
            }
          }
        }).request();
      } else {
        s.start();
      }
    } else {
      loadMyData(new ApiResponseListener<BaseResponse<User>>() {
        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
          WatchaApp.getInstance().setUser(result.getData());
          WSessionManager.setIsFirstVisitInV3(InitActivity.this);
          if (WatchaApp.getUser().isFbConnected()) {
            FacebookBroker.getAccessTokenWithGrantedPermissions(AccessToken.getCurrentAccessToken(), InitActivity.this)
                .subscribe();
          }
          if (!TextUtils.isEmpty(to)) {
            Intent schemeIntent = new Intent(InitActivity.this, SchemeActivity.class);
            schemeIntent.setData(Uri.parse(to));
            startActivity(schemeIntent);
          } else {
            ActivityStarter.with(InitActivity.this, FragmentTask.HOME)
                .setAnimationType(ActivityStarter.AnimationType.NONE)
                .start();
          }
        }
      })
          .disableErrorDialog()
          .setErrorCallback(new RetrofitErrorHandler.OnErrorCallback() {

        @Override public void onErrorCallback(QueryType queryType, String errorMessage,
            @IntRange(from = 201, to = 9999) int errorCode) {
          if (errorCode == 401) {
            WSessionManager.setIsFirstVisitInV3(InitActivity.this);
            ActivityStarter.with(InitActivity.this, FragmentTask.INTRO)
                .setAnimationType(ActivityStarter.AnimationType.NONE)
                .start();
          }
        }
      }).request();
    }
  }

  private DataProvider loadMyData(ApiResponseListener<BaseResponse<User>> listener) {
    DataProvider provider = new DataProvider(this, QueryType.ACCOUNT, createReferrerBuilder());
    provider.responseTo(listener);
    return provider;
  }

  private ReferrerBuilder createReferrerBuilder() {
    return new ReferrerBuilder(AnalyticsManager.INIT);
  }
}