package com.frograms.watcha.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.listeners.BaseSchemeHandler;
import com.frograms.watcha.listeners.Scheme;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.SchemeHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SchemeActivity extends Activity {

  private static SchemeConfig[] sSchemeConfigs = new SchemeConfig[0];
  private static List<Scheme> sRoute = null;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getIntent() != null && getIntent().getData() != null) {
      Uri uri = getIntent().getData();

      String uriStr = uri.toString();
      if (uriStr.contains("/me/")) {
        uriStr.replace("/me/", "/" + WatchaApp.getUser().getCode() + "/");
        uri = Uri.parse(uriStr);
      }

      int pos = getHandlerPosition(uri);

      Uri handlerUri = sRoute.get(pos).getUri();
      SchemeHandler handler = sRoute.get(pos).getSchemeHandler();
      if (handler != null && handlerUri != null) {
        Map<String, String> params = getParams(handlerUri, uri);
        handler.onScheme(this, uri, putPreviousScreenName(params, getIntent().getExtras()));
      }
    }
    finish();
  }

  private Map<String, String> putPreviousScreenName(@Nullable Map<String, String> params, @Nullable Bundle bundle) {
    if (params != null && bundle != null && bundle.containsKey(ActivityStarter.BUNDLE)) {
      Bundle b = bundle.getBundle(ActivityStarter.BUNDLE);
      if (b != null && b.containsKey(BundleSet.PREVIOUS_SCREEN_NAME)) {
        params.put(BundleSet.PREVIOUS_SCREEN_NAME, b.getString(BundleSet.PREVIOUS_SCREEN_NAME));
      }
    }
    return params;
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == BaseActivity.RESULT_DESTROY) {
      setResult(BaseActivity.RESULT_DESTROY);
      finish();
    }
  }

  public static void addSchemeConfig(SchemeConfig config) {
    List<SchemeConfig> configs = new ArrayList<>();
    if (sSchemeConfigs != null) {
      configs.addAll(Arrays.asList(sSchemeConfigs));
    }
    configs.add(config);
    sSchemeConfigs = configs.toArray(new SchemeConfig[configs.size()]);
  }

  public static void register(String uri, SchemeHandler handler) {
    if (sRoute == null) {
      sRoute = new ArrayList<>();
      String defaultUri = "watcha://";
      sRoute.add(new Scheme(buildSchemeUri(defaultUri.split(File.separator)),
          new BaseSchemeHandler(FragmentTask.INIT)));
    }
    sRoute.add(new Scheme(buildSchemeUri(uri.split(File.separator)), handler));
  }

  private int getHandlerPosition(@NonNull Uri uri) {
    if (uri.getScheme().equals("watcha")
        && !TextUtils.isEmpty(uri.getAuthority())
        && sRoute != null) {
      final int pathLength = uri.getPathSegments().size();
      for (int Loop1 = 0; Loop1 < sRoute.size(); Loop1++) {
        Scheme m = sRoute.get(Loop1);
        boolean isVariableAuthority = isVariabledPath(m.getUri().getAuthority());
        if (m.getUri().getPathSegments().size() == pathLength // path.length 는 반드시 같고
            && isVariableAuthority // 변수형일 때는
            && matchesSchemeConfig(m.getUri().getAuthority(), uri.getAuthority())
            // Config 에 설정된 대로 잘 받아와야 함.
            || !isVariableAuthority // 일반형일 때는
            && m.getUri().getAuthority().equals(uri.getAuthority())) { // authority 값이 같아야 함.
          // path.size() 똑같고, authority 가 변수 형태일 경우는 잘 맞게 들어오거나, 변수 형태가 아닌 경우일 때는 uri.authority 와 동일한 값일 때.
          boolean isPathMatched = false;
          if (m.getUri().getPathSegments().size() == uri.getPathSegments().size()) {
            isPathMatched = true;
            List<String> paths = m.getUri().getPathSegments();
            for (int i = 0; i < paths.size(); i++) {
              String watchaPath = paths.get(i);
              String candidatePath = uri.getPathSegments().get(i);
              if (!isVariabledPath(watchaPath) && !candidatePath.equals(watchaPath)) {
                isPathMatched = false;
                break;
              } else if (isVariabledPath(watchaPath) && !matchesSchemeConfig(watchaPath,
                  candidatePath) && !getPaths().contains(candidatePath)) {
                isPathMatched = false;
                break;
              }
            }
          }

          if (isPathMatched) {
            return Loop1;
          }
        }
      }
    }
    return 0;
  }

  private boolean isVariabledPath(String value) {
    return value.startsWith(File.pathSeparator);
  }

  private Map<String, String> getParams(Uri watchaUri, Uri uri) {
    Map<String, String> params = new HashMap<>();
    if (watchaUri.getAuthority().startsWith(File.pathSeparator)) {
      params.put(watchaUri.getAuthority().replaceAll(File.pathSeparator, ""), uri.getAuthority());
    }
    for (int i = 0; i < watchaUri.getPathSegments().size(); i++) {
      String key = watchaUri.getPathSegments().get(i);
      if (key.startsWith(File.pathSeparator)) {
        params.put(key.replaceAll(File.pathSeparator, ""), uri.getPathSegments().get(i));
      }
    }
    return params;
  }

  private List<String> getPaths() {
    List<String> list = new ArrayList<>();
    for (Scheme m : sRoute) {
      String authority = m.getUri().getAuthority();
      String[] paths =
          m.getUri().getPathSegments().toArray(new String[m.getUri().getPathSegments().size()]);
      if (!authority.startsWith(File.pathSeparator)) {
        list.add(authority);
      }
      for (String p : paths) {
        if (!p.startsWith(File.pathSeparator)) {
          list.add(p);
        }
      }
    }
    return list;
  }

  private boolean matchesSchemeConfig(String key, String value) {
    if (!TextUtils.isEmpty(key) && sSchemeConfigs != null && sSchemeConfigs.length > 0) {
      for (SchemeConfig config : sSchemeConfigs) {
        if (config.getKey().equals(key)) {
          return config.getTypes().contains(value);
        }
      }
    }
    return true;
  }

  private static Uri buildSchemeUri(String[] pathSegments) {
    if (pathSegments != null) {
      Uri.Builder builder = new Uri.Builder().scheme("watcha");
      for (int i = 0; i < pathSegments.length; i++) {
        if (i == 0) {
          builder.authority(pathSegments[i]);
        } else {
          builder.appendPath(pathSegments[i]);
        }
      }
      return builder.build();
    }
    return null;
  }

  public static class SchemeConfig {

    List<String> mTypes = new ArrayList<>();
    String mKey = null;

    public SchemeConfig(String key, String[] types) {
      mKey = File.pathSeparator + key;
      mTypes = Arrays.asList(types);
    }

    public String getKey() {
      return mKey;
    }

    public List<String> getTypes() {
      return mTypes;
    }
  }
}
