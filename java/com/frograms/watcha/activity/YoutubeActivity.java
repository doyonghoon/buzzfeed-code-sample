package com.frograms.watcha.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * 유투브 영상 재생 화면
 */
public class YoutubeActivity extends YouTubeBaseActivity
    implements YouTubePlayer.OnInitializedListener, YouTubePlayer.OnFullscreenListener {

  @Bind(R.id.youtube_view) YouTubePlayerView mPlayerView;

  private String mYoutubeId;
  private YouTubePlayer mPlayer;

  @Override protected void onStart() {
    super.onStart();
    AnalyticsManager.sendScreenName(AnalyticsManager.YOUTUBE);
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);

    if (getActionBar() != null) getActionBar().hide();
    setContentView(R.layout.frag_youtube);
    ButterKnife.bind(this);

    if (getIntent() != null && getIntent().getExtras() != null && getIntent().hasExtra(
        "youtube_id")) {
      mYoutubeId = getIntent().getStringExtra("youtube_id");
    }
    if (mPlayerView != null) mPlayerView.initialize(BuildConfig.YOUTUBE_KEY, this);
  }

  @Override
  public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer,
      boolean wasRestored) {
    youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
    youTubePlayer.setOnFullscreenListener(this);
    if (!wasRestored) {
      if (!TextUtils.isEmpty(mYoutubeId)) {
        youTubePlayer.cueVideo(mYoutubeId);
        youTubePlayer.play();
      } else {
        Toast.makeText(this, getString(R.string.msg_false_youtube_id), Toast.LENGTH_SHORT).show();
        finish();
      }
    }
  }

  @Override public void onInitializationFailure(YouTubePlayer.Provider provider,
      YouTubeInitializationResult youTubeInitializationResult) {
    if (youTubeInitializationResult.isUserRecoverableError()) {
      youTubeInitializationResult.getErrorDialog(this, 26).show();
    } else {
      String errorMessage =
          String.format(getString(R.string.error_player), youTubeInitializationResult.toString());
      Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }
  }

  @Override public void onFullscreen(boolean isFullscreen) {
    if (mPlayer != null && !mPlayer.isPlaying()) mPlayer.play();
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (mPlayerView != null) mPlayerView.initialize(BuildConfig.YOUTUBE_KEY, this);
  }
}
