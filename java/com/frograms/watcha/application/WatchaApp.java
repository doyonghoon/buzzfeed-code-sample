package com.frograms.watcha.application;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.crashlytics.android.Crashlytics;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.GCMIntentService;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.SchemeActivity;
import com.frograms.watcha.activity.YoutubeActivity;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.WGuinnessHeader;
import com.frograms.watcha.helpers.WSessionManager;
import com.frograms.watcha.listeners.BaseCategorySchemeHandler;
import com.frograms.watcha.listeners.BaseSchemeHandler;
import com.frograms.watcha.listeners.CategoryRatingSchemeHandler;
import com.frograms.watcha.listeners.CommentSchemeHandler;
import com.frograms.watcha.listeners.DeckSchemeHandler;
import com.frograms.watcha.listeners.NoticeSchemeHandler;
import com.frograms.watcha.listeners.RatingSchemeHandler;
import com.frograms.watcha.listeners.UserSchemeHandler;
import com.frograms.watcha.listeners.UserTasteSchemeHandler;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.model.items.PredictedRatings;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.items.UserSmall;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.serializer.CardItemDeserializer;
import com.frograms.watcha.model.serializer.DateDeserializer;
import com.frograms.watcha.model.serializer.DeckDeserializer;
import com.frograms.watcha.model.serializer.PredictedRatingsDeserializer;
import com.frograms.watcha.model.serializer.SmallUserDeserializer;
import com.frograms.watcha.model.serializer.UserActionDeserializer;
import com.frograms.watcha.model.serializer.UserBaseDeserializer;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.fonticon.FontIconTypefaceHolder;
import com.google.android.gcm.GCMRegistrar;
import com.newrelic.agent.android.NewRelic;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class WatchaApp extends MultiDexApplication {

    public static String DOMAIN = BuildConfig.END_POINT;


    private static WatchaApp singleton = null;

    private User mUser = null;


    private OkHttpClient client;
    private RequestInterceptor requestInterceptor;
    private RestAdapter.Builder restAdapterBuilder;


    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;
    public boolean wasInBackground;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;


    public void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                WatchaApp.this.wasInBackground = true;
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask, MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        this.wasInBackground = false;
    }

    private void setHttps(OkHttpClient client) {
        if (!TextUtils.isEmpty(DOMAIN) && DOMAIN.contains("https")) {
            try {
                SSLContext sslContext;
                try {
                    sslContext = SSLContext.getInstance("TLS");
                    sslContext.init(null, null, null);
                } catch (GeneralSecurityException e) {
                    throw new AssertionError(); // The system has no TLS. Just give up.
                }
                client.setSslSocketFactory(sslContext.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate() {
      super.onCreate();
      singleton = this;

      // 왓챠 페이스북 설정.
      FacebookBroker.getInstance();

      // 트위터 설정.
      TwitterAuthConfig authConfig = new TwitterAuthConfig(BuildConfig.TWITTER_KEY, BuildConfig.TWITTER_SECRET_KEY);
      Fabric.with(this, new Crashlytics(), new Twitter(authConfig));

      // 서버용 통계 툴
      if (!TextUtils.isEmpty(BuildConfig.NEWRELIC_KEY)) {
        NewRelic.withApplicationToken(BuildConfig.NEWRELIC_KEY).start(this);
      }

      AnalyticsManager.getInstance();

      FontIconTypefaceHolder.init(getAssets(), "icon.ttf");
      Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);

      try {
        try {
          GCMRegistrar.checkDevice(this);
          GCMRegistrar.checkManifest(this);
        } catch(IllegalStateException e) {
          WLog.logException(e);
        }

        boolean pushEnabled = !TextUtils.isEmpty(GCMRegistrar.getRegistrationId(this));
        if (!pushEnabled) {
          GCMRegistrar.register(this, GCMIntentService.SENDER_ID);
        }

      } catch(RuntimeException e) {
        Toast.makeText(this, getString(R.string.no_identify), Toast.LENGTH_SHORT).show();
      }

      initializeRetrofit();

      registerSchemes();
    }

  private void registerSchemes() {
    SchemeActivity.addSchemeConfig(new SchemeActivity.SchemeConfig("category", new String[] {
        "movies", "tv_seasons", "books"
    }));
    SchemeActivity.register(":category/:code/crew", new BaseSchemeHandler()); // O
    SchemeActivity.register(":category/:code/ratings/friends", new BaseSchemeHandler(FragmentTask.GENERAL_CARDS)); // O
    SchemeActivity.register(":category/:code/articles", new BaseSchemeHandler(getString(R.string.articles_page))); // O
    SchemeActivity.register(":category/:code/series", new BaseSchemeHandler()); // O
    SchemeActivity.register(":category/:code/comments", new BaseSchemeHandler(FragmentTask.GENERAL_CARDS, new BundleSet.Builder().putShowUnderbarView(true))); // O
    SchemeActivity.register(":category/:code/similar", new BaseSchemeHandler()); // O
    SchemeActivity.register(":category/recommend", new BaseCategorySchemeHandler(FragmentTask.HOME, new BundleSet.Builder().putSelectedTab(2))); // O
    SchemeActivity.register(":category/evaluate/:id", new RatingSchemeHandler()); // O
    SchemeActivity.register(":category/evaluate", new RatingSchemeHandler()); // O
    SchemeActivity.register(":category/popular", new BaseCategorySchemeHandler(FragmentTask.HOME, new BundleSet.Builder().putSelectedTab(1))); // O
    SchemeActivity.register(":category/popular/:id", new BaseCategorySchemeHandler(FragmentTask.HOME, new BundleSet.Builder().putSelectedTab(1))); // O
    SchemeActivity.register(":category/people/:id", new BaseSchemeHandler()); // O
    SchemeActivity.register(":category/tagged/:id", new BaseSchemeHandler()); // O

    SchemeActivity.register(":category/:code", new BaseCategorySchemeHandler(FragmentTask.DETAIL_CONTENT)); // O

    SchemeActivity.register("users/me", new BaseCategorySchemeHandler(FragmentTask.HOME, new BundleSet.Builder().putSelectedTab(4))); // O
    SchemeActivity.register("users/:code", new UserSchemeHandler(FragmentTask.PROFILE)); // O
    SchemeActivity.register("users/:code/ratings/:category", new UserSchemeHandler(FragmentTask.USER_CONTENT_LIST, new BundleSet.Builder().putShowUnderbarView(false).putUserActionType(UserActionType.RATE).putLoadActionCount(true))); // O
    SchemeActivity.register("users/:code/ratings/:category/:id", new CategoryRatingSchemeHandler()); // O
    SchemeActivity.register("users/:code/wishes/:category", new UserSchemeHandler(FragmentTask.USER_CONTENT_CATEGORY_LIST, new BundleSet.Builder().putShowUnderbarView(true).putUserActionType(UserActionType.WISH).putLoadActionCount(true))); // O
    SchemeActivity.register("users/:code/comments/:category", new UserSchemeHandler(FragmentTask.USER_CONTENT_CATEGORY_LIST, new BundleSet.Builder().putUserActionType(
        UserActionType.COMMENT).putShowUnderbarView(true).putLoadActionCount(true))); // O
    SchemeActivity.register("users/:code/mehs", new UserSchemeHandler(FragmentTask.USER_CONTENT_LIST, new BundleSet.Builder().putUserActionType(
        UserActionType.MEH).putLoadActionCount(true))); // O
    SchemeActivity.register("users/:code/decks", new UserSchemeHandler(FragmentTask.USER_CONTENT_LIST, new BundleSet.Builder().putUserActionType(UserActionType.DECK).putShowUnderbarView(
        true).putLoadActionCount(true))); // O
    SchemeActivity.register("users/:code/friends", new UserSchemeHandler(FragmentTask.FOLLOW, new BundleSet.Builder().putSelectedTab(
        0))); // O
    SchemeActivity.register("users/:code/followers", new UserSchemeHandler(FragmentTask.FOLLOW, new BundleSet.Builder().putSelectedTab(1))); // O
    SchemeActivity.register("users/:code/taste", new UserTasteSchemeHandler()); // O
    SchemeActivity.register("staffmades/:id", new BaseSchemeHandler()); // O
    SchemeActivity.register("comments/:code", new CommentSchemeHandler(FragmentTask.DETAIL_COMMENT)); // O
    SchemeActivity.register("comments/:code/likers", new BaseSchemeHandler(getString(R.string.likers)));
    SchemeActivity.register("comments/:code/replies", new CommentSchemeHandler(FragmentTask.REPLY, new BundleSet.Builder().putReplyType(ReplyFragment.ReplyType.COMMENT)));
    SchemeActivity.register("comments/:code/edit", new CommentSchemeHandler(FragmentTask.WRITE_COMMENT)); // O
    SchemeActivity.register("notices/:id", new NoticeSchemeHandler()); // O
    SchemeActivity.register("staffmades/awards", new BaseSchemeHandler(FragmentTask.GENERAL_CARDS, new BundleSet.Builder().putSchemeTitle(getString(R.string.collection), ""))); // O
    SchemeActivity.register("decks/popular", new BaseSchemeHandler(getString(R.string.popular_decks))); // O
    SchemeActivity.register("decks/:code", new DeckSchemeHandler(FragmentTask.DETAIL_DECK)); // O
    SchemeActivity.register("decks/:code/likers", new BaseSchemeHandler(getString(R.string.likers)));
    SchemeActivity.register("decks/:code/replies", new DeckSchemeHandler(FragmentTask.REPLY, new BundleSet.Builder().putReplyType(ReplyFragment.ReplyType.DECK)));

    SchemeActivity.register("replies/:code/likers", new BaseSchemeHandler(getString(R.string.likers)));
    SchemeActivity.register("partner", new BaseSchemeHandler(FragmentTask.PARTNER_CANDIDATES)); // O
    SchemeActivity.register("quizzes", new BaseSchemeHandler()); // O
    SchemeActivity.register("magazines", new BaseSchemeHandler()); // O
    SchemeActivity.register("feed", new BaseSchemeHandler(FragmentTask.HOME)); // O
    SchemeActivity.register("notifications", new BaseSchemeHandler(FragmentTask.HOME, new BundleSet.Builder().putSelectedTab(3))); // O
    SchemeActivity.register("search", new BaseSchemeHandler(FragmentTask.SEARCH)); // O
    SchemeActivity.register("invite", new BaseSchemeHandler(FragmentTask.INVITE_FRIENDS)); // O
    SchemeActivity.register("gifts", new BaseSchemeHandler(FragmentTask.WEBVIEW, new BundleSet.Builder()
        .putUrl(BuildConfig.END_POINT
            + "/webview/gifts", getString(R.string.hoppin)))); // O
    SchemeActivity.register("account/settings", new BaseSchemeHandler(FragmentTask.SETTING)); // O
    SchemeActivity.register("sns-share", new BaseSchemeHandler(FragmentTask.SETTING)); // O
  }

  @Override protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  private com.google.gson.Gson gson = null;

    public com.google.gson.Gson getGson() {
      if (gson == null) {
        gson = new com.google.gson.GsonBuilder().serializeNulls()
            .registerTypeAdapter(Date.class, new DateDeserializer())
            .registerTypeAdapter(CardItem.class, new CardItemDeserializer())
            .registerTypeAdapter(UserAction.class, new UserActionDeserializer())
            .registerTypeAdapter(PredictedRatings.class, new PredictedRatingsDeserializer())
            .registerTypeAdapter(UserBase.class, new UserBaseDeserializer())
            .registerTypeAdapter(UserSmall.class, new SmallUserDeserializer())
            .registerTypeAdapter(DeckBase.class, new DeckDeserializer())
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss+09:00")
            .create();
      }
        return gson;
    }

    public static void initializeRetrofit() {
      SynchronousQueue<Runnable> queue = new SynchronousQueue<>();
      ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 30, 15, TimeUnit.SECONDS, queue);
      getInstance().client = getInstance().getHttpClient();
      getInstance().gson = getInstance().getGson();
      getInstance().restAdapterBuilder = new RestAdapter.Builder().setEndpoint(DOMAIN)
          .setClient(new OkClient(getInstance().client))
          .setExecutors(AsyncTask.THREAD_POOL_EXECUTOR, executor)
          .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
          .setConverter(new GsonConverter(getInstance().getGson()));
    }

    public static RestAdapter.Builder getRestAdapterBuilder() {
        return getInstance().restAdapterBuilder;
    }

  public RequestInterceptor getRetrofitRequestInterceptor(final WGuinnessHeader referrerHeader) {
    requestInterceptor = request -> {
      request.addHeader("Accept", "application/json");
      if (referrerHeader != null && !TextUtils.isEmpty(referrerHeader.getValue())) {
        request.addHeader(referrerHeader.getKey(), referrerHeader.getValue());
      }
      // 왓챠 커스텀 헤더.
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.USER_AGENT, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.VERSION, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.DEVICE_ID, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.FACEBOOK_ACCESS_TOKEN, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.FACEBOOK_USER_ID, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.REGISTRATION_ID, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.STAGING_FROG, request);
      WGuinnessHeader.setRequestHeader(WGuinnessHeader.Type.REFERRER, request);
    };
    return requestInterceptor;
  }

    private OkHttpClient getHttpClient() {
        client = new OkHttpClient();
        try {
            int cacheSize = 5 * 1024 * 1024; // 10 MiB
            File cacheDirectory = new File(getCacheDir().getAbsolutePath(), "HttpCache");
            Cache cache = new Cache(cacheDirectory, cacheSize);
            client.setCache(cache);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setHttps(client);
        getSessionManager().setUpCookie(this, client);
        return client;
    }

  private WSessionManager mSessionManager;
  public static WSessionManager getSessionManager() {
    if (getInstance().mSessionManager == null) {
      getInstance().mSessionManager = new WSessionManager();
    }
    return getInstance().mSessionManager;
  }


  public static WatchaApp getInstance() {
    return singleton;
  }

    public void goYoutube(Context context, String youtubeId) {
        Intent intent = new Intent(context, YoutubeActivity.class);
        Bundle b = new Bundle();
        b.putString("youtube_id", youtubeId);
        b.putBoolean("force_fullscreen", true);
        intent.putExtras(b);
        context.startActivity(intent);
    }


    public void setUser(User user) {
        mUser = user;
    }

    public static User getUser() {
      return getInstance().mUser;
    }
}
