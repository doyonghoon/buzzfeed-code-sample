package com.frograms.watcha.listeners;

public interface OnRemoveItemListener {
  public void onRemove(int position);
}
