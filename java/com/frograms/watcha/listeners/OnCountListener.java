package com.frograms.watcha.listeners;

public interface OnCountListener {
  public void onCount(int... count);
}
