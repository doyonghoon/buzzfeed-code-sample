package com.frograms.watcha.listeners;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.DeckSelectable;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.DeckData;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by doyonghoon on 15. 6. 10..
 */
public class BottomSheetDeckCardItemListener implements RecyclerAdapter.OnClickCardItemListener {

  private Activity mActivity;
  //private Content mContent;
  private String mContentCode;
  private BottomSheetLayout mLayout;

  public BottomSheetDeckCardItemListener(@NonNull BottomSheetLayout layout,
      @NonNull Content content) {
    mLayout = layout;
    mActivity = (Activity) mLayout.getContext();
    mContentCode = content.getCode();
  }

  public BottomSheetDeckCardItemListener(@NonNull BottomSheetLayout layout,
      @NonNull String contentCode) {
    mLayout = layout;
    mActivity = (Activity) mLayout.getContext();
    mContentCode = contentCode;
  }

  @Override public void onClickCardItem(ItemView v, Item item) {
    if (item instanceof DeckSelectable) {
      final DeckSelectable deck = (DeckSelectable) item;
      QueryType requestType = deck.isAdded() ? QueryType.DECK_ITEM_REMOVE.setApi(deck.getDeckCode())
          : QueryType.DECK_ITEM_ADD.setApi(deck.getDeckCode());

      Map<String, String> deckUpdateParams = new HashMap<>();
      deckUpdateParams.put("content_codes", mContentCode);

      ReferrerBuilder referrerBuilder = createReferrerBuilder();
      if (referrerBuilder != null) {
        referrerBuilder.appendArgument(deck.getDeckCode());
      }
      DataProvider<BaseResponse<DeckData>> provider =
          new DataProvider(mActivity, requestType, referrerBuilder);
      provider.withParams(deckUpdateParams);
      provider.responseTo(new BaseApiResponseListener<BaseResponse<DeckData>>(mActivity) {
        @Override public void onSuccess(@NonNull final QueryType queryType,
            @NonNull BaseResponse<DeckData> result) {
          super.onSuccess(queryType, result);
          if (result.getData() != null) {
            if (mLayout.isSheetShowing()) {
              mLayout.dismissSheet();
            }
            String message = deck.isAdded() ? deck.getDeckName() + " 컬렉션에서 삭제되었어요"
                : deck.getDeckName() + " 컬렉션에 추가되었어요";
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
          }
        }
      });
      provider.request();
    }
  }

  private Observable<DeckSelectable> getDeckItemObservable(final Item item) {
    return Observable.create(new Observable.OnSubscribe<DeckSelectable>() {
      @Override public void call(Subscriber<? super DeckSelectable> subscriber) {
        if (item instanceof DeckSelectable) {
          subscriber.onNext((DeckSelectable) item);
        }
      }
    });
  }

  private Observable<Map<String, String>> getParamsObservable(@NonNull final DeckSelectable item) {
    return Observable.create(new Observable.OnSubscribe<Map<String, String>>() {
      @Override public void call(Subscriber<? super Map<String, String>> subscriber) {
        subscriber.onNext(Collections.singletonMap("content_codes", item.getDeckCode()));
        subscriber.onCompleted();
      }
    }).filter(s -> s != null && !s.isEmpty() && s.containsKey("content_codes"));
  }

  private Observable<QueryType> getQueryTypeObservable(@NonNull final DeckSelectable item) {
    return Observable.create(subscriber -> {
      QueryType requestType =
          item.isAdded() ? QueryType.DECK_ITEM_REMOVE.setApi(item.getDeckCode())
              : QueryType.DECK_ITEM_ADD.setApi(item.getDeckCode());
    });
  }

  private Observable<DeckData> request(@NonNull final String code) {
    return null;
  }

  private ReferrerBuilder createReferrerBuilder() {
    if (mLayout != null
        && mLayout.getContext() != null
        && mLayout.getContext() instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) mLayout.getContext();
      BaseFragment baseFragment = baseActivity.getFragment();
      if (baseFragment != null) {
        return new ReferrerBuilder(baseFragment.getCurrentScreenName());
      }
    }
    return null;
  }
}
