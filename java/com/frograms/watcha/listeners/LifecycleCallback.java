package com.frograms.watcha.listeners;

/**
 * 앱이 백그라운드로 가거나 백그라운드에서 실행되는지 콜백해줌.
 */
public interface LifecycleCallback {
  void onAppBackFromBackground();

  void onAppGoesBackground();
}
