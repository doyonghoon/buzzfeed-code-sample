package com.frograms.watcha.listeners;

import android.net.Uri;
import com.frograms.watcha.model.SchemeHandler;

public class Scheme {

  private Uri mUri;
  private SchemeHandler mSchemeHandler;

  public Scheme(Uri uri, SchemeHandler handler) {
    mUri = uri;
    mSchemeHandler = handler;
  }

  public Uri getUri() {
    return mUri;
  }

  public SchemeHandler getSchemeHandler() {
    return mSchemeHandler;
  }
}
