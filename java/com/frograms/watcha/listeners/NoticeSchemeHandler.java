package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;

/**
 * 공지사항 스키마.
 */
public class NoticeSchemeHandler extends BaseSchemeHandler {

  public NoticeSchemeHandler() {
    this(FragmentTask.WEBVIEW);
  }

  public NoticeSchemeHandler(@NonNull FragmentTask task) {
    super(task, null);
  }

  public NoticeSchemeHandler(@NonNull FragmentTask task, @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override
  protected BundleSet.Builder createDefaultBundleSet(@Nullable BundleSet.Builder builder) {
    mLazyBundleBuilder = super.createDefaultBundleSet(builder);
    mLazyBundleBuilder.putUrl(BuildConfig.END_POINT + "/webview/notices", "공지사항");
    return mLazyBundleBuilder;
  }

  @Override protected void putId(String id) {
    mLazyBundleBuilder.putUrl(BuildConfig.END_POINT + "/webview/notices/" + id, "공지사항");
  }
}
