package com.frograms.watcha.listeners;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.Content;

/**
 * Created by doyonghoon on 15. 6. 10..
 */
public class BottomSheetDeckCreateButtonClickListener implements View.OnClickListener {

  private Context mContext;
  private BottomSheetLayout mLayout;
  private String mContentCode;

  public BottomSheetDeckCreateButtonClickListener(@NonNull BottomSheetLayout layout,
      @NonNull Content content) {
    mLayout = layout;
    mContext = layout.getContext();
    mContentCode = content.getCode();
  }

  public BottomSheetDeckCreateButtonClickListener(@NonNull BottomSheetLayout layout,
      @NonNull String contentCode) {
    mLayout = layout;
    mContext = layout.getContext();
    mContentCode = contentCode;
  }

  @Override public void onClick(View v) {
    if (mLayout != null && mContext != null && !TextUtils.isEmpty(mContentCode)) {
      if (mLayout.isSheetShowing()) {
        mLayout.dismissSheet();
      }

      ActivityStarter.with(mContext, FragmentTask.CREATE_DECK)
          .setRequestCode(ActivityStarter.CREATE_DECK_REQUEST)
          .addBundle(new BundleSet.Builder().putContentCode(mContentCode).build().getBundle())
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .start();
    }
  }
}
