package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.enums.UserActionType;

/**
 * N점 준 영화/드라마/도서 스키마.
 */
public class CategoryRatingSchemeHandler extends BaseSchemeHandler {

  public CategoryRatingSchemeHandler() {
    this(FragmentTask.USER_CONTENT_LIST);
  }

  public CategoryRatingSchemeHandler(@NonNull FragmentTask task) {
    super(task);
  }

  public CategoryRatingSchemeHandler(@NonNull FragmentTask task,
      @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override
  protected BundleSet.Builder createDefaultBundleSet(@Nullable BundleSet.Builder builder) {
    mLazyBundleBuilder = super.createDefaultBundleSet(builder)
        .putUserActionType(UserActionType.RATE)
        .putLoadActionCount(true)
        .putShowUnderbarView(true);
    return mLazyBundleBuilder;
  }

  @Override protected void putId(String id) {
    mLazyBundleBuilder.putContentRating(id);
  }

  @Override protected void putCode(String code) {
    mLazyBundleBuilder.putUserCode(code);
  }
}
