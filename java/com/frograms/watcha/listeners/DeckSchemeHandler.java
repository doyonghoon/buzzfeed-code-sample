package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;

/**
 * 덱 스키마.
 */
public class DeckSchemeHandler extends BaseSchemeHandler {

  public DeckSchemeHandler() {
    super();
  }

  public DeckSchemeHandler(@NonNull FragmentTask task) {
    super(task, null);
  }

  public DeckSchemeHandler(@NonNull FragmentTask task, @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override protected void putCode(String code) {
    mLazyBundleBuilder.putDeckCode(code);
  }
}
