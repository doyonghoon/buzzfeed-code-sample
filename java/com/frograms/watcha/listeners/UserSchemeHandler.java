package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;

/**
 * 유저 스키마.
 */
public class UserSchemeHandler extends BaseSchemeHandler {

  public UserSchemeHandler() {
    super();
  }

  public UserSchemeHandler(@NonNull FragmentTask task) {
    super(task);
  }

  public UserSchemeHandler(@NonNull FragmentTask task, @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override protected void putCode(String code) {
    mLazyBundleBuilder.putUserCode(code);
  }
}
