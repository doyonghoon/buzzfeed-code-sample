package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;

/**
 * 스키마 핸들러.
 */
public class BaseCategorySchemeHandler extends BaseSchemeHandler {

  public BaseCategorySchemeHandler() {
    super();
  }

  public BaseCategorySchemeHandler(@NonNull FragmentTask task) {
    super(task);
  }

  public BaseCategorySchemeHandler(@NonNull FragmentTask task,
      @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override protected void putCode(String code) {
    mLazyBundleBuilder.putContentCode(code);
  }

  @Override protected void putId(String id) {
    switch (id) {
      case "ko":
        mLazyBundleBuilder.putCategoryId("0");
        break;

      case "us":
        mLazyBundleBuilder.putCategoryId("1");
        break;

      case "jp":
        mLazyBundleBuilder.putCategoryId("2");
        break;
    }
  }
}
