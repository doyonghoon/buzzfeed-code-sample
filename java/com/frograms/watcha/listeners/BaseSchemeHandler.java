package com.frograms.watcha.listeners;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.activity.InitActivity;
import com.frograms.watcha.fragment.GeneralCardsFragment;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.SchemeHandler;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.utils.WLog;
import java.util.Map;

/**
 * 스키마 핸들러.
 */
public class BaseSchemeHandler implements SchemeHandler {

  protected FragmentTask mFragmentTask = null;
  protected BundleSet.Builder mLazyBundleBuilder = new BundleSet.Builder();
  protected Bundle mDefaultBundle = null;

  protected String mTitle;

  public BaseSchemeHandler() {
    this(FragmentTask.GENERAL_CARDS);
  }

  public BaseSchemeHandler(String title) {
    this(FragmentTask.GENERAL_CARDS);
    mTitle = title;
  }

  public BaseSchemeHandler(@NonNull FragmentTask task) {
    this(task, null);
  }

  public BaseSchemeHandler(@NonNull FragmentTask task, @Nullable BundleSet.Builder builder) {
    mFragmentTask = task;
    mDefaultBundle = createDefaultBundleSet(builder).build().getBundle();
  }

  @Override public void onScheme(Context context, Uri uri, Map<String, String> params) {
    WLog.i("uri: " + uri + ", params: " + params);
    switch (mFragmentTask) {
      case GENERAL_CARDS:
        mLazyBundleBuilder.putSchemeApi(GeneralCardsFragment.buildSchemeApi(uri));
        break;
      default:
        mLazyBundleBuilder = createBundleSetBuilder(params);
        break;
    }

    if (mTitle != null) {
      mLazyBundleBuilder.putSchemeTitle(mTitle, null);
    }

    if (params.containsKey(BundleSet.PREVIOUS_SCREEN_NAME)) {
      mLazyBundleBuilder.putPreviousScreenName(params.get(BundleSet.PREVIOUS_SCREEN_NAME));
    }

    mDefaultBundle.putAll(mLazyBundleBuilder.build().getBundle());
    if (mFragmentTask == null) {
      context.startActivity(new Intent(context, InitActivity.class));
    } else if (mFragmentTask == FragmentTask.INIT) {
      context.startActivity(new Intent(context, InitActivity.class));
    } else {
      ActivityStarter.with(context, mFragmentTask)
          .setRequestCode(getRequestCode())
          .addBundle(mDefaultBundle)
          .start();
    }
  }

  @ActivityStarter.RequestCode protected int getRequestCode() {
    @ActivityStarter.RequestCode int none = 0;
    return none;
  }

  protected BundleSet.Builder createDefaultBundleSet(@Nullable BundleSet.Builder builder) {
    if (builder == null) {
      mLazyBundleBuilder = new BundleSet.Builder();
    } else {
      mLazyBundleBuilder = builder;
    }
    return mLazyBundleBuilder;
  }

  protected BundleSet.Builder createBundleSetBuilder(Map<String, String> params) {
    if (params.containsKey("category")) {
      mLazyBundleBuilder.putCategoryType(CategoryType.getCategory(params.get("category")));
    }

    if (params.containsKey("code")) {
      putCode(params.get("code"));
    }

    if (params.containsKey("id")) {
      putId(params.get("id"));
    }
    return mLazyBundleBuilder;
  }

  protected void putCode(String code) {
  }

  protected void putId(String id) {
  }
}
