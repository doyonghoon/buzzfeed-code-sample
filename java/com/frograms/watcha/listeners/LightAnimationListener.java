package com.frograms.watcha.listeners;

import com.nineoldandroids.animation.Animator;

/**
 * Created by doyonghoon on 15. 3. 27..
 */
public class LightAnimationListener implements Animator.AnimatorListener {
  @Override public void onAnimationStart(Animator animation) {
  }

  @Override public void onAnimationEnd(Animator animation) {
  }

  @Override public void onAnimationCancel(Animator animation) {
  }

  @Override public void onAnimationRepeat(Animator animation) {
  }
}
