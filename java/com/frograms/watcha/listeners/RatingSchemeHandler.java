package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import java.util.Map;

/**
 * 스키마 핸들러.
 */
public class RatingSchemeHandler extends BaseSchemeHandler {

  public RatingSchemeHandler() {
    this(FragmentTask.RATING_TAB);
  }

  public RatingSchemeHandler(@NonNull FragmentTask task) {
    super(task);
  }

  public RatingSchemeHandler(@NonNull FragmentTask task, @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override protected int getRequestCode() {
    return ActivityStarter.INCREASE_NUMBER_OF_CONTENTS_REQUEST;
  }

  @Override protected BundleSet.Builder createBundleSetBuilder(Map<String, String> params) {
    super.createBundleSetBuilder(params);

    if (params.containsKey("category")) {
      mLazyBundleBuilder.putSelectedTab(
          CategoryType.getCategory(params.get("category")).getTypeInt());
    }

    return mLazyBundleBuilder;
  }

  @Override protected void putId(String id) {
    mLazyBundleBuilder.putCategoryId(id);
  }
}
