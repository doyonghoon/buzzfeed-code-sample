package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;

/**
 * 유저 취향분석 스키마.
 */
public class UserTasteSchemeHandler extends BaseSchemeHandler {

  public UserTasteSchemeHandler() {
    this(FragmentTask.WEBVIEW);
  }

  public UserTasteSchemeHandler(@NonNull FragmentTask task) {
    super(task);
  }

  public UserTasteSchemeHandler(@NonNull FragmentTask task, @Nullable BundleSet.Builder builder) {
    super(task, builder);
  }

  @Override protected void putCode(String code) {
    mLazyBundleBuilder.putUrl(BuildConfig.END_POINT + "/users/" + code + "/taste",
        WatchaApp.getInstance().getResources().getString(R.string.taste_more));

    mLazyBundleBuilder.putUserCode(code);
  }
}
