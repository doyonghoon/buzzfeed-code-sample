package com.frograms.watcha.listeners;

/**
 * Created by doyonghoon on 15. 2. 27..
 */
public class UserActionPopupListener implements UserActionPopupCallback {
  @Override public void onChangedRating(float rating) {

  }

  @Override public void onClickWish() {

  }

  @Override public void onClickMeh() {

  }

  @Override public void onClickComment() {

  }

  @Override public void onClickAddCollection() {

  }
}
