package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.utils.WatchaPermission;

/**
 * 페이스북에 새로운 퍼미션을 background thread로 요청하고 콜백받을 인터페이스.
 */
public interface OnPermissionListener {
  /**
   * 페이스북에서 퍼미션을 받고 왔음.
   *
   * @param accessToken 페이스북 유저의 토큰 값. 이 값이 Null이면 성공하지 못한 것임.
   * @param permission 어떤 퍼미션 묶음을 요청한건지 콜백함.
   */
  public void onPermissionCallback(@Nullable String accessToken,
      @NonNull WatchaPermission permission);
}
