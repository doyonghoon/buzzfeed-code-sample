package com.frograms.watcha.listeners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.retrofit.WatchaApi;
import java.util.Map;

/**
 * 서버로 데이터 요청할때 사용하는 틀.
 */
public interface WatchaQuery<T extends BaseResponse> {
  /**
   * 레트로핏에서 사용하는 인터페이스를 그룹으로 묶어서 관리하려고 만듬.
   *
   * @param request 레트로핏 요청 인터페이스.
   * @param params 요청할때 포함시킬 파라미터.
   * @param files 요청할때 포함시킬 파일.
   * @return 구현 코드에선 BaseResponse받은 모델을 반환 해주기만 하면 됨.
   */
  T getResponse(@NonNull WatchaApi request, @Nullable Map<String, String> params,
      @Nullable QueryFile[] files);
}
