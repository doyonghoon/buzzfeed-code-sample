package com.frograms.watcha.listeners;

import java.util.List;

/**
 * 페이스북 퍼미션들을 반환하는 인터페이스.
 *
 * enum 타입으로 선언한 퍼미션들의 묶음에 사용되는 인터페이스.
 * 이 곳에서 사용됨. {@link com.frograms.watcha.utils.WatchaPermission}
 */
public interface WatchaPermissionsCreator {
  /**
   * 새로운 퍼미션을 요청하거나, 페이스북 유저가 퍼미션들을 가지고 있는지 확인할때 사용하면 됨.
   */
  List<String> getPermissions();
}
