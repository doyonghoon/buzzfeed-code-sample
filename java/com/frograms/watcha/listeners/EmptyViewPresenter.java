package com.frograms.watcha.listeners;

import android.view.View;
import com.frograms.watcha.fragment.abstracts.ListFragment;

/**
 * 카드가 없을 때 보여줄 뷰.
 *
 * @see ListFragment
 */
public interface EmptyViewPresenter {
  View getEmptyView();
}
