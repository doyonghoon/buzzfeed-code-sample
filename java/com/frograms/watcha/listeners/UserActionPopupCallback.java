package com.frograms.watcha.listeners;

/**
 * Created by doyonghoon on 15. 2. 27..
 */
public interface UserActionPopupCallback {
  public void onChangedRating(float rating);

  public void onClickWish();

  public void onClickMeh();

  public void onClickComment();

  public void onClickAddCollection();
}
