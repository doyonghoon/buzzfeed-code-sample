package com.frograms.watcha.listeners;

/**
 * 뒤로가기 버튼 눌렀을때 콜백.
 *
 * 뒤로가기 버튼 눌렀을때 이 값으로 액티비티를 종료하거나 그냥 놔둠.
 */
public interface OnBackKeyListener {

  /**
   * 뒤로가기 버튼 눌렀을때 불림.
   *
   * @return true면 액티비티를 종료하고 false면 그냥 놔둠.
   */
  boolean onBackKeyDown();
}
