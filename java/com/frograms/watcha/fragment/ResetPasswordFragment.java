package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.OnClick;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FormValidator;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.textviews.RobotoBoldView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import java.util.HashMap;
import java.util.Map;

/**
 * 비밀번호 찾기 화면.
 */
public class ResetPasswordFragment extends BaseFragment
    implements FormValidator.FormValidationListener {

  @Bind(R.id.email) EditText mEmailTextView;
  @Bind(R.id.btn_find_password) RobotoBoldView mFindPasswordButton;
  @Bind(R.id.find_password_title) TextView mTitle;
  @Bind(R.id.find_password_description) TextView mDescription;

  private final FormValidator mEmailValidator = new FormValidator(FormValidator.FormStyle.EMAIL);
  private boolean isValidEmail = false;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_find_password;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return null;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("비밀번호 초기화");
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.RESET_PASSWORD;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    activateFindPasswordButton(false);
    mEmailValidator.registerValidator(mEmailTextView, this);
    FontHelper.RobotoRegular(mEmailTextView);
  }

  @OnClick(R.id.btn_find_password) void onClickFindPassword() {
    if (isValidEmail) {
      requestDataProvider(mEmailTextView.getText().toString());
    } else {
      String message =
          mEmailTextView.getText().toString().length() > 0 ? "이메일 형식이 맞지 않습니다.\n옳바른 이메일을 입력해주세요."
              : "이메일을 입력해주세요";
      SnackbarManager.show(Snackbar.with(getActivity())
          .text(message)
          .position(Snackbar.SnackbarPosition.BOTTOM)
          .type(mEmailTextView.getText().toString().length() > 0 ? SnackbarType.MULTI_LINE
              : SnackbarType.SINGLE_LINE), getActivity());
    }
  }

  private void requestDataProvider(String email) {
    Map<String, String> params = new HashMap<>();
    params.put("email", email);
    new DataProvider<>(getActivity(), QueryType.FIND_PASSWORD, createReferrerBuilder())
        .responseTo((queryType, result) -> {
          Toast.makeText(getActivity(), "비밀번호를 재설정할 수 있는 웹페이지 링크를 이메일로 보내드렸습니다.", Toast.LENGTH_LONG)
              .show();
          mEmailTextView.setText("");
          getActivity().finish();
        })
        .withParams(params)
        .withDialogMessage("처리 중..")
        .request();
  }

  private FontIconDrawable getCheckIconDrawable(boolean isValid) {
    FontIconDrawable checkIcon = null;
    if (isValid) {
      checkIcon = FontIconDrawable.inflate(getActivity(), R.xml.icon_arrow_check);
      checkIcon.setTextColor(getResources().getColor(R.color.green));
      checkIcon.setTextSize(getResources().getDimension(R.dimen.more_font));
    }
    return checkIcon;
  }

  private void activateFindPasswordButton(boolean enabled) {
    mFindPasswordButton.setEnabled(enabled);
    mFindPasswordButton.setBackgroundResource(
        enabled ? R.drawable.bg_welcome_ok : R.drawable.bg_button_corner_gray_nor);
  }

  @Override public void onValidation(FormValidator.FormStyle style, EditText v, String text,
      boolean isValid) {
    switch (style) {
      case EMAIL:
        isValidEmail = isValid;
        break;
    }
    activateFindPasswordButton(isValidEmail);
    v.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(isValid), null);
  }

  @Override protected boolean hasDefaultDropshadow() {
    return false;
  }
}
