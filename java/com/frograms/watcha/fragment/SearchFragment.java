package com.frograms.watcha.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.IncredibleSearchTextLayout;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

/**
 * 검색화면.
 */
public class SearchFragment extends ListFragment {

  private IncredibleSearchTextLayout mSearchTextLayout = null;

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.EXPLORE;
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected QueryType getQueryType() {
    return QueryType.EXPLORE;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getToolbar()
        .setBackgroundColor(getResources().getColor(R.color.heavy_purple));
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override protected View getCustomToolbarView() {
    return getSearchTextView();
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mList.setBackgroundColor(getResources().getColor(R.color.heavy_purple));
  }

  private IncredibleSearchTextLayout getSearchTextView() {
    if (mSearchTextLayout == null) {
      mSearchTextLayout = new IncredibleSearchTextLayout.Builder(getActivity()).setStyle(
          IncredibleSearchTextLayout.Builder.Style.DARK)
          .enableDropdown(true)
          .enableAutocomplete(true)
          .enableQueryCache(true)
          .setHintText("제목, 감독, 배우 검색 (초성)")
          .build();
    }
    return mSearchTextLayout;
  }

  /**
   * 뒤로가기나 툴바 닫기 버튼 눌릴때 키보드 닫고 종료.
   *
   * 뒤로가기 {@link #onBackPressed(boolean)}
   * 툴바 닫기 버튼 {@link Toolbar#setNavigationOnClickListener(View.OnClickListener)}
   *
   * @return 뒤로가기 버튼 눌릴때 콜백받을 리스너.
   */
  @Override public OnBackKeyListener getOnBackKeyListener() {
    return () -> {
      getSearchTextView().closeKeyboard();
      return true;
    };
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == ActivityStarter.SEARCH_RESULT_REQUEST) {
      // 유저가 검색결과 화면에서 뒤로 왔다면, 걍 무조건 검색뷰 띄움.
      Handler handler = new Handler();
      handler.postDelayed(() -> getSearchTextView().onResume(), 500);
    }
  }
}
