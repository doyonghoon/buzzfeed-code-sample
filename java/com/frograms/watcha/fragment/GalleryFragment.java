package com.frograms.watcha.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.GalleryViewPager;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Media;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.YoutubeThumbnailUtils;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.widget.TouchImageView;
import com.frograms.watcha.view.widget.wImages.ImageType;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

/**
 * 사진 보여주는 화면.
 */
public class GalleryFragment extends BaseFragment {

  public enum GalleryType {
    PROFILE_PICTURE(0),
    PROFILE_COVER(1),
    MOVIE(2),
    DRAMA(3);

    private int mIntValue = -1;

    GalleryType(int intValue) {
      mIntValue = intValue;
    }

    public int getIntValue() {
      return mIntValue;
    }

    public static GalleryType getType(int intValue) {
      for (GalleryType t : values()) {
        if (t.getIntValue() == intValue) {
          return t;
        }
      }
      return null;
    }
  }

  @Bind(R.id.gallery_pager) GalleryViewPager mGalleryPager;
  @Bind(R.id.gallery_image) TouchImageView mGalleryImageView;

  @Bind(R.id.gallery_page) TextView mPageTextView;
  @Bind(R.id.gallery_navigation) View mNavigationView;

  private OnToolbarVisibilityChangeListener mToolbarVisibilityChangeListener = null;
  private String mUrl;

  private String mMediaJson;
  private int mCurrentPosition = 0;
  private List<Media> mMedias = new ArrayList<>();
  private GalleryType mGalleryType = null;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null) {
      if (bundle.containsKey(BundleSet.MEDIA_JSON_ARRAY) && bundle.containsKey(
          BundleSet.SELECTED_TAB)) {
        mCurrentPosition = bundle.getInt(BundleSet.SELECTED_TAB);
        mMediaJson = bundle.getString(BundleSet.MEDIA_JSON_ARRAY);
        mMedias =
            WatchaApp.getInstance().getGson().fromJson(mMediaJson, new TypeToken<List<Media>>() {
            }.getType());
      }

      if (bundle.containsKey(BundleSet.IMAGE_URL)) {
        mUrl = bundle.getString(BundleSet.IMAGE_URL);
      }

      if (bundle.containsKey(BundleSet.GALLERY_TYPE)) {
        mGalleryType = GalleryType.getType(bundle.getInt(BundleSet.GALLERY_TYPE));
        AnalyticsManager.sendScreenName(getCurrentScreenName());
      }
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    if (mGalleryType != null) {
      switch (mGalleryType) {
        case MOVIE:
          return AnalyticsManager.GALLERY_MOVIE;
        case DRAMA:
          return AnalyticsManager.GALLERY_DRAMA;
        case PROFILE_COVER:
          return AnalyticsManager.GALLERY_PROFILE_COVER;
        case PROFILE_PICTURE:
          return AnalyticsManager.GALLERY_PROFILE_PICTURE;
      }
    }
    return null;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.SELECTED_TAB, mCurrentPosition);
    outState.putString(BundleSet.MEDIA_JSON_ARRAY, mMediaJson);
    outState.putString(BundleSet.IMAGE_URL, mUrl);
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_gallery;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("갤러리");
    FontIconDrawable back = FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back);
    back.setTextSize(getResources().getDimension(R.dimen.toolbar_navi_back_icon_size));
    back.setTextColor(getResources().getColor(R.color.white_alpha_70));
    getToolbarHelper().getToolbar().setNavigationIcon(back);
    getToolbarHelper().getToolbar().setBackgroundColor(Color.BLACK);
    setStatusbarColor(Color.BLACK);
  }

  private boolean isMediaGallery() {
    return mMedias != null && !mMedias.isEmpty();
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (isMediaGallery()) {
      mToolbarVisibilityChangeListener =
          new OnToolbarVisibilityChangeListener(getToolbarHelper().getToolbar(), mNavigationView);
      mGalleryImageView.setVisibility(View.GONE);
      mGalleryPager.setVisibility(View.VISIBLE);
      TouchImageAdapter adapter = new TouchImageAdapter(mToolbarVisibilityChangeListener);
      adapter.setMedias(mMedias);
      mGalleryPager.setAdapter(adapter);
      mGalleryPager.setCurrentItem(mCurrentPosition);
      mGalleryPager.setOffscreenPageLimit(mMedias.size());

      mPageTextView.setText(getPageText(mCurrentPosition));
      mGalleryPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override public void onPageSelected(int position) {
          mPageTextView.setText(getPageText(position));
          if (mToolbarVisibilityChangeListener != null) {
            View[] views = mToolbarVisibilityChangeListener.getViews();
            for (View v : views) {
              if (v.getVisibility() != View.GONE) {
                v.setVisibility(View.GONE);
              }
            }
          }
        }

        @Override public void onPageScrollStateChanged(int state) {

        }
      });
      getToolbarHelper().getToolbar().setVisibility(View.VISIBLE);
      mNavigationView.setVisibility(View.VISIBLE);
    } else {
      mToolbarVisibilityChangeListener =
          new OnToolbarVisibilityChangeListener(getToolbarHelper().getToolbar());
      mNavigationView.setVisibility(View.GONE);
    }

    mGalleryImageView.setOnClickListener(mToolbarVisibilityChangeListener);
    if (!TextUtils.isEmpty(mUrl)) {
      mGalleryPager.setVisibility(View.GONE);
      mGalleryImageView.setImageType(ImageType.PROFILE)
          .setScaleType(ImageView.ScaleType.FIT_CENTER);

      mGalleryImageView.load(mUrl);
    }
  }

  private String getPageText(int position) {
    return (position + 1) + " / " + mMedias.size();
  }

  @OnClick(R.id.gallery_navigation) void onClickNavigationLayout() {
  }

  @OnClick(R.id.gallery_left) void onClickGalleryLeft() {
    if (mGalleryPager.getCurrentItem() + 1 > 0) {
      mGalleryPager.setCurrentItem(mGalleryPager.getCurrentItem() - 1);
    }
  }

  @OnClick(R.id.gallery_right) void onClickGalleryRight() {
    if (mGalleryPager.getCurrentItem() + 1 < mMedias.size()) {
      mGalleryPager.setCurrentItem(mGalleryPager.getCurrentItem() + 1);
    }
  }

  private static class OnToolbarVisibilityChangeListener implements View.OnClickListener {

    private View[] mViews;

    private OnToolbarVisibilityChangeListener(@NonNull View... views) {
      mViews = views;
    }

    @Override public void onClick(View v) {
      setVisibility();
    }

    public void setVisibility() {
      for (View view : mViews) {
        view.setVisibility(view.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
      }
    }

    public View[] getViews() {
      return mViews;
    }
  }

  static class TouchImageAdapter extends PagerAdapter {

    private OnToolbarVisibilityChangeListener mVisibilityListener;

    private TouchImageAdapter(@NonNull OnToolbarVisibilityChangeListener listener) {
      mVisibilityListener = listener;
    }

    private List<Media> mMedias = new ArrayList<>();

    void setMedias(List<Media> urls) {
      mMedias.clear();
      mMedias.addAll(urls);
    }

    @Override public int getCount() {
      return mMedias.size();
    }

    @Override public View instantiateItem(final ViewGroup container, final int position) {
      final View v = LayoutInflater.from(container.getContext())
          .inflate(R.layout.view_item_gallery_fullscreen, container, false);
      final TouchImageView image = ButterKnife.findById(v, R.id.gallery_image);
      final View playIcon = ButterKnife.findById(v, R.id.media_play);
      playIcon.setVisibility(View.GONE);
      switch (mMedias.get(position).getType()) {
        case PHOTO:
          image.setImageType(ImageType.POSTER).load(mMedias.get(position).getPhoto().getXLarge());
          image.setOnClickListener(mVisibilityListener);
          image.setRatioType(RatioType.POSTER);
          image.setScaleType(ImageView.ScaleType.FIT_CENTER);
          break;
        case COVER:
          if (mMedias.get(position).getPhoto() != null) {
            image.setImageType(ImageType.STILLCUT)
                .load(mMedias.get(position).getPhoto().getXLarge());
            image.setOnClickListener(mVisibilityListener);
            image.setRatioType(RatioType.STILLCUT);
            image.setScaleType(ImageView.ScaleType.FIT_CENTER);
          }
          break;
        case YOUTUBE:
          image.setImageType(ImageType.STILLCUT);
          image.setRatioType(RatioType.STILLCUT);
          image.load(YoutubeThumbnailUtils.getUrl(mMedias.get(position).getUrl(),
              YoutubeThumbnailUtils.Quality.HQ_DEFAULT));
          image.setScaleType(ImageView.ScaleType.FIT_CENTER);
          playIcon.setVisibility(View.VISIBLE);
          playIcon.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
              WatchaApp.getInstance()
                  .goYoutube(container.getContext(), mMedias.get(position).getUrl());
            }
          });
          break;
      }
      container.addView(v, LinearLayout.LayoutParams.MATCH_PARENT,
          LinearLayout.LayoutParams.MATCH_PARENT);
      return v;
    }

    @Override public void destroyItem(ViewGroup container, int position, Object object) {
      container.removeView((View) object);
    }

    @Override public boolean isViewFromObject(View view, Object object) {
      return view == object;
    }
  }
}
