package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import butterknife.Bind;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.TwitterBroker;
import com.frograms.watcha.view.SettingItemView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.widget.IconActionGridButton;

/**
 * 친구찾기 화면.
 */
public class FindUserFragment extends BaseFragment {

  @Bind(R.id.find_user_invite) IconActionGridButton mInviteButton;
  @Bind(R.id.find_user_search) SettingItemView mSearchButton;
  @Bind(R.id.find_user_facebook) SettingItemView mFacebookButton;
  @Bind(R.id.find_user_twitter) SettingItemView mTwitterButton;

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().setToolbarTitle("친구찾기");
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_find_user;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.FIND_FRIENDS;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mInviteButton.setTextNormalColor(getResources().getColor(R.color.gray));
    mInviteButton.setIconColor(getResources().getColor(R.color.gray));
    mSearchButton.getIconView().setTextColor(getResources().getColor(R.color.black));
    mFacebookButton.getIconView().setTextColor(getResources().getColor(R.color.facebook));
    mTwitterButton.getIconView().setTextColor(getResources().getColor(R.color.twitter));
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @OnClick(R.id.find_user_search) void onClickSearch() {
    ActivityStarter.with(getActivity(), FragmentTask.SEARCH_USER)
        .addBundle(new BundleSet.Builder().putPreviousScreenName(getCurrentScreenName())
            .build()
            .getBundle())
        .start();
  }

  @OnClick(R.id.find_user_invite) void onClickInvite() {
    ActivityStarter.with(getActivity(), FragmentTask.INVITE_FRIENDS).start();
  }

  @OnClick(R.id.find_user_facebook) void onClickFacebook() {
    if (WatchaApp.getUser() == null) {
      return;
    }

    if (!WatchaApp.getUser().isFbConnected()) {
      final SettingHelper.OnSettingResponseListener fbTokenListener =
          new SettingHelper.OnSettingResponseListener(getActivity()) {
            @Override
            public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
              super.onSuccess(queryType, result);
              getGeneralCardsStarter("facebook").start();
            }
          };
      MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
      builder.title(R.string.fb_connect)
          .content(R.string.want_connect_fb)
          .callback(new MaterialDialog.ButtonCallback() {
            @Override public void onPositive(MaterialDialog dialog) {
              super.onPositive(dialog);
              FacebookBroker.accessTokenObservable(getActivity())
                  .subscribe(accessToken -> {
                    SettingHelper.connectFb(getActivity(), accessToken.getToken(),
                        fbTokenListener);
                  });
            }
          })
          .positiveText(R.string.yes)
          .negativeText(R.string.no);
      builder.show();
    } else {
      getGeneralCardsStarter("facebook").start();
    }
  }

  @OnClick(R.id.find_user_twitter) void onClickTwitter() {
    boolean hasSession = TwitterBroker.hasSession();
    if (!hasSession) {
      final SettingHelper.OnSettingResponseListener twitterTokenListener =
          new SettingHelper.OnSettingResponseListener(getActivity()) {
            @Override public void onSuccess(@NonNull QueryType queryType,
                @NonNull BaseResponse<User> result) {
              super.onSuccess(queryType, result);
              getGeneralCardsStarter("twitter").start();
            }
          };
      TwitterBroker.connectTwitter(getBaseActivity(), twitterTokenListener);
    } else {
      getGeneralCardsStarter("twitter").start();
    }
  }

  private ActivityStarter getGeneralCardsStarter(@NonNull String apiName) {
    return ActivityStarter.with(getActivity(), FragmentTask.GENERAL_CARDS)
        .addBundle(new BundleSet.Builder().putSchemeApi("friends/" + apiName).build().getBundle());
  }
}
