package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

/**
 * 파트너 설정 화면.
 */
public class PartnerCandidatesFragment extends ListFragment<ListData>
    implements EmptyViewPresenter, View.OnClickListener {

  @Override protected QueryType getQueryType() {
    return QueryType.PARTNER_CANDIDATES;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("무비파트너 설정");
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.CANDIDATE_MOVIE_PARTNERS;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    shouldLoadImmediately(true);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public void onClick(View v) {
    switch (v.getId()) {
      case R.id.empty_large_invite:
        ActivityStarter.with(getActivity(), FragmentTask.FIND_USER).start();
        break;
      case R.id.empty_large_referral:
        break;
    }
  }

  @Override public View getEmptyView() {
    // 내 목록.
    View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_empty_large, null);
    ButterKnife.findById(view, R.id.empty_large_invite).setOnClickListener(this);
    ButterKnife.findById(view, R.id.empty_large_referral).setOnClickListener(this);
    return view;
  }
}
