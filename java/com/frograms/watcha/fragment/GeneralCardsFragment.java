package com.frograms.watcha.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * 모두보기 따위 같은 카드만 보이는 화면.
 *
 * api 와 액션바 타이틀/서브타이틀을 받아야함.
 */
public class GeneralCardsFragment extends ListFragment<ListData> implements EmptyViewPresenter {

  private QueryType mQueryType = QueryType.GENERAL_CARDS;

  private String mSchemeApi = null;
  private String mSchemeSubTitle = null;
  private String mSchemeTitle = null;
  private boolean mShouldShowSortUnderbarView = false;

  /**
   * UserActionType == COMMENT 일때만 이걸 씀.
   *
   * @see UserContentFragment#getSort(UserActionType)
   */
  private static final UserContentFragment.Sort[] SORTS_COMMENT = {
      new UserContentFragment.Sort("popular", "좋아요 순"),
      new UserContentFragment.Sort("recent", "최신 순"),
      new UserContentFragment.Sort("best", "별점 높은 순"),
      new UserContentFragment.Sort("worst", "별점 낮은 순"),
  };

  public static String buildSchemeApi(Uri uri) {
    String api = uri.getAuthority();
    for (int Loop1 = 0; Loop1 < uri.getPathSegments().size(); Loop1++) {
      api += "/" + uri.getPathSegments().get(Loop1);
    }
    return api;
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.SCHEME_API)) {
      mSchemeApi = bundle.getString(BundleSet.SCHEME_API);
      WLog.i("schemeApi: " + mSchemeApi);
    }

    if (bundle.containsKey(BundleSet.SCHEME_TITLE)) {
      mSchemeTitle = bundle.getString(BundleSet.SCHEME_TITLE);
    }

    if (bundle.containsKey(BundleSet.SCHEME_SUBTITLE)) {
      mSchemeSubTitle = bundle.getString(BundleSet.SCHEME_SUBTITLE);
    }

    if (bundle.containsKey(BundleSet.SHOW_SORT)) {
      mShouldShowSortUnderbarView =
          bundle.getBoolean(BundleSet.SHOW_SORT, mShouldShowSortUnderbarView);
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.SCHEME_API, mSchemeApi);
    outState.putString(BundleSet.SCHEME_TITLE, mSchemeTitle);
    outState.putString(BundleSet.SCHEME_SUBTITLE, mSchemeSubTitle);
    outState.putBoolean(BundleSet.SHOW_SORT, mShouldShowSortUnderbarView);
  }

  @Override public void load() {
    super.load();
    if (mShouldShowSortUnderbarView) {
      addUnderToolbarView(getSortView());
    }
  }

  @Override public void unload() {
    super.unload();
    if (mShouldShowSortUnderbarView) {
      removeUnderToolbarView(getSortView());
    }
  }

  private View mSortView = null;
  private IconActionGridButton mSortTitleTextView = null;
  private String mSortParam = null;
  private Map<String, String> mParams = new HashMap<>();

  @Override protected Map<String, String> getRequestDataParams() {
    if (!TextUtils.isEmpty(mSortParam)) {
      mParams.put("sort", mSortParam);
    } else if (TextUtils.isEmpty(mSortParam) && mParams.containsKey("sort")) {
      mParams.remove("sort");
    }
    return mParams;
  }

  private View getSortView() {
    if (mSortView == null) {
      final UserContentFragment.Sort[] sorts = SORTS_COMMENT;
      mSortView = LayoutInflater.from(getActivity()).inflate(R.layout.layer_user_content, null);
      mSortTitleTextView = ButterKnife.findById(mSortView, R.id.layout_single_button);
      mSortTitleTextView.setIcon(getString(R.string.icon_arrow_spinner));
      mSortTitleTextView.setText(sorts[0].getDisplayName());
      mSortTitleTextView.setTextNormalColor(getResources().getColor(R.color.gray));
      mSortTitleTextView.setIconColor(getResources().getColor(R.color.skyblue));
      mSortView.setOnClickListener(v -> {
        if (sorts != null) {
          final String[] sortDisplayNames = new String[sorts.length];
          for (int i = 0; i < sorts.length; i++) {
            sortDisplayNames[i] = sorts[i].getDisplayName();
          }
          FakeActionBar header = new FakeActionBar(getActivity());
          header.setTitle("정렬");
          ArrayAdapter<String> simpleAdapter =
              new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1,
                  sortDisplayNames);
          DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
              .setContentHolder(new ListHolder())
              .setAdapter(simpleAdapter)
              .setGravity(DialogPlus.Gravity.BOTTOM)
              .setCancelable(true)
              .setOnItemClickListener((dialogPlus, o, view, i) -> {
                dialogPlus.dismiss();
                mSortTitleTextView.setText(sortDisplayNames[i - 1]);
                mSortParam = sorts[i - 1].getCode();
                AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.SORT_IN_COMMENT)
                    .setLabel(mSortParam)
                    .build());
                refresh();
              })
              .create();
          dialog.show();
        }
      });
    }
    return mSortView;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle(mSchemeTitle);
    getToolbarHelper().getActionbar().setSubtitle(mSchemeSubTitle);
  }

  @Override protected QueryType getQueryType() {
    mQueryType = QueryType.GENERAL_CARDS;
    return mQueryType.setApi(mSchemeApi);
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ListData> result) {
    refreshEmptyListView();
    super.onSuccess(queryType, result);
    if (result.getData() != null) {
      String title = result.getData().getTitle();
      String subtitle = result.getData().getSubtitle();
      if (!TextUtils.isEmpty(title)) {
        getToolbarHelper().getActionbar().setTitle(title);
      }
      if (!TextUtils.isEmpty(subtitle)) {
        getToolbarHelper().getActionbar().setSubtitle(subtitle);
      }
    }
  }

  @Override public View getEmptyView() {
    if (mQueryType != null) {
      if (mQueryType.getApi().contains("friends/facebook")) {
        return new SimpleEmptyView(getActivity(), R.string.icon_friends, "모든 페이스북 친구를 팔로우 하셨어요");
      } else if (mQueryType.getApi().contains("friends/twitter")) {
        return new SimpleEmptyView(getActivity(), R.string.icon_friends, "모든 트위터 친구를 팔로우 하셨어요");
      }
    }
    return null;
  }

  /**
   * StringUtils 가 어떤 메소드를 쓰는지 잘 봐야함.
   * else if 로 나눠 놓은 메소드 종류는 endsWith, equals, contains 뿐임.
   *
   * @see StringUtils#equals(CharSequence, CharSequence)
   * @see StringUtils#endsWith(CharSequence, CharSequence)
   * @see StringUtils#contains(CharSequence, CharSequence)
   * */
  @Nullable
  @AnalyticsManager.ScreenNameType
  public String getCurrentScreenName() {
    if (TextUtils.isEmpty(mSchemeApi)) {
      return null;
    }
    if (StringUtils.endsWith(mSchemeApi, "ratings/friends")) {
      // 친구들의 평가
      return AnalyticsManager.EVERY_COMMENTS_FILTERED_BY_FRIEND;
    } else if (StringUtils.endsWith(mSchemeApi, "comments")) {
      return AnalyticsManager.EVERY_COMMENTS_FILTERED_BY_CONTENT;
    } else if (StringUtils.endsWith(mSchemeApi, "crew")) {
      return AnalyticsManager.EVERY_STAFFS_FILTERED_BY_CONTENT;
    } else if (StringUtils.endsWith(mSchemeApi, "series")) {
      return AnalyticsManager.EVERY_SERIES_OF_CONTENTS_FILTERED_BY_SINGLE_CONTENT;
    } else if (StringUtils.endsWith(mSchemeApi, "likers")) {
      return AnalyticsManager.LIKERS;
    } else if (StringUtils.endsWith(mSchemeApi, "similar")) {
      return AnalyticsManager.EVERY_CONTENTS_FILTERED_BY_SIMILARITIES;
    } else if (StringUtils.contains(mSchemeApi, "tagged")) {
      return AnalyticsManager.EVERY_CONTENTS_FILTERED_BY_TAGS;
    } else if (StringUtils.contains(mSchemeApi, "staffmades/awards")) {
      return AnalyticsManager.AWARDED_MOVIES;
    } else if (StringUtils.equals(mSchemeApi, "decks/popular")) {
      return AnalyticsManager.POPULAR_DECKS;
    } else if (StringUtils.equals(mSchemeApi, "quizzes")) {
      return AnalyticsManager.QUIZ;
    } else if (StringUtils.equals(mSchemeApi, "magazines")) {
      return AnalyticsManager.MAGAZINE;
    } else if (StringUtils.equals(mSchemeApi, "friends/facebook")) {
      return AnalyticsManager.FELLOWS_FROM_FACEBOOK;
    } else if (StringUtils.equals(mSchemeApi, "friends/twitter")) {
      return AnalyticsManager.FELLOWS_FROM_TWITTER;
    } else if (StringUtils.contains(mSchemeApi, "articles")) {
      return AnalyticsManager.ARTICLES;
    }
    return null;
  }
}
