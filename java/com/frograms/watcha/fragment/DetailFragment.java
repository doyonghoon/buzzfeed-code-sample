package com.frograms.watcha.fragment;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.GridDrawerAdapter;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.model.Media;
import com.frograms.watcha.model.callbacks.BaseScrollCallbacks;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.Gallery;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.DetailData;
import com.frograms.watcha.model.response.data.ShareMessageData;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.models.ShareApp;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.ColorUtils;
import com.frograms.watcha.utils.ShareAppLoader;
import com.frograms.watcha.view.ContentToolbarView;
import com.frograms.watcha.view.DetailContentHeaderView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rx.Observable;

/**
 * 개영페
 */
public class DetailFragment extends ListFragment<DetailData> {

  private CategoryType mCategory;
  private String mCode;
  private Content mContent;

  private QueryType mQueryType = null;
  private DetailContentHeaderView mDetailContentHeaderView;
  private View mShareTextViewLayout;
  private ContentToolbarView mContentToolbarView = null;

  private String mPreviousScreenName = null;

  @Override public String getCurrentScreenName() {
    if (mCategory != null) {
      switch (mCategory) {
        case MOVIES:
          return AnalyticsManager.MOVIE_DETAIL;
        case TV_SEASONS:
          return AnalyticsManager.DRAMA_DETAIL;
      }
    }
    return null;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategory = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }

    if (bundle.containsKey(BundleSet.CONTENT_CODE)) {
      mCode = bundle.getString(BundleSet.CONTENT_CODE);
    }

    if (bundle.containsKey(BundleSet.PREVIOUS_SCREEN_NAME)) {
      mPreviousScreenName = bundle.getString(BundleSet.PREVIOUS_SCREEN_NAME);
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.CATEGORY_TYPE, mCategory.getTypeInt());
    outState.putString(BundleSet.CONTENT_CODE, mCode);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected QueryType getQueryType() {
    if (mQueryType == null) {
      mQueryType = QueryType.CONTENT_DETAIL;
      mQueryType.setApi(mCategory.getApiPath(), mCode);
    }
    return mQueryType;
  }

  @Override public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup,
      Bundle paramBundle) {
    // 화면이 그려지기 전에 statusBar 색상을 바꿈.
    setStatusbarColor(ColorUtils.getAccentColor(getResources().getColor(R.color.heavy_purple)));
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getToolbarHelper().getToolbar().addView(getShareTextViewLayout());
    getToolbarHelper().getToolbar()
        .setBackgroundColor(getResources().getColor(R.color.heavy_purple));
  }

  private View getShareTextViewLayout() {
    if (mShareTextViewLayout == null) {
      mShareTextViewLayout = LayoutInflater.from(getActivity())
          .inflate(R.layout.view_menu_item_share, getToolbarHelper().getToolbar(), false);
    }
    return mShareTextViewLayout;
  }

  @Override public View getListHeaderView() {
    return getDetailContentHeaderView();
  }

  @Override protected View getCustomToolbarView() {
    if (mContentToolbarView == null) {
      mContentToolbarView = new ContentToolbarView(getActivity());
      mContentToolbarView.getIcon()
          .setOnClickListener(v -> Observable.create(createShareMesasgeObservable())
              .doOnSubscribe(() -> {
                @AnalyticsEvent.AnalyticsEventLabel final String categoryName =
                    mCategory.getApiPath();
                AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL, AnalyticsActionType.SHARE_CONTENT)
                    .setLabel(categoryName)
                    .build());
              })
              .subscribe(shareDataWrapper -> {
                String message =
                    String.format("%s(%d)\n%s", mContent.getTitle(), mContent.getYear(), shareDataWrapper.data
                        .getUrl());
                if (shareDataWrapper.shareApp.getPackageName()
                    .equals(ShareAppLoader.PACKAGE_TWITTER)) {
                  message =
                      String.format("%s (%d) #왓챠 %s", mContent.getTitle(), mContent.getYear(), shareDataWrapper.data
                          .getUrl());
                }
                shareDataWrapper.loader.goChosenApp(message, shareDataWrapper.shareApp);
              }));
    }
    return mContentToolbarView;
  }

  private Observable.OnSubscribe<ShareDataWrapper> createShareMesasgeObservable() {
    return subscriber -> {
      GridDrawerAdapter adapter = new GridDrawerAdapter(getActivity());
      final ShareAppLoader loader = new ShareAppLoader(getActivity());
      List<ShareApp> apps = loader.loadApps();
      adapter.addAll(apps);
      adapter.notifyDataSetChanged();
      FakeActionBar header = new FakeActionBar(getActivity());
      header.setTitle("공유하기");
      DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
          .setContentHolder(new ListHolder())
          .setAdapter(adapter)
          .setGravity(DialogPlus.Gravity.BOTTOM)
          .setCancelable(true)
          .setOnItemClickListener((dialog1, item, view, position) -> {
            if (item instanceof ShareApp) {
              dialog1.dismiss();
              final ShareApp app = (ShareApp) item;
              ApiResponseListener<BaseResponse<ShareMessageData>> listener =
                  (queryType, result) -> {
                    ShareMessageData data = result.getData();
                    ShareDataWrapper shareDataWrapper = new ShareDataWrapper();
                    shareDataWrapper.shareApp = app;
                    shareDataWrapper.data = data;
                    shareDataWrapper.loader = loader;
                    subscriber.onNext(shareDataWrapper);
                    subscriber.onCompleted();
                  };

              DataProvider<BaseResponse<ShareMessageData>> provider =
                  new DataProvider<>(getActivity(), QueryType.SHARE_MESSAGE.setApi(mCategory.getApiPath(), mCode), createReferrerBuilder());
              provider.withDialogMessage(getResources().getString(R.string.loading));
              provider.responseTo(listener);
              provider.request();
            }
          })
          .create();
      dialog.show();
    };
  }

  public static class ShareDataWrapper {
    public ShareApp shareApp;
    public ShareMessageData data;
    ShareAppLoader loader;
  }

  public DetailContentHeaderView getDetailContentHeaderView() {
    if (mDetailContentHeaderView == null) {
      mDetailContentHeaderView = new DetailContentHeaderView(getActivity());
      mDetailContentHeaderView.setBottomSheet(getBaseActivity().mBottomSheetLayout);
      mDetailContentHeaderView.setPaletteSubscriber(bitmap -> {
        if (bitmap != null) {
          drawPalettedToolbar(bitmap);
        }
      });
      mDetailContentHeaderView.setVisibility(View.GONE);
    }
    return mDetailContentHeaderView;
  }

  public CategoryType getCategoryType() {
    return mCategory;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> map = super.getRequestDataParams();
    if (map == null) {
      map = new HashMap<>();
    }

    List<TagItems.Tag> tags = CacheManager.getCachedTags();
    if (tags != null && !tags.isEmpty()) {
      List<String> tagIds = new ArrayList<>();
      for(TagItems.Tag tag : tags) {
        tagIds.add(tag.getId() + "");
      }
      map.put("tag_ids", TextUtils.join(",", tagIds));
    }

    return map;
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<DetailData> result) {
    getDetailContentHeaderView().setVisibility(View.VISIBLE);
    mContent = result.getData().getContent();
    if (mContent != null && !TextUtils.isEmpty(mContent.getHanmadiText()) && mPage == 1) {
      mListHeader.findViewById(R.id.toolbar_place).setVisibility(View.VISIBLE);

      BaseScrollCallbacks scrollCallbacks = new BaseScrollCallbacks();
      scrollCallbacks.addCallback(getToolbarScrollCallbacks());
      scrollCallbacks.addCallback(new OnLoadMoreScrollCallbacks());
      scrollCallbacks.addCallback(getScrollCallback());
      scrollCallbacks.addCallback(getUnderToolbarScrollCallback());
      mList.setScrollViewCallbacks(scrollCallbacks);
    }
    super.onSuccess(queryType, result);
    if (mPage == 1) {
      // 개영페 상단 컨텐츠 커버 영역 데이터 채움.
      getDetailContentHeaderView().setDetailData(mContent);
      getDetailContentHeaderView().setMedias(getContentMedias(result.getData()));
      if (result.getData() != null && result.getData().getContent() != null) {
        Observable.just(result.getData().getContent().getTitle())
            .subscribe(mContentToolbarView.getTitleUpdateAction());
      }
    }
  }

  @Override public void onResume() {
    super.onResume();
    if (mContent != null && mDetailContentHeaderView != null) {
      getDetailContentHeaderView().setDetailData(mContent);
    }
  }

  private List<Media> getContentMedias(ListData data) {
    if (data != null && data.getCards() != null && !data.getCards().isEmpty()) {
      for (int i = 0; i < data.getCards().size(); i++) {
        List<CardItem> items = data.getCards().get(i).getListCardItem();
        if (items != null && !items.isEmpty()) {
          for (CardItem item : items) {
            Item itemData = item.getItem();
            if (itemData instanceof Gallery) {
              Gallery gallery = (Gallery) itemData;
              return gallery.getMedias();
            }
          }
        }
      }
    }
    return Collections.emptyList();
  }

  private Observable.OnSubscribe<ContentToolbarView.SimpleSwatch> createSimpleSwatchObservable(
      Palette.Swatch swatch) {
    if (swatch != null) {
      final int backgroundColor = swatch.getRgb();
      final int menuTextColor = swatch.getTitleTextColor();
      final int statusBarColor = ColorUtils.getAccentColor(backgroundColor);
      return subscriber -> subscriber.onNext(
          new ContentToolbarView.SimpleSwatch(menuTextColor, backgroundColor, statusBarColor));
    }
    return null;
  }

  private void drawPalettedToolbar(@NonNull Bitmap bitmap) {
    Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
      private FontIconDrawable getBackIcon(int menuTextColor) {
        FontIconDrawable icon = FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back);
        icon.setTextSize(getResources().getDimension(R.dimen.toolbar_navi_back_icon_size));
        icon.setTextColor(menuTextColor);
        return icon;
      }

      @Override public void onGenerated(Palette palette) {
        if (getActivity() != null && getResources() == null) {
          return;
        }
        Palette.Swatch swatch =
            palette.getLightMutedSwatch() != null ? palette.getLightMutedSwatch()
                : palette.getDarkMutedSwatch();

        Observable<ContentToolbarView.SimpleSwatch> swatchConnectableObservable =
            Observable.create(createSimpleSwatchObservable(swatch));
        swatchConnectableObservable.doOnNext(simpleSwatch -> {
          // 뒤로가기 버튼 색
          getToolbarHelper().getToolbar()
              .setNavigationIcon(getBackIcon(simpleSwatch.getTextColor()));
          setStatusbarColor(simpleSwatch.getStatusbarColor());
          ObjectAnimator.ofObject(getToolbarHelper().getToolbar(), "backgroundColor",
              new ArgbEvaluator(), getResources().getColor(R.color.heavy_purple),
              simpleSwatch.getBackgroundColor()).setDuration(400).start();
        }).doOnNext(simpleSwatch -> {
          mDetailContentHeaderView.updateHanmadiColor(simpleSwatch);
          // 한마디가 있을 때는 그림자를 제거함.
          for (int i = 0; i < getHeaderView().getChildCount(); i++) {
            if (getHeaderView().getChildAt(i).getTag() != null && getHeaderView().getChildAt(i)
                .getTag()
                .equals("dropshadow")) {
              getHeaderView().getChildAt(i).setVisibility(View.GONE);
              break;
            }
          }
        }).subscribe(mContentToolbarView.getColorUpdateAction());
      }
    });
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case ActivityStarter.CREATE_DECK_REQUEST:
        // 컬렉션 생성 화면에서 돌아오면, 유저가 생성을 했든 안했든 drawer 는 닫혀 있었기 때문에 다시 드로우어를 띄움.
        mDetailContentHeaderView.refreshDeckList();
        break;
    }
  }
}
