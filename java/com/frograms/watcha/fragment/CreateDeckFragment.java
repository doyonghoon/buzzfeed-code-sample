package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AlertHelper;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.menus.MenuSet;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.DeckData;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FormValidator;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.LoadingView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.views.CircleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 컬렉션을 생성/수정하는 화면.
 */
public class CreateDeckFragment extends BaseFragment<BaseResponse<DeckData>>
    implements FormValidator.FormValidationListener {

  public static final String KEY = "ADDED_CODES";

  @Bind(R.id.loading_view) LoadingView mLoadingView;
  @Bind(R.id.deck_layer) View mDeckLayer;

  @Bind(R.id.title) EditText mTitle;
  @Bind(R.id.body) EditText mBody;
  @Bind(R.id.deck_title_count_indicator) TextView mTitleCountIndicatorText;
  @Bind(R.id.deck_body_count_indicator) TextView mBodyCountIndicatorText;

  private TextView mCompleteView;

  private View mToolbarContentView;
  private View mEditContentLayer;
  private CircleTextView mContentsCountView;
  private IconActionGridButton mAddContentView;

  private DeckBase mDeck;
  private String mDeckCode = null;
  private ArrayList<String> mAddedContentCodes;

  private FormValidator mTitleValidator = new FormValidator(FormValidator.FormStyle.DECK_TITLE);
  private FormValidator mBodyValidator = new FormValidator(FormValidator.FormStyle.DECK_BODY);

  private boolean isTitleLengthEnabled, isBodyLengthEnabled;

  private boolean isShowingAlert = false;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.DECK_CODE)) {
      mDeckCode = bundle.getString(BundleSet.DECK_CODE);
    }

    if (bundle != null && bundle.containsKey(BundleSet.CONTENT_CODE)) {
      getAddedContentCodes().add(bundle.getString(BundleSet.CONTENT_CODE));
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Nullable @Override public String getCurrentScreenName() {
    return !TextUtils.isEmpty(mDeckCode) ? AnalyticsManager.EDIT_DECK
        : AnalyticsManager.CREATE_DECK;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putString(BundleSet.DECK_CODE, mDeckCode);
    outState.putStringArrayList(BundleSet.CONTENT_CODE, getAddedContentCodes());
  }

  private ArrayList<String> getAddedContentCodes() {
    if (mAddedContentCodes == null) {
      mAddedContentCodes = new ArrayList<>();
    }

    return mAddedContentCodes;
  }

  ;

  private ApiResponseListener<BaseResponse<DeckData>> mCreateResponseListener =
      (queryType, result) -> {
        if (result.getData() != null) {
          AlertDialog.Builder builder = null;
          if (getQueryType(mDeckCode).equals(QueryType.DECK_CREATE)) {
            WatchaApp.getUser().addDecksCount(true);
            AlertHelper.OnDismissListener listener = () -> {
              getActivity().setResult(Activity.RESULT_OK);
              getActivity().finish();
            };

            builder = AlertHelper.showAlert(getActivity(),
                AlertHelper.AlertType.SHARE_COLLECTION.setDeck(result.getData().getDeck()),
                listener);
          }

          if (builder == null) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
          } else {
            isShowingAlert = true;
          }
        }
      };

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    final boolean isEdit = !TextUtils.isEmpty(mDeckCode);

    getToolbarHelper().getActionbar().setTitle(isEdit ? R.string.create_deck : R.string.modify_deck);
    mTitleValidator.registerValidator(mTitle, this);
    mBodyValidator.registerValidator(mBody, this);

    addUnderToolbarView(getAddContentView());
    if (isEdit) {
      mLoadingView.setVisibility(View.VISIBLE);
      mLoadingView.onLoading();
      mDeckLayer.setVisibility(View.GONE);

      requestData(getDataProvider());
    } else {
      mLoadingView.setVisibility(View.GONE);
      Util.openKeyboard(mTitle);
    }

    Boolean isVisited = PrefHelper.getBoolean(getActivity(), PrefHelper.PrefType.FIRST_VISIT,
        FragmentTask.CREATE_DECK.toString());

    if (!isVisited) {
      AlertDialog.Builder builder =
          new AlertDialog.Builder(getActivity()).setMessage(R.string.create_deck_first_visit_msg)
              .setPositiveButton(R.string.ok, null);

      builder.show();

      PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.FIRST_VISIT,
          FragmentTask.CREATE_DECK.toString(), true);
    }
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onMenuItemSet(Menu menu) {
    super.onMenuItemSet(menu);

    MenuItem item =
        menu.add(0, MenuSet.TEXT_COMPLETE.getMenuId(), 0, MenuSet.TEXT_COMPLETE.getXmlResId());
    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    View view = LayoutInflater.from(getActivity()).inflate(R.layout.action_menu, null);
    item.setActionView(view);
    mCompleteView = (TextView) view.findViewById(R.id.action_menu);
    mCompleteView.setOnClickListener(new View.OnClickListener() {

      @Override public void onClick(View view) {
        if (TextUtils.isEmpty(mTitle.getText())) {
          Toast.makeText(getActivity(), R.string.make_title, Toast.LENGTH_SHORT).show();
          return;
        }

        if (mTitle.getText().toString().length()
            > FormValidator.FormStyle.DECK_TITLE.getMaxCount()) {
          Toast.makeText(getActivity(),
              "제목은 최대 " + FormValidator.FormStyle.DECK_TITLE.getMaxCount() + "자까지만 가능해요.",
              Toast.LENGTH_SHORT).show();
          return;
        }

        if (mBody != null && mBody.getText().toString().length() > FormValidator.FormStyle.DECK_BODY
            .getMaxCount()) {
          Toast.makeText(getActivity(),
              "설명은 최대 " + FormValidator.FormStyle.DECK_BODY.getMaxCount() + "자까지만 가능해요.",
              Toast.LENGTH_SHORT).show();
          return;
        }

        if (getAddedContentCodes().isEmpty()) {
          Toast.makeText(getActivity(), R.string.add_contents, Toast.LENGTH_SHORT).show();
          return;
        }

        if (mTitle != null) {
          requestCreate(mTitle.getText().toString(), mBody.getText().toString());
        }
      }
    });
  }

  /**
   * 파라미터 없이 데이터 요청 함.
   */
  @Override protected QueryType getQueryType() {
    return QueryType.DECK_INFO.setApi(mDeckCode);
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_create_deck;
  }

  private View getAddContentView() {
    if (mToolbarContentView == null) {
      mToolbarContentView =
          LayoutInflater.from(getActivity()).inflate(R.layout.undertoolbar_create_deck, null);

      mAddContentView = (IconActionGridButton) mToolbarContentView.findViewById(R.id.add_contents);
      mAddContentView.setIconColor(getResources().getColor(R.color.light_gray));

      mEditContentLayer = mToolbarContentView.findViewById(R.id.edit_contents_layer);
      mEditContentLayer.setVisibility(TextUtils.isEmpty(mDeckCode) ? View.GONE : View.VISIBLE);

      mContentsCountView = (CircleTextView) mToolbarContentView.findViewById(R.id.contents_count);

      mAddContentView.setOnClickListener(new View.OnClickListener() {

        @Override public void onClick(View v) {
          BundleSet.Builder bundleBuilder = new BundleSet.Builder();

          WLog.e(getAddedContentCodes().size() + "");
          if (!getAddedContentCodes().isEmpty()) {
            bundleBuilder.putContentCodes(getAddedContentCodes());
          }

          if (mDeckCode != null) {
            bundleBuilder.putDeckCode(mDeckCode);
          }

          ActivityStarter.with(getActivity(), FragmentTask.ADD_CONTENT_DECK)
              .addBundle(bundleBuilder.build().getBundle())
              .start();
        }
      });

      mEditContentLayer.setOnClickListener(new View.OnClickListener() {

        @Override public void onClick(View v) {
          BundleSet.Builder bundleBuilder = new BundleSet.Builder();
          if (!getAddedContentCodes().isEmpty()) {
            bundleBuilder.putContentCodes(getAddedContentCodes());
          }

          if (mDeckCode != null) {
            bundleBuilder.putDeckCode(mDeckCode);
          }

          String title = (mTitle.getText() == null) ? null : mTitle.getText().toString();

          ActivityStarter.with(getActivity(), FragmentTask.SORT_CONTENT_DECK)
              .addBundle(bundleBuilder.putDeckTitle(title).build().getBundle())
              .start();
        }
      });

      if (getAddedContentCodes().size() == 0) {
        mEditContentLayer.setVisibility(View.GONE);
        mContentsCountView.setVisibility(View.GONE);
      } else {
        mEditContentLayer.setVisibility(View.VISIBLE);
        mContentsCountView.setVisibility(View.VISIBLE);
        mContentsCountView.setTitleText(getAddedContentCodes().size() + "");
      }
    }

    return mToolbarContentView;
  }

  ;

  @Override public void onValidation(FormValidator.FormStyle style, EditText v, String text,
      boolean isValid) {
    switch (style) {
      case DECK_TITLE:
        //isValidTitle = isValid;
        int titleCount = style.getMaxCount() - text.length();
        mTitleCountIndicatorText.setTextColor(
            getResources().getColor(titleCount > 0 ? R.color.light_gray : R.color.light_pink));
        mTitleCountIndicatorText.setText(titleCount + " 자 입력 가능");
        isTitleLengthEnabled = (titleCount > 0);
        break;
      case DECK_BODY:
        //isValidBody = isValid;
        int bodyTextCount = style.getMaxCount() - text.length();
        mBodyCountIndicatorText.setTextColor(
            getResources().getColor(bodyTextCount > 0 ? R.color.light_gray : R.color.light_pink));
        mBodyCountIndicatorText.setText(bodyTextCount + " 자 입력 가능");
        isBodyLengthEnabled = (bodyTextCount > 0);
        break;
    }

    isEnabled();
  }

  private void requestCreate(String title, @Nullable String body) {
    Map<String, String> params = new HashMap<>();
    params.put("name", title);
    if (!TextUtils.isEmpty(body)) {
      params.put("description", body);
    } else {
      params.put("description", "");
    }

    if (!TextUtils.isEmpty(mDeckCode)) {
      params.put("id", mDeckCode);
    }

    params.put("content_codes", TextUtils.join(",", getAddedContentCodes()));

    final QueryType q = getQueryType(mDeckCode);
    DataProvider<BaseResponse<DeckData>> provider =
        new DataProvider<>(getActivity(), q, createReferrerBuilder());
    provider.withParams(params);
    provider.responseTo(mCreateResponseListener);
    provider.withDialogMessage(!TextUtils.isEmpty(mDeckCode) ? "컬렉션 수정 중.." : "컬렉션 생성 중..");
    provider.request();
  }

  private QueryType getQueryType(String deckCode) {
    return !TextUtils.isEmpty(deckCode) ? QueryType.DECK_UPDATE.setApi(deckCode)
        : QueryType.DECK_CREATE;
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull final BaseResponse<DeckData> result) {
    super.onSuccess(queryType, result);

    mDeck = result.getData().getDeck();
    if (mDeck.getContentCodes() != null) getAddedContentCodes().addAll(mDeck.getContentCodes());

    mTitle.setText(mDeck.getName());
    mBody.setText(mDeck.getDesc());

    setDeckInfo();

    mLoadingView.setVisibility(View.GONE);
    mDeckLayer.setVisibility(View.VISIBLE);

    Util.openKeyboard(mTitle);
  }

  private void setDeckInfo() {
    if (getAddedContentCodes().size() == 0) {
      mEditContentLayer.setVisibility(View.GONE);
      mContentsCountView.setVisibility(View.GONE);
    } else {
      mEditContentLayer.setVisibility(View.VISIBLE);
      mContentsCountView.setVisibility(View.VISIBLE);
      mContentsCountView.setTitleText(getAddedContentCodes().size() + "");
    }

    isEnabled();
  }

  private void isEnabled() {
    if (!isTitleLengthEnabled) {
      mCompleteView.setTextColor(getResources().getColor(R.color.white_alpha_50));
      return;
    }

    if (!isBodyLengthEnabled) {
      mCompleteView.setTextColor(getResources().getColor(R.color.white_alpha_50));
      return;
    }

    if (getAddedContentCodes().isEmpty()) {
      mCompleteView.setTextColor(getResources().getColor(R.color.white_alpha_50));
      return;
    }

    mCompleteView.setTextColor(getResources().getColor(R.color.white));
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == Activity.RESULT_OK) {
      if ((requestCode == FragmentTask.ADD_CONTENT_DECK.getValue()
          || requestCode == FragmentTask.SORT_CONTENT_DECK.getValue()) && data != null) {
        if (data.hasExtra(KEY)) {
          mAddedContentCodes = data.getStringArrayListExtra(KEY);
          setDeckInfo();
        }
      }
    }
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return () -> {
      Util.closeKeyboard(mTitle);
      return true;
    };
  }

  @Override public void onResume() {
    super.onResume();

    if (isShowingAlert) {
      getActivity().setResult(Activity.RESULT_OK);
      getActivity().finish();
    }
  }
}

