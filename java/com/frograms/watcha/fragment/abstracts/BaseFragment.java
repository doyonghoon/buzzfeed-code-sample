package com.frograms.watcha.fragment.abstracts;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.HomeFragment;
import com.frograms.watcha.fragment.RatingTabFragment;
import com.frograms.watcha.fragment.TutorialFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontIconHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.callbacks.OnTransparentCallback;
import com.frograms.watcha.model.callbacks.OnUpDownScrollCallback;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.menus.MenuSet;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.observablescrollview.ObservableScrollViewCallbacks;
import com.frograms.watcha.observablescrollview.ScrollState;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.retrofit.RetrofitErrorHandler;
import com.frograms.watcha.utils.ColorUtils;
import com.frograms.watcha.utils.ImageChooser;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.kbeanie.imagechooser.api.ChooserType;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseFragment<T extends BaseResponse> extends Fragment
    implements ApiResponseListener<T> {

  private static final String LOAD_IMMEDIATELY = "LoadImmediately";

  protected boolean isTab = false;
  protected boolean mShouldLoadImmediately = true;
  protected boolean isPageLoaded = false;

  protected WatchaApp mApp;

  protected Intent mResultData;

  //protected OnSimpleFacebookSessionListener mFbLoginListener;
  //protected SimpleFacebook mSimpleFacebook;

  private HashMap<String, String> mapParams;

  protected DataProvider<T> mDataProvider;

  protected ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
  protected ImageChooser mImageChooser = null;

  protected String mPreviousScreenName = null;

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(LOAD_IMMEDIATELY, mShouldLoadImmediately + "");
    outState.putString(BundleSet.PREVIOUS_SCREEN_NAME, mPreviousScreenName);

    if(WatchaApp.getUser() != null){
      outState.putParcelable("USER", WatchaApp.getUser());
    }
  }

  public void setPreviousScreenName(String screenName) {
    mPreviousScreenName = screenName;
  }

  protected void downScroll() {
    getHeaderView().animate().cancel();
    getHeaderView().animate().translationY(0).setDuration(170).start();
  }

  protected boolean shouldRetriveDataImmediately() {
    return true;
  }

  protected void isTab(boolean isTab) {
    this.isTab = isTab;
  }

  protected void shouldLoadImmediately(boolean shouldLoadImmediately) {
    this.mShouldLoadImmediately = shouldLoadImmediately;
  }

  public void load() {
    mShouldLoadImmediately = true;
    if (!isPageLoaded()) {
      isPageLoaded = true;
      requestData(getDataProvider());
    }
  }

  public void unload() {
    mShouldLoadImmediately = false;
    switch (getToolbarActionType()) {
      case SCROLL:
        downScroll();
        break;
    }
  }

  @Override public void onSuccess(@NonNull QueryType queryType, @NonNull final T result) {
    WatchaApp.getSessionManager().storeWatchaSessionInLocalStorage(getActivity());
  }

  public boolean isPageLoaded() {
    return isPageLoaded;
  }

  protected final class OnUnderToolbarScrollCallback implements ObservableScrollViewCallbacks {
    @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    }

    @Override public void onDownMotionEvent() {
    }

    @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {
      if (getBaseActivity().getUnderToolbar() == null) return;

      float headerTranslationY = getBaseActivity().getUnderToolbar().getTranslationY();

      int toolbarHeight = getBaseActivity().getUnderToolbar().getHeight();
      if (scrollState == ScrollState.UP) {
        if (headerTranslationY != toolbarHeight) {
          getBaseActivity().getUnderToolbar().animate().cancel();
          getBaseActivity().getUnderToolbar()
              .animate()
              .translationY(toolbarHeight)
              .setDuration(170)
              .start();
        }
      } else if (scrollState == ScrollState.DOWN) {
        if (headerTranslationY != 0) {
          getBaseActivity().getUnderToolbar().animate().cancel();
          getBaseActivity().getUnderToolbar().animate().translationY(0).setDuration(170).start();
        }
      }
    }
  }

  /**
   * 파라미터 없이 데이터 요청 함.
   */
  protected abstract QueryType getQueryType();

  protected DataProvider<T> getDataProvider() {
    return getDataProvider(null);
  }

  public ReferrerBuilder createReferrerBuilder() {
    if (!TextUtils.isEmpty(mPreviousScreenName)) {
      return new ReferrerBuilder(mPreviousScreenName);
    }
    return null;
  }

  /**
   * 통계 때 볼 현재 화면의 이름을 여기서 정의함.
   * 오버라이드 하고 리턴해주면 통계 보낼 때 잘 쓰이게 댐.
   *
   * @see AnalyticsManager#sendScreenName(String)
   * @see ReferrerBuilder
   * */
  @Nullable
  @AnalyticsManager.ScreenNameType
  public String getCurrentScreenName() {
    return null;
  }

  protected DataProvider<T> getDataProvider(Map<String, String> params) {
    mDataProvider =
        new DataProvider<T>(getActivity(), getQueryType(), createReferrerBuilder()).setErrorCallback(getErrorCallback())
            .responseTo(this);
    Map<String, String> requestParams = new HashMap<>();
    if (getRequestDataParams() != null) {
      requestParams.putAll(getRequestDataParams());
    }
    if (params != null) {
      requestParams.putAll(params);
    }
    mDataProvider.withParams(requestParams);
    return mDataProvider;
  }

  protected Map<String, String> getRequestDataParams() {
    return null;
  }

  protected RetrofitErrorHandler.OnErrorCallback getErrorCallback() {
    return null;
  }

  protected T requestData(DataProvider<T> builder) {
    if (builder == null) {
      return null;
    }
    return builder.request();
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    if (mTransparentCallback != null) {
      mTransparentCallback.clear();
    }
    ButterKnife.unbind(this);
  }

  @Override public void onDestroy() {
    super.onDestroy();

    mApp = null;
    mResultData = null;
    mTransparentCallback = null;
  }

  @LayoutRes protected abstract int getLayoutId();

  public void setBundle(Bundle bundle) {
    if (WatchaApp.getUser() == null) {
      WatchaApp.getInstance().setUser(bundle.getParcelable("USER"));
    }
    if (bundle != null && bundle.containsKey(BundleSet.PREVIOUS_SCREEN_NAME)) {
      mPreviousScreenName = bundle.getString(BundleSet.PREVIOUS_SCREEN_NAME);
    }
  }

  public void setIntent(Intent intent) {
    mResultData = intent;
  }

  private void throwLayoutIdError() {
    if (getLayoutId() == 0x00) {
      throw new NullPointerException(
          "layoutId may not be null from " + ((Object) this).getClass().getSimpleName());
    }
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    if (savedInstanceState != null) {
      setBundle(savedInstanceState);
      if (mShouldLoadImmediately) {
        mShouldLoadImmediately = Boolean.valueOf(savedInstanceState.getString(LOAD_IMMEDIATELY));
      }
    }

    setDefaultToolbar();

    getToolbarHelper().setToolbarActionType(getToolbarActionType());
    if (hasDefaultDropshadow()) {
      addDropshadow();
    }

    if (!isTab) {
      AnalyticsManager.sendScreenName(getCurrentScreenName());
    }
  }

  /**
   * 그림자는 언제나 마지막에 헤더로 추가함.
   * 화면마다 언제 헤더뷰를 붙일 수 있는지 모르기 때문에 직접 각 화면들이 이 메소드를 호출해줘야 함.
   *
   * @see AbsPagerFragment#onViewCreated(View, Bundle)
   * @see ListFragment#onViewCreated(View, Bundle)
   */
  protected void addDropshadow() {
    if (getHeaderView() != null) {
      boolean canAdd = true;
      for (int i = 0; i < getHeaderView().getChildCount(); i++) {
        if (getHeaderView().getChildAt(i).getTag() != null && getHeaderView().getChildAt(i)
            .getTag()
            .equals(KEY_DROPSHADOW)) {
          canAdd = false;
          break;
        }
      }
      if (canAdd) {
        addToolbarHeaderView(createDropShadowView());
      }
    }
  }

  protected boolean hasDefaultDropshadow() {
    return true;
  }

  public ImageChooser getImageChooser(@QueryFile.QueryFileKey int fileType) {
    return mImageChooser = new ImageChooser((BaseActivity) getActivity(), fileType);
  }

  private OnTransparentCallback mTransparentCallback = null;

  protected ObservableScrollViewCallbacks getToolbarScrollCallbacks() {
    // SET TOOLBAR ANIMATION TYPE
    switch (getToolbarHelper().getToolbarActionType()) {
      case SCROLL:
        return new OnUpDownScrollCallback(getBaseActivity());

      case NONE:
        return null;

      case TRANSPARENT:
        if (mTransparentCallback == null) {
          mTransparentCallback = new OnTransparentCallback(getBaseActivity());
        }
        return mTransparentCallback;
      default:
        return null;
    }
  }

  protected ObservableScrollViewCallbacks getUnderToolbarScrollCallback() {
    return new OnUnderToolbarScrollCallback();
  }

  @Override public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup,
      Bundle paramBundle) {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);

    mApp = (WatchaApp) getActivity().getApplication();

    View view = null;
    throwLayoutIdError();
    if (getLayoutId() > 0) {
      view = paramLayoutInflater.inflate(getLayoutId(), paramViewGroup, false);
    }

    ButterKnife.bind(this, view);

    return view;
  }

  @Override public void onResume() {
    super.onResume();
    if (getActivity() != null) {
      InputMethodManager inputManager =
          (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      if (inputManager != null && getActivity().getCurrentFocus() != null) {
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
            InputMethodManager.HIDE_NOT_ALWAYS);
      }
    }
  }

  @Override public void onPause() {
    super.onPause();
  }

  /**
   * 뒤로가기 버튼 눌릴때 콜백받고 싶으면 이걸 오버라이드하면 됨.
   *
   * @return 뒤로가기 버튼 눌릴때 콜백받을 리스너.
   */
  public OnBackKeyListener getOnBackKeyListener() {
    return null;
  }

  public boolean alwaysFinishOnBackPressed() {
    return true;
  }

  public void onBackPressed(boolean value) {
  }

  public void refresh() {
    WLog.i("Called Refresh");
  }

  public BaseActivity getBaseActivity() {
    return (BaseActivity) getActivity();
  }

  public ToolbarHelper getToolbarHelper() {
    return getBaseActivity().getToolbarHelper();
  }

  public LinearLayout getHeaderView() {
    return getBaseActivity().getHeaderView();
  }

  protected void addToolbarHeaderView(View view) {
    getHeaderView().addView(view);

    if (onGlobalLayoutListener != null) {
      getHeaderView().getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
    }
  }

  protected View mDropshadowView = null;
  private static final String KEY_DROPSHADOW = "dropshadow";

  protected View createDropShadowView() {
    if (mDropshadowView == null) {
      mDropshadowView = LayoutInflater.from(getActivity())
          .inflate(R.layout.view_dropshadow, getHeaderView(), false);
      mDropshadowView.setTag(KEY_DROPSHADOW);
    }
    return mDropshadowView;
  }

  protected void removeToolbarHeaderView(View view) {
    getHeaderView().removeView(view);

    if (onGlobalLayoutListener != null) {
      getHeaderView().getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
    }
  }

  protected View getToolbarHeaderView(int id) {
    for (int Loop1 = 0; Loop1 < getHeaderView().getChildCount(); Loop1++) {
      View view = getHeaderView().getChildAt(Loop1);

      if (view.getId() == id) {
        return view;
      }
    }

    return null;
  }

  // Toolbar 관련 코드
  public void setDefaultToolbar() {
    if (isTab && !(this instanceof RatingTabFragment)) {
      return;
    }

    if (this instanceof HomeFragment
        || this instanceof RatingTabFragment
        || this instanceof TutorialFragment) {
      getToolbarHelper().getActionbar().setDisplayUseLogoEnabled(false);
    } else {
      getToolbarHelper().getActionbar().setDisplayShowTitleEnabled(true);
      getToolbarHelper().getToolbar()
          .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
      getToolbarHelper().getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (getOnBackKeyListener() != null) {
            boolean canFinish = getOnBackKeyListener().onBackKeyDown();
            if (canFinish) {
              getActivity().onBackPressed();
            }
          } else {
            getActivity().onBackPressed();
          }
        }
      });
    }

    getToolbarHelper().setToolbarHeight(getToolbarHeight());
    getToolbarHelper().setBackground(getToolbarColor());
    getToolbarHelper().setTitleColor(getToolbarTitleTextColor());
    getToolbarHelper().getToolbar().setSubtitleTextColor(getResources().getColor(R.color.white));
    if (getCustomToolbarView() != null) {
      getToolbarHelper().getToolbar().addView(getCustomToolbarView());
      ((Toolbar.LayoutParams) getCustomToolbarView().getLayoutParams()).gravity =
          Gravity.CENTER_HORIZONTAL;
    }

    setStatusbarColor(ColorUtils.getAccentColor(getResources().getColor(getToolbarColor())));
    getToolbarHelper().getActionbar().setShowHideAnimationEnabled(true);
  }

  protected abstract ToolbarHelper.ToolbarActionType getToolbarActionType();

  protected int getToolbarColor() {
    return R.color.heavy_purple;
  }

  protected int getToolbarHeight() {
    return getResources().getDimensionPixelSize(R.dimen.toolbar_height);
  }

  protected View getCustomToolbarView() {
    return null;
  }

  protected void setToolbarTitle(String title, String subtitle) {
    if (title != null) {
      getToolbarHelper().setToolbarTitle(title);
    }
    if (subtitle != null) {
      getToolbarHelper().setToolbarSubtitle(subtitle);
    }
  }

  protected int getToolbarTitleTextColor() {
    return R.color.white;
  }

  public void onMenuItemSet(Menu menu) {
    final int groupId = 0;
    MenuItems items = getToolbarMenuItems();
    if (items != null) {
      MenuSet[] sets = items.getMenuSets();
      for (MenuSet set : sets) {
        MenuItem item;
        switch (set.getType()) {
          case ICON:
            menu.add(groupId, set.getMenuId(), 0, getString(set.getTitleResId()))
                .setIcon(getIconDrawable(set))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            break;
          case TEXT:
            item = menu.add(groupId, set.getMenuId(), 0, set.getXmlResId());
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            break;
        }
      }
    }
  }

  private FontIconDrawable getIconDrawable(MenuSet set) {
    return FontIconHelper.setFontIconDrawable(getActivity(), set.getXmlResId())
        .setSize(getResources().getDimension(R.dimen.toolbar_menu_text_size))
        .setColor(set.getColor() == 0 ? getResources().getColor(set.getColorId()) : set.getColor())
        .getDrawable();
  }

  public MenuItems getToolbarMenuItems() {
    return null;
  }

  protected void addUnderToolbarView(View view) {
    getBaseActivity().getUnderToolbar().addView(view);
  }

  protected void removeUnderToolbarView(View view) {
    getBaseActivity().getUnderToolbar().removeView(view);
  }

  public void notifyList() {
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (!(this instanceof AbsPagerFragment) && mImageChooser != null) {
      // 사진 고르고 돌아왔을때 ImageChooser 를 호출함.
      switch (requestCode) {
        case ChooserType.REQUEST_CAPTURE_PICTURE:
        case ChooserType.REQUEST_PICK_PICTURE:
        case ImageChooser.REQUEST_CROP:
          mImageChooser.onImageChosenResult(requestCode, resultCode, data);
          break;
      }
    }
  }

  /**
   * 상태표시줄 배경색을 바꿈.
   *
   * @param color 배경 색상. colorResId 가 아니고, 실제 색상임. {@code Color.WHITE;
   * getResources().getColor(R.color.black);}
   */
  @TargetApi(Build.VERSION_CODES.LOLLIPOP) protected void setStatusbarColor(@ColorInt int color) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = getActivity().getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      window.setStatusBarColor(color);
    }
  }
}