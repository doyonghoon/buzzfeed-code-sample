package com.frograms.watcha.fragment.abstracts;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.AbsPagerAdapter;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.listeners.BottomSheetDeckCardItemListener;
import com.frograms.watcha.listeners.BottomSheetDeckCreateButtonClickListener;
import com.frograms.watcha.listeners.OnCountListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.Tab;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.ImageChooser;
import com.frograms.watcha.utils.ViewUtil;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.drawers.CardBottomSheet;
import com.frograms.watcha.view.slidingtabs.SlidingTabLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Pager안에 Fragment들 채울때 쓰는 껍데기 클래스.
 *
 * {@link #getTabs()}만 반환해주면 됨.
 */
public abstract class AbsPagerFragment extends BaseFragment
    implements OnPageChangeListener, ImageChooser.OnImageChosenListener, OnCountListener {

  private static final String TAG_TAB = "TAB";
  private static final String TAG_FRAGMENT = "Fragment";

  private static final int LAYOUT = R.layout.fragment_abs_pager;

  @Bind(R.id.pager_view_pager) ViewPager mPager;
  protected AbsPagerAdapter mAdapter;
  protected FragmentManager mFragmentManager;
  protected SlidingTabLayout mTab;

  public ArrayList<Tab> mTabs;
  /**
   * 현재 페이지.
   */
  public int mCurrentPageItem = 0;

  @Override public void onDestroy() {
    super.onDestroy();
    mPager = null;
    mAdapter = null;
    mFragmentManager = null;
  }

  @Override public void onCount(int... count) {
  }

  @Override public int getLayoutId() {
    return LAYOUT;
  }

  @Override public void setBundle(Bundle bundle) {
    if (bundle != null && bundle.containsKey(BundleSet.PREVIOUS_SCREEN_NAME)) {
      mPreviousScreenName = bundle.getString(BundleSet.PREVIOUS_SCREEN_NAME);
    }

    if (bundle.containsKey(BundleSet.SELECTED_TAB)) {
      mCurrentPageItem = bundle.getInt(BundleSet.SELECTED_TAB);
    }

    if (bundle.containsKey(TAG_TAB)) {
      mTabs = new ArrayList<>();
      mTabs.addAll(bundle.<Tab>getParcelableArrayList(TAG_TAB));
      for (int Loop1 = 0; Loop1 < mTabs.size(); Loop1++) {
        BaseFragment baseFragment = (BaseFragment) (getActivity().getSupportFragmentManager()
            .getFragment(bundle, TAG_FRAGMENT + Loop1));
        mTabs.get(Loop1).setFragment(baseFragment);
      }
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.SELECTED_TAB, mCurrentPageItem);

    if (mTabs != null && mFragmentManager != null) {
      outState.putParcelableArrayList(TAG_TAB, mTabs);

      for (int Loop1 = 0; Loop1 < mTabs.size(); Loop1++) {
        mFragmentManager.putFragment(outState, TAG_FRAGMENT + Loop1,
            mTabs.get(Loop1).getFragment());
      }
    }
  }

  /**
   * Pager에 보여줄 Fragmnet들.
   */
  protected abstract List<Tab> getTabs();

  public BaseFragment getCurrentFragment() {
    return getTabs().get(mCurrentPageItem).getFragment();
  }

  @Nullable @Override public String getCurrentScreenName() {
    return getCurrentFragment().getCurrentScreenName();
  }

  protected int getCurrentPosition() {
    return mPager.getCurrentItem();
  }

  protected boolean shouldLoadAll() {
    return false;
  }

  protected void initPager() {
    mPager.setOffscreenPageLimit(getTabs().size());
    for (int Loop1 = 0; Loop1 < getTabs().size(); Loop1++) {
      getTabs().get(Loop1).getFragment().isTab(true);
      getTabs().get(Loop1).getFragment().setPreviousScreenName(mPreviousScreenName);
      if (mCurrentPageItem != Loop1) {
        getTabs().get(Loop1).getFragment().shouldLoadImmediately(shouldLoadAll());
      }
    }

    if (mFragmentManager == null) {
      mFragmentManager = getActivity().getSupportFragmentManager();
    }

    if (mAdapter == null) {
      mAdapter = new AbsPagerAdapter(mFragmentManager, getTabs());
    }

    mPager.setAdapter(mAdapter);

    if (mCurrentPageItem < mAdapter.getCount()) {
      mPager.setCurrentItem(mCurrentPageItem);
      // Pager 에 Fragment 가 최초로 붙을 땐 #onPageSelected() 가 안불리니까 여기서 선언해줘야 함.
      if (!TextUtils.isEmpty(getCurrentFragment().getCurrentScreenName())) {
        AnalyticsManager.sendScreenName(getCurrentFragment().getCurrentScreenName());
      }
    }

    mTab.setWeight(isWeight());
    mTab.setViewPager(mPager);

    setListeners();

    for (TextView v : mTab.getTextViews()) {
      if (getTabTextSizeInDp() > 0) {
        v.setTextSize(TypedValue.COMPLEX_UNIT_SP, getTabTextSizeInDp());
      }
    }
  }

  protected float getTabTextSizeInDp() {
    return 0;
  }

  @Override public ReferrerBuilder createReferrerBuilder() {
    return getCurrentFragment().createReferrerBuilder();
  }

  protected void setTabTextColor(int position) {
    TextView[] textViews = mTab.getTextViews();
    for (int i = 0; i < textViews.length; i++) {
      int textColor = getResources().getColor(R.color.purple);
      if (i == position) {
        textColor = getResources().getColor(R.color.white);
      }
      textViews[i].setTextColor(textColor);
    }
  }

  protected TextView getTabTextView(int position) {
    return mTab.getTextViews()[position];
  }

  protected void setListeners() {
    // indicator
  }

  /**
   * TabSliding weight값을 없애서 탭바가 가로로 스크롤 될 수 있게 함.
   */
  protected boolean isWeight() {
    return true;
  }

  protected ViewPager getPager() {
    return mPager;
  }

  protected AbsPagerAdapter getAdapter() {
    return mAdapter;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    if (mTab == null) {
      mTab = new SlidingTabLayout(getActivity());
      mTab.setId(R.id.sliding_tab);
      ViewUtil.setBackground(mTab, getResources().getDrawable(R.color.heavy_purple));
    }

    addToolbarHeaderView(mTab);

    //if (savedInstanceState == null) {
    //  if (mTab == null) {
    //    mTab = new SlidingTabLayout(getActivity());
    //    mTab.setId(R.id.sliding_tab);
    //    ViewUtil.setBackground(mTab, getResources().getDrawable(R.color.white));
    //  }
    //
    //  addToolbarHeaderView(mTab);
    //} else {
    //  mTab = (SlidingTabLayout)getToolbarHeaderView(R.id.sliding_tab);
    //}

    initPager();
    mTab.setOnPageChangeListener(this);
    setTabTextColor(mPager.getCurrentItem());
    addDropshadow();
  }

  private BottomSheetLayout getBottomSheetLayout() {
    return getBaseActivity().mBottomSheetLayout;
  }

  public void showDeckSelectableBottomSheetLayout(@NonNull String contentCode) {
    getBottomSheetLayout().showWithSheetView(
        new CardBottomSheet.Builder(getActivity()).setTitle("컬렉션 선택")
            .setQueryType(QueryType.DECK_MY_LIST)
            .setParams(Collections.singletonMap("content_code", contentCode))
            .setButtonClickListener(
                new BottomSheetDeckCreateButtonClickListener(getBottomSheetLayout(), contentCode))
            .setOnCardItemClickListener(
                new BottomSheetDeckCardItemListener(getBottomSheetLayout(), contentCode))
            .build());
  }

  @Override public void onPageScrollStateChanged(int state) {

  }

  @Override protected boolean hasDefaultDropshadow() {
    return false;
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    switch (getToolbarActionType()) {
      case SCROLL:
        getHeaderView().animate().cancel();
        getHeaderView().animate().translationY(0).setDuration(50).start();

        if (getBaseActivity().getUnderToolbar() != null) {
          getBaseActivity().getUnderToolbar().animate().cancel();
          getBaseActivity().getUnderToolbar().animate().translationY(0).setDuration(50).start();
        }
        break;
    }
  }

  @Override public void onPageSelected(int position) {
    getTabs().get(mCurrentPageItem).getFragment().unload();

    mCurrentPageItem = position;
    setTabTextColor(position);
    final BaseFragment frag = getTabs().get(position).getFragment();
    // 페이지가 선택되면 데이터를 서버에서 불러옴. 만약 이미 데이터가 보여지고 있다면, 아무것도 안함.
    frag.load();
    AnalyticsManager.sendScreenName(mTabs.get(position).getFragment().getCurrentScreenName());
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case ActivityStarter.CREATE_DECK_REQUEST:
        getTabs().get(mCurrentPageItem)
            .getFragment()
            .onActivityResult(requestCode, resultCode, data);
        break;
      default:
        // 사진 고르고 돌아왔을때 ImageChooser 를 호출함.
        getTabs().get(mCurrentPageItem)
            .getFragment()
            .onActivityResult(requestCode, resultCode, data);
        break;
    }
  }

  @Override public void onImageChosenCallback(int fileType, Uri bitmapUri,
      ImageChooser.ImageUriParser parser) {
    WLog.i("bitmapUri: " + bitmapUri.toString());
    if (getTabs().get(mCurrentPageItem)
        .getFragment() instanceof ImageChooser.OnImageChosenListener) {
      ImageChooser.OnImageChosenListener listener =
          (ImageChooser.OnImageChosenListener) getTabs().get(mCurrentPageItem).getFragment();
      listener.onImageChosenCallback(fileType, bitmapUri, parser);
    }
  }
}
