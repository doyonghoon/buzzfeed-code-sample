package com.frograms.watcha.fragment.abstracts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.DetailFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.BottomSheetDeckCardItemListener;
import com.frograms.watcha.listeners.BottomSheetDeckCreateButtonClickListener;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Card;
import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.model.CardTask;
import com.frograms.watcha.model.callbacks.BaseScrollCallbacks;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.model.views.ViewCard;
import com.frograms.watcha.observablescrollview.ObservableRecyclerView;
import com.frograms.watcha.observablescrollview.ObservableScrollViewCallbacks;
import com.frograms.watcha.observablescrollview.ScrollState;
import com.frograms.watcha.recyclerview.DividerItemDecoration;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.drawers.CardBottomSheet;
import com.rey.material.widget.ProgressView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 카드뷰를 보여주는 화면.
 *
 * 카드뷰를 보여줄 Fragment는 모두 오버라이드해서 사용 함.
 * 카드뷰를 보여주는 보여주거나, 로드모어(pagenation) 기능도 포함 되어있음.
 */
public abstract class ListFragment<T extends ListData> extends BaseFragment<BaseResponse<T>>
    implements SwipeRefreshLayout.OnRefreshListener {

  @Override public void onRefresh() {
    refresh();
  }

  protected RecyclerView.LayoutManager mLayoutManager;
  @Bind(R.id.root) FrameLayout mRootView;
  @Bind(R.id.swipe_refresh) protected SwipeRefreshLayout mSwipeLayout;
  @Bind(R.id.list) public ObservableRecyclerView mList;
  @Bind(R.id.empty_layout) FrameLayout mEmptyViewLayout;
  @Bind(R.id.progress_loading) protected ProgressView mProgressView;

  private View mEmptyCardView = null;
  protected LinearLayout mListHeader;
  protected RecyclerAdapter mAdapter;
  protected Map<String, String> mAddLoadMoreParams = new HashMap<>();

  protected boolean hasNext;
  protected int mPage = 1;

  private static RecyclerView.OnItemTouchListener mItemTouchListener =
      new RecyclerView.OnItemTouchListener() {
        @Override public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
          return false;
        }

        @Override public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
      };

  public final class OnLoadMoreScrollCallbacks implements ObservableScrollViewCallbacks {

    @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
      if (hasNext) {
        int[] pos = new int[mList.getSpans()];
        if (getLayoutManager() instanceof StaggeredGridLayoutManager)
          ((StaggeredGridLayoutManager)mLayoutManager).findLastVisibleItemPositions(pos);

        int lastItem = -1;
        for (int Loop1 = 0; Loop1 < mList.getSpans(); Loop1++) {
          if (lastItem < pos[Loop1]) lastItem = pos[Loop1];
        }
        final boolean canLoadNextItems = lastItem + 1 >= mAdapter.getItemCount();
        if (canLoadNextItems) {
          hasNext = false;
          mPage++;

          if (shouldRetriveDataImmediately() && mShouldLoadImmediately) {
            requestData(getDataProvider());
          }
        }
      }
    }

    @Override public void onDownMotionEvent() {
    }

    @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }
  }

  public ViewGroup getRootView() {
    return mRootView;
  }

  protected void downScroll() {
    getHeaderView().animate().cancel();
    getHeaderView().animate().translationY(0).setDuration(170).start();
  }

  protected void setEmptyViewVisibility(int visibility) {
    mEmptyViewLayout.setVisibility(visibility);
    if (mAdapter != null && mAdapter.getFooterView() != null && mEmptyCardView != null) {
      mAdapter.getFooterView()
          .setVisibility(visibility == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
    }
  }

  @Override protected Map<String, String> getRequestDataParams() {
    return mAddLoadMoreParams;
  }

  @Override protected BaseResponse<T> requestData(DataProvider<BaseResponse<T>> builder) {
    setEmptyViewVisibility(View.GONE);
    if (mAdapter != null
        && mAdapter.getFooterView() != null
        && mAdapter.getLoadingViewHolder() != null) {
      if (mAdapter.getData() != null && !mAdapter.getData().isEmpty()) {
        mAdapter.getFooterView().setVisibility(View.VISIBLE);
        mAdapter.getLoadingViewHolder().onLoading();
        mProgressView.setVisibility(View.GONE);
      } else {
        mAdapter.getFooterView().setVisibility(View.GONE);
        mAdapter.getLoadingViewHolder().onEnd();
        mProgressView.setVisibility(View.VISIBLE);
        mSwipeLayout.setRefreshing(false);
      }
    }

    mShouldLoadImmediately = true;
    return super.requestData(builder);
  }

  @Override public void onDestroy() {
    super.onDestroy();

    mAdapter = null;
    mList = null;
    //mItemTouchListener = null;
  }

  public RecyclerAdapter getAdapter() {
    return mAdapter;
  }

  @Override protected int getLayoutId() {
    return R.layout.list;
  }

  @Override protected DataProvider<BaseResponse<T>> getDataProvider() {
    Map<String, String> addLoadMore = new HashMap<>();
    if (getRequestDataParams() != null && !getRequestDataParams().isEmpty()) {
      addLoadMore.putAll(getRequestDataParams());
    }
    addLoadMore.put("page", String.valueOf(mPage));

    return getDataProvider(addLoadMore);
  }

  public void refreshEmptyListView() {
    mEmptyViewLayout.removeAllViews();
    mEmptyCardView =
        (this instanceof EmptyViewPresenter) ? ((EmptyViewPresenter) this).getEmptyView() : null;
    if (mEmptyCardView != null) {
      mEmptyViewLayout.addView(mEmptyCardView);
    }
  }

  @Override public void onViewCreated(View view, final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override public void onGlobalLayout() {
        getToolbarAsHeaderView().getChildAt(0).getLayoutParams().height =
            getHeaderView().getMeasuredHeight() - getResources().getDimensionPixelSize(
                R.dimen.height_dropshadow);
        getHeaderView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
        if (mSwipeLayout != null) {
          final int start = getHeaderView().getMeasuredHeight() / 2;
          final int end = (int) (getHeaderView().getMeasuredHeight() * 1.2);
          mSwipeLayout.setProgressViewOffset(true, start, end);
        }
      }
    };

    initializeRecyclerView();
    refreshEmptyListView();
    mSwipeLayout.setOnRefreshListener(this);
    mSwipeLayout.setColorSchemeResources(R.color.pink);
    mSwipeLayout.setEnabled(enableSwipeRefresh());

    // SET RECYCLER_ADAPTER
    mAdapter = new RecyclerAdapter(getActivity(), getToolbarAsHeaderView());
    mList.setAdapter(mAdapter);

    if (shouldRetriveDataImmediately() && mShouldLoadImmediately) {
      load();
    }

    addToolbarHeaderView(getEmptyHeaderView());
    if (!isTab) {
      addDropshadow();
    }
  }

  @Override protected boolean hasDefaultDropshadow() {
    return false;
  }

  public void showDeckSelectableBottomSheetLayout(String contentCode) {
    getBaseActivity().mBottomSheetLayout.showWithSheetView(
        new CardBottomSheet.Builder(getActivity()).setTitle("컬렉션 선택")
            .setQueryType(QueryType.DECK_MY_LIST)
            .setParams(Collections.singletonMap("content_code", contentCode))
            .setButtonClickListener(
                new BottomSheetDeckCreateButtonClickListener(getBaseActivity().mBottomSheetLayout,
                    contentCode))
            .setOnCardItemClickListener(
                new BottomSheetDeckCardItemListener(getBaseActivity().mBottomSheetLayout,
                    contentCode))
            .build());
  }

  private View getEmptyHeaderView() {
    View v = new View(getActivity());
    LinearLayout.LayoutParams l =
        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0);
    v.setLayoutParams(l);
    return v;
  }

  protected RecyclerView.LayoutManager getLayoutManager() {
    if (mLayoutManager == null)
      mLayoutManager =
          new StaggeredGridLayoutManager(mList.getSpans(), StaggeredGridLayoutManager.VERTICAL);

    return mLayoutManager;
  }

  protected void initializeRecyclerView() {
    // SET RECYCLERVIEW LAYOUTMANAGER
    mList.setLayoutManager(getLayoutManager());
    mList.addItemDecoration(new DividerItemDecoration(mList.getSpans()));

    mList.addOnItemTouchListener(mItemTouchListener);
    BaseScrollCallbacks scrollCallbacks = new BaseScrollCallbacks();
    scrollCallbacks.addCallback(getToolbarScrollCallbacks());
    scrollCallbacks.addCallback(new OnLoadMoreScrollCallbacks());
    scrollCallbacks.addCallback(getScrollCallback());
    scrollCallbacks.addCallback(getUnderToolbarScrollCallback());
    mList.setScrollViewCallbacks(scrollCallbacks);
  }

  public LinearLayout getToolbarAsHeaderView() {
    if (mListHeader == null) {
      mListHeader = (LinearLayout) LayoutInflater.from(getActivity())
          .inflate(R.layout.recycler_header, mList, false);

      if (getToolbarActionType() == ToolbarHelper.ToolbarActionType.TRANSPARENT) {
        mListHeader.findViewById(R.id.toolbar_place).setVisibility(View.GONE);
      }

      View header = getListHeaderView();
      if (header != null && header.getParent() == null) {
        mListHeader.addView(header);
      }

      mListHeader.getChildAt(0).getLayoutParams().height = getHeaderView().getMeasuredHeight();
    }
    return mListHeader;
  }

  protected boolean enableSwipeRefresh() {
    return false;
  }

  /**
   * 스크롤 리스너를 등록함.
   *
   * 스크롤 될때 콜백해줌.
   */
  protected ObservableScrollViewCallbacks getScrollCallback() {
    return null;
  }

  public View getListHeaderView() {
    return null;
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull final BaseResponse<T> result) {
    super.onSuccess(queryType, result);
    if (mProgressView != null) {
      mProgressView.setVisibility(View.GONE);
    }
    if (mAdapter != null && mAdapter.getFooterView() != null) {
      mAdapter.getFooterView().setVisibility(View.GONE);
    }
    if (mAdapter != null && mAdapter.getLoadingViewHolder() != null) {
      mAdapter.getLoadingViewHolder().onEnd();
    }
    if (mSwipeLayout != null) {
      mSwipeLayout.setRefreshing(false);
    }
    ListData data = result.getData();

    if (data != null && mAdapter != null) {
      hasNext = data.hasNext();
      mAdapter.setHasNext(hasNext);

      ArrayList<Card> cards = data.getCards();

      CardTask cardTask;
      if (this instanceof DetailFragment && mPage == 1) {
        cardTask = new CardTask(cards, true);
      } else {
        cardTask = new CardTask(cards);
      }

      mAdapter.addItems(cardTask.getViewCards());

      if (mPage == 1 && cardTask.getViewCards() != null && cardTask.getViewCards().size() == 0
          || mPage == 1 && cardTask.getViewCards() == null) {
        setEmptyViewVisibility(View.VISIBLE);
      }
    }
    notifyList();
  }

  @Override public void notifyList() {
    super.notifyList();
    if (mAdapter != null) {
      mAdapter.notifyDataSetChanged();
    }
  }

  @Override public void onResume() {
    super.onResume();
    notifyList();
  }

  @Override public void refresh() {
    switch (getToolbarActionType()) {
      case SCROLL:
        downScroll();
        break;
    }

    setEmptyViewVisibility(View.GONE);

    hasNext = false;
    mPage = 1;

    mAdapter.clearItems();

    mShouldLoadImmediately = true;
    if (!isPageLoaded()) {
      isPageLoaded = true;
      requestData(getDataProvider());
    } else {
      if (shouldRetriveDataImmediately() && mShouldLoadImmediately) {
        requestData(getDataProvider());
      }
    }
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (this instanceof DetailFragment) {
      return;
    }
    switch (requestCode) {
      case ActivityStarter.CREATE_DECK_REQUEST:
        if (resultCode == Activity.RESULT_OK
            && data != null
            && data.getExtras() != null
            && data.getExtras().containsKey(BundleSet.CONTENT_CODE)) {
          final String contentCode = data.getStringExtra(BundleSet.CONTENT_CODE);
          if (mAdapter != null && mAdapter.getItemCount() > 0) {
            List<ViewCard> cards = mAdapter.getData();
            for (ViewCard c : cards) {
              CardItem i = c.getCardItem();
              if (i.getItem() instanceof Content) {
                if (((Content) i.getItem()).getCode().equals(contentCode)) {
                  ((Content) i.getItem()).setRequiredToShowDialog(true);
                  mAdapter.notifyDataSetChanged();
                  return;
                }
              }
            }
          }
        }
        break;
    }
  }
}
