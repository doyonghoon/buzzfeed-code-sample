package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Tab;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.IncredibleSearchTextLayout;
import java.util.ArrayList;
import java.util.List;
import rx.functions.Action1;

public class SelectContentPagerFragment extends AbsPagerFragment {

  private IncredibleSearchTextLayout mSearchTextLayout = null;
  private String mCachedQuery = null;

  @Override protected List<Tab> getTabs() {
    if (mTabs == null) {
      mTabs = new ArrayList<>();
      SelectContentFragment movieFragment = new SelectContentFragment();
      SelectContentFragment tvFragment = new SelectContentFragment();
      movieFragment.setBundle(
          new BundleSet.Builder().putCategoryType(CategoryType.MOVIES).build().getBundle());
      tvFragment.setBundle(
          new BundleSet.Builder().putCategoryType(CategoryType.TV_SEASONS).build().getBundle());
      mTabs.add(new Tab(getString(R.string.movie), movieFragment));
      mTabs.add(new Tab(getString(R.string.drama), tvFragment));
      mTabs.add(new Tab(getString(R.string.book), new BookEmptyFragment()));
    }

    return mTabs;
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected float getTabTextSizeInDp() {
    return 14.5f;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getToolbarHelper().getToolbar().addView(getSearchTextView());
    //getToolbarHelper().setToolbarTitle(null, null);
  }

  private IncredibleSearchTextLayout getSearchTextView() {
    if (mSearchTextLayout == null) {
      mSearchTextLayout = new IncredibleSearchTextLayout.Builder(getActivity()).setStyle(
          IncredibleSearchTextLayout.Builder.Style.DARK)
          .enableDropdown(true)
          .enableAutocomplete(true)
          .enableQueryCache(true)
          .setHintText("제목, 감독, 배우 검색 (초성)")
          .setOnSearchSubscriber(new Action1<String>() {
            @Override public void call(String query) {
              ((SelectContentFragment) getCurrentFragment()).setQuery(query);
              getSearchTextView().getSearchView().setText(query);
              mSearchTextLayout.getSearchView().clearFocus();
              mSearchTextLayout.getSearchView().dismissDropDown();
              Util.closeKeyboard(mSearchTextLayout.getSearchView());
              getCurrentFragment().refresh();
              refresh();
            }
          })
          .build();
    }
    return mSearchTextLayout;
  }

  @Override public void onPageSelected(int position) {
    super.onPageSelected(position);
    String query = getSearchTextView().getSearchView().getText().toString();
    getSearchTextView().getSearchView().clearFocus();
    getSearchTextView().getSearchView().dismissDropDown();
    Util.closeKeyboard(mSearchTextLayout.getSearchView());
    if (TextUtils.isEmpty(mCachedQuery) && !(getCurrentFragment() instanceof BookEmptyFragment)) {
      mCachedQuery = query;
      ((SelectContentFragment) getCurrentFragment()).setQuery(query);
      ((SelectContentFragment) getCurrentFragment()).setRetry(true);
      return;
    }

    if (!TextUtils.isEmpty(mCachedQuery)
        && !mCachedQuery.equals(query)
        && !(getCurrentFragment() instanceof BookEmptyFragment)) {
      mCachedQuery = query;
      ((SelectContentFragment) getCurrentFragment()).setQuery(query);
      getCurrentFragment().refresh();
    }
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return new OnBackKeyListener() {
      @Override public boolean onBackKeyDown() {
        Util.closeKeyboard(mSearchTextLayout.getSearchView());
        return true;
      }
    };
  }
}
