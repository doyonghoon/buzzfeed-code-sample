package com.frograms.watcha.fragment;

import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.retrofit.QueryType;

/**
 * Created by doyonghoon on 15. 1. 20..
 */
public class FriendsActivitiesFragment extends ListFragment {

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  /**
   * 파라미터 없이 데이터 요청 함.
   */
  @Override protected QueryType getQueryType() {
    return QueryType.NOTIFICATIONS_FRIENDS;
  }
}
