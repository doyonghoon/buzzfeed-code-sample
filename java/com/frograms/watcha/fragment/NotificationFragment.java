package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.SimpleEmptyView;

public class NotificationFragment extends ListFragment<ListData> implements EmptyViewPresenter {

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected boolean enableSwipeRefresh() {
    return true;
  }

  @Override protected QueryType getQueryType() {
    return QueryType.NOTIFICATIONS;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.NOTIFICATION;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public View getEmptyView() {
    return new SimpleEmptyView(getActivity(), R.string.icon_activity, "소식이 없습니다.");
  }
}
