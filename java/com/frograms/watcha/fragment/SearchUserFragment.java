package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.IncredibleSearchTextLayout;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import java.util.Map;

/**
 * 유저 검색화면. 결과까지도 한꺼번에 보여주는 화면.
 */
public class SearchUserFragment extends ListFragment<ListData>
    implements RecyclerAdapter.OnCreateFooterViewListener, EmptyViewPresenter {

  private QueryType mQueryType = QueryType.SEARCH_USER;
  private String mQuery = null;
  private boolean mLoadNextPage = false;
  private IncredibleSearchTextLayout mSearchTextLayout = null;

  @Override protected QueryType getQueryType() {
    return mQueryType;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.SEARCH_USER;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getToolbar()
        .setBackgroundColor(getResources().getColor(R.color.heavy_purple));
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (mAdapter != null) {
      mAdapter.setOnCreateFooterViewListener(this);
    }
    getSearchTextView().getSearchView().requestFocus();
    Util.openKeyboard(getSearchTextView().getSearchView());
  }

  private IncredibleSearchTextLayout getSearchTextView() {
    if (mSearchTextLayout == null) {
      mSearchTextLayout = new IncredibleSearchTextLayout.Builder(getActivity()).setStyle(
          IncredibleSearchTextLayout.Builder.Style.DARK)
          .setHintText("이메일, 닉네임 검색")
          .setOnSearchSubscriber(query -> {
            if (!TextUtils.isEmpty(mQuery = query)) {
              mLoadNextPage = true;
              refresh();
            }
          })
          .build();
    }
    return mSearchTextLayout;
  }

  @Override protected View getCustomToolbarView() {
    return getSearchTextView();
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    mAddLoadMoreParams.put("query", mQuery);
    mAddLoadMoreParams.put("page", mPage + "");
    return mAddLoadMoreParams;
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ListData> result) {
    super.onSuccess(queryType, result);
    if (mPage == 1) {
      mLoadNextPage = true;
    }
  }

  /**
   * 뒤로가기나 툴바 닫기 버튼 눌릴때 키보드 닫고 종료.
   *
   * 뒤로가기 {@link #onBackPressed(boolean)}
   * 툴바 닫기 버튼 {@link Toolbar#setNavigationOnClickListener(View.OnClickListener)}
   *
   * @return 뒤로가기 버튼 눌릴때 콜백받을 리스너.
   */
  @Override public OnBackKeyListener getOnBackKeyListener() {
    return () -> {
      Util.closeKeyboard(getSearchTextView().getSearchView());
      return true;
    };
  }

  @Override protected boolean shouldRetriveDataImmediately() {
    return mLoadNextPage;
  }

  @Override protected DataProvider getDataProvider() {
    return new DataProvider(getActivity(), mQueryType, createReferrerBuilder()).responseTo(this)
        .withParams(getRequestDataParams());
  }

  @Override public void onCreateFooterView(FrameLayout footerView,
      RecyclerAdapter.FooterViewHolder footerViewHolder) {
    footerView.setVisibility(View.GONE);
    mProgressView.setVisibility(View.GONE);
  }

  @Override public View getEmptyView() {
    return new SimpleEmptyView(getActivity(), R.string.icon_search, "검색 결과가 없습니다.");
  }
}
