package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.view.MenuItem;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.Tab;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import java.util.ArrayList;
import java.util.List;

/**
 * 팔로워/팔로잉을 가진 탭 화면.
 */
public class FollowFragment extends AbsPagerFragment {

  private String mUserCode;
  private Bundle mBundle;
  private int mFollowersCount, mFriendsCount;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    mBundle = bundle;
    if (bundle != null && bundle.containsKey(BundleSet.USER_CODE)) {
      mUserCode = bundle.getString(BundleSet.USER_CODE);
    }
    if (bundle != null && bundle.containsKey(BundleSet.FOLLOWERS_COUNT)) {
      mFollowersCount = bundle.getInt(BundleSet.FOLLOWERS_COUNT);
    }
    if (bundle != null && bundle.containsKey(BundleSet.FRIENDS_COUNT)) {
      mFriendsCount = bundle.getInt(BundleSet.FRIENDS_COUNT);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.USER_CODE, mUserCode);
    outState.putInt(BundleSet.FOLLOWERS_COUNT, mFollowersCount);
    outState.putInt(BundleSet.FRIENDS_COUNT, mFriendsCount);
  }

  @Override protected List<Tab> getTabs() {
    if (mTabs == null) {
      FollowersFragment followersFragment = new FollowersFragment();
      FriendsFragment friendsFragment = new FriendsFragment();
      followersFragment.setBundle(mBundle);
      friendsFragment.setBundle(mBundle);
      mTabs = new ArrayList<>();
      mTabs.add(new Tab(getFollowerTabName(), followersFragment));
      mTabs.add(new Tab(getFriendTabName(), friendsFragment));
    }
    return mTabs;
  }

  private String getFollowerTabName() {
    if (mUserCode != null && WatchaApp.getUser() != null && mUserCode.equals(
        WatchaApp.getUser().getCode())) {
      mFollowersCount = WatchaApp.getUser().getFollowersCount();
    }

    return getString(R.string.follower) + ((mFollowersCount > 0) ? " " + mFollowersCount : "");
  }

  private String getFriendTabName() {
    if (mUserCode != null && WatchaApp.getUser() != null && mUserCode.equals(
        WatchaApp.getUser().getCode())) {
      mFriendsCount = WatchaApp.getUser().getFriendsCount();
    }

    return getString(R.string.following) + ((mFriendsCount > 0) ? " " + mFriendsCount : "");
  }

  @Override protected float getTabTextSizeInDp() {
    return 14.5f;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
    getToolbarHelper().getActionbar().setTitle(getString(R.string.follower_friend));
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override public void onPageSelected(int position) {
    super.onPageSelected(position);
    if (mTabs.get(position).getFragment() instanceof ListFragment) {
      ListFragment frag = (ListFragment) mTabs.get(position).getFragment();
      if (frag.getAdapter().getItemCount() > 0) {
        frag.getAdapter().notifyDataSetChanged();
      }
    }
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_search:
        ActivityStarter.with(getActivity(), FragmentTask.SEARCH_USER)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getCurrentScreenName())
                .build()
                .getBundle())
            .start();
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public MenuItems getToolbarMenuItems() {
    return MenuItems.FOLLOW;
  }

  public void updateTabCount() {
    getTabTextView(0).setText(getFollowerTabName());
    getTabTextView(1).setText(getFriendTabName());
  }

  @Override public void onResume() {
    super.onResume();
    updateTabCount();
  }

  @Override public void onCount(int... count) {
    mFollowersCount = count[0];
    mFriendsCount = count[1];

    updateTabCount();
  }
}
