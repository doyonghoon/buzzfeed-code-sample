package com.frograms.watcha.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Tab;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.retrofit.QueryType;
import java.util.ArrayList;
import java.util.List;

/**
 * 유저 보관함의 카테고리 탭 화면.
 *
 * 예를들면 보고싶어요 화면의 영화, tv시리즈, 도서 탭을 가지고 있는 화면임.
 */
public class UserContentTabFragment extends AbsPagerFragment {

  private Bundle mBundle;
  private String mUserCode;
  private UserActionType mUserActionType;
  private CategoryType mCategoryType;
  private boolean mShouldShowSortUnderbarView = false;

  private int mMovieCount = 0;
  private int mDramaCount = 0;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    mBundle = bundle;
    if (mBundle != null) {
      if (mBundle.containsKey(BundleSet.USER_ACTION_TYPE)) {
        mUserActionType =
            UserActionType.getUserActionType(mBundle.getInt(BundleSet.USER_ACTION_TYPE));
      }
      if (mBundle.containsKey(BundleSet.USER_CODE)) {
        mUserCode = mBundle.getString(BundleSet.USER_CODE);
      }
      if (mBundle.containsKey(BundleSet.SHOW_SORT)) {
        mShouldShowSortUnderbarView = mBundle.getBoolean(BundleSet.SHOW_SORT);
      }
      if (mBundle.containsKey(BundleSet.MOVIE_COUNT)
          && mBundle.getInt(BundleSet.MOVIE_COUNT, 0) > 0) {
        mMovieCount = mBundle.getInt(BundleSet.MOVIE_COUNT, 0);
      }
      if (mBundle.containsKey(BundleSet.DRAMA_COUNT)
          && mBundle.getInt(BundleSet.DRAMA_COUNT, 0) > 0) {
        mDramaCount = mBundle.getInt(BundleSet.DRAMA_COUNT, 0);
      }

      if (mBundle.containsKey(BundleSet.CATEGORY_TYPE)) {
        mCurrentPageItem =
            CategoryType.getCategory(mBundle.getInt(BundleSet.CATEGORY_TYPE)).getTypeInt();
      }
    }
  }

  @Override public void onCount(int... count) {

    mMovieCount = count[0];
    mDramaCount = count[1];

    getTabTextView(0).setText(getMovieCount());
    getTabTextView(1).setText(getDramaCount());
  }

  public UserActionType getUserActionType() {
    return mUserActionType;
  }

  public String getUserCode() {
    return mUserCode;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.USER_ACTION_TYPE, mUserActionType.getTypeInt());
    outState.putString(BundleSet.USER_CODE, mUserCode);
    outState.putBoolean(BundleSet.SHOW_SORT, mShouldShowSortUnderbarView);
  }

  @Override protected List<Tab> getTabs() {
    if (mTabs == null) {
      UserContentFragment movieContentFragment = new UserContentFragment();
      movieContentFragment.setBundle(new BundleSet.Builder().putUserActionType(mUserActionType)
          .putUserCode(mUserCode)
          .putCategoryType(CategoryType.MOVIES)
          .putAttachedToTabPager(true)
          .putShowUnderbarView(mShouldShowSortUnderbarView)
          .build()
          .getBundle());

      movieContentFragment.setOnCountListener(this);

      UserContentFragment dramaContentFragment = new UserContentFragment();
      dramaContentFragment.setBundle(new BundleSet.Builder().putUserActionType(mUserActionType)
          .putUserCode(mUserCode)
          .putCategoryType(CategoryType.TV_SEASONS)
          .putAttachedToTabPager(true)
          .putShowUnderbarView(mShouldShowSortUnderbarView)
          .build()
          .getBundle());

      dramaContentFragment.setOnCountListener(this);

      mTabs = new ArrayList<>();
      mTabs.add(new Tab(getMovieCount(), movieContentFragment));
      mTabs.add(new Tab(getDramaCount(), dramaContentFragment));
      mTabs.add(new Tab(R.string.book, new BookEmptyFragment()));
    }
    return mTabs;
  }

  public String getMovieCount() {
    return mMovieCount > 0 ? String.format("%s %d", getResources().getString(R.string.movie),
        mMovieCount) : getString(R.string.movie);
  }

  public String getDramaCount() {
    return mDramaCount > 0 ? String.format("%s %d", getResources().getString(R.string.drama),
        mDramaCount) : getString(R.string.drama);
  }

  @Override protected float getTabTextSizeInDp() {
    return 14.5f;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    //getToolbarHelper().getToolbar().setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    if (mUserActionType != null) {
      if (mUserActionType == UserActionType.COMMENT) {
        getToolbarHelper().getActionbar().setTitle("코멘트");
      } else if (mUserActionType == UserActionType.WISH) {
        getToolbarHelper().getActionbar().setTitle("보고싶어요");
      }
    }
  }

  public static String getTitle(@NonNull Context c, @NonNull UserActionType type) {
    switch (type) {
      case COMMENT:
        return c.getString(R.string.review);
      case MEH:
        return c.getString(R.string.ignore);
      case RATE:
        return c.getString(R.string.already);
      case WISH:
        return c.getString(R.string.wish);
      case DECK:
        return c.getString(R.string.collection);
    }
    return null;
  }

  public void updateTabCount(int movieCount, int dramaCount) {
    if (mTab != null
        && WatchaApp.getUser() != null
        && !TextUtils.isEmpty(mUserCode)
        && mUserCode.equals(WatchaApp.getUser().getCode())) {
      mTab.getTextViews()[0].setText(movieCount > 0 ? getString(R.string.movie) + " " + movieCount
          : getString(R.string.movie));
      mTab.getTextViews()[1].setText(dramaCount > 0 ? getString(R.string.drama) + " " + dramaCount
          : getString(R.string.drama));
    }
  }
}
