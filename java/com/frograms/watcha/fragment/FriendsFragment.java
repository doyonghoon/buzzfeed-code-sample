package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.listeners.OnCountListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.FollowCountListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.SimpleEmptyView;

/**
 * 팔로워 목록 화면.
 */
public class FriendsFragment extends ListFragment<FollowCountListData>
    implements View.OnClickListener, EmptyViewPresenter {

  private String mUserCode;

  private OnCountListener onCountListener;

  public void setOnCountListener(OnCountListener listener) {
    onCountListener = listener;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.USER_CODE)) {
      mUserCode = bundle.getString(BundleSet.USER_CODE);
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    ReferrerBuilder referrerBuilder = new ReferrerBuilder(AnalyticsManager.FOLLOWINGS);
    if (!TextUtils.isEmpty(mUserCode) && WatchaApp.getUser() != null) {
      referrerBuilder.appendArgument(
          TextUtils.equals(mUserCode, WatchaApp.getUser().getCode()) ? "me" : mUserCode);
    }
    @AnalyticsManager.ScreenNameType String screenName = referrerBuilder.toString();
    return screenName;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.USER_CODE, mUserCode);
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void load() {
    super.load();
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override protected QueryType getQueryType() {
    return QueryType.FRIENDS.setApi(mUserCode);
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull BaseResponse<FollowCountListData> result) {
    super.onSuccess(queryType, result);

    if (mPage == 1) {
      FollowCountListData data = result.getData();
      int followersCount = data.getFollowersCount();
      int friendsCount = data.getFriendsCount();

      if (mUserCode.equals(WatchaApp.getUser().getCode())) {
        WatchaApp.getUser().setFollowersCount(followersCount);
        WatchaApp.getUser().setFriendsCount(friendsCount);
      }

      if (onCountListener != null) {
        onCountListener.onCount(followersCount, friendsCount);
      }
    }
  }

  @Override public View getEmptyView() {
    if (mUserCode != null && mUserCode.equals(WatchaApp.getUser().getCode())) {
      // 내 목록.
      View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_empty_large, null);
      ButterKnife.findById(view, R.id.empty_large_invite).setOnClickListener(this);
      ButterKnife.findById(view, R.id.empty_large_referral).setOnClickListener(this);
      return view;
    } else if (mUserCode != null && !mUserCode.equals(WatchaApp.getUser().getCode())) {
      // 다른 유저의 목록.
      return new SimpleEmptyView(getActivity(), R.string.icon_friends, "팔로잉한 사람이 없어요");
    }
    return null;
  }

  @Override public void onClick(View v) {
    switch (v.getId()) {
      case R.id.empty_large_invite:
        ActivityStarter.with(getActivity(), FragmentTask.FIND_USER).start();
        break;
      case R.id.empty_large_referral:
        break;
    }
  }
}
