package com.frograms.watcha.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.handlers.ProgressDrawable;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.RatingPhase;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.toolbarViews.ToolbarRatingView;

/**
 * 회원가입 후에 평가하기.
 */
public class TutorialFragment extends RatingFragment implements View.OnClickListener {

  private CategoryType mCategoryType = CategoryType.MOVIES;
  private final QueryType mQueryType = QueryType.TUTORIAL;
  private String mNextButtonText = null;
  private ToolbarRatingView mToolbarRatingView;
  private int mRatingLimit;
  private ProgressDrawable mProgressDrawable = new ProgressDrawable();
  private String mPreviousScreenName = null;

  /**
   * 튜토리얼 드라마 구간별 평가 문구.
   */
  private RatingPhase[] mDramaRatingPhases = {
      new RatingPhase(0, 10, "자, 이제 드라마 차례에요! :) 10개 이상 평가해주세요."),
      new RatingPhase(2, 10, "좋아요, 드라마도 한번 달려볼까요!"),
      new RatingPhase(4, 10, "잘 찾아보시면 본 드라마 수도 꽤 될 거예요."),
      new RatingPhase(7, 10, "10개 거의 다 왔어요. 조금만 더 화이팅!"),
      new RatingPhase(10, 10, "10개 달성! 더 평가하셔도 좋고, ‘완료’를 누르셔도 좋아요."),
      new RatingPhase(11, 20, "10개 달성! 더 평가하셔도 좋고, ‘완료’를 누르셔도 좋아요."),
      new RatingPhase(14, 20, "그렇죠. 자존심이 허락 안하죠. 더 해야죠."),
      new RatingPhase(17, 20, "영차영차와앗챠, 조금만 더 힘내요. 20개가 코 앞이에요."),
      new RatingPhase(20, 20, "하늘을 보고 외치세요! 내가 드라마 20개 본 사람이다!"),
      new RatingPhase(21, 30, "하늘을 보고 외치세요! 내가 드라마 20개 본 사람이다!"),
      new RatingPhase(25, 30, "세상에! 곧 드라마 30개 돌파!"),
      new RatingPhase(30, 30, "이거, 드라마 매니아의 살기가 느껴지는데요?"),
      new RatingPhase(31, 40, "이거, 드라마 매니아의 살기가 느껴지는데요?"),
      new RatingPhase(35, 40, "혹시… 드라마랑 현실이랑 구분은 되시죠?"),
      new RatingPhase(40, 40, "살면서 약 500시간 동안 드라마를 보셨어요!"),
      new RatingPhase(41, 50, "살면서 약 500시간 동안 드라마를 보셨어요!"),
      new RatingPhase(43, 50, "포기하지 마세요. 사십몇개에서 끝내기엔 좀 아쉽잖아요."),
      new RatingPhase(50, 50, "당신 - 드라마 = 0"), new RatingPhase(51, 70, "손 푸시고! 이제 2라운드 시작해볼까요?"),
      new RatingPhase(70, 70, "엄청나네요! 이 기세로 쭉쭉 밀고 나갑시다!"),
      new RatingPhase(71, 100, "엄청나네요! 이 기세로 쭉쭉 밀고 나갑시다!"),
      new RatingPhase(80, 100, "이쯤되면 유명한 작품은 뭐 거의 다 보셨죠."),
      new RatingPhase(90, 100, "에이, 여기까지 왔으면 100개를 안 찍을 수가 없죠."),
      new RatingPhase(100, 100, "짝짝짝! 당신을 드라마계의 상위 1% 고수로 인정합니다!"),
      new RatingPhase(101, 120, "짝짝짝! 당신을 드라마계의 상위 1% 고수로 인정합니다!"),
      new RatingPhase(120, 120, "이럴수가! 아직 보신 드라마가 남아있군요..."),
      new RatingPhase(121, 140, "이럴수가! 아직 보신 드라마가 남아있군요..."),
      new RatingPhase(140, 140, "당신의 드라마를 향한 열정에 물개박수를 보냅니다!"),
      new RatingPhase(141, 160, "당신의 드라마를 향한 열정에 물개박수를 보냅니다!"),
      new RatingPhase(160, 160, "좋아요! 이대로 쭉~ 드라마의 전설을 향하여!"),
      new RatingPhase(161, 180, "좋아요! 이대로 쭉~ 드라마의 전설을 향하여!"),
      new RatingPhase(180, 180, "멋져요, 그대로 200까지 달려볼까요?"),
      new RatingPhase(181, 200, "멋져요, 그대로 200까지 달려볼까요?"),
      new RatingPhase(200, 200, "드디어 200개 돌파! 상위 0.1%의 포스!"),
      new RatingPhase(201, 230, "드디어 200개 돌파! 상위 0.1%의 포스!"),
      new RatingPhase(230, 230, "이쯤되면 손에 꼽을 드라마 폐ㅇ.. 전문가죠!"),
      new RatingPhase(231, 260, "이쯤되면 손에 꼽을 드라마 폐ㅇ.. 전문가죠!"),
      new RatingPhase(260, 260, "킁킁! 이제 서서히 전설의 냄새가 납니다."),
      new RatingPhase(261, 290, "킁킁! 이제 서서히 전설의 냄새가 납니다."),
      new RatingPhase(290, 290, "조금만 힘을 내요! 300의 고지가 보입니다!"),
      new RatingPhase(291, 300, "조금만 힘을 내요! 300의 고지가 보입니다!"),
      new RatingPhase(300, 300, "당신을 0.01% 드라마계의 전설적인 존재로 인정합니다!"),
      new RatingPhase(9999, 300, "당신을 0.01% 드라마계의 전설적인 존재로 인정합니다!"),
  };

  /**
   * 튜토리얼 영화 구간별 평가 문구.
   */
  private RatingPhase[] mMovieRatingPhases = {
      new RatingPhase(0, 15, "최소 15개 이상의 영화를 평가해주세요 :)"),
      new RatingPhase(2, 15, "조금씩 당신의 취향을 알아가는 중입니다."),
      new RatingPhase(4, 15, "평가만 하는 것도 나름 재미있지 않으세요?"),
      new RatingPhase(7, 15, "좋아요. 이제 조금씩 취향의 윤곽이 드러납니다."),
      new RatingPhase(10, 15, "어떤 영화를 좋아하실지 조금씩 감이 와요."),
      new RatingPhase(13, 15, "이제 알듯 말듯 하네요. 조금만 더 알려주세요!"),
      new RatingPhase(15, 15, "15개 달성! 평가를 더 하셔도 좋고, 넘어가셔도 좋아요."),
      new RatingPhase(16, 30, "더 하시기로 마음 먹었군요... 좋아요!"),
      new RatingPhase(20, 30, "영화를 많이 보는 사람은 깊이가 다르죠."),
      new RatingPhase(25, 30, "여기까지 왔으면 30개는 찍어야죠."), new RatingPhase(30, 30, "30개! 힘을 내어 더 가보아요!"),
      new RatingPhase(31, 50, "와우! 취향 독특하신데요?"), new RatingPhase(34, 50, "한번 마음 먹으면 하는 분이시네요."),
      new RatingPhase(40, 50, "기왕 이렇게 된 거 50개 찍으세요."),
      new RatingPhase(50, 50, "찍으라고 진짜 찍으시면 어떡해요..."), new RatingPhase(51, 70, "자, 그럼 70개 가볼까요?"),
      new RatingPhase(60, 70, "살면서 영화 본 시간만 5일이 넘네요!"),
      new RatingPhase(70, 70, "70개 달성! :) 여기까지 왔으면 100개는 채워야..."),
      new RatingPhase(71, 100, "이제 웬만한 친구보다 제가 당신을 더 잘 알걸요?"),
      new RatingPhase(85, 100, "제가 무슨 말을 할지 궁금하지 않으세요?"),
      new RatingPhase(100, 100, "100개라니! 끈기와 투지에 박수를 보냅니다."),
      new RatingPhase(101, 120, "100개라니! 끈기와 투지에 박수를 보냅니다."),
      new RatingPhase(120, 120, "살면서 영화 본 시간, 10일 돌파!"),
      new RatingPhase(121, 150, "살면서 영화 본 시간, 10일 돌파!"),
      new RatingPhase(150, 200, "오, 영화 많이 보셨네요. 인정합니다! :)"),
      new RatingPhase(200, 250, "오예, 200개 돌파! 한번 기록을 세워볼까요!"),
      new RatingPhase(251, 300, "와우! 당신은 왓챠 상위 10%의 영화 마니아입니다!"),
      new RatingPhase(301, 350, "아주 장난이 아니십니다!"),
      new RatingPhase(351, 400, "이제부터 자기소개 취미에 ‘영화’라고 적어도 됩니다."),
      new RatingPhase(401, 450, "살면서 영화 본 시간, 1개월 돌파!"),
      new RatingPhase(451, 500, "그래요. 기왕 이렇게 된 거 500개 갑시다!"),
      new RatingPhase(501, 550, "500개 달성! 보통 영화인이 아니셨군요..."),
      new RatingPhase(551, 600, "이 정도면, 웬만한 명작들은 거의 다 보셨겠는걸요?"),
      new RatingPhase(601, 700, "살면서 영화 본 시간, 50일 돌파! 무려 1200시간!"),
      new RatingPhase(701, 800, "이야, 남들이 잘 모르는 영화도 많이 보셨겠어요."),
      new RatingPhase(801, 900, "영화가 혹시 직업은 아니시죠?"),
      new RatingPhase(901, 1000, "900편 달성! 천편 찍고 왓챠의 전설이 되어 보세요."),
      new RatingPhase(1000, 1000, "YOU WIN"), new RatingPhase(9999, 1000, "YOU WIN"),
  };

  /**
   * 튜토리얼 구간별 평가 문구.
   */
  private RatingPhase[] getRatingPhases(CategoryType type) {
    switch (type) {
      case MOVIES:
        return mMovieRatingPhases;
      case TV_SEASONS:
        return mDramaRatingPhases;
      default:
      case BOOKS:
        return null;
    }
  }

  @Override protected QueryType getQueryType() {
    return mQueryType.setApi(mCategoryType.getApiPath());
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          mRatingLimit = 15;
          mNextButtonText = getString(R.string.next);
          break;
        case TV_SEASONS:
          mRatingLimit = 10;
        default:
          mNextButtonText = getString(R.string.completed);
          break;
      }
    }
  }

  @Override public void onAttach(Activity activity) {
    super.onAttach(activity);
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setDisplayHomeAsUpEnabled(false);
    getToolbarHelper().getActionbar().setDisplayUseLogoEnabled(false);
    getToolbarHelper().getActionbar().setDisplayShowHomeEnabled(false);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getToolbarHelper().getToolbar().setBackground(getProgressDrawable(mCategoryType));
    setRatingsCount();
  }

  public String getCurrentScreenName() {
    if (mCategoryType == null) {
      return null;
    }
    switch (mCategoryType) {
      case MOVIES:
        return AnalyticsManager.TUTORIAL_MOVIE;
      case TV_SEASONS:
        return AnalyticsManager.TUTORIAL_DRAMA;
      default:
        return null;
    }
  }

  @Override protected boolean attachRatingView() {
    return false;
  }

  public void setRatingsCount() {
    if (mToolbarRatingView != null
        && mCategoryType != null
        && WatchaApp.getUser() != null
        && WatchaApp.getUser().getActionCount(mCategoryType.getApiPath()) != null) {
      int ratingCount = WatchaApp.getUser().getActionCount(mCategoryType.getApiPath()).getRatings();
      mToolbarRatingView.getRatingCountTextView().setText(ratingCount + "");
      getProgressDrawable(mCategoryType);
      boolean isRatingLimited = ratingCount >= mRatingLimit;
      mToolbarRatingView.getNextView()
          .setTextColor(
              getResources().getColor(isRatingLimited ? R.color.white : R.color.white_alpha_50));
      mToolbarRatingView.getNextView().setText(isRatingLimited ? mNextButtonText : "건너뛰기");
    }
  }

  private ProgressDrawable getProgressDrawable(CategoryType type) {
    if (mProgressDrawable == null) {
      mProgressDrawable = new ProgressDrawable();
    }
    final int count = WatchaApp.getUser().getActionCount(type.getApiPath()).getRatings();
    final RatingPhase[] phases = getRatingPhases(type);
    drawProgressCountText(count, phases);
    mProgressDrawable.setProgress(count);
    return mProgressDrawable;
  }

  private void drawProgressCountText(int ratingCount, RatingPhase[] phases) {
    if (phases != null) {
      for (int i = 0; i < phases.length - 1; i++) {
        if (ratingCount == 0) {
          // 시작일땐 걍 하드코딩..
          mToolbarRatingView.getRatingTextView().setText(phases[0].getMessage());
          int maxProgressValue = phases[0].getMaxProgressValue();
          if (maxProgressValue > 0) {
            mProgressDrawable.setMax(maxProgressValue);
          }
          mToolbarRatingView.getRatingTextView().setText(phases[0].getMessage());
          break;
        }
        RatingPhase phase = phases[i];
        RatingPhase nextPhase = phases[i + 1];
        if (phase.getRatingCount() <= ratingCount && ratingCount < nextPhase.getRatingCount()) {
          if (nextPhase.getMaxProgressValue() > 0) {
            mProgressDrawable.setMax(phase.getMaxProgressValue());
          }
          mToolbarRatingView.getRatingTextView().setText(phase.getMessage());
          break;
        }
      }
    }
  }

  @Override protected View getCustomToolbarView() {
    if (mToolbarRatingView == null) {
      mToolbarRatingView = new ToolbarRatingView(getActivity());
      mToolbarRatingView.getNextView().setVisibility(View.VISIBLE);
      mToolbarRatingView.getNextView().setText("건너뛰기");
      mToolbarRatingView.getNextView().setOnClickListener(this);
      mToolbarRatingView.getBackView().setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          getActivity().finish();
        }
      });
      setRatingsCount();
    }
    return mToolbarRatingView;
  }

  @Override public int getToolbarHeight() {
    return getResources().getDimensionPixelSize(R.dimen.toolbar_rating_height);
  }

  @Override public void onClick(View v) {
    switch (v.getId()) {
      case R.id.rating_next:
        if (mCategoryType != null) {
          switch (mCategoryType) {
            case MOVIES:
              ActivityStarter.with(getActivity(), FragmentTask.TUTORIAL)
                  .addBundle(new BundleSet.Builder().putCategoryType(CategoryType.TV_SEASONS)
                      .build()
                      .getBundle())
                  .start();
              break;
            default:
              ActivityStarter.with(getActivity(), FragmentTask.HOME).start();
              break;
          }
        }
        break;
    }
  }

  public static boolean betweenExclusive(int x, int min, int max) {
    return x > min && x < max;
  }
}
