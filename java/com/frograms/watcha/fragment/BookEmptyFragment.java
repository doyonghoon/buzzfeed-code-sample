package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.View;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.RobotoRegularView;

/**
 * 도서화면.
 */
public class BookEmptyFragment extends BaseFragment {

  @Bind(R.id.empty_simple_icon) FontIconTextView mIcon;
  @Bind(R.id.empty_simple_description) RobotoRegularView mDescription;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override public void setDefaultToolbar() {
    if (!isTab) {
      super.setDefaultToolbar();
      getToolbarHelper().getToolbar()
          .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
      getToolbarHelper().getActionbar().setTitle(getString(R.string.book));
    }
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_empty_book;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return "Book";
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mIcon.setText(getString(R.string.icon_book));
    mIcon.setTextSize(TypedValue.COMPLEX_UNIT_SP, 50);
    mDescription.setText("도서는 아직 준비 중이에요! :)");
  }
}
