package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Card;
import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.SearchResultData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.IncredibleSearchTextLayout;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 컬렉션에 컨텐츠를 추가하는 화면.
 */
public class AddContentDeckFragment extends ListFragment<SearchResultData> implements
    RecyclerAdapter.OnClickCardItemListener, EmptyViewPresenter {

  private IncredibleSearchTextLayout mSearchTextLayout = null;
  private QueryType mQueryType;
  private String mDeckCode = null;

  private String mQuery = null;
  private ArrayList<String> mAddedContents = new ArrayList<>();

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.DECK_CODE)) {
      mDeckCode = bundle.getString(BundleSet.DECK_CODE);
    }
    if (bundle.containsKey(BundleSet.CONTENT_CODES)) {
      getAddedContentCodes().addAll(bundle.getStringArrayList(BundleSet.CONTENT_CODES));
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Nullable @Override public String getCurrentScreenName() {
    return TextUtils.isEmpty(mDeckCode) ? AnalyticsManager.ADD_MOVIE_FROM_CREATE_DECK
        : AnalyticsManager.EDIT_MOVIE_FROM_CREATE_DECK;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.DECK_CODE, mDeckCode);
    outState.putStringArrayList(BundleSet.CONTENT_CODES, getAddedContentCodes());
  }

  private ArrayList<String> getAddedContentCodes() {
    if (mAddedContents == null) {
      mAddedContents = new ArrayList<>();
    }
    return mAddedContents;
  }

  @Override protected View getCustomToolbarView() {
    return getSearchTextView();
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mAdapter.setOnClickCardItemListener(this);
  }

  @Override protected QueryType getQueryType() {
    mQueryType = QueryType.UPDATE_CONTENTS_IN_DECK;
    if (mDeckCode == null) {
      mQueryType.setApi("new", "new");
    } else {
      mQueryType.setApi(mDeckCode, "new");
    }

    return mQueryType;
  }

  @Override public MenuItems getToolbarMenuItems() {
    return MenuItems.COMPLETE;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_text_complete:
        if (getAddedContentCodes().isEmpty()) {
          Toast.makeText(getActivity(), "추가할 영화가 없습니다.", Toast.LENGTH_SHORT).show();
        } else {
          Intent data = new Intent();
          data.putStringArrayListExtra(CreateDeckFragment.KEY, getAddedContentCodes());

          getActivity().setResult(Activity.RESULT_OK, data);
          getActivity().finish();
        }
        return true;
    }
    return false;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> map = super.getRequestDataParams();

    if (mQuery != null) {
      map.put("query", mQuery);
    }

    if (!getAddedContentCodes().isEmpty()) {
      map.put("content_codes", TextUtils.join(",", getAddedContentCodes()));
    }

    return map;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  private IncredibleSearchTextLayout getSearchTextView() {
    if (mSearchTextLayout == null) {
      mSearchTextLayout = new IncredibleSearchTextLayout.Builder(getActivity()).setStyle(
          IncredibleSearchTextLayout.Builder.Style.DARK)
          .enableDropdown(true)
          .enableAutocomplete(true)
          .enableQueryCache(true)
          .setHintText("제목, 감독, 배우 검색 (초성)")
          .setOnSearchSubscriber(query -> {
            if (!TextUtils.isEmpty(mQuery = query)) {
              mSearchTextLayout.getSearchView().setText(mQuery);
              mSearchTextLayout.dismissDropdown();
              if (!getRootView().hasFocus()) {
                getRootView().requestFocus();
              }
              refresh();
              Util.closeKeyboard(mSearchTextLayout.getSearchView());
            }
          })
          .build();
    }
    return mSearchTextLayout;
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull BaseResponse<SearchResultData> result) {
    List<Card> cards = result.getData().getCards();
    for (Card c : cards) {
      for (CardItem cardItem : c.getListCardItem()) {
        if (cardItem.getItem() instanceof Content) {
          Content content = (Content) cardItem.getItem();
          content.setAdded((getAddedContentCodes().contains(content.getCode())));
        }
      }
    }
    super.onSuccess(queryType, result);
  }

  @Override public void onClickCardItem(ItemView v, Item item) {
    if (item instanceof Content) {
      Content content = (Content) item;
      if (getAddedContentCodes().contains(content.getCode())) {
        getAddedContentCodes().remove(content.getCode());
      } else {
        getAddedContentCodes().add(0, content.getCode());
      }
    }
  }

  @Override public View getEmptyView() {
    return new SimpleEmptyView(getActivity(), R.string.icon_movie, "평가한 영화가 없어요.");
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return () -> {
      Util.closeKeyboard(mSearchTextLayout.getSearchView());
      return true;
    };
  }
}
