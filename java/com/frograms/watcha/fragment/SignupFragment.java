package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.OnClick;
import carbon.animation.AnimUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.LightAnimationListener;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.SimpleFacebookUser;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.BitmapUtil;
import com.frograms.watcha.utils.FormValidator;
import com.frograms.watcha.utils.ImageChooser;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.widget.wImages.WImageView;
import com.nineoldandroids.animation.Animator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 회원가입 화면.
 */
public class SignupFragment extends BaseFragment
    implements FormValidator.FormValidationListener, ImageChooser.OnImageChosenListener {

  @Bind(R.id.email) EditText mEmailTextView;
  @Bind(R.id.signup_root) View mSignupRootView;
  @Bind(R.id.username) EditText mUsernameTextView;
  @Bind(R.id.password) EditText mPasswordTextView;
  @Bind(R.id.btn_signup) TextView mSignupButton;
  @Bind(R.id.signup_userimage) WImageView mUserPicture;
  @Bind(R.id.signup_userimage_layout) FrameLayout mUserPictureLayout;

  private final FormValidator mEmailValidator = new FormValidator(FormValidator.FormStyle.EMAIL);
  private final FormValidator mPasswordValidator =
      new FormValidator(FormValidator.FormStyle.PASSWORD);
  private final FormValidator mUsernameValidator =
      new FormValidator(FormValidator.FormStyle.USERNAME);
  private boolean isValidPassword = false;
  private boolean isValidUsername = false;
  private boolean isValidEmail = false;
  private boolean mRevealed = false;

  private Bitmap mUserPictureBitmap = null;
  private Bitmap mUserCoverPictureBitmap = null;
  private String mFacebookToken = null;
  private SimpleFacebookUser mFacebookUser = null;

  private static final int PROFILE_IMAGE_MAX_PX_SIZE = 200;

  private ApiResponseListener mSignupResponseListener = null;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.SIMPLE_FACEBOOK_USER)) {
      mFacebookUser = bundle.getParcelable(BundleSet.SIMPLE_FACEBOOK_USER);
    }

    if (bundle != null && bundle.containsKey(BundleSet.CIRCLE_REVEAL_ANIMATION)) {
      mRevealed = bundle.getBoolean(BundleSet.CIRCLE_REVEAL_ANIMATION, false);
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.SIGNUP;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mSignupResponseListener = new BaseApiResponseListener<BaseResponse<User>>(getActivity()) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);
        WLog.i("result: " + result.getData().toString());
        WatchaApp.getSessionManager().storeWatchaSessionInLocalStorage(getActivity());
        WatchaApp.getInstance().setUser(result.getData());
        CacheManager.putMainUser(result.getData());
        ActivityStarter.with(getBaseActivity(), FragmentTask.WELCOME_SIGNUP).start();
        getActivity().finish();
      }
    };
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if (mFacebookUser != null) {
      outState.putParcelable(BundleSet.SIMPLE_FACEBOOK_USER, mFacebookUser);
    }

    outState.putBoolean(BundleSet.CIRCLE_REVEAL_ANIMATION, mRevealed);
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("가입");
    getToolbarHelper().getToolbar().setBackgroundColor(Color.TRANSPARENT);
    FontIconDrawable back = FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back);
    back.setTextColor(getResources().getColor(R.color.black));
    getToolbarHelper().getToolbar().setNavigationIcon(back);
    getToolbarHelper().getToolbar().setTitleTextColor(getResources().getColor(R.color.black));
  }

  @Override protected int getToolbarColor() {
    return R.color.white;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_signup;
  }

  @Override public void onViewCreated(final View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mUserPicture.loadResource(R.drawable.placeholder_profile);
    //if (mRevealed) {
    //  getToolbarHelper().getToolbar().setAlpha(0.0f);
    //  view.setAlpha(0.0f);
    //  Handler handler = new Handler();
    //  handler.postDelayed(new Runnable() {
    //    @Override public void run() {
    //      try {
    //        AnimUtils.fadeIn(view, null);
    //        AnimUtils.fadeIn(getToolbarHelper().getToolbar(), null);
    //      } catch(Exception e) {
    //      }
    //    }
    //  }, 250);
    //}

    mEmailValidator.registerValidator(mEmailTextView, this);
    mUsernameValidator.registerValidator(mUsernameTextView, this);
    mPasswordValidator.registerValidator(mPasswordTextView, this);
    mPasswordTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          onClickSignup();
          return true;
        }
        return false;
      }
    });
    FontHelper.RobotoRegular(mEmailTextView);
    FontHelper.RobotoRegular(mUsernameTextView);
    FontHelper.RobotoRegular(mPasswordTextView);
    if (mFacebookUser != null) {
      setFacebookUserSignup(mRevealed);
    }
  }

  private void setFacebookUserSignup(boolean revealed) {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override public void run() {
        mFacebookToken = mFacebookUser.getToken();
        mEmailTextView.setText(mFacebookUser.getEmail());
        mEmailTextView.setTextColor(getResources().getColor(R.color.heavy_gray));
        mUsernameTextView.setText(mFacebookUser.getName().replaceAll(" ", ""));
        mPasswordTextView.requestFocus();
        mUserPicture.setVisibility(View.VISIBLE);
        loadFacebookUserProfileBitmap();
        loadFacebookUserProfileCoverBitmap();
      }
    }, revealed ? 270 : 1);
  }

  private void loadFacebookUserProfileBitmap() {
    Glide.with(getActivity())
        .load(mFacebookUser.getProfileUrl())
        .asBitmap()
        .into(new BitmapImageViewTarget(mUserPicture) {
          @Override public void onResourceReady(Bitmap resource,
              GlideAnimation<? super Bitmap> glideAnimation) {
            super.onResourceReady(resource, glideAnimation);
            mUserPictureBitmap = resource;
            //if (resource != null && resource.getWidth() > 0) {
            //  mUserPictureBitmap =
            //      new WImageCircleTransform(mFacebookUser.getProfileUrl(), Glide.get(getActivity())
            //          .getBitmapPool(), 0, Color.TRANSPARENT).transform(resource);
            //  mUserPicture.setImageBitmap(mUserPictureBitmap);
            //}
          }
        });
  }

  private void loadFacebookUserProfileCoverBitmap() {
    SimpleTarget<Bitmap> target = new SimpleTarget<Bitmap>() {
      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        mUserCoverPictureBitmap = resource;
      }
    };
    Glide.with(getActivity()).load(mFacebookUser.getCoverUrl()).asBitmap().into(target);
  }

  private static File getPersistImage(Bitmap bitmap, String name) {
    File filesDir = WatchaApp.getInstance().getFilesDir();
    File imageFile = new File(filesDir, name + ".jpg");

    OutputStream os;
    try {
      os = new FileOutputStream(imageFile);
      bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
      os.flush();
      os.close();
      return imageFile;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @OnClick(R.id.btn_signup) void onClickSignup() {
    String email = mEmailTextView.getText().toString();
    String username = mUsernameTextView.getText().toString();
    String password = mPasswordTextView.getText().toString();
    //mUserPictureBitmap = mUserPicture.getBitmap();

    if (!mEmailValidator.isValid(email)
        || !mUsernameValidator.isValid(username)
        || !mPasswordValidator.isValid(password)) {
      Toast.makeText(getActivity(), "이메일이나 비밀번호나 이름 형식이 맞지 않아서 로그인을 진행할 수가 없어요", Toast.LENGTH_SHORT)
          .show();
      return;
    }
    Map<String, String> params = new HashMap<>();
    params.put("email", email);
    params.put("name", username);
    params.put("password", password);

    if (!TextUtils.isEmpty(mFacebookToken)) {
      params.put("fb_user_access_token", mFacebookToken);
    }
    DataProvider provider = new DataProvider(getActivity(), QueryType.SIGNUP_EMAIL, createReferrerBuilder());
    provider.withParams(params);
    if (mUserPictureBitmap != null) {
      provider.addFile(QueryFile.PROFILE_PHOTO,
          getPersistImage(mUserPictureBitmap, "photo_" + System.currentTimeMillis()));
    }
    if (mUserCoverPictureBitmap != null) {
      provider.addFile(QueryFile.COVER_PHOTO,
          getPersistImage(mUserCoverPictureBitmap, "cover_photo_" + System.currentTimeMillis()));
    }

    provider.withDialogMessage("가입 중..");
    provider.responseTo(mSignupResponseListener);
    provider.request();
  }

  @OnClick(R.id.signup_userimage_layout) void onClickUserImage() {
    getImageChooser(QueryFile.PROFILE_PHOTO).showCaptureImageDialog("이미지 선택하기");
  }

  @OnClick(R.id.agreement) void onClickAgreement() {
    Bundle b = new BundleSet.Builder().putUrl(WatchaApp.DOMAIN + "/webview/legals", "약관")
        .build()
        .getBundle();
    ActivityStarter.with(getActivity(), FragmentTask.WEBVIEW).addBundle(b).start();
  }

  private FontIconDrawable getCheckIconDrawable(boolean isValid) {
    FontIconDrawable checkIcon = null;
    if (isValid) {
      checkIcon = FontIconDrawable.inflate(getActivity(), R.xml.icon_arrow_check);
      checkIcon.setTextColor(getResources().getColor(R.color.green));
      checkIcon.setTextSize(getResources().getDimension(R.dimen.more_font));
    }
    return checkIcon;
  }

  @Override public void onValidation(FormValidator.FormStyle style, EditText v, String text,
      boolean isValid) {
    switch (style) {
      case EMAIL:
        isValidEmail = isValid;
        break;
      case USERNAME:
        isValidUsername = isValid;
        break;
      case PASSWORD:
        isValidPassword = isValid;
        break;
    }
    final boolean enabled = (isValidUsername && isValidEmail && isValidPassword);
    mSignupButton.setEnabled(enabled);
    mSignupButton.setBackgroundResource(
        enabled ? R.drawable.bg_welcome_ok : R.drawable.bg_button_corner_gray_nor);
    v.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(isValid), null);
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    if (mRevealed) {
      return new OnBackKeyListener() {
        @Override public boolean onBackKeyDown() {
          if (mImageChooser != null
              && mImageChooser.captureImageDialog != null
              && mImageChooser.captureImageDialog.isShowing()) {
            return false;
          }
          AnimUtils.fadeOut(getToolbarHelper().getToolbar(), null);
          AnimUtils.fadeOut(mSignupRootView, new LightAnimationListener() {
            @Override public void onAnimationEnd(Animator animation) {
              Util.closeKeyboard(mEmailTextView);
              Util.closeKeyboard(mPasswordTextView);
              Util.closeKeyboard(mUsernameTextView);
              Intent i = new Intent();
              i.putExtra("reveal", true);
              getActivity().setResult(Activity.RESULT_OK, i);
              getActivity().finish();
            }
          });
          return false;
        }
      };
    } else {
      return new OnBackKeyListener() {
        @Override public boolean onBackKeyDown() {
          Util.closeKeyboard(mEmailTextView);
          Util.closeKeyboard(mPasswordTextView);
          Util.closeKeyboard(mUsernameTextView);
          getActivity().finish();
          return false;
        }
      };
    }
  }

  @Override public void onImageChosenCallback(int fileType, Uri bitmapUri,
      ImageChooser.ImageUriParser parser) {
    WLog.i("uri: " + bitmapUri.getPath());
    mUserPictureBitmap = parser.getBitmapFromUri(getActivity(), bitmapUri);
    // 서버에 이미지 전송 시에 데이터량 부담을 덜기 위해서, 설정 해놓은 최대 사이즈를 넘지 않도록 이미지 사이즈 변경함.
    if (mUserPictureBitmap != null && mUserPictureBitmap.getWidth() > PROFILE_IMAGE_MAX_PX_SIZE
        || mUserPicture.getHeight() > PROFILE_IMAGE_MAX_PX_SIZE) {
      mUserPictureBitmap = BitmapUtil.scaleDown(mUserPictureBitmap, PROFILE_IMAGE_MAX_PX_SIZE);
    }
    //mUserIcon.setVisibility(View.GONE);
    mUserPicture.setVisibility(View.VISIBLE);
    mUserPictureLayout.setBackgroundColor(Color.TRANSPARENT);
    mUserPicture.loadFromMediaStore(bitmapUri);
  }

  @Override protected boolean hasDefaultDropshadow() {
    return false;
  }
}
