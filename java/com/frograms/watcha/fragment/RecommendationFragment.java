package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.ButterKnife;
import carbon.animation.AnimUtils;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.listeners.LightAnimationListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.itemViews.ItemTagsView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.select_tabs.SelectTabLayout;
import com.frograms.watcha.view.select_tabs.SelectTabView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.views.CircleTextView;
import com.nineoldandroids.animation.Animator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecommendationFragment extends ListFragment<ListData>
    implements SelectTabView.OnTabSelectListener, RecyclerAdapter.OnClickCardItemListener,
    EmptyViewPresenter {

  private static final int MOVIES_TAB = 0;
  private static final int DRAMAS_TAB = 1;
  private static final int BOOKS_TAB = 2;

  private CategoryType mCategoryType = CategoryType.MOVIES;

  private SelectTabLayout mSelectTabLayout;
  private CircleTextView mTagCountTextView;
  private View mSettingTagView;
  private int[] mTagIds = null;
  private int mSelectedTabPosition = 0;
  protected List<TagItems.Tag> mTagSnapshot = new ArrayList<>();

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected boolean enableSwipeRefresh() {
    return true;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
      mSelectedTabPosition = mCategoryType.getTypeInt();
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          return AnalyticsManager.RECOMMENDATION_MOVIE;
        case TV_SEASONS:
          return AnalyticsManager.RECOMMENDATION_DRAMA;
        case BOOKS:
          return AnalyticsManager.BOOK;
      }
    }
    return null;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if (mCategoryType != null) {
      outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());
    }
  }

  @Override protected QueryType getQueryType() {
    return QueryType.RECOMMENDATION.setApi(mCategoryType.getApiPath());
  }

  private View getSettingTagView() {
    if (mSettingTagView == null) {
      mSettingTagView =
          LayoutInflater.from(getActivity()).inflate(R.layout.undertoolbar_recommendation, null);
      IconActionGridButton title = ButterKnife.findById(mSettingTagView, R.id.layout_single_button);
      title.setTextNormalColor(getResources().getColor(R.color.gray));
      title.setIconColor(getResources().getColor(R.color.skyblue));
      title.setText("태그설정");
      title.setIcon(getResources().getString(R.string.icon_rate_recommend));
      mTagCountTextView = ButterKnife.findById(mSettingTagView, R.id.tag_count);
      mSettingTagView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          BundleSet.Builder builder = new BundleSet.Builder().putCategoryType(mCategoryType);
          if (mTagIds != null) {
            builder.putTagIds(mTagIds);
          }
          // 태그 덤프 생성. 태그설정화면에서 유저가 뒤로가기 버튼으로 걍 꺼버릴 수도 있을테니 그때 되돌릴 태그들.
          mTagSnapshot = new ArrayList(CacheManager.getCachedTags());
          ActivityStarter s = ActivityStarter.with(getActivity(),
              mCategoryType == CategoryType.TV_SEASONS ? FragmentTask.TAG_SETTING_DRAMA
                  : FragmentTask.TAG_SETTING)
              .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
              .setRequestCode(ActivityStarter.TAG_SETTING_REQUEST);
          if (builder != null) {
            s.addBundle(builder.build().getBundle());
          }
          s.start();
        }
      });
    }
    return mSettingTagView;
  }

  private void animatePopup(final CircleTextView v, int count) {
    if (count > 0 && v.getVisibility() != View.VISIBLE) {
      // 보임.
      AnimUtils.popIn(v, new LightAnimationListener() {
        @Override public void onAnimationStart(Animator animation) {
          v.setVisibility(View.VISIBLE);
          v.setAlpha(0.0f);
        }
      });
    } else if (count < 1) {
      // 사라짐.
      AnimUtils.popOut(v, new LightAnimationListener() {
        @Override public void onAnimationEnd(Animator animation) {
          v.setVisibility(View.GONE);
        }
      });
    }
  }

  @Override public View getListHeaderView() {
    if (mSelectTabLayout == null) {
      mSelectTabLayout = new SelectTabLayout(getActivity());
      mSelectTabLayout.addTab(getString(R.string.movie), this);
      mSelectTabLayout.addTab(getString(R.string.drama), this);
      mSelectTabLayout.addTab(getString(R.string.book), this);
    }
    return mSelectTabLayout;
  }

  @Override public void load() {
    super.load();
    addUnderToolbarView(getSettingTagView());
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void unload() {
    super.unload();
    removeUnderToolbarView(getSettingTagView());
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mSelectTabLayout.selectTabPosition(mSelectedTabPosition);
    mAdapter.setOnClickCardItemListener(this);

    if (mSelectedTabPosition == CategoryType.BOOKS.getTypeInt()) {
      showBooks();
    }
  }

  @Override protected Map<String, String> getRequestDataParams() {
    if (mTagIds != null && mTagIds.length > 0) {
      Map<String, String> params = new HashMap<>();
      params.put("tags", getSelectedTags(mTagIds));
      return params;
    }
    return super.getRequestDataParams();
  }

  private String getSelectedTags(int[] tagIds) {
    List<Integer> list = new ArrayList<>();
    for (int i : tagIds) {
      list.add(i);
    }
    return TextUtils.join(",", list);
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case ActivityStarter.TAG_SETTING_REQUEST:
        if (resultCode == Activity.RESULT_OK && data != null && data.hasExtra(BundleSet.TAG_IDS)) {
          mTagIds = data.getIntArrayExtra(BundleSet.TAG_IDS);
          refresh();
        } else if (resultCode == Activity.RESULT_CANCELED) {
          CacheManager.getTagCache().evictAll();
          for (TagItems.Tag tag : mTagSnapshot) {
            CacheManager.putTag(tag);
          }
          WLog.i("revertedTagsCount: " + CacheManager.getTotalTagsCount());
        }
        break;
      case ActivityStarter.INCREASE_NUMBER_OF_CONTENTS_REQUEST:
        if (resultCode == Activity.RESULT_OK) {
          refresh();
        }
        break;
    }
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ListData> result) {
    super.onSuccess(queryType, result);

    //emptyview가 보여질 경우에, 이전 emptyview가 그대로 보여지므로 -> emptyview도 리프레시 함
    refreshEmptyListView();

    if (mTagIds != null && mTagCountTextView != null) {
      mTagCountTextView.setTitleText(mTagIds.length + "");
      animatePopup(mTagCountTextView, mTagIds.length);
    }
    //유저가 Back Button으로 나갔다오면 태그가 초기화되어야 함
    if (mTagIds == null && CacheManager.getTotalTagsCount() > 0) {
      CacheManager.getTagCache().evictAll();
    }
  }

  @Override public void onClickCardItem(ItemView v, Item item) {
    if (item instanceof TagItems
        && v.getTag() != null
        && v.getTag() instanceof Integer
        && v instanceof ItemTagsView) {
      Integer tagId = (Integer) v.getTag();
      mTagIds = new int[1];
      mTagIds[0] = tagId;
      refresh();
    }
  }

  @Override
  public void onClickSelectableTab(final SelectTabLayout layout, SelectTabView v, int position) {
    mSelectTabLayout.selectTabPosition(position);
    mTagIds = null;
    if (mTagCountTextView.getVisibility() != View.GONE && position != BOOKS_TAB) {
      AnimUtils.popOut(mTagCountTextView, new LightAnimationListener() {
        @Override public void onAnimationEnd(Animator animation) {
          mTagCountTextView.setTitleText(null);
          mTagCountTextView.setVisibility(View.GONE);
        }
      });
    }
    switch (position) {
      case MOVIES_TAB:
        if (mCategoryType != CategoryType.MOVIES) {
          if (mDataProvider != null) {
            WLog.e("call ShouldCancel");
            mDataProvider.shouldCancel(true);
          }

          CacheManager.getTagCache().evictAll();
          mSelectedTabPosition = position;
          mCategoryType = CategoryType.MOVIES;
          refresh();
          getSettingTagView().setVisibility(View.VISIBLE);
        }
        AnalyticsManager.sendScreenName(getCurrentScreenName());
        break;
      case DRAMAS_TAB:
        if (mCategoryType != CategoryType.TV_SEASONS) {
          if (mDataProvider != null) {
            WLog.e("call ShouldCancel");
            mDataProvider.shouldCancel(true);
          }

          CacheManager.getTagCache().evictAll();
          mSelectedTabPosition = position;
          mCategoryType = CategoryType.TV_SEASONS;
          refresh();
          getSettingTagView().setVisibility(View.VISIBLE);
          AnalyticsManager.sendScreenName(getCurrentScreenName());
        }
        break;
      case BOOKS_TAB:
        if (mCategoryType != CategoryType.BOOKS) {
          showBooks();
        }
        break;
    }
  }

  private void showBooks() {
    if (mDataProvider != null) {
      mDataProvider.shouldCancel(true);
    }

    CacheManager.getTagCache().evictAll();
    mSelectedTabPosition = CategoryType.BOOKS.getTypeInt();
    ;
    mCategoryType = CategoryType.BOOKS;
    mAdapter.clearItems();

    if (mAdapter.getFooterView() != null) {
      mAdapter.clearItems();
      mAdapter.getFooterView().setVisibility(View.INVISIBLE);
    } else {
      mAdapter.setDefaultLoadingVisibility(false);
    }

    if (mAdapter.getLoadingViewHolder() != null) {
      mAdapter.getLoadingViewHolder().onEnd();
    }
    refreshEmptyListView();
    setEmptyViewVisibility(View.VISIBLE);
    getSettingTagView().setVisibility(View.GONE);
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public View getEmptyView() {
    switch (mCategoryType) {
      case BOOKS:
        return new SimpleEmptyView(getActivity(), R.string.icon_book, "도서는 아직 준비중이에요!:)");
      case MOVIES:
        if (WatchaApp.getUser() != null
            && WatchaApp.getUser().getRatings() != null
            && WatchaApp.getUser().getRatings().getMoviesRating() != null
            && WatchaApp.getUser().getRatings().getMoviesRating().getCount() > 0) {
          return new SimpleEmptyView(getActivity(), R.string.icon_wish, "추천할 작품이 없습니다");
        }
        break;
      case TV_SEASONS:
        if (WatchaApp.getUser() != null
            && WatchaApp.getUser().getRatings() != null
            && WatchaApp.getUser().getRatings().getTvSeasonsRating() != null
            && WatchaApp.getUser().getRatings().getTvSeasonsRating().getCount() > 0) {
          return new SimpleEmptyView(getActivity(), R.string.icon_wish, "추천할 작품이 없습니다");
        }
        break;
    }
    return new SimpleEmptyView(getActivity(), R.string.icon_wish, "추천할 작품이 없습니다");
  }
}
