package com.frograms.watcha.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.PartnerHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.enums.PrivacyLevel;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.UserListData;
import com.frograms.watcha.retrofit.QueryType;

/**
 * 일반 유저 화면.
 */
public class UserProfileFragment extends ProfileFragment {

  private final QueryType mQueryType = QueryType.USER;
  private ToolbarHelper.ToolbarActionType mToolbarActionType = ToolbarHelper.ToolbarActionType.NONE;

  private MenuItems mMenuItems = null;
  private User mUser;
  private String mUserCode;

  public String getUserCode() {
    return mUserCode;
  }

  private PartnerHelper.OnBaseFollowResponseListener createFollowResponseListener(Context context) {
    return new PartnerHelper.OnBaseFollowResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);
        boolean isFriend = !mUser.isFriend();
        mUser.setFriend(isFriend);
        CacheManager.putUser(mUser);
        getActivity().supportInvalidateOptionsMenu();
        // 친구공개 때문에 접근할 수 없었다면, 접근할 수 있게 해줘야 해서 리프레시함.
        if (mUser.getPrivacyLevel() == PrivacyLevel.FRIENDS) {
          refresh();
        }
      }
    };
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.USER_CODE)) {
      mUserCode = bundle.getString(BundleSet.USER_CODE);
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    @AnalyticsManager.ScreenNameType String screenName =
        new ReferrerBuilder(AnalyticsManager.USER_PROFILE).appendArgument(mUserCode).toString();
    return screenName;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.USER_CODE, mUserCode);
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle(null);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mListHeader.findViewById(R.id.toolbar_place).setVisibility(View.GONE);
    getMypageHomeHeaderView().setPadding(0, getResources().getDimensionPixelSize(R.dimen.toolbar_height), 0, 0);
    getMypageHomeHeaderView().getCover().setOnClickListener(this);
  }

  @Override protected QueryType getQueryType() {
    return mQueryType.setApi(mUserCode);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return mToolbarActionType;
  }

  @Override public MenuItems getToolbarMenuItems() {
    if (mUser != null && WatchaApp.getUser() != null && !mUser.getCode()
        .equals(WatchaApp.getUser().getCode())) {
      return mMenuItems =
          mUser.isFriend() ? MenuItems.USER_PROFILE_FOLLOWING : MenuItems.USER_PROFILE_FOLLOW;
    }
    return null;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_follow:
      case R.id.menu_item_following:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
            !mUser.isFriend() ? AnalyticsActionType.FOLLOW : AnalyticsActionType.UNFOLLOW).build());
        PartnerHelper.setFollow(getActivity(), createReferrerBuilder(), mUserCode, !mUser
            .isFriend(), createFollowResponseListener(getActivity()));
        return true;
    }
    return false;
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<UserListData> result) {
    super.onSuccess(queryType, result);
    mUser = result.getData().getUser();
    if (mPage == 1 && mUser != null) {
      CacheManager.putUser(mUser);
      getActivity().supportInvalidateOptionsMenu();
    }
  }
}
