package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import butterknife.Bind;
import butterknife.OnClick;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.SelectedTagsView;
import com.frograms.watcha.view.widget.TagLayout;
import com.frograms.watcha.view.widget.TagTextView;
import com.rey.material.widget.ProgressView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link TagSettingFragment} 에 붙어있는 탭 화면.
 */
public class TagsFragment extends BaseFragment
    implements TagLayout.OnTagItemClickListener, SelectedTagsView.OnTagItemResetListener {

  @Bind(R.id.tag_layout) TagLayout mTagLayout;
  @Bind(R.id.tag_load_btn_layout) View mLoadButtonLayout;
  @Bind(R.id.progress_loading) ProgressView mProgressView;

  protected List<TagItems.Tag> mSelectedTags = new ArrayList<>();

  private int mPage = 1;
  private QueryType mQueryType = QueryType.TAGS;
  private boolean mHasNextPage = false;

  /**
   * {@link RecommendationFragment} 가 이미 선택된 태그 id 들을 넘겨줌.
   */
  private int[] mBundledTagIds = null;

  /**
   * 카테고리 마다 태그 api 가 다름.
   */
  private CategoryType mCategoryType = null;

  @Override protected QueryType getQueryType() {
    if (mCategoryType != null) {
      return mQueryType.setApi(mCategoryType.getApiPath());
    }
    return null;
  }

  private Map<String, String> mParams = new HashMap<>();

  @Override protected Map<String, String> getRequestDataParams() {
    mParams.put("page", String.valueOf(mPage));
    return mParams;
  }

  @Override protected DataProvider getDataProvider() {
    return getDataProvider(getRequestDataParams());
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_tags;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.TAG_IDS)) {
      mBundledTagIds = bundle.getIntArray(BundleSet.TAG_IDS);
    }

    if (bundle != null && bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putIntArray(BundleSet.TAG_IDS, mBundledTagIds);
    outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mTagLayout.setOnClickTagItemListener(this);
    load();
    mProgressView.setVisibility(View.VISIBLE);
  }

  @Override public void load() {
    super.load();
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  public String getCurrentScreenName() {
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          return AnalyticsManager.TAG_SETTING_MOVIE;
        case TV_SEASONS:
          return AnalyticsManager.TAG_SETTING_DRAMA;
      }
    }
    return null;
  }

  private boolean isFocusedTagItem(TagItems.Tag tag) {
    if (mBundledTagIds == null) {
      return false;
    }
    for (int id : mBundledTagIds) {
      if (tag.getId() == id) {
        return true;
      }
    }
    return false;
  }

  @Override public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse result) {
    super.onSuccess(queryType, result);
    if (mProgressView != null && mProgressView.getVisibility() != View.GONE) {
      mProgressView.setVisibility(View.GONE);
    }
    if (result.getData() instanceof TagItems) {
      TagItems data = (TagItems) result.getData();
      mHasNextPage = data.hasNextPage();
      mLoadButtonLayout.setVisibility(mHasNextPage ? View.VISIBLE : View.GONE);
      if (data.getTags() != null) {
        for (TagItems.Tag t : data.getTags()) {
          boolean isBundledTagItem = isFocusedTagItem(t);
          mTagLayout.addTag(t, isBundledTagItem);
          if (isBundledTagItem) {
            // 이미 선택된 태그니까 추가.
            mSelectedTags.add(t);
            CacheManager.putTag(t);
          }
        }
      }
    }
    mPage++;
  }

  @OnClick(R.id.tag_load_btn) void onClickLoadMore() {
    if (mHasNextPage) {
      requestData(getDataProvider());
    }
  }

  @Override public void onClickTagItem(TagLayout layout, TagTextView v, TagItems.Tag tag) {
    switch (layout.getId()) {
      case R.id.tag_layout:
        if (mSelectedTags.contains(tag)) {
          // 이미 선택된 태그를 클릭했다면 지움.
          v.setFocused(false);
          mSelectedTags.remove(tag);
          CacheManager.removeTag(tag);
        } else {
          // 선택한 태그 추가.
          v.setFocused(true);
          mSelectedTags.add(tag);
          CacheManager.putTag(tag);
        }
        break;
    }
  }

  @Override public void resetTagItems(List<TagItems.Tag> tags) {
    if (mTagLayout != null) {
      mBundledTagIds = null;
      for (TagItems.Tag tag : tags) {
        TagTextView tagText = mTagLayout.getTagTextView(tag);
        if (tagText != null) {
          mSelectedTags.remove(tag);
          tagText.setFocused(false);
        }
      }
    }
  }
}
