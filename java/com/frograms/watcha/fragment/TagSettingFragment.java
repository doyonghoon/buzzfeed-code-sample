package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.database.TagCache;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Tab;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.model.response.data.VodTagsFragment;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.SelectedTagsView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.widget.TagSettingToolbarView;
import java.util.ArrayList;
import java.util.List;

/**
 * 태그설정 화면.
 */
public class TagSettingFragment extends AbsPagerFragment
    implements TagCache.OnTagCacheUpdateListener {

  private TagSettingToolbarView mToolbarView;
  //private List<TagItems.Tag> mTags = new ArrayList<>();
  //private List<TagItems.Tag> mVodTags = new ArrayList<>();
  private CategoryType mCategoryType = null;
  private Bundle mBundle = null;
  private int mSelectedTagsCount = 0;
  private SelectedTagsView mSelectedTagsView = null;

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle(null);
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    mBundle = bundle;
    if (bundle != null && bundle.containsKey(BundleSet.TAG_IDS)) {
      mSelectedTagsCount = bundle.getIntArray(BundleSet.TAG_IDS).length;
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override protected boolean shouldLoadAll() {
    return false;
  }

  @Override protected List<Tab> getTabs() {
    if (mTabs == null) {
      CacheManager.addTagUpdateListener(this);
      TagsFragment tags = new TagsFragment();
      tags.setBundle(mBundle);
      VodTagsFragment vodTags = new VodTagsFragment();
      vodTags.setBundle(mBundle);
      mTabs = new ArrayList<>();
      mTabs.add(new Tab("태그", tags));
      mTabs.add(new Tab("감상", vodTags));
    }
    return mTabs;
  }

  private View getToolbarView() {
    if (mToolbarView == null) {
      int tagCount = CacheManager.getTotalTagsCount();
      mToolbarView = new TagSettingToolbarView(getActivity());
      if (tagCount > 0) {
        mToolbarView.setTagCount(tagCount);
      }
      mToolbarView.setOnTagOkClickListener(new TagSettingToolbarView.OnTagOkClickListener() {
        @Override public void onClickOk(int tagCount) {
          int[] combined = CacheManager.getTagIds();
          Intent data = new Intent();
          data.putExtra(BundleSet.TAG_IDS, combined != null ? combined : new int[] {});
          getActivity().setResult(Activity.RESULT_OK, data);
          getActivity().finish();
        }
      });
    }
    return mToolbarView;
  }

  private SelectedTagsView getSelectedTagsView() {
    if (mSelectedTagsView == null) {
      mSelectedTagsView = new SelectedTagsView(getActivity());

      //Tab들을 OnTagItemResetListener로 캐스팅해서 mSelectedTagsView에게 넘긴다
      for (Tab tab : getTabs())
        if (tab.getFragment() instanceof SelectedTagsView.OnTagItemResetListener) {
          mSelectedTagsView.addOnTagItemResetListener(
              (SelectedTagsView.OnTagItemResetListener) tab.getFragment());
        }
    }
    return mSelectedTagsView;
  }

  @Override protected View getCustomToolbarView() {
    return getToolbarView();
  }

  @Override protected float getTabTextSizeInDp() {
    return 14.5f;
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    CacheManager.removeTagUpdateListener(TagSettingFragment.this);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (mSelectedTagsCount > 0) {
      mToolbarView.setTagCount(mSelectedTagsCount);
    }
    // 기본 값으로 태그설정 화면이 닫히는건 취소임.
    getActivity().setResult(Activity.RESULT_CANCELED);

    // Cache된 태그들 있다면 UnderBar에 추가
    if (CacheManager.getTagSnapshot().entrySet().size() > 0) {
      if (!getSelectedTagsView().isShown()) {
        addUnderToolbarView(getSelectedTagsView());
        getSelectedTagsView().setIsShown(true);
      }
      getSelectedTagsView().addTags(CacheManager.getTagSnapshot().entrySet());
      getSelectedTagsView().autoScrollToEnd();
    }
  }

  @Override public void unload() {
    super.unload();
    removeUnderToolbarView(getSelectedTagsView());
    getSelectedTagsView().setIsShown(false);
  }

  @Override public void onAddTagCache(TagCache cache, TagItems.Tag tag) {
    mToolbarView.setTagCount(cache.size());
    WLog.i("AddedTag: " + tag.getName() + ", size: " + cache.size());

    if (cache.size() >= 1 && !getSelectedTagsView().isShown()) {
      addUnderToolbarView(getSelectedTagsView());
      getSelectedTagsView().setIsShown(true);
    }
    getSelectedTagsView().addTag(tag);
  }

  @Override public void onRemoveTagCache(TagCache cache, String key) {
    mToolbarView.setTagCount(cache.size());
    WLog.i("RemovedTag: " + key + ", size: " + cache.size());
    if (cache.size() < 1) {
      removeUnderToolbarView(getSelectedTagsView());
      getSelectedTagsView().setIsShown(false);
    }
    getSelectedTagsView().removeTag(Integer.parseInt(key));
  }
}
