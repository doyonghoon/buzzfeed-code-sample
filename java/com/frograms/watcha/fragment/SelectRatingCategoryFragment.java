package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.EvaluateCategoryItem;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.itemViews.ItemEvaluateCategoryView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

/**
 * 영평늘 하단뷰를 클릭했을 때 넘어오는 뷰.
 */
public class SelectRatingCategoryFragment extends ListFragment<ListData>
    implements RecyclerAdapter.OnClickCardItemListener {

  private QueryType mQueryType = QueryType.EVALUATE_CATEGORIES;
  private CategoryType mCategoryType = null;

  @Override protected QueryType getQueryType() {
    if (mCategoryType != null) {
      return mQueryType.setApi(mCategoryType.getApiPath());
    }
    return null;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          return AnalyticsManager.EVALUATE_CATEGORIES_MOVIE;
        case TV_SEASONS:
          return AnalyticsManager.EVALUATE_CATEGORIES_DRAMA;
        case BOOKS:
          return AnalyticsManager.BOOK;
      }
    }
    return null;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ListData> result) {
    super.onSuccess(queryType, result);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mAdapter.setOnClickCardItemListener(this);
    getActivity().setResult(Activity.RESULT_CANCELED);
    getToolbarHelper().getActionbar().setTitle(R.string.evaluate_categories);
  }

  @Override public void onClickCardItem(ItemView v, Item item) {
    if (item != null
        && item instanceof EvaluateCategoryItem
        && v != null
        && v instanceof ItemEvaluateCategoryView) {
      EvaluateCategoryItem i = (EvaluateCategoryItem) item;
      @AnalyticsEvent.AnalyticsEventLabel final String categoryId = i.getId();
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.CATEGORY_IN_RATE_MORE)
          .setLabel(categoryId)
          .build());
      Intent data = new Intent();
      data.putExtra(BundleSet.CATEGORY_ID, i.getId());
      getActivity().setResult(Activity.RESULT_OK, data);
      getActivity().finish();
    }
  }
}
