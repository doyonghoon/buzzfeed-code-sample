package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.helpers.AlertHelper;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.Tab;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.UnreadData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import java.util.ArrayList;
import java.util.List;

/**
 * 발견 화면.
 *
 * 리스트 헤더뷰가 있음.
 */
public class HomeFragment extends AbsPagerFragment {

  private final String[] mTitles = { "피드", "랭킹", "추천", "소식", "내 프로필" };

  private CategoryType mCategoryType;
  private String mCategoryId;

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_search:
        ActivityStarter.with(getActivity(), FragmentTask.SEARCH)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getCurrentFragment().getCurrentScreenName())
                .build()
                .getBundle())
            .start();
        return true;

      case R.id.menu_item_more_vertical:
        showMyProfilePopupMenu(getActivity().findViewById(R.id.menu_item_more_vertical));
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }

    if (bundle.containsKey(BundleSet.CATEGORY_ID)) {
      mCategoryId = bundle.getString(BundleSet.CATEGORY_ID);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if (mCategoryType != null) outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());

    if (mCategoryId != null) outState.putString(BundleSet.CATEGORY_ID, mCategoryId);
  }

  @Override public MenuItems getToolbarMenuItems() {
    return MenuItems.HOME;
  }

  @Override protected List<Tab> getTabs() {
    if (mTabs == null) {
      mTabs = new ArrayList<>();
      mTabs.add(new Tab(R.string.icon_feed, new FeedFragment()));
      PopularFragment frag1 = new PopularFragment();
      RecommendationFragment frag2 = new RecommendationFragment();

      if (mCategoryType != null) {
        Bundle bundle;
        if (mCurrentPageItem == 1) {
          bundle = new BundleSet.Builder().putCategoryType(mCategoryType)
              .putCategoryId(mCategoryId)
              .build()
              .getBundle();
          frag1.setBundle(bundle);
        } else if (mCurrentPageItem == 2) {
          bundle = new BundleSet.Builder().putCategoryType(mCategoryType).build().getBundle();
          frag2.setBundle(bundle);
        }
      }

      mTabs.add(new Tab(R.string.icon_popular, frag1));
      mTabs.add(new Tab(R.string.icon_wish, frag2));
      mTabs.add(new Tab(R.string.icon_activity, new NotificationFragment()));
      mTabs.add(new Tab(R.string.icon_profile, new ProfileFragment()));
    }
    return mTabs;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setLogo();

    int count =
        PrefHelper.getCount(getActivity(), PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.VISIT);

    WLog.e(count + "");
    if (count <= 5) {
      PrefHelper.addCount(getActivity(), PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.VISIT);
    }

    if (count == 3) {
      AlertHelper.showAlert(getActivity(), AlertHelper.AlertType.PLAY_RATING);
    } else if (count == 5) {
      AlertHelper.showAlert(getActivity(), AlertHelper.AlertType.INVITE);
    }

    DataProvider<BaseResponse<UnreadData>> provider =
        new DataProvider<BaseResponse<UnreadData>>(getActivity(),
            QueryType.UNREAD_NOTIFICATIONS, createReferrerBuilder()).responseTo((queryType, result) -> {
              int unread = result.getData().getUnread();
              if (unread > 0) {
                mTab.getNumTextViews()[3].setVisibility(View.VISIBLE);
                mTab.getNumTextViews()[3].setText((unread <= 9) ? unread + "" : "9+");
              }
            });

    provider.request();
  }

  private void setLogo() {
    if (mCurrentPageItem > 0) {
      getToolbarHelper().getActionbar().setDisplayUseLogoEnabled(false);
      getToolbarHelper().getActionbar().setDisplayShowTitleEnabled(true);
      getToolbarHelper().getActionbar().setTitle(mTitles[mCurrentPageItem]);
    } else {
      getToolbarHelper().getActionbar().setDisplayUseLogoEnabled(true);
      FontIconDrawable drawable = FontIconDrawable.inflate(getActivity(), R.xml.icon_logotype);
      drawable.setTextColor(getResources().getColor(R.color.white));
      drawable.setTextSize(getResources().getDimension(R.dimen.logotype_font));

      getToolbarHelper().getActionbar().setLogo(drawable);
      //getToolbarHelper().getActionbar().setTitle(mTitles[mCurrentPageItem]);
      getToolbarHelper().getActionbar().setDisplayShowTitleEnabled(false);
    }
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override public void onPageSelected(int position) {
    super.onPageSelected(position);
    if (position == 3) {
      mTab.getNumTextViews()[3].setVisibility(View.GONE);
    }
    setLogo();
    getActivity().supportInvalidateOptionsMenu();
  }

  private void showMyProfilePopupMenu(View anchor) {
    PopupMenu popupMenu = new PopupMenu(getActivity(), anchor);
    popupMenu.inflate(R.menu.menu_my_profile);
    popupMenu.setOnMenuItemClickListener(item -> {
      switch (item.getItemId()) {
        case R.id.profile_setting:
          ActivityStarter.with(getActivity(), FragmentTask.SETTING).start();
          return true;

        case R.id.profile_partner_setting:
          ActivityStarter.with(getActivity(), FragmentTask.PARTNER_CANDIDATES).start();
          return true;

        case R.id.profile_find_friends:
          ActivityStarter.with(getActivity(), FragmentTask.FIND_USER).start();
          return true;

        case R.id.profile_gift:
          ActivityStarter.with(getActivity(), FragmentTask.WEBVIEW)
              .addBundle(new BundleSet.Builder().putUrl(BuildConfig.END_POINT + "/webview/gifts",
                  getString(R.string.hoppin)).build().getBundle())
              .start();
          return true;

        case R.id.profile_meh:
          ActivityStarter.with(getActivity(), FragmentTask.USER_CONTENT_LIST)
              .addBundle(new BundleSet.Builder().putUserActionType(UserActionType.MEH)
                  .putUserCode(WatchaApp.getUser().getCode())
                  .build()
                  .getBundle())
              .start();
          return true;
      }
      return false;
    });
    popupMenu.show();
  }
}
