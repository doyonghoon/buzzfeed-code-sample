package com.frograms.watcha.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.facebook.AccessToken;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.helpers.UserActionHelper.OnBaseRatingResponseListener;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.WPopupRateManager;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.menus.MenuSet;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.ContentData;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.FormValidator;
import com.frograms.watcha.utils.RatingUtils;
import com.frograms.watcha.utils.TwitterBroker;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.utils.WatchaPermission;
import com.frograms.watcha.view.LoadingView;
import com.frograms.watcha.view.textviews.FeedRatingTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.orhanobut.dialogplus.DialogPlus;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import rx.functions.Action1;

public class WriteCommentFragment extends BaseFragment<BaseResponse<ContentData>>
    implements View.OnClickListener, FormValidator.FormValidationListener {

  private static final int MAX_NUM = 100;

  @Bind(R.id.loading_view) LoadingView mLoadingView;

  @Bind(R.id.write_comment_layer) View mWriteCommentLayer;

  @Bind(R.id.plz_rating) View mPlzRating;
  @Bind(R.id.rating_text) FeedRatingTextView mRatingTextView;
  @Bind(R.id.right_arrow) FontIconTextView mRightArrowView;

  @Bind(R.id.comment_layer) View mCommentLayer;
  @Bind(R.id.comment) EditText mCommentView;
  @Bind(R.id.activity) TextView mActivityView;
  @Bind(R.id.remain_num) TextView mNumView;

  @Bind(R.id.date) FontIconTextView mDateView;
  @Bind(R.id.share_twitter) FontIconTextView mShareTwtView;
  @Bind(R.id.share_facebook) FontIconTextView mShareFbView;

  private TextView mCompleteView;

  private QueryType mQueryType = QueryType.NEW_COMMENT_INFO;
  private QueryType mWriteQueryType = QueryType.WRITE_COMMENT;

  private CategoryType mCategoryType;
  private String mCode, mCommentCode, mTitle;
  private UserAction mUserAction;

  private WPopupRateManager mPopupRateManager;
  private OnBaseRatingResponseListener mListener;

  private DataProvider<BaseResponse<RatingData>> mCommentDataProvider;

  private FormValidator mValidator = new FormValidator(FormValidator.FormStyle.COMMENT);

  private boolean isTextLengthEnabled = false;

  // Activity 관련
  private String mActivity;
  private Map<String, String> mActivityParams;

  // Date 관련
  private DatePickerDialog mPickerDialog;
  private Date mDate;

  // Privacy 관련
  private DialogPlus mPrivacyDialog;
  //private PrivacyLevel mPrivacy = PrivacyLevel.ALL;

  // facebook 공유 관련
  private boolean isShareFb = false;

  // twitter 공유 관련
  private boolean isShareTwt = false;

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override protected QueryType getQueryType() {
    if (mQueryType.equals(QueryType.COMMENT_INFO)) mQueryType.setApi(mCommentCode);

    return mQueryType;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_write_comment;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.CONTENT_CODE)) {
      mCode = bundle.getString(BundleSet.CONTENT_CODE);
    }

    if (bundle.containsKey(BundleSet.COMMENT_CODE)) {
      mQueryType = QueryType.COMMENT_INFO;
      mCommentCode = bundle.getString(BundleSet.COMMENT_CODE);
    } else {
      mQueryType = QueryType.NEW_COMMENT_INFO;
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Nullable @Override public String getCurrentScreenName() {
    return TextUtils.isEmpty(mCode) ? AnalyticsManager.WRITE_COMMENT
        : AnalyticsManager.EDIT_COMMENT;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.CONTENT_CODE, mCode);

    if (mCommentCode != null) outState.putString(BundleSet.COMMENT_CODE, mCommentCode);
  }

  protected Map<String, String> getRequestDataParams() {
    switch (mQueryType) {
      case NEW_COMMENT_INFO:
        Map<String, String> params = super.getRequestDataParams();

        if (params == null) params = new HashMap<>();
        params.put("content_code", mCode);
        return params;
    }

    return super.getRequestDataParams();
  }

  @Override public void onClick(View v) {
    switch (v.getId()) {
      case R.id.action_menu:
        if (mUserAction == null) {
          Toast.makeText(getActivity(), R.string.plz_rating, Toast.LENGTH_SHORT).show();
          return;
        }

        if (mUserAction.getRating() == 0 && !mUserAction.isWished() && !mUserAction.isMehed()) {
          Toast.makeText(getActivity(), R.string.plz_rating, Toast.LENGTH_SHORT).show();
          return;
        }

        if (mCommentView.getText().length() == 0) {
          Toast.makeText(getActivity(), R.string.need_comment, Toast.LENGTH_SHORT).show();
          return;
        }

        if (!isTextLengthEnabled) {
          Toast.makeText(getActivity(), R.string.need_length_comment, Toast.LENGTH_SHORT).show();
          return;
        }

        Util.closeKeyboard(mCommentView);

        Map<String, String> params = new HashMap<>();
        params.put("content_code", mCode);
        params.put("text", mCommentView.getText().toString());
        if (mActivityParams != null) params.putAll(mActivityParams);

        getCommentDataProvider().withParams(params).request();
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.COMMENT)
            .build());
        break;

      case R.id.rating_layer:
        if (mPopupRateManager != null) {
          mPopupRateManager.show();
        }
        break;

      case R.id.comment_layer:
        mCommentView.requestFocus();
        Util.openKeyboard(mCommentView);
        break;

      case R.id.date:
        if (mDate == null) {
          getDatePickerDialog().show();
        } else {
          mDate = null;
          setActivity();
        }
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.BUTTON_CALENDAR_IN_COMMENT_EDITOR)
            .build());
        break;

      //case R.id.privacy:
      //  if (mUserAction == null
      //      || mUserAction.getUser() == null
      //      || mUserAction.getUser().getPrivacyLevel() == null) {
      //    break;
      //  }
      //  if (mUserAction.getUser().getPrivacyLevel() == PrivacyLevel.ALL) {
      //    //getPrivacyDialog().show();
      //  } else {
      //    Toast.makeText(getActivity(), String.format("공개설정이 %s 로 설정되어있습니다. 개별 코멘트의 공개여부를 제어하기 위해선 '설정>공개여부설정' 화면에서 전체공개로 선택 후, 코멘트 공개여부 설정을 해주세요!", getString(mUserAction
      //        .getUser()
      //        .getPrivacyLevel()
      //        .getStringId())), Toast.LENGTH_LONG).show();
      //  }
      //  break;

      case R.id.share_twitter:
        if (isShareTwt) {
          // 트위터 공유 해제.
          isShareTwt = false;
          setActivity();
        } else {
          final SettingHelper.OnSettingResponseListener twitterTokenListener =
              new SettingHelper.OnSettingResponseListener(getActivity()) {
                @Override public void onSuccess(@NonNull QueryType queryType,
                    @NonNull BaseResponse<User> result) {
                  super.onSuccess(queryType, result);
                  isShareTwt = true;
                  setActivity();
                }
              };

          if (WatchaApp.getUser().isTwtConnected()) {
            // 트위터 공유 함.
            isShareTwt = true;
            setActivity();
          } else {

            TwitterBroker.connectTwitter(getBaseActivity(), twitterTokenListener);
          }
        }
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.SWITCH_TWITTER_IN_COMMENT_EDITOR)
            .build());
        break;

      case R.id.share_facebook:
        final SettingHelper.OnSettingResponseListener fbTokenListener =
            new SettingHelper.OnSettingResponseListener(getActivity()) {
              @Override public void onSuccess(@NonNull QueryType queryType,
                  @NonNull BaseResponse<User> result) {
                super.onSuccess(queryType, result);
                isShareFb = !isShareFb;
                setActivity();
              }
            };

        if (isShareFb || (WatchaApp.getUser().isFbConnected()
            && AccessToken.getCurrentAccessToken() != null
            && FacebookBroker.hasGrantedFBPublishPermission(WatchaPermission.PUBLISH))) {
          isShareFb = !isShareFb;
          setActivity();
        } else {
          FacebookBroker.getInstance()
              .canPublishAccessToken(getActivity())
              .subscribe(accessToken -> {
                SettingHelper.connectFb(getActivity(), accessToken.getToken(), fbTokenListener);
              });
        }
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.SWITCH_FACEBOOK_IN_COMMENT_EDITOR)
            .build());
        break;
    }
  }

  private DatePickerDialog getDatePickerDialog() {
    Calendar c = Calendar.getInstance();
    mDate = c.getTime();

    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    int day = c.get(Calendar.DAY_OF_MONTH);

    //if (mPickerDialog == null) {

      DatePickerDialog.OnDateSetListener mDateSetListener =
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              Calendar c = Calendar.getInstance();

              c.set(Calendar.YEAR, year);
              c.set(Calendar.MONTH, monthOfYear + 1);
              c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
              SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);

              format.setCalendar(c);
              mDate = c.getTime();
              setActivity();
            }
          };

      mPickerDialog = new DatePickerDialog(getActivity(), mDateSetListener, year, month, day);
    //} else {
    //  mPickerDialog.updateDate(year, month, day);
    //}

    mPickerDialog.getDatePicker().setMaxDate(mDate.getTime());

    return mPickerDialog;
  }

  //private DialogPlus getPrivacyDialog() {
  //  if (mPrivacyDialog == null) {
  //    String[] strs = new String[PrivacyLevel.values().length];
  //    for (int Loop1 = 0; Loop1 < PrivacyLevel.values().length; Loop1++) {
  //      strs[Loop1] = getString(PrivacyLevel.getPrivacyLevel(Loop1).getStringId());
  //    }
  //
  //    FakeActionBar header = new FakeActionBar(getActivity());
  //    header.setTitle(getString(R.string.privacy_title));
  //    ArrayAdapter<String> simpleAdapter = new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
  //    mPrivacyDialog = new DialogPlus.Builder(getActivity()).setHeader(header)
  //        .setContentHolder(new ListHolder())
  //        .setAdapter(simpleAdapter)
  //        .setGravity(DialogPlus.Gravity.CENTER)
  //        .setCancelable(true)
  //        .setOnItemClickListener(new OnItemClickListener() {
  //          @Override public void onItemClick(DialogPlus dialogPlus, Object o, View view, int i) {
  //            dialogPlus.dismiss();
  //
  //            //mPrivacy = PrivacyLevel.getPrivacyLevel(i - 1);
  //
  //            setActivity();
  //          }
  //        })
  //        .create();
  //  }
  //
  //  return mPrivacyDialog;
  //
  //}

  private void setUserAction() {
    isEnabled();
    if (!mRatingTextView.setUserAction(mUserAction)) {
      mPlzRating.setVisibility(View.VISIBLE);
      mRightArrowView.setVisibility(View.VISIBLE);
      mRatingTextView.setVisibility(View.GONE);
    } else {
      mPlzRating.setVisibility(View.GONE);
      mRightArrowView.setVisibility(View.GONE);
      mRatingTextView.setVisibility(View.VISIBLE);
    }

    if (mUserAction != null) {
      if (mUserAction.getComment() != null) {
        mWriteQueryType = QueryType.UPDATE_COMMENT.setApi(mUserAction.getComment().getCode());
        mCommentView.setText(mUserAction.getComment().getText());

        Date watchedAt = mUserAction.getComment().getWatchedAt();

        if (watchedAt != null) {
          mDate = watchedAt;
          //String[] array = watchedAt.split("-");
          //mYear = Integer.valueOf(array[0]);
          //mMonth = Integer.valueOf(array[1]);
          //mDay = Integer.valueOf(array[2]);
        }

        //PrivacyLevel privacyLevel = mUserAction.getComment().getPrivacyLevel();
        //if (privacyLevel != null) {
        //  mPrivacy = privacyLevel;
        //} else {
        //  mPrivacy = PrivacyLevel.ALL;
        //}

        setActivity();
      } else {
        mWriteQueryType = QueryType.WRITE_COMMENT;
      }

      if (mPopupRateManager != null) mPopupRateManager.setUserAction(mUserAction);
    } else {
      mDateView.setTextColor(getResources().getColor(R.color.light_gray));
      //mPrivacyView.setTextColor(getResources().getColor(R.color.light_gray));
    }

    mNumView.setText(String.valueOf(MAX_NUM - mCommentView.length()));
  }

  private void setActivity() {
    mActivity = "";
    clearActivityParams();

    if (mDate != null) {
      mDateView.setTextColor(getResources().getColor(R.color.pink));
      mActivity = RatingUtils.getHumanReadableDate(mDate) + "에";

      getActivityParams().put("watched_at",
          new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA).format(mDate));
    } else {
      mDateView.setTextColor(getResources().getColor(R.color.light_gray));
    }

    //if (mPrivacy != null) {
    //  mPrivacyView.setTextColor(getResources().getColor(R.color.pink));
    //  switch(mPrivacy) {
    //    case ALL:
    //      mPrivacyView.setText(R.string.icon_privacy_public);
    //      break;
    //
    //    case FRIENDS:
    //      mPrivacyView.setText(R.string.icon_friends);
    //      break;
    //
    //    case ONLY_ME:
    //      mPrivacyView.setText(R.string.icon_privacy_onlyme);
    //      break;
    //  }
    //
    //  getActivityParams().put("privacy_level", mPrivacy.toString().toLowerCase());
    //}

    if (isShareTwt) {
      mShareTwtView.setTextColor(getResources().getColor(R.color.pink));
    } else {
      mShareTwtView.setTextColor(getResources().getColor(R.color.light_gray));
    }

    PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.COMMENT_SHARE,
        PrefHelper.PrefKey.TWITTER.toString(), isShareTwt);
    getActivityParams().put("share_twitter", isShareTwt + "");

    if (mUserAction != null && mUserAction.getRating() > 0f) {
      mShareFbView.setVisibility(View.VISIBLE);
      if (isShareFb) {
        mShareFbView.setTextColor(getResources().getColor(R.color.pink));
      } else {
        mShareFbView.setTextColor(getResources().getColor(R.color.light_gray));
      }
    } else {
      mShareFbView.setVisibility(View.INVISIBLE);
    }

    PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.COMMENT_SHARE,
        PrefHelper.PrefKey.FACEBOOK.toString(), isShareFb);
    getActivityParams().put("share_facebook", isShareFb + "");

    if (!"".equals(mActivity)) {
      mActivity += " " + getString(R.string.watched);
    }

    mActivityView.setText(mActivity);
  }

  private void clearActivityParams() {
    mActivityParams = null;
  }

  private Map<String, String> getActivityParams() {
    if (mActivityParams == null) {
      mActivityParams = new HashMap<>();
    }

    return mActivityParams;
  }

  @Override public void onViewCreated(View view, final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(view);

    getToolbarHelper().setToolbarTitle(null, null);

    mValidator.registerValidator(mCommentView, this);

    ((View) mPlzRating.getParent()).setOnClickListener(this);

    mCommentLayer.setOnClickListener(this);

    mDateView.setOnClickListener(this);
    //mPrivacyView.setOnClickListener(this);
    mShareTwtView.setOnClickListener(this);
    mShareFbView.setOnClickListener(this);
    mCommentLayer.setOnClickListener(this);

    mWriteCommentLayer.setVisibility(View.GONE);

    mLoadingView.setVisibility(View.VISIBLE);
    mLoadingView.onLoading();

    //FB Publish 권한을 따로 요구해야하기 때문에 페북연동되어 있더라도 isShareFB = false
    /*if (WatchaApp.getUser().isFbConnected())
      isShareFb = PrefHelper.getBoolean(getActivity(), PrefHelper.PrefType.COMMENT_SHARE, PrefHelper.PrefKey.FACEBOOK.toString(), true);
    else
      isShareFb = false;*/
    isShareFb = WatchaApp.getUser().isFbConnected()
        && PrefHelper.getBoolean(getActivity(), PrefHelper.PrefType.COMMENT_SHARE, PrefHelper.PrefKey.FACEBOOK
        .toString(), true);

    isShareTwt = WatchaApp.getUser().isTwtConnected()
        && PrefHelper.getBoolean(getActivity(), PrefHelper.PrefType.COMMENT_SHARE, PrefHelper.PrefKey.TWITTER
        .toString(), false);

    FontHelper.RobotoCondensed(mNumView);

    requestData(getDataProvider());
  }

  @Override public MenuItems getToolbarMenuItems() {
    return null;
  }

  @Override public void onMenuItemSet(Menu menu) {
    super.onMenuItemSet(menu);

    MenuItem item =
        menu.add(0, MenuSet.ICON_SHARE.getMenuId(), 0, MenuSet.ICON_SHARE.getXmlResId());
    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    View view = LayoutInflater.from(getActivity()).inflate(R.layout.action_menu, null);
    item.setActionView(view);
    mCompleteView = (TextView) view.findViewById(R.id.action_menu);
    mCompleteView.setOnClickListener(this);
  }

  private DataProvider<BaseResponse<RatingData>> getCommentDataProvider() {
    if (mCommentDataProvider == null) {
      mCommentDataProvider = new DataProvider<BaseResponse<RatingData>>(getActivity(),
          mWriteQueryType, createReferrerBuilder()).withDialogMessage(getString(R.string.saving_comment))
          .withParams(getActivityParams())
          .responseTo(getWriteCommentListener());
    }
    return mCommentDataProvider;
  }

  private OnBaseRatingResponseListener mWriteCommentListener;

  private OnBaseRatingResponseListener getWriteCommentListener() {
    if (mWriteCommentListener == null) {
      mWriteCommentListener = new OnBaseRatingResponseListener(getActivity()) {
        @Override public void onSuccess(@NonNull QueryType queryType,
            @NonNull BaseResponse<RatingData> result) {
          super.onSuccess(queryType, result);
          getBaseActivity().finish();
        }
      };
    }

    mWriteCommentListener.setDefaultUserAction(mCategoryType.getApiPath(), mUserAction);

    return mWriteCommentListener;
  }

  private Content mContent = null;

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull final BaseResponse<ContentData> result) {
    super.onSuccess(queryType, result);

    mContent = result.getData().getContent();

    mCategoryType = CategoryType.getCategory(mContent.getCategory());
    mCode = mContent.getCode();
    mTitle = mContent.getTitle();
    setToolbarTitle(mTitle, null);

    mUserAction = mContent.getUserAction(WatchaApp.getUser().getCode());

    setUserAction();

    mListener = new OnBaseRatingResponseListener(getActivity()) {

      @Override public void onSuccess(@NonNull QueryType queryType,
          @NonNull BaseResponse<RatingData> result) {
        super.onSuccess(queryType, result);

        RatingData data = result.getData();

        mUserAction = CacheManager.getMyUserAction(mCode);
        setUserAction();
      }
    };

    mPopupRateManager =
        new WPopupRateManager.Builder(getActivity(), WPopupType.WRITE_COMMENT).setTitle(mTitle)
            .setSubtitle(mContent.getDescription())
            .setUserAction(mUserAction)
            .setUserActionPopupListener(new UserActionPopupListener() {
              @Override public void onChangedRating(float rating) {
                super.onChangedRating(rating);
                if (rating > 0) {
                  if (mUserAction != null && rating == mUserAction.getRating()) {
                    // 같은 별점을 클릭했으면 취소.
                    requestRating(UserActionOperator.ActionType.RATING_CANCEL, null);
                  } else {
                    // 별점 매김 혹은 변경.
                    requestRating(UserActionOperator.ActionType.RATING, String.valueOf(rating));
                  }
                } else {
                  // 별 0은 취소.
                  requestRating(UserActionOperator.ActionType.RATING_CANCEL, null);
                }
              }

              @Override public void onClickWish() {
                super.onClickWish();
                requestWish();
              }

              @Override public void onClickMeh() {
                super.onClickMeh();
                requestMeh();
              }
            })
            .build();

    setActivity();

    mLoadingView.setVisibility(View.GONE);
    mWriteCommentLayer.setVisibility(View.VISIBLE);

    Util.openKeyboard(mCommentView);
  }

  protected void requestRating(UserActionOperator.ActionType actionType,
      @Nullable String ratingValue) {
    UserActionOperator.Builder builder =
        new UserActionOperator.Builder(getActivity(), actionType).setContentCode(mContent.getCode())
            .setCategoryType(CategoryType.getCategory(mContent.getCategory()))
            .setReleasedAt(mContent.getReleasedAt());
    if (NumberUtils.isNumber(ratingValue)) {
      builder.setParams(Collections.singletonMap("value", ratingValue));
    }
    builder.setRatingResponseListener(new OnBaseRatingResponseListener(getActivity()));
    builder.build().request(new Action1<RatingData>() {
      @Override public void call(RatingData ratingData) {
        mUserAction = CacheManager.getMyUserAction(mCode);
        setUserAction();
        setActivity();
      }
    });
  }

  protected void requestWish() {
    if (mUserAction == null || !mUserAction.isWished()) {
      requestRating(UserActionOperator.ActionType.WISH, null);
    } else {
      requestRating(UserActionOperator.ActionType.WISH_CANCEL, null);
    }
  }

  protected void requestMeh() {
    if (mUserAction == null || !mUserAction.isMehed()) {
      requestRating(UserActionOperator.ActionType.MEH, null);
    } else {
      requestRating(UserActionOperator.ActionType.MEH_CANCEL, null);
    }
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return new OnBackKeyListener() {
      @Override public boolean onBackKeyDown() {
        Util.closeKeyboard(mCommentView);
        return true;
      }
    };
  }

  @Override public void onValidation(FormValidator.FormStyle style, EditText v, String text,
      boolean isValid) {

    switch (style) {
      case COMMENT:
        mNumView.setText((style.getMaxCount() - text.length()) + "");
        isTextLengthEnabled = (style.getMaxCount() - text.length() >= 0);
        isEnabled();
        break;
    }
  }

  private void isEnabled() {
    if (mUserAction == null) {
      mCompleteView.setTextColor(getResources().getColor(R.color.light_purple));
      WLog.e("rating");
      return;
    }

    if (mUserAction.getRating() == 0 && !mUserAction.isWished() && !mUserAction.isMehed()) {
      mCompleteView.setTextColor(getResources().getColor(R.color.light_purple));
      return;
    }

    if (mCommentView.getText().length() == 0) {
      mCompleteView.setTextColor(getResources().getColor(R.color.light_purple));
      WLog.e("nothing");
      return;
    }

    if (!isTextLengthEnabled) {
      mCompleteView.setTextColor(getResources().getColor(R.color.light_purple));
      WLog.e("length");
      return;
    }

    WLog.e("ok");
    mCompleteView.setTextColor(getResources().getColor(R.color.white));
  }
}
