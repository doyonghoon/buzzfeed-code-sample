package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.RatingListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.widget.IconActionGridButton;
import java.util.HashMap;
import java.util.Map;

public class RatingFragment extends ListFragment<RatingListData> {

  private String mCategoryId;
  private CategoryType mCategoryType;
  private IconActionGridButton mCategoryView;
  private View mRatingView;

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override protected QueryType getQueryType() {
    return QueryType.EVALUATION.setApi(mCategoryType.getApiPath());
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setDisplayHomeAsUpEnabled(false);
    getToolbarHelper().getActionbar().setDisplayUseLogoEnabled(false);
    getToolbarHelper().getActionbar().setDisplayShowHomeEnabled(false);
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> map = super.getRequestDataParams();

    if (mCategoryId != null) {
      if (map == null) map = new HashMap<>();

      map.put("id", mCategoryId);

      return map;
    }

    return super.getRequestDataParams();
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }

    if (bundle.containsKey(BundleSet.CATEGORY_ID)) {
      mCategoryId = bundle.getString(BundleSet.CATEGORY_ID);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if (mCategoryType != null) {
      outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());
    }
    if (!TextUtils.isEmpty(mCategoryId)) {
      outState.putString(BundleSet.CATEGORY_ID, mCategoryId);
    }
  }

  private View getRatingView() {
    if (mRatingView == null) {
      mRatingView = LayoutInflater.from(getActivity()).inflate(R.layout.undertoolbar_rating, null);
      mCategoryView = ButterKnife.findById(mRatingView, R.id.layout_single_button);
      mCategoryView.setTextNormalColor(getResources().getColor(R.color.gray));
      mCategoryView.setIcon(getString(R.string.icon_arrow_spinner));
      mCategoryView.setIconColor(getResources().getColor(R.color.skyblue));
      mCategoryView.setText(null);
      mRatingView.setOnClickListener(v -> ActivityStarter.with(getActivity(), FragmentTask.SELECT_RATING_CATEGORY)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .setRequestCode(ActivityStarter.SELECT_EVALUATE_CATEGORY_REQUEST)
          .addBundle(new BundleSet.Builder().putCategoryType(mCategoryType).build().getBundle())
          .start());
    }

    return mRatingView;
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case ActivityStarter.SELECT_EVALUATE_CATEGORY_REQUEST:
        if (resultCode == Activity.RESULT_OK && data != null && data.hasExtra(
            BundleSet.CATEGORY_ID)) {
          mCategoryId = data.getStringExtra(BundleSet.CATEGORY_ID);
          refresh();
        }
        break;
    }
  }

  /**
   * {@link TutorialFragment} 화면이 오버라이드 해서 false 로 리턴함.
   * 왜냐면 튜토리얼 화면은 하단에 붙는 카테고리 선택 뷰가 필요 없으니까.
   */
  protected boolean attachRatingView() {
    return true;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void load() {
    super.load();
    if (attachRatingView()) {
      addUnderToolbarView(getRatingView());
    }
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void unload() {
    super.unload();
    if (attachRatingView()) {
      removeUnderToolbarView(getRatingView());
    }
  }

  @Nullable
  @AnalyticsManager.ScreenNameType
  public String getCurrentScreenName() {
    String result = null;
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          result = AnalyticsManager.RATE_MORE_MOVIE;
          break;
        case TV_SEASONS:
          result =AnalyticsManager.RATE_MORE_DRAMA;
          break;
      }
    }
    @AnalyticsManager.ScreenNameType String screenName = new ReferrerBuilder(result).appendArgument(mCategoryId).toString();
    return screenName;
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull BaseResponse<RatingListData> result) {
    super.onSuccess(queryType, result);
    if (result.getData() != null) {
      mCategoryView.setText(result.getData().getItemCategory().getName());
    }
  }
}
