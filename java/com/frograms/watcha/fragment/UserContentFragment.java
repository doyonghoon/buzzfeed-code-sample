package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.listeners.OnCountListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.PrivacyLevel;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.ActionCount;
import com.frograms.watcha.model.response.data.list.ActionCountsListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.HashMap;
import java.util.Map;

/**
 * 보고싶어요, 봤어요 목록
 */
public class UserContentFragment extends ListFragment<ActionCountsListData>
    implements EmptyViewPresenter {

  /**
   * UserActionType == RATE 일때 이걸 씀.
   *
   * @see #getSort(UserActionType)
   */
  private static final Sort[] SORTS_RATE = {
      new Sort("", "담은 순"), new Sort("average", "평균 별점 순"), new Sort("abc", "가나다 순"),
      new Sort("latest", "최신 작품 순"), new Sort("earliest", "고전 작품 순")
  };

  /**
   * UserActionType == WISH 일때 이걸 씀.
   *
   * @see #getSort(UserActionType)
   */
  private static final Sort[] SORTS_WISH = {
      new Sort("", "담은 순"), new Sort("predicted", "예상별점 순"), new Sort("average", "평균 별점 순"),
      new Sort("abc", "가나다 순"), new Sort("latest", "최신 작품 순"), new Sort("earliest", "고전 작품 순")
  };

  /**
   * UserActionType == COMMENT 일때만 이걸 씀.
   *
   * @see #getSort(UserActionType)
   */
  private static final Sort[] MY_SORTS_COMMENT = {
      new Sort("", "작성 순"), new Sort("average", "평균 별점 순"), new Sort("abc", "가나다 순"),
      new Sort("latest", "최신 작품 순"), new Sort("earliest", "고전 작품 순"), new Sort("best", "내 별점 높은 순"),
      new Sort("worst", "내 별점 낮은 순")
  };

  private static final Sort[] SORTS_COMMENT = {
      new Sort("", "작성 순"), new Sort("best", "이 회원의 별점 높은 순"), new Sort("worst", "이 회원의 별점 낮은 순"),
      new Sort("average", "평균 별점 순"), new Sort("abc", "가나다 순"), new Sort("latest", "최신 작품 순"),
      new Sort("earliest", "고전 작품 순")
  };

  private QueryType mQueryType = QueryType.USER_ACTIONED_CONTENT;

  private String mUserCode;
  private CategoryType mCategoryType;
  private Map<String, ActionCount> mActionCounts;
  private UserActionType mUserActionType = UserActionType.RATE;
  private int mCount;
  private String mContentRating = null;
  private boolean mShouldLoadUserActionCount = false;
  private boolean isAttachedToTabPager = false;
  private boolean mShouldShowSortUnderbarView = false;
  private Map<String, String> mParams = new HashMap<>();
  private String mSortParam = null;
  private View mSortView = null;
  private IconActionGridButton mSortTitleTextView = null;
  private PrivacyLevel mPrivacyLevel = PrivacyLevel.ALL;

  private OnCountListener onCountListener;

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  public void setOnCountListener(OnCountListener listener) {
    onCountListener = listener;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    //getToolbarHelper().getActionbar().setTitle(null);
    //getToolbarHelper().getToolbar().setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.USER_CODE)) {
      mUserCode = bundle.getString(BundleSet.USER_CODE);
    } else {
      mUserCode = WatchaApp.getUser().getCode();
    }

    if (bundle.containsKey(BundleSet.USER_ACTION_TYPE)) {
      mUserActionType = UserActionType.getUserActionType(bundle.getInt(BundleSet.USER_ACTION_TYPE));
    }

    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }

    if (bundle.containsKey(BundleSet.USER_ACTION_COUNT)) {
      mCount = bundle.getInt(BundleSet.USER_ACTION_COUNT);
    }

    if (bundle.containsKey(BundleSet.LOAD_ACTION_COUNT)) {
      mShouldLoadUserActionCount = bundle.getBoolean(BundleSet.LOAD_ACTION_COUNT);
    }

    if (bundle.containsKey(BundleSet.IS_ATTACHED_TO_TAB_PAGER)) {
      isAttachedToTabPager = bundle.getBoolean(BundleSet.IS_ATTACHED_TO_TAB_PAGER);
    }

    if (bundle.containsKey(BundleSet.CONTENT_RATING)) {
      mContentRating = bundle.getString(BundleSet.CONTENT_RATING);
    }

    if (bundle.containsKey(BundleSet.SHOW_SORT)) {
      mShouldShowSortUnderbarView = bundle.getBoolean(BundleSet.SHOW_SORT);
    }

    if (bundle.containsKey(BundleSet.PRIVACY_LEVEL_INT)) {
      mPrivacyLevel = PrivacyLevel.getPrivacyLevel(bundle.getInt(BundleSet.PRIVACY_LEVEL_INT));
    }
  }

  public UserActionType getUserActionType() {
    return mUserActionType;
  }

  public String getContentRating() {
    return mContentRating;
  }

  public String getUserCode() {
    return mUserCode;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.USER_CODE, mUserCode);
    if (mUserActionType != null) {
      outState.putInt(BundleSet.USER_ACTION_TYPE, mUserActionType.getTypeInt());
    }
    if (mCategoryType != null) outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());

    outState.putInt(BundleSet.USER_ACTION_COUNT, mCount);
    outState.putBoolean(BundleSet.LOAD_ACTION_COUNT, mShouldLoadUserActionCount);
    outState.putBoolean(BundleSet.IS_ATTACHED_TO_TAB_PAGER, isAttachedToTabPager);
    outState.putString(BundleSet.CONTENT_RATING, mContentRating);
    outState.putBoolean(BundleSet.SHOW_SORT, mShouldShowSortUnderbarView);

    if (mPrivacyLevel != null) {
      outState.putInt(BundleSet.PRIVACY_LEVEL_INT, mPrivacyLevel.getTypeInt());
    }
  }

  @Override protected QueryType getQueryType() {
    if (TextUtils.isEmpty(mContentRating)) {
      String path = mUserActionType.getUserContentType();
      boolean hasCateogyrField = path.contains("%s");
      if (hasCateogyrField) {
        // 카테고리 값을 삽입할 곳이 있으면 추가.
        path = String.format(path, mCategoryType.getApiPath());
      }
      return mQueryType.setApi(mUserCode, path);
    } else {
      return mQueryType.setApi(mUserCode,
          "ratings/" + mCategoryType.getApiPath() + "/" + mContentRating);
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (mUserActionType != null) {
      StringBuilder builder = new StringBuilder();

      if (mCategoryType != null && !isAttachedToTabPager) {
        builder.append(getCategoryTitle(mCategoryType)).append(" ").append(mCount);
      } else {
        builder.append(UserContentTabFragment.getTitle(getActivity(), mUserActionType));
      }
      //getToolbarHelper().getActionbar().setTitle(builder.toString());
    }
  }

  @Override public void load() {
    super.load();
    if (mShouldShowSortUnderbarView) {
      addUnderToolbarView(
          mUserActionType == UserActionType.DECK ? getDeckUnderbarView() : getSortView());
    }
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void unload() {
    super.unload();
    if (mShouldShowSortUnderbarView) {
      removeUnderToolbarView(
          mUserActionType == UserActionType.DECK ? getDeckUnderbarView() : getSortView());
    }
  }

  @Nullable
  @AnalyticsManager.ScreenNameType
  public String getCurrentScreenName() {
    @AnalyticsManager.ScreenNameType String result = null;
    if (mUserActionType != null && mCategoryType != null) {
      switch (mUserActionType) {
        case RATE:
          if (mCategoryType == CategoryType.MOVIES) {
            result = AnalyticsManager.CONTENT_WATCHED_MOVIE;
          } else if (mCategoryType == CategoryType.TV_SEASONS) {
            result = AnalyticsManager.CONTENT_WATCHED_DRAMA;
          }
          break;
        case COMMENT:
          if (mCategoryType == CategoryType.MOVIES) {
            result = AnalyticsManager.CONTENT_WRITTEN_MOVIE_COMMENT;
          } else if (mCategoryType == CategoryType.TV_SEASONS) {
            result = AnalyticsManager.CONTENT_WRITTEN_DRAMA_COMMENT;
          } else if (mCategoryType == CategoryType.BOOKS) {
            result = AnalyticsManager.BOOK;
          }
          break;
        case WISH:
          if (mCategoryType == CategoryType.MOVIES) {
            result = AnalyticsManager.CONTENT_WISHED_MOVIE;
          } else if (mCategoryType == CategoryType.TV_SEASONS) {
            result = AnalyticsManager.CONTENT_WISHED_DRAMA;
          } else if (mCategoryType == CategoryType.BOOKS) {
            result = AnalyticsManager.BOOK;
          }
          break;
        case DECK:
          result = AnalyticsManager.CONTENT_ONES_DECKS_FOR_MOVIE;
          break;
        case MEH:
          result = AnalyticsManager.CONTENT_MEH_FOR_MOVIES;
          break;
      }
    }
    if (!TextUtils.isEmpty(result)) {
      @AnalyticsManager.ScreenNameType String referer =
          new ReferrerBuilder(result).appendArgument(mUserCode).toString();
      return referer;
    }
    return result;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    if (!TextUtils.isEmpty(mSortParam)) {
      mParams.put("sort", mSortParam);
    } else if (TextUtils.isEmpty(mSortParam) && mParams.containsKey("sort")) {
      mParams.remove("sort");
    }
    return mParams;
  }

  private View getSortView() {
    if (mSortView == null) {
      mSortView = LayoutInflater.from(getActivity()).inflate(R.layout.layer_user_content, null);
      mSortTitleTextView = ButterKnife.findById(mSortView, R.id.layout_single_button);
      mSortTitleTextView.setTextNormalColor(getResources().getColor(R.color.heavy_gray));
      mSortTitleTextView.getIcon().setTextColor(getResources().getColor(R.color.skyblue));
      mSortTitleTextView.setIcon(getString(R.string.icon_arrow_spinner));
      if (mUserActionType != null) {
        mSortTitleTextView.setText(mUserActionType == UserActionType.COMMENT ? "작성 순" : "담은 순");
      }
      mSortTitleTextView.setIconColor(getResources().getColor(R.color.skyblue));
      mSortView.setOnClickListener(v -> {
        final Sort[] sorts = getSort(mUserActionType);
        if (sorts != null) {
          final String[] sortDisplayNames = new String[sorts.length];
          for (int i = 0; i < sorts.length; i++) {
            sortDisplayNames[i] = sorts[i].getDisplayName();
          }
          FakeActionBar header = new FakeActionBar(getActivity());
          header.setTitle("정렬");
          ArrayAdapter<String> simpleAdapter =
              new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1,
                  sortDisplayNames);
          DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
              .setContentHolder(new ListHolder())
              .setAdapter(simpleAdapter)
              .setGravity(DialogPlus.Gravity.BOTTOM)
              .setCancelable(true)
              .setOnItemClickListener((dialogPlus, o, view, i) -> {
                dialogPlus.dismiss();
                mSortTitleTextView.setText(sortDisplayNames[i - 1]);
                mSortParam = sorts[i - 1].getCode();
                AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, getAnalyticsActionType(mUserActionType))
                    .setLabel(mSortParam)
                    .build());
                refresh();
              })
              .create();
          dialog.show();
        }
      });
    }
    return mSortView;
  }

  @Nullable
  private AnalyticsActionType getAnalyticsActionType(@Nullable UserActionType userActionType) {
    if (userActionType != null) {
      switch (userActionType) {
        case COMMENT:
          return AnalyticsActionType.SORT_IN_COMMENT;
        case RATE:
          return AnalyticsActionType.SORT_IN_RATE;
        case WISH:
          return AnalyticsActionType.SORT_IN_WISH;
      }
    }
    return null;
  }

  private View mDeckUnderbarView = null;

  private View getDeckUnderbarView() {
    if (mDeckUnderbarView == null) {
      mDeckUnderbarView = LayoutInflater.from(getActivity()).inflate(R.layout.layer_deck, null);
      IconActionGridButton v = ButterKnife.findById(mDeckUnderbarView, R.id.layout_single_button);
      v.getIcon().setTextColor(getResources().getColor(R.color.skyblue));
      v.setTextNormalColor(getResources().getColor(R.color.heavy_gray));
      mDeckUnderbarView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          ActivityStarter.with(getActivity(), FragmentTask.CREATE_DECK)
              .setRequestCode(ActivityStarter.CREATE_DECK_REQUEST)
              .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
              .start();
        }
      });
    }
    return mDeckUnderbarView;
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == ActivityStarter.CREATE_DECK_REQUEST && resultCode == Activity.RESULT_OK) {
      refresh();
    }
  }

  private int getActionCount(UserActionType type, ActionCount count) {
    switch (type) {
      case WISH:
        return count.getWishes();
      case COMMENT:
        return count.getWishes();
      case MEH:
        return count.getMehs();
      case RATE:
        return count.getRatings();
      default:
        return 0;
    }
  }

  private String getCategoryTitle(@Nullable CategoryType type) {
    if (type == null) {
      return null;
    }

    switch (type) {
      case MOVIES:
        return getString(R.string.movie);
      case BOOKS:
        return getString(R.string.book);
      case TV_SEASONS:
        return getString(R.string.drama);
      default:
        return null;
    }
  }

  private Sort[] getSort(UserActionType type) {
    if (type == null) {
      return null;
    }
    switch (type) {
      case COMMENT:
        return isMe() ? MY_SORTS_COMMENT : SORTS_COMMENT;
      case WISH:
        return SORTS_WISH;
      case RATE:
        return SORTS_RATE;
      default:
        return null;
    }
  }

  private User mUser = null;

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull BaseResponse<ActionCountsListData> result) {
    super.onSuccess(queryType, result);
    if (result.getData() != null) {
      String title = result.getData().getTitle();
      String subtitle = result.getData().getSubtitle();
      WLog.i("title: " + title + ", subtitle: " + subtitle);
      if (!TextUtils.isEmpty(title)) {
        getToolbarHelper().getActionbar().setTitle(title);
      } else {
        getToolbarHelper().getActionbar()
            .setTitle(UserContentTabFragment.getTitle(getActivity(), mUserActionType));
      }
      if (!TextUtils.isEmpty(subtitle)) {
        getToolbarHelper().getActionbar().setTitle(subtitle);
      }

      refreshEmptyListView();

      if (result.getData().getActionCounts() != null) {

        if (mUserCode.equals(WatchaApp.getUser().getCode())) {
          WatchaApp.getUser().setActionCounts(result.getData().getActionCounts());
        }
        mActionCounts = result.getData().getActionCounts();

        if (onCountListener != null) setCount();
      }

      //if (result.getData() instanceof UserListData && ((UserListData) result.getData()).getUser() != null) {
      //  UserListData userData = (UserListData) result.getData();
      //  mUser = userData.getUser();
      //  if (mUser != null) {
      //    switch (mUser.getPrivacyLevel()) {
      //      case ALL:
      //        break;
      //      case FRIENDS:
      //        if (!mUser.isFriend() && mAdapter != null) {
      //          mAdapter.clearItems();
      //          notifyList();
      //          setEmptyViewVisibility(View.VISIBLE);
      //        }
      //        break;
      //      case ONLY_ME:
      //        if (mAdapter != null) {
      //          mAdapter.clearItems();
      //          notifyList();
      //          setEmptyViewVisibility(View.VISIBLE);
      //        }
      //        break;
      //    }
      //  }
      //}
    }
  }

  @Override public View getEmptyView() {
    if (mUserActionType != null) {
      switch (mUserActionType) {
        case RATE:
          SimpleEmptyView rateEmptyView = null;
          // category, isMe, privacy_level
          if (mCategoryType == CategoryType.MOVIES) {
            rateEmptyView = new SimpleEmptyView(getActivity(), R.string.icon_movie,
                isMe() ? "에이, 설마 하나도 안 보셨겠어요?\n이 곳을 눌러 평가를 늘려보세요"
                    : getEmptyMessage(mUser, mPrivacyLevel, mCategoryType, UserActionType.RATE));
          } else if (mCategoryType == CategoryType.TV_SEASONS) {
            rateEmptyView = new SimpleEmptyView(getActivity(), R.string.icon_tv,
                isMe() ? "에이, 설마 하나도 안 보셨겠어요?\n이 곳을 눌러 평가를 늘려보세요"
                    : getEmptyMessage(mUser, mPrivacyLevel, mCategoryType, UserActionType.RATE));
          }
          if (rateEmptyView != null && isMe()) {
            rateEmptyView.getIconView().setOnClickListener(new EmptyClickListener(mCategoryType));
            rateEmptyView.getTextView().setOnClickListener(new EmptyClickListener(mCategoryType));
          }
          return rateEmptyView;
        case WISH:
          return new SimpleEmptyView(getActivity(), R.string.icon_wish,
              isMe() ? "마음에 드는 작품에 보고싶어요를 눌러 담아 놓으세요 :)" : "보고싶어요한 컨텐츠가 없어요");
        case MEH:
          return new SimpleEmptyView(getActivity(), R.string.icon_no, "관심없어요한 작품이 없어요");
        case COMMENT:
          return new SimpleEmptyView(getActivity(), R.string.icon_comment,
              isMe() ? "아직 작성하신 코멘트가 없어요.\n감동이 사라지기 전에 글로 추억을 남겨보세요" : "작성한 코멘트가 없어요");
        case DECK:
          return new SimpleEmptyView(getActivity(), R.string.icon_collection,
              isMe() ? "컬렉션이 없어요\n회원님만의 컬렉션을 만들어 보세요 :)" : "컬렉션이 없어요");
      }
    }
    return null;
  }

  private boolean isMe() {
    return WatchaApp.getUser() != null && mUserCode != null && mUserCode.equals(
        WatchaApp.getUser().getCode());
  }

  private String getEmptyMessage(User user, PrivacyLevel privacyLevel, CategoryType categoryType,
      UserActionType userActionType) {
    switch (privacyLevel) {
      case FRIENDS:
        if (userActionType == UserActionType.RATE && user != null) {
          return String.format("%s님이 평가를 친구공개 하셨어요!",
              (user.getName() != null ? user.getName() : ""));
        } else if (userActionType == UserActionType.WISH && user != null) {
          return String.format("%s님이 보고싶어요를 친구공개 하셨어요!",
              (user.getName() != null ? user.getName() : ""));
        }
      case ONLY_ME:
        if (userActionType == UserActionType.RATE && user != null) {
          return String.format("%s님이 평가를 비공개 하셨어요!",
              (user.getName() != null ? user.getName() : ""));
        } else if (userActionType == UserActionType.WISH && user != null) {
          return String.format("%s님이 보고싶어요를 비공개 하셨어요!",
              (user.getName() != null ? user.getName() : ""));
        }
      case ALL:
        if (userActionType == UserActionType.RATE) {
          return String.format("본 %s가 없어요",
              categoryType != null ? getString(categoryType.getStringId()) : "");
        } else if (userActionType == UserActionType.WISH) {
          return String.format("보고싶어요한 %s가 없어요",
              categoryType != null ? getString(categoryType.getStringId()) : "");
        }
    }
    return null;
  }

  public static class Sort {

    private String mCode;
    private String mDisplayName;

    Sort(String code, String displayName) {
      mCode = code;
      mDisplayName = displayName;
    }

    Sort(Context context, String code, @StringRes int displayNameResId) {
      this(code, context.getString(displayNameResId));
    }

    public String getCode() {
      return mCode;
    }

    public String getDisplayName() {
      return mDisplayName;
    }
  }

  private static class EmptyClickListener implements View.OnClickListener {

    private CategoryType mType;

    private EmptyClickListener(CategoryType type) {
      mType = type;
    }

    @Override public void onClick(View v) {
      Bundle bundle =
          new BundleSet.Builder().putSelectedTab(getTabIndex(mType)).build().getBundle();
      ActivityStarter.with(v.getContext(), FragmentTask.RATING_TAB)
          .addBundle(bundle)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .start();
    }

    private int getTabIndex(@NonNull CategoryType type) {
      switch (type) {
        case MOVIES:
          return 0;
        case TV_SEASONS:
          return 1;
      }
      return 0;
    }
  }

  @Override public void notifyList() {
    super.notifyList();
    if (mUserCode.equals(WatchaApp.getUser().getCode())) {
      mActionCounts = WatchaApp.getUser().getActionCounts();
      if (onCountListener != null) setCount();

      switch (mUserActionType) {
        case RATE:
          if (mContentRating == null) {
            int count = mActionCounts.get(mCategoryType.getApiPath()).getRatings();
            getToolbarHelper().getActionbar()
                .setTitle(String.format("%s %d", getString(mCategoryType.getStringId()), count));
          }
          break;
      }
    }
  }

  private void setCount() {
    int movieCount, dramaCount;

    switch (mUserActionType) {
      case COMMENT:
        movieCount = mActionCounts.get(CategoryType.MOVIES.getApiPath()).getComments();
        dramaCount = mActionCounts.get(CategoryType.TV_SEASONS.getApiPath()).getComments();
        onCountListener.onCount(movieCount, dramaCount);
        break;

      case WISH:
        movieCount = mActionCounts.get(CategoryType.MOVIES.getApiPath()).getWishes();
        dramaCount = mActionCounts.get(CategoryType.TV_SEASONS.getApiPath()).getWishes();
        onCountListener.onCount(movieCount, dramaCount);
        break;
    }
  }
}
