package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import java.util.Map;

public class SelectContentFragment extends ListFragment implements
    RecyclerAdapter.OnClickCardItemListener, EmptyViewPresenter {

  private QueryType mQueryType = QueryType.SELECT_CONTENT_IN_COMMENT;

  private CategoryType mCategoryType;
  private String mQuery;

  private SimpleEmptyView mEmptyView;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());
  }

  public void setQuery(String query) {
    mQuery = query;
  }

  @Override protected QueryType getQueryType() {
    return mQueryType;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> map = super.getRequestDataParams();

    if (mCategoryType != null) {
      map.put("category", mCategoryType.getApiPath());
    }

    if (mQuery != null) {
      map.put("query", mQuery);
    }

    return map;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mAdapter.setOnClickCardItemListener(this);
  }

  @Override public void load() {
    super.load();
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @AnalyticsManager.ScreenNameType
  public String getCurrentScreenName() {
    String result = null;
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          result = AnalyticsManager.ADD_MOVIE_FROM_CREATE_DECK;
          break;
        case TV_SEASONS:
          result = AnalyticsManager.ADD_DRAMA_FROM_CREATE_DECK;
          break;
        case BOOKS:
          result = AnalyticsManager.BOOK;
          break;
      }
    }
    @AnalyticsManager.ScreenNameType String screenName =
        new ReferrerBuilder(result).appendArgument(mCategoryType.getApiPath()).toString();
    return screenName;
  }

  @Override public void onClickCardItem(ItemView v, Item item) {
    Content content;
    if (item instanceof Content) {
      content = (Content) item;
    } else {
      return;
    }

    UserAction userAction = CacheManager.getMyUserAction(content.getCode());
    if (userAction != null && userAction.getComment() != null) {
      ActivityStarter.with(getActivity(), FragmentTask.WRITE_COMMENT)
          .addBundle(new BundleSet.Builder().putCommentCode(userAction.getComment().getCode())
              .putPreviousScreenName(getCurrentScreenName())
              .build()
              .getBundle())
          .start();
    } else {
      ActivityStarter.with(getActivity(), FragmentTask.WRITE_COMMENT)
          .addBundle(new BundleSet.Builder().putContentCode(content.getCode())
              .putPreviousScreenName(getCurrentScreenName())
              .build()
              .getBundle())
          .start();
    }
    getActivity().finish();
  }

  @Override public View getEmptyView() {
    if (mCategoryType != null) {
      if (mEmptyView == null) {
        mEmptyView = new SimpleEmptyView(getActivity(), getIconResId(mCategoryType),
            String.format("평가한 %s가 없어요.", getString(mCategoryType.getStringId())));
      }

      return mEmptyView;
    }
    return null;
  }

  @StringRes private int getIconResId(CategoryType type) {
    switch (type) {
      case TV_SEASONS:
        return R.string.icon_tv;
      case MOVIES:
        return R.string.icon_movie;
    }
    return 0;
  }

  private boolean needToRetry = false;

  public void setRetry(boolean retry) {
    needToRetry = retry;
  }

  @Override public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse result) {
    super.onSuccess(queryType, result);
    if (mQuery == null || mQuery.isEmpty()) {
      ((SimpleEmptyView) getEmptyView()).getTextView()
          .setText(String.format("평가한 %s가 없어요.", getString(mCategoryType.getStringId())));
    } else {
      ((SimpleEmptyView) getEmptyView()).getTextView().setText("검색 결과가 없습니다. 다른 검색어를 입력해보세요.");
    }

    if (needToRetry) {
      refresh();
      needToRetry = false;
    }
  }
}
