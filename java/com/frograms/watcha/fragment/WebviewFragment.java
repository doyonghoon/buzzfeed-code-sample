package com.frograms.watcha.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import butterknife.Bind;
import com.crashlytics.android.Crashlytics;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.GridDrawerAdapter;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.DetailFragment.ShareDataWrapper;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.helpers.WGuinnessHeader;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.items.GeneralShareData;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.ShareMessageData;
import com.frograms.watcha.models.ShareApp;
import com.frograms.watcha.observablescrollview.ObservableWebView;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.ShareAppLoader;
import com.frograms.watcha.utils.TwitterBroker;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.LoadingView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.views.FakeActionBar;
import com.google.android.gms.analytics.HitBuilders;
import com.kakao.AppActionBuilder;
import com.kakao.KakaoLink;
import com.kakao.KakaoParameterException;
import com.kakao.KakaoTalkLinkMessageBuilder;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;

/**
 * 웹 페이지 보여주는 화면.
 */
public class WebviewFragment extends BaseFragment {

  @Bind(R.id.webview) public ObservableWebView mWebview;
  @Bind(R.id.web_loading) public LoadingView mLoadingView;

  private Map<String, String> mHeaders = null;
  private String mTitle = null;
  public String mLink = null;

  private boolean isTaste = false;
  private String mUserName, mUserCode;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_webview;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.WEB_URL)) {
      mLink = bundle.getString(BundleSet.WEB_URL);
      WLog.e("link: " + mLink);
      if (!TextUtils.isEmpty(mLink) && mLink.contains("/taste")) {
        isTaste = true;
      }
    }

    if (bundle.containsKey(BundleSet.USER_CODE)) {
      mUserCode = bundle.getString(BundleSet.USER_CODE);
    }

    if (bundle.containsKey(BundleSet.USER_NAME)) {
      mUserName = bundle.getString(BundleSet.USER_NAME);
    }

    if (bundle.containsKey(BundleSet.WEB_TITLE)) {
      mTitle = bundle.getString(BundleSet.WEB_TITLE);
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public MenuItems getToolbarMenuItems() {

    if (isTaste) {
      return MenuItems.DETAIL;
    } else {
      return null;
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_share:
        Observable.create(createShareMesasgeObservable())
            .doOnSubscribe(() -> AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL, AnalyticsActionType.SHARE_TASTE)
                .build()))
            .subscribe(shareDataWrapper -> {
              String message = mUserName + "님의 취향분석! 그 결과는? ";

              switch (shareDataWrapper.shareApp.getPackageName()) {
                case ShareAppLoader.PACKAGE_FACEBOOK:
                  message = "";
                  break;

                case ShareAppLoader.PACKAGE_TWITTER:
                  message += "#왓챠 ";
                  break;

                default:
                  break;
              }
              message += shareDataWrapper.data.getUrl();
              shareDataWrapper.loader.goChosenApp(message, shareDataWrapper.shareApp);
            });
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private Observable.OnSubscribe<ShareDataWrapper> createShareMesasgeObservable() {
    return subscriber -> {
      GridDrawerAdapter adapter = new GridDrawerAdapter(getActivity());
      final ShareAppLoader loader = new ShareAppLoader(getActivity());
      List<ShareApp> apps = loader.loadApps();
      adapter.addAll(apps);
      adapter.notifyDataSetChanged();
      FakeActionBar header = new FakeActionBar(getActivity());
      header.setTitle("공유하기");
      DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
          .setContentHolder(new ListHolder())
          .setAdapter(adapter)
          .setGravity(DialogPlus.Gravity.BOTTOM)
          .setCancelable(true)
          .setOnItemClickListener((dialog1, item, view, position) -> {
            if (item instanceof ShareApp) {
              dialog1.dismiss();
              final ShareApp app = (ShareApp) item;
              ApiResponseListener<BaseResponse<ShareMessageData>> listener =
                  (queryType, result) -> {
                    ShareMessageData data = result.getData();
                    ShareDataWrapper shareDataWrapper = new ShareDataWrapper();
                    shareDataWrapper.shareApp = app;
                    shareDataWrapper.data = data;
                    shareDataWrapper.loader = loader;
                    subscriber.onNext(shareDataWrapper);
                    subscriber.onCompleted();
                  };

              DataProvider<BaseResponse<ShareMessageData>> provider =
                  new DataProvider<>(getActivity(), QueryType.SHARE_MESSAGE.setApi("users", mUserCode), createReferrerBuilder());
              provider.withDialogMessage(getResources().getString(R.string.loading));
              provider.responseTo(listener);
              provider.request();
            }
          })
          .create();
      dialog.show();
    };
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.WEB_URL, mLink);
    outState.putString(BundleSet.WEB_TITLE, mTitle);
    if (mUserCode != null) outState.putString(BundleSet.USER_CODE, mUserCode);

    if (mUserName != null) outState.putString(BundleSet.USER_NAME, mUserName);
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
    getToolbarHelper().getActionbar().setTitle(mTitle);
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setPrimaryConfiguration();
    mWebview.loadUrl(mLink, getHeaders());
  }

  @Override public String getCurrentScreenName() {
    if (StringUtils.contains(mLink, "legals")) {
      return AnalyticsManager.LEGAL;
    } else if (StringUtils.contains(mLink, "taste")) {
      return AnalyticsManager.TASTE;
    } else if (StringUtils.contains(mLink, "gifts")) {
      return AnalyticsManager.GIFT;
    } else if (StringUtils.contains(mLink, "privacy")) {
      return AnalyticsManager.SETTING_PRIVACY;
    } else if (StringUtils.contains(mLink, "push")) {
      return AnalyticsManager.SETTING_ALARM;
    } else if (StringUtils.contains(mLink, "faqs")) {
      return AnalyticsManager.FAQ;
    }
    return null;
  }

  /**
   * 웹 페이지를 로드하기 위한 기본 설정.
   */
  @SuppressLint({ "AddJavascriptInterface", "SetJavaScriptEnabled" }) @JavascriptInterface
  private void setPrimaryConfiguration() {
    CookieSyncManager.createInstance(getActivity());
    try {
      android.webkit.CookieManager.getInstance().acceptThirdPartyCookies(mWebview);
    } catch (NoSuchMethodError e) {
      e.printStackTrace();
    }
    mWebview.getSettings().setJavaScriptEnabled(true);
    mWebview.getSettings()
        .setUserAgentString(WGuinnessHeader.get(WGuinnessHeader.Type.USER_AGENT).getValue());
    mWebview.addJavascriptInterface(new JavaScriptExtention(), "android");
    mWebview.setWebViewClient(new WebViewInternalClient(getActivity(), mLoadingView, getHeaders()));
    mWebview.setWebChromeClient(new WebChromeClient());
  }

  private Map<String, String> getHeaders() {
    if (mHeaders == null) {
      mHeaders = new HashMap<>();
      if (!TextUtils.isEmpty(BuildConfig.STAGING_HEADER) && !TextUtils.isEmpty(mLink)) {
        if (mLink.contains("watcha") || mLink.contains("192.168")) {
          mHeaders.put("Authorization",
              "Basic " + Base64.encodeToString(BuildConfig.STAGING_HEADER.getBytes(),
                  Base64.NO_WRAP));
        }
      }
      Map<String, String> guinnessTokens =
          WatchaApp.getSessionManager().getGuinnessSessionTokens(getActivity());
      if (guinnessTokens != null && !guinnessTokens.isEmpty()) {
        mHeaders.putAll(guinnessTokens);
      }
    }
    return mHeaders;
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return () -> {
      if (mWebview != null && mWebview.canGoBack()) {
        mWebview.goBack();
        return false;
      }
      return true;
    };
  }

  /**
   * 웹뷰가 링크를 부를때 응답을 가로채서
   *
   * 스키마면 앱으로 보내고, 아니면 웹 페이지 로드 함.
   * 웹 페이지 불려질때 시작과 끝을 알 수 있어서 로딩뷰같은 것도 띄워 줌.
   */
  private final static class WebViewInternalClient extends WebViewClient {

    private final Activity mActivity;
    private Map<String, String> mHeaders = null;
    private LoadingView mLoadingView;

    public WebViewInternalClient(@NonNull Activity act, LoadingView loadingView,
        Map<String, String> headers) {
      mActivity = act;
      mLoadingView = loadingView;
      mHeaders = headers;
    }

    @Override public boolean shouldOverrideUrlLoading(WebView view, String url)
        throws NullPointerException {
      String scheme = Uri.parse(url).getScheme();
      String host = Uri.parse(url).getHost();
      String path = Uri.parse(url).getPath();

      if (scheme == null) scheme = "";
      if (host == null) host = "";
      if (path == null) path = "";

      if ((host.equals("play.google.com") && path.equals("/store/apps/details")) || (scheme.equals(
          "watcha") || shouldOpenBrowser(url))) {
        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        return true;
      } else {
        view.loadUrl(url, mHeaders);
      }
      return false;
    }

    @Override public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
      mLoadingView.onLoading();
      mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override public void onPageFinished(WebView view, String url) throws NullPointerException {
      mLoadingView.onEnd();
      mLoadingView.setVisibility(View.GONE);
    }
  }

  /**
   * 웹뷰가 외부 앱을 통해서 부를지, 여기서 부를지 알려줌.
   *
   * @param link 웹페이지가 로드할 링크.
   */
  private static boolean shouldOpenBrowser(String link) {
    boolean shouldOpenBrowser = false;
    if (link != null) {
      boolean hasParams = link.contains("browser_open_type");

      if (hasParams) {
        try {
          shouldOpenBrowser =
              Uri.parse(link).getQueryParameter("browser_open_type").equals("external");
        } catch (NullPointerException e) {
          Crashlytics.logException(e);
        }
      }
    }
    return shouldOpenBrowser;
  }

  /**
   * 웹 페이지가 부르는 인터페이스.
   */
  public final class JavaScriptExtention {

    @JavascriptInterface public boolean shareTwitter(String json) {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.TARGET_CAMPAIGN, AnalyticsActionType.SHARE_PROMOTION)
          .setLabel(AnalyticsEvent.TWITTER)
          .build());
      final TweetComposer.Builder builder = new TweetComposer.Builder(getActivity());
      builder.text(json);
      boolean hasSession = TwitterBroker.hasSession();
      if (!hasSession) {
        final SettingHelper.OnSettingResponseListener twitterTokenListener =
            new SettingHelper.OnSettingResponseListener(getActivity()) {
              @Override public void onSuccess(@NonNull QueryType queryType,
                  @NonNull BaseResponse<User> result) {
                super.onSuccess(queryType, result);
                builder.show();
              }
            };
        TwitterBroker.connectTwitter(getBaseActivity(), twitterTokenListener);
      } else {
        builder.show();
      }
      return true;
    }

    @JavascriptInterface public boolean shareFacebook(final String url) {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.TARGET_CAMPAIGN, AnalyticsActionType.SHARE_PROMOTION)
          .setLabel(AnalyticsEvent.FACEBOOK)
          .build());
      FacebookBroker.getInstance()
          .publish(getActivity(), url)
          .subscribe(shareLinkContent -> {
            if (ShareDialog.canShow(ShareLinkContent.class)) {
              ShareDialog.show(getActivity(), shareLinkContent);
            }
          });
      return true;
    }

    @JavascriptInterface public boolean shareKakaoTalk(String message) {
      try {
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.TARGET_CAMPAIGN, AnalyticsActionType.SHARE_PROMOTION)
            .setLabel(AnalyticsEvent.KAKAOTALK)
            .build());
        KakaoLink kakaoLink = KakaoLink.getKakaoLink(getActivity().getApplicationContext());

        final KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder =
            kakaoLink.createKakaoTalkLinkMessageBuilder();

        kakaoTalkLinkMessageBuilder.addText(message);
        kakaoTalkLinkMessageBuilder.addAppButton("왓챠로 이동",
            new AppActionBuilder().setUrl(BuildConfig.END_POINT).build());
        kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder.build(), getActivity());
        return true;
      } catch (KakaoParameterException e) {
        e.printStackTrace();
      }
      return false;
    }

    @JavascriptInterface public boolean share(String json) {
      final GeneralShareData data =
          WatchaApp.getInstance().getGson().fromJson(json, GeneralShareData.class);
      GridDrawerAdapter adapter = new GridDrawerAdapter(getActivity());
      final ShareAppLoader loader = new ShareAppLoader(getActivity());
      List<ShareApp> apps = loader.loadApps();
      adapter.addAll(apps);
      adapter.notifyDataSetChanged();
      FakeActionBar header = new FakeActionBar(getActivity());
      header.setTitle("공유하기");
      DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
          .setContentHolder(new ListHolder())
          .setAdapter(adapter)
          .setGravity(DialogPlus.Gravity.BOTTOM)
          .setCancelable(true)
          .setOnItemClickListener((dialog1, item, view, position) -> {
            if (item instanceof ShareApp) {
              dialog1.dismiss();
              final ShareApp app = (ShareApp) item;
              switch (app.getPackageName()) {
                case ShareAppLoader.PACKAGE_FACEBOOK:
                  break;
                case ShareAppLoader.PACKAGE_TWITTER:
                  break;
                case ShareAppLoader.PACKAGE_KAKAO_TALK:
                  break;
                default:
                  loader.goChosenApp(data.getData().getMessage(), app);
                  break;
              }
            }
          })
          .create();
      dialog.show();

      return true;
    }

    @JavascriptInterface public boolean playYoutube(String youtubeId) {
      WLog.i("youtubeId: " + youtubeId);
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.TARGET_CAMPAIGN, AnalyticsActionType.PLAY_YOUTUBE)
          .setLabel(AnalyticsEvent.PROMOTION)
          .build());
      WatchaApp.getInstance().goYoutube(getActivity(), youtubeId);
      return true;
    }

    /**
     * 자바스크립트 코드가 안드 기기 클립보드에 텍스트를 추가할 때 호출 함.
     *
     * @param arg 클립보드에 추가할 텍스트.
     */
    @JavascriptInterface public boolean copyToClipboard(final String arg) {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.TARGET_CAMPAIGN, AnalyticsActionType.COPY_PASTEBOARD)
          .build());
      android.content.ClipboardManager clipboard =
          (android.content.ClipboardManager) getActivity().getSystemService(
              Context.CLIPBOARD_SERVICE);
      ClipData clip = ClipData.newPlainText(arg, arg);
      clipboard.setPrimaryClip(clip);

      if (clipboard.hasPrimaryClip()) {
        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
        String clippedText = item.getText().toString();
        Toast.makeText(getActivity(), "클립보드에 복사 되었습니다", Toast.LENGTH_SHORT).show();
        return clippedText.equals(arg);
      }
      return false;
    }

    @JavascriptInterface
    public void trackEvent(String category, String action, String arg1, String arg2) {
      if (arg1.equals("undefined")) arg1 = "";
      if (arg2.equals("undefined")) arg2 = "0";
      HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder(category, action)
          .setLabel(arg1).setValue(Long.parseLong(arg2));
      AnalyticsManager.getInstance().getTracker().send(builder.build());
    }
  }
}
