package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import butterknife.Bind;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AlertHelper;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.UnreadData;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.TwitterBroker;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.utils.WatchaPermission;
import com.frograms.watcha.view.SettingItemView;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.HashMap;
import java.util.Map;

public class SettingFragment extends BaseFragment<BaseResponse<User>>
    implements View.OnClickListener {

  @Bind(R.id.setting_profile) SettingItemView mProfileView;
  @Bind(R.id.setting_notification) SettingItemView mNotificationView;
  @Bind(R.id.setting_privacy) SettingItemView mPrivacyView;

  @Bind(R.id.setting_fb) SettingItemView mFbView;
  @Bind(R.id.setting_fb_timeline) SettingItemView mTimelineView;
  @Bind(R.id.setting_twt) SettingItemView mTwtView;

  @Bind(R.id.setting_notice) SettingItemView mNoticeView;
  @Bind(R.id.setting_update) SettingItemView mUpdateView;
  @Bind(R.id.setting_faq) SettingItemView mFaqView;
  @Bind(R.id.setting_sns) SettingItemView mSnsView;
  @Bind(R.id.setting_feedback) SettingItemView mFeedbackView;

  @Bind(R.id.setting_invite) SettingItemView mInviteView;

  @Bind(R.id.setting_reset) SettingItemView mResetView;
  @Bind(R.id.setting_logout) SettingItemView mLogoutView;
  @Bind(R.id.setting_deactivate) SettingItemView mDeAuthorizeView;

  private String mResetCategory, mResetActionType;

  private AlertDialog.Builder mAlertBuilder;

  private boolean mIsReset = false;
  private int mUnread = 0;

  private SettingHelper.OnSettingResponseListener mListener, mDeleteFbListener, mDeActivateListener;
  private ApiResponseListener<BaseResponse<UnreadData>> unreadListener;

  private SettingHelper.OnSettingResponseListener getResponseListener() {
    if (mListener == null) {
      mListener = new SettingHelper.OnSettingResponseListener(getActivity()) {
        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
          super.onSuccess(queryType, result);
          Toast.makeText(getActivity(), getString(R.string.saved), Toast.LENGTH_SHORT).show();
          if (WatchaApp.getUser() == null) {
            goIntro();
          }
          setUser(WatchaApp.getUser());
        }
      };
    }

    return mListener;
  }

  private SettingHelper.OnSettingResponseListener getDeleteFBResponseListener() {
    if (mDeleteFbListener == null) {
      mDeleteFbListener = new SettingHelper.OnSettingResponseListener(getActivity()) {
        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
          super.onSuccess(queryType, result);

          FacebookBroker.destroyAccessToken();

          // 연동해제될 때, 코멘트 작성 on/off 스위치 캐시된 값도 제거.
          PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.COMMENT_SHARE, PrefHelper.PrefKey.FACEBOOK
              .toString(), false);

          WLog.e("페북 연동 해제");
          Toast.makeText(getActivity(), getString(R.string.saved), Toast.LENGTH_SHORT).show();

          if (WatchaApp.getUser() == null) {
            goIntro();
          }

          //페북 다시 연동 시에, 튜토리얼을 제외한 곳에서 처음 별점 평가 시 유도 alert 띄우기 위해
          PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.FIRST_VISIT,
              PrefHelper.PrefKey.FIRST_RATING_AFTER_FB_CONNECT.toString(), true);
          PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.ALERT,
              AlertHelper.AlertType.FB_PUBLISH_PERMISSION.toString(), false);

          setUser(WatchaApp.getUser());
        }
      };
    }

    return mDeleteFbListener;
  }

  private ApiResponseListener<BaseResponse<UnreadData>> getUnreadListener() {
    if (unreadListener == null) {
      unreadListener = (queryType, result) -> {
        mUnread = result.getData().getUnread();

        if (mUnread == 0) {
          mNoticeView.setDescription(getString(R.string.no_noti));
        } else {
          mNoticeView.setDescription(mUnread + getString(R.string.there_is_noti));
        }
      };
    }

    return unreadListener;
  }

  private SettingHelper.OnSettingResponseListener getDeActivateListener() {
    if (mDeActivateListener == null) {
      mDeActivateListener = new SettingHelper.OnSettingResponseListener(getActivity()) {

        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
          super.onSuccess(queryType, result);
          AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
          alertBuilder.setCancelable(false);
          alertBuilder.setMessage(R.string.msg_deactivate)
              .setPositiveButton(R.string.ok, (dialog, which) -> {
                goIntro();
              });

          alertBuilder.show();
        }
      };
    }

    return mDeActivateListener;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.SETTING;
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_setting;
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override public void onClick(View v) {

    switch (v.getId()) {
      case R.id.setting_profile:
        ActivityStarter.with(getActivity(), FragmentTask.CHANGE_PROFILE).start();
        break;

      case R.id.setting_notification:
        ActivityStarter.with(getActivity(), FragmentTask.WEBVIEW)
            .addBundle(
                new BundleSet.Builder().putUrl(WatchaApp.DOMAIN + "/webview/account/settings/push",
                    getString(R.string.alarm)).build().getBundle())
            .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
            .start();
        break;

      case R.id.setting_privacy:
        ActivityStarter.with(getActivity(), FragmentTask.WEBVIEW)
            .addBundle(new BundleSet.Builder().putUrl(
                BuildConfig.END_POINT + "/webview/account/settings/privacy",
                getString(R.string.privacy_setting)).build().getBundle())
            .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
            .start();
        break;

      case R.id.setting_fb:
        if (!WatchaApp.getUser().isInitialized()) {
          mAlertBuilder = new AlertDialog.Builder(getActivity());
          mAlertBuilder.setTitle(R.string.set_password_before_disconnect_fb_title)
              .setMessage(R.string.set_password_before_disconnect_fb_title_msg)
              .setPositiveButton(R.string.set_password, (dialogInterface, i) -> {
                ActivityStarter.with(getActivity(), FragmentTask.CHANGE_INFO)
                    .addBundle(new BundleSet.Builder().putSettingType(ChangeInfoFragment.SettingType.SET_PASSWORD)
                        .build()
                        .getBundle())
                    .start();
              })
              .setNegativeButton(R.string.cancel, null);

          mAlertBuilder.show();
        } else {
          AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
              WatchaApp.getUser().isFbConnected() ? AnalyticsActionType.SWITCH_DISCONNECT_FACEBOOK
                  : AnalyticsActionType.SWITCH_CONNECT_FACEBOOK).build());

          if (WatchaApp.getUser().isFbConnected()) {
            mAlertBuilder = new AlertDialog.Builder(getActivity());
            mAlertBuilder.setTitle(R.string.disconnect_fb)
                .setMessage(R.string.disconnect_fb_msg)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                  SettingHelper.deleteFb(getActivity(), getDeleteFBResponseListener());
                })
                .setNegativeButton(R.string.no, null);

            mAlertBuilder.show();
          } else {
            FacebookBroker.accessTokenObservable(getActivity())
                .subscribe(accessToken -> {
                  SettingHelper.connectFb(getActivity(), accessToken.getToken(),
                      getResponseListener());
                });
          }
        }
        break;

      case R.id.setting_fb_timeline:
        //만약 페북 타임라인 게시 권한이 없을 때, 타임라인 공유를 설정할 때
        if (!FacebookBroker.hasGrantedFBPublishPermission(WatchaPermission.PUBLISH)) {
          FacebookBroker.getInstance()
              .canPublishAccessToken(getActivity())
              .subscribe(accessToken -> {
                setSettingSendEvaluationToFB();
              });
          break;
        }
        setSettingSendEvaluationToFB();
        break;

      case R.id.setting_twt:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
            WatchaApp.getUser().isTwtConnected() ? AnalyticsActionType.SWITCH_DISCONNECT_TWITTER
                : AnalyticsActionType.SWITCH_CONNECT_TWITTER).build());

        if (WatchaApp.getUser().isTwtConnected()) {
          mAlertBuilder = new AlertDialog.Builder(getActivity());
          mAlertBuilder.setTitle(R.string.remove_twt)
              .setMessage(R.string.want_remove_twt)
              .setPositiveButton(R.string.yes, (dialog, which) -> {
                TwitterBroker.removeTwitterSession(getBaseActivity(), getResponseListener());
              })
              .setNegativeButton(R.string.no, null);

          mAlertBuilder.show();
        } else {
          TwitterBroker.loadTwitterSession(getBaseActivity(), getResponseListener());
        }
        break;

      case R.id.setting_notice:
        ActivityStarter.with(getActivity(), FragmentTask.WEBVIEW)
            .addBundle(new BundleSet.Builder().putUrl(WatchaApp.DOMAIN + "/webview/notices",
                getString(R.string.notification)).build().getBundle())
            .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
            .start();

        mNoticeView.setDescription(getString(R.string.no_noti));
        mUnread = 0;
        break;

      case R.id.setting_update:
        break;

      case R.id.setting_faq:
        ActivityStarter.with(getActivity(), FragmentTask.WEBVIEW)
            .addBundle(new BundleSet.Builder().putUrl(WatchaApp.DOMAIN + "/webview/faqs",
                getString(R.string.faq)).build().getBundle())
            .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
            .start();
        break;

      case R.id.setting_sns:
        goSns();
        break;

      case R.id.setting_feedback:
        sendFeedback();
        break;

      case R.id.setting_invite:
        ActivityStarter.with(getActivity(), FragmentTask.INVITE_FRIENDS).start();
        break;

      case R.id.setting_reset:
        choiceCategory();
        break;

      case R.id.setting_logout:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.BUTTON_LOGOUT)
            .build());

        mAlertBuilder = new AlertDialog.Builder(getActivity());
        mAlertBuilder.setTitle(R.string.logout)
            .setMessage(R.string.want_logout)
            .setPositiveButton(R.string.yes, (dialog, which) -> {
              logout();
            })
            .setNegativeButton(R.string.no, null);

        mAlertBuilder.show();
        break;

      case R.id.setting_deactivate:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.BUTTON_TERMINATE_ACCOUNT)
            .build());
        mAlertBuilder = new AlertDialog.Builder(getActivity());
        mAlertBuilder.setTitle(R.string.deactivate_title)
            .setMessage(R.string.deactivate_msg)
            .setPositiveButton(R.string.anti_social_yes, (dialog, which) -> {
              AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.CONFIRM_TERMINIATE_ACCOUNT)
                  .build());
              SettingHelper.deActivate(getActivity(), getDeActivateListener());
            })
            .setNegativeButton(R.string.anti_social_no, null);

        mAlertBuilder.show();
        break;
    }
  }

  private void setSettingSendEvaluationToFB() {
    Map<String, String> params = new HashMap<>();
    params.put("send_eval_to_facebook", !WatchaApp.getUser().isTimelineAllowed() + "");
    SettingHelper.setting(getActivity(), params, getResponseListener());

    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
        !WatchaApp.getUser().isTimelineAllowed() ? AnalyticsActionType.SWITCH_OFF_POSTWALL_FACEBOOK
            : AnalyticsActionType.SWITCH_ON_POSTWALL_FACEBOOK).build());
  }

  private void goSns() {
    String[] strs = getResources().getStringArray(R.array.watcha_sns_array);

    FakeActionBar header = new FakeActionBar(getActivity());
    header.setTitle(getString(R.string.watcha_sns));
    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnItemClickListener((dialogPlus, o, view, i) -> {
          dialogPlus.dismiss();

          switch (i - 1) {
            case 0:
              ActivityStarter.with(getActivity(), "http://www.facebook.com/watcha.net").start();
              break;

            case 1:
              ActivityStarter.with(getActivity(), "https://twitter.com/watcha_app").start();
              break;

            case 2:
              ActivityStarter.with(getActivity(), "https://instagram.com/watcha_app").start();
              break;
            case 3:
              ActivityStarter.with(getActivity(), "http://frograms.com/blog").start();
              break;
          }
        })
        .create();
    dialog.show();
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.WATCHA_SNS_LIST)
        .build());
  }

  private void sendFeedback() {
    Intent intent = new Intent(Intent.ACTION_SEND);
    String emailText = "\n\n--\n";
    emailText += getString(R.string.do_not_touch_msg);

    try {
      emailText += "App Version : " + getActivity().getPackageManager()
          .getPackageInfo(getActivity().getPackageName(), 0).versionName + "\n";
    } catch (PackageManager.NameNotFoundException e1) {
      e1.printStackTrace();
    }

    emailText += "Device : " + Build.MODEL + "\n";
    emailText += "OS : API Level " + Build.VERSION.SDK_INT + "\n";
    emailText += "User Code : " + WatchaApp.getUser().getCode();

    intent.setType("message/rfc822");
    intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "cs@frograms.com" });
    intent.putExtra(Intent.EXTRA_SUBJECT,
        "[" + WatchaApp.getUser().getName() + "] " + getString(R.string.feedback));
    intent.putExtra(Intent.EXTRA_TEXT, emailText);
    try {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.BUTTON_EMAIL_FOR_FEEDBACK)
          .build());
      startActivity(Intent.createChooser(intent, "Send mail..."));
    } catch (android.content.ActivityNotFoundException ex) {
      Toast.makeText(getActivity(), "이메일 전송 가능한 앱이 없습니다", Toast.LENGTH_SHORT)
          .show();
    }
  }

  private void choiceCategory() {
    String[] strs = new String[CategoryType.values().length];
    for (int Loop1 = 0; Loop1 < CategoryType.values().length; Loop1++) {
      strs[Loop1] = getString(CategoryType.getCategory(Loop1).getStringId());
    }

    FakeActionBar header = new FakeActionBar(getActivity());
    header.setTitle("초기화 카테고리 선택");
    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnItemClickListener((dialogPlus, o, view, i) -> {
          dialogPlus.dismiss();

          CategoryType type = CategoryType.getCategory(i - 1);
          if (type == CategoryType.BOOKS) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.books_not_ready).setPositiveButton(R.string.ok, null);

            builder.show();
          } else {
            mResetCategory = type.getApiPath();
            new Handler().postDelayed(() -> {
              AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.BUTTON_SELECT_CATEGORY_TO_RESET_USER_ACTIONS)
                  .setLabel(mResetCategory)
                  .build());
              choiceActionType();
            }, 500);
          }
        })
        .create();
    dialog.show();
  }

  private void choiceActionType() {
    String[] strs = getResources().getStringArray(R.array.reset_array);

    FakeActionBar header = new FakeActionBar(getActivity());
    header.setTitle(getString(R.string.rating_reset));
    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnItemClickListener((dialogPlus, o, view, i) -> {
          dialogPlus.dismiss();
          switch (i - 1) {
            case 0:
              mResetActionType = "all";
              break;

            case 1:
              mResetActionType = "wish";
              break;

            case 2:
              mResetActionType = "rate";
              break;

            case 3:
              mResetActionType = "meh";
              break;

            case 4:
              mResetActionType = "comment";
              break;
          }

          AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.BUTTON_RESET_USER_ACTIONS)
              .setLabel(mResetActionType)
              .build());

          BundleSet.Builder bundleBuilder = new BundleSet.Builder().isResetActions(true);
          if (WatchaApp.getUser().isInitialized()) {
            bundleBuilder.putSettingType(ChangeInfoFragment.SettingType.CONFIRM_PASSWORD);
          } else {
            bundleBuilder.putSettingType(ChangeInfoFragment.SettingType.SET_PASSWORD);
          }

          mIsReset = true;
          ActivityStarter.with(getActivity(), FragmentTask.CHANGE_INFO)
              .addBundle(bundleBuilder.build().getBundle())
              .start();
        })
        .create();
    dialog.show();
  }

  private void resetRating(String password) {
    Map<String, String> params = new HashMap<>();
    params.put("password", password);
    params.put("content_types", mResetCategory);
    params.put("action_types", mResetActionType);

    SettingHelper.resetActions(getActivity(), password, mResetCategory, mResetActionType,
        getResponseListener());
  }

  private void logout() {
    DataProvider<BaseResponse<User>> provider =
        new DataProvider<BaseResponse<User>>(getActivity(), QueryType.LOGOUT, createReferrerBuilder())
            .withDialogMessage(getString(R.string.logging_out))
            .responseTo(this);
    provider.request();
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setToolbarTitle("설정", null);

    setUser(WatchaApp.getUser());
    mProfileView.setOnClickListener(this);
    mNotificationView.setOnClickListener(this);
    mPrivacyView.setOnClickListener(this);

    mFbView.setOnClickListener(this);
    mTimelineView.setOnClickListener(this);
    mTwtView.setOnClickListener(this);

    mNoticeView.setOnClickListener(this);
    mUpdateView.setOnClickListener(this);
    mFaqView.setOnClickListener(this);
    mSnsView.setOnClickListener(this);
    mFeedbackView.setOnClickListener(this);

    mInviteView.setOnClickListener(this);

    mResetView.setOnClickListener(this);
    mLogoutView.setOnClickListener(this);
    mDeAuthorizeView.setOnClickListener(this);

    String version = PrefHelper.getString(getActivity(), PrefHelper.PrefType.VERSION,
        PrefHelper.PrefKey.LATEST.name());

    if (version != null) {
      mUpdateView.setDescription(getString(
          (Util.needToUpdateApplication(version)) ? R.string.new_update : R.string.latest_version));
    }

    SettingHelper.unreadNotices(getActivity(), getUnreadListener());
  }

  private void setUser(User user) {
    //mPrivacyView.setDescription(getString(user.getPrivacyLevel().getStringId()));
    mFbView.setChecked(user.isFbConnected());
    if (user.isFbConnected()) {
      mTimelineView.setVisibility(View.VISIBLE);
      mTimelineView.setChecked(user.isTimelineAllowed());
    } else {
      mTimelineView.setVisibility(View.GONE);
    }
    mTwtView.setChecked(user.isTwtConnected());
  }

  @Override public void onResume() {
    super.onResume();

    setUser(WatchaApp.getUser());
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
    super.onSuccess(queryType, result);

    switch (queryType) {
      case LOGOUT:
        goIntro();
        break;
    }
  }

  private void goIntro() {
    FacebookBroker.destroyAccessToken();
    WatchaApp.getSessionManager().destroySession(getActivity());
    CacheManager.getTagCache().evictAll();
    ActivityStarter.with(getActivity(), FragmentTask.INTRO)
        .setAnimationType(ActivityStarter.AnimationType.NONE)
        .start();
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == Activity.RESULT_OK
        && requestCode == FragmentTask.CHANGE_INFO.getValue()
        && mIsReset) {
      final String password = data.getStringExtra("password");

      AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
      builder.setMessage(R.string.alert_rating_reset_msg)
          .setPositiveButton(R.string.alert_rating_reset_yes, (dialogInterface, i) -> {
            resetRating(password);
            AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, AnalyticsActionType.CONFIRM_RESET_USER_ACTIONS)
                .setLabel(mResetActionType)
                .build());
          })
          .setNegativeButton(R.string.alert_rating_reset_no, null);

      builder.show();
    }

    mIsReset = false;
  }
}
