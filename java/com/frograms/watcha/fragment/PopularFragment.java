package com.frograms.watcha.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.PopularListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.select_tabs.SelectTabLayout;
import com.frograms.watcha.view.select_tabs.SelectTabView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

public class PopularFragment extends ListFragment<PopularListData>
    implements SelectTabView.OnTabSelectListener, EmptyViewPresenter {

  private CategoryType mCategoryType = CategoryType.MOVIES;
  private SelectTabLayout mSelectTabLayout;
  private View mPopularView;
  private IconActionGridButton mCategoryView = null;
  private String mCategoryId = "ko";
  private int mSelectedTabPosition = 0;
  private TourGuide mCoachmarkHandler = null;

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected boolean enableSwipeRefresh() {
    return true;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.CATEGORY_TYPE)) {
      mCategoryType = CategoryType.getCategory(bundle.getInt(BundleSet.CATEGORY_TYPE));
      mSelectedTabPosition = mCategoryType.getTypeInt();
    }

    if (bundle.containsKey(BundleSet.CATEGORY_ID)) {
      int type = bundle.getInt(BundleSet.CATEGORY_ID);
      switch (type) {
        case 0:
          mCategoryId = "ko";
          break;

        case 1:
          mCategoryId = "us";
          break;

        case 2:
          mCategoryId = "jp";
          break;
      }
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    if (mCategoryType != null) {
      switch (mCategoryType) {
        case MOVIES:
          return AnalyticsManager.POPULAR_MOVIE;
        case TV_SEASONS:
          return AnalyticsManager.POPULAR_DRAMA;
        case BOOKS:
          return AnalyticsManager.BOOK;
      }
    }
    return null;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if (mCategoryType != null) outState.putInt(BundleSet.CATEGORY_TYPE, mCategoryType.getTypeInt());

    if (mCategoryId != null) {
      switch (mCategoryId) {
        case "ko":
          outState.putInt(BundleSet.CATEGORY_ID, 0);
          break;

        case "us":
          outState.putInt(BundleSet.CATEGORY_ID, 1);
          break;

        case "jp":
          outState.putInt(BundleSet.CATEGORY_ID, 2);
          break;
      }
    }
  }

  @Override protected QueryType getQueryType() {
    return QueryType.POPULAR.setApi(mCategoryType.getApiPath());
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> params = new HashMap<>();
    if (mCategoryId != null) {
      params.put("id", mCategoryId);
    }
    return params;
  }

  private View getPopularView() {
    if (mPopularView == null) {
      mPopularView =
          LayoutInflater.from(getActivity()).inflate(R.layout.undertoolbar_popular, null);
      mCategoryView = (IconActionGridButton) mPopularView.findViewById(R.id.layout_single_button);
      mCategoryView.getIcon().setVisibility(View.GONE);
      mCategoryView.setTextNormalColor(getResources().getColor(R.color.gray));
      //mCategoryView.showRipple(false);
      mCategoryView.setIconColor(getResources().getColor(R.color.skyblue));
      mPopularView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          if (mCoachmarkHandler != null) {
            mCoachmarkHandler.cleanUp();
            mCoachmarkHandler = null;
            return;
          }
          FakeActionBar header = new FakeActionBar(getActivity());
          header.setTitle(mCategoryType == CategoryType.MOVIES ? "인기 카테고리 설정" : "국가 설정");
          List<String> strs = new ArrayList<>();
          if (mCategoryType == CategoryType.MOVIES) {
            strs.add(getString(R.string.boxoffice_rank));
          } else if (mCategoryType == CategoryType.TV_SEASONS) {
            strs.add(getString(R.string.popular_in_korea));
            strs.add(getString(R.string.popular_in_america));
            strs.add(getString(R.string.popular_in_japan));
          }
          ArrayAdapter<String> simpleAdapter =
              new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
          DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
              .setContentHolder(new ListHolder())
              .setAdapter(simpleAdapter)
              .setGravity(DialogPlus.Gravity.BOTTOM)
              .setCancelable(true)
              .setOnItemClickListener((dialogPlus, o, view, i) -> {
                dialogPlus.dismiss();
                int index = i - 1;
                if (mCategoryType == CategoryType.MOVIES) {
                  switch (index) {
                    case 0:
                      break;
                  }
                } else if (mCategoryType == CategoryType.TV_SEASONS) {
                  switch (index) {
                    case 0:
                      mCategoryId = "ko";
                      break;
                    case 1:
                      mCategoryId = "us";
                      break;
                    case 2:
                      mCategoryId = "jp";
                      break;
                  }
                }
                refresh();
              })
              .create();

          if (mCategoryType == CategoryType.TV_SEASONS) {
            dialog.show();
          }
        }
      });
    }
    return mPopularView;
  }

  @Override public View getListHeaderView() {
    if (mSelectTabLayout == null) {
      mSelectTabLayout = new SelectTabLayout(getActivity());
      mSelectTabLayout.addTab(getString(R.string.movie), this);
      mSelectTabLayout.addTab(getString(R.string.drama), this);
      mSelectTabLayout.addTab(getString(R.string.book), this);
    }
    return mSelectTabLayout;
  }

  @Override public void load() {
    super.load();
    addUnderToolbarView(getPopularView());
  }

  @Override public void unload() {
    super.unload();
    removeUnderToolbarView(getPopularView());
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mSelectTabLayout.selectTabPosition(mSelectedTabPosition);

    if (mSelectedTabPosition == 2) {
      showBooks();
    }
  }

  @Override public void onClickSelectableTab(final SelectTabLayout layout, SelectTabView button,
      int position) {
    mSelectTabLayout.selectTabPosition(position);
    switch (position) {
      case 0:
        if (mCategoryType != CategoryType.MOVIES) {
          if (mDataProvider != null) {
            mDataProvider.shouldCancel(true);
          }
          mSelectedTabPosition = position;
          mCategoryId = null;
          mCategoryType = CategoryType.MOVIES;
          mAdapter.getFooterView().setVisibility(View.VISIBLE);
          refresh();
          getPopularView().setVisibility(View.VISIBLE);
        }
        AnalyticsManager.sendScreenName(getCurrentScreenName());
        break;
      case 1:
        if (mCategoryType != CategoryType.TV_SEASONS) {
          if (mDataProvider != null) {
            mDataProvider.shouldCancel(true);
          }
          mSelectedTabPosition = position;
          mCategoryId = "ko";
          mCategoryType = CategoryType.TV_SEASONS;
          refresh();
          getPopularView().setVisibility(View.VISIBLE);
        }
        AnalyticsManager.sendScreenName(getCurrentScreenName());
        break;
      case 2:
        if (mCategoryType != CategoryType.BOOKS) {
          showBooks();
          if (mProgressView != null) {
            mProgressView.setVisibility(View.GONE);
          }
        }
        break;
    }
  }

  private void showBooks() {
    if (mDataProvider != null) {
      mDataProvider.shouldCancel(true);
    }

    CacheManager.getTagCache().evictAll();
    mSelectedTabPosition = CategoryType.BOOKS.getTypeInt();
    mCategoryType = CategoryType.BOOKS;

    if (mAdapter.getFooterView() != null) {
      mAdapter.clearItems();
      mAdapter.getFooterView().setVisibility(View.INVISIBLE);
    } else {
      mAdapter.setDefaultLoadingVisibility(false);
    }

    if (mAdapter.getLoadingViewHolder() != null) {
      mAdapter.getLoadingViewHolder().onEnd();
    }
    refreshEmptyListView();
    setEmptyViewVisibility(View.VISIBLE);
    getPopularView().setVisibility(View.GONE);
    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull final BaseResponse<PopularListData> result) {
    super.onSuccess(queryType, result);
    PopularListData data = result.getData();

    if (mCategoryType == CategoryType.TV_SEASONS && mCategoryView != null) {
      switch (mCategoryId) {
        default:
        case "ko":
          mCategoryView.setText(getString(R.string.popular_in_korea));
          break;
        case "us":
          mCategoryView.setText(getString(R.string.popular_in_america));
          break;
        case "jp":
          mCategoryView.setText(getString(R.string.popular_in_japan));
          break;
      }
      //mCategoryView.showRipple(true);
      mCategoryView.getIcon().setVisibility(View.VISIBLE);
      mCategoryView.setIconSize(getResources().getDimension(R.dimen.under_toolbar_icon_font));
      if (!PrefHelper.getBoolean(getActivity(), PrefHelper.PrefType.COACHMARK, PrefHelper.PrefKey.COACHMARK_POPULAR_DRAMA_FILTER
          .name())) {
        PrefHelper.setBoolean(getActivity(), PrefHelper.PrefType.COACHMARK, PrefHelper.PrefKey.COACHMARK_POPULAR_DRAMA_FILTER
            .name(), true);
        ToolTip t = new ToolTip().setGravity(Gravity.TOP)
            .setTitleTextSize(21)
            .setDescriptionTextSize(20)
            .setTypeface(FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
            .setBackgroundColor(Color.TRANSPARENT)
            .setTitle("미국, 일본 드라마 인기순위도 있어요")
            .setDescription("알겠어요!")
            .setTextColor(Color.WHITE)
            .setClickListener(new View.OnClickListener() {
              @Override public void onClick(View v) {
                if (mCoachmarkHandler != null) {
                  mCoachmarkHandler.cleanUp();
                  mCoachmarkHandler = null;
                }
              }
            })
            .setShadow(false);
        mCoachmarkHandler = TourGuide.init(getActivity())
            .with(TourGuide.Technique.Click)
            .setToolTip(t)
            .setOverlay(new Overlay().setStyle(Overlay.Style.Rectangle)
                .setBackgroundColor(getActivity().getResources().getColor(R.color.skyblue_95)))
            .playOn(mCategoryView);
      }
    } else if (mCategoryType == CategoryType.MOVIES && mCategoryView != null) {
      mCategoryView.setText(getString(R.string.boxoffice_rank));
      //mCategoryView.showRipple(false);
      mCategoryView.getIcon().setVisibility(View.GONE);
    }
  }

  @Override public View getEmptyView() {
    if (mCategoryType == CategoryType.BOOKS) {
      return new SimpleEmptyView(getActivity(), R.string.icon_book, "도서는 아직 준비중이에요!:)");
    }
    return null;
  }
}
