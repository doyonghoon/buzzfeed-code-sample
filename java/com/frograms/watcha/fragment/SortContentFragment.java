package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.DragSortRecycler;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.model.views.ViewCard;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 컬렉션에 영화 컨텐츠 순서를 변경하는 화면.
 */
public class SortContentFragment extends ListFragment<ListData>
    implements RecyclerAdapter.OnClickCardItemListener {

  private String mDeckCode = null;
  private QueryType mQueryType = QueryType.UPDATE_CONTENTS_IN_DECK;

  private String mDeckTitle = null;
  private int mContentCounts;

  private boolean mAbleToLoadData = false;
  private ArrayList<String> mAddedContentCodes = new ArrayList<>();

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.DECK_CODE)) {
      mDeckCode = bundle.getString(BundleSet.DECK_CODE, null);
    }

    if (bundle.containsKey(BundleSet.DECK_TITLE)) {
      mDeckTitle = bundle.getString(BundleSet.DECK_TITLE, null);
    }

    if (bundle.containsKey(BundleSet.CONTENT_CODES)) {
      getAddedContentCodes().addAll(bundle.getStringArrayList(BundleSet.CONTENT_CODES));
    }

    AnalyticsManager.sendScreenName(getCurrentScreenName());
  }

  @Nullable @Override public String getCurrentScreenName() {
    return TextUtils.isEmpty(mDeckCode) ? AnalyticsManager.SORT_MOVIE_ORDER_TO_CREATE_DECK
        : AnalyticsManager.SORT_MOVIE_ORDER_TO_EDIT_DECK;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.DECK_CODE, mDeckCode);
    outState.putString(BundleSet.DECK_TITLE, mDeckTitle);
    outState.putStringArrayList(BundleSet.CONTENT_CODES, getAddedContentCodes());
  }

  private ArrayList<String> getAddedContentCodes() {
    if (mAddedContentCodes == null) {
      mAddedContentCodes = new ArrayList<>();
    }

    return mAddedContentCodes;
  }

  @Override public MenuItems getToolbarMenuItems() {
    return MenuItems.ACCEPT;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_accept:
        getAddedContentCodes().clear();
        List<ViewCard> cards = mAdapter.getData();
        for (ViewCard card : cards) {
          if (card.getCardItem().getItem() instanceof Content) {
            getAddedContentCodes().add(((Content) card.getCardItem().getItem()).getCode());
          }
        }

        Intent data = new Intent();
        data.putStringArrayListExtra(CreateDeckFragment.KEY, getAddedContentCodes());

        getActivity().setResult(Activity.RESULT_OK, data);
        getActivity().finish();

        return true;
    }
    return false;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup,
      Bundle paramBundle) {
    getToolbarHelper().setToolbarTitle("영화 순서 변경");
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  private void switchLocation(List<ViewCard> cards, int from, int to) {
    if (from == to) return;

    if (to - 1 == 0) {
      cards.get(from - 1).setLocation(ViewCard.LOCATION.TOP);
      cards.get(0).setLocation(ViewCard.LOCATION.NONE);
    } else if (to == cards.size()) {
      cards.get(from - 1).setLocation(ViewCard.LOCATION.BOT);
      cards.get(to - 1).setLocation(ViewCard.LOCATION.NONE);
    } else {
      cards.get(from - 1).setLocation(ViewCard.LOCATION.NONE);
    }

    if (from - 1 == 0) {
      cards.get(from).setLocation(ViewCard.LOCATION.TOP);
    } else if (from == cards.size()) {
      cards.get(from - 2).setLocation(ViewCard.LOCATION.BOT);
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getToolbarHelper().getActionbar().setTitle(mDeckTitle);
    mContentCounts = getAddedContentCodes().size();

    mSwipeLayout.setEnabled(false);
    mList.setItemAnimator(null);
    DragSortRecycler dragSortRecycler = new DragSortRecycler();
    dragSortRecycler.setViewHandleId(R.id.drag_handle);
    dragSortRecycler.setOnItemMovedListener((from, to) -> {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT,
          TextUtils.isEmpty(mDeckCode) ? AnalyticsActionType.SORT_SELECTED_MOVIE_TO_CREATE_DECK
              : AnalyticsActionType.SORT_SELECTED_MOVIE_TO_EDIT_DECK).build());

      List<ViewCard> cards = mAdapter.getData();
      if (to < 1) to = 1;
      if (to > cards.size()) to = cards.size();
      switchLocation(cards, from, to);
      if (1 <= to && to <= cards.size()) {
        ViewCard item = cards.remove(from - 1);
        cards.add(to - 1, item);
        mAdapter.notifyDataSetChanged();
      }
    });
    mList.addItemDecoration(dragSortRecycler);
    mList.addOnItemTouchListener(dragSortRecycler);
    mList.setOnScrollListener(dragSortRecycler.getScrollListener());
    mAbleToLoadData = true;
    mAdapter.setOnClickCardItemListener(this);
    requestData(getDataProvider());
  }

  @Override protected boolean shouldRetriveDataImmediately() {
    return mAbleToLoadData;
  }

  @Override protected QueryType getQueryType() {
    if (mDeckCode == null) {
      mQueryType.setApi("new", "edit");
    } else {
      mQueryType.setApi(mDeckCode, "edit");
    }

    return mQueryType;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> map = super.getRequestDataParams();

    if (!getAddedContentCodes().isEmpty()) {
      map.put("content_codes", TextUtils.join(",", getAddedContentCodes()));
    }

    return map;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onClickCardItem(ItemView v, Item item) {
    if (item instanceof Content) {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT,
          TextUtils.isEmpty(mDeckCode) ? AnalyticsActionType.REMOVE_SELECTED_MOVIE_TO_CREATE_DECK
              : AnalyticsActionType.REMOVE_SELECTED_MOVIE_TO_EDIT_DECK).build());

      Content content = (Content) item;
      List<ViewCard> cards = mAdapter.getData();
      for (ViewCard card : cards) {
        if (card.getCardItem().getItem() instanceof Content) {
          Content c = (Content) card.getCardItem().getItem();
          if (content.equals(c)) {
            cards.remove(card);
            mContentCounts--;
            break;
          }
        }
      }
      mAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ListData> result) {
    super.onSuccess(queryType, result);
  }
}
