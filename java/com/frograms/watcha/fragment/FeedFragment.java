package com.frograms.watcha.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.FeedListData;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.widget.IconActionGridButton;
import java.util.HashMap;
import java.util.Map;

public class FeedFragment extends ListFragment<FeedListData> {

  private static final QueryType mQueryType = QueryType.FEED;

  private View mRatingView;

  private String mLastSeenId;
  private int mLoadCount = 1;

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected boolean enableSwipeRefresh() {
    return true;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey("LAST_SEEN_ID")) {
      mLastSeenId = bundle.getString("LAST_SEEN_ID");
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.FEED;
  }

  @Override protected QueryType getQueryType() {
    return mQueryType;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    if (mLastSeenId != null) {
      outState.putString("LAST_SEEN_ID", mLastSeenId);
    }
  }

  private View getRatingView() {
    if (mRatingView == null) {
      mRatingView = LayoutInflater.from(getActivity()).inflate(R.layout.undertoolbar_feed, null);
      mRatingView.setBackgroundColor(Color.TRANSPARENT);

      IconActionGridButton comment = ButterKnife.findById(mRatingView, R.id.comment);
      comment.setBackgroundColor(getResources().getColor(R.color.background_alpha_95));
      comment.setTextNormalColor(getResources().getColor(R.color.heavy_gray));
      comment.setIconColor(getResources().getColor(R.color.skyblue));
      comment.findViewById(R.id.comment).setOnClickListener(v -> ActivityStarter.with(getActivity(), FragmentTask.SELECT_CONTENT)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .start());

      IconActionGridButton collection = ButterKnife.findById(mRatingView, R.id.collection);
      collection.setBackgroundColor(getResources().getColor(R.color.background_alpha_95));
      collection.setTextNormalColor(getResources().getColor(R.color.heavy_gray));
      collection.setIconColor(getResources().getColor(R.color.skyblue));
      collection.findViewById(R.id.collection).setOnClickListener(v -> ActivityStarter.with(getActivity(), FragmentTask.CREATE_DECK)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .start());

      IconActionGridButton rating = ButterKnife.findById(mRatingView, R.id.rating);
      rating.setBackgroundColor(getResources().getColor(R.color.background_alpha_95));
      rating.setTextNormalColor(getResources().getColor(R.color.heavy_gray));
      rating.setIconColor(getResources().getColor(R.color.skyblue));
      rating.setOnClickListener(v -> ActivityStarter.with(getActivity(), FragmentTask.RATING_TAB)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .start());
    }
    return mRatingView;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    Map<String, String> map = new HashMap<>();
    if (mLastSeenId != null) {
      map.put("last_seen_id", mLastSeenId);
    }
    return map;
  }

  @Override public void load() {
    super.load();
    addUnderToolbarView(getRatingView());
  }

  @Override public void unload() {
    super.unload();
    removeUnderToolbarView(getRatingView());
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void refresh() {
    mLastSeenId = null;
    mLoadCount = 1;
    super.refresh();
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.REFRESH)
        .setLabel(AnalyticsEvent.FEED)
        .build());
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull final BaseResponse<FeedListData> result) {
    super.onSuccess(queryType, result);
    if (mLoadCount > 1) {
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.PAGINATION)
          .setLabel(AnalyticsEvent.FEED)
          .setValue(mLoadCount)
          .build());
    }
    mLastSeenId = result.getData().getLastSeenId();
    mLoadCount++;
  }
}
