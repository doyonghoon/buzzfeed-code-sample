package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.database.TagCache;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.view.SelectedTagsView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.widget.TagSettingToolbarView;
import java.util.List;

/**
 * 드라마 태그 설정화면.
 */
public class DramaTagsFragment extends TagsFragment implements TagCache.OnTagCacheUpdateListener {

  @Bind(R.id.tag_root) View mRoot;
  private TagSettingToolbarView mToolbarView;
  private int mSelectedTagsCount = 0;
  private SelectedTagsView mSelectedTagsView = null;

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle(null);
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.TAG_IDS)) {
      mSelectedTagsCount = bundle.getIntArray(BundleSet.TAG_IDS).length;
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.TAG_SETTING_DRAMA;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override protected View getCustomToolbarView() {
    return getToolbarView();
  }

  private View getToolbarView() {
    if (mToolbarView == null) {
      mToolbarView = new TagSettingToolbarView(getActivity());
      mToolbarView.setOnTagOkClickListener(tagCount -> {
        int[] tagIds = CacheManager.getTagIds();
        Intent data = new Intent();
        data.putExtra(BundleSet.TAG_IDS, tagIds != null ? tagIds : new int[] {});
        getActivity().setResult(Activity.RESULT_OK, data);
        getActivity().finish();
      });
    }
    return mToolbarView;
  }

  protected int[] getTagIds(List<TagItems.Tag> tags) {
    if (tags != null && !tags.isEmpty()) {
      int[] ids = new int[tags.size()];
      for (int i = 0; i < ids.length; i++) {
        ids[i] = tags.get(i).getId();
      }
      return ids;
    }
    return null;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mRoot.setPadding(0, getResources().getDimensionPixelSize(R.dimen.toolbar_height), 0, 0);
    CacheManager.addTagUpdateListener(this);
    if (mSelectedTagsCount > 0) {
      mToolbarView.setTagCount(mSelectedTagsCount);
    }
    // 기본 값으로 태그설정 화면이 닫히는건 취소임.
    getActivity().setResult(Activity.RESULT_CANCELED);

    // Cache된 태그들 있다면 UnderBar에 추가
    if (CacheManager.getTagSnapshot().entrySet().size() > 0) {
      if (!getSelectedTagsView().isShown()) {
        addUnderToolbarView(getSelectedTagsView());
        getSelectedTagsView().setIsShown(true);
      }
      getSelectedTagsView().addTags(CacheManager.getTagSnapshot().entrySet());
      getSelectedTagsView().autoScrollToEnd();
    }
  }

  @Override public void onDestroy() {
    super.onDestroy();
    CacheManager.removeTagUpdateListener(DramaTagsFragment.this);
  }

  private SelectedTagsView getSelectedTagsView() {
    if (mSelectedTagsView == null) {
      mSelectedTagsView = new SelectedTagsView(getActivity());

      //DramaTagsFragment는 TagsFragment 하나로 구성되어 있으니깐.
      mSelectedTagsView.addOnTagItemResetListener(this);
    }
    return mSelectedTagsView;
  }

  @Override public void unload() {
    super.unload();
    removeUnderToolbarView(getSelectedTagsView());
    getSelectedTagsView().setIsShown(false);
  }

  @Override public void onAddTagCache(TagCache cache, TagItems.Tag tag) {
    mToolbarView.setTagCount(cache.size());
    if (cache.size() >= 1 && !getSelectedTagsView().isShown()) {
      addUnderToolbarView(getSelectedTagsView());
      getSelectedTagsView().setIsShown(true);
    }
    getSelectedTagsView().addTag(tag);
  }

  @Override public void onRemoveTagCache(TagCache cache, String key) {
    mToolbarView.setTagCount(cache.size());
    if (cache.size() < 1) {
      removeUnderToolbarView(getSelectedTagsView());
      getSelectedTagsView().setIsShown(false);
    }
    getSelectedTagsView().removeTag(Integer.parseInt(key));
  }
}