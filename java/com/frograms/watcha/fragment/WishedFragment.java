package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;

/**
 * 보고싶어요 화면.
 */
public class WishedFragment extends ListFragment<ListData> {

  private String mUserCode = null;
  private QueryType mQueryType = null;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.USER_CODE)) {
      mUserCode = bundle.getString(BundleSet.USER_CODE);
      mQueryType = QueryType.WISHED.setApi(mUserCode);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.USER_CODE, mUserCode);
  }

  @Override public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup,
      Bundle paramBundle) {
    getToolbarHelper().setToolbarTitle("보고싶어요");
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  /**
   * 파라미터 없이 데이터 요청 함.
   */
  @Override protected QueryType getQueryType() {
    return mQueryType;
  }
}
