package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FormValidator;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import java.util.HashMap;
import java.util.Map;

public class ChangeInfoFragment extends BaseFragment<BaseResponse<User>>
    implements View.OnClickListener, FormValidator.FormValidationListener {

  public enum SettingType {
    CHANGE_NAME(0), CHANGE_EMAIL(1), CHANGE_PASSWORD(2), SET_PASSWORD(3), CONFIRM_PASSWORD(4), CHANGE_MESSAGE(5);

    int mNum;

    SettingType(int num) {
      mNum = num;
    }

    public int getNum() {
      return mNum;
    }

    public static SettingType getSettingType(int typeInt) {
      SettingType[] tabs = SettingType.values();
      for (SettingType t : tabs) {
        if (t.getNum() == typeInt) {
          return t;
        }
      }
      return null;
    }
  }

  @Bind(R.id.edit1) EditText mFirstEditView;
  @Bind(R.id.edit2) EditText mSecondEditView;
  @Bind(R.id.edit3) EditText mThirdEditView;
  @Bind(R.id.complete) TextView mCompleteView;

  private boolean mIsEnabled;
  private boolean mIsResetActions;
  private SettingType mSettingType;

  private final FormValidator mNameValidator = new FormValidator(FormValidator.FormStyle.USERNAME);
  private final FormValidator mEmailValidator = new FormValidator(FormValidator.FormStyle.EMAIL);
  private final FormValidator mOldPasswordValidator =
      new FormValidator(FormValidator.FormStyle.PASSWORD);
  private final FormValidator mPasswordValidator =
      new FormValidator(FormValidator.FormStyle.PASSWORD);
  private final FormValidator mPasswordAgainValidator =
      new FormValidator(FormValidator.FormStyle.PASSWORD);

  private boolean isValidFirst = false;
  private boolean isValidSecond = false;
  private boolean isValidThird = false;

  private SettingHelper.OnSettingResponseListener createSettingResponseListener(Context context) {
    return new SettingHelper.OnSettingResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);
        if (mSettingType.equals(SettingType.CHANGE_EMAIL))
          Toast.makeText(getActivity(), getString(R.string.email_change), Toast.LENGTH_SHORT).show();
        else
          Toast.makeText(getActivity(), getString(R.string.saved), Toast.LENGTH_SHORT).show();

        if (mIsResetActions) {
          Intent intent = new Intent();
          intent.putExtra("password", mFirstEditView.getText().toString());
          getActivity().setResult(Activity.RESULT_OK, intent);
        }
        getActivity().finish();
      }
    };
  }

  @Nullable @Override public String getCurrentScreenName() {
    if (mSettingType != null) {
      switch (mSettingType) {
        case CHANGE_EMAIL:
          return AnalyticsManager.ALTER_EMAIL;
        case CHANGE_NAME:
          return AnalyticsManager.ALTER_NAME;
        case CHANGE_PASSWORD:
          return AnalyticsManager.ALTER_PASSWORD;
        case CONFIRM_PASSWORD:
          return AnalyticsManager.VERIFY_PASSWORD;
        case SET_PASSWORD:
          return AnalyticsManager.ASSIGNING_PASSWORD;
        case CHANGE_MESSAGE:
          // TODO : 상태메세지 GA 추가.
      }
    }
    return null;
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_change_info;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.RESET_ACTIONS)) {
      mIsResetActions = bundle.getBoolean(BundleSet.RESET_ACTIONS, false);
    }

    if (bundle.containsKey(BundleSet.SETTING_TYPE)) {
      mSettingType = SettingType.getSettingType(bundle.getInt(BundleSet.SETTING_TYPE));
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putBoolean(BundleSet.RESET_ACTIONS, mIsResetActions);
    outState.putInt(BundleSet.SETTING_TYPE, mSettingType.getNum());
  }

  @Override protected DataProvider<BaseResponse<User>> getDataProvider(Map<String, String> params) {
    if (params == null) return null;

    mDataProvider =
        new DataProvider<BaseResponse<User>>(getActivity(), getQueryType(), createReferrerBuilder())
            .withDialogMessage(getString(R.string.changing))
            .responseTo(this);

    mDataProvider.withParams(params);

    return mDataProvider;
  }

  @Override protected Map<String, String> getRequestDataParams() {

    Map<String, String> params = new HashMap<>();

    switch (mSettingType) {
      case CHANGE_NAME:
        params.put("nickname", mFirstEditView.getText().toString());
        break;

      case CHANGE_EMAIL:
        params.put("email", mFirstEditView.getText().toString());
        params.put("validate_password", mSecondEditView.getText().toString());
        break;

      case CHANGE_PASSWORD:
        params.put("password", mSecondEditView.getText().toString());
        params.put("validate_password", mFirstEditView.getText().toString());
        break;

      case SET_PASSWORD:
        params.put("password", mFirstEditView.getText().toString());
        break;

      case CONFIRM_PASSWORD:
        break;

      case CHANGE_MESSAGE:
        params.put("bio", mFirstEditView.getText().toString());
        break;
    }

    return params;
  }

  @Override public void onClick(View v) {

    switch (v.getId()) {
      case R.id.complete:
        if (!mIsEnabled) break;

        String pwd, pwdAgain;
        switch (mSettingType) {
          case CHANGE_PASSWORD:
            pwd = mSecondEditView.getText().toString();
            pwdAgain = mThirdEditView.getText().toString();

            if (!pwd.equals(pwdAgain)) {
              Toast.makeText(getActivity(), R.string.not_correct_pwd, Toast.LENGTH_SHORT).show();
              return;
            }
            break;

          case SET_PASSWORD:
            pwd = mFirstEditView.getText().toString();
            pwdAgain = mSecondEditView.getText().toString();

            if (!pwd.equals(pwdAgain)) {
              Toast.makeText(getActivity(), R.string.not_correct_pwd, Toast.LENGTH_SHORT).show();
              return;
            }
            break;
        }

        if (mSettingType != SettingType.CONFIRM_PASSWORD) {
          SettingHelper.setting(getActivity(), getRequestDataParams(), createSettingResponseListener(getActivity()));
        } else {
          Util.closeKeyboard(mFirstEditView);
          Intent intent = new Intent();
          intent.putExtra("password", mFirstEditView.getText().toString());
          getActivity().setResult(Activity.RESULT_OK, intent);
          getActivity().finish();
        }
        break;
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setToolbarTitle("프로필 설정", null);

    if (mSettingType == null) return;

    mFirstEditView.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(false),
        null);
    mSecondEditView.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(false),
        null);
    mThirdEditView.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(false),
        null);

    AnalyticsManager.sendScreenName(getCurrentScreenName());

    activateCompleteButton(false);

    switch (mSettingType) {
      case CHANGE_NAME:
        getToolbarHelper().getActionbar().setTitle(getString(R.string.change_name));
        mFirstEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        mFirstEditView.setHint(WatchaApp.getUser().getName());

        mNameValidator.registerValidator(mFirstEditView, this);

        mSecondEditView.setVisibility(View.GONE);
        mThirdEditView.setVisibility(View.GONE);

        isValidSecond = isValidThird = true;
        break;

      case CHANGE_EMAIL:
        getToolbarHelper().getActionbar().setTitle(getString(R.string.change_email));
        mFirstEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mFirstEditView.setHint(R.string.new_email);

        mSecondEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mSecondEditView.setHint(R.string.password);

        mThirdEditView.setVisibility(View.GONE);

        mEmailValidator.registerValidator(mFirstEditView, this);
        mPasswordValidator.registerValidator(mSecondEditView, this);
        break;

      case CHANGE_PASSWORD:
        getToolbarHelper().getActionbar().setTitle(getString(R.string.change_password));
        mFirstEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mFirstEditView.setHint(R.string.old_password);

        mFirstEditView.invalidate();

        mSecondEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mSecondEditView.setHint(R.string.new_password);

        mThirdEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mThirdEditView.setHint(R.string.input_new_password_again);

        mOldPasswordValidator.registerValidator(mFirstEditView, this);
        mPasswordValidator.registerValidator(mSecondEditView, this);
        mPasswordAgainValidator.registerValidator(mThirdEditView, this);
        break;

      case SET_PASSWORD:
        getToolbarHelper().getActionbar().setTitle(getString(R.string.set_password));
        mFirstEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mFirstEditView.setHint(R.string.password);

        mSecondEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mSecondEditView.setHint(R.string.verify_password);

        mPasswordValidator.registerValidator(mFirstEditView, this);
        mPasswordAgainValidator.registerValidator(mSecondEditView, this);

        mThirdEditView.setVisibility(View.GONE);
        isValidThird = true;
        break;

      case CONFIRM_PASSWORD:
        getToolbarHelper().getActionbar().setTitle(getString(R.string.confirm_password));
        mFirstEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mFirstEditView.setHint(R.string.password);

        mPasswordValidator.registerValidator(mFirstEditView, this);

        mSecondEditView.setVisibility(View.GONE);
        mThirdEditView.setVisibility(View.GONE);
        isValidSecond = true;
        isValidThird = true;
        break;

      case CHANGE_MESSAGE:
        getToolbarHelper().getActionbar().setTitle(getString(R.string.change_message));
        mFirstEditView.setInputType(
            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        mFirstEditView.setHint("상태메세지를 입력하세요");
        if (!TextUtils.isEmpty(WatchaApp.getUser().getBio())) {
          mFirstEditView.setText(WatchaApp.getUser().getBio());
        }

        mSecondEditView.setVisibility(View.GONE);
        mThirdEditView.setVisibility(View.GONE);

        mIsEnabled = true;
        activateCompleteButton(true);
        break;
    }

    mCompleteView.setOnClickListener(this);

    Util.openKeyboard(mFirstEditView);
  }

  @Override protected QueryType getQueryType() {
    return QueryType.SETTING;
  }

  @Override public void onValidation(FormValidator.FormStyle style, EditText v, String text,
      boolean isValid) {

    switch (v.getId()) {
      case R.id.edit1:
        isValidFirst = isValid;
        break;

      case R.id.edit2:
        isValidSecond = isValid;
        break;

      case R.id.edit3:
        isValidThird = isValid;
        break;
    }

    v.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(isValid), null);

    switch (mSettingType) {
      case CHANGE_EMAIL:
        mIsEnabled = isValidFirst && isValidSecond;
        break;

      default:
        mIsEnabled = isValidFirst && isValidSecond && isValidThird;
    }

    activateCompleteButton(mIsEnabled);
  }

  private void activateCompleteButton(boolean enabled) {
    mCompleteView.setEnabled(enabled);
    mCompleteView.setBackgroundResource(
        enabled ? R.drawable.bg_welcome_ok : R.drawable.bg_button_corner_gray_nor);
  }

  private FontIconDrawable getCheckIconDrawable(boolean isValid) {
    FontIconDrawable checkIcon = null;
    if (isValid) {
      checkIcon = FontIconDrawable.inflate(getActivity(), R.xml.icon_arrow_check);
      checkIcon.setTextColor(getResources().getColor(R.color.green));
      checkIcon.setTextSize(getResources().getDimension(R.dimen.more_font));
    }
    return checkIcon;
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return new OnBackKeyListener() {
      @Override public boolean onBackKeyDown() {
        Util.closeKeyboard(mFirstEditView);
        Util.closeKeyboard(mSecondEditView);
        Util.closeKeyboard(mThirdEditView);
        return true;
      }
    };
  }
}
