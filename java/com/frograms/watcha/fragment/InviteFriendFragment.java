package com.frograms.watcha.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import butterknife.Bind;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.FriendAdapter;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.models.Contact;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.LoadingView;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.kakao.AppActionBuilder;
import com.kakao.KakaoLink;
import com.kakao.KakaoParameterException;
import com.kakao.KakaoTalkLinkMessageBuilder;
import java.util.ArrayList;

public class InviteFriendFragment extends BaseFragment {

  private static final String MESSAGE = "\"저랑 함께 '왓챠' 하실래요?\"\n"
      + "당신만을 위한 영화, 드라마 정보는 물론,\n"
      + "지금 제일 핫한 리뷰들을 보실 수 있어요.\n"
      + "그럼 이제 왓챠에서 만나요!\n"
      + "자세한 앱 소개: http://wcha.it/1tKfILD";

  @Bind(R.id.friend_list) ListView mFriends;
  @Bind(R.id.friend_empty) SimpleEmptyView mEmptyView;

  private LoadingView mFooter;

  private ArrayList<Contact> mContacts;
  private FriendAdapter mFriendAdapter;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.INVITE_OTHERS;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("친구초대");
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    if (mFriendAdapter != null) {
      mFriendAdapter.clearAdapter();
    }
    mFriends = null;
    mContacts = null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_friends;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mApp = (WatchaApp) getActivity().getApplication();

    View kakaotalk =
        LayoutInflater.from(getActivity()).inflate(R.layout.view_kakaotalk, null, false);
    mFooter = new LoadingView(getActivity());

    mFriends.addHeaderView(kakaotalk);
    mFriends.addFooterView(mFooter);

    mContacts = new ArrayList<>();
    mFriendAdapter = new FriendAdapter(getActivity(), R.layout.view_friends, mContacts);
    mFriends.setAdapter(mFriendAdapter);

    mFriendAdapter.setOnInviteClickListener(v -> {
      int position = (Integer) v.getTag();
      if (mContacts.get(position).getPhone() == null) {
        return;
      }
      Uri uri = Uri.parse("smsto:" + mContacts.get(position).getPhone());
      Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
      intent.putExtra("sms_body", MESSAGE);
      startActivity(intent);
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL, AnalyticsActionType.SHARE_WATCHA)
          .setLabel(AnalyticsEvent.CONTACT)
          .build());
    });
    loadContacts();

    kakaotalk.setOnClickListener(v -> {
      try {
        KakaoLink kakaoLink = KakaoLink.getKakaoLink(getActivity().getApplicationContext());

        final KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder =
            kakaoLink.createKakaoTalkLinkMessageBuilder();

        kakaoTalkLinkMessageBuilder.addText(MESSAGE);
        kakaoTalkLinkMessageBuilder.addAppButton("왓챠로 이동",
            new AppActionBuilder().setUrl(BuildConfig.END_POINT).build());
        kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder.build(), getActivity());
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL, AnalyticsActionType.SHARE_WATCHA)
            .setLabel(AnalyticsEvent.KAKAOTALK)
            .build());
      } catch (KakaoParameterException e) {
        e.printStackTrace();
      }
    });
  }

  private void loadContacts() {
    Cursor cursor = getURI();
    if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
      mFooter.onLoading();
      for (int Loop1 = 0; Loop1 < cursor.getCount(); Loop1++) {
        if (cursor.getString(0) != null) {
          Contact contact = new Contact(cursor.getString(0), cursor.getString(1));
          mContacts.add(contact);
        }
        cursor.moveToNext();
      }
      mFooter.onEnd();
      mFriendAdapter.notifyDataSetChanged();
    } else {
      mFriends.removeFooterView(mFooter);
      mEmptyView.setVisibility(View.VISIBLE);
      mEmptyView.getIconView().setText(getString(R.string.icon_friends));
      mEmptyView.getTextView().setText("연락처를 불러올 수 없어요");
      mEmptyView.getIconView().setPadding(0, 0, 0, 0);
    }
  }

  private Cursor getURI() {
    Uri people = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

    String[] projection = new String[] {
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Contacts._ID
    };
    String sortOrder =
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

    return getActivity().getContentResolver().query(people, projection, null, null, sortOrder);
  }
}