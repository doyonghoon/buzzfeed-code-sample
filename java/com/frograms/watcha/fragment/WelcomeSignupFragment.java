package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.retrofit.QueryType;

/**
 * 회원가입 후에 환영 메세지 보여주는 화면.
 */
public class WelcomeSignupFragment extends BaseFragment {

  @Bind(R.id.welcome_title) TextView mTitle;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_welcome_signup;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getToolbar().setVisibility(View.GONE);
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.WELCOME;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    if (WatchaApp.getUser() != null) {
      mTitle.setText(
          String.format(getString(R.string.welcome_title), WatchaApp.getUser().getName()));
    }
    getActivity().setResult(BaseActivity.RESULT_DESTROY);
  }

  @OnClick(R.id.welcome_ok) public void onClickWelcome() {
    ActivityStarter.with(getActivity(), FragmentTask.TUTORIAL).start();
  }
}
