package com.frograms.watcha.fragment;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AlertHelper;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.UserListData;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.BitmapUtil;
import com.frograms.watcha.utils.FacebookProfileUpdateHelper;
import com.frograms.watcha.utils.ImageChooser;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.ProfileHeaderView;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.io.File;
import java.util.Collections;
import java.util.Map;

/**
 * 유저 페이지.
 */
public class ProfileFragment extends ListFragment<UserListData> implements View.OnClickListener, ImageChooser.OnImageChosenListener {

  private ProfileHeaderView mHeaderView = null;
  private User mUser;

  private static final int PROFILE_IMAGE_MAX_PX_SIZE = 200;

  private String mLastSeenId;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey("LAST_SEEN_ID")) {
      mLastSeenId = bundle.getString("LAST_SEEN_ID");
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.MY_PROFILE;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    if (mLastSeenId != null) {
      outState.putString("LAST_SEEN_ID", mLastSeenId);
    }
    if (mImageChooser != null) {
      WLog.i("stored variables: " + mImageChooser.getImageFilePath() + ", fileType: " + mImageChooser.getFileType());
      outState.putInt("image_type", mImageChooser.getFileType());
      outState.putString("image_path", mImageChooser.getImageFilePath());
    }

    if (mUser != null) {
      outState.putParcelable("user", mUser);
    }
  }

  @Override protected QueryType getQueryType() {
    if (WatchaApp.getUser() != null && !TextUtils.isEmpty(WatchaApp.getUser().getCode())) {
      return QueryType.USER.setApi(WatchaApp.getUser().getCode());
    }
    return null;
  }

  @Override protected Map<String, String> getRequestDataParams() {
    return Collections.singletonMap("last_seen_id", mLastSeenId);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override protected boolean enableSwipeRefresh() {
    return true;
  }

  @Override public void onResume() {
    super.onResume();
    if (mHeaderView != null
        && mUser != null
        && WatchaApp.getUser() != null
        && mUser.getCode()
        .equals(WatchaApp.getUser().getCode())) {
      mUser = WatchaApp.getUser();
      mHeaderView.setUser(WatchaApp.getUser());
      int movieCount = mUser.getActionCount(CategoryType.MOVIES.getApiPath()).getRatings();
      int dramaCount = mUser.getActionCount(CategoryType.TV_SEASONS.getApiPath()).getRatings();
      mHeaderView.updateShortcutCount(movieCount, dramaCount);
    }
  }

  @Override public View getListHeaderView() {
    return getMypageHomeHeaderView();
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<UserListData> result) {
    super.onSuccess(queryType, result);
    if (mLastSeenId == null) {
      getMypageHomeHeaderView().setVisibility(View.VISIBLE);
      mUser = result.getData().getUser();
      getMypageHomeHeaderView().setUser(mUser);

      if (mUser.getCode().equals(WatchaApp.getUser().getCode()) && mUser.getRatingCounts() >= 100) {
        AlertHelper.showAlert(getActivity(),
            AlertHelper.AlertType.TASTE.setUserCode(mUser.getCode()));
      }
    }

    mLastSeenId = result.getData().getLastSeenId();
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if (savedInstanceState != null) {
      if (savedInstanceState.containsKey("user")) {
        mUser = savedInstanceState.getParcelable("user");
        getMypageHomeHeaderView().setVisibility(View.VISIBLE);
        getMypageHomeHeaderView().setUser(mUser);
      }

      if (savedInstanceState.containsKey("image_path")
          && savedInstanceState.containsKey("image_type")) {
        @QueryFile.QueryFileKey int imageType = savedInstanceState.getInt("image_type");
        String imagePath = savedInstanceState.getString("image_path");
        WLog.i("imagePath: " + imagePath + ", imageType: " + imageType);
        mImageChooser = new ImageChooser(getBaseActivity(), imageType);
        mImageChooser.setImageFilePath(savedInstanceState.getString("image_path"));
      }
    }
  }

  protected ProfileHeaderView getMypageHomeHeaderView() {
    if (mHeaderView == null) {
      mHeaderView = new ProfileHeaderView(getActivity());
      mHeaderView.getUserImageProfile().setOnClickListener(this);
      mHeaderView.getCover().setOnClickListener(this);
      mHeaderView.setVisibility(View.GONE);
    }
    return mHeaderView;
  }

  @Override public void onClick(View v) {
    switch (v.getId()) {
      case R.id.profile_image:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT,
            WatchaApp.getUser().getCode().equals(mUser.getCode())
                ? AnalyticsActionType.BUTTON_MY_PROFILE_PICTURE
                : AnalyticsActionType.BUTTON_PROFILE_PICTURE).build());

        if (WatchaApp.getUser().getCode().equals(mUser.getCode())) {
          showChangeProfileDialog();
        } else {
          // 확대보기
          if (mUser.getPhoto() != null && !TextUtils.isEmpty(mUser.getPhoto().getOriginal())) {
            ActivityStarter.with(getActivity(), FragmentTask.GALLERY)
                .addBundle(new BundleSet.Builder().putImageUrl(mUser.getPhoto().getOriginal())
                    .putGalleryType(GalleryFragment.GalleryType.PROFILE_PICTURE)
                    .build()
                    .getBundle())
                .start();
          }
        }
        break;
      case R.id.profile_cover_image:
        boolean isMe = WatchaApp.getUser().getCode().equals(mUser.getCode());
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT,
            isMe ? AnalyticsActionType.BUTTON_MY_PROFILE_COVER
                : AnalyticsActionType.BUTTON_PROFILE_COVER).build());
        if (isMe) {
          showChangeCoverDialog();
        } else if (!isMe && !TextUtils.isEmpty(mUser.getCover())) {
          ActivityStarter.with(getActivity(), FragmentTask.GALLERY)
              .addBundle(new BundleSet.Builder().putImageUrl(mUser.getCover())
                  .putGalleryType(GalleryFragment.GalleryType.PROFILE_COVER)
                  .build()
                  .getBundle())
              .start();
        }
        break;
    }
  }

  private MaterialDialog mProgressDialog = null;

  private void getProfileFromFacebook(@QueryFile.QueryFileKey final int type) {
    if (mProgressDialog == null) {
      mProgressDialog =
          new MaterialDialog.Builder(getActivity()).content(getString(R.string.get_photo_from_fb))
              .progress(true, 0)
              .theme(Theme.LIGHT)
              .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
                  FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
              .build();
    }
    new FacebookProfileUpdateHelper.Builder(getActivity(), type).setDialog(mProgressDialog)
        .setSettingResponseListener(getSettingResponseListener())
        .build()
        .load();
    mProgressDialog.show();
  }

  private void showChangeCoverDialog() {
    String title_cover;
    String[] strs_cover;

    if (WatchaApp.getUser().getCover() != null) {
      title_cover = getString(R.string.changing_cover);
      strs_cover = getResources().getStringArray(R.array.changing_cover_array);
    } else {
      title_cover = getString(R.string.setting_cover);
      strs_cover = getResources().getStringArray(R.array.setting_cover_array);
    }

    FakeActionBar header_cover = new FakeActionBar(getActivity());
    header_cover.setTitle(title_cover);
    ArrayAdapter<String> simpleCoverAdapter =
        new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs_cover);
    DialogPlus coverDialog = new DialogPlus.Builder(getActivity()).setHeader(header_cover)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleCoverAdapter)
        .setGravity(DialogPlus.Gravity.BOTTOM)
        .setCancelable(true)
        .setOnItemClickListener((dialogPlus, o, view, i) -> {
          dialogPlus.dismiss();
          if (WatchaApp.getUser().getCover() != null) {
            i--;
          }
          switch (i - 1) {
            case -1:
              ActivityStarter.with(getActivity(), FragmentTask.GALLERY)
                  .addBundle(new BundleSet.Builder().putImageUrl(WatchaApp.getUser().getCover())
                      .putGalleryType(GalleryFragment.GalleryType.PROFILE_COVER)
                      .build()
                      .getBundle())
                  .start();
              break;

            case 0:
              getImageChooser(QueryFile.COVER_PHOTO).requestTakePicture();
              break;
            case 1:
              getImageChooser(QueryFile.COVER_PHOTO).requestBringPictureFromGallery();
              break;

            case 2:
              getProfileFromFacebook(QueryFile.COVER_PHOTO);
              break;

            case 3:
              SettingHelper.deleteCover(getActivity(), getSettingResponseListener());
              break;
          }
        })
        .create();

    coverDialog.show();
  }

  /**
   * 조건1: 커버, 프로필 사진
   * 조건2: 사진을 교체하는 것인가, 처음 고르는 것인가
   *
   * 메뉴엔 조건이 있다.
   * 사진을 갖고온다.
   * 크롭할 사이즈가 다르다.
   * 비트맵을 리턴해준다.
   */
  private void showChangeProfileDialog() {
    String title;
    String[] strs;

    if (WatchaApp.getUser().getPhoto() != null) {
      title = getString(R.string.changing_profile);
      strs = getResources().getStringArray(R.array.changing_profile_array);
    } else {
      title = getString(R.string.setting_profile);
      strs = getResources().getStringArray(R.array.setting_profile_array);
    }

    FakeActionBar header = new FakeActionBar(getActivity());
    header.setTitle(title);
    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.BOTTOM)
        .setCancelable(true)
        .setOnItemClickListener((dialogPlus, o, view, i) -> {
          dialogPlus.dismiss();
          if (WatchaApp.getUser().getPhoto() != null) {
            i--;
          }
          switch (i - 1) {
            case -1:
              ActivityStarter.with(getActivity(), FragmentTask.GALLERY)
                  .addBundle(new BundleSet.Builder().putImageUrl(WatchaApp.getUser()
                      .getPhoto()
                      .getOriginal())
                      .putGalleryType(GalleryFragment.GalleryType.PROFILE_PICTURE)
                      .build()
                      .getBundle())
                  .start();
              break;

            case 0:
              getImageChooser(QueryFile.PROFILE_PHOTO).requestTakePicture();
              break;
            case 1:
              getImageChooser(QueryFile.PROFILE_PHOTO).requestBringPictureFromGallery();
              break;

            case 2:
              getProfileFromFacebook(QueryFile.PROFILE_PHOTO);
              break;

            case 3:
              SettingHelper.deletePhoto(getActivity(), getSettingResponseListener());
              break;
          }
        })
        .create();

    dialog.show();
  }

  private SettingHelper.OnSettingResponseListener mListener = null;

  private SettingHelper.OnSettingResponseListener getSettingResponseListener() {
    if (mListener == null) {
      mListener = new SettingHelper.OnSettingResponseListener(getActivity()) {
        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
          super.onSuccess(queryType, result);
          Toast.makeText(getActivity(), getString(R.string.saved), Toast.LENGTH_SHORT).show();
          mUser = result.getData();
          getMypageHomeHeaderView().setUser(mUser);

          if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
          }
        }
      };
    }
    return mListener;
  }

  @Override public void onImageChosenCallback(int fileType, Uri bitmapUri,
      ImageChooser.ImageUriParser parser) {
    Bitmap bm = parser.getBitmapFromUri(getActivity(), bitmapUri);
    switch (fileType) {
      case QueryFile.PROFILE_PHOTO:
        if (bm != null && bm.getWidth() > PROFILE_IMAGE_MAX_PX_SIZE
            || bm != null && bm.getHeight() > PROFILE_IMAGE_MAX_PX_SIZE) {
          bm = BitmapUtil.scaleDown(bm, PROFILE_IMAGE_MAX_PX_SIZE);
        }
        break;

      case QueryFile.COVER_PHOTO:
        break;
    }
    File f = parser.getFileFromBitmap("photo_" + System.currentTimeMillis(), bm);
    switch (fileType) {
      case QueryFile.PROFILE_PHOTO:
        SettingHelper.uploadPhoto(getActivity(), new QueryFile(QueryFile.PROFILE_PHOTO, f),
            getSettingResponseListener());
        break;

      case QueryFile.COVER_PHOTO:
        SettingHelper.uploadCover(getActivity(), new QueryFile(QueryFile.COVER_PHOTO, f),
            getSettingResponseListener());
        break;
    }
  }

  @Override public void refresh() {
    mLastSeenId = null;
    super.refresh();
  }
}
