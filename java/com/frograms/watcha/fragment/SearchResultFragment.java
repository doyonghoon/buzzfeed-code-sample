package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.EmptyViewPresenter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.SearchResultData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.SimpleEmptyView;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import java.util.Map;

/**
 * 검색 결과화면.
 */
public class SearchResultFragment extends ListFragment<SearchResultData> implements
    EmptyViewPresenter {

  private QueryType mQueryType = null;
  private String mQuery = null;
  private boolean mLoadNextPage = false;

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    mQuery = bundle.getString(BundleSet.SEARCH_QUERY);
    mQueryType = QueryType.SEARCH_QUERY;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.SEARCH_RESULT;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.SEARCH_QUERY, mQuery);
  }

  @Override protected Map<String, String> getRequestDataParams() {
    mAddLoadMoreParams.put("query", mQuery);
    mAddLoadMoreParams.put("page", mPage + "");
    return mAddLoadMoreParams;
  }

  @Override protected DataProvider getDataProvider() {
    return new DataProvider(getActivity(), mQueryType, createReferrerBuilder()).responseTo(this)
        .withParams(getRequestDataParams());
  }

  /**
   * mQuery 와 mPage 값을 넘겨주기 위해서 일단 false 로 오버라이드 함.
   */
  @Override protected boolean shouldRetriveDataImmediately() {
    return mLoadNextPage;
  }

  @Override protected QueryType getQueryType() {
    return mQueryType;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle(null);
    getToolbarHelper().getActionbar().setSubtitle(null);
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    requestData(getDataProvider());
  }

  @Override public void onSuccess(@NonNull QueryType queryType,
      @NonNull BaseResponse<SearchResultData> result) {
    super.onSuccess(queryType, result);
    if (mPage == 1) {
      mLoadNextPage = true;
    }
    if (result.getData() != null) {
      getToolbarHelper().setToolbarTitle(result.getData().getTitle(),
          result.getData().getSubtitle());
    }
  }

  @Override public View getEmptyView() {
    return new SimpleEmptyView(getActivity(), R.string.icon_search,
        "검색 결과가 없습니다. 다른 검색어를 입력해 보세요.");
  }
}
