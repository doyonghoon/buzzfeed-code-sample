package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import butterknife.Bind;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

/**
 * 페이스북으로 공유하는 화면.
 */
public class FacebookShareMessageFragment extends BaseFragment {

  @Bind(R.id.share_message) EditText mShareMessageText;
  private String mUrl = null;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_share_message_editor_facebook;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("페이스북에 공유하기");
    getToolbarHelper().getToolbar()
        .setNavigationIcon(FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back));
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.WEB_URL)) {
      mUrl = bundle.getString(BundleSet.WEB_URL);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(BundleSet.WEB_URL, mUrl);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_text_complete:
        //Feed feed = buildFeed(mShareMessageText.getText().toString(), mUrl);
        //FacebookBroker.publish(getActivity(), feed, new OnPublishListener() {
        //  @Override public void onException(Throwable throwable) {
        //    super.onException(throwable);
        //    if (mDialog != null && mDialog.isShowing()) {
        //      mDialog.dismiss();
        //    }
        //    Toast.makeText(getActivity(), "공유하지 못했습니다!", Toast.LENGTH_SHORT).show();
        //  }
        //
        //  @Override public void onFail(String reason) {
        //    super.onFail(reason);
        //    if (mDialog != null && mDialog.isShowing()) {
        //      mDialog.dismiss();
        //    }
        //    Toast.makeText(getActivity(), "공유하지 못했습니다!", Toast.LENGTH_SHORT).show();
        //  }
        //
        //  @Override public void onThinking() {
        //    super.onThinking();
        //    showLoadingDialog();
        //  }
        //
        //  @Override public void onComplete(String response) {
        //    super.onComplete(response);
        //    Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
        //    getActivity().finish();
        //  }
        //});
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public MenuItems getToolbarMenuItems() {
    return MenuItems.COMPLETE;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  //private Feed buildFeed(String message, String link) {
  //  Feed.Builder builder = new Feed.Builder();
  //  if (!TextUtils.isEmpty(link)) {
  //    builder.setLink(link);
  //  }
  //  if (!TextUtils.isEmpty(message)) {
  //    builder.setMessage(message);
  //  }
  //  Privacy privacy =
  //      new Privacy.Builder().setPrivacySettings(Privacy.PrivacySettings.EVERYONE).build();
  //  builder.setPrivacy(privacy);
  //  return builder.build();
  //}

  private MaterialDialog mDialog = null;

  private void showLoadingDialog() {
    if (mDialog == null) {
      mDialog = new MaterialDialog.Builder(getActivity()).content("공유 중..")
          .progress(true, 0)
          .theme(Theme.LIGHT)
          .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
              FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
          .build();
    }
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override public void run() {
        mDialog.show();
      }
    });
  }
}
