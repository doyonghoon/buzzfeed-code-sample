package com.frograms.watcha.fragment;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.SimpleFacebookUser;
import com.frograms.watcha.model.UserExistenceData;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.models.FacebookProfile;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.ColorUtils;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.GifLoader;
import com.frograms.watcha.view.CircularRevealView;
import com.frograms.watcha.view.textviews.RobotoRegularView;
import java.util.Collections;
import rx.functions.Action1;

/**
 * 이쁜 배경 이미지 깔고 로그인, 회원가입 버튼들 보여주는 화면.
 */
public class IntroFragment extends BaseFragment {

  private final float SCREEN_HEIGHT_RATE_FOR_LOGO_TOP_PADDING = 0.15f;
  private final float SCREEN_HEIGHT_RATE_FOR_DIVIER_VIEW_HEIGHT = 0.05f;
  private final String GIF_FILE_NAME = "intro_background.gif";

  //로고와 문구가 들어있는 레이아웃
  @Bind(R.id.logo_container) LinearLayout mLogoContainer;
  //Gif 이미지가 들어가게 될 이미지뷰
  @Bind(R.id.background) ImageView mBackgroundGifImgView;
  //로그인 문구와 회원가입버튼 레이아웃 사이에 여백을 주기 위한 용도의 View
  @Bind(R.id.divider) View mDividerView;

  @Bind(R.id.login) RobotoRegularView mLoginTextView;
  @Bind(R.id.btn_fb) View mFacebookBtn;
  @Bind(R.id.btn_email) View mEmailBtn;
  @Bind(R.id.reveal) CircularRevealView mRevealView;

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_intro;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getToolbar().setVisibility(View.GONE);
  }

  @Override protected int getToolbarColor() {
    return R.color.white;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.INTRO;
  }


  @Override public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup,
      Bundle paramBundle) {
    View view = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    //BaseFragment에서 레이아웃을 View객체로 받아온 이후 실행되는 코드

    //스크린의 height을 받아온다.
    int screenHeight = getScreenHeightPixels();

    //mLogoContainer에 일정 비율로 패딩을 준다.
    int topPaddingHeight = (int)(screenHeight * SCREEN_HEIGHT_RATE_FOR_LOGO_TOP_PADDING);
    mLogoContainer.setPadding(0, topPaddingHeight, 0, 0);

    //mDividerView에 일정 비율로 height을 준다.
    mDividerView.getLayoutParams().height = (int)(screenHeight * SCREEN_HEIGHT_RATE_FOR_DIVIER_VIEW_HEIGHT);

    return view;
  }

  //스크린의 height을 받아온다.
  private int getScreenHeightPixels(){
    DisplayMetrics metrics = new DisplayMetrics();
    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
    return metrics.heightPixels;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    //Gif파일 로드 시작
    GifLoader.with(getActivity())
        .load(getActivity().getAssets(), GIF_FILE_NAME)
        .into(mBackgroundGifImgView);

    //로고 레이아웃을 View계층 최상단으로 올린다.
    mLogoContainer.bringToFront();

    //윈도우 설정을 전체화면으로 한다.
    getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);

    //인트로 화면이 보인다는 건 로그인한 유저가 아님.
    FacebookBroker.destroyAccessToken();
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @OnClick(R.id.login) void onClickLogin() {
    startActivityReveal(mLoginTextView, FragmentTask.LOGIN);
  }

  @OnClick(R.id.btn_email) void onClickEmailBtn() {
    startActivityReveal(mEmailBtn, FragmentTask.SIGNUP);
  }

  private AccessToken fbToken;
  @OnClick(R.id.btn_fb) void onClickFacebookBtn() {

    FacebookBroker.accessTokenObservable(getActivity())
        .flatMap(accessToken -> {
          fbToken = accessToken;
          return FacebookBroker.getInstance().profileObservable(getActivity());
        })
        .subscribe(new Action1<FacebookProfile>() {
          @Override public void call(final FacebookProfile facebookProfile) {
            final String email = facebookProfile.getEmail();
            final String name = facebookProfile.getName();
            final String uid = facebookProfile.getId();

            DataProvider<BaseResponse<UserExistenceData>> provider =
                new DataProvider<>(getActivity(), QueryType.EXIST_USER, createReferrerBuilder());
            provider.withParams(Collections.singletonMap("fb_token", fbToken.getToken()))
                .withDialogMessage("가입 확인 중..")
                .responseTo(new ApiResponseListener<BaseResponse<UserExistenceData>>() {
                  @Override public void onSuccess(@NonNull QueryType queryType,
                      @NonNull BaseResponse<UserExistenceData> result) {
                    if (result.getData() != null) {
                      if (!result.getData().isExist()) {
                        // 유저가 왓챠에 존재하지 않을 때만 회원가입 화면으로 보냄.
                        SimpleFacebookUser facebookUser = new SimpleFacebookUser.Builder(
                            AccessToken.getCurrentAccessToken().getToken()).setEmail(email)
                            .setProfileUrl(facebookProfile.getPictureUrl())
                            .setName(name)
                            .setCoverUrl(facebookProfile.getCoverUrl())
                            .build();

                        ActivityStarter.with(getActivity(), FragmentTask.SIGNUP)
                            .addBundle(new BundleSet.Builder().putRevealAnimation(false)
                                .putSimpleFacebookUser(facebookUser)
                                .build()
                                .getBundle())
                            .start();
                      } else {
                        String watchaEmail = result.getData().getEmail();

                        // 이미 가입한 회원이면 알러트로 안내하고 로그인 화면으로 보냄.
                        new MaterialDialog.Builder(getActivity()).content(
                            String.format("이미 '%s' 로 가입한 왓챠 계정이 있어요. 로그인을 해주세요.", watchaEmail))
                            .positiveText("로그인 화면으로 이동하기")
                            .callback(new MaterialDialog.ButtonCallback() {
                              @Override public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ActivityStarter.with(getActivity(), FragmentTask.LOGIN)
                                    .addBundle(new BundleSet.Builder().putRevealAnimation(false)
                                        .build()
                                        .getBundle())
                                    .start();
                              }
                            })
                            .show();
                      }
                    }
                  }
                })
                .request();
          }
        });
  }

  /**
   * Reveal 애니메이션 효과로 화면 전환했었다면, 돌아왔을때 reveal 에 backgroundColor 가 깔려있을테니, {@link
   * CircularRevealView#hide(int, int, int, Animator.AnimatorListener)} 시켜줘야 함.
   * {@link LoginFragment#getOnBackKeyListener()}, {@link SignupFragment#getOnBackKeyListener()} 두
   * Fragment 에 reveal 값을 넘기도록 했음.
   */
  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (data != null
        && data.hasExtra("reveal")
        && data.getBooleanExtra("reveal", false)
        && resultCode == Activity.RESULT_OK) {
      Handler handler = new Handler();
      handler.postDelayed(() -> {
        final Point p = getLocationInView(mRevealView, mSelectedRevealAnchorView);
        if (p != null) {
          mRevealView.hide(p.x, p.y, Color.TRANSPARENT, 0, 330, null);
        }
      }, 370);
    }
  }

  private View mSelectedRevealAnchorView = null;

  /**
   * Reveal 애니메이션을 어느 위치에서 시작할지 알아냄.
   *
   * @return 애니메이션 시작 위치.
   */
  @Nullable private Point getLocationInView(View src, View target) {
    if (target == null || src == null) {
      return null;
    }
    final int[] l0 = new int[2];
    src.getLocationOnScreen(l0);

    final int[] l1 = new int[2];
    target.getLocationOnScreen(l1);

    l1[0] = l1[0] - l0[0] + target.getWidth() / 2;
    l1[1] = l1[1] - l0[1] + target.getHeight() / 2;

    return new Point(l1[0], l1[1]);
  }

  /**
   * 뒤로가기 버튼 눌릴때 콜백받고 싶으면 이걸 오버라이드하면 됨.
   *
   * @return 뒤로가기 버튼 눌릴때 콜백받을 리스너.
   */
  @Override public OnBackKeyListener getOnBackKeyListener() {
    return () -> {
      getActivity().setResult(BaseActivity.RESULT_DESTROY);
      getActivity().finish();
      return true;
    };
  }

  private void startActivityReveal(View anchor, final FragmentTask frag) {
    final int color = getResources().getColor(R.color.background);
    final Point p = getLocationInView(mRevealView, mSelectedRevealAnchorView = anchor);
    mRevealView.reveal(p.x, p.y, color, anchor.getHeight()
            / 2, 440, new Animator.AnimatorListener() {
          @Override public void onAnimationStart(Animator animation) {

          }

          @Override public void onAnimationEnd(Animator animation) {
            ActivityStarter.with(getActivity(), frag)
                .setAnimationType(ActivityStarter.AnimationType.NOTHING)
                .addBundle(new BundleSet.Builder().putRevealAnimation(true).build().getBundle())
                .start();
          }

          @Override public void onAnimationCancel(Animator animation) {

          }

          @Override public void onAnimationRepeat(Animator animation) {

          }
        });
  }
}
