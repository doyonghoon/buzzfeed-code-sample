package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.retrofit.QueryType;

/**
 * 유저의 컬렉션 상세화면.
 */
public class DetailDeckFragment extends ListFragment<ListData>
    implements RecyclerAdapter.OnRemoveCardItemListener {

  private String mDeckCode = null;
  private boolean mShouldGoReply = false;

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle.containsKey(BundleSet.DECK_CODE)) {
      mDeckCode = bundle.getString(BundleSet.DECK_CODE, null);
    }

    if (bundle.containsKey(BundleSet.OPEN_KEYBOARD)) {
      mShouldGoReply = bundle.getBoolean(BundleSet.OPEN_KEYBOARD);
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.DECK_DETAIL;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putString(BundleSet.DECK_CODE, mDeckCode);
  }

  @Override public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup,
      Bundle paramBundle) {
    getToolbarHelper().setToolbarTitle(getString(R.string.deck_detail_page));
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mAdapter.setOnRemoveCardItemListener(this);

  }

  @Override protected QueryType getQueryType() {
    return QueryType.DECK_DETAIL.setApi(mDeckCode);
  }

  @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == FragmentTask.CREATE_DECK.getValue() && resultCode == Activity.RESULT_OK) {
      refresh();
    }
  }

  @Override public void onRemoveCardItem(int position) {
    getActivity().finish();
  }

  @Override public void onSuccess(@NonNull QueryType queryType, @NonNull final BaseResponse<ListData> result) {
    super.onSuccess(queryType, result);

    new Handler().postDelayed(new Runnable() {

      @Override public void run() {
        if (mShouldGoReply) {
          ActivityStarter.with(getActivity(), FragmentTask.REPLY)
              .addBundle(new BundleSet.Builder().putReplyType(ReplyFragment.ReplyType.DECK)
                  .putDeckCode(mDeckCode)
                  .build()
                  .getBundle())
                  .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
                  .start();

          mShouldGoReply = false;
        }
      }
    }, 500);
  }
}