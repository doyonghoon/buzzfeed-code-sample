package com.frograms.watcha.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import butterknife.Bind;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.helpers.SettingHelper.OnSettingResponseListener;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.BitmapUtil;
import com.frograms.watcha.utils.FacebookProfileUpdateHelper;
import com.frograms.watcha.utils.ImageChooser;
import com.frograms.watcha.view.SettingItemView;
import com.frograms.watcha.view.widget.wImages.WImageView;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ChangeProfileFragment extends BaseFragment<BaseResponse<User>>
    implements View.OnClickListener, ImageChooser.OnImageChosenListener {

  @Bind(R.id.name) SettingItemView mNameView;
  @Bind(R.id.message) SettingItemView mMessageView;
  @Bind(R.id.change_profile) SettingItemView mChangeProfileView;
  @Bind(R.id.profile) WImageView mProfileView;
  @Bind(R.id.change_cover) SettingItemView mChangeCoverView;
  @Bind(R.id.cover) WImageView mCoverView;
  @Bind(R.id.certify) SettingItemView mCertifyView;
  @Bind(R.id.email) SettingItemView mEmailView;
  @Bind(R.id.password) SettingItemView mPasswordView;

  private MaterialDialog mProgressDialog;

  private OnSettingResponseListener createSettingResponseListener(Context context) {
    return new OnSettingResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);
        Toast.makeText(getActivity(), getString(R.string.saved), Toast.LENGTH_SHORT).show();
        setProfile();
      }
    };
  }

  @Override public ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.SETTING_FOR_PROFILE;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_profile;
  }

  @Override protected QueryType getQueryType() {
    return QueryType.PROFILE_SETTING;
  }

  private void getProfileFromFacebook(@QueryFile.QueryFileKey final int type) {
    if (mProgressDialog == null) {
      mProgressDialog =
          new MaterialDialog.Builder(getActivity()).content(getString(R.string.get_photo_from_fb))
              .progress(true, 0)
              .theme(Theme.LIGHT)
              .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(), FontHelper.FontType.ROBOTO_REGULAR
                  .getTypeface())
              .build();
    }

    new FacebookProfileUpdateHelper.Builder(getActivity(), type).setDialog(mProgressDialog)
        .setSettingResponseListener(createSettingResponseListener(getActivity()))
        .build()
        .load();
    //mProgressDialog.show();
  }

  @Override public void onClick(View v) {

    switch (v.getId()) {
      case R.id.profile:
      case R.id.change_profile:
        String title;
        String[] strs;

        if (WatchaApp.getUser().getPhoto() != null) {
          title = getString(R.string.changing_profile);
          strs = getResources().getStringArray(R.array.changing_profile_array);
        } else {
          title = getString(R.string.setting_profile);
          strs = getResources().getStringArray(R.array.setting_profile_array);
        }

        FakeActionBar header = new FakeActionBar(getActivity());
        header.setTitle(title);
        ArrayAdapter<String> simpleAdapter =
            new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1, strs);
        DialogPlus dialog = new DialogPlus.Builder(getActivity()).setHeader(header)
            .setContentHolder(new ListHolder())
            .setAdapter(simpleAdapter)
            .setGravity(DialogPlus.Gravity.BOTTOM)
            .setCancelable(true)
            .setOnItemClickListener((dialogPlus, o, view, i) -> {
              dialogPlus.dismiss();

              if (WatchaApp.getUser().getPhoto() != null) {
                i--;
              }

              switch (i - 1) {
                case -1:
                  ActivityStarter.with(getActivity(), FragmentTask.GALLERY)
                      .addBundle(new BundleSet.Builder().putImageUrl(WatchaApp.getUser()
                          .getPhoto()
                          .getOriginal())
                          .putGalleryType(GalleryFragment.GalleryType.PROFILE_PICTURE)
                          .build()
                          .getBundle())
                      .start();
                  break;

                case 0:
                  getImageChooser(QueryFile.PROFILE_PHOTO).requestTakePicture();
                  break;
                case 1:
                  getImageChooser(QueryFile.PROFILE_PHOTO).requestBringPictureFromGallery();
                  break;

                case 2:
                  if (WatchaApp.getUser().isFbConnected()) {
                    getProfileFromFacebook(QueryFile.PROFILE_PHOTO);
                  } else {
                    Toast.makeText(getActivity(), R.string.not_connect_fb, Toast.LENGTH_SHORT)
                        .show();
                  }
                  break;

                case 3:
                  SettingHelper.deletePhoto(getActivity(), createSettingResponseListener(getActivity()));
                  break;
              }
            })
            .create();

        dialog.show();
        break;

      case R.id.name:
        ActivityStarter.with(getActivity(), FragmentTask.CHANGE_INFO)
            .addBundle(
                new BundleSet.Builder().putSettingType(ChangeInfoFragment.SettingType.CHANGE_NAME)
                    .build()
                    .getBundle())
            .start();
        break;

      case R.id.message:
        ActivityStarter.with(getActivity(), FragmentTask.CHANGE_INFO)
            .addBundle(
                new BundleSet.Builder().putSettingType(ChangeInfoFragment.SettingType.CHANGE_MESSAGE)
                    .build()
                    .getBundle())
            .start();
        break;

      case R.id.change_cover:
        String title_cover;
        String[] strs_cover;

        if (WatchaApp.getUser().getCover() != null) {
          title_cover = getString(R.string.changing_cover);
          strs_cover = getResources().getStringArray(R.array.changing_cover_array);
        } else {
          title_cover = getString(R.string.setting_cover);
          strs_cover = getResources().getStringArray(R.array.setting_cover_array);
        }

        FakeActionBar header_cover = new FakeActionBar(getActivity());
        header_cover.setTitle(title_cover);
        ArrayAdapter<String> simpleCoverAdapter =
            new ArrayAdapter<>(getActivity(), R.layout.view_list_item_basic, R.id.text1,
                strs_cover);
        DialogPlus coverDialog = new DialogPlus.Builder(getActivity()).setHeader(header_cover)
            .setContentHolder(new ListHolder())
            .setAdapter(simpleCoverAdapter)
            .setGravity(DialogPlus.Gravity.BOTTOM)
            .setCancelable(true)
            .setOnItemClickListener((dialogPlus, o, view, i) -> {
              dialogPlus.dismiss();

              if (WatchaApp.getUser().getCover() != null) {
                i--;
              }

              switch (i - 1) {
                case -1:
                  ActivityStarter.with(getActivity(), FragmentTask.GALLERY)
                      .addBundle(new BundleSet.Builder().putImageUrl(WatchaApp.getUser().getCover())
                              .putGalleryType(GalleryFragment.GalleryType.PROFILE_COVER)
                              .build()
                              .getBundle())
                      .start();
                  break;

                case 0:
                  getImageChooser(QueryFile.COVER_PHOTO).requestTakePicture();
                  break;
                case 1:
                  getImageChooser(QueryFile.COVER_PHOTO).requestBringPictureFromGallery();
                  break;

                case 2:
                  getProfileFromFacebook(QueryFile.COVER_PHOTO);
                  break;

                case 3:
                  SettingHelper.deleteCover(getActivity(), createSettingResponseListener(getActivity()));
                  break;
              }
            })
            .create();

        coverDialog.show();
        break;

      case R.id.certify:
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.email_certify_title)
            .setMessage(R.string.email_certify_msg)
            .setPositiveButton(R.string.yes, (dialog1, which) -> {
              DataProvider<BaseResponse> provider =
                  new DataProvider<>(getActivity(), QueryType.VERIFY, createReferrerBuilder()).withDialogMessage(
                      getString(R.string.sending_mail))
                      .responseTo((queryType, result) -> {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setMessage(R.string.verify_completed)
                            .setPositiveButton(R.string.ok, null);

                        builder1.show();
                      });

              provider.request();
            })
            .setNegativeButton(R.string.no, null);

        builder.show();
        break;

      case R.id.email:
        ActivityStarter.with(getActivity(), FragmentTask.CHANGE_INFO)
            .addBundle(
                new BundleSet.Builder().putSettingType(ChangeInfoFragment.SettingType.CHANGE_EMAIL)
                    .build()
                    .getBundle())
            .start();
        break;

      case R.id.password:
        ActivityStarter.with(getActivity(), FragmentTask.CHANGE_INFO)
            .addBundle(new BundleSet.Builder().putSettingType(
                WatchaApp.getUser().isInitialized() ? ChangeInfoFragment.SettingType.CHANGE_PASSWORD
                    : ChangeInfoFragment.SettingType.SET_PASSWORD).build().getBundle())
            .start();
        break;
    }
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    setToolbarTitle(getString(R.string.profile_setting), null);

    mProfileView.setOnClickListener(this);
    mNameView.setOnClickListener(this);
    mMessageView.setOnClickListener(this);
    mChangeProfileView.setOnClickListener(this);
    mChangeCoverView.setOnClickListener(this);
    mEmailView.setOnClickListener(this);
    mPasswordView.setOnClickListener(this);
  }

  @Override public void onResume() {
    super.onResume();

    setProfile();
  }

  private void setProfile() {
    final String profileUrl =
        WatchaApp.getUser().getPhoto() != null ? WatchaApp.getUser().getPhoto().getLarge() : null;
    mProfileView.setOnFinishLoadingImageListener(new WImageView.OnFinishLoadingImageListener() {
      @Override public void onFinishLoadingImage(@Nullable Bitmap bitmap, WImageView view) {
        mProfileView.setVisibility(
            bitmap == null && TextUtils.isEmpty(profileUrl) ? View.GONE : View.VISIBLE);
      }
    });
    mProfileView.load(profileUrl);

    final String coverUrl =
        WatchaApp.getUser().getCover() != null ? WatchaApp.getUser().getCover() : null;
    mCoverView.setOnFinishLoadingImageListener(new WImageView.OnFinishLoadingImageListener() {
      @Override public void onFinishLoadingImage(@Nullable Bitmap bitmap, WImageView view) {
        mCoverView.setVisibility(
            bitmap == null && TextUtils.isEmpty(coverUrl) ? View.GONE : View.VISIBLE);
      }
    });
    mCoverView.load(coverUrl);

    mNameView.setDescription(WatchaApp.getUser().getName());
    if (!TextUtils.isEmpty(WatchaApp.getUser().getBio())) {
      mMessageView.setDescription(WatchaApp.getUser().getBio());
    } else {
      mMessageView.setDescription("");
    }

    mChangeProfileView.setTitle(getString(
        (WatchaApp.getUser().getPhoto() != null) ? R.string.changing_profile
            : R.string.setting_profile));

    mChangeCoverView.setTitle(getString(
        (WatchaApp.getUser().getCover() != null) ? R.string.changing_cover
            : R.string.setting_cover));

    if (WatchaApp.getUser().getEmail() == null) {
      mCertifyView.setVisibility(View.GONE);

      mEmailView.setTitle(getString(R.string.setting_email));
    } else {
      mCertifyView.setTitle(WatchaApp.getUser().getEmail());
      mCertifyView.setDescription(
          getString((WatchaApp.getUser().isVerified()) ? R.string.certified : R.string.certify));

      if (!WatchaApp.getUser().isVerified()) {
        mCertifyView.setOnClickListener(this);
      } else {
        mCertifyView.setOnClickListener(null);
      }

      mEmailView.setTitle(getString(R.string.change_email));
    }

    mPasswordView.setTitle(getString(
        (WatchaApp.getUser().isInitialized()) ? R.string.change_pwd : R.string.set_password));
  }

  @Override protected DataProvider<BaseResponse<User>> getDataProvider(Map<String, String> params) {
    mDataProvider =
        new DataProvider<BaseResponse<User>>(getActivity(), getQueryType(), createReferrerBuilder())
            .withDialogMessage(getString(R.string.changing))
            .responseTo(createSettingResponseListener(getActivity()));
    mDataProvider.withParams(params);
    return mDataProvider;
  }

  @Override protected Map<String, String> getRequestDataParams() {

    Map<String, String> params = new HashMap<>();
    params.put("id", WatchaApp.getUser().getCode());

    return params;
  }

  private static final int PROFILE_IMAGE_MAX_PX_SIZE = 200;

  @Override public void onImageChosenCallback(int fileType, Uri bitmapUri,
      ImageChooser.ImageUriParser parser) {
    Bitmap bm = parser.getBitmapFromUri(getActivity(), bitmapUri);

    switch (fileType) {
      case QueryFile.PROFILE_PHOTO:
        if (bm != null && bm.getWidth() > PROFILE_IMAGE_MAX_PX_SIZE
            || bm != null && bm.getHeight() > PROFILE_IMAGE_MAX_PX_SIZE) {
          bm = BitmapUtil.scaleDown(bm, PROFILE_IMAGE_MAX_PX_SIZE);
        }
        break;

      case QueryFile.COVER_PHOTO:
        break;
    }

    File f = parser.getFileFromBitmap("photo_" + System.currentTimeMillis(), bm);

    switch (fileType) {
      case QueryFile.PROFILE_PHOTO:
        SettingHelper.uploadPhoto(getActivity(), new QueryFile(QueryFile.PROFILE_PHOTO, f),
            createSettingResponseListener(getActivity()));
        break;

      case QueryFile.COVER_PHOTO:
        SettingHelper.uploadCover(getActivity(), new QueryFile(QueryFile.COVER_PHOTO, f),
            createSettingResponseListener(getActivity()));
        break;
    }
  }
}
