package com.frograms.watcha.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.Card;
import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.model.CardTask;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.Comment;
import com.frograms.watcha.model.items.LoadMore;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.menus.MenuItems;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.model.views.ViewCard;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.itemViews.ItemLoadMoreView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.viewHolders.ViewHolderHelper;
import com.frograms.watcha.viewHolders.ViewHolderMeta;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;

public class ReplyFragment extends ListFragment<ListData> {

  @Bind(R.id.write_reply) EditText mWriteReplyView;
  @Bind(R.id.send_reply) FontIconTextView mSendReplyView;

  private ReplyType mReplyType;
  private String mCode;

  private ApiResponseListener<BaseResponse<ListData>> mReplyListener;

  private boolean mIsOpenKeyboard;

  public enum ReplyType {
    COMMENT(0, "comments"),
    DECK(1, "decks");

    int mTypeValue;
    String mStr;

    ReplyType(int typeValue, String str) {
      mTypeValue = typeValue;
      mStr = str;
    }

    public String getString() {
      return mStr;
    }

    public int getTypeValue() {
      return mTypeValue;
    }

    public static ReplyType getType(int intValue) {
      for (ReplyType t : values()) {
        if (t.getTypeValue() == intValue) {
          return t;
        }
      }
      return null;
    }
  }

  private QueryType mQueryType = QueryType.GENERAL_CARDS;

  @Override protected QueryType getQueryType() {
    return mQueryType.setApi(mReplyType.getString() + "/" + mCode + "/replies");
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_reply;
  }

  @Override public MenuItems getToolbarMenuItems() {
    switch(mReplyType) {
      case COMMENT:
        return MenuItems.SEE_COMMENT;

      case DECK:
        return MenuItems.SEE_DECK;
    }

    return null;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_comment:
        ActivityStarter.with(getActivity(), FragmentTask.DETAIL_COMMENT)
            .addBundle(new BundleSet.Builder().putCommentCode(mCode).build().getBundle())
            .start();

        return true;

      case R.id.menu_item_deck:
        ActivityStarter.with(getActivity(), FragmentTask.DETAIL_DECK)
            .addBundle(new BundleSet.Builder().putDeckCode(mCode).build().getBundle())
            .start();

        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void setBundle(Bundle bundle) {
    super.setBundle(bundle);

    if (bundle.containsKey(BundleSet.REPLY_TYPE)) {
      mReplyType = ReplyType.getType(bundle.getInt(BundleSet.REPLY_TYPE));

      switch(mReplyType) {
        case COMMENT:
          mCode = bundle.getString(BundleSet.COMMENT_CODE);
          break;

        case DECK:
          mCode = bundle.getString(BundleSet.DECK_CODE);
          break;
      }
    }

    mIsOpenKeyboard = bundle.getBoolean(BundleSet.OPEN_KEYBOARD);
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.REPLY;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override
  protected RecyclerView.LayoutManager getLayoutManager() {
    if (mLayoutManager == null) {
      mLayoutManager = new LinearLayoutManager(getActivity());
      //((LinearLayoutManager)mLayoutManager).setReverseLayout(true);
      //((LinearLayoutManager)mLayoutManager).setStackFromEnd(true);
    }

    return mLayoutManager;
  }

  public Observable<List<ViewCard>> parseCards(@NonNull final ListData data) {
    return Observable.create(new Observable.OnSubscribe<List<ViewCard>>() {
      @Override public void call(Subscriber<? super List<ViewCard>> subscriber) {
        ArrayList<Card> cards = data.getCards();
        CardTask cardTask = new CardTask(cards);
        subscriber.onNext(cardTask.getViewCards());
        subscriber.onCompleted();
      }
    });
  }

  public void syncReplyCount(boolean b){
    switch(mReplyType) {
      case COMMENT:
        Comment parentComment = CacheManager.getComment(mCode);
        if (parentComment != null) {
          parentComment.addReply(b);
          getToolbarHelper().setToolbarTitle(
              getString(R.string.reply) + " " + parentComment.getRepliesCount());
        }
        break;

      case DECK:
        DeckBase deck = CacheManager.getDeck(mCode);
        if (deck != null) {
          deck.addReply(b);
          getToolbarHelper().setToolbarTitle(
              getString(R.string.reply) + " " + deck.getRepliesCount());
        }
        break;
    }
  }

  protected ApiResponseListener<BaseResponse<ListData>> getReplyListener() {
    if (mReplyListener == null) {
      mReplyListener = new BaseApiResponseListener<BaseResponse<ListData>>(getActivity()) {
        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ListData> result) {
          super.onSuccess(queryType, result);
          parseCards(result.getData()).subscribe(new Action1<List<ViewCard>>() {
            @Override public void call(List<ViewCard> viewCards) {
              Util.closeKeyboard(mWriteReplyView);
              syncReplyCount(true);
              getAdapter().addItems(viewCards);
              getAdapter().notifyDataSetChanged();
              ((LinearLayoutManager)getLayoutManager()).scrollToPositionWithOffset(getAdapter().getItemCount() - 1, 0);
            }
          });
        }
      };
    }

    return mReplyListener;
  }

  protected void requestWriteReply() {
    DataProvider<BaseResponse<ListData>>
        provider = new DataProvider<>(getBaseActivity(), QueryType.REPLY_WRITE.setApi(mReplyType.getString(), mCode), null);
    provider.withParams(Collections.singletonMap("message", mWriteReplyView.getText().toString()));
    provider.responseTo(getReplyListener());
    provider.withDialogMessage("댓글 저장 중..");

    provider.request();
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
        AnalyticsActionType.SEND_REPLY).build());
  }

  @Override public void onViewCreated(View view, final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    getToolbarHelper().setToolbarTitle(getString(R.string.reply));
    mAdapter.setIsUseGrid((getLayoutManager() instanceof LinearLayoutManager) ? false : true);

    mWriteReplyView.addTextChangedListener(new TextWatcher() {

      @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence != null && !charSequence.toString().isEmpty()) {
          mSendReplyView.setTextColor(getResources().getColor(R.color.purple));
        } else {
          mSendReplyView.setTextColor(getResources().getColor(R.color.light_gray));
        }
      }

      @Override public void afterTextChanged(Editable editable) {

      }
    });
    mWriteReplyView.setOnEditorActionListener((textView, actionId, keyEvent) -> {
      if (actionId == EditorInfo.IME_ACTION_DONE && !textView.getText().toString().isEmpty()) {
        requestWriteReply();
        mWriteReplyView.setText("");
        return true;
      }
      return false;
    });

    mSendReplyView.setOnClickListener(view1 -> {
      if (mWriteReplyView.getText() != null && !mWriteReplyView.getText().toString().isEmpty())
        requestWriteReply();
      mWriteReplyView.setText("");
    });

    mSendReplyView.setOnClickListener(view1 -> {
      if (mWriteReplyView.getText() != null && !mWriteReplyView.getText().toString().isEmpty())
        requestWriteReply();
      mWriteReplyView.setText("");
    });

    RecyclerAdapter.OnClickCardItemListener onClickCardItemListener = (v, item) -> {
      if (v instanceof ItemLoadMoreView) {
        getAdapter().removeItem((Integer)v.getTag(R.id.TAG_POSITION));
        //LoadMore loadMore = (LoadMore)item;
        //loadMore.setIsVisible(false);
        mPage++;
        requestData(getDataProvider());
        //setData();
      }
    };

    mAdapter.setOnClickCardItemListener(onClickCardItemListener);

    if (mIsOpenKeyboard) {
      Util.openKeyboard(mWriteReplyView);
    }
  }

  @Override
  public void onSuccess(@NonNull QueryType queryType, @NonNull final BaseResponse<ListData> result) {
    WatchaApp.getSessionManager().storeWatchaSessionInLocalStorage(getActivity());
    if (mProgressView != null) {
      mProgressView.setVisibility(View.GONE);
    }
    if (mAdapter != null && mAdapter.getFooterView() != null) {
      mAdapter.getFooterView().setVisibility(View.GONE);
    }
    if (mAdapter != null && mAdapter.getLoadingViewHolder() != null) {
      mAdapter.getLoadingViewHolder().onEnd();
    }
    mSwipeLayout.setRefreshing(false);
    ListData data = result.getData();

    if (data != null) {

      if (data.getTotal() != 0)
        getToolbarHelper().setToolbarTitle(getString(R.string.reply) + " " + data.getTotal());

      hasNext = data.hasNext();
      mAdapter.setHasNext(false);

      ArrayList<Card> cards = data.getCards();

      CardTask cardTask = new CardTask(cards);

      List<ViewCard> viewCards = cardTask.getViewCards();
      if (hasNext) {
        ViewCard loadmore = new ViewCard(ViewHolderHelper.CARD_TYPE.LIST,
            new CardItem(ViewHolderMeta.LOAD_MORE, new LoadMore()));

        loadmore.setLocation(ViewCard.LOCATION.TOP);
        if (viewCards.size() > 0) {
          viewCards.get(0).setLocation(ViewCard.LOCATION.NONE);
          loadmore.setIsMerge(viewCards.get(0).isMerge());
          viewCards.get(0).setIsMerge(false);
        }

        viewCards.add(0, loadmore);
      }

      mAdapter.addItemsToTop(viewCards);

      if (viewCards != null && viewCards.size() > 0) {
        ((LinearLayoutManager)getLayoutManager()).scrollToPositionWithOffset(viewCards.size(), 0);
      }

      if (mPage == 1 && cardTask.getViewCards() != null && cardTask.getViewCards().size() == 0
          || mPage == 1 && cardTask.getViewCards() == null) {
        setEmptyViewVisibility(View.VISIBLE);
      }
    }
    notifyList();
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    return new OnBackKeyListener() {
      @Override public boolean onBackKeyDown() {
        if (mWriteReplyView != null)
          Util.closeKeyboard(mWriteReplyView);
        return true;
      }
    };
  }
}
