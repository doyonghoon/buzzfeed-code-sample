package com.frograms.watcha.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.OnClick;
import carbon.animation.AnimUtils;
import com.facebook.AccessToken;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.listeners.LightAnimationListener;
import com.frograms.watcha.listeners.OnBackKeyListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.FormValidator;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.RobotoBoldView;
import com.frograms.watcha.view.textviews.RobotoRegularView;
import com.nineoldandroids.animation.Animator;
import java.util.HashMap;
import java.util.Map;

/**
 * 로그인 화면.
 */
public class LoginFragment extends BaseFragment implements FormValidator.FormValidationListener {

  @Bind(R.id.email) EditText mEmailTextView;
  @Bind(R.id.login_root) View mLoginRootView;
  @Bind(R.id.password) EditText mPasswordTextView;
  @Bind(R.id.btn_login) RobotoBoldView mLoginButton;
  @Bind(R.id.btn_fb_login) RobotoRegularView mLoginFbButton;
  @Bind(R.id.btn_fb_login_icon) FontIconTextView mLoginFbButtonIcon;
  @Bind(R.id.btn_fb_login_layout) LinearLayout mLoginFbButtonLayout;

  private final FormValidator mEmailValidator = new FormValidator(FormValidator.FormStyle.EMAIL);
  private final FormValidator mPasswordValidator =
      new FormValidator(FormValidator.FormStyle.PASSWORD);
  private boolean isValidPassword = false;
  private boolean isValidEmail = false;

  private ApiResponseListener mLoginResponseListener =
      new ApiResponseListener<BaseResponse<User>>() {
        @Override
        public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
          WLog.i("result: " + result.getData().toString());
          WatchaApp.getSessionManager().storeWatchaSessionInLocalStorage(getActivity());
          WatchaApp.getInstance().setUser(result.getData());
          CacheManager.putMainUser(result.getData());

          if (WatchaApp.getUser().isFbConnected()) {
            FacebookBroker.getAccessTokenWithGrantedPermissions(AccessToken.getCurrentAccessToken(), getActivity())
                .subscribe();
          }

          ActivityStarter.with(getBaseActivity(), FragmentTask.HOME).start();
        }
      };

  private boolean mRevealed = false;

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.CIRCLE_REVEAL_ANIMATION)) {
      mRevealed = bundle.getBoolean(BundleSet.CIRCLE_REVEAL_ANIMATION, false);
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.LOGIN;
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putBoolean(BundleSet.CIRCLE_REVEAL_ANIMATION, mRevealed);
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.SCROLL;
  }

  @Override public void setDefaultToolbar() {
    super.setDefaultToolbar();
    getToolbarHelper().getActionbar().setTitle("로그인");
    getToolbarHelper().getToolbar().setBackgroundColor(Color.WHITE);
    FontIconDrawable back = FontIconDrawable.inflate(getActivity(), R.xml.icon_navi_back);
    back.setTextColor(getResources().getColor(R.color.black));
    getToolbarHelper().getToolbar().setNavigationIcon(back);
    getToolbarHelper().getToolbar().setTitleTextColor(getResources().getColor(R.color.black));
  }

  @Override protected int getToolbarColor() {
    return R.color.white;
  }

  @Override protected QueryType getQueryType() {
    return null;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_login;
  }

  @Override public void onViewCreated(final View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    //if (mRevealed) {
    //  getToolbarHelper().getToolbar().setAlpha(0.0f);
    //  view.setAlpha(0.0f);
    //  Handler handler = new Handler();
    //  handler.postDelayed(new Runnable() {
    //    @Override public void run() {
    //      AnimUtils.fadeIn(view, null);
    //      AnimUtils.fadeIn(getToolbarHelper().getToolbar(), null);
    //    }
    //  }, 250);
    //}

    // 회원가입 버튼의 화살표 아이콘 달아줌.
    FontIconDrawable arrow = FontIconDrawable.inflate(getActivity(), R.xml.icon_arrow_right);
    arrow.setTextColor(getResources().getColor(R.color.pink));
    arrow.setTextSize(getResources().getDimension(R.dimen.popup_menu_font));

    // 이메일, 비번 텍스트 입력 콜백 리스너 달아줌.
    mEmailValidator.registerValidator(mEmailTextView, this);
    mPasswordValidator.registerValidator(mPasswordTextView, this);
    FontHelper.RobotoRegular(mLoginButton);
    FontHelper.RobotoRegular(mLoginFbButton);

    mPasswordTextView.setOnEditorActionListener((v, actionId, event) -> {
      if (actionId == EditorInfo.IME_ACTION_DONE) {
        onClickLogin();
        return true;
      }
      return false;
    });
  }

  @OnClick(R.id.login_find_password) void onClickFindPassword() {
    ActivityStarter.with(getActivity(), FragmentTask.FIND_PASSWORD).start();
  }

  /**
   * 가져온 세션 값은 새로 갱신된 값이라서 로그인 후에 서버에 보내줘야 함.
   */
  @OnClick(R.id.btn_fb_login_layout) void onClickFacebookLogin() {
    FacebookBroker.accessTokenObservable(getActivity()).subscribe(accessToken -> {
      Map<String, String> params = new HashMap<>();
      params.put("token", accessToken.getToken());
      WLog.i("params: " + params);
      DataProvider provider = new DataProvider(getActivity(), QueryType.LOGIN_FACEBOOK_TOKEN, createReferrerBuilder());
      provider.withParams(params);
      provider.withDialogMessage("로그인 중..");
      provider.responseTo(mLoginResponseListener);
      provider.request();
    }, throwable -> throwable.printStackTrace());
  }

  /**
   * 로그인 버튼을 누르면, 이메일과 비밀번호가 적합한 값인지 확인하고 왓챠서버로 로그인 요청을 보냄.
   * 로그인 과정이 성공적이면 유저 모델을 json으로 보내줌.
   * 모델 정의 후에 {@link HomeFragment} 로 이동함.
   */
  @OnClick(R.id.btn_login) void onClickLogin() {
    String email = mEmailTextView.getText().toString();
    String password = mPasswordTextView.getText().toString();

    if (!mEmailValidator.isValid(email) || !mPasswordValidator.isValid(password)) {
      Toast.makeText(getActivity(), "이메일이나 비밀번호 형식이 맞지 않아서 로그인을 진행할 수가 없어요", Toast.LENGTH_SHORT)
          .show();
      return;
    }

    Map<String, String> params = new HashMap<>();
    params.put("email", email);
    params.put("password", password);
    DataProvider provider = new DataProvider(getActivity(), QueryType.LOGIN_PASSWORD, createReferrerBuilder());
    provider.withParams(params);
    provider.withDialogMessage("로그인 중..");
    provider.responseTo(mLoginResponseListener);
    provider.request();
  }

  private FontIconDrawable getCheckIconDrawable(boolean isValid) {
    FontIconDrawable checkIcon = null;
    if (isValid) {
      checkIcon = FontIconDrawable.inflate(getActivity(), R.xml.icon_arrow_check);
      checkIcon.setTextColor(getResources().getColor(R.color.green));
      checkIcon.setTextSize(getResources().getDimension(R.dimen.more_font));
    }
    return checkIcon;
  }

  @Override public OnBackKeyListener getOnBackKeyListener() {
    if (mRevealed) {
      return () -> {
        AnimUtils.fadeOut(getToolbarHelper().getToolbar(), null);
        AnimUtils.fadeOut(mLoginRootView, new LightAnimationListener() {
          @Override public void onAnimationEnd(Animator animation) {
            Util.closeKeyboard(mEmailTextView);
            Util.closeKeyboard(mPasswordTextView);
            Intent i = new Intent();
            i.putExtra("reveal", true);
            getActivity().setResult(Activity.RESULT_OK, i);
            getActivity().finish();
          }
        });
        return false;
      };
    } else {
      return () -> {
        Util.closeKeyboard(mEmailTextView);
        Util.closeKeyboard(mPasswordTextView);
        getActivity().finish();
        return false;
      };
    }
  }

  @Override public void onValidation(FormValidator.FormStyle style, EditText v, String text,
      boolean isValid) {
    switch (style) {
      case EMAIL:
        isValidEmail = isValid;
        break;
      case PASSWORD:
        isValidPassword = isValid;
        break;
    }
    boolean enabled = (isValidEmail && isValidPassword);
    mLoginButton.setEnabled(enabled);
    mLoginButton.setBackgroundResource(
        enabled ? R.drawable.bg_welcome_ok : R.drawable.bg_button_corner_gray_nor);
    v.setCompoundDrawablesWithIntrinsicBounds(null, null, getCheckIconDrawable(isValid), null);
  }

  @Override protected boolean hasDefaultDropshadow() {
    return false;
  }
}