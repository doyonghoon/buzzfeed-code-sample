package com.frograms.watcha.observablescrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.model.callbacks.BaseScrollCallbacks;
import com.frograms.watcha.utils.WLog;

public class ObservableRecyclerView extends RecyclerView {
  private static final String TAG = ObservableRecyclerView.class.getSimpleName();

  private ObservableScrollViewCallbacks mCallbacks;
  private int mPrevFirstVisiblePosition;
  private int mPrevFirstVisibleChildHeight = -1;
  private int mPrevScrolledChildrenHeight;
  private SparseIntArray mChildrenHeights;
  private int mPrevScrollY;
  private int mScrollY;
  private ScrollState mScrollState;
  private boolean mFirstScroll;
  private boolean mDragging;

  private int mSpans;
  private int mOrientation;

  public ObservableRecyclerView(Context context) {
    this(context, null);
  }

  public ObservableRecyclerView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ObservableRecyclerView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();

    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ObservableRecyclerView);

    mSpans = Math.max(2, a.getInt(R.styleable.ObservableRecyclerView_numSpans, -1));
    mOrientation = a.getInt(R.styleable.ObservableRecyclerView_flow, 0);
    a.recycle();
  }

  @Override protected void onScrollChanged(int l, int t, int oldl, int oldt) {
    super.onScrollChanged(l, t, oldl, oldt);
    if (mCallbacks != null) {
      if (getChildCount() > 0) {
        int firstVisiblePosition = getChildPosition(getChildAt(0));
        int lastVisiblePosition = getChildPosition(getChildAt(getChildCount() - 1));
        for (int i = firstVisiblePosition, j = 0; i <= lastVisiblePosition; i++, j++) {
          if (getChildAt(j) != null
              && mChildrenHeights != null
              && mChildrenHeights.indexOfKey(i) < 0
              || getChildAt(j) != null && getChildAt(j).getHeight() != mChildrenHeights.get(i)) {
            mChildrenHeights.put(i, getChildAt(j).getHeight());
          }
        }

        View firstVisibleChild = getChildAt(0);
        if (firstVisibleChild != null) {
          if (mPrevFirstVisiblePosition < firstVisiblePosition) {
            // scroll down
            int skippedChildrenHeight = 0;
            if (firstVisiblePosition - mPrevFirstVisiblePosition != 1) {
              //LogUtils.v(TAG, "Skipped some children while scrolling down: " + (firstVisiblePosition - mPrevFirstVisiblePosition));
              for (int i = firstVisiblePosition - 1; i > mPrevFirstVisiblePosition; i--) {
                if (0 < mChildrenHeights.indexOfKey(i)) {
                  skippedChildrenHeight += mChildrenHeights.get(i);
                  //LogUtils.v(TAG, "Calculate skipped child height at " + i + ": " + mChildrenHeights.get(i));
                } else {
                  //LogUtils.v(TAG, "Could not calculate skipped child height at " + i);
                }
              }
            }
            mPrevScrolledChildrenHeight += mPrevFirstVisibleChildHeight + skippedChildrenHeight;
            mPrevFirstVisibleChildHeight = firstVisibleChild.getHeight();
          } else if (firstVisiblePosition < mPrevFirstVisiblePosition) {
            // scroll up
            int skippedChildrenHeight = 0;
            if (mPrevFirstVisiblePosition - firstVisiblePosition != 1) {
              //LogUtils.v(TAG, "Skipped some children while scrolling up: " + (mPrevFirstVisiblePosition - firstVisiblePosition));
              for (int i = mPrevFirstVisiblePosition - 1; i > firstVisiblePosition; i--) {
                if (0 < mChildrenHeights.indexOfKey(i)) {
                  skippedChildrenHeight += mChildrenHeights.get(i);
                  //LogUtils.v(TAG, "Calculate skipped child height at " + i + ": " + mChildrenHeights.get(i));
                } else {
                  //LogUtils.v(TAG, "Could not calculate skipped child height at " + i);
                }
              }
            }
            mPrevScrolledChildrenHeight -= firstVisibleChild.getHeight() + skippedChildrenHeight;
            mPrevFirstVisibleChildHeight = firstVisibleChild.getHeight();
          } else if (firstVisiblePosition == 0) {
            mPrevFirstVisibleChildHeight = firstVisibleChild.getHeight();
          }
          if (mPrevFirstVisibleChildHeight < 0) {
            mPrevFirstVisibleChildHeight = 0;
          }
          mScrollY = mPrevScrolledChildrenHeight - firstVisibleChild.getTop();
          mPrevFirstVisiblePosition = firstVisiblePosition;

          //LogUtils.v(TAG, "first: " + firstVisiblePosition + " scrollY: " + mScrollY + " first height: " + firstVisibleChild.getHeight() + " first top: " + firstVisibleChild.getTop());
          mCallbacks.onScrollChanged(mScrollY, mFirstScroll, mDragging);
          if (mFirstScroll) {
            mFirstScroll = false;
          }

          if (mPrevScrollY < mScrollY) {
            //down
            mScrollState = ScrollState.UP;
          } else if (mScrollY < mPrevScrollY) {
            //up
            mScrollState = ScrollState.DOWN;
          } else {
            mScrollState = ScrollState.STOP;
          }
          mPrevScrollY = mScrollY;
        } else {
          //LogUtils.v(TAG, "first: " + firstVisiblePosition);
        }
      }
    }
  }

  @Override public boolean onTouchEvent(MotionEvent ev) {
    if (mCallbacks != null) {
      switch (ev.getActionMasked()) {
        case MotionEvent.ACTION_DOWN:
        case MotionEvent.ACTION_MOVE:
          mFirstScroll = mDragging = true;
          mCallbacks.onDownMotionEvent();
          break;
        case MotionEvent.ACTION_UP:
        case MotionEvent.ACTION_CANCEL:
          mDragging = false;
          mCallbacks.onUpOrCancelMotionEvent(mScrollState);
          break;
      }
    }
    return super.onTouchEvent(ev);
  }

  public void setScrollViewCallbacks(ObservableScrollViewCallbacks listener) {
    mCallbacks = listener;
  }

  public BaseScrollCallbacks getCallbacks() {
    if (mCallbacks instanceof BaseScrollCallbacks) {
      return (BaseScrollCallbacks) mCallbacks;
    }
    return null;
  }

  public int getCurrentScrollY() {
    return mScrollY;
  }

  private void init() {
    mChildrenHeights = new SparseIntArray();
  }

  public int getSpans() {
    return mSpans;
  }

  public int getOrientation() {
    switch (mOrientation) {
      case 0:
        return StaggeredGridLayoutManager.HORIZONTAL;

      case 1:
      default:
        return StaggeredGridLayoutManager.VERTICAL;
    }
  }
}
