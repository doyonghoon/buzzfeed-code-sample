package com.frograms.watcha.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.frograms.watcha.model.items.UserBase;
import com.google.gson.annotations.SerializedName;
import java.util.Date;

/**
 * 나일 수도 있고, 다른 유저일 수도 있음.
 */
public class User extends UserBase {

  @SerializedName("created_at") Date createdAt;
  @SerializedName("email") String email;
  @SerializedName("followers_count") int followersCount;
  @SerializedName("friends_count") int friendsCount;
  @SerializedName("taste_match") float tasteMatch;
  @SerializedName("partner") UserBase partner;

  @SerializedName("is_facebook_connected") boolean isFbConnected;
  @SerializedName("is_facebook_timeline_allowed") boolean isTimelineAllowed;
  @SerializedName("is_twitter_connected") boolean isTwtConnected;

  @SerializedName("is_password_initialized") boolean isInitialized;
  @SerializedName("is_email_confirmed") boolean isVerified;

  @SerializedName("ratings") User.Ratings ratings;

  public User(Parcel in) {
    super(in);
  }

  public Ratings getRatings() {
    return ratings;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getFollowersCount() {
    return followersCount;
  }

  public void changeFriendsCount(boolean isUp) {
    if (isUp) {
      friendsCount++;
    } else {
      friendsCount--;
    }
  }

  public double getTasteMatch() {
    return tasteMatch;
  }

  public int getFriendsCount() {
    return friendsCount;
  }

  public void setFriendsCount(int count) {
    friendsCount = count;
  }

  public void setFollowersCount(int count) {
    followersCount = count;
  }

  public UserBase getPartner() {
    return partner;
  }

  public void setPartner(UserBase user) {
    partner = user;
  }

  public void deleteMoviePartner() {
    partner = null;
  }

  public boolean isFbConnected() {
    return isFbConnected;
  }

  public boolean isTimelineAllowed() {
    return isTimelineAllowed;
  }

  public boolean isTwtConnected() {
    return isTwtConnected;
  }

  public boolean isInitialized() {
    return isInitialized;
  }

  public boolean isVerified() {
    return isVerified;
  }

  public static class Ratings {

    @SerializedName("movies") Rating moviesRating;
    @SerializedName("tv_seasons") Rating tvSeasonsRating;

    public Rating getTvSeasonsRating() {
      return tvSeasonsRating;
    }

    public Rating getMoviesRating() {
      return moviesRating;
    }

    public static class Rating {
      @SerializedName("count") int count;
      @SerializedName("image") Poster photo;

      public Poster getPhoto() {
        return photo;
      }

      public int getCount() {
        return count;
      }
    }
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    super.writeToParcel(dest, flags);

    dest.writeLong(createdAt.getTime());
    dest.writeString(email);
    dest.writeInt(followersCount);
    dest.writeInt(friendsCount);
    dest.writeFloat(tasteMatch);
    dest.writeParcelable(partner, flags);

    dest.writeString(isFbConnected + "");
    dest.writeString(isTimelineAllowed + "");
    dest.writeString(isTwtConnected + "");
    dest.writeString(isInitialized + "");
    dest.writeString(isVerified + "");
  }

  @Override protected void readFromParcel(Parcel in) {
    super.readFromParcel(in);

    createdAt = new Date(in.readLong());
    email = in.readString();
    followersCount = in.readInt();
    friendsCount = in.readInt();
    tasteMatch = in.readFloat();
    partner = in.readParcelable(UserBase.class.getClassLoader());

    isFbConnected = Boolean.valueOf(in.readString());
    isTimelineAllowed = Boolean.valueOf(in.readString());
    isTwtConnected = Boolean.valueOf(in.readString());
    isInitialized = Boolean.valueOf(in.readString());
    isVerified = Boolean.valueOf(in.readString());
  }

  public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
    public User createFromParcel(Parcel in) {
      return new User(in);
    }

    public User[] newArray(int size) {
      return new User[size];
    }
  };
}
