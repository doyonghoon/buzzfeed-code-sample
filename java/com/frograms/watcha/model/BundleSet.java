package com.frograms.watcha.model;

import android.os.Bundle;
import com.frograms.watcha.fragment.ChangeInfoFragment;
import com.frograms.watcha.fragment.GalleryFragment;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.fragment.WebviewFragment;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.PrivacyLevel;
import com.frograms.watcha.model.enums.UserActionType;
import java.util.ArrayList;
import java.util.List;

public class BundleSet {

  public static final String PUSH_KEY = "Notification";
  public static final String CATEGORY_TYPE = "Category_Type"; // movie, tvshow
  public static final String CATEGORY_ID = "Category_Id"; // movie, tvshow
  public static final String CONTENT_CODE = "Content_Code";

  public static final String USER_CODE = "User_Code";
  public static final String USER_ACTION_TYPE = "UserAction_Type";
  public static final String USER_NAME = "User_Name";

  public static final String COMMENT_CODE = "Comment_Code";

  public static final String SELECTED_TAB = "Selected_Tab";
  public static final String SEARCH_QUERY = "SearchQuery";
  public static final String WEB_URL = "WebUrl";
  public static final String WEB_TITLE = "WebTitle";
  public static final String TAG_IDS = "TagIds";
  public static final String DECK_CODE = "DeckCode";
  public static final String CONTENT_CODES = "DeckCodes";
  public static final String DECK_TITLE = "DeckTitle";
  public static final String DECK_DESCRIPTION = "DeckDescrption";
  public static final String TRANSITION_ANIMATION_TYPE = "TransitionAnimationType";
  public static final String SIMPLE_FACEBOOK_USER = "SimpleFacebookUser";
  public static final String IMAGE_TYPE = "ImageType";
  public static final String MEDIA_JSON_ARRAY = "MediaArrayJson";
  public final static String IMAGE_URL = "ImageUrl";
  public static final String SETTING_TYPE = "SettingType";
  public static final String CIRCLE_REVEAL_ANIMATION = "CircleRevealAnimation";
  public static final String USER_ACTION_COUNT = "UserActionCount";
  public static final String CONTENT_RATING = "ContentRating";
  public static final String LOAD_ACTION_COUNT = "LoadUserActionCount";
  public static final String SHOW_SORT = "ShowSort";
  public static final String IS_ATTACHED_TO_TAB_PAGER = "IsAttachedToTabPager";

  public static final String RESET_ACTIONS = "ResetActions";

  public static final String REPLY_TYPE = "ReplyType";

  public static final String SCHEME_API = "SchemeApi";
  public static final String SCHEME_TITLE = "SchemeTitle";
  public static final String SCHEME_SUBTITLE = "SchemeSubTitle";
  public static final String JSON = "Json";
  public static final String PRIVACY_LEVEL_INT = "PrivacyLevelInt";
  public static final String FOLLOWERS_COUNT = "FollowersCount";
  public static final String FRIENDS_COUNT = "FriendsCount";
  public static final String MOVIE_COUNT = "MovieCount";
  public static final String DRAMA_COUNT = "DramaCount";
  public static final String GALLERY_TYPE = "GalleryType";
  public static final String PREVIOUS_SCREEN_NAME = "PreviousScreenName";

  public static final String OPEN_KEYBOARD = "OpenKeyboard";

  private Bundle mBundle;
  private Builder mBuilder = null;

  private BundleSet(Builder builder) {
    this.mBuilder = builder;
    mBundle = new Bundle();
    if (builder.mFollowersCount > 0) {
      mBundle.putInt(FOLLOWERS_COUNT, builder.mFollowersCount);
    }
    if (builder.mFriendsCount > 0) {
      mBundle.putInt(FRIENDS_COUNT, builder.mFriendsCount);
    }
    if (builder.mPrivacyLevel > 0) {
      mBundle.putInt(PRIVACY_LEVEL_INT, builder.mPrivacyLevel);
    }
    if (builder.mSimpleFacebookUser != null) {
      mBundle.putParcelable(SIMPLE_FACEBOOK_USER, builder.mSimpleFacebookUser);
    }
    mBundle.putBoolean(IS_ATTACHED_TO_TAB_PAGER, builder.mIsAttachedToTabPager);
    if (builder.mTagIds != null) {
      mBundle.putIntArray(TAG_IDS, builder.mTagIds);
    }
    mBundle.putBoolean(CIRCLE_REVEAL_ANIMATION, builder.mIsRevealAnimation);
    if (builder.mImageTypeString != null) {
      mBundle.putString(IMAGE_TYPE, builder.mImageTypeString);
    }
    if (builder.mMediaList != null) {
      mBundle.putString(MEDIA_JSON_ARRAY, builder.mMediaList.toString());
    }
    if (builder.mImageUrl != null) {
      mBundle.putString(IMAGE_URL, builder.mImageUrl);
    }
    if (builder.mChangeInfoSettingType != null) {
      mBundle.putInt(SETTING_TYPE, builder.mChangeInfoSettingType.getNum());
    }
    if (builder.mWebUrl != null) {
      mBundle.putString(WEB_URL, builder.mWebUrl);
    }
    if (builder.mWebTitle != null) {
      mBundle.putString(WEB_TITLE, builder.mWebTitle);
    }
    if (builder.mCategoryType != null) {
      mBundle.putInt(CATEGORY_TYPE, builder.mCategoryType.getTypeInt());
    }
    mBundle.putString(CATEGORY_ID, builder.mCategoryId);
    mBundle.putBoolean(LOAD_ACTION_COUNT, builder.mShouldLoadActionCount);
    if (builder.mContentRatingValue != null) {
      mBundle.putString(CONTENT_RATING, builder.mContentRatingValue);
    }
    mBundle.putBoolean(SHOW_SORT, builder.mShowSortUnderbarView);
    if (builder.mDeckCode != null) {
      mBundle.putString(DECK_CODE, builder.mDeckCode);
    }
    if (builder.mContentCodes != null) {
      mBundle.putStringArrayList(CONTENT_CODES, builder.mContentCodes);
    }
    if (builder.mDeckTitle != null) {
      mBundle.putString(DECK_TITLE, builder.mDeckTitle);
    }
    if (builder.mDeckDescription != null) {
      mBundle.putString(DECK_DESCRIPTION, builder.mDeckDescription);
    }
    if (builder.mContentCode != null) {
      mBundle.putString(CONTENT_CODE, builder.mContentCode);
    }
    if (builder.mUserCode != null) {
      mBundle.putString(USER_CODE, builder.mUserCode);
    }
    if (builder.mUserName != null) {
      mBundle.putString(USER_NAME, builder.mUserName);
    }
    if (builder.mUserActionType != null) {
      mBundle.putInt(USER_ACTION_TYPE, builder.mUserActionType.getTypeInt());
    }
    if (builder.mCommentCode != null) {
      mBundle.putString(COMMENT_CODE, builder.mCommentCode);
    }
    mBundle.putInt(USER_ACTION_COUNT, builder.mActionCount);
    mBundle.putInt(SELECTED_TAB, builder.mSelectedTabPosition);
    if (builder.mSearchQuery != null) {
      mBundle.putString(SEARCH_QUERY, builder.mSearchQuery);
    }
    mBundle.putBoolean(RESET_ACTIONS, builder.mShouldResetActions);
    if (builder.mSchemeApiPath != null) {
      mBundle.putString(SCHEME_API, builder.mSchemeApiPath);
    }
    if (builder.mSchemeTitle != null) {
      mBundle.putString(SCHEME_TITLE, builder.mSchemeTitle);
    }
    if (builder.mSchemeSubtitle != null) {
      mBundle.putString(SCHEME_SUBTITLE, builder.mSchemeSubtitle);
    }
    if (builder.mShareMessageJsonForFacebook != null) {
      mBundle.putString(JSON, builder.mShareMessageJsonForFacebook);
    }
    mBundle.putInt(MOVIE_COUNT, builder.mMovieCount);
    mBundle.putInt(DRAMA_COUNT, builder.mDramaCount);
    if (builder.mGalleryType != null) {
      mBundle.putInt(GALLERY_TYPE, builder.mGalleryType.getIntValue());
    }
    mBundle.putInt(REPLY_TYPE, builder.mReplyType);
    mBundle.putBoolean(OPEN_KEYBOARD, builder.mIsOpenKeyboard);
    if (builder.mPreviousScreenName != null) {
      mBundle.putString(PREVIOUS_SCREEN_NAME, mBuilder.mPreviousScreenName);
    }
  }

  public Builder getBuilder() {
    return this.mBuilder;
  }

  public Bundle getBundle() {
    return mBundle;
  }

  public static class Builder {

    private int mFollowersCount = -1;
    private int mFriendsCount = -1;
    private int mPrivacyLevel = -1;
    private SimpleFacebookUser mSimpleFacebookUser;
    private boolean mIsAttachedToTabPager;
    private int[] mTagIds;
    private boolean mIsRevealAnimation;
    private String mImageTypeString;
    private List<Media> mMediaList;
    private String mImageUrl;
    private ChangeInfoFragment.SettingType mChangeInfoSettingType;
    private String mWebUrl, mWebTitle;
    private CategoryType mCategoryType;
    private String mCategoryId;
    private boolean mShouldLoadActionCount;
    private String mContentRatingValue;
    private boolean mShowSortUnderbarView = false;
    private String mDeckCode;
    private ArrayList<String> mContentCodes;
    private String mDeckTitle;
    private String mDeckDescription;
    private String mContentCode;
    private String mUserCode;
    private String mUserName;
    private UserActionType mUserActionType;
    private String mCommentCode;
    private int mActionCount;
    private int mSelectedTabPosition;
    private String mSearchQuery;
    private boolean mShouldResetActions;
    private String mSchemeTitle, mSchemeSubtitle, mSchemeApiPath;
    private String mShareMessageJsonForFacebook;
    private int mMovieCount = 0;
    private int mDramaCount = 0;
    private GalleryFragment.GalleryType mGalleryType = null;
    private String mPreviousScreenName = null;

    private int mReplyType;
    private boolean mIsOpenKeyboard;

    public Builder() {
    }

    //public Builder from(BundleSet.Builder builder) {
    //  return this;
    //}

    /**
     * 단순히 Google Analytics 에 ScreenName 을 보내주기 위해서 추가한 목적 뿐임.
     *
     * @see GalleryFragment
     * */
    public Builder putGalleryType(GalleryFragment.GalleryType galleryType) {
      mGalleryType = galleryType;
      return this;
    }

    public Builder putMovieCount(int count) {
      mMovieCount = count;
      return this;
    }

    public Builder putDramaCount(int count) {
      mDramaCount = count;
      return this;
    }

    public Builder putFollowersCount(int count) {
      mFollowersCount = count;
      return this;
    }

    public Builder putFriendsCount(int count) {
      mFriendsCount = count;
      return this;
    }

    public Builder putPrivacyLevel(PrivacyLevel privacyLevel) {
      if (privacyLevel != null) {
        mPrivacyLevel = privacyLevel.getTypeInt();
      }
      return this;
    }

    public Builder putSimpleFacebookUser(SimpleFacebookUser simpleFacebookUser) {
      mSimpleFacebookUser = simpleFacebookUser;
      return this;
    }

    public Builder putAttachedToTabPager(boolean isAttachedToTabPager) {
      mIsAttachedToTabPager = isAttachedToTabPager;

      return this;
    }

    public Builder putTagIds(int[] ids) {
      mTagIds = ids;
      return this;
    }

    public Builder putRevealAnimation(boolean revealed) {
      mIsRevealAnimation = revealed;
      return this;
    }

    public Builder putImageType(String type) {
      mImageTypeString = type;

      return this;
    }

    public Builder putMediaJsonArray(List<Media> arrayJson) {
      mMediaList = arrayJson;
      return this;
    }

    public Builder putImageUrl(String url) {
      mImageUrl = url;
      return this;
    }

    public Builder putSettingType(ChangeInfoFragment.SettingType type) {
      mChangeInfoSettingType = type;
      return this;
    }

    /**
     * {@link WebviewFragment} 부를때 넣는 url과 액션바 타이틀.
     *
     * @param url 웹페이지 주소.
     * @param title 툴바(액션바) 타이틀.
     */
    public Builder putUrl(String url, String title) {
      mWebUrl = url;
      mWebTitle = title;
      return this;
    }

    public Builder putCategoryType(CategoryType type) {
      mCategoryType = type;
      return this;
    }

    public Builder putCategoryId(String categoryId) {
      mCategoryId = categoryId;
      return this;
    }

    public Builder putLoadActionCount(boolean shouldLoad) {
      mShouldLoadActionCount = shouldLoad;
      return this;
    }

    public Builder putContentRating(String rating) {
      mContentRatingValue = rating;

      return this;
    }

    public Builder putShowUnderbarView(boolean value) {
      mShowSortUnderbarView = value;
      return this;
    }

    public Builder putDeckCode(String code) {
      mDeckCode = code;
      return this;
    }

    public Builder putContentCodes(ArrayList<String> codes) {
      mContentCodes = codes;
      return this;
    }

    public Builder putDeckTitle(String title) {
      mDeckTitle = title;
      return this;
    }

    public Builder putDeckDescription(String description) {
      mDeckDescription = description;
      return this;
    }

    public Builder putContentCode(String code) {
      mContentCode = code;
      return this;
    }

    public Builder putUserCode(String code) {
      mUserCode = code;
      return this;
    }

    public Builder putUserName(String name) {
      mUserName = name;
      return this;
    }

    public Builder putUserActionType(UserActionType type) {
      mUserActionType = type;
      return this;
    }

    public Builder putCommentCode(String code) {
      mCommentCode = code;
      return this;
    }

    public Builder putActionCount(int count) {
      mActionCount = count;
      return this;
    }

    public Builder putSelectedTab(int selectedTabPosition) {
      mSelectedTabPosition = selectedTabPosition;
      return this;
    }

    public Builder putSearchQuery(String query) {
      mSearchQuery = query;
      return this;
    }

    public Builder isResetActions(boolean isReset) {
      mShouldResetActions = isReset;
      return this;
    }

    public Builder putSchemeApi(String api) {
      mSchemeApiPath = api;
      return this;
    }

    public Builder putSchemeTitle(String title, String subtitle) {
      mSchemeTitle = title;
      mSchemeSubtitle = subtitle;
      return this;
    }

    public Builder putShareMessageFacebookJson(String json) {
      mShareMessageJsonForFacebook = json;
      return this;
    }

    public Builder putReplyType(ReplyFragment.ReplyType type) {
      mReplyType = type.getTypeValue();
      return this;
    }

    public Builder putIsOpenKeyboard(boolean isOpenKeyboard) {
      mIsOpenKeyboard = isOpenKeyboard;
      return this;
    }

    public Builder putPreviousScreenName(String screenName) {
      mPreviousScreenName = screenName;
      return this;
    }
    public BundleSet build() {
      return new BundleSet(this);
    }
  }
}
