package com.frograms.watcha.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;

/**
 * Fragment와 Fragment의 이름을 그룹으로 묶은 모델.
 *
 * {@link AbsPagerFragment}를 오버라이드하면, {@link #com.frograms.watcha.fragment.AbsPagerFragment#getTabs()}를
 * 반환해야 함.
 */
public class Tab implements Parcelable {

  private int mIcon = 0;
  private int mTitleSizeInDp = 20;
  private String mTitle = null;
  private BaseFragment mFragment;

  public Tab(Parcel in) {
    readFromParcel(in);
  }

  public <T extends BaseFragment> Tab(String title, T fragment) {
    this.mTitle = title;
    this.mFragment = fragment;
  }

  public <T extends BaseFragment> Tab(int icon, T fragment) {
    this.mIcon = icon;
    this.mFragment = fragment;
  }

  public void setTitleSize(int dp) {
    mTitleSizeInDp = dp;
  }

  public int getTitleSizeInDp() {
    return mTitleSizeInDp;
  }

  public int getIcon() {
    return mIcon;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

  public String getTitle() {
    return mTitle;
  }

  public BaseFragment getFragment() {
    return mFragment;
  }

  public void setFragment(BaseFragment fragment) {
    mFragment = fragment;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(mIcon);
    dest.writeInt(mTitleSizeInDp);
    dest.writeString(mTitle);
  }

  private void readFromParcel(Parcel in) {
    mIcon = in.readInt();
    mTitleSizeInDp = in.readInt();
    mTitle = in.readString();
  }

  public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
    public Tab createFromParcel(Parcel in) {
      return new Tab(in);
    }

    public Tab[] newArray(int size) {
      return new Tab[size];
    }
  };
}
