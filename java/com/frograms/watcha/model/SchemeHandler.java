package com.frograms.watcha.model;

import android.content.Context;
import android.net.Uri;
import java.util.Map;

/**
 * 쉽게 쓰는 스키마.
 */
public interface SchemeHandler {
  void onScheme(Context context, Uri uri, Map<String, String> params);
}
