package com.frograms.watcha.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.InitActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.utils.WLog;
import org.json.JSONException;
import org.json.JSONObject;

public class PushNotification {

  public static final String PUSH_ACTION_TYPE = "to";
  private static final String PUSH_TICKER = "ticker";
  public static final String PUSH_TITLE = "title";
  public static final String PUSH_MESSAGE = "message";
  private static final String PUSH_IMAGE_URL = "url";
  private static final String PUSH_BIG_PICTURE_URL = "big_picture_url";
  private static final String PUSH_BIG_TEXT = "big_text";
  private static final String PUSH_EXTRA_BUTTON_TITLE = "title";

  private final Context mContext;
  private final int androidSmallIconDrawableId;

  private String title = null;
  private String message = null;
  private String to = null;
  private String iconUrl = null;

  private String ticker = null;
  private String bigPictureUrl = null;
  private String bigText = null;
  private String actionButtonIcon1 = null;
  private String actionButtonIcon2 = null;
  private String actionButtonTo1 = null;
  private String actionButtonTitle1 = null;
  private String actionButtonTo2 = null;
  private String actionButtonTitle2 = null;

  public PushNotification(Context context, Intent intent) {
    mContext = context;
    androidSmallIconDrawableId =
        (context.getResources().getDisplayMetrics().densityDpi <= 240 ? R.drawable.android_icon_96
            : R.drawable.android_icon_96);

    Bundle extra = intent.getExtras();
    //WLog.e(extra.toString());
    title = intent.hasExtra(PUSH_TITLE) ? extra.getString(PUSH_TITLE) : null;
    message = intent.hasExtra(PUSH_MESSAGE) ? extra.getString(PUSH_MESSAGE) : null;
    to = intent.hasExtra(PUSH_ACTION_TYPE) ? extra.getString(PUSH_ACTION_TYPE) : null;
    iconUrl = intent.hasExtra(PUSH_IMAGE_URL) ? extra.getString(PUSH_IMAGE_URL) : null;

    WLog.i("PushNotification : " + title + " " + message + " " + to + " " + iconUrl);
    if (intent.hasExtra("opt")) {
      try {
        JSONObject optionObject = new JSONObject(intent.getExtras().getString("opt"));
        ticker = optionObject.has(PUSH_TICKER) ? optionObject.getString(PUSH_TICKER) : null;
        bigPictureUrl =
            optionObject.has(PUSH_BIG_PICTURE_URL) ? optionObject.getString(PUSH_BIG_PICTURE_URL)
                : null;
        bigText = optionObject.has(PUSH_BIG_TEXT) ? optionObject.getString(PUSH_BIG_TEXT) : null;

        JSONObject actionButton1 =
            optionObject.has("action_button1") ? optionObject.getJSONObject("action_button1")
                : null;
        JSONObject actionButton2 =
            optionObject.has("action_button2") ? optionObject.getJSONObject("action_button2")
                : null;
        if (actionButton1 != null) {
          actionButtonIcon1 = actionButton1.has("icon") ? actionButton1.getString("icon") : null;
          actionButtonTo1 =
              actionButton1.has(PUSH_ACTION_TYPE) ? actionButton1.getString(PUSH_ACTION_TYPE)
                  : null;
          actionButtonTitle1 = actionButton1.has(PUSH_EXTRA_BUTTON_TITLE) ? actionButton1.getString(
              PUSH_EXTRA_BUTTON_TITLE) : null;
        }
        if (actionButton2 != null) {
          actionButtonIcon2 = actionButton2.has("icon") ? actionButton2.getString("icon") : null;
          actionButtonTo2 =
              actionButton2.has(PUSH_ACTION_TYPE) ? actionButton2.getString(PUSH_ACTION_TYPE)
                  : null;
          actionButtonTitle2 = actionButton2.has(PUSH_EXTRA_BUTTON_TITLE) ? actionButton2.getString(
              PUSH_EXTRA_BUTTON_TITLE) : null;
        }
      } catch (JSONException e) {
        Crashlytics.logException(e);
      }
    }
  }

  public String getTitle() {
    return title;
  }

  public String getMessage() {
    return message;
  }

  public String getScheme() {
    return to;
  }

  private Notification getNotification(Context context) {
    Notification notification = null;
    Bitmap iconBitmap = null, bigPictureBitmap = null;

    // download images
    try {
      int maxSize =
          context.getResources().getDimensionPixelSize(R.dimen.notification_profile_icon_size);
      if (iconUrl != null) {
        iconBitmap = Glide.with(mContext).load(iconUrl).asBitmap().into(maxSize, maxSize).get();
      }

      maxSize =
          context.getResources().getDimensionPixelSize(R.dimen.notification_profile_icon_size);
      if (this.bigPictureUrl != null) {
        bigPictureBitmap =
            Glide.with(mContext).load(bigPictureUrl).asBitmap().into(maxSize, maxSize).get();
      }
    } catch (Exception e) {
      WLog.logException(e);
    }

    // normal
    NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
    builder.setAutoCancel(true);
    builder.setContentTitle((title == null) ? context.getString(R.string.app_name) : title);
    builder.setContentText(message);
    builder.setSmallIcon(androidSmallIconDrawableId);
    builder.setLargeIcon(iconBitmap);
    builder.setTicker(ticker);
    builder.setContentIntent(getToIntent(context, to));
    builder.setWhen(System.currentTimeMillis());

    // action buttons
    if (actionButtonTitle1 != null && actionButtonTo1 != null) {
      builder.addAction(
          (actionButtonIcon1 != null) ? getActionButtonDrawableId(this.actionButtonIcon1)
              : androidSmallIconDrawableId, actionButtonTitle1,
          getToIntent(mContext, actionButtonTo1));
    }

    if (actionButtonTitle2 != null && actionButtonTo2 != null) {
      builder.addAction(
          (actionButtonIcon2 != null) ? getActionButtonDrawableId(this.actionButtonIcon2)
              : androidSmallIconDrawableId, actionButtonTitle2,
          getToIntent(mContext, actionButtonTo2));
    }
    notification = builder.build();

    // bigText?
    if (this.bigText != null) {
      NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle(builder);
      bigTextStyle.bigText(this.bigText);
      bigTextStyle.setBigContentTitle(this.title);
      notification = bigTextStyle.build();
    }

    // bigPicture?
    if (this.bigPictureUrl != null) {
      NotificationCompat.BigPictureStyle bigPictureStyle =
          new NotificationCompat.BigPictureStyle(builder);
      bigPictureStyle.bigPicture(bigPictureBitmap);
      bigPictureStyle.setBigContentTitle(title);
      bigPictureStyle.setSummaryText(message);

      notification = builder.setStyle(bigPictureStyle).build();
    }

    // set notification options
    setNotifyOptions(notification);

    return notification;
  }

  public boolean show(int notificationId) {
    NotificationManager notificationManager =
        (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    Notification notification = getNotification(mContext);
    if (notificationManager != null && notification != null) {
      notificationManager.notify(notificationId, notification);
      return true;
    }

    return false;
  }

  private void setNotifyOptions(Notification notification) {
    notification.flags |= Notification.FLAG_AUTO_CANCEL;
    notification.defaults |= Notification.DEFAULT_SOUND;
    notification.defaults |= Notification.DEFAULT_VIBRATE;
    notification.defaults |= Notification.DEFAULT_LIGHTS;
  }

  private PendingIntent getToIntent(Context context, String to) {
    WLog.i(to + " " + ((WatchaApp.getInstance().getUser() != null)));
    Intent i = new Intent(context, InitActivity.class);
    i.putExtra(BundleSet.PUSH_KEY, "watcha://" + to);

    WLog.e(to);
    if (WatchaApp.getInstance().getUser() != null) {
      i = new Intent(Intent.ACTION_VIEW, Uri.parse("watcha://" + to));
      i.putExtra(BundleSet.PUSH_KEY, to);
      WLog.i("has UserInfo");
    } else {
      WLog.i("has not UserInfo");
    }

    //		if (event != null) i.putExtra("event", event);

    try {
      PendingIntent pi =
          PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
      return pi;
    } catch (NullPointerException e) {
      WLog.logException(e);
    }

    return null;
  }

  private int getActionButtonDrawableId(String iconType) {
    if (iconType.equals("film")) {
      return R.drawable.icon_noti_detail;
    } else if (iconType.equals("star")) {
      return R.drawable.icon_noti_rate;
    } else if (iconType.equals("boxoffice")) {
      return R.drawable.icon_noti_boxoffice;
    } else if (iconType.equals("drama")) return R.drawable.icon_noti_drama;

    return androidSmallIconDrawableId;
  }
}