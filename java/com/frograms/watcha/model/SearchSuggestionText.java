package com.frograms.watcha.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by doyonghoon on 15. 6. 4..
 */
public class SearchSuggestionText {
  @SerializedName("is_title") private boolean mHasTitle;
  @SerializedName("text") private String mText;
  @SerializedName("from_local") private boolean mIsFromLocal;

  public SearchSuggestionText(String t, boolean isFromLocal) {
    mText = t;
    mIsFromLocal = isFromLocal;
  }

  public SearchSuggestionText(String t, boolean isFromLocal, boolean isTitle) {
    mText = t;
    mIsFromLocal = isFromLocal;
    mHasTitle = isTitle;
  }

  public boolean hasTitle() {
    return mHasTitle;
  }

  public String getText() {
    return mText;
  }

  public boolean isFromLocal() {
    return mIsFromLocal;
  }

  @Override public String toString() {
    return getText();
  }
}
