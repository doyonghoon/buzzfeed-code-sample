package com.frograms.watcha.model.menus;

import android.support.annotation.ColorRes;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.annotation.XmlRes;
import com.frograms.watcha.R;

public enum MenuSet {
  ICON_SETTING(Type.ICON, R.xml.icon_setting, R.color.purple, R.id.menu_item_setting,
      R.string.setting),
  ICON_SEARCH(Type.ICON, R.xml.icon_search, R.color.purple, R.id.menu_item_search, R.string.search),
  ICON_LIKE(Type.ICON, R.xml.icon_like, R.color.purple, R.id.menu_item_like, R.string.like),
  TEXT_COMPLETE(Type.TEXT, R.string.completed, R.color.purple, R.id.menu_item_text_complete,
      R.string.completed),
  TEXT_COMPLETE_WHITE(Type.TEXT, R.string.completed, R.color.purple, R.id.menu_item_text_complete,
      R.string.completed),
  TEXT_NEXT(Type.TEXT, R.string.next, R.color.purple, R.id.menu_item_text_next, R.string.next),
  TEXT_MODIFY(Type.TEXT, R.string.modify, R.color.purple, R.id.menu_item_modify, R.string.modify),
  TEXT_CREATE(Type.TEXT, R.string.create, R.color.purple, R.id.menu_item_create, R.string.create),
  ICON_SHARE(Type.ICON, R.xml.icon_share, R.color.purple, R.id.menu_item_share, R.string.share),
  ICON_FIND_FRIEND(Type.ICON, R.xml.icon_follow_off, R.color.purple, R.id.menu_item_find_friend,
      R.string.find_friend),
  ICON_MORE_VERTICAL(Type.ICON, R.xml.icon_more_vertical, R.color.purple,
      R.id.menu_item_more_vertical, R.string.more),
  TEXT_FOLLOW(Type.TEXT, R.string.follow, R.color.purple, R.id.menu_item_follow, R.string.follow),
  TEXT_FOLLOWING(Type.TEXT, R.string.doing_follow, R.color.purple, R.id.menu_item_following,
      R.string.doing_follow),
  TEXT_ACCEPT(Type.TEXT, R.string.accept, R.color.purple, R.id.menu_item_accept, R.string.accept),
  TEXT_COMMENT(Type.TEXT, R.string.see_comment, R.color.purple, R.id.menu_item_comment, R.string.see_comment),
  TEXT_DECK(Type.TEXT, R.string.see_deck, R.color.purple, R.id.menu_item_deck, R.string.see_deck);

  private int mXmlResId, mColorId, mColor, mMenuId;
  private Type mType;
  private int mTitleResId;

  MenuSet(Type type, @XmlRes int xmlResId, @ColorRes int colorId, @IdRes int menuId,
      @StringRes int titleResId) {
    mType = type;
    mXmlResId = xmlResId;
    mColorId = colorId;
    mMenuId = menuId;
    mTitleResId = titleResId;
  }

  public Type getType() {
    return mType;
  }

  public int getTitleResId() {
    return mTitleResId;
  }

  public int getMenuId() {
    return mMenuId;
  }

  public int getXmlResId() {
    return mXmlResId;
  }

  public int getColorId() {
    return mColorId;
  }

  public int getColor() {
    return mColor;
  }

  public void setColor(int color) {
    mColor = color;
  }

  public enum Type {
    TEXT,
    ICON;
  }
}
