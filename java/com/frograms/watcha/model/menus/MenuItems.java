package com.frograms.watcha.model.menus;

public enum MenuItems {
  HOME(MenuSet.ICON_SEARCH, MenuSet.ICON_MORE_VERTICAL),
  FOLLOW(MenuSet.ICON_SEARCH),
  COMPLETE(MenuSet.TEXT_COMPLETE),
  ACCEPT(MenuSet.TEXT_ACCEPT),
  MODIFY_DECK(MenuSet.TEXT_MODIFY),
  CREATE_DECK(MenuSet.TEXT_CREATE),
  DETAIL(MenuSet.ICON_SHARE),
  TUTORIAL_NEXT(MenuSet.TEXT_NEXT),
  TUTORIAL_COMPLETE(MenuSet.TEXT_COMPLETE_WHITE),
  USER_PROFILE_FOLLOW(MenuSet.TEXT_FOLLOW),
  USER_PROFILE_FOLLOWING(MenuSet.TEXT_FOLLOWING),
  PROFILE(MenuSet.ICON_FIND_FRIEND, MenuSet.ICON_SETTING, MenuSet.ICON_MORE_VERTICAL),
  INVITE_FRIEND(MenuSet.ICON_SEARCH),
  SEE_COMMENT(MenuSet.TEXT_COMMENT),
  SEE_DECK(MenuSet.TEXT_DECK),
  DEBUG(MenuSet.TEXT_COMPLETE);


  private MenuSet[] mMenuSets;

  MenuItems(MenuSet... menuSets) {
    mMenuSets = menuSets;
  }

  public MenuSet[] getMenuSets() {
    return mMenuSets;
  }
}
