package com.frograms.watcha.model;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * 평가 구간별 메세지.
 */
public class RatingPhase {

  private String mMessage;
  private int mRatingCount, mMaxProgressValue;

  public RatingPhase(int ratingCount, int maxProgressValue, String message) {
    mRatingCount = ratingCount;
    mMessage = message;
    mMaxProgressValue = maxProgressValue;
  }

  public RatingPhase(Context context, int ratingCount, int maxProgressValue,
      @StringRes int messageResId) {
    this(ratingCount, maxProgressValue, context.getString(messageResId));
  }

  public String getMessage() {
    return mMessage;
  }

  public int getRatingCount() {
    return mRatingCount;
  }

  public int getMaxProgressValue() {
    return mMaxProgressValue;
  }
}
