package com.frograms.watcha.model;

import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.utils.YoutubeThumbnailUtils;
import com.frograms.watcha.view.MediaView;
import com.google.gson.annotations.SerializedName;

public class Media {

  @SerializedName("type") protected MediaType type;
  @SerializedName("photo") protected MediaPhoto photo;
  @SerializedName("url") protected String url;

  public MediaType getType() {
    return type;
  }

  public void setType(MediaType type) {
    this.type = type;
  }

  public MediaPhoto getPhoto() {
    return photo;
  }

  /**
   * 유투브 id 값인데, 아래 링크를 참고하면 thumbnail 이미지를 다양한 크기로 가져올 수 있는지 알 수 있음.
   *
   * 참고 {@link MediaView#setItem(Media)}
   * http://stackoverflow.com/questions/2068344/how-do-i-get-a-youtube-video-thumbnail-from-the-youtube-api
   *
   * @see YoutubeThumbnailUtils#getUrl(String, YoutubeThumbnailUtils.Quality)
   */
  public String getUrl() {
    return url;
  }

  public enum MediaType {
    @SerializedName("photo")PHOTO,
    @SerializedName("youtube")YOUTUBE,
    @SerializedName("cover")COVER,
  }

  @Override public String toString() {
    return WatchaApp.getInstance().getGson().toJson(this);
  }
}
