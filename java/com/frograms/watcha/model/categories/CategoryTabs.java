package com.frograms.watcha.model.categories;

import com.frograms.watcha.model.Tab;
import java.util.List;

/**
 * Pager에 보일 탭들.
 */
public interface CategoryTabs {
  public List<Tab> getCategoryTabs();
}
