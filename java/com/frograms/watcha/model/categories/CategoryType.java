package com.frograms.watcha.model.categories;

import com.frograms.watcha.R;

/**
 * 영화, 드라마, 도서 등 추천&인기 탭이 있는 화면의 카테고리를 정하는 enum 타입.
 */
public enum CategoryType {
  MOVIES(0, R.string.movie, "movies"),
  TV_SEASONS(1, R.string.drama, "tv_seasons"),
  BOOKS(2, R.string.book, "books");

  private int mTypeInt = -1, mStrId;
  private String mApiPath;

  CategoryType(int typeInt, int strId, String apiPath) {
    mTypeInt = typeInt;
    mStrId = strId;
    mApiPath = apiPath;
  }

  public int getTypeInt() {
    return mTypeInt;
  }

  public int getStringId() {
    return mStrId;
  }

  public String getApiPath() {
    return mApiPath;
  }

  public static CategoryType getCategory(int typeInt) {
    CategoryType[] tabs = CategoryType.values();
    for (CategoryType t : tabs) {
      if (t.mTypeInt == typeInt) {
        return t;
      }
    }
    return null;
  }

  public static CategoryType getCategory(String category) {
    return CategoryType.valueOf(category.toUpperCase());
  }
}
