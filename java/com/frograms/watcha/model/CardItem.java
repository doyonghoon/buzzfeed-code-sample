package com.frograms.watcha.model;

import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.viewHolders.ViewHolderMeta;
import com.google.gson.annotations.SerializedName;

public class CardItem {
  @SerializedName("item_type") protected ViewHolderMeta itemType;
  @SerializedName("item") protected Item item;

  public CardItem() {

  }

  public CardItem(ViewHolderMeta itemType, Item item) {
    this.itemType = itemType;
    this.item = item;
  }

  public void setItemType(ViewHolderMeta item_type) {
    itemType = item_type;
  }

  public void setItem(Item item) {
    this.item = item;
  }

  public ViewHolderMeta getItemType() {
    return itemType;
  }

  public Item getItem() {
    return item;
  }

  @Override public String toString() {
    return Item.gson.toJson(this);
  }
}
