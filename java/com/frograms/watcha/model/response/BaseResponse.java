package com.frograms.watcha.model.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Created by juyupsung on 2014. 7. 16..
 */
public class BaseResponse<T> {

  private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

  //@SerializedName("result") protected String result;
  @SerializedName("msg") protected String msg;
  @SerializedName("code") protected String code;
  @SerializedName("data") protected T data;

  //public String getResult() {
  //  return result;
  //}
  //
  //public boolean isSuccessful() {
  //  return !TextUtils.isEmpty(result) && result.equals("success");
  //}

  public String getMsg() {
    return msg;
  }

  public String getCode() {
    return code;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  @Override public String toString() {
    return gson.toJson(this);
  }
}