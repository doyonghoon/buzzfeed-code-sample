package com.frograms.watcha.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * 자동완성 검색어 결과.
 */
public class AutocompleteResponse extends BaseResponse<AutocompleteResponse.AutocompleteTextData> {

  public static class AutocompleteTextData {

    @SerializedName("suggestions") public String[] suggestions;

    public String[] getSuggestions() {
      return suggestions;
    }
  }
}
