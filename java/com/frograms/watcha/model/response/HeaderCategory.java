package com.frograms.watcha.model.response;

import com.frograms.watcha.model.ItemCategory;
import com.frograms.watcha.model.response.data.list.ListData;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class HeaderCategory extends ListData {

  //@SerializedName("title") protected String title;
  @SerializedName("item_categories") List<ItemCategory> itemCategories;

  //public String getTitle() {
  //  return title;
  //}

  public List<ItemCategory> getItemCategories() {
    return itemCategories;
  }
}
