package com.frograms.watcha.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by doyonghoon on 15. 4. 28..
 */
public class Autocomplete {
  @SerializedName("suggestions") public String[] suggestions;

  public String[] getSuggestions() {
    return suggestions;
  }
}
