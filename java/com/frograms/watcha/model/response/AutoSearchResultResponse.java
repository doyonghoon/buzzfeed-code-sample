package com.frograms.watcha.model.response;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by juyupsung on 2014. 9. 4..
 */
public class AutoSearchResultResponse extends BaseResponse {
  @SerializedName("data") protected List<Text> texts;

  public List<String> getData() {
    List<String> result = new ArrayList<String>();
    if (texts != null) {
      for (Text text : texts) {
        result.add(text.getText());
      }
    }
    return result;
  }

  public class Text {
    @SerializedName("text") private String text;

    public String getText() {
      return text;
    }
  }
}