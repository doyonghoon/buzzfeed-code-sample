package com.frograms.watcha.model.response.data.list;

import com.frograms.watcha.model.response.data.ActionCount;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.Map;

public class ActionCountsListData extends FeedListData {
  @SerializedName("action_counts") HashMap<String, ActionCount> actionCounts;

  public Map<String, ActionCount> getActionCounts() {
    return actionCounts;
  }
}
