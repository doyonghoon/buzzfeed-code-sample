package com.frograms.watcha.model.response.data.list;

import com.google.gson.annotations.SerializedName;

public class FollowCountListData extends ListData {
  @SerializedName("followers_count") private int followersCount;
  @SerializedName("friends_count") private int friendsCount;

  public int getFollowersCount() {
    return followersCount;
  }

  public int getFriendsCount() {
    return friendsCount;
  }
}
