package com.frograms.watcha.model.response.data.list;

import com.frograms.watcha.model.User;
import com.google.gson.annotations.SerializedName;

public class UserListData extends FeedListData {
  @SerializedName("user") private User user;

  public User getUser() {
    return user;
  }
}
