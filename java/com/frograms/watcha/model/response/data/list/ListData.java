package com.frograms.watcha.model.response.data.list;

import com.frograms.watcha.model.Card;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * 리스트 데이터.
 */
public class ListData {
  @SerializedName("cards") private ArrayList<Card> cards;
  @SerializedName("has_next") private boolean hasNext;
  @SerializedName("title") private String title;
  @SerializedName("sub_title") private String subtitle;
  @SerializedName("total") private int total;

  public ArrayList<Card> getCards() {
    return cards;
  }

  public boolean hasNext() {
    return hasNext;
  }

  public String getTitle() {
    return title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public int getTotal() {
    return total;
  }
}
