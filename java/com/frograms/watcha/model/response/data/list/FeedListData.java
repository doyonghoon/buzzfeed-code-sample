package com.frograms.watcha.model.response.data.list;

import com.google.gson.annotations.SerializedName;

/**
 * 리스트 데이터.
 */
public class FeedListData extends ListData {
  @SerializedName("last_seen_id") private String lastSeenId;

  public String getLastSeenId() {
    return lastSeenId;
  }
}
