package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.ItemCategory;
import com.frograms.watcha.model.response.data.list.ListData;
import com.google.gson.annotations.SerializedName;

public class RatingListData extends ListData {

  @SerializedName("category") ItemCategory itemCategory;
  @SerializedName("selected_category_id") int selectedCategoryId;

  public int getSelectedCategoryId() {
    return selectedCategoryId;
  }

  public ItemCategory getItemCategory() {
    return itemCategory;
  }
}
