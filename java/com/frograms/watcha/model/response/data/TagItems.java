package com.frograms.watcha.model.response.data;

import android.text.TextUtils;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.items.Item;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * 태그들.
 */
public class TagItems extends Item {

  @SerializedName("has_next") boolean mHasNextPage;
  @SerializedName("total_count") int mTotalCount;
  @SerializedName("tags") List<Tag> mTags;

  public boolean hasNextPage() {
    return mHasNextPage;
  }

  public int getTotalCount() {
    return mTotalCount;
  }

  public List<Tag> getTags() {
    if (mTags != null) {
      return mTags;
    }
    return new ArrayList<>();
  }

  public static class Tag {
    @SerializedName("id") int id;
    @SerializedName("name") String name;

    public int getId() {
      return id;
    }

    public String getName() {
      if (!TextUtils.isEmpty(name) && !name.startsWith("#")) {
        return "#" + name;
      }
      return name;
    }

    @Override public String toString() {
      return WatchaApp.getInstance().getGson().toJson(this);
    }

    @Override public int hashCode() {
      return id;
    }

    @Override public boolean equals(Object o) {
      if (o != null && o instanceof Tag) {
        return ((Tag) o).id == this.id;
      }
      return super.equals(o);
    }
  }
}
