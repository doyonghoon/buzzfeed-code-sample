package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.UserAction;
import com.google.gson.annotations.SerializedName;

public class RatingData {

  @SerializedName("user_action") protected UserAction userActions;

  public UserAction getUserAction() {
    return userActions;
  }

  @Override public String toString() {
    return Item.gson.toJson(this);
  }
}