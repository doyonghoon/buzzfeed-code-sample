package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.response.HeaderCategory;
import com.frograms.watcha.model.response.data.list.ListData;
import com.google.gson.annotations.SerializedName;

public class PopularListData extends ListData {

  @SerializedName("header") protected HeaderCategory headerCategory;

  public HeaderCategory getHeader() {
    return headerCategory;
  }
}
