package com.frograms.watcha.model.response.data;

import com.google.gson.annotations.SerializedName;

public class CommentReportData {
  @SerializedName("is_liked") protected String liked;
  @SerializedName("is_spoiler_cautioned") protected String spoilerCautioned;
  @SerializedName("is_improper_cautioned") protected String improperCautioned;

  public String isLiked() {
    return liked;
  }

  public String isSpoilerCautioned() {
    return spoilerCautioned;
  }

  public String isImproperCautioned() {
    return improperCautioned;
  }
}