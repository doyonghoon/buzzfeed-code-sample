package com.frograms.watcha.model.response.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.frograms.watcha.model.items.Item;
import com.google.gson.annotations.SerializedName;

/**
 * 평가 수
 */
public class ActionCount extends Item implements Parcelable {

  @SerializedName("wishes") public int mWishes;
  @SerializedName("ratings") public int mRatings;
  @SerializedName("comments") public int mComments;
  @SerializedName("mehs") public int mMehs;

  public ActionCount(Parcel in) {
    readFromParcel(in);
  }

  public int getWishes() {
    return mWishes;
  }

  public int getRatings() {
    return mRatings;
  }

  public int getComments() {
    return mComments;
  }

  public int getMehs() {
    return mMehs;
  }

  public void setWishes(int wishes) {
    mWishes = wishes;
  }

  public void setRatings(int ratings) {
    mRatings = ratings;
  }

  public void setComments(int comments) {
    mComments = comments;
  }

  public void setMehs(int mehs) {
    mMehs = mehs;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(mWishes);
    dest.writeInt(mRatings);
    dest.writeInt(mComments);
    dest.writeInt(mMehs);
  }

  private void readFromParcel(Parcel in) {
    mWishes = in.readInt();
    mRatings = in.readInt();
    mComments = in.readInt();
    mMehs = in.readInt();
  }

  public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
    public ActionCount createFromParcel(Parcel in) {
      return new ActionCount(in);
    }

    public ActionCount[] newArray(int size) {
      return new ActionCount[size];
    }
  };
}
