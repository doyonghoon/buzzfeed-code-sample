package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.response.data.list.ListData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by doyonghoon on 15. 2. 12..
 */
public class SearchResultData extends ListData {

  @SerializedName("total_count") int mTotalCount;

  public int getTotalCount() {
    return mTotalCount;
  }
}
