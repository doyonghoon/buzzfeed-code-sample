package com.frograms.watcha.model.response.data;

import com.google.gson.annotations.SerializedName;

/**
 * 리스트 데이터.
 */
public class ShareMessageData {
  @SerializedName("url") private String url;

  public String getUrl() {
    return url;
  }
}
