package com.frograms.watcha.model.response.data;

import com.google.gson.annotations.SerializedName;

/**
 * 덱 좋아요 했을때 받는 데이터.
 */
public class LikeData {
  @SerializedName("is_liked") boolean isLiked;

  public boolean isLiked() {
    return isLiked;
  }
}
