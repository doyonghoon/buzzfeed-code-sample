package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.items.Content;
import com.google.gson.annotations.SerializedName;

public class ContentData {

  @SerializedName("content") public Content content;

  public Content getContent() {
    return content;
  }
}
