package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.items.bases.DeckBase;

/**
 * 컬렉션 데이터.
 */
public class DeckData {

  public DeckBase deck;

  public DeckBase getDeck() {
    return deck;
  }
}
