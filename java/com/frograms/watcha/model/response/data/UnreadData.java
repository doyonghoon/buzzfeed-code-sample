package com.frograms.watcha.model.response.data;

import com.google.gson.annotations.SerializedName;

public class UnreadData {

  @SerializedName("unread") private int unread;

  public int getUnread() {
    return unread;
  }
}
