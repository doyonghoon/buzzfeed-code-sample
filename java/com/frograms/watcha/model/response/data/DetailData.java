package com.frograms.watcha.model.response.data;

import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.response.data.list.ListData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * 개영페 데이터.
 */
public class DetailData extends ListData {
  private static final Gson INTERNAL_GSON = new GsonBuilder().setPrettyPrinting().create();

  @SerializedName("content") protected Content mContent;

  public Content getContent() {
    return mContent;
  }

  @Override public String toString() {
    return INTERNAL_GSON.toJson(this);
  }
}
