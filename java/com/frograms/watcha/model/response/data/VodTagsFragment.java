package com.frograms.watcha.model.response.data;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.ToolbarHelper;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.SelectedTagsView;
import com.frograms.watcha.view.widget.TagLayout;
import com.frograms.watcha.view.widget.TagTextView;
import com.rey.material.widget.ProgressView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 감상태그들 목록.
 */
public class VodTagsFragment extends BaseFragment
    implements TagLayout.OnTagItemClickListener, SelectedTagsView.OnTagItemResetListener {

  @Bind(R.id.vod_tag_layout) LinearLayout mLayout;
  @Bind(R.id.progress_loading) ProgressView mProgressView;

  private List<TagItems.Tag> mSelectedTags = new ArrayList<>();

  private int[] mBundledTagIds = null;
  private QueryType mQueryType = QueryType.VOD_TAGS;

  @Override protected QueryType getQueryType() {
    return mQueryType;
  }

  @Override protected int getLayoutId() {
    return R.layout.frag_vod_tag;
  }

  @Override public void setBundle(Bundle bundle) {
    super.setBundle(bundle);
    if (bundle != null && bundle.containsKey(BundleSet.TAG_IDS)) {
      mBundledTagIds = bundle.getIntArray(BundleSet.TAG_IDS);
    }
  }

  @Nullable @Override public String getCurrentScreenName() {
    return AnalyticsManager.TAG_SETTING_MOVIE_VOD;
  }

  @Override protected ToolbarHelper.ToolbarActionType getToolbarActionType() {
    return ToolbarHelper.ToolbarActionType.NONE;
  }

  @Override public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    load();
    mProgressView.setVisibility(View.VISIBLE);
  }

  private TagLayout createTagLayout(String title, List<TagItems.Tag> tags) {
    TagLayout l = new TagLayout(getActivity());
    l.setTitle(title);
    l.setOnClickTagItemListener(this);
    for (TagItems.Tag t : tags) {
      boolean isBundledTagItem = isFocusedTagItem(t);
      l.addTag(t, isBundledTagItem);
      if (isBundledTagItem) {
        mSelectedTags.add(t);
        CacheManager.putTag(t);
      }
    }
    return l;
  }

  private boolean isFocusedTagItem(TagItems.Tag tag) {
    if (mBundledTagIds == null) {
      return false;
    }
    for (int id : mBundledTagIds) {
      if (tag.getId() == id) {
        return true;
      }
    }
    return false;
  }

  private TagTextView getTagTextView(TagItems.Tag tag) {
    if (mLayout != null && mLayout.getChildCount() > 0) {
      for (int i = 0; i < mLayout.getChildCount(); i++) {
        if (mLayout.getChildAt(i) instanceof TagLayout) {
          TagLayout l = (TagLayout) mLayout.getChildAt(i);
          TagTextView tmp = l.getTagTextView(tag);
          if (tmp != null && tmp.getTagData() != null && tmp.getTagData().equals(tag)) {
            return tmp;
          }
        }
      }
    }
    return null;
  }

  @Override public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse result) {
    super.onSuccess(queryType, result);
    if (mProgressView != null && mProgressView.getVisibility() != View.GONE) {
      mProgressView.setVisibility(View.GONE);
    }
    if (result.getData() != null && result.getData() instanceof HashMap) {
      Map<String, List<TagItems.Tag>> tags =
          ((HashMap<String, List<TagItems.Tag>>) result.getData());
      if (!tags.isEmpty()) {
        for (Map.Entry<String, List<TagItems.Tag>> t : tags.entrySet()) {
          String key = t.getKey();
          List<TagItems.Tag> value = t.getValue();
          mLayout.addView(createTagLayout(key, value));
        }
      }
    }
  }

  @Override public void onClickTagItem(TagLayout layout, TagTextView v, TagItems.Tag tag) {
    switch (layout.getId()) {
      default:
        if (mSelectedTags.contains(tag)) {
          // 이미 선택된 태그를 클릭했다면 지움.
          v.setFocused(false);
          mSelectedTags.remove(tag);
          CacheManager.removeTag(tag);
        } else {
          // 선택한 태그 추가.
          v.setFocused(true);
          mSelectedTags.add(tag);
          CacheManager.putTag(tag);
        }
        break;
    }
  }

  @Override public void resetTagItems(List<TagItems.Tag> tags) {
    TagLayout tagLayout;
    for (int i = 0; i < mLayout.getChildCount(); i++) {
      if (mLayout.getChildAt(i) instanceof TagLayout) {
        tagLayout = (TagLayout) mLayout.getChildAt(i);
        for (TagItems.Tag tag : tags) {
          TagTextView tagText = tagLayout.getTagTextView(tag);
          if (tagText != null) {
            mSelectedTags.remove(tag);
            tagText.setFocused(false);
          }
        }
      }
    }
  }
}