package com.frograms.watcha.model;

/**
 * 서버 로깅에 사용될 데이터 집합.
 */
public enum LogAction {
  RATING,
  LOGIN,
}
