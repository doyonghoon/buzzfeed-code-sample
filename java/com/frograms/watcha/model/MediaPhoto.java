package com.frograms.watcha.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class MediaPhoto implements Parcelable {
  @SerializedName("original") private String mOriginal;
  @SerializedName("xlarge") private String mXLarge;
  @SerializedName("large") private String mLarge;
  @SerializedName("medium") private String mMedium;
  @SerializedName("small") private String mSmall;

  public MediaPhoto(Parcel in) {
    readFromParcel(in);
  }

  public String getOriginal() {
    return mOriginal;
  }

  public String getXLarge() {
    return mXLarge;
  }

  public String getLarge() {
    return mLarge;
  }

  public String getMedium() {
    return mMedium;
  }

  public String getSmall() {
    return mSmall;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(mOriginal);
    dest.writeString(mXLarge);
    dest.writeString(mLarge);
    dest.writeString(mMedium);
    dest.writeString(mSmall);
  }

  private void readFromParcel(Parcel in) {
    mOriginal = in.readString();
    mXLarge = in.readString();
    mLarge = in.readString();
    mMedium = in.readString();
    mSmall = in.readString();
  }

  public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
    public MediaPhoto createFromParcel(Parcel in) {
      return new MediaPhoto(in);
    }

    public MediaPhoto[] newArray(int size) {
      return new MediaPhoto[size];
    }
  };
}
