package com.frograms.watcha.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by juyupsung on 2014. 8. 7..
 */
public class LinkText {
  @SerializedName("text") protected String text;
  @SerializedName("links") protected ArrayList<Link> links;

  public class Link {
    @SerializedName("text") protected String text;
    @SerializedName("url") protected String url;

    public String getText() {
      return text;
    }

    public String getUrl() {
      return url;
    }
  }

  public String getText() {
    return text;
  }

  public ArrayList<Link> getLinks() {
    if (links == null) return new ArrayList<Link>();
    return links;
  }
}
