package com.frograms.watcha.model.headers;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juyupsung on 2014. 8. 5..
 */
public class CardHeader {
  @SerializedName("type") protected String type;
  @SerializedName("title") protected String title;

  public String getType() {
    return type;
  }

  public String getTitle() {
    return title;
  }
}
