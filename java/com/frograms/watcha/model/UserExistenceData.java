package com.frograms.watcha.model;

import com.frograms.watcha.model.items.Item;
import com.google.gson.annotations.SerializedName;

/**
 * 유저가 왓챠 서버에 가입되어 있는지 확인할 때 쓰는 모델.
 */
public class UserExistenceData {

  @SerializedName("exist") boolean exist;
  @SerializedName("email") String email;

  public String getEmail() {
    return email;
  }

  public boolean isExist() {
    return exist;
  }

  @Override public String toString() {
    return Item.gson.toJson(this);
  }
}
