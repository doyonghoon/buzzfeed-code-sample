package com.frograms.watcha.model;

import com.google.gson.annotations.SerializedName;

public class Cover {
  @SerializedName("default") private String mDefault;
  @SerializedName("small") private String mSmall;
  @SerializedName("thumb") private String mThumb;

  public String getDefault() {
    return mDefault;
  }

  public String getSmall() {
    return mSmall;
  }

  public String getThumb() {
    return mThumb;
  }
}
