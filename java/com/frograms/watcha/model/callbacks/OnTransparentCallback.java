package com.frograms.watcha.model.callbacks;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.observablescrollview.ObservableScrollViewCallbacks;
import com.frograms.watcha.observablescrollview.ScrollState;

public class OnTransparentCallback implements ObservableScrollViewCallbacks {

  private BaseActivity mActivity;

  private Drawable mDrawable;
  private View mListHeaderView;

  public OnTransparentCallback(BaseActivity activity) {
    mActivity = activity;
  }

  @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    mDrawable = mActivity.getToolbarHelper().getToolbar().getBackground();
    mListHeaderView = ((ListFragment) mActivity.getFragment()).getToolbarAsHeaderView();
    if (mListHeaderView == null || mDrawable == null) {
      return;
    }
    float height = (float) (-mListHeaderView.getTop()) / mListHeaderView.getHeight();
    mDrawable.setAlpha(Math.min(255, (int) (height * 255)));
  }

  @Override public void onDownMotionEvent() {
  }

  @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {
  }

  public void clear() {
    if (mActivity != null) {
      mActivity.getToolbarHelper().getToolbar().getBackground().setAlpha(0);
      mActivity.getToolbarHelper()
          .getToolbar()
          .getBackground()
          .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }
  }
}