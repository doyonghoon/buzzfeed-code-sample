package com.frograms.watcha.model.callbacks;

import com.frograms.watcha.observablescrollview.ObservableScrollViewCallbacks;
import com.frograms.watcha.observablescrollview.ScrollState;
import java.util.ArrayList;
import java.util.List;

public class BaseScrollCallbacks implements ObservableScrollViewCallbacks {

  private List<ObservableScrollViewCallbacks> mCallbacks = new ArrayList<>();

  public BaseScrollCallbacks() {
  }

  public List<ObservableScrollViewCallbacks> getCallbacks() {
    return mCallbacks;
  }

  public void addCallback(ObservableScrollViewCallbacks callback) {
    if (callback != null && !mCallbacks.contains(callback)) {
      mCallbacks.add(callback);
    }
  }

  @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    if (mCallbacks != null && !mCallbacks.isEmpty()) {
      for (ObservableScrollViewCallbacks c : mCallbacks) {
        c.onScrollChanged(scrollY, firstScroll, dragging);
      }
    }
  }

  @Override public void onDownMotionEvent() {
    if (mCallbacks != null && !mCallbacks.isEmpty()) {
      for (ObservableScrollViewCallbacks c : mCallbacks) {
        c.onDownMotionEvent();
      }
    }
  }

  @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    if (mCallbacks != null && !mCallbacks.isEmpty()) {
      for (ObservableScrollViewCallbacks c : mCallbacks) {
        c.onUpOrCancelMotionEvent(scrollState);
      }
    }
  }
}