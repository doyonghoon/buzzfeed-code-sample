package com.frograms.watcha.model.callbacks;

import android.support.v7.widget.Toolbar;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.observablescrollview.ObservableScrollViewCallbacks;
import com.frograms.watcha.observablescrollview.ScrollState;

public class OnUpDownScrollCallback implements ObservableScrollViewCallbacks {

  private BaseActivity mActivity;
  private int mBaseTranslationY;

  private int mScrollY = 0;
  private boolean mOnMove = false;

  public OnUpDownScrollCallback(BaseActivity activity) {
    mActivity = activity;
  }

  @Override public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    if (mScrollY == 0 || !mOnMove) {
      mScrollY = scrollY;
      return;
    }

    Toolbar toolbar = mActivity.getToolbarHelper().getToolbar();
    float currentHeaderTranslationY = mActivity.getHeaderView().getTranslationY();

    if (scrollY > mScrollY) {
      int toolbarHeight = toolbar.getHeight();
      if (-toolbarHeight < currentHeaderTranslationY) {
        float translationY =
            Math.max(-toolbarHeight, currentHeaderTranslationY - (scrollY - mScrollY));
        mActivity.getHeaderView().animate().cancel();
        mActivity.getHeaderView().setTranslationY(translationY);
      }
    } else if (scrollY < mScrollY) {
      if (0 > currentHeaderTranslationY) {
        float translationY = Math.min(0, currentHeaderTranslationY + (mScrollY - scrollY));
        mActivity.getHeaderView().animate().cancel();
        mActivity.getHeaderView().setTranslationY(translationY);
      }
    }

    mScrollY = scrollY;
    //if (dragging) {
    //  int toolbarHeight = toolbar.getHeight();
    //  if (firstScroll) {
    //    float currentHeaderTranslationY = mActivity.getHeaderView().getTranslationY();
    //    if (-toolbarHeight < currentHeaderTranslationY && toolbarHeight < scrollY) {
    //      mBaseTranslationY = scrollY;
    //    }
    //  }
    //  int headerTranslationY =
    //      Math.min(0, Math.max(-toolbarHeight, -(scrollY - mBaseTranslationY)));
    //  mActivity.getHeaderView().animate().cancel();
    //  mActivity.getHeaderView().setTranslationY(headerTranslationY);
    //}
  }

  @Override public void onDownMotionEvent() {
    mOnMove = true;
  }

  @Override public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    mOnMove = false;

    Toolbar mToolbarView = mActivity.getToolbarHelper().getToolbar();
    mBaseTranslationY = 0;

    float headerTranslationY = mActivity.getHeaderView().getTranslationY();
    int toolbarHeight = mToolbarView.getHeight();
    if (scrollState == ScrollState.UP) {
      if (headerTranslationY != -toolbarHeight) {
        mActivity.getHeaderView().animate().cancel();
        mActivity.getHeaderView().animate().translationY(0).setDuration(170).start();
      }
    } else if (scrollState == ScrollState.DOWN) {
      //if (headerTranslationY != 0) {
      mActivity.getHeaderView().animate().cancel();
      mActivity.getHeaderView().animate().translationY(0).setDuration(170).start();
      //}
    }
  }
}