package com.frograms.watcha.model;

import com.frograms.watcha.model.items.Item;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by juyupsung on 2014. 8. 4..
 */
public class Card {
  @SerializedName("card_type") protected String cardType;
  @SerializedName("items") protected ArrayList<CardItem> listCardItem;
  @SerializedName("header") protected CardItem header;
  @SerializedName("footer") protected CardItem footer;
  @SerializedName("merge") protected boolean isMerge;

  public void setCardType(String card_type) {
    cardType = card_type;
  }

  public void setListCardItem(ArrayList<CardItem> list) {
    listCardItem = list;
  }

  public void setHeader(CardItem item) {
    header = item;
  }

  public void setFooter(CardItem item) {
    footer = item;
  }

  public String getCardType() {
    return cardType;
  }

  public ArrayList<CardItem> getListCardItem() {
    return listCardItem;
  }

  public CardItem getHeader() {
    return header;
  }

  public CardItem getFooter() {
    return footer;
  }

  public boolean isMerge() {
    return isMerge;
  }

  @Override public String toString() {
    return Item.gson.toJson(this);
  }
}
