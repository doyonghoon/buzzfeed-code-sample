package com.frograms.watcha.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * Created by parkjiho on 15. 7. 8..
 */
public class ProfilePhoto implements Parcelable {
  @SerializedName("original") private String mOriginal;
  @SerializedName("large") private String mLarge;
  @SerializedName("small") private String mSmall;

  public ProfilePhoto(Parcel in) {
    readFromParcel(in);
  }

  public String getOriginal() {
    return mOriginal;
  }

  public String getLarge() {
    return mLarge;
  }


  public String getSmall() {
    return mSmall;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(mOriginal);
    dest.writeString(mLarge);
    dest.writeString(mSmall);
  }

  private void readFromParcel(Parcel in) {
    mOriginal = in.readString();
    mLarge = in.readString();
    mSmall = in.readString();
  }

  public static final Creator CREATOR = new Creator() {
    public ProfilePhoto createFromParcel(Parcel in) {
      return new ProfilePhoto(in);
    }

    public ProfilePhoto[] newArray(int size) {
      return new ProfilePhoto[size];
    }
  };
}