package com.frograms.watcha.model;

import com.frograms.watcha.model.items.Item;
import com.google.gson.annotations.SerializedName;

public class Button extends Item {

  @SerializedName("text") protected String text;
  @SerializedName("dismiss") protected String dismiss;

  public String getText() {
    return text;
  }

  public String getDismiss() {
    return dismiss;
  }
}
