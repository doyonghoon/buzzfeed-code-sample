package com.frograms.watcha.model;

import com.google.gson.annotations.SerializedName;

/**
 * 영평늘의 목록의 기준.
 */
public class ItemCategory {

  @SerializedName("id") protected String id;
  @SerializedName("name") protected String name;
  @SerializedName("thumbnail") protected String thumbnail;

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getThumbnail() {
    return thumbnail;
  }
}
