package com.frograms.watcha.model.views;

import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.viewHolders.ViewHolderHelper.CARD_TYPE;

public class ViewCard {

  public static enum LOCATION {
    ALL, TOP, BOT, NONE
  }

  protected CARD_TYPE mCardType;
  protected CardItem mItem;
  protected LOCATION mLocationId;

  protected boolean isMerge;
  protected boolean isReversed = false;

  public void setIsMerge(boolean isMerge) {
    this.isMerge = isMerge;
  }

  public boolean isMerge() {
    return isMerge;
  }

  public ViewCard(CARD_TYPE card_type, CardItem item) {
    mCardType = card_type;
    mItem = item;
  }

  public CARD_TYPE getCardType() {
    return mCardType;
  }

  public CardItem getCardItem() {
    return mItem;
  }

  public void setLocation(LOCATION location) {
    mLocationId = location;
  }

  public LOCATION getLocation() {
    return mLocationId;
  }

  public boolean isReversed() {
    return isReversed;
  }

  public void setIsReversed(boolean isReversed) {
    this.isReversed = isReversed;
  }
}
