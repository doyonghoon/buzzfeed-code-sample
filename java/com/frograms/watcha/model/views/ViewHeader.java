package com.frograms.watcha.model.views;

import com.frograms.watcha.model.headers.CardHeader;

public class ViewHeader {

  private CardHeader header;

  public ViewHeader(CardHeader header) {
    this.header = header;
  }

  public CardHeader getHeader() {
    return header;
  }
}
