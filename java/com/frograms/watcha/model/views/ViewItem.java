package com.frograms.watcha.model.views;

import com.frograms.watcha.model.CardItem;
import java.util.List;

public class ViewItem {

  public static enum VIEW_TYPE {GRID, SINGLE}

  ;

  private VIEW_TYPE view_type;

  private CardItem item;
  private List<CardItem> items;

  public ViewItem(List<CardItem> items) {
  }

  public VIEW_TYPE getView() {
    return view_type;
  }

  public CardItem getItem() {
    return item;
  }

  public void setItem(CardItem item) {
    this.item = item;
  }

  public List<CardItem> getItems() {
    return items;
  }
}
