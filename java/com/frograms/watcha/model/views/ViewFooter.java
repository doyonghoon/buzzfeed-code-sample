package com.frograms.watcha.model.views;

import com.frograms.watcha.model.Button;

public class ViewFooter {

  private Button button;

  public ViewFooter(Button button) {
    this.button = button;
  }

  public Button getButton() {
    return button;
  }
}
