package com.frograms.watcha.model.views;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import carbon.animation.AnimUtils;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.DetailCommentFragment;
import com.frograms.watcha.fragment.UserProfileFragment;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.PartnerHelper;
import com.frograms.watcha.listeners.LightAnimationListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.RatingUtils;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.itemViews.ItemContentXLargeView;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsCommentActionView;
import com.frograms.watcha.view.textviews.FeedRatingTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.textviews.RichTextView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.nineoldandroids.animation.Animator;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by parkjiho on 15. 6. 19..
 * ItemUserActionXLargeView랑 ItemUserActionLargeView랑 상속관계에 놓게 하기 위한 abstract class
 */
public abstract class ItemUserActionView<T extends Item> extends ItemAbsCommentActionView<T> {
  @Bind(R.id.photo) GreatImageView mUserPhotoView;
  @Bind(R.id.title) NameTextView mUserNameView;
  @Bind(R.id.follow) TextView mFollowView;
  @Bind(R.id.description) TextView mDescView;
  @Bind(R.id.more) FontIconTextView mMoreView;

  @Bind(R.id.comment_winner) ImageView mWinner;

  @Bind(R.id.comment) RichTextView mCommentView;
  @Bind(R.id.comment_activity) RichTextView mActivityView;

  @Bind(R.id.content_layer) ItemContentXLargeView mContentLayer;

  @Bind(R.id.comment_likecount) TextView mLikeCountView;
  @Bind(R.id.comment_repliescount) TextView mRepliesCountView;
  @Bind(R.id.like_grid) IconActionGridButton mLikeView;
  @Bind(R.id.reply_grid) IconActionGridButton mReplyView;

  @Bind(R.id.blind_layer) View mBlindLayer;

  protected String mUserCode;
  protected boolean isMine;

  public ItemUserActionView(Context context) {
    super(context);
  }

  public ItemUserActionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public int getLayoutId() {
    return R.layout.view_item_useraction_large;
  }

  @Override protected void init() {
    super.init();
    FontHelper.Bold(mUserNameView);
    mContentLayer.setVisibility(GONE);

    mMoreView.setVisibility(VISIBLE);
    mMoreView.setOnClickListener(this);
    mFollowView.setOnClickListener(this);

    mLikeCountView.setOnClickListener(this);
    mRepliesCountView.setOnClickListener(this);
    mLikeView.setOnClickListener(this);
    mReplyView.setOnClickListener(this);
  }

  private PartnerHelper.OnBaseFollowResponseListener createFollowListener(Context context) {
    return new PartnerHelper.OnBaseFollowResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);

        UserBase user = CacheManager.getUser(mUserCode);
        user.setFriend(true);
        CacheManager.putUser(user);

        final float translationX = mFollowView.getTranslationX();
        final float translationY = mFollowView.getTranslationY();
        AnimUtils.flyOut(mFollowView, new LightAnimationListener() {
          @Override public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            this.onAnimationEnd(animation);
          }

          @Override public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            mFollowView.setVisibility(View.GONE);
            ViewCompat.setTranslationX(mFollowView, translationX);
            ViewCompat.setTranslationY(mFollowView, translationY);
            ViewCompat.setAlpha(mFollowView, 1);
            if (getContext() != null && getContext() instanceof BaseActivity) {
              BaseFragment baseFragment = ((BaseActivity) getContext()).getFragment();
              if (baseFragment instanceof ListFragment) {
                ((ListFragment) baseFragment).getAdapter().notifyDataSetChanged();
              } else if (baseFragment instanceof AbsPagerFragment) {
                BaseFragment currentFragment =
                    ((AbsPagerFragment) baseFragment).getCurrentFragment();
                if (currentFragment instanceof ListFragment) {
                  ((ListFragment) currentFragment).getAdapter().notifyDataSetChanged();
                }
              }
            }
          }
        });
      }
    };
  }

  protected ItemContentXLargeView getContentLayer() {
    return mContentLayer;
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.photo:
        ActivityStarter.with(getContext(), FragmentTask.PROFILE)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                .putUserCode(mUserCode)
                .build()
                .getBundle())
            .start();
        break;

      case R.id.follow:
        PartnerHelper.setFollow((BaseActivity) getContext()
            , new ReferrerBuilder(getScreenName()).appendArgument(mUserCode)
            , mUserCode
            , true
            , createFollowListener(getContext()));
        break;

      case R.id.more:
        if (isMine) {
          showDialog();
        } else {
          showReportDialog();
        }
        break;

      case R.id.comment_likecount:
        if (mComment.getLikesCount() > 0) {
          ActivityStarter.with(getContext(), "watcha://comments/" + mComment.getCode() + "/likers")
              .start();
        }
        break;

      case R.id.comment_repliescount:
        if (mComment.getRepliesCount() > 0) {
          goReply(false);
        }
        break;

      case R.id.like_grid:
        requestLike();
        scaleSpringAnimation(mLikeView);
        break;

      case R.id.reply_grid:
        goReply(true);
        break;

      case R.id.blind_layer:
        AlertDialog.Builder builder =
            new AlertDialog.Builder(getContext()).setMessage(R.string.open_spoil_comment)
                .setPositiveButton(R.string.spoiler_open_ok, new DialogInterface.OnClickListener() {

                  @Override public void onClick(DialogInterface dialog, int which) {
                    mBlindLayer.setVisibility(View.GONE);
                  }
                })
                .setNegativeButton(R.string.report_confirm_cancel, null);

        builder.show();
        break;

      case R.id.content_layer:
        break;

      default:
        if (mComment != null
            && !(((BaseActivity) getContext()).getFragment() instanceof DetailCommentFragment)) {
          ActivityStarter.with(getContext(), "watcha://comments/" + mComment.getCode()).start();
        }
        break;
    }
  }

  @Override
  public boolean setItem(T item) {
    return super.setItem(item);
  }

  protected boolean setUserActionItem(UserAction item) {

    mContentCode = item.getContentCode();
    if (item.getUser().getCode().equals(WatchaApp.getUser().getCode())) {

      item = CacheManager.getMyUserAction(item.getContentCode());
      mComment = item.getComment();
    } else {

      if (item.getComment() != null) {
        mComment = CacheManager.getComment(item.getComment().getCode());
      } else {
        mComment = null;
      }
    }

    if (item == null) {
      return false;
    }

    if (item.getUser() != null) {
      String url =
          (item.getUser().getPhoto() == null) ? null : item.getUser().getPhoto().getSmall();
      mUserPhotoView.load(url);

      mUserNameView.setUser(item.getUser());

      mName = item.getUser().getName();
      isMine = item.getUser().getCode().equals(WatchaApp.getUser().getCode());

      mUserCode = item.getUser().getCode();
      mUserPhotoView.setOnClickListener(this);

      if (!WatchaApp.getUser().getCode().equals(mUserCode)) {
        UserBase user = CacheManager.getUser(mUserCode);
        if (user != null) {
          item.getUser().setFriend(CacheManager.getUser(mUserCode).isFriend());
        }
        mFollowView.setVisibility((item.getUser().isFriend()) ? GONE : VISIBLE);
        if (getContext() != null && getContext() instanceof BaseActivity) {
          BaseFragment baseFragment = ((BaseActivity) getContext()).getFragment();
          if (baseFragment != null && baseFragment instanceof UserProfileFragment) {
            String userCode = ((UserProfileFragment) baseFragment).getUserCode();
            boolean hide = StringUtils.equals(userCode, item.getUser().getCode());
            mFollowView.setVisibility(hide ? View.GONE : View.VISIBLE);
          }
        }
      } else {
        mFollowView.setVisibility(GONE);
      }
    }

    if (mComment != null) {
      setOnClickListener(this);

      if (mComment.isBlind() && !mComment.isLiked() && !mUserCode.equals(
          WatchaApp.getUser().getCode())) {
        mBlindLayer.setVisibility(View.VISIBLE);
        mBlindLayer.setOnClickListener(this);
      } else {
        mBlindLayer.setVisibility(View.GONE);
      }

      mCommentView.setText("");
      FeedRatingTextView.getRatingText(mCommentView, item);
      mCommentView.append(" ");
      if (mComment.isWinner()) {
        mCommentView.toBoldText("• " + getContext().getString(R.string.watcha_premiere) + " ");
        mWinner.setVisibility(VISIBLE);
      } else {
        mWinner.setVisibility(GONE);
      }

      mCommentView.append(mComment.getText());

      if (mComment.getWatchedAt() == null) {
        mActivityView.setVisibility(View.GONE);
      } else {
        mActivityView.setVisibility(View.VISIBLE);

        Date watchedAt = mComment.getWatchedAt();

        if (watchedAt != null) {
          mActivityView.setText(RatingUtils.getHumanReadableDate(watchedAt) + "에 봄");
        }
      }

      ((View) mLikeView.getParent().getParent()).setVisibility(VISIBLE);

      mDescView.setCompoundDrawables(null, null, null, null);

      mLikeCountView.setVisibility(View.VISIBLE);
      setLiked();
      setReplies();
    } else {
      setOnClickListener(null);

      mBlindLayer.setVisibility(View.GONE);

      mActivityView.setVisibility(GONE);
      ((View) mLikeView.getParent().getParent()).setVisibility(GONE);
      mLikeCountView.setVisibility(View.GONE);

      if (item.getRating() > 0) {
        mCommentView.setText(
            getContext().getResources().getStringArray(R.array.rating_text)[Math.max(
                (int) item.getRating() - 1, 0)]);
      } else if (item.isWished()) {
        mCommentView.setText(getContext().getString(R.string.wish));
      } else if (item.isMehed()) {
        mCommentView.setText(getContext().getString(R.string.ignore));
      }
    }

    if (item.getUpdatedAt() != null) {
      mDescView.setText(Util.calTime(item.getUpdatedAt().getTime(), new Date().getTime())
          + getContext().getString(R.string.before));
    } else {
      mDescView.setText("");
    }

    return true;
  }

  @Override protected void setLiked() {
    mLikeView.setFocused(mComment.isLiked());
    if (mComment.getLikesCount() > 0) {
      mLikeCountView.setVisibility(VISIBLE);
      mLikeCountView.setText(String.format("%s %s%s", getResources().getString(R.string.like),
          mComment.getLikesCount(), getResources().getString(R.string.rating_unit)));
    } else {
      mLikeCountView.setVisibility(GONE);
    }
  }

  @Override
  protected void setReplies() {
    if (mComment.getRepliesCount() > 0) {
      mRepliesCountView.setVisibility(VISIBLE);
      mRepliesCountView.setText(getResources().getString(R.string.reply)
          + " "
          + mComment.getRepliesCount()
          + getResources().getString(R.string.rating_unit));
    } else {
      mRepliesCountView.setVisibility(GONE);
    }
  }

}