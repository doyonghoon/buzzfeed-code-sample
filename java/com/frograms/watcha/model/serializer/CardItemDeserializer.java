package com.frograms.watcha.model.serializer;

import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.CardItem;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.viewHolders.ViewHolderHelper;
import com.frograms.watcha.viewHolders.ViewHolderMeta;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class CardItemDeserializer implements JsonDeserializer<CardItem> {

  @Override public CardItem deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
      throws JsonParseException {
    if (je == null) return null;

    final ViewHolderMeta itemType = getItemType(je);
    final Item item = getItem(je, itemType);

    CardItem cardItem = new CardItem();
    cardItem.setItem(item);
    cardItem.setItemType(itemType);
    return cardItem;
  }

  private Item getItem(JsonElement je, ViewHolderMeta itemType) {
    final JsonObject itemObject = je.getAsJsonObject().getAsJsonObject("item");
    Class<? extends Item> itemTypeClass = itemType != null ? itemType.getItemType() : null;
    return itemTypeClass == null ? null
        : WatchaApp.getInstance().getGson().fromJson(itemObject, itemType.getItemType());
  }

  private ViewHolderMeta getItemType(JsonElement je) {
    final String itemType = je.getAsJsonObject().get("item_type").getAsString();
    return ViewHolderHelper.getItemType(itemType);
  }
}