package com.frograms.watcha.model.serializer;

import android.os.Parcel;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.items.UserBase;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * 일반 유저 파싱할때 캐싱함.
 */
public class UserBaseDeserializer implements JsonDeserializer<UserBase> {

  private static class SerializedUserBase extends UserBase {
    public SerializedUserBase(Parcel in) {
      super(in);
    }
  }

  @Override
  public UserBase deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    if (json != null && WatchaApp.getUser() != null) {
      SerializedUserBase user = WatchaApp.getInstance()
          .getGson()
          .fromJson(json.getAsJsonObject(), SerializedUserBase.class);
      if (user != null && !user.getCode().equals(WatchaApp.getUser().getCode())) {
        CacheManager.putUser(user);
      }
      return user;
    }
    return null;
  }
}
