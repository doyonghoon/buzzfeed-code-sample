package com.frograms.watcha.model.serializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by juyupsung on 2014. 7. 23..
 */
public class DateDeserializer implements JsonDeserializer<Date> {

  private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+09:00");
  private static SimpleDateFormat DATE_FORMAT2 = new SimpleDateFormat("yyyy-MM-dd");

  @Override public Date deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
      throws JsonParseException {
    try {
      return DATE_FORMAT.parse(arg0.getAsString());
    } catch (Exception ex) {
      try {
        return DATE_FORMAT2.parse(arg0.getAsString());
      } catch(Exception ex2) {
      }
      return null;
    }
  }
}