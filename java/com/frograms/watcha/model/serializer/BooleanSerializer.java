package com.frograms.watcha.model.serializer;

import android.text.TextUtils;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * Created by doyonghoon on 14. 9. 29..
 */
public class BooleanSerializer implements JsonDeserializer<Boolean> {
  @Override public Boolean deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
      throws JsonParseException {
    try {
      if (arg0 == null || arg0.isJsonNull()) return false;
      if (TextUtils.isDigitsOnly(arg0.toString())) {
        return arg0.getAsInt() == 1;
      } else {
        return arg0.getAsBoolean();
      }
    } catch (Exception e) {
      return false;
    }
  }
}
