package com.frograms.watcha.model.serializer;

import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.utils.WLog;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class UserActionDeserializer implements JsonDeserializer<UserAction> {

  private static class SerializedUserAction extends UserAction {
  }

  @Override
  public UserAction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    if (json != null) {
      SerializedUserAction action = WatchaApp.getInstance()
          .getGson()
          .fromJson(json.getAsJsonObject(), SerializedUserAction.class);

      if (WatchaApp.getUser() == null) WLog.e("User Null!!!");
      if (action.getUser() == null) WLog.e("Action's User Null!!!");
      if (WatchaApp.getUser().getCode() == null) WLog.e("User's code null!!");
      if (action.getUser().getCode() == null) WLog.e("Action's UserCode null!!");

      if (action.getUser() != null &&
          WatchaApp.getUser() != null &&
          WatchaApp.getUser().getCode().equals(action.getUser().getCode())) {
        CacheManager.putMyUserAction(action);
      } else if (action.getComment() != null) {
        CacheManager.putComment(action.getComment());
      }

      return action;
    }
    return null;
  }
}
