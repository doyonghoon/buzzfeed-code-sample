package com.frograms.watcha.model.serializer;

import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class DeckDeserializer implements JsonDeserializer<DeckBase> {

  private static class SerializedDeck extends DeckBase {
  }

  @Override
  public DeckBase deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    if (json != null) {
      SerializedDeck deck =
          WatchaApp.getInstance().getGson().fromJson(json.getAsJsonObject(), SerializedDeck.class);

      DeckBase defaultDeck = CacheManager.getDeck(deck.getCode());
      if (defaultDeck != null) {
        if (deck.getUser() == null) deck.setUser(defaultDeck.getUser());

        if (deck.getCode() == null) deck.setCode(defaultDeck.getCode());

        if (deck.getName() == null) deck.setName(defaultDeck.getName());

        if (deck.getContents() == null) deck.setContents(defaultDeck.getContents());

        if (deck.getImage() == null) deck.setImage(defaultDeck.getImage());
      }

      CacheManager.putDeck(deck);
      return deck;
    }

    return null;
  }
}
