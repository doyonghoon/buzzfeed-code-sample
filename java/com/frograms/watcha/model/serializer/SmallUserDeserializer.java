package com.frograms.watcha.model.serializer;

import android.os.Parcel;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.items.UserSmall;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * 일반 유저 파싱할때 캐싱함.
 */
public class SmallUserDeserializer implements JsonDeserializer<UserSmall> {

  private static class SerializedUserSmall extends UserSmall {
    public SerializedUserSmall(Parcel in) {
      super(in);
    }
  }

  @Override
  public UserSmall deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    if (json != null) {
      SerializedUserSmall user = WatchaApp.getInstance()
          .getGson()
          .fromJson(json.getAsJsonObject(), SerializedUserSmall.class);
      if (user != null) {
        //WLog.i("name: " + user.getName() + ", is_friend: " + user.isFriend());
        CacheManager.putUser(user);
      }
      return user;
    }
    return null;
  }
}
