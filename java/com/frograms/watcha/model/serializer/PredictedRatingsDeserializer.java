package com.frograms.watcha.model.serializer;

import android.text.TextUtils;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.items.PredictedRatings;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * 나와 무비파트너의 예상별점을 UserCode로 가져오는 클래스.
 */
public class PredictedRatingsDeserializer implements JsonDeserializer<PredictedRatings> {
  @Override public PredictedRatings deserialize(JsonElement json, Type typeOfT,
      JsonDeserializationContext context) throws JsonParseException {
    if (json != null) {
      PredictedRatings ratings = new PredictedRatings();

      final String userCode = getMyUserCode();
      if (!TextUtils.isEmpty(userCode)
          && json.getAsJsonObject().getAsJsonPrimitive(getMyUserCode()) != null) {
        ratings.setMyRating(
            json.getAsJsonObject().getAsJsonPrimitive(getMyUserCode()).getAsFloat());
      }

      final String partnerUserCode = getMoviePartnerUserCode();
      if (!TextUtils.isEmpty(partnerUserCode)
          && json.getAsJsonObject().getAsJsonPrimitive(getMoviePartnerUserCode()) != null) {
        ratings.setPartnerRating(
            json.getAsJsonObject().getAsJsonPrimitive(getMoviePartnerUserCode()).getAsFloat());
      }
      return ratings;
    }
    return null;
  }

  /**
   * 무비파트너 유저 코드를 반환 함.
   */
  private String getMoviePartnerUserCode() {
    if (WatchaApp.getUser().getPartner() != null) {
      return WatchaApp.getUser().getPartner().getCode();
    }
    return null;
  }

  /**
   * 나의 유저 코드를 반환 함.
   */
  private String getMyUserCode() {
    return WatchaApp.getUser().getCode();
  }
}
