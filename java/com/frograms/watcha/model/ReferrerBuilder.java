package com.frograms.watcha.model;

import android.text.TextUtils;
import com.frograms.watcha.helpers.AnalyticsManager;
import java.util.ArrayList;
import java.util.List;

/**
 * RnD 통계용 로그 값.
 *
 * Http Request 헤더에 붙음.
 */
public class ReferrerBuilder {

  @AnalyticsManager.ScreenNameType private final String mScreenName;
  private List<String> mArguments = new ArrayList<>();

  public ReferrerBuilder(@AnalyticsManager.ScreenNameType String screenName) {
    mScreenName = screenName;
  }

  public ReferrerBuilder appendArgument(String argument) {
    if (!TextUtils.isEmpty(argument)) {
      mArguments.add(argument);
    }
    return this;
  }

  @Override public String toString() {
    List<String> values = new ArrayList<>();
    values.add(mScreenName);
    values.addAll(mArguments);
    return TextUtils.join("/", values.toArray(new String[values.size()]));
  }
}
