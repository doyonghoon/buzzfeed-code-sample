package com.frograms.watcha.model;

import com.frograms.watcha.fragment.AddContentDeckFragment;
import com.frograms.watcha.fragment.BookEmptyFragment;
import com.frograms.watcha.fragment.ChangeInfoFragment;
import com.frograms.watcha.fragment.ChangeProfileFragment;
import com.frograms.watcha.fragment.CreateDeckFragment;
import com.frograms.watcha.fragment.DetailCommentFragment;
import com.frograms.watcha.fragment.DetailDeckFragment;
import com.frograms.watcha.fragment.DetailFragment;
import com.frograms.watcha.fragment.DramaTagsFragment;
import com.frograms.watcha.fragment.FindUserFragment;
import com.frograms.watcha.fragment.FollowFragment;
import com.frograms.watcha.fragment.GalleryFragment;
import com.frograms.watcha.fragment.GeneralCardsFragment;
import com.frograms.watcha.fragment.HomeFragment;
import com.frograms.watcha.fragment.IntroFragment;
import com.frograms.watcha.fragment.InviteFriendFragment;
import com.frograms.watcha.fragment.LoginFragment;
import com.frograms.watcha.fragment.PartnerCandidatesFragment;
import com.frograms.watcha.fragment.RatingTabFragment;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.fragment.ResetPasswordFragment;
import com.frograms.watcha.fragment.SearchFragment;
import com.frograms.watcha.fragment.SearchResultFragment;
import com.frograms.watcha.fragment.SearchUserFragment;
import com.frograms.watcha.fragment.SelectContentPagerFragment;
import com.frograms.watcha.fragment.SelectRatingCategoryFragment;
import com.frograms.watcha.fragment.SettingFragment;
import com.frograms.watcha.fragment.SignupFragment;
import com.frograms.watcha.fragment.SortContentFragment;
import com.frograms.watcha.fragment.TagSettingFragment;
import com.frograms.watcha.fragment.TutorialFragment;
import com.frograms.watcha.fragment.UserContentFragment;
import com.frograms.watcha.fragment.UserContentTabFragment;
import com.frograms.watcha.fragment.UserProfileFragment;
import com.frograms.watcha.fragment.WebviewFragment;
import com.frograms.watcha.fragment.WelcomeSignupFragment;
import com.frograms.watcha.fragment.WishedFragment;
import com.frograms.watcha.fragment.WriteCommentFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;

public enum FragmentTask {

  INIT(null),

  HOME(HomeFragment.class),

  WELCOME_SIGNUP(WelcomeSignupFragment.class),
  SELECT_CONTENT(SelectContentPagerFragment.class),
  WRITE_COMMENT(WriteCommentFragment.class),
  DETAIL_COMMENT(DetailCommentFragment.class),
  /**
   * 영화, tv 시리즈, 도서 탭이 있는 유저 보관함.
   */
  USER_CONTENT_CATEGORY_LIST(UserContentTabFragment.class),
  /**
   * 하나의 탭만 있는 유저 보관함.
   *
   * 디폴트로 영화.
   */
  USER_CONTENT_LIST(UserContentFragment.class),
  FOLLOW(FollowFragment.class),
  WISHED(WishedFragment.class),
  RATING_TAB(RatingTabFragment.class),
  TUTORIAL(TutorialFragment.class),
  WEBVIEW(WebviewFragment.class),
  SEARCH(SearchFragment.class),
  SEARCH_USER(SearchUserFragment.class),
  FIND_USER(FindUserFragment.class),
  SEARCH_RESULT(SearchResultFragment.class),
  DETAIL_CONTENT(DetailFragment.class),
  GALLERY(GalleryFragment.class),
  PARTNER_CANDIDATES(PartnerCandidatesFragment.class),
  DETAIL_DECK(DetailDeckFragment.class),
  LOGIN(LoginFragment.class),
  SIGNUP(SignupFragment.class),
  FIND_PASSWORD(ResetPasswordFragment.class),
  CREATE_DECK(CreateDeckFragment.class),
  SORT_CONTENT_DECK(SortContentFragment.class),
  INTRO(IntroFragment.class),
  PROFILE(UserProfileFragment.class),
  ADD_CONTENT_DECK(AddContentDeckFragment.class),

  GENERAL_CARDS(GeneralCardsFragment.class),

  REPLY(ReplyFragment.class),

  PHOTO_ALBUM(null),
  INVITE_FRIENDS(InviteFriendFragment.class),
  CHANGE_INFO(ChangeInfoFragment.class),
  CHANGE_PROFILE(ChangeProfileFragment.class),
  SETTING(SettingFragment.class),
  TAG_SETTING(TagSettingFragment.class),
  TAG_SETTING_DRAMA(DramaTagsFragment.class),
  EMPTY_BOOK(BookEmptyFragment.class),
  SELECT_RATING_CATEGORY(SelectRatingCategoryFragment.class);

  private Class<? extends BaseFragment> mFragmentClass;
  private String mTitle;

  FragmentTask(Class<? extends BaseFragment> frag) {
    mFragmentClass = frag;
  }

  public Class<? extends BaseFragment> getFragmentClass() {
    return mFragmentClass;
  }

  public int getValue() {
    return ordinal();
  }

  //public FragmentTask setTitle(String title) {
  //  mTitle = title;
  //  return this;
  //}

  //public String getTitle() {
  //  return mTitle;

  //}
}
