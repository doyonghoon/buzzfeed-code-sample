package com.frograms.watcha.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.AnimatorRes;
import android.support.annotation.IntDef;
import android.text.TextUtils;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.utils.ScreenNameUtils;
import com.frograms.watcha.utils.WLog;

public class ActivityStarter {

  /**
   * 유저가 뒤로가기로 이전 화면으로 이동했을 때 뭔가 데이터를 가져와야 하거나, 특별히 봐야할 변수가 있다면 아래에 정의된 걸로 사용 하면 됨.
   *
   * 참고 {@link BaseFragment#onActivityResult(int, int, Intent)}
   */
  @IntDef({
      CREATE_DECK_REQUEST, UPDATE_DECK_REQUEST, ADD_DECK_ITEM_REQUEST, SORT_DECK_ITEM_REQUEST,
      TAG_SETTING_REQUEST, SEARCH_RESULT_REQUEST, INCREASE_NUMBER_OF_CONTENTS_REQUEST,
      SELECT_EVALUATE_CATEGORY_REQUEST
  }) public @interface RequestCode {
  }

  public static final int CREATE_DECK_REQUEST = 0x01;
  public static final int UPDATE_DECK_REQUEST = 0x02;
  public static final int ADD_DECK_ITEM_REQUEST = 0x03;
  public static final int SORT_DECK_ITEM_REQUEST = 0x04;
  public static final int TAG_SETTING_REQUEST = 0x05;
  public static final int SEARCH_RESULT_REQUEST = 0x06;
  public static final int INCREASE_NUMBER_OF_CONTENTS_REQUEST = 0x07;
  public static final int SELECT_EVALUATE_CATEGORY_REQUEST = 0x08;

  private static ActivityStarter sStarter;
  public static String TASK = BuildConfig.APPLICATION_ID + ".Task";
  public static String BUNDLE = BuildConfig.APPLICATION_ID + ".Bundle";

  private Context mContext;
  private Intent mIntent;
  private Bundle mBundle;
  private AnimationType mAnimType = AnimationType.SLIDE_HORIZONTAL;

  private int mRequestCode;

  public enum AnimationType {
    NOTHING(Open.NOTHING, Close.NOTHING),
    NONE(null, null),
    SLIDE_HORIZONTAL(Open.SLIDE_LEFT, Close.SLIDE_RIGHT),
    SLIDE_UP(Open.SLIDE_UP, Close.SLIDE_DOWN);

    private Open open;
    private Close close;

    AnimationType(Open open, Close close) {
      this.open = open;
      this.close = close;
    }

    public Open getOpenAnimation() {
      return open;
    }

    public Close getCloseAnimation() {
      return close;
    }

    public enum Open {
      NOTHING(0, 0),
      SHRINK(R.anim.shrink_enter, R.anim.shrink_exit),
      SLIDE_UP(R.anim.anim_window_in, R.anim.anim_window_out),
      SLIDE_LEFT(R.anim.anim_window_in_horizontal, R.anim.anim_window_out_horizontal);

      private int in, out;

      Open(@AnimatorRes int in, @AnimatorRes int out) {
        this.in = in;
        this.out = out;
      }

      @AnimatorRes public int getInAnimationId() {
        return in;
      }

      @AnimatorRes public int getOutAnimationId() {
        return out;
      }
    }

    public enum Close {
      NOTHING(0, 0),
      SHRINK(R.anim.shrink_exit, R.anim.shrink_enter),
      SLIDE_DOWN(R.anim.anim_window_close_in, R.anim.anim_window_close_out),
      SLIDE_RIGHT(R.anim.anim_window_close_in_horizontal, R.anim.anim_window_close_out_horizontal);

      private int in, out;

      Close(@AnimatorRes int in, @AnimatorRes int out) {
        this.in = in;
        this.out = out;
      }

      @AnimatorRes public int getInAnimationId() {
        return in;
      }

      @AnimatorRes public int getOutAnimationId() {
        return out;
      }
    }
  }

  /**
   * Base 액티비티를 띄울때 사용 함.
   *
   * @param context 이 함수를 호출하는 context. 그냥 activity 넣으면 됨.
   * @param task 띄울 화면.
   * @return singleton으로 만들어진 starter.
   */
  public static ActivityStarter with(Context context, FragmentTask task) {
    if (sStarter == null) {
      synchronized (ActivityStarter.class) {
        if (sStarter == null) {
          sStarter = new ActivityStarter(context, task);
        }
      }
    } else {
      sStarter.newStarter(context, task);
    }
    return sStarter;
  }

  public static ActivityStarter with(Context context, String uri) {
    if (sStarter == null) {
      synchronized (ActivityStarter.class) {
        if (sStarter == null) {
          sStarter = new ActivityStarter(context, uri);
        }
      }
    } else {
      sStarter.newStarter(context, uri);
    }
    return sStarter;
  }

  public ActivityStarter(Context context, FragmentTask task) {
    newStarter(context, task);
  }

  public ActivityStarter(Context context, String uri) {
    newStarter(context, uri);
  }

  // SET ALL TO DEFAULT
  private void newStarter(Context context, FragmentTask task) {
    setContext(context);
    setTask(task);
    clearBundle();
    clearAnimation();
  }

  // SET ALL TO DEFAULT
  private void newStarter(Context context, String uri) {
    setContext(context);
    setTask(uri);
    clearBundle();
    clearAnimation();
  }

  // SET CONTEXT
  private void setContext(Context context) {
    mContext = context;
  }

  // SET TASK
  private ActivityStarter setTask(FragmentTask task) {
    WLog.d(task.name() + " loaded to Activity Starter");
    mRequestCode = task.getValue();
    mIntent = new Intent(mContext, BaseActivity.class);
    mIntent.putExtra(TASK, task);
    return this;
  }

  private ActivityStarter setTask(String uri) {
    WLog.d(uri + " loaded to Activity Starter");
    mIntent = new Intent(Intent.ACTION_VIEW);
    mIntent.setData(Uri.parse(uri));
    return this;
  }

  public ActivityStarter setRequestCode(@RequestCode int code) {
    mRequestCode = code;
    return this;
  }

  // SET BUNDLE
  public ActivityStarter addBundle(Bundle bundle) {
    if (mBundle == null) {
      mBundle = new Bundle();
    }

    mBundle.putAll(bundle);

    return this;
  }

  private void clearBundle() {
    if (mBundle == null) {
      mBundle = new Bundle();
    } else {
      mBundle.clear();
    }
  }

  public ActivityStarter setAnimationType(AnimationType type) {
    mAnimType = type;
    return this;
  }

  private void clearAnimation() {
    mAnimType = AnimationType.SLIDE_HORIZONTAL;
  }

  public void start() {
    if (mBundle == null) {
      mBundle = new Bundle();
    }
    if (!mBundle.containsKey(BundleSet.PREVIOUS_SCREEN_NAME)) {
      String previousScreenName = ScreenNameUtils.getScreenName(mContext);
      if (!TextUtils.isEmpty(previousScreenName)) {
        mBundle.putString(BundleSet.PREVIOUS_SCREEN_NAME, previousScreenName);
      }
    }
    mBundle.putSerializable(BundleSet.TRANSITION_ANIMATION_TYPE, mAnimType);
    mIntent.putExtra(BUNDLE, mBundle);

    Bundle options = null;

    if (mContext instanceof BaseActivity) {
      ((BaseActivity) mContext).startActivityForResult(mIntent, mRequestCode, options);
    } else {
      mContext.startActivity(mIntent, options);
    }

    if (mContext instanceof Activity && mAnimType != AnimationType.NONE) {
      ((Activity) mContext).overridePendingTransition(mAnimType.getOpenAnimation()
          .getInAnimationId(), mAnimType.getOpenAnimation().getOutAnimationId());
    }

    mBundle = null;
    mContext = null;
    mIntent = null;
    mRequestCode = 0;
    mAnimType = AnimationType.SLIDE_HORIZONTAL;
  }
}
