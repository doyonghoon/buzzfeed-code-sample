package com.frograms.watcha.model;

import com.frograms.watcha.model.views.ViewCard;
import com.frograms.watcha.viewHolders.ViewHolderHelper;
import java.util.ArrayList;

public class CardTask {
  private ArrayList<Card> cards;
  private ArrayList<ViewCard> viewCards;

  private boolean hasStartMargin;

  public CardTask(ArrayList<Card> mCards, boolean hasStartMargin) {
    this.hasStartMargin = hasStartMargin;
    cards = mCards;
    if (cards == null) return;
    for (Card card : cards) {
      setViewCard(card);
      this.hasStartMargin = false;
    }
  }

  public CardTask(ArrayList<Card> mCards) {
    this(mCards, false);
  }

  public CardTask(Card mCard) {
    cards = new ArrayList<Card>();
    if (mCard == null) return;
    cards.add(mCard);
    for (Card card : cards) {
      setViewCard(card);
    }
  }

  public ArrayList<ViewCard> getViewCards() {
    return viewCards;
  }

  public void setViewCard(Card card) {
    if (card == null) return;
    if (viewCards == null) viewCards = new ArrayList<>();

    ViewHolderHelper.CARD_TYPE cardType = ViewHolderHelper.getCardType(card.getCardType());

    boolean hasHeader = (card.getHeader() != null) ? true : false;
    boolean hasFooter = (card.getFooter() != null) ? true : false;

    if (hasFooter && card.getFooter().getItemType() == null) {
      card.setFooter(null);
      hasFooter = false;
    }

    if (card.getHeader() != null) {
      if (card.getHeader().getItemType() == null) {
        card.setHeader(null);
        hasHeader = false;
      } else {
        ViewCard viewCard = new ViewCard(ViewHolderHelper.CARD_TYPE.LIST, card.getHeader());
        viewCard.setLocation(ViewCard.LOCATION.TOP);

        if (hasStartMargin) {
          viewCard.setLocation(ViewCard.LOCATION.NONE);
          hasStartMargin = false;
        }

        viewCards.add(viewCard);
      }
    }

    if (card.getListCardItem() != null) {
      for (int Loop1 = 0; Loop1 < card.getListCardItem().size(); Loop1++) {
        CardItem item = card.getListCardItem().get(Loop1);

        if (item.getItemType() == null) {
          card.getListCardItem().remove(item);
        }
      }

      int last = ((card.getListCardItem().size() - 1) / 3) * 3;
      for (int Loop1 = 0; Loop1 < card.getListCardItem().size(); Loop1++) {
        ViewCard viewCard = new ViewCard(cardType, card.getListCardItem().get(Loop1));
        if (Loop1 == 0 || (cardType == ViewHolderHelper.CARD_TYPE.GRID && Loop1 <= 2)) {
          viewCard.setIsMerge(card.isMerge());
        } else {
          viewCard.setIsMerge(false);
        }

        if (cardType == ViewHolderHelper.CARD_TYPE.GRID) {
          if (Loop1 >= last && !hasFooter) {
            viewCard.setLocation(ViewCard.LOCATION.BOT);
          } else {
            viewCard.setLocation(ViewCard.LOCATION.NONE);
          }
        } else {
          if (Loop1 == 0
              && Loop1 == card.getListCardItem().size() - 1
              && !hasHeader
              && !hasFooter) {
            viewCard.setLocation(ViewCard.LOCATION.ALL);
          } else if (Loop1 == card.getListCardItem().size() - 1 && !hasFooter) {
            viewCard.setLocation(ViewCard.LOCATION.BOT);
          } else if (Loop1 == 0 && !hasHeader) {
            viewCard.setLocation(ViewCard.LOCATION.TOP);

            if (hasStartMargin) {
              viewCard.setLocation(ViewCard.LOCATION.NONE);
              hasStartMargin = false;
            }
          } else {
            viewCard.setLocation(ViewCard.LOCATION.NONE);
          }
        }

        viewCards.add(viewCard);
      }
    }

    if (card.getFooter() != null) {
      ViewCard viewCard = new ViewCard(ViewHolderHelper.CARD_TYPE.LIST, card.getFooter());
      viewCard.setLocation(ViewCard.LOCATION.BOT);

      viewCards.add(viewCard);
    }
  }
}