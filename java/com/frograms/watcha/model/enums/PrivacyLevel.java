package com.frograms.watcha.model.enums;

import com.frograms.watcha.R;
import com.google.gson.annotations.SerializedName;

public enum PrivacyLevel {
  @SerializedName("all")ALL(0, R.string.privacy_all, R.string.icon_privacy_public),
  @SerializedName("friends")FRIENDS(1, R.string.privacy_friends, R.string.icon_friends),
  @SerializedName("only_me")ONLY_ME(2, R.string.privacy_only_me, R.string.icon_privacy_onlyme);

  private int mTypeInt, mStrId, mIconId;

  PrivacyLevel(int type, int strId, int iconId) {
    mTypeInt = type;
    mStrId = strId;
    mIconId = iconId;
  }

  public int getTypeInt() {
    return mTypeInt;
  }

  public int getStringId() {
    return mStrId;
  }

  public int getIconId() {
    return mIconId;
  }

  public static PrivacyLevel getPrivacyLevel(int typeInt) {
    PrivacyLevel[] tabs = PrivacyLevel.values();
    for (PrivacyLevel t : tabs) {
      if (t.mTypeInt == typeInt) {
        return t;
      }
    }
    return null;
  }

  public static PrivacyLevel getPrivacyLevel(String privacy) {
    return PrivacyLevel.valueOf(privacy.toUpperCase());
  }

}
