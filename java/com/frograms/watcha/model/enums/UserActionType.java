package com.frograms.watcha.model.enums;

public enum UserActionType {

  WISH(0, "wish", "wishes/%s"),
  RATE(1, "rate", "ratings/%s"),
  MEH(2, "meh", "mehs"),
  COMMENT(3, "comment", "comments/%s"),
  DECK(4, "deck", "decks");

  private int mTypeInt;
  private String mActionName, mUserContentType;

  UserActionType(int type, String actionName, String userContentType) {
    mTypeInt = type;
    mActionName = actionName;
    mUserContentType = userContentType;
  }

  public int getTypeInt() {
    return mTypeInt;
  }

  public String getActionName() {
    return mActionName;
  }

  /**
   * %s 값이 있으면, 카테고리 값으로 대체 됨.
   */
  public String getUserContentType() {
    return mUserContentType;
  }

  public static UserActionType getUserActionType(int typeInt) {
    UserActionType[] tabs = UserActionType.values();
    for (UserActionType t : tabs) {
      if (t.mTypeInt == typeInt) {
        return t;
      }
    }
    return null;
  }
}
