package com.frograms.watcha.model;

import com.google.gson.annotations.SerializedName;

public class StillCut {
  @SerializedName("original") private String mOriginal;
  @SerializedName("xlarge") private String mXLarge;
  @SerializedName("large") private String mLarge;
  @SerializedName("small") private String mSmall;

  public String getOriginal() {
    return mOriginal;
  }

  public String getXLarge() {
    return mXLarge;
  }

  public String getLarge() {
    return mLarge;
  }

  public String getSmall() {
    return mSmall;
  }
}
