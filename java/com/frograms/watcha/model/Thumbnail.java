package com.frograms.watcha.model;

import com.frograms.watcha.model.items.Event;
import com.google.gson.annotations.SerializedName;

public class Thumbnail {
  @SerializedName("type") protected ThumbnailType type;
  @SerializedName("text") protected String text;
  @SerializedName("image") protected String image;
  @SerializedName("event") protected Event event;

  public ThumbnailType getType() {
    return type;
  }

  public String getText() {
    return text;
  }

  public String getImage() {
    return image;
  }

  public Event getEvent() {
    return event;
  }

  public enum ThumbnailType {
    @SerializedName("person") PERSON,
    @SerializedName("poster") POSTER,
    @SerializedName("tag") TAG;
  }
}
