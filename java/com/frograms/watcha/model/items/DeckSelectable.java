package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Poster;
import com.google.gson.annotations.SerializedName;

public class DeckSelectable extends Item {

  @SerializedName("code") String mDeckCode;
  @SerializedName("name") String mDeckName;
  @SerializedName("image") Poster mImage;
  @SerializedName("is_added") boolean isAdded;

  public boolean isAdded() {
    return isAdded;
  }

  public void setAdded(boolean added) {
    isAdded = added;
  }

  public Poster getImage() {
    return mImage;
  }

  public String getDeckName() {
    return mDeckName;
  }

  public String getDeckCode() {
    return mDeckCode;
  }
}
