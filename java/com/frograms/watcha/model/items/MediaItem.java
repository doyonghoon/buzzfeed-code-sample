package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Thumbnail;
import com.google.gson.annotations.SerializedName;

public class MediaItem extends Item {

  @SerializedName("title") protected String title;
  @SerializedName("description") protected String description;
  @SerializedName("thumbnail") protected Thumbnail thumbnail;

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public Thumbnail getThumbnail() {
    return thumbnail;
  }
}
