package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Comment extends Item {

  @SerializedName("code") protected String code;
  @SerializedName("content_code") protected String contentCode;
  @SerializedName("text") protected String text;
  @SerializedName("likes_count") protected int likesCount;
  @SerializedName("replies_count") protected int repliesCount;
  @SerializedName("watched_at") protected Date mWatchedAt;
  @SerializedName("is_blind") protected boolean isBlind;
  @SerializedName("is_premiere_winner") protected boolean isPremiere;
  @SerializedName("is_liked") protected boolean isLiked;
  @SerializedName("is_spoiler_cautioned") protected boolean isSpoiler;
  @SerializedName("is_improper_cautioned") protected boolean isImproper;

  public String getCode() {
    return code;
  }

  public String getContentCode() {
    return contentCode;
  }

  public String getText() {
    return text;
  }

  public int getLikesCount() {
    return likesCount;
  }

  public int getRepliesCount() {
    return repliesCount;
  }

  public Date getWatchedAt() {
    return mWatchedAt;
  }

  public boolean isBlind() {
    return isBlind;
  }

  public boolean isWinner() {
    return isPremiere;
  }

  public boolean isLiked() {
    return isLiked;
  }

  public boolean isSpoiler() {
    return isSpoiler;
  }

  public boolean isImproper() {
    return isImproper;
  }

  public void setIsLiked(boolean isLiked) {
    this.isLiked = isLiked;
  }

  public void setIsSpoiler(boolean isSpoiler) {
    this.isSpoiler = isSpoiler;
  }

  public void setIsImproper(boolean isImproper) {
    this.isImproper = isImproper;
  }

  public void addLike(boolean isAdd) {
    if (isAdd) {
      likesCount++;
    } else {
      likesCount--;
    }
  }

  public void addReply(boolean isAdd) {
    if (isAdd) {
      repliesCount++;
    } else {
      repliesCount--;
    }
  }

}
