package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Reply extends Item {

  @SerializedName("code") protected String code;
  @SerializedName("user") protected UserBase user;
  @SerializedName("message") protected String message;
  @SerializedName("created_at") protected Date mCreatedAt;
  @SerializedName("likes_count") protected int likesCount;
  @SerializedName("is_liked") protected boolean isLiked;

  public String getCode() {
    return code;
  }

  public UserBase getUser() {
    return user;
  }

  public String getMessage() {
    return message;
  }

  public Date getCreatedAt() {
    return mCreatedAt;
  }

  public int getLikesCount() {
    return likesCount;
  }

  public boolean isLiked() {
    return isLiked;
  }

  public void setIsLiked(boolean isLiked) {
    this.isLiked = isLiked;
  }

  public void addLike(boolean isAdd) {
    if (isAdd) {
      likesCount++;
    } else {
      likesCount--;
    }
  }

}
