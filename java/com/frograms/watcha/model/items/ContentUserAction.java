package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class ContentUserAction extends Content {

  @SerializedName("default_user_code") String default_user_code;

  public String getDefaultUserCode() {
    return default_user_code;
  }
}
