package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Thumbnail;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class MediaCompact extends Item {

  @SerializedName("title") protected String title;
  @SerializedName("description") protected List<Description> mDescriptions;
  @SerializedName("thumbnail") protected Thumbnail thumbnail;

  public String getTitle() {
    return title;
  }

  public Thumbnail getThumbnail() {
    return thumbnail;
  }

  public List<Description> getDescriptions() {
    return mDescriptions;
  }

  public static class Description {
    public enum DescriptionType {
      @SerializedName("plain")PLAIN,
      @SerializedName("wish")WISH,
      @SerializedName("meh")MEH,
      @SerializedName("rating")RATING,
      @SerializedName("predicted_rating")PREDICTED_RATING

    }

    @SerializedName("type") protected DescriptionType mDescriptionType;
    @SerializedName("text") protected String mText;

    public String getText() {
      return mText;
    }

    public DescriptionType getDescriptionType() {
      return mDescriptionType;
    }
  }
}
