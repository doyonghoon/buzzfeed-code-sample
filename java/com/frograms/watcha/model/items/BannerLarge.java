package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class BannerLarge extends Banner {

  @SerializedName("description") protected String mDesc;
  @SerializedName("button_text") protected String mBtnText;

  public String getDesc() {
    return mDesc;
  }

  public String getButtonText() {
    return mBtnText;
  }
}