package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class UsersCompact extends Item {
  @SerializedName("friends_count") protected int friends_count;
  @SerializedName("friends") protected List<UserBase> friends;

  public int getFriendsCount() {
    return friends_count;
  }

  public List<UserBase> getFriends() {
    return friends;
  }
}
