package com.frograms.watcha.model.items;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public abstract class Item {

  public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

  @SerializedName("event") Event event;
  @SerializedName("can_remove") boolean canRemove;

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public boolean canRemove() {
    return canRemove;
  }

  @Override public String toString() {
    return gson.toJson(this);
  }
}
