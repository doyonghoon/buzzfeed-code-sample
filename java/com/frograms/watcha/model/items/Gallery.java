package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Media;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Gallery extends Item {

  @SerializedName("media") List<Media> medias;

  public List<Media> getMedias() {
    return medias;
  }
}
