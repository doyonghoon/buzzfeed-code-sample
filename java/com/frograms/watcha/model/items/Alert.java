package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Button;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Alert extends Item {

  @SerializedName("title") protected String mTitle;
  @SerializedName("description") protected String mDescription;
  @SerializedName("buttons") protected List<Button> mButtons;

  public String getTitle() {
    return mTitle;
  }

  public String getDescription() {
    return mDescription;
  }

  public List<Button> getButtons() {
    return mButtons;
  }
}
