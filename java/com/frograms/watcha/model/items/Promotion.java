package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class Promotion extends Item {

  @SerializedName("image") protected String mImage;
  @SerializedName("title") protected String mTitle;
  @SerializedName("description") protected String mDesc;
  @SerializedName("banner") protected BannerLarge mBannerLarge;

  public String getImage() {
    return mImage;
  }

  public String getTitle() {
    return mTitle;
  }

  public String getDesc() {
    return mDesc;
  }

  public BannerLarge getBanner() {
    return mBannerLarge;
  }
}
