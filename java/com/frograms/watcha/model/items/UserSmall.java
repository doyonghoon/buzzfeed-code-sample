package com.frograms.watcha.model.items;

import android.os.Parcel;
import android.support.annotation.StringRes;
import com.frograms.watcha.R;
import com.google.gson.annotations.SerializedName;

public class UserSmall extends UserBase {

  @SerializedName("button_type") protected ButtonType buttonType;

  public UserSmall(Parcel in) {
    super(in);
  }

  public void setPartner(boolean isPartner) {
    this.isPartner = String.valueOf(isPartner);
  }

  public ButtonType getButtonType() {
    return buttonType;
  }

  public enum ButtonType {
    @SerializedName("follow")FOLLOW(R.string.doing_follow, R.string.follow),
    @SerializedName("invite")INVITE(R.string.icon_follow_on, R.string.icon_follow_on);

    private final int mButtonIconOnResId;
    private final int mButtonIconOffResId;

    ButtonType(@StringRes int iconOnResId, @StringRes int iconOffResId) {
      mButtonIconOnResId = iconOnResId;
      mButtonIconOffResId = iconOffResId;
    }

    public int getIconOnId() {
      return mButtonIconOnResId;
    }

    public int getIconOffId() {
      return mButtonIconOffResId;
    }
  }
}
