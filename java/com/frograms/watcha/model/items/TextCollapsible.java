package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class TextCollapsible extends Item {
  @SerializedName("text1") String mText1;
  @SerializedName("text2") String mText2;
  @SerializedName("text3") String mText3;

  public String getText3() {
    return mText3;
  }

  public String getText2() {
    return mText2;
  }

  public String getText1() {
    return mText1;
  }
}
