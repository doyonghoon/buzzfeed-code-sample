package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

/**
 * 영평늘 카테고리 선택 화면에서 사용하는 아이템.
 */
public class EvaluateCategoryItem extends MediaItem {

  @SerializedName("id") protected String id;

  public String getId() {
    return id;
  }
}
