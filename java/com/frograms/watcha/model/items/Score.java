package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Score extends Item {

  @SerializedName("score") protected int mScore;
  @SerializedName("user") protected UserBase mUser;
  @SerializedName("banner") protected BannerLarge mBannerLarge;

  @SerializedName("updated_at") protected Date updatedAt;
  @SerializedName("category") protected String category;
  @SerializedName("description") protected String description;

  public int getScore() {
    return mScore;
  }

  public UserBase getUser() {
    return mUser;
  }

  public BannerLarge getBanner() {
    return mBannerLarge;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public String getCategory() {
    return category;
  }

  public String getDescription() {
    return description;
  }
}
