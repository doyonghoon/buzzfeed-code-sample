package com.frograms.watcha.model.items;

import android.text.TextUtils;
import com.frograms.watcha.fragment.AddContentDeckFragment;
import com.frograms.watcha.model.Cover;
import com.frograms.watcha.model.Poster;
import com.frograms.watcha.model.StillCut;
import com.frograms.watcha.utils.RatingUtils;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.Map;

public class Content extends Item {

  @SerializedName("is_added") boolean mIsAdded;

  @SerializedName("category") protected String mCategory;
  @SerializedName("code") protected String mCode;
  @SerializedName("title") protected String mTitle;
  @SerializedName("description") protected String mDescription;
  @SerializedName("photo") protected Poster mPoster;
  @SerializedName("cover") protected StillCut mStillCut;
  @SerializedName("user_actions") protected Map<String, UserAction> mUserActions;
  @SerializedName("predicted_ratings") protected Map<String, Float> mPreRatings;
  //@SerializedName("is_unreleased") boolean isUnreleased;
  @SerializedName("released_at") Date mReleasedAt;

  @SerializedName("ratings_count") int mRatingCount;
  @SerializedName("comments_count") protected int mCommentsCount;
  @SerializedName("average_rating") protected float mAverageRating;
  @SerializedName("story") String mStory;
  @SerializedName("year") int mYear;
  @SerializedName("main_genre") String mMainGenre;
  @SerializedName("nation") String mNation;
  @SerializedName("running_time_str") String mRunningTime;
  @SerializedName("title_eng") String mAkaTitle;
  @SerializedName("filmrate_ate") int mFilmRate;

  @SerializedName("vods") Vod[] mVods;
  @SerializedName("hanmadi") String mHanmadiText;

  public boolean mRequiredToShowDialog = false;

  //public boolean isUnreleased() {
  //  return isUnreleased;
  //}

  public Date getReleasedAt() {
    return mReleasedAt;
  }

  public boolean isRequiredToShowDialog() {
    return mRequiredToShowDialog;
  }

  public void setRequiredToShowDialog(boolean value) {
    mRequiredToShowDialog = value;
  }

  public String getHanmadiText() {
    return mHanmadiText;
  }

  public void setHanmadi(String text) {
    mHanmadiText = text;
  }

  /**
   * {@link AddContentDeckFragment#onClickCardItem(ItemView, Item)} 애초에 아이템이 덱에 추가 되어있을땐 유저가 수정을 할
   * 수
   * 없음.
   * true이면 유저가 수정할 수 없는 컨텐츠이고, false이면 선택할 수 있는 컨텐츠.
   */
  public boolean isAdded() {
    return mIsAdded;
  }

  public void setAdded(boolean isAdded) {
    mIsAdded = isAdded;
  }

  public String getCategory() {
    return mCategory;
  }

  public String getCode() {
    return mCode;
  }

  public String getTitle() {
    return mTitle;
  }

  public String getDescription() {
    return mDescription;
  }

  public Poster getPoster() {
    return mPoster;
  }

  public StillCut getStillCut() {
    return mStillCut;
  }

  public UserAction getUserAction(String code) {
    if (mUserActions != null && !TextUtils.isEmpty(code) && mUserActions.containsKey(code)) {
      return mUserActions.get(code);
    }
    return null;
  }

  public void setUserAction(UserAction userAction) {
    if (mUserActions != null
        && userAction != null
        && userAction.getUser() != null
        && !TextUtils.isEmpty(userAction.getUser().getCode())) {
      mUserActions.put(userAction.getUser().getCode(), userAction);
    }
  }

  public float getPreRating(String code) {
    if (mPreRatings != null) {
      if (mPreRatings.containsKey(code)) {
        return (float) RatingUtils.round(mPreRatings.get(code), 1);
      }
    }
    return 0.0f;
  }

  public int getRatingCount() {
    return mRatingCount;
  }

  public int getCommentsCount() {
    return mCommentsCount;
  }

  public float getAverageRating() {
    return (float) RatingUtils.round(mAverageRating, 1);
  }

  public String getStory() {
    return mStory;
  }

  public int getYear() {
    return mYear;
  }

  public String getMainGenre() {
    return mMainGenre;
  }

  public String getNation() {
    return mNation;
  }

  public String getRunningTime() {
    return mRunningTime;
  }

  public String getAkaTitle() {
    return mAkaTitle;
  }

  public String getFilmRate() {
    switch (mFilmRate) {
      case 12:
        return "12";
      case 15:
        return "15";
      case 18:
        return "18";
      case 19:
        return "19";
      default:
        return "ALL";
    }
  }

  public Vod[] getVods() {
    return mVods;
  }

  public static class Vod {

    @SerializedName("vendor_name") String mVendorName;
    @SerializedName("price") String mPrice;
    @SerializedName("quality") String mQuality;
    @SerializedName("event") Event mEvent;
    @SerializedName("ref_args") String mReferenceArguments;

    public String getVendorName() {
      return mVendorName;
    }

    public String getPrice() {
      return mPrice;
    }

    public String getQuality() {
      return mQuality;
    }

    public String getReferenceArguments() {
      return mReferenceArguments;
    }

    public Event getEvent() {
      return mEvent;
    }

    @Override public String toString() {
      return gson.toJson(this);
    }
  }

  /**
   * {@link AddContentDeckFragment#onClickCardItem(ItemView, Item)} 에서 서로 다른 instance 의 값을 비교해야 하는
   * 일이 있는데
   * 걍 unique한 값으로 Content 라는 모델은 구별할 수 있게 함.
   *
   * @see #getCode()
   */
  @Override public boolean equals(Object o) {
    if (o != null && o instanceof Content) {
      Content tmp = (Content) o;
      return this.getCode().equals(tmp.getCode());
    }
    return super.equals(o);
  }

  /**
   * {@link AddContentDeckFragment#onClickCardItem(ItemView, Item)} 에서 서로 다른 instance 의 값을 비교해야 하는
   * 일이 있는데
   * 걍 unique한 값으로 Content 라는 모델은 구별할 수 있게 함.
   *
   * @see #getCode()
   */
  @Override public int hashCode() {
    if (!TextUtils.isEmpty(this.getCode())) {
      return this.getCode().hashCode();
    }
    return super.hashCode();
  }
}
