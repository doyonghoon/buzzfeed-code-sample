package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class TextEdgedAligned extends Item {

  @SerializedName("text1") protected String text1;
  @SerializedName("text2") protected String text2;

  public String getText2() {
    return text2;
  }

  public String getText1() {
    return text1;
  }
}
