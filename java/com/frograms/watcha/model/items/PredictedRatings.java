package com.frograms.watcha.model.items;

/**
 * Created by doyonghoon on 15. 2. 3..
 */
public class PredictedRatings {

  private float mMyRating;
  private float mPartnerRating;

  public float getPartnerRating() {
    return mPartnerRating;
  }

  public void setPartnerRating(float partnerRating) {
    this.mPartnerRating = partnerRating;
  }

  public float getMyRating() {
    return mMyRating;
  }

  public void setMyRating(float myRating) {
    this.mMyRating = myRating;
  }
}
