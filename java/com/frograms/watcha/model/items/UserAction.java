package com.frograms.watcha.model.items;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class UserAction extends Item {

  @SerializedName("content_code") protected String contentCode;
  @SerializedName("content_title") protected String contentTitle;
  @SerializedName("rating") protected double rating;
  @SerializedName("wished") protected String wished;
  @SerializedName("mehed") protected String mehed;
  @SerializedName("comment") protected Comment comment;
  @SerializedName("user") protected UserBase user;
  @SerializedName("updated_at") protected Date updatedAt;

  public String getContentCode() {
    return contentCode;
  }

  public String getContentTitle() {
    return contentTitle;
  }

  public float getRating() {
    return (float) rating;
  }

  public boolean isWished() {
    return !TextUtils.isEmpty(wished) && wished.equals("true");
    //return wished;
  }

  public boolean isMehed() {
    return !TextUtils.isEmpty(mehed) && mehed.equals("true");
    //return mehed;
  }

  public Comment getComment() {
    return comment;
  }

  public void setComment(Comment comment) {
    this.comment = comment;
  }

  public UserBase getUser() {
    return user;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  /**
   * UserAction 모델이 같은지는 유저 코드로 구분함.
   *
   * @see UserBase#getCode()
   */
  @Override public boolean equals(Object o) {
    if (o != null && o instanceof UserAction) {
      UserAction objAction = (UserAction) o;
      if (objAction.getUser() != null && !TextUtils.isEmpty(objAction.getUser().getCode())) {
        String objUserCode = objAction.getUser().getCode();
        return this.getUser() != null
            && !TextUtils.isEmpty(this.getUser().getCode())
            && this.getUser().getCode().equals(objUserCode);
      }
    }
    return super.equals(o);
  }

  /**
   * UserAction 모델의 유니크 코드는 유저 모델의 유저 코드임.
   *
   * @see UserBase#getCode()
   */
  @Override public int hashCode() {
    if (this.getUser() != null && !TextUtils.isEmpty(this.getUser().getCode())) {
      return this.getUser().getCode().hashCode();
    }
    return super.hashCode();
  }
}
