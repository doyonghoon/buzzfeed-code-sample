package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Button;
import com.google.gson.annotations.SerializedName;

public class ButtonCompact extends Item {

  @SerializedName("button") protected Button mBtn;

  public Button getButton() {
    return mBtn;
  }
}
