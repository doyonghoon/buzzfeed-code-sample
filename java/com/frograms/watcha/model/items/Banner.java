package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class Banner extends Item {
  @SerializedName("title") protected String title;
  @SerializedName("image") protected String image;

  public String getTitle() {
    return title;
  }

  public String getImage() {
    return image;
  }
}
