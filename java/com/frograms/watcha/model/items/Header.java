package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

public class Header extends Item {

  @SerializedName("title") protected String mTitle;

  public String getTitle() {
    return mTitle;
  }
}
