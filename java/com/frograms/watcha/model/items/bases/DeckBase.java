package com.frograms.watcha.model.items.bases;

import com.frograms.watcha.model.Poster;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.UserBase;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.List;

public class DeckBase extends Item {

  @SerializedName("user") UserBase user;
  @SerializedName("code") private String mCode;
  @SerializedName("name") private String mName;
  @SerializedName("is_liked") private boolean isLiked;
  @SerializedName("likes_count") private int mLikesCount;
  @SerializedName("replies_count") private int mRepliesCount;
  @SerializedName("updated_at") protected Date updatedAt;
  @SerializedName("description") private String mDesc;
  @SerializedName("contents_count") private int mContentsCount;
  @SerializedName("content_codes") private List<String> mContentCodes;
  @SerializedName("contents") private List<Content> mContents;
  @SerializedName("image") private Poster image;

  public UserBase getUser() {
    return user;
  }

  public void setUser(UserBase user) {
    this.user = user;
  }

  public String getCode() {
    return mCode;
  }

  public void setCode(String code) {
    mCode = code;
  }

  public String getName() {
    return mName;
  }

  public void setName(String name) {
    this.mName = name;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date date) {
    updatedAt = date;
  }

  public boolean isLiked() {
    return isLiked;
  }

  public void setIsLiked(boolean isLiked) {
    this.isLiked = isLiked;
  }

  public void addLike(boolean isAdd) {
    if (isAdd) {
      mLikesCount++;
    } else {
      mLikesCount--;
    }
  }

  public int getRepliesCount() {
    return mRepliesCount;
  }

  public void setRepliesCount(int count) {
    mRepliesCount = count;
  }

  public void addReply(boolean isAdd) {
    if (isAdd) {
      mRepliesCount++;
    } else {
      mRepliesCount--;
    }
  }

  public int getLikesCount() {
    return mLikesCount;
  }

  public void setLikesCount(int count) {
    mLikesCount = count;
  }

  public String getDesc() {
    return mDesc;
  }

  public void setDesc(String desc) {
    this.mDesc = desc;
  }

  public int getContentCount() {
    return mContentsCount;
  }

  public void setContentCount(int count) {
    mContentsCount = count;
  }

  public List<String> getContentCodes() {
    return mContentCodes;
  }

  public void setContentCodes(List<String> codes) {
    mContentCodes = codes;
  }

  public List<Content> getContents() {
    return mContents;
  }

  public void setContents(List<Content> contents) {
    mContents = contents;
  }

  public Poster getImage() {
    return image;
  }

  public void setImage(Poster photo) {
    image = photo;
  }
}