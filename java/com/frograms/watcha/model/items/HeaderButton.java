package com.frograms.watcha.model.items;

import com.frograms.watcha.model.Button;
import com.google.gson.annotations.SerializedName;

public class HeaderButton extends Item {
  @SerializedName("title") protected String mTitle;
  @SerializedName("button") protected Button mButton;

  public Button getButton() {
    return mButton;
  }

  public String getTitle() {
    return mTitle;
  }
}
