package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

/**
 * 앱 내에서 어디로 이동하라는 지시를 담은 모델.
 *
 * 웹 링크, 앱 스키마, 알러트를 담고 있음.
 */
public class Event {
  @SerializedName("scheme") String scheme;
  @SerializedName("link") Link link;
  @SerializedName("alert") Alert alert;

  public Event(String scheme) {
    this.scheme = scheme;
  }

  public Alert getAlert() {
    return alert;
  }

  public Link getLink() {
    return link;
  }

  public String getScheme() {
    return scheme;
  }

  /**
   * 웹 링크 모델.
   *
   * 왓챠 앱 내의 웹뷰로 띄울지, 외부 (third-party앱 크롬 등) 으로 띄울지.
   */
  public static class Link {
    @SerializedName("path") public String path;
    @SerializedName("is_external") public boolean isExternal;

    public boolean isExternal() {
      return isExternal;
    }

    public String getUrl() {
      return path;
    }
  }

  public static class Alert {
    @SerializedName("message") public String message;
    public String ok;
    public String cancel;

    public String getCancel() {
      return cancel;
    }

    public String getOk() {
      return ok;
    }

    public String getMessage() {
      return message;
    }
  }
}
