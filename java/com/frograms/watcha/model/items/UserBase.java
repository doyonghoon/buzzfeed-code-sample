package com.frograms.watcha.model.items;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.frograms.watcha.model.ProfilePhoto;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.PrivacyLevel;
import com.frograms.watcha.model.response.data.ActionCount;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.Map;

public class UserBase extends Item implements Parcelable {

  @SerializedName("code") protected String code;
  @SerializedName("name") protected String name;
  @SerializedName("photo") protected ProfilePhoto photo;
  @SerializedName("cover") protected String cover;
  @SerializedName("decks_count") protected int decksCount;
  @SerializedName("is_official_account") protected String isOfficialAccount;
  @SerializedName("is_partner") protected String isPartner;
  @SerializedName("is_friend") protected String isFriend;
  @SerializedName("is_follower") protected String isFollower;
  @SerializedName("privacy_level") protected PrivacyLevel privacyLevel;

  @SerializedName("action_counts") HashMap<String, ActionCount> actionCounts;
  @SerializedName("bio") String bio;

  public UserBase(Parcel in) {
    readFromParcel(in);
  }

  public String getCode() {
    return code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PrivacyLevel getPrivacyLevel() {
    return privacyLevel;
  }

  public ProfilePhoto getPhoto() {
    return photo;
  }

  public String getCover() {
    return cover;
  }

  public int getDecksCount() {
    return decksCount;
  }

  public void addDecksCount(boolean isAdd) {
    if (isAdd) {
      decksCount++;
    } else {
      decksCount--;
    }
  }

  public boolean isOfficialAccount() {
    return !TextUtils.isEmpty(isOfficialAccount) && isOfficialAccount.equals("true");
  }

  public boolean isPartner() {
    return !TextUtils.isEmpty(isPartner) && isPartner.equals("true");
  }

  public boolean isFriend() {
    return !TextUtils.isEmpty(isFriend) && isFriend.equals("true");
  }

  public void setFriend(boolean isFriend) {
    this.isFriend = String.valueOf(isFriend);
  }

  public boolean isFollower() {
    return !TextUtils.isEmpty(isFollower) && isFollower.equals("true");
  }

  public void setFollower(boolean isFollower) {
    this.isFollower = String.valueOf(isFollower);
  }

  public Map<String, ActionCount> getActionCounts() {
    return actionCounts;
  }

  public void setActionCounts(Map<String, ActionCount> counts) {
    if (actionCounts == null) {
      actionCounts = new HashMap<>();
    }

    actionCounts.putAll(counts);
  }

  public ActionCount getActionCount(String code) {
    if (actionCounts != null) {
      if (actionCounts.containsKey(code)) {
        return actionCounts.get(code);
      }
    }
    return null;
  }

  public int getWishCounts() {
    if (actionCounts == null) return 0;

    CategoryType[] types = CategoryType.values();
    int count = 0;
    for (CategoryType type : types) {
      if (actionCounts.containsKey(type.getApiPath())) {
        count += actionCounts.get(type.getApiPath()).getWishes();
      }
    }
    return count;
  }

  public int getRatingCounts() {
    if (actionCounts == null) return 0;

    CategoryType[] types = CategoryType.values();
    int count = 0;
    for (CategoryType type : types) {
      if (actionCounts.containsKey(type.getApiPath())) {
        count += actionCounts.get(type.getApiPath()).getRatings();
      }
    }
    return count;
  }

  public int getCommentCounts() {
    if (actionCounts == null) return 0;

    CategoryType[] types = CategoryType.values();
    int count = 0;
    for (CategoryType type : types) {
      if (actionCounts.containsKey(type.getApiPath())) {
        count += actionCounts.get(type.getApiPath()).getComments();
      }
    }
    return count;
  }

  public String getBio() {
    return bio;
  }

  @Override public boolean equals(Object o) {
    if (o != null && o instanceof UserBase) {
      String tmpCode = ((UserBase) o).getCode();
      return tmpCode.equals(this.getCode());
    }
    return super.equals(o);
  }

  @Override public int hashCode() {
    return getCode().hashCode();
  }

  @Override public int describeContents() {
    return 0;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(code);
    dest.writeString(name);
    dest.writeParcelable(photo, flags);
    dest.writeString(cover);
    dest.writeInt(decksCount);
    dest.writeString(isOfficialAccount + "");
    dest.writeString(isPartner + "");
    dest.writeString(isFriend + "");
    dest.writeString(isFollower + "");
    dest.writeString(bio);

    if (privacyLevel == null) privacyLevel = PrivacyLevel.ALL;
    dest.writeInt(privacyLevel.getTypeInt());

    if (actionCounts != null) {
      dest.writeInt(actionCounts.size());
      for (Map.Entry<String, ActionCount> entry : actionCounts.entrySet()) {
        dest.writeString(entry.getKey());
        dest.writeParcelable(entry.getValue(), flags);
      }
    } else {
      dest.writeInt(0);
    }
  }

  protected void readFromParcel(Parcel in) {
    code = in.readString();
    name = in.readString();
    photo = in.readParcelable(ProfilePhoto.class.getClassLoader());
    cover = in.readString();
    decksCount = in.readInt();
    isOfficialAccount = in.readString();
    isPartner = in.readString();
    isFriend = in.readString();
    isFollower = in.readString();
    bio = in.readString();
    privacyLevel = PrivacyLevel.getPrivacyLevel(in.readInt());

    int size = in.readInt();
    if (size > 0) {
      actionCounts = new HashMap<>();
      for (int Loop1 = 0; Loop1 < size; Loop1++) {
        actionCounts.put(in.readString(),
            (ActionCount) in.readParcelable(ActionCount.class.getClassLoader()));
      }
    }
  }

  public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
    public UserBase createFromParcel(Parcel in) {
      return new UserBase(in);
    }

    public UserBase[] newArray(int size) {
      return new UserBase[size];
    }
  };
}
