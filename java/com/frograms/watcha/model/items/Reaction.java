package com.frograms.watcha.model.items;

import com.frograms.watcha.application.WatchaApp;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.Map;

public class Reaction extends Item {

  @SerializedName("category") String mCategory;
  //@SerializedName("is_unreleased") boolean mIsUnreleased;
  @SerializedName("released_at") Date mReleasedAt;
  @SerializedName("user_actions") protected Map<String, UserAction> mUserActions;

  public String getCategory() {
    return mCategory;
  }

  //public boolean isUnreleased() {
  //  return mIsUnreleased;
  //}

  public Date getReleasedAt() {
    return mReleasedAt;
  }

  public UserAction getUserAction() {
    if (mUserActions != null) {
      if (mUserActions.containsKey(WatchaApp.getUser().getCode())) {
        return mUserActions.get(WatchaApp.getUser().getCode());
      }
    }
    return null;
  }
}
