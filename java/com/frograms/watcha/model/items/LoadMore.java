package com.frograms.watcha.model.items;

public class LoadMore extends Item {

  private boolean mIsVisible;

  public LoadMore() {
    mIsVisible = true;
  }

  public boolean isVisible() {
    return mIsVisible;
  }

  public void setIsVisible(boolean isVisible) {
    mIsVisible = isVisible;
  }
}
