package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ContentLarge extends Content {

  @SerializedName("friends") protected List<UserBase> mFriends;
  @SerializedName("friends_count") protected int mFriendsCount;
  @SerializedName("label") protected String mLabel;

  public String getLabel() {
    return mLabel;
  }

  public List<UserBase> getFriends() {
    return mFriends;
  }

  public int getFriendsCount() {
    return mFriendsCount;
  }
}
