package com.frograms.watcha.model.items;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by juyupsung on 2014. 10. 10..
 */
public class ChartLarge extends Item {

  @SerializedName("distributions") public Distributions distributions;
  @SerializedName("average_rating") private double averageRating;
  @SerializedName("ratings_count") private int ratingsCount;

  private int[] ratings;
  private String mostRating;
  private String ratingCount;

  private class Distributions {
    @SerializedName("0.5") private int r1;
    @SerializedName("1.0") private int r2;
    @SerializedName("1.5") private int r3;
    @SerializedName("2.0") private int r4;
    @SerializedName("2.5") private int r5;
    @SerializedName("3.0") private int r6;
    @SerializedName("3.5") private int r7;
    @SerializedName("4.0") private int r8;
    @SerializedName("4.5") private int r9;
    @SerializedName("5.0") private int r10;
  }

  public int[] getDistributions() {
    if (ratings == null) {
      if (distributions != null) {
        ratings = new int[] {
            distributions.r1, distributions.r2, distributions.r3, distributions.r4,
            distributions.r5, distributions.r6, distributions.r7, distributions.r8,
            distributions.r9, distributions.r10
        };
      } else {
        ratings = new int[] { 0 * 10 };
      }
    }
    return ratings;
  }

  public String getMostRating() {
    if (mostRating != null) return mostRating;
    if (ratings == null) {
      ratings = getDistributions();
    }
    int max = 0;
    if (ratings != null) {
      for (int Loop1 = 0; Loop1 < ratings.length; Loop1 += 1) {
        if (ratings[Loop1] >= max) {
          max = ratings[Loop1];
          mostRating = Float.valueOf((float) (Loop1 + 1) / 2).toString();
        }
      }
      if (!TextUtils.isEmpty(mostRating) && mostRating.charAt(mostRating.length() - 1) == '0') {
        return mostRating.substring(0, 1);
      }
      return mostRating;
    }
    return null;
  }

  public float getAverageRating() {
    return ((int) (averageRating * 10)) / 10.0f;
  }

  public String getRatingCount() {
    if (ratingCount == null) {
      ratingCount = String.valueOf(ratingsCount);
      for (int Loop1 = 0; Loop1 < ratingCount.length(); Loop1 += 3) {
        if (ratingCount.length() > Loop1 + 3) {
          ratingCount = ratingCount.substring(0, ratingCount.length() - (Loop1 + 3))
              + ","
              + ratingCount.substring(ratingCount.length() - (Loop1 + 3));
          Loop1 += 1;
        }
      }
    }

    return ratingCount;
  }
}