package com.frograms.watcha.model.items;

import com.google.gson.annotations.SerializedName;

/**
 * Created by doyonghoon on 15. 4. 28..
 */
public class GeneralShareData {

  @SerializedName("title") String title;
  @SerializedName("data") Data data;
  @SerializedName("twitter") String twitterMessage;
  @SerializedName("facebook") String facebookMessage;
  @SerializedName("kakaotalk") String kakaoTalkMessage;

  public String getKakaoTalkMessage() {
    return kakaoTalkMessage;
  }

  public String getFacebookMessage() {
    return facebookMessage;
  }

  public String getTwitterMessage() {
    return twitterMessage;
  }

  public Data getData() {
    return data;
  }

  public String getTitle() {
    return title;
  }

  public static class Data {
    String message;

    public String getMessage() {
      return message;
    }
  }
}
