package com.frograms.watcha.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * 회원가입할때 사용하는 간단한 페북 유저 데이터 집합.
 */
public class SimpleFacebookUser implements Parcelable {

  private String mToken, mEmail, mName, mProfileUrl, mCoverUrl;

  public SimpleFacebookUser(String token, String email, String name, String profileUrl,
      String coverUrl) {
    mToken = token;
    mEmail = email;
    mName = name;
    mProfileUrl = profileUrl;
    mCoverUrl = coverUrl;
  }

  public SimpleFacebookUser(Parcel in) {
    mToken = in.readString();
    mEmail = in.readString();
    mName = in.readString();
    mProfileUrl = in.readString();
    mCoverUrl = in.readString();
  }

  public String getToken() {
    return mToken;
  }

  public String getEmail() {
    return mEmail;
  }

  public String getName() {
    return mName;
  }

  public String getProfileUrl() {
    return mProfileUrl;
  }

  public String getCoverUrl() {
    return mCoverUrl;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(mToken);
    dest.writeString(mEmail);
    dest.writeString(mName);
    dest.writeString(mProfileUrl);
    dest.writeString(mCoverUrl);
  }

  public static final Parcelable.Creator<SimpleFacebookUser> CREATOR =
      new Parcelable.Creator<SimpleFacebookUser>() {
        public SimpleFacebookUser createFromParcel(Parcel in) {
          return new SimpleFacebookUser(in);
        }

        @Override public SimpleFacebookUser[] newArray(int size) {
          return new SimpleFacebookUser[size];
        }
      };

  public static class Builder {

    private String mToken, mEmail, mName, mProfileUrl, mCoverUrl;

    public Builder(@NonNull String token) {
      this.mToken = token;
    }

    public Builder setEmail(@NonNull String email) {
      this.mEmail = email;
      return this;
    }

    public Builder setName(@NonNull String name) {
      this.mName = name;
      return this;
    }

    public Builder setProfileUrl(@NonNull String profileUrl) {
      this.mProfileUrl = profileUrl;
      return this;
    }

    public Builder setCoverUrl(@Nullable String coverUrl) {
      mCoverUrl = coverUrl;
      return this;
    }

    public SimpleFacebookUser build() {
      return new SimpleFacebookUser(mToken, mEmail, mName, mProfileUrl, mCoverUrl);
    }
  }
}
