package com.frograms.watcha.models;

import com.frograms.watcha.model.items.Item;
import com.google.gson.annotations.SerializedName;

/**
 * 페이스북 그래프 api 모델을 그대로 옮겨놓음.
 */
public class FacebookProfile {
  @SerializedName("id") String id;
  @SerializedName("email") String email;
  @SerializedName("name") String name;
  @SerializedName("link") String link;
  @SerializedName("cover") Cover cover;
  @SerializedName("picture") Picture picture;

  public String getEmail() {
    return email;
  }

  public String getPictureUrl() {
    return picture != null && picture.getData() != null ? picture.getData().getUrl() : null;
  }

  public String getCoverUrl() {
    return cover != null ? cover.getSource() : null;
  }

  public String getName() {
    return name;
  }

  public String getId() {
    return id;
  }

  public static class Cover {

    @SerializedName("id") String id;
    @SerializedName("offset_y") int offsetY;
    @SerializedName("source") String source;

    public String getId() {
      return id;
    }

    public int getOffsetY() {
      return offsetY;
    }

    public String getSource() {
      return source;
    }
  }

  public static class Picture {

    @SerializedName("data") Data data;

    public Data getData() {
      return data;
    }

    public static class Data {

      @SerializedName("url") String url;
      @SerializedName("is_silhouette") boolean isSilhouette;

      public String getUrl() {
        return url;
      }

      public boolean isSilhouette() {
        return isSilhouette;
      }
    }
  }

  @Override public String toString() {
    return Item.gson.toJson(this);
  }
}
