package com.frograms.watcha.models;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class ShareApp {

  private int mIconId;
  private Drawable mIconDrawable, mLogoDrawable;
  private String mName;
  private String mPackageName;
  private int mPriority;

  public ShareApp(int defaultPriority) {
    setOrderPriority(defaultPriority);
  }

  public void setOrderPriority(int priority) {
    mPriority = priority;
  }

  public int getOrderPriority() {
    return mPriority;
  }

  public void setIconId(int iconId) {
    mIconId = iconId;
  }

  public void setIconDrawable(Drawable d) {
    mIconDrawable = d;
  }

  public void setLogoDrawable(Drawable d) {
    mLogoDrawable = d;
  }

  public void setName(String name) {
    mName = name;
  }

  public void setPackageName(String packageName) {
    mPackageName = packageName;
  }

  public String getName() {
    return mName;
  }

  public String getPackageName() {
    return mPackageName;
  }

  public int getIconId() {
    return mIconId;
  }

  public Drawable getIconDrawable() {
    return mIconDrawable;
  }

  public Drawable getLogoDrawable() {
    return mLogoDrawable;
  }

  public Intent getShareIntent(String text) {
    Intent share = new Intent(android.content.Intent.ACTION_SEND);
    share.setType("text/plain");
    share.putExtra(Intent.EXTRA_TEXT, text);
    share.setPackage(mPackageName);
    return share;
  }
}
