package com.frograms.watcha.models;

public class Contact {

  private String name, phone;

  public Contact(String name, String phone) {
    this.name = name;
    this.phone = phone;
  }

  public String getName() {
    return name;
  }

  public String getPhone() {
    return phone;
  }
}