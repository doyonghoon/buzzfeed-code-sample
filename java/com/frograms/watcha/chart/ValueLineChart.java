/**
 * Copyright (C) 2014 Paul Cech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.frograms.watcha.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import com.frograms.watcha.R;
import com.frograms.watcha.chart.model.Point2D;
import com.frograms.watcha.chart.model.ValueLinePoint;
import com.frograms.watcha.chart.model.ValueLineSeries;
import com.frograms.watcha.helpers.FontHelper;

/**
 * A LineChart which displays various line series with one value and the remaining information is
 * calculated dynamically. It is possible to draw normal and cubic lines.
 */
public class ValueLineChart extends BaseChart {

  /**
   * Simple constructor to use when creating a view from code.
   *
   * @param context The Context the view is running in, through which it can
   * access the current theme, resources, etc.
   */
  public ValueLineChart(Context context) {
    super(context);

    mUseCubic = DEF_USE_CUBIC;
    mUseOverlapFill = DEF_USE_OVERLAP_FILL;
    mLineStroke = Utils.dpToPx(DEF_LINE_STROKE);
    mFirstMultiplier = DEF_FIRST_MULTIPLIER;
    mSecondMultiplier = 1.0f - mFirstMultiplier;
    mXAxisStroke = Utils.dpToPx(DEF_X_AXIS_STROKE);
    mUseDynamicScaling = DEF_USE_DYNAMIC_SCALING;
    mScalingFactor = DEF_SCALING_FACTOR;

    initializeGraph();
  }

  /**
   * Constructor that is called when inflating a view from XML. This is called
   * when a view is being constructed from an XML file, supplying attributes
   * that were specified in the XML file. This version uses a default style of
   * 0, so the only attribute values applied are those in the Context's Theme
   * and the given AttributeSet.
   * <p/>
   * <p/>
   * The method onFinishInflate() will be called after ALL children have been
   * added.
   *
   * @param context The Context the view is running in, through which it can
   * access the current theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @see #View(android.content.Context, android.util.AttributeSet, int)
   */
  public ValueLineChart(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a =
        context.getTheme().obtainStyledAttributes(attrs, R.styleable.ValueLineChart, 0, 0);

    try {

      mUseCubic = a.getBoolean(R.styleable.ValueLineChart_egUseCubic, DEF_USE_CUBIC);
      mUseOverlapFill =
          a.getBoolean(R.styleable.ValueLineChart_egUseOverlapFill, DEF_USE_OVERLAP_FILL);
      mLineStroke =
          a.getDimension(R.styleable.ValueLineChart_egLineStroke, Utils.dpToPx(DEF_LINE_STROKE));
      mFirstMultiplier =
          a.getFloat(R.styleable.ValueLineChart_egCurveSmoothness, DEF_FIRST_MULTIPLIER);
      mSecondMultiplier = 1.0f - mFirstMultiplier;
      mXAxisStroke =
          a.getDimension(R.styleable.ValueLineChart_egXAxisStroke, Utils.dpToPx(DEF_X_AXIS_STROKE));
      mUseDynamicScaling =
          a.getBoolean(R.styleable.ValueLineChart_egUseDynamicScaling, DEF_USE_DYNAMIC_SCALING);
      mScalingFactor = a.getFloat(R.styleable.ValueLineChart_egScalingFactor, DEF_SCALING_FACTOR);
    } finally {
      // release the TypedArray so that it can be reused.
      a.recycle();
    }

    initializeGraph();
  }

  /**
   * Adds a new series to the graph.
   *
   * @param _Series The series which should be added.
   */
  public void addSeries(ValueLineSeries _Series) {
    mSeries = _Series;
    onDataChanged();
  }

  /**
   * Resets and clears the data object.
   */
  @Override public void clearChart() {
    mSeries = new ValueLineSeries();
  }

  /**
   * Checks if the graph is a cubic graph.
   *
   * @return True if it's a cubic graph.
   */
  public boolean isUseCubic() {
    return mUseCubic;
  }

  /**
   * Sets the option if the graph should use a cubic spline interpolation or not.
   *
   * @param _useCubic True if the graph should use cubic spline interpolation.
   */
  public void setUseCubic(boolean _useCubic) {
    mUseCubic = _useCubic;
    onDataChanged();
  }

  /**
   * Checks if the graph uses an overlap fill. An overlap fill occurs whether the user set it
   * explicitly
   * through the attributes or if only one data set is present.
   *
   * @return True if overlap fill is activated.
   */
  public boolean isUseOverlapFill() {
    return mUseOverlapFill;
  }

  /**
   * Sets the overlap fill attribute.
   *
   * @param _useOverlapFill True if an overlap fill should be used.
   */
  public void setUseOverlapFill(boolean _useOverlapFill) {
    mUseOverlapFill = _useOverlapFill;
    onDataChanged();
  }

  /**
   * Returns the size of the line stroke for every series.
   *
   * @return Line stroke in px.
   */
  public float getLineStroke() {
    return mLineStroke;
  }

  /**
   * Sets the line stroke for every series.
   *
   * @param _lineStroke Line stroke as a dp value.
   */
  public void setLineStroke(float _lineStroke) {
    mLineStroke = Utils.dpToPx(_lineStroke);
    invalidateGlobal();
  }

  /**
   * Returns the stroke size of the X-axis.
   *
   * @return Stroke size in px.
   */
  public float getXAxisStroke() {
    return mXAxisStroke;
  }

  /**
   * Sets the stroke size of the X-axis.
   *
   * @param _XAxisStroke Stroke size in dp.
   */
  public void setXAxisStroke(float _XAxisStroke) {
    mXAxisStroke = Utils.dpToPx(_XAxisStroke);
    invalidateGraphOverlay();
  }

  public boolean isUseDynamicScaling() {
    return mUseDynamicScaling;
  }

  public void setUseDynamicScaling(boolean _useDynamicScaling) {
    mUseDynamicScaling = _useDynamicScaling;
    onDataChanged();
  }

  public float getScalingFactor() {
    return mScalingFactor;
  }

  public void setScalingFactor(float _scalingFactor) {
    mScalingFactor = _scalingFactor;
    onDataChanged();
  }

  /**
   * Implement this to do your drawing.
   *
   * @param canvas the canvas on which the background will be drawn
   */
  @Override protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
  }

  @Override protected void onLayout(boolean changed, int l, int t, int r, int b) {

  }

  /**
   * This is called during layout when the size of this view has changed. If
   * you were just added to the view hierarchy, you're called with the old
   * values of 0.
   *
   * @param w Current width of this view.
   * @param h Current height of this view.
   * @param oldw Old width of this view.
   * @param oldh Old height of this view.
   */
  @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);

    onDataChanged();
  }

  /**
   * This is the main entry point after the graph has been inflated. Used to initialize the graph
   * and its corresponding members.
   */
  @Override protected void initializeGraph() {
    super.initializeGraph();

    mGraphOverlay.decelerate();

    mSeries = new ValueLineSeries();
    mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mLinePaint.setStrokeWidth(mLineStroke);

    mLegendOnLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mLegendOnLinePaint.setColor(mLegendColor);
    mLegendOnLinePaint.setTextSize(mLegendTextSize);
    mLegendOnLinePaint.setStrokeWidth(2);
    mLegendOnLinePaint.setStyle(Paint.Style.FILL);
  }

  /**
   * Should be called after new data is inserted. Will be automatically called, when the view
   * dimensions
   * changed.
   *
   * Calculates various offsets and positions for different overlay features based on the graph
   * settings.
   * After the calculation the Path is generated as a normal path or cubic path (Based on
   * 'egUseCubic' attribute).
   */
  @Override protected void onDataChanged() {
    if (mSeries != null) {
      int usableGraphHeight = (int) (mGraphHeight - Utils.dpToPx(1.f));
      float minValue = Float.MAX_VALUE;
      mNegativeValue = 0.f;
      mNegativeOffset = 0.f;
      mHasNegativeValues = false;

      // calculate the maximum and minimum value present in data
      for (ValueLinePoint point : mSeries.getSeries()) {

        if (point.getValue() > maxValue) maxValue = point.getValue();

        if (point.getValue() < mNegativeValue) mNegativeValue = point.getValue();

        if (point.getValue() < minValue) minValue = point.getValue();
      }

      if (!mUseDynamicScaling) {
        minValue = 0;
      } else {
        minValue *= mScalingFactor;
      }

      // check if values below zero were found
      if (mNegativeValue < 0) {
        mHasNegativeValues = true;
        maxValue += (mNegativeValue * -1);
        minValue = 0;
      }

      float heightMultiplier = 0.9f * (usableGraphHeight / (maxValue - minValue));

      // calculate the offset
      if (mHasNegativeValues) {
        mNegativeOffset = (mNegativeValue * -1) * heightMultiplier;
      }

      int seriesPointCount = mSeries.getSeries().size();

      float widthOffset = (float) mGraphWidth / (float) seriesPointCount;
      widthOffset += widthOffset / seriesPointCount;
      float currentOffset = 0;
      mSeries.setWidthOffset(widthOffset);

      // used to store first point and set it later as ending point, if a graph fill is selected
      float firstX = currentOffset;
      float firstY = usableGraphHeight - ((mSeries.getSeries().get(0).getValue() - minValue)
          * heightMultiplier);

      Path path = new Path();
      path.moveTo(firstX, firstY);
      mSeries.getSeries().get(0).setCoordinates(new Point2D(firstX, firstY));

      // If a cubic curve should be drawn then calculate cubic path
      // If not then just draw basic lines
      if (mUseCubic) {
        Point2D P1 = new Point2D();
        Point2D P2 = new Point2D();
        Point2D P3 = new Point2D();

        for (int i = 0; i < seriesPointCount - 1; i++) {

          int i3 = (seriesPointCount - i) < 3 ? i + 1 : i + 2;
          float offset2 = (seriesPointCount - i) < 3 ? mGraphWidth : currentOffset + widthOffset;
          float offset3 =
              (seriesPointCount - i) < 3 ? mGraphWidth : currentOffset + (2 * widthOffset);

          P1.setX(currentOffset);
          P1.setY(usableGraphHeight - ((mSeries.getSeries().get(i).getValue() - minValue)
              * heightMultiplier));

          P2.setX(offset2);
          P2.setY(usableGraphHeight - ((mSeries.getSeries().get(i + 1).getValue() - minValue)
              * heightMultiplier));
          Utils.calculatePointDiff(P1, P2, P1, mSecondMultiplier);

          P3.setX(offset3);
          P3.setY(usableGraphHeight - ((mSeries.getSeries().get(i3).getValue() - minValue)
              * heightMultiplier));
          Utils.calculatePointDiff(P2, P3, P3, mFirstMultiplier);

          currentOffset += widthOffset;

          mSeries.getSeries().get(i + 1).setCoordinates(new Point2D(P2.getX(), P2.getY()));

          ValueLinePoint point = mSeries.getSeries().get(i + 1);
          point.setCoordinates(new Point2D(currentOffset,
              usableGraphHeight - ((point.getValue() - minValue) * heightMultiplier)));

          /**
           * 수정, cubicTo 대신 lineTo로 만들고 yAdjust 적용하여 간극 조정
           */
          float yAdjust = -10;
          float diff1 = P2.getY() - P1.getY();
          float diff2 = P2.getY() - P3.getY();
          float per = Math.abs(diff1 + diff2) / (usableGraphHeight);
          if (diff1 + diff2 < 0) {
            yAdjust += per * 100;
          } else {
            yAdjust -= per * 100;
          }
          mLinePaint.setDither(true);
          mLinePaint.setStyle(Paint.Style.STROKE);
          mLinePaint.setStrokeJoin(Paint.Join.ROUND);
          mLinePaint.setStrokeCap(Paint.Cap.ROUND);
          mLinePaint.setPathEffect(new CornerPathEffect(70));
          path.lineTo(point.getCoordinates().getX(), point.getCoordinates().getY() - (yAdjust));
          //                    path.cubicTo(P1.getX(), P1.getY(), P2.getX(), P2.getY() - (yAdjust * 2), P3.getX(), P3.getY());

        }
      } else {
        boolean first = true;
        int count = 1;

        for (ValueLinePoint point : mSeries.getSeries()) {
          if (first) {
            first = false;
            continue;
          }
          currentOffset += widthOffset;
          if (count == seriesPointCount - 1) {
            // if the last offset is smaller than the width, then the offset should be as long as the graph
            // to prevent a graph drop
            if (currentOffset < mGraphWidth) {
              currentOffset = mGraphWidth;
            }
          }

          mLinePaint.setDither(true);
          mLinePaint.setStyle(Paint.Style.STROKE);
          mLinePaint.setStrokeJoin(Paint.Join.ROUND);
          mLinePaint.setStrokeCap(Paint.Cap.ROUND);
          mLinePaint.setPathEffect(new CornerPathEffect(20));
          point.setCoordinates(new Point2D(currentOffset,
              usableGraphHeight - ((point.getValue() - minValue) * heightMultiplier)));
          path.lineTo(point.getCoordinates().getX(), point.getCoordinates().getY());
          count++;
        }
      }
      path.lineTo(mGraphWidth, usableGraphHeight);
      path.lineTo(0, usableGraphHeight);
      path.lineTo(firstX, firstY);

      mSeries.setPath(path);
    }

    super.onDataChanged();
    invalidateGlobal();
  }

  /**
   * Returns ALL series which are currently inserted.
   *
   * @return Inserted series.
   */
  public ValueLineSeries getDataSeries() {
    return mSeries;
  }

  // ---------------------------------------------------------------------------------------------
  //                          Override methods from view layers
  // -----------------------3----------------------------------------------------------------------

  @Override protected void onGraphDraw(Canvas _Canvas) {
    super.onGraphDraw(_Canvas);
    mLinePaint.setStyle(Paint.Style.FILL);

    _Canvas.concat(mScale);
    if (mHasNegativeValues) {
      _Canvas.translate(0, -mNegativeOffset);
    }
    // drawing of lines
    mLinePaint.setColor(mSeries.getColor());
    _Canvas.drawPath(mSeries.getPath(), mLinePaint);

    _Canvas.restore();
  }

  @Override protected void onGraphOverlayDraw(Canvas _Canvas) {
    super.onGraphOverlayDraw(_Canvas);

    // draw legend on line
    for (ValueLinePoint point : mSeries.getSeries()) {
      if (point.getLegendLabel() == null) continue;
      if (maxValue == point.getValue()) {
        mLegendOnLinePaint.setTypeface(FontHelper.FontType.ROBOTO_BOLD.getTypeface());
        mLegendOnLinePaint.setTextSize(mLegendHighlightTextSize);
      } else {
        mLegendOnLinePaint.setTypeface(FontHelper.FontType.ROBOTO_LIGHT.getTypeface());
        mLegendOnLinePaint.setTextSize(mLegendTextSize);
      }

      float xAdjust = 0;
      float yAdjust = Utils.dpToPx(5);
      if (point.getLegendLabel() != null && point.getLegendLabel().length() == 1) {
        xAdjust = Utils.dpToPx(3);
      } else {
        xAdjust = Utils.dpToPx(6);
      }

      _Canvas.drawText(point.getLegendLabel(), point.getCoordinates().getX() - xAdjust,
          point.getCoordinates().getY() - yAdjust, mLegendOnLinePaint);
    }
  }

  //##############################################################################################
  // Variables
  //##############################################################################################
  float maxValue = 0.f;

  private static final String LOG_TAG = ValueLineChart.class.getSimpleName();

  public static final boolean DEF_USE_CUBIC = false;
  public static final boolean DEF_USE_OVERLAP_FILL = false;
  public static final float DEF_LINE_STROKE = 5f;
  public static final float DEF_FIRST_MULTIPLIER = 0.33f;
  // will be interpreted as sp value
  public static final float DEF_X_AXIS_STROKE = 2f;
  // dimension value
  public static final boolean DEF_USE_DYNAMIC_SCALING = false;
  public static final float DEF_SCALING_FACTOR = 0.96f;

  private Paint mLinePaint;
  private Paint mLegendOnLinePaint;

  private ValueLineSeries mSeries;

  private boolean mHasNegativeValues = false;
  private float mNegativeValue = 0.f;
  private float mNegativeOffset = 0.f;

  private float mFirstMultiplier;
  private float mSecondMultiplier;

  /**
   * Indicates to fill the bottom area of a series with its given color.
   */
  private boolean mUseOverlapFill;
  private boolean mUseCubic;
  private float mLineStroke;
  private float mXAxisStroke;
  /**
   * Enabling this when only positive and big values are present and only have little fluctuations,
   * a y-axis scaling takes place to see a better difference between the values.
   */
  private boolean mUseDynamicScaling;
  /**
   * The factor for the dynamic scaling, which determines how many percent of the minimum value
   * should be subtracted to achieve the scaling.
   */
  private float mScalingFactor;

  protected Matrix mScale = new Matrix();
}
