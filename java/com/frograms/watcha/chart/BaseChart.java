/**
 * Copyright (C) 2014 Paul Cech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.frograms.watcha.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.frograms.watcha.R;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * This is the main chart class and should be inherited by every graph. This class provides some
 * general
 * methods and variables, which are needed and used by every type of chart.
 */
public abstract class BaseChart extends ViewGroup {

  /**
   * Simple constructor to use when creating a view from code.
   *
   * @param context The Context the view is running in, through which it can
   * access the current theme, resources, etc.
   */
  protected BaseChart(Context context) {
    super(context);

    mLegendTextSize = Utils.dpToPx(DEF_LEGEND_TEXT_SIZE);
    mLegendHighlightTextSize = Utils.dpToPx(DEF_LEGEND_HIGHLIGHT_TEXT_SIZE);
    mLegendColor = DEF_LEGEND_COLOR;
    mShowDecimal = DEF_SHOW_DECIMAL;
  }

  /**
   * Constructor that is called when inflating a view from XML. This is called
   * when a view is being constructed from an XML file, supplying attributes
   * that were specified in the XML file. This version uses a default style of
   * 0, so the only attribute values applied are those in the Context's Theme
   * and the given AttributeSet.
   * <p/>
   * <p/>
   * The method onFinishInflate() will be called after ALL children have been
   * added.
   *
   * @param context The Context the view is running in, through which it can
   * access the current theme, resources, etc.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @see #View(android.content.Context, android.util.AttributeSet, int)
   */
  public BaseChart(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BaseChart, 0, 0);

    try {

      mLegendTextSize = a.getDimension(R.styleable.BaseChart_egLegendTextSize,
          Utils.dpToPx(DEF_LEGEND_TEXT_SIZE));
      mLegendHighlightTextSize = a.getDimension(R.styleable.BaseChart_egLegendTextSize,
          Utils.dpToPx(DEF_LEGEND_HIGHLIGHT_TEXT_SIZE));
      mShowDecimal = a.getBoolean(R.styleable.BaseChart_egShowDecimal, DEF_SHOW_DECIMAL);
      mLegendColor = a.getColor(R.styleable.BaseChart_egLegendColor, DEF_LEGEND_COLOR);
    } finally {
      // release the TypedArray so that it can be reused.
      a.recycle();
    }
  }

  public boolean isShowDecimal() {
    return mShowDecimal;
  }

  public void setShowDecimal(boolean _showDecimal) {
    mShowDecimal = _showDecimal;
    invalidate();
  }

  /**
   * This is called during layout when the size of this view has changed. If
   * you were just added to the view hierarchy, you're called with the old
   * values of 0.
   *
   * @param w Current width of this view.
   * @param h Current height of this view.
   * @param oldw Old width of this view.
   * @param oldh Old height of this view.
   */
  @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);

    mWidth = w;
    mHeight = h;

    mLeftPadding = getPaddingLeft();
    mTopPadding = getPaddingTop();
    mRightPadding = getPaddingRight();
    mBottomPadding = getPaddingBottom();

    mGraph.layout(mLeftPadding, mTopPadding, w - mRightPadding, (h - mBottomPadding));
    mGraphOverlay.layout(mLeftPadding, mTopPadding, w - mRightPadding, (h - mBottomPadding));
  }

  /**
   * This is the main entry point after the graph has been inflated. Used to initialize the graph
   * and its corresponding members.
   */
  protected void initializeGraph() {
    mGraph = new Graph(getContext());
    addView(mGraph);

    mGraphOverlay = new GraphOverlay(getContext());
    addView(mGraphOverlay);
  }

  /**
   * Should be called after new data is inserted. Will be automatically called, when the view
   * dimensions
   * has changed.
   */
  protected void onDataChanged() {
    invalidate();
  }

  /**
   * Resets and clears the data object.
   */
  public abstract void clearChart();

  /**
   * Should be called when the dataset changed and the graph should update and redraw.
   * Graph implementations might overwrite this method to do more work than just call
   * onDataChanged()
   */
  public void update() {
    onDataChanged();
  }

  /**
   * Invalidates graph and legend and forces them to be redrawn.
   */
  protected final void invalidateGlobal() {
    mGraph.invalidate();
    mGraphOverlay.invalidate();
  }

  protected final void invalidateGraph() {
    mGraph.invalidate();
  }

  protected final void invalidateGraphOverlay() {
    mGraphOverlay.invalidate();
  }

  // ---------------------------------------------------------------------------------------------
  //                          Override methods from view layers
  // ---------------------------------------------------------------------------------------------

  protected void onGraphDraw(Canvas _Canvas) {

  }

  protected void onGraphOverlayDraw(Canvas _Canvas) {

  }

  protected void onGraphSizeChanged(int w, int h, int oldw, int oldh) {

  }

  protected void onGraphOverlaySizeChanged(int w, int h, int oldw, int oldh) {

  }

  //##############################################################################################
  // Graph
  //##############################################################################################
  protected class Graph extends View {
    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in, through which it can
     * access the current theme, resources, etc.
     */
    private Graph(Context context) {
      super(context);
    }

    /**
     * Enable hardware acceleration (consumes memory)
     */
    public void accelerate() {
      Utils.setLayerToHW(this);
    }

    /**
     * Disable hardware acceleration (releases memory)
     */
    public void decelerate() {
      Utils.setLayerToSW(this);
    }

    /**
     * Implement this to do your drawing.
     *
     * @param canvas the canvas on which the background will be drawn
     */
    @Override protected void onDraw(Canvas canvas) {
      super.onDraw(canvas);

      if (Build.VERSION.SDK_INT < 11) {
        mTransform.set(canvas.getMatrix());
        mTransform.preRotate(mRotation, mPivot.x, mPivot.y);
        canvas.setMatrix(mTransform);
      }

      onGraphDraw(canvas);
    }

    /**
     * This is called during layout when the size of this view has changed. If
     * you were just added to the view hierarchy, you're called with the old
     * values of 0.
     *
     * @param w Current width of this view.
     * @param h Current height of this view.
     * @param oldw Old width of this view.
     * @param oldh Old height of this view.
     */
    @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
      super.onSizeChanged(w, h, oldw, oldh);
      mGraphWidth = w;
      mGraphHeight = h;

      onGraphSizeChanged(w, h, oldw, oldh);
    }

    public void rotateTo(float pieRotation) {
      mRotation = pieRotation;
      if (Build.VERSION.SDK_INT >= 11) {
        setRotation(pieRotation);
      } else {
        this.invalidate();
      }
    }

    public void setPivot(float x, float y) {
      mPivot.x = x;
      mPivot.y = y;
      if (Build.VERSION.SDK_INT >= 11) {
        setPivotX(x);
        setPivotY(y);
      } else {
        this.invalidate();
      }
    }

    private float mRotation = 0;
    private Matrix mTransform = new Matrix();
    private PointF mPivot = new PointF();
  }

  //##############################################################################################
  // GraphOverlay
  //##############################################################################################
  protected class GraphOverlay extends View {
    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in, through which it can
     * access the current theme, resources, etc.
     */
    private GraphOverlay(Context context) {
      super(context);
    }

    /**
     * Enable hardware acceleration (consumes memory)
     */
    public void accelerate() {
      Utils.setLayerToHW(this);
    }

    /**
     * Disable hardware acceleration (releases memory)
     */
    public void decelerate() {
      Utils.setLayerToSW(this);
    }

    /**
     * Implement this to do your drawing.
     *
     * @param canvas the canvas on which the background will be drawn
     */
    @Override protected void onDraw(Canvas canvas) {
      super.onDraw(canvas);
      onGraphOverlayDraw(canvas);
    }

    /**
     * This is called during layout when the size of this view has changed. If
     * you were just added to the view hierarchy, you're called with the old
     * values of 0.
     *
     * @param w Current width of this view.
     * @param h Current height of this view.
     * @param oldw Old width of this view.
     * @param oldh Old height of this view.
     */
    @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
      super.onSizeChanged(w, h, oldw, oldh);

      onGraphOverlaySizeChanged(w, h, oldw, oldh);
    }
  }

  //##############################################################################################
  // Variables
  //##############################################################################################

  protected final static NumberFormat mFormatter = NumberFormat.getInstance(Locale.getDefault());

  public static final int DEF_LEGEND_COLOR = 0xFF666584;
  // will be interpreted as sp value
  public static final float DEF_LEGEND_TEXT_SIZE = 10.f;
  public static final float DEF_LEGEND_HIGHLIGHT_TEXT_SIZE = 12.f;
  public static final boolean DEF_SHOW_DECIMAL = false;

  protected Graph mGraph;
  protected GraphOverlay mGraphOverlay;

  protected int mHeight;
  protected int mWidth;

  protected int mGraphWidth;
  protected int mGraphHeight;

  protected float mLegendHighlightTextSize;
  protected float mLegendTextSize;
  protected int mLegendColor;

  protected int mLeftPadding;
  protected int mTopPadding;
  protected int mRightPadding;
  protected int mBottomPadding;

  protected boolean mShowDecimal;
}
