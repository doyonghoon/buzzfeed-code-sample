/**
 * Copyright (C) 2014 Paul Cech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.frograms.watcha.chart.model;

/**
 * The BaseModel is the parent model of every chart model. It basically only holds the information
 * about the legend labels of the childs value.
 */
public abstract class BaseModel {

  protected BaseModel(String _legendLabel) {
    mLegendLabel = _legendLabel;
  }

  public String getLegendLabel() {
    return mLegendLabel;
  }

  public void setLegendLabel(String _LegendLabel) {
    mLegendLabel = _LegendLabel;
  }

  /**
   * Label value
   */
  protected String mLegendLabel;
}
