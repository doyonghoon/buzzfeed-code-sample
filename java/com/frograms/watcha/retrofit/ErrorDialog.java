package com.frograms.watcha.retrofit;

import android.app.Activity;
import android.content.Context;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;

/**
 * {@link QueryType} 요청 후에 서버가 에러 메세지를 주면, 유저한테 보여주는 다이얼로그.
 * 재시도 버튼과 확인 버튼이 있음.
 * 재시도 버튼을 누르면 {@link OnInternalRetryCallback#onClickRetry()} 를 호출함.
 */
public class ErrorDialog {

  public interface OnInternalRetryCallback {
    void onClickRetry();
  }

  private Context mContext;
  private MaterialDialog.Builder mBuilder = null;

  private String mErrorMessage;
  private OnInternalRetryCallback mInternalCallback;

  public ErrorDialog(Context context, String errorMessage,
      OnInternalRetryCallback internalRetryCallback) {
    mContext = context;
    mErrorMessage = errorMessage;
    mInternalCallback = internalRetryCallback;
    createDialog();
  }

  private void createDialog() {
    mBuilder = new MaterialDialog.Builder(mContext).title(R.string.failed)
        .content(mErrorMessage)
        .positiveText(R.string.ok)
        .negativeText(R.string.retry)
        .theme(Theme.LIGHT)
        .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
            FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
        .callback(new MaterialDialog.ButtonCallback() {
          @Override public void onPositive(MaterialDialog dialog) {
            super.onPositive(dialog);
            dialog.dismiss();
          }

          @Override public void onNegative(MaterialDialog dialog) {
            super.onNegative(dialog);
            dialog.dismiss();
            if (mInternalCallback != null) {
              mInternalCallback.onClickRetry();
            }
          }
        });
  }

  public void show() {
    if (mContext != null && mContext instanceof Activity) {
      Activity act = (Activity) mContext;
      act.runOnUiThread(new Runnable() {
        @Override public void run() {
          if (mBuilder != null) mBuilder.build().show();
        }
      });
    }
  }
}
