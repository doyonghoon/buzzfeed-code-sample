package com.frograms.watcha.retrofit;

import android.content.Context;
import android.support.annotation.NonNull;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.response.BaseResponse;

/**
 * 응답으로 받은 쿠키를 항상 저장하려고 만든 클래스.
 */
public class BaseApiResponseListener<V extends BaseResponse> implements ApiResponseListener<V> {

  private final Context mContext;

  public BaseApiResponseListener(@NonNull Context context) {
    mContext = context;
  }

  @Override public void onSuccess(@NonNull QueryType queryType, @NonNull V result) {
    WatchaApp.getSessionManager().storeWatchaSessionInLocalStorage(mContext);
  }

  protected Context getContext() {
    return mContext;
  }
}
