package com.frograms.watcha.retrofit;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import com.facebook.AccessToken;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.LoginFragment;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.helpers.WGuinnessHeader;
import com.frograms.watcha.listeners.WatchaQuery;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.UserExistenceData;
import com.frograms.watcha.model.response.Autocomplete;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.model.response.data.ContentData;
import com.frograms.watcha.model.response.data.DeckData;
import com.frograms.watcha.model.response.data.DetailData;
import com.frograms.watcha.model.response.data.LikeData;
import com.frograms.watcha.model.response.data.PopularListData;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.model.response.data.RatingListData;
import com.frograms.watcha.model.response.data.SearchResultData;
import com.frograms.watcha.model.response.data.ShareMessageData;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.model.response.data.UnreadData;
import com.frograms.watcha.model.response.data.list.ActionCountsListData;
import com.frograms.watcha.model.response.data.list.FeedListData;
import com.frograms.watcha.model.response.data.list.FollowCountListData;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.model.response.data.list.UserListData;
import com.frograms.watcha.utils.FacebookBroker;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import rx.Observable;
import rx.Subscriber;

/**
 * 왓챠 서버와 http 통신할때 사용하는 url들을 관리하는 enum 클래스.
 */
public enum QueryType implements WatchaQuery {
  GENERAL_CARDS("%s") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  ACCOUNT("account") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      Response r = request.me(getApi(), params);
      if (r != null) {
        return parseBody(r, User.class);
      }
      return null;
    }
  },
  VERIFY("account/confirmation") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.postDefault(getApi());
    }
  },
  USER("users/%s") {
    @Override
    public BaseResponse<UserListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.userCards(getApi(), params);
    }
  },
  PROFILE_SETTING("account/settings") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.profileSetting(getApi(), params);
    }
  },
  SETTING("account/%s") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      TypedFile photo = null;
      if (files != null && files.length > 0) {
        for (QueryFile f : files) {
          photo = f.getTypedFile();
        }
      }
      Response r;
      if (photo == null) {
        r = request.setting(getApi(), params);
      } else {
        r = request.setting(getApi(), params, photo);
      }

      return parseBody(r, User.class);
    }
  },

  DELETE_SETTING("account/%s") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deleteSetting(getApi());
    }
  },

  UNREAD_NOTICES("notices/unread") {
    @Override
    public BaseResponse<UnreadData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.unreadNotices(getApi());
    }
  },
  UNREAD_NOTIFICATIONS("notifications/unread") {
    @Override
    public BaseResponse<UnreadData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.unreadNotices(getApi());
    }
  },
  SIGNUP_EMAIL("users") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      TypedFile userProfilePhoto = null, userCoverPhoto = null;
      if (files != null && files.length > 0) {
        for (QueryFile f : files) {
          switch (f.getKey()) {
            case QueryFile.PROFILE_PHOTO:
              userProfilePhoto = f.getTypedFile();
              break;
            case QueryFile.COVER_PHOTO:
              userCoverPhoto = f.getTypedFile();
              break;
          }
        }
      }
      Response r;
      if (userProfilePhoto == null && userCoverPhoto == null) {
        r = request.signup(getApi(), params);
      } else {
        r = request.signupWithImage(getApi(), params, userProfilePhoto, userCoverPhoto);
      }
      return parseBody(r, User.class);
    }
  },
  LOGIN_PASSWORD("session") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      Response r = request.login(getApi(), params);
      return parseBody(r, User.class);
    }
  },
  LOGIN_FACEBOOK_TOKEN("session/facebook") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      Response r = request.login(getApi(), params);
      return parseBody(r, User.class);
    }
  },
  LOGOUT("session") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.logout(getApi());
    }
  },
  FEED("feed") {
    @Override
    public BaseResponse<FeedListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.feedCards(getApi(), params);
    }
  },
  NOTIFICATIONS("notifications") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  NOTIFICATIONS_FRIENDS("activities/friends") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  DISCOVER("discover") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  FOLLOW("users/%s/followers") {
    @Override public BaseResponse<FollowCountListData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithFollowCounts(getApi(), params);
    }
  },
  FRIENDS("users/%s/friends") {
    @Override public BaseResponse<FollowCountListData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithFollowCounts(getApi(), params);
    }
  },
  MYPAGE("users/%s") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  PARTNER_CANDIDATES("partner/candidates") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  PARTNERIZE("partner") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.partnerize(getApi(), params);
    }
  },
  DELETE_PARTNER("partner") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deletePartner(getApi());
    }
  },
  USER_ACTIONED_CONTENT("users/%s/%s") {
    @Override public BaseResponse<ActionCountsListData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithCounts(getApi(), params);
    }
  },
  CONTENT_DETAIL("%s/%s") {
    @Override
    public BaseResponse<DetailData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cardsWithContentDetail(getApi(), params);
    }
  },
  ALERT_DISMISS("alerts/%s/dismiss") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.postDefault(getApi());
    }
  },
  RECOMMENDATION("%s/recommend") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  POPULAR("%s/popular") {
    @Override public BaseResponse<PopularListData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithPopular(getApi(), params);
    }
  },
  EVALUATION("%s/evaluate") {
    @Override public BaseResponse<RatingListData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithRating(getApi(), params);
    }
  },
  TUTORIAL("%s/tutorial") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  USER_ACTION("%s/%s/%s") {
    @Override
    public BaseResponse<RatingData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.ratingRequest(getApi(), params);
    }
  },
  USER_ACTION_CANCEL("%s/%s/%s") {
    @Override
    public BaseResponse<RatingData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.ratingCancelRequest(getApi());
    }
  },
  COMMENT_ACTION("comments/%s/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportRequest(getApi(), params);
    }
  },
  COMMENT_ACTION_CANCEL("comments/%s/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportCancelRequest(getApi());
    }
  },
  REPLY_WRITE("%s/%s/reply") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.postCards(getApi(), params);
    }
  },
  REPLY_DELETE("replies/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportCancelRequest(getApi());
    }
  },
  REPLY_ACTION("replies/%s/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportRequest(getApi(), params);
    }
  },
  REPLY_ACTION_CANCEL("replies/%s/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportCancelRequest(getApi());
    }
  },
  EXPLORE("explore") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  SEARCH_DECK_ADD_CONTENT("decks/%s/items/new") {
    @Override public BaseResponse<SearchResultData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithSearchResult(getApi(), params);
    }
  },
  SEARCH_QUERY("search") {
    @Override public BaseResponse<SearchResultData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithSearchResult(getApi(), params);
    }
  },
  SEARCH_USER("search/user") {
    @Override public BaseResponse<SearchResultData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithSearchResult(getApi(), params);
    }
  },
  AUTOCOMPLETE("search/autocomplete") {
    @Override
    public BaseResponse<Autocomplete> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.autocompleteTexts(getApi(), params);
    }
  },

  WISHED("users/%s/wished") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },

  NEW_COMMENT_INFO("comments/new") {
    @Override
    public BaseResponse<ContentData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.contentInfo(getApi(), params);
    }
  },

  COMMENT_INFO("comments/%s/edit") {
    @Override
    public BaseResponse<ContentData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.contentInfo(getApi(), params);
    }
  },

  READ_NOTIFICATION("notifications/%s/read") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.readNotification(getApi(), params);
    }
  },
  DECK_MY_LIST("decks") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  DECK_POPULAR_LIST("decks/popular") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  COMMENT_DETAIL("comments/%s") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  WRITE_COMMENT("comments") {
    @Override
    public BaseResponse<RatingData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.writeComment(getApi(), params);
    }
  },
  UPDATE_COMMENT("comments/%s") {
    @Override
    public BaseResponse<RatingData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.updateComment(getApi(), params);
    }
  },
  DELETE_COMMENT("comments/%s") {
    @Override
    public BaseResponse<RatingData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deleteComment(getApi());
    }
  },
  SELECT_CONTENT_IN_COMMENT("comments/new/contents") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  DECK_ITEM_REMOVE("decks/%s/items") {
    @Override
    public BaseResponse<DeckData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckItemRemove(getApi(), params);
    }
  },
  DECK_ITEM_ADD("decks/%s/items") {
    @Override
    public BaseResponse<DeckData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckItemAdd(getApi(), params);
    }
  },
  DECK_LIKEERS("decks") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  DECK_DETAIL("decks/%s") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  DECK_LIKE("decks/%s/like") {
    @Override
    public BaseResponse<LikeData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckLike(getApi(), params);
    }
  },
  DECK_LIKE_CANCEL("decks/%s/like") {
    @Override
    public BaseResponse<LikeData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckLikeCancel(getApi());
    }
  },
  DECK_SHARE_MESSAGE("decks/%s/share_url") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.getShareMessage(getApi());
    }
  },
  SHARE_MESSAGE("%s/%s/share_url") {
    @Override public BaseResponse<ShareMessageData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.getShareMessage(getApi());
    }
  },
  FIND_PASSWORD("password/retrieve") {
    @Override public BaseResponse<ShareMessageData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.findPassword(getApi(), params);
    }
  },
  SET_FRIEND("users/%s/%s") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.setToUserData(getApi());
    }
  },
  DELETE_FRIEND("users/%s/%s") {
    @Override
    public BaseResponse<User> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deleteToUserData(getApi());
    }
  },
  TAGS("tags/%s") {
    @Override
    public BaseResponse<TagItems> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.tags(getApi(), params);
    }
  },
  VOD_TAGS("tags/vod") {
    @Override
    public BaseResponse<HashMap<String, List<TagItems.Tag>>> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.vodTags(getApi(), params);
    }
  },
  // DECK 관련 API들
  DECK_INFO("decks/%s/edit") {
    @Override
    public BaseResponse<DeckData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckInfo(getApi());
    }
  },
  DECK_CREATE("decks") {
    @Override
    public BaseResponse<DeckData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckCreate(getApi(), params);
    }
  },
  DECK_UPDATE("decks/%s") {
    @Override
    public BaseResponse<DeckData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckUpdate(getApi(), params);
    }
  },
  DECK_DELETE("decks/%s") {
    @Override public BaseResponse getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.deckDelete(getApi());
    }
  },
  DECK_ACTION("decks/%s/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportRequest(getApi(), params);
    }
  },
  DECK_ACTION_CANCEL("decks/%s/%s") {
    @Override public BaseResponse<CommentReportData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.commentReportCancelRequest(getApi());
    }
  },
  UPDATE_CONTENTS_IN_DECK(
      "decks/%s/items/%s") { // 첫번째 %s : {DECK_ID or new}, 두번째 %s : {new or edit}

    @Override public BaseResponse<SearchResultData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.cardsWithSearchResult(getApi(), params);
    }
  },
  FIND_FRIEND_TWITTER("friends/twitter") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  FIND_FRIEND_FACEBOOK("friends/facebook") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  FIND_FRIEND("friends/candidates") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  EVALUATE_CATEGORIES("%s/evaluate/categories") {
    @Override
    public BaseResponse<ListData> getResponse(@NonNull WatchaApi request, @Nullable Map params,
        @Nullable QueryFile[] files) {
      return request.cards(getApi(), params);
    }
  },
  EXIST_USER("users/exist") {
    @Override public BaseResponse<UserExistenceData> getResponse(@NonNull WatchaApi request,
        @Nullable Map params, @Nullable QueryFile[] files) {
      return request.userExistenceData(getApi(), params);
    }
  };

  private String mApi, mCompleteApi;

  QueryType(String api) {
    mApi = mCompleteApi = api;
  }

  public String getApi() {
    return "api/" + mCompleteApi + ".json";
  }

  public QueryType setApi(String... paths) {
    final int count = getNumberOfValues(mApi, "%s");
    //WLog.i("paths.length: " + paths.length + ", values: " + count);
    if (count != paths.length) {
      throw new IllegalArgumentException(
          "api에 넣을 수 있는 공간 수와 넣을 값의 수가 서로 달라서 제대로 api를 만들 수가 없음.. api: "
              + getApi()
              + ", paths: "
              + Arrays.toString(paths)
              + "length: "
              + paths.length
              + ", count: "
              + count);
    }

    mCompleteApi = String.format(mApi, paths);

    return this;
  }

  private int getNumberOfValues(String text, String targetValue) {
    Pattern p = Pattern.compile(targetValue);
    Matcher m = p.matcher(text);
    int count = 0;
    while (m.find()) count++;
    return count;
  }

  /**
   * 응답 헤더를 가로채기 위해서.. 응답 형태를 {@link Response} 로 받아야 하는데 왓챠가 사용할때 {@link BaseResponse} 를 사용해야만 하는
   * 구조를 가지고 있어서..
   * 하는 수 없이 요청 후 받아온 응답을 살짝 가로채서 헤더만 쏙 뺀 후에 {@link BaseResponse} 를 만들고 {@link T} 를 파싱해서 리턴함.
   *
   * 일단은 두 군 데에 사용 하기로 함.
   * {@link LoginFragment#onClickLogin()} 비밀번호 로그인이나, {@link LoginFragment#onClickFacebookLogin()}
   * 페이스북 로그인할때 사용 함.
   *
   * @param r retrofit 의 응답.
   * @param bodyClassType {@link BaseResponse#getData()} 를 반환할 데이터 형태.
   */
  private static <T> BaseResponse<T> parseBody(Response r, Class<T> bodyClassType) {
    // 일단 헤더부터 파싱.
    interceptHeaders(r);
    String text = null;
    try {
      text = fromStream(r.getBody().in());
    } catch (IOException ignored) {
    }

    if (!TextUtils.isEmpty(text)) {
      // generic 걸린 데이터 때문에.. 손수 파싱해야함..
      JsonObject obj = WatchaApp.getInstance().getGson().fromJson(text, JsonObject.class);
      JsonObject data = obj.getAsJsonObject("data");
      T d = WatchaApp.getInstance().getGson().fromJson(data, bodyClassType);
      BaseResponse<T> result = WatchaApp.getInstance().getGson().fromJson(text, BaseResponse.class);
      result.setData(d);
      return result;
    }
    return null;
  }

  /**
   * 가로챈 헤더 중에 facebook-token 값은 반드시 저장 시켜놓아야 함.
   */
  public static void interceptHeaders(Response r) {
    List<Header> headers = r.getHeaders();
    facebookAccessTokenObservable(headers).subscribe(AccessToken::setCurrentAccessToken);
    for (Header h : headers) {
      WGuinnessHeader guinnessHeader = WGuinnessHeader.get(h.getName());
      if (guinnessHeader != null) {
        switch (guinnessHeader.getType()) {
          case LATEST_VERSION:
            PrefHelper.setString(WatchaApp.getInstance(), PrefHelper.PrefType.VERSION,
                PrefHelper.PrefKey.LATEST.name(), h.getValue());
            break;
        }
      }
    }
  }

  private static Observable<AccessToken> facebookAccessTokenObservable(final List<Header> headers) {
    return Observable.create(new Observable.OnSubscribe<AccessToken>() {
      @Override public void call(Subscriber<? super AccessToken> subscriber) {
        String accessToken = null, userId = null;
        for (Header h : headers) {
          WGuinnessHeader guinnessHeader = WGuinnessHeader.get(h.getName());
          if (guinnessHeader != null) {
            switch (guinnessHeader.getType()) {
              case FACEBOOK_ACCESS_TOKEN:
                accessToken = h.getValue();
                break;
              case FACEBOOK_USER_ID:
                userId = h.getValue();
                break;
            }
          }
        }
        if (!TextUtils.isEmpty(accessToken) && !TextUtils.isEmpty(userId)) {
          subscriber.onNext(FacebookBroker.createAccessToken(accessToken, userId));
          subscriber.onCompleted();
        }
      }
    }).filter(accessToken -> accessToken != null);
  }

  /**
   * {@link Response#getBody()} 를 파싱 함.
   * {@link QueryType#parseBody(Response, Class)} 에서만 사용 함.
   */
  private static String fromStream(InputStream in) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    StringBuilder out = new StringBuilder();
    String newLine = System.getProperty("line.separator");
    String line;
    while ((line = reader.readLine()) != null) {
      out.append(line);
      out.append(newLine);
    }
    return out.toString();
  }

  private static TypedFile getTypedFile(File f) {
    TypedFile imageFile = null;
    if (f != null) {
      Uri uri = Uri.fromFile(f);
      Uri.Builder b = uri.buildUpon();
      String url = b.build().toString();
      String extension = MimeTypeMap.getFileExtensionFromUrl(url);
      String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
      imageFile = new TypedFile(mimeType, f);
    }
    return imageFile;
  }
}