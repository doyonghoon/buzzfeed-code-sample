package com.frograms.watcha.retrofit;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import bolts.Continuation;
import bolts.Task;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.WGuinnessHeader;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.LoadingView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.http.protocol.RequestContent;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.MimeUtil;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Retrofit을 이용해서 요청할때 필요한 값들을 만들고 요청하는 클래스.
 */
public class DataProvider<R extends BaseResponse> {

  /**
   * 요청을 만든 주체(Activity).
   * ThemedContext 같은걸 사용하면 즉, Context != Activity 라면 에러가 납니다.
   */
  private Activity mActivity;

  /**
   * 응답을 콜백받을 리스너.
   */
  private List<ApiResponseListener<R>> mCallbacks;

  /**
   * 요청에 필요한 파라미터.
   */
  private Map<String, String> mParams;

  /**
   * 요청할 주소.
   */
  private List<String> mPaths;
  private QueryType mQueryType;

  /**
   * 다이얼로그 메세지.
   * 이 값이 null이 아닐때만 다이얼로그가 보입니다.
   */
  private String mDialogLoadingMessage = null;

  /**
   * 캔슬해야 될 경우 true
   */
  private boolean mShouldCancel = false;

  /**
   * RESTful Api를 요청할 수 있는 클라이언트.
   */
  private RetrofitClient mClient;

  /**
   *
   * */
  private RequestContent mContent = null;

  /**
   * 서버로 보낼 파일.
   * 이미지 파일 같은거
   */
  private List<QueryFile> mFiles = new ArrayList<>();

  private Continuation<R, R> mInterceptedContinuation = null;

  /**
   * 요청 실패할때 처리할 햄들러.
   */
  private RetrofitErrorHandler mErrorHandler = null;

  private ReferrerBuilder mReferrerBuilder = null;

  /**
   * Retrofit을 이용해서 서버로 요청할때 사용하는 클래스.
   *
   * @param activity 요청하는 Activity.
   * @param queryType 왓챠 내부에서 사용하는 api들만 모아놓은 enum 클래스.
   */
  public DataProvider(Activity activity, QueryType queryType, ReferrerBuilder referrerBuilder) {
    mActivity = activity;
    mQueryType = queryType;
    mPaths = new ArrayList<>();
    mReferrerBuilder = referrerBuilder;

    // 기본적으로 왓챠 내부적으로 정한 에러코드들을 처리할 에러 핸들러 생성.
    mErrorHandler = new RetrofitErrorHandler(activity, mQueryType);
    mErrorHandler.setInternalRetryCallback(new ErrorDialog.OnInternalRetryCallback() {
      @Override public void onClickRetry() {
        request();
      }
    });
  }

  //에러가 나도 error dialog가 안뜨게
  public DataProvider<R> disableErrorDialog(){
    mErrorHandler.disableErrorDialog();
    return this;
  }

  /**
   * 에러 다이얼로그가 노출될때 콜백되는 함수.
   *
   * @param errorCallback 받을 에러 콜백.
   */
  public DataProvider<R> setErrorCallback(RetrofitErrorHandler.OnErrorCallback errorCallback) {
    if (mErrorHandler != null) mErrorHandler.setErrorCallback(errorCallback);
    return this;
  }

  /**
   * 재시도 다이얼로그 안뜸.
   */
  public DataProvider<R> ignoreRetryDialog() {
    mErrorHandler.ignoreRetryDialog(true);
    return this;
  }

  public void shouldCancel(boolean shouldCancel) {
    mShouldCancel = shouldCancel;
  }

  /**
   * 응답을 받을 리스너.
   *
   * 응답 리스너를 복수로 등록시킬 수 있음. View, Fragment, Activity 등 모두 등록시켜놓고 응답이 올 경우에 등록시킨 쪽으로 모두 응답이 감.
   *
   * @param listener 등록시킬 응답 리스너.
   */
  public DataProvider<R> responseTo(ApiResponseListener<R> listener) {
    if (mCallbacks == null) mCallbacks = new ArrayList<>();

    if (!mCallbacks.contains(listener)) mCallbacks.add(listener);

    return this;
  }

  /**
   * 등록해놓은 리스너를 제거할때 호출 함.
   *
   * @param listener 등록했던 리스너와 같은 instance임.
   */
  public DataProvider<R> removeResponseListener(ApiResponseListener<R> listener) {
    if (mCallbacks != null && !mCallbacks.isEmpty() && mCallbacks.contains(listener)) {
      mCallbacks.remove(listener);
    }
    return this;
  }

  /**
   * 요청할때 추가할 파라미터를 Map의 형태로 받음.
   *
   * @param params key(String), value(String) 으로 받기 때문에 어떤 데이터 타입이든 그냥 스트링으로 변환하고 넣어주면 됨.
   * params를 여러번 호출한다고 중복 추가는 안되고 그냥 마지막에 넣어진 params가 적용되어짐.
   */
  public DataProvider<R> withParams(Map<String, String> params) {
    mParams = params;
    return this;
  }

  /**
   * 서버로 요청 중에 로딩 다이얼로그를 보여줌.
   *
   * @param message 보여줄 메세지.
   */
  public DataProvider<R> withDialogMessage(String message) {
    mDialogLoadingMessage = message;
    return this;
  }

  /**
   * 요청할때 추가할 파일 타입의 파라미터.
   *
   * @param file 서버로 보낼 파일. (이미지 파일같은걸 서버로 보낼때 사용함)
   */
  public DataProvider<R> addFile(int key, File file) {
    mFiles.add(new QueryFile(key, file));
    return this;
  }

  public DataProvider<R> addFile(QueryFile file) {
    mFiles.add(file);
    return this;
  }

  /**
   * 요청할때 추가할 바이너리 타입의 파라미터.
   *
   * @param content 서버로 보낼 바이너리.
   */
  public DataProvider<R> setRetrofitContent(RequestContent content) {
    mContent = content;
    return this;
  }

  /**
   * 미리 정의된 url에 정해지지 않은 값을 넣을때 사용 함.
   *
   * 예를 들면, userCode나 movieCode같은 값을 넣을때 사용 함.
   *
   * @param path runtime때 정해지는 값.
   */
  public DataProvider<R> addPath(String path) {
    mPaths.add(path);
    return this;
  }

  /**
   * 요청이 {@code responseTo()}에 등록한 리스너로 응답이 가기 전에 백그라운드 스레드로 돌릴 작업을 추가함.
   *
   * 백그라운드 스레드를 사용하기 때문에 UiThread() 에서 사용하는 함수는 사용하면 작동하지 않음.
   *
   * @param continuation 백그라운드 스레드로 작업할 코드 Continuation.
   */
  public DataProvider<R> interceptResponseCallback(Continuation<R, R> continuation) {
    mInterceptedContinuation = continuation;
    return this;
  }

  /**
   * 서버로 요청함.
   * responseTo(ApiListener listener)를 호출했으면 {@code Result} 값이 Null임. 대신 응답이 ApiListener로 감.
   */
  public R request() {
    if (mActivity == null) {
      return null;
    }

    if (mPaths != null && mPaths.size() > 0) {
      mQueryType.setApi(mPaths.toArray(new String[mPaths.size()]));
    }

    if (mActivity instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) mActivity;
      BaseFragment fragment = baseActivity.getFragment();
    }

    if (hasCallbacks()) {
      // 응답을 가로채서 백그라운드 스레드로 무언가 할 작업이 있는지 확인.
      if (mInterceptedContinuation != null) {
        Task.callInBackground(mInternalCallable)
            .continueWith(mInterceptedContinuation)
            .continueWithTask(mInternalCallableForCallback, Task.UI_THREAD_EXECUTOR);
      } else {
        Task.callInBackground(mInternalCallable)
            .continueWithTask(mInternalCallableForCallback, Task.UI_THREAD_EXECUTOR);
      }
    } else {
      return getResult(mClient =
          createClient(mActivity, WatchaApp.getRestAdapterBuilder(), mErrorHandler,
              mDialogLoadingMessage));
    }
    return null;
  }

  /**
   * 응답을 콜백해줄 리스너들이 있는지 확인함.
   */
  private boolean hasCallbacks() {
    return mCallbacks != null && !mCallbacks.isEmpty();
  }

  private R getResult(final RetrofitClient client) {
    if (client == null) {
      return null;
    }

    try {
      if (TextUtils.isEmpty(mDialogLoadingMessage)) {
        client.hideProgressDialog();
      }
      return (R) client.request(mQueryType, mParams, mFiles.toArray(new QueryFile[mFiles.size()]));
    } catch (RetrofitFailException ignored) {
    }
    return null;
  }

  private RetrofitClient createClient(Context context, RestAdapter.Builder adapterBuilder,
      retrofit.ErrorHandler handler, String loadingMessage) {
    if (adapterBuilder == null) {
      WatchaApp.initializeRetrofit();
    }

    if (adapterBuilder != null && handler != null) {
      adapterBuilder.setErrorHandler(handler);
    }

    return new RetrofitClient(context, adapterBuilder, loadingMessage, mReferrerBuilder);
  }

  /**
   * Retrofit에 요청하는 별도의 background thread.
   * 로딩 다이얼로그를 띄울지 결정하며, 요청 클라이언트를 생성하고 요청 결과를 반환 함.
   */
  private Callable<R> mInternalCallable = new Callable<R>() {
    @Override public R call() {
      return getResult(mClient =
          createClient(mActivity, WatchaApp.getRestAdapterBuilder(), mErrorHandler,
              mDialogLoadingMessage));
    }
  };

  /**
   * mInternalCallable 이 완료된 후에 응답을 콜백해줄 리스너들이 존재하는지 확인하고 응답을 콜백해 줌.
   */
  private Continuation<R, Task<Void>> mInternalCallableForCallback =
      new Continuation<R, Task<Void>>() {
        @Override public Task<Void> then(Task<R> task) {
          final boolean canResponseToListeners = hasCallbacks() && task.getResult() != null;
          try {
            for (ApiResponseListener listener : mCallbacks) {
              if (canResponseToListeners && !mShouldCancel) {
                listener.onSuccess(mQueryType, task.getResult());
              } else {
                RetrofitFailException e = null;
                if (task != null
                    && task.getError() != null
                    && task.getError() instanceof RetrofitFailException) {
                  e = (RetrofitFailException) task.getError();
                }
                // 에러가 발생하면 OnErrorCallback 이 호출 됨. listener.onFailure(mQueryType, e);
              }
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          return null;
        }
      };

  public static class RetrofitClient {
    private Context context;
    private RestAdapter.Builder builder;
    private final ReferrerBuilder mReferrerBuilder;

    // Show progress dialog
    //private MaterialDialog mProgressDialog;
    private String mProgressMsg;
    private boolean isShowProgressDialog;
    private boolean isRequestCanceled;

    protected LoadingView mLoadingView;

    //public RetrofitClient(Context context, RestAdapter.Builder restAdapterBuilder) {
    //  this.context = context;
    //  builder = restAdapterBuilder;
    //  isShowProgressDialog = false;
    //}

    public RetrofitClient(Context context, RestAdapter.Builder restAdapterBuilder,
        String progressMsg, ReferrerBuilder referrerBuilder) {
      this.context = context;
      builder = restAdapterBuilder;
      mReferrerBuilder = referrerBuilder;
      mProgressMsg = progressMsg;
      isShowProgressDialog = true;
    }

    public void hideProgressDialog() {
      isShowProgressDialog = false;
    }

    public void clear() {
      context = null;
      builder = null;
    }

    //public RestAdapter getRestAdapter() {
    //  return builder.build();
    //}

    private void showLoadingDialog() {
      isRequestCanceled = false;
      try {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
          @Override public void run() {
            //if (isShowProgressDialog) {
            //  if (getDialog() != null && !getDialog().isShowing()) getDialog().show();
            //}
            if (mLoadingView != null) {
              mLoadingView.onLoading();
            }
          }
        });
      } catch (NullPointerException e) {
        WLog.logException(e);
      }
    }

    private void hideLoadingDialog() {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override public void run() {
          //if (isShowProgressDialog && getDialog() != null && getDialog().isShowing()) {
          //  getDialog().dismiss();
          //}
          if (mLoadingView != null) {
            mLoadingView.onEnd();
          }
        }
      });
    }

    //protected MaterialDialog getDialog() {
    //  try {
    //    if (mProgressDialog == null) {
    //      mProgressDialog = new MaterialDialog.Builder(context).content(mProgressMsg)
    //          .progress(true, 0)
    //          .theme(Theme.LIGHT)
    //          .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
    //              FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
    //          .cancelListener(new MaterialDialog.OnCancelListener() {
    //            @Override public void onCancel(DialogInterface dialog) {
    //              isRequestCanceled = true;
    //            }
    //          })
    //          .build();
    //    }
    //    return mProgressDialog;
    //  } catch (NullPointerException e) {
    //    WLog.logException(e);
    //  }
    //
    //  return null;
    //}

    public RetrofitFailException getFailException(RetrofitError e) {
      RetrofitFailException ex = null;
      try {
        e.printStackTrace();
        ex = WatchaApp.getInstance()
            .getGson()
            .fromJson(getStringFromResponse(e.getResponse()), RetrofitFailException.class);
      } catch (Exception exception) {
        // 에러코드가 의도대로 나오고 있지 않을 경우....
        exception.printStackTrace();
      }
      if (ex == null) {
        ex = new RetrofitFailException(-1, null);
      }

      if (ex.getCode() == -1 && e.getResponse().getStatus() != 0) {
        ex.setCode(e.getResponse().getStatus());
      }
      return ex;
    }

    public static String getStringFromResponse(Response response) {
      try {
        TypedInput body = response.getBody();
        byte[] bodyBytes = ((TypedByteArray) body).getBytes();
        String bodyMime = body.mimeType();
        String bodyCharset = MimeUtil.parseCharset(bodyMime);
        return new String(bodyBytes, bodyCharset);
      } catch (Exception e) {
        return null;
      }
    }

    public <T> BaseResponse<T> request(QueryType queryType, Map<String, String> params,
        QueryFile[] files) throws RetrofitFailException {
      try {
        showLoadingDialog();
        WGuinnessHeader referrerHeader = new WGuinnessHeader(WGuinnessHeader.Type.REFERRER);
        if (mReferrerBuilder != null) {
          referrerHeader.setValue(mReferrerBuilder.toString());
        }
        RequestInterceptor requestInterceptor = WatchaApp.getInstance()
            .getRetrofitRequestInterceptor(referrerHeader);
        final WatchaApi r = builder.setRequestInterceptor(requestInterceptor).build().create(WatchaApi.class);
        BaseResponse<T> result = (BaseResponse<T>) queryType.getResponse(r, params, files);
        return result;
      } catch (RetrofitError e) {
        QueryType.interceptHeaders(e.getResponse());

        if (e.getResponse() != null) {
          WLog.i("ErrorCode: " + e.getResponse().getStatus());
          hideLoadingDialog();
          throw getFailException(e);
        } else {
          hideLoadingDialog();
          throw new RetrofitFailException(-1, null);
        }
      } catch (Exception ex) {
        hideLoadingDialog();
        throw new RetrofitFailException(-1, null);
      } finally {
        hideLoadingDialog();
      }
    }
  }
}
