package com.frograms.watcha.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juyupsung on 2014. 7. 22..
 */
public class RetrofitFailException extends Exception {

  @SerializedName("msg") private String msg;
  @SerializedName("code") private int code;
  @SerializedName("result") private String result;

  public RetrofitFailException() {

  }

  @Override public String toString() {
    return "code: " + code + ", result: " + result + ", message: " + msg;
  }

  public RetrofitFailException(int code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getResult() {
    return result;
  }
}