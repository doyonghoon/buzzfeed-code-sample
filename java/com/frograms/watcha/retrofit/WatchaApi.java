package com.frograms.watcha.retrofit;

import com.frograms.watcha.model.User;
import com.frograms.watcha.model.UserExistenceData;
import com.frograms.watcha.model.response.Autocomplete;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.model.response.data.ContentData;
import com.frograms.watcha.model.response.data.DeckData;
import com.frograms.watcha.model.response.data.DetailData;
import com.frograms.watcha.model.response.data.LikeData;
import com.frograms.watcha.model.response.data.PopularListData;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.model.response.data.RatingListData;
import com.frograms.watcha.model.response.data.SearchResultData;
import com.frograms.watcha.model.response.data.ShareMessageData;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.model.response.data.UnreadData;
import com.frograms.watcha.model.response.data.list.ActionCountsListData;
import com.frograms.watcha.model.response.data.list.FeedListData;
import com.frograms.watcha.model.response.data.list.FollowCountListData;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.model.response.data.list.UserListData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit.client.Response;
import retrofit.http.DELETE;
import retrofit.http.EncodedPath;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

/**
 * Created by juyupsung on 2014. 7. 23..
 */
public interface WatchaApi {

  @POST("/{api}") BaseResponse postDefault(@EncodedPath("api") String api);

  @FormUrlEncoded @POST("/{api}") BaseResponse<RatingData> ratingRequest(
      @EncodedPath("api") String api, @FieldMap Map<String, String> params);

  @DELETE("/{api}") BaseResponse<RatingData> ratingCancelRequest(@EncodedPath("api") String api);

  @FormUrlEncoded @POST("/{api}") BaseResponse<CommentReportData> commentReportRequest(
      @EncodedPath("api") String api, @FieldMap Map<String, String> params);

  @DELETE("/{api}") BaseResponse<CommentReportData> commentReportCancelRequest(
      @EncodedPath("api") String api);

  @GET("/{api}") BaseResponse<UserExistenceData> userExistenceData(@EncodedPath("api") String api,
      @QueryMap Map<String, String> params);

  // GET ListData
  @POST("/{path}") BaseResponse<ListData> postCards(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  // GET ListData
  @GET("/{path}") BaseResponse<ListData> cards(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  // GET FeedData(extends ListData)
  @GET("/{path}") BaseResponse<FeedListData> feedCards(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  // GET UserData(extends FeedData)
  @GET("/{path}") BaseResponse<UserListData> userCards(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @GET("/{path}") BaseResponse<ActionCountsListData> cardsWithCounts(
      @EncodedPath("path") String path, @QueryMap Map<String, String> params);

  @GET("/{path}") BaseResponse<SearchResultData> cardsWithSearchResult(
      @EncodedPath("path") String path, @QueryMap Map<String, String> params);

  // GET ListData
  @GET("/{path}") BaseResponse<FollowCountListData> cardsWithFollowCounts(
      @EncodedPath("path") String path, @QueryMap Map<String, String> params);

  // GET My User Data
  @GET("/{path}") Response me(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @GET("/{path}") BaseResponse<DeckData> cardsWithDeckData(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @GET("/{path}") BaseResponse<DeckData> cardsWithDeckSort(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @GET("/{path}") BaseResponse<ShareMessageData> getShareMessage(@EncodedPath("path") String path);

  @GET("/{path}") BaseResponse<DetailData> cardsWithContentDetail(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  // Get Popular Card List
  @GET("/{path}") BaseResponse<PopularListData> cardsWithPopular(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  // Get Evaluation Card List
  @GET("/{path}") BaseResponse<RatingListData> cardsWithRating(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @GET("/{api}") BaseResponse<DeckData> deckInfo(@EncodedPath("api") String api);

  @FormUrlEncoded @POST("/{api}") BaseResponse<DeckData> deckCreate(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  @FormUrlEncoded @POST("/{api}") BaseResponse<DeckData> deckItemAdd(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  @FormUrlEncoded @PUT("/{api}") BaseResponse<DeckData> deckUpdate(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  // response에  data가 비어서 오기 때문에 Data generic을 설정할 필요가 없음.
  @DELETE("/{api}") BaseResponse deckDelete(@EncodedPath("api") String api);

  @DELETE("/{api}") BaseResponse<DeckData> deckItemRemove(@EncodedPath("api") String api,
      @QueryMap Map<String, String> params);

  @FormUrlEncoded @POST("/{api}") BaseResponse<LikeData> deckLike(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  @DELETE("/{api}") BaseResponse<LikeData> deckLikeCancel(@EncodedPath("api") String api);

  @GET("/{path}") BaseResponse<ContentData> contentInfo(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @GET("/{path}") BaseResponse<RatingData> getComment(@EncodedPath("path") String api);

  @POST("/{path}") BaseResponse<RatingData> writeComment(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @PUT("/{path}") BaseResponse<RatingData> updateComment(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @DELETE("/{path}") BaseResponse<RatingData> deleteComment(@EncodedPath("path") String path);

  @POST("/{path}") BaseResponse findPassword(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @POST("/{path}") BaseResponse partnerize(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @PUT("/{path}") BaseResponse readNotification(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);

  @FormUrlEncoded @POST("/{api}") Response login(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  @DELETE("/{api}") BaseResponse<User> logout(@EncodedPath("api") String api);

  @DELETE("/{api}") BaseResponse deletePartner(@EncodedPath("api") String api);

  @Multipart @POST("/{api}") Response signupWithImage(@EncodedPath("api") String api,
      @QueryMap Map<String, String> params, @Part("photo") TypedFile photo,
      @Part("cover") TypedFile coverPhoto);

  @FormUrlEncoded @POST("/{api}") Response signup(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  @GET("/{api}") BaseResponse<UnreadData> unreadNotices(@EncodedPath("api") String api);

  @GET("/{api}") BaseResponse<TagItems> tags(@EncodedPath("api") String api,
      @QueryMap Map<String, String> params);

  @GET("/{api}") BaseResponse<HashMap<String, List<TagItems.Tag>>> vodTags(
      @EncodedPath("api") String api, @QueryMap Map<String, String> params);

  @POST("/{api}") BaseResponse setToUserData(@EncodedPath("api") String api);

  @DELETE("/{api}") BaseResponse<User> deleteToUserData(@EncodedPath("api") String api);

  @FormUrlEncoded @PUT("/{api}") BaseResponse<User> profileSetting(@EncodedPath("api") String api,
      @FieldMap Map<String, String> params);

  @POST("/{api}") Response setting(@EncodedPath("api") String api,
      @QueryMap Map<String, String> params);

  @Multipart @POST("/{api}") Response setting(@EncodedPath("api") String api,
      @QueryMap Map<String, String> params, @Part("file") TypedFile photo);

  @DELETE("/{api}") BaseResponse<User> deleteSetting(@EncodedPath("api") String api);

  @GET("/{path}") BaseResponse<Autocomplete> autocompleteTexts(@EncodedPath("path") String path,
      @QueryMap Map<String, String> params);
}

