package com.frograms.watcha.retrofit;

import android.net.Uri;
import android.support.annotation.IntDef;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import retrofit.mime.TypedFile;

/**
 * 복수의 파일을 서버에 요청할 때 필요한 클래스.
 */
public class QueryFile {

  @IntDef({ PROFILE_PHOTO, COVER_PHOTO }) @Retention(RetentionPolicy.SOURCE)
  public @interface QueryFileKey {
  }

  public static final int PROFILE_PHOTO = 0;
  public static final int COVER_PHOTO = 1;

  private int mKey;
  private File mFile;
  private TypedFile mTypedFile;

  /**
   * 서버로 보내기 전에, 어떤 파일인지 간단히 key 를 부여해서 구별함.
   *
   * @param key {@link QueryFileKey} 값을 넣으면 파일에 키가 부여됨.
   * @param file 서버로 보낼 파일.
   */
  public QueryFile(@QueryFileKey int key, File file) {
    mKey = key;
    mFile = file;
    if (getFile() != null) {
      mTypedFile = createTypedFile(getFile());
    }
  }

  public File getFile() {
    return mFile;
  }

  public TypedFile getTypedFile() {
    return mTypedFile;
  }

  public int getKey() {
    return mKey;
  }

  private TypedFile createTypedFile(File f) {
    TypedFile imageFile = null;
    if (f != null) {
      Uri uri = Uri.fromFile(f);
      Uri.Builder b = uri.buildUpon();
      String url = b.build().toString();
      String extension = MimeTypeMap.getFileExtensionFromUrl(url);
      String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
      imageFile = new TypedFile(mimeType, f);
    }
    return imageFile;
  }
}
