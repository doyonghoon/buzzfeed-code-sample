package com.frograms.watcha.retrofit;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.AccessToken;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.SimpleFacebookUser;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.models.FacebookProfile;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.WLog;
import java.util.HashMap;
import java.util.Map;
import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Retrofit을 이용해서 요청했을때 발생한 에러를 처리하는 핸들러.
 */
public class RetrofitErrorHandler implements ErrorHandler {

  public enum ErrorType {

    FORCED_UPDATE(1),
    NOT_REGISTERED_WITH_FB_MAIL(2),
    NO_PASSWORD(3),
    NO_SESSION(401);

    private int mAttributeValue;

    ErrorType(int attributeValue) {
      mAttributeValue = attributeValue;
    }

    public int getAttributeValue() {
      return mAttributeValue;
    }

    public static ErrorType getErrorType(int attributeValue) {
      for (ErrorType t : values()) {
        if (t.getAttributeValue() == attributeValue) {
          return t;
        }
      }

      return null;
    }

    public static ErrorType getErrorType(String attributeValue) {
      if (attributeValue == null) return null;
      int value;
      try {
        value = Integer.valueOf(attributeValue);
      } catch (Exception e) {
        return null;
      }

      return getErrorType(value);
    }
  }

  /**
   * Fragment, Activity, View 등에서 요청 후에 에러났을때 받을 수 있는 콜백.
   */
  public interface OnErrorCallback {
    void onErrorCallback(QueryType queryType, String errorMessage,
        @IntRange(from = 201, to = 9999) int errorCode);
  }

  private Context mContext;
  private OnErrorCallback mCallback;

  private ErrorDialog.OnInternalRetryCallback mRetryCallback;
  private QueryType mQueryType;
  private boolean mIgnoreRetryDialog = false, mHideErrorDialog = false;

  public RetrofitErrorHandler(Context context, QueryType queryType) {
    mContext = context;
    mQueryType = queryType;
  }

  public void disableErrorDialog(){ mHideErrorDialog = true; }

  public void ignoreRetryDialog(boolean ignore) {
    mIgnoreRetryDialog = ignore;
  }

  public void setErrorCallback(OnErrorCallback callback) {
    mCallback = callback;
  }

  public void setInternalRetryCallback(ErrorDialog.OnInternalRetryCallback callback) {
    mRetryCallback = callback;
  }

  @Override public Throwable handleError(RetrofitError cause) {
    Response r = cause.getResponse();
    if (r != null) {
      QueryType.interceptHeaders(r);
      String body = DataProvider.RetrofitClient.getStringFromResponse(r);
      String parsedErrorMessage = parseFailMessage(body);
      //WatchaError error = WatchaError.getError(r.getStatus());
      WLog.d("Error code : " + r.getStatus());
      WLog.d("Detail : " + r.getUrl());
      WLog.d("Detail : " + r.getReason());
      WLog.d("Header : " + r.getHeaders());
      //WLog.d("ErrorMessage : " + mContext.getString(error.getErrorMessageResourceId()));

      ErrorType errorType = ErrorType.getErrorType(parseErrorCode(body));
      if (errorType == null) errorType = ErrorType.getErrorType(r.getStatus());
      if (errorType != null) {
        MaterialDialog.Builder builder = getAlertBuilderByErrorCode(parsedErrorMessage);
        switch (errorType) {
          case FORCED_UPDATE:
            builder.positiveText(R.string.cancel)
                .negativeText(R.string.ok)
                .callback(new MaterialDialog.ButtonCallback() {
                  @Override public void onPositive(MaterialDialog dialog) {
                    super.onPositive(dialog);
                    dialog.dismiss();
                    if (mContext instanceof BaseActivity) {
                      ((BaseActivity) mContext).setResult(-2);
                      ((BaseActivity) mContext).finish();
                    }
                  }

                  @Override public void onNegative(MaterialDialog dialog) {
                    super.onNegative(dialog);
                    dialog.dismiss();
                    ActivityStarter.with(mContext, mContext.getString(R.string.market)).start();
                    if (mContext instanceof BaseActivity) {
                      ((BaseActivity) mContext).setResult(-2);
                      ((BaseActivity) mContext).finish();
                    }
                  }
                });

            showDialogByErrorCode(builder);
            break;

          case NOT_REGISTERED_WITH_FB_MAIL:
            builder.positiveText(R.string.cancel)
                .negativeText(R.string.go_register)
                .callback(new MaterialDialog.ButtonCallback() {
                  @Override public void onPositive(MaterialDialog dialog) {
                    super.onPositive(dialog);
                    dialog.dismiss();
                  }

                  @Override public void onNegative(MaterialDialog dialog) {
                    super.onNegative(dialog);
                    dialog.dismiss();

                    FacebookBroker.accessTokenObservable((BaseActivity)mContext)
                        .flatMap(new Func1<AccessToken, Observable<FacebookProfile>>() {
                          @Override public Observable<FacebookProfile> call(AccessToken accessToken) {
                            return FacebookBroker.getInstance().profileObservable((BaseActivity)mContext);
                          }
                        })
                        .subscribe(new Action1<FacebookProfile>() {
                          @Override public void call(final FacebookProfile facebookProfile) {
                            final String email = facebookProfile.getEmail();
                            final String name = facebookProfile.getName();
                            final String uid = facebookProfile.getId();

                            SimpleFacebookUser facebookUser = new SimpleFacebookUser.Builder(
                                AccessToken.getCurrentAccessToken().getToken()).setEmail(email)
                                .setProfileUrl(facebookProfile.getPictureUrl())
                                .setName(name)
                                .setCoverUrl(facebookProfile.getCoverUrl())
                                .build();

                            ActivityStarter.with(mContext, FragmentTask.SIGNUP)
                                .addBundle(new BundleSet.Builder().putRevealAnimation(false)
                                    .putSimpleFacebookUser(facebookUser)
                                    .build()
                                    .getBundle())
                                .start();

                          }
                        });
                  }
                });

            showDialogByErrorCode(builder);
            break;

          case NO_PASSWORD:
            builder.positiveText(R.string.cancel)
                .negativeText("네, 좋아요!")
                .callback(new MaterialDialog.ButtonCallback() {
                  @Override public void onPositive(MaterialDialog dialog) {
                    super.onPositive(dialog);
                    dialog.dismiss();
                  }

                  @Override public void onNegative(MaterialDialog dialog) {
                    super.onNegative(dialog);
                    dialog.dismiss();
                    FacebookBroker.getInstance()
                        .profileObservable((BaseActivity) mContext)
                        .subscribe(new Action1<FacebookProfile>() {
                          @Override public void call(final FacebookProfile facebookProfile) {
                            String email = facebookProfile.getEmail();
                            WLog.e(email);

                            Map<String, String> params = new HashMap<>();
                            params.put("email", email);
                            new DataProvider<>((BaseActivity)mContext, QueryType.FIND_PASSWORD, createReferrerBuilder()).responseTo(
                                new BaseApiResponseListener<BaseResponse>(mContext) {
                                  @Override
                                  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse result) {
                                    super.onSuccess(queryType, result);
                                    if (mContext == null) return;

                                    Toast.makeText(mContext, "비밀번호를 재설정할 수 있는 웹페이지 링크를 이메일로 보내드렸습니다.",
                                        Toast.LENGTH_LONG).show();
                                  }
                                }).withParams(params).withDialogMessage("처리 중..").request();

                          }
                        });
                  }
                });

            showDialogByErrorCode(builder);
            break;

          case NO_SESSION:
            //if (!r.getUrl().contains(QueryType.ACCOUNT.getApi())) {
              builder.positiveText(R.string.ok).callback(new MaterialDialog.ButtonCallback() {
                @Override public void onPositive(MaterialDialog dialog) {
                  super.onPositive(dialog);
                  dialog.dismiss();
                  ActivityStarter.with(mContext, FragmentTask.INTRO).start();
                  if (mContext instanceof BaseActivity) {
                    FacebookBroker.destroyAccessToken();
                    WatchaApp.getSessionManager().destroySession(mContext);
                    ((BaseActivity) mContext).finish();
                  }
                }
              });

              showDialogByErrorCode(builder);
            //}
            break;

          default:
            break;
        }
      }

      if (mCallback != null && !mIgnoreRetryDialog) {
        mCallback.onErrorCallback(mQueryType, parsedErrorMessage, r.getStatus());
        mIgnoreRetryDialog = true;
      }

      if (!mIgnoreRetryDialog) {
        ErrorDialog errorDialog = new ErrorDialog(mContext, parsedErrorMessage, mRetryCallback);
        errorDialog.show();
      }
    }
    return cause;
  }

  private ReferrerBuilder createReferrerBuilder() {
    if (mContext != null && mContext instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) mContext).getFragment();
      return new ReferrerBuilder(baseFragment.getCurrentScreenName());
    }
    return null;
  }

  private String parseErrorCode(String json) {
    BaseResponse response = WatchaApp.getInstance().getGson().fromJson(json, BaseResponse.class);
    if (response != null) {
      return response.getCode();
    }
    return null;
  }

  private String parseFailMessage(String json) {
    BaseResponse response = WatchaApp.getInstance().getGson().fromJson(json, BaseResponse.class);
    if (response != null) {
      return response.getMsg();
    }
    return null;
  }

  private MaterialDialog.Builder getAlertBuilderByErrorCode(String msg) {
    MaterialDialog.Builder builder = new MaterialDialog.Builder(mContext);
    builder.content(msg)
        .theme(Theme.LIGHT)
        .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
            FontHelper.FontType.ROBOTO_REGULAR.getTypeface());

    return builder;
  }

  private void showDialogByErrorCode(final MaterialDialog.Builder builder) {
    if (mHideErrorDialog) return ;
    mIgnoreRetryDialog = true;
    if (mContext != null && mContext instanceof Activity) {
      Activity act = (Activity) mContext;
      act.runOnUiThread(new Runnable() {
        @Override public void run() {
          if (builder != null) builder.build().show();
        }
      });
    }
  }
}