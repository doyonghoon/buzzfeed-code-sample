package com.frograms.watcha.retrofit;

import android.support.annotation.NonNull;
import com.frograms.watcha.model.response.BaseResponse;

/**
 * Retrofit 이용해서 서버로 요청하고 응답할때 사용하는 리스너.
 * 보통 Fragment, CustomView에 붙여서 사용하면 됩니다.
 */
public interface ApiResponseListener<V extends BaseResponse> {
  void onSuccess(@NonNull QueryType queryType, @NonNull V result);
}