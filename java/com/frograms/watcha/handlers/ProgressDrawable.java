package com.frograms.watcha.handlers;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;

public class ProgressDrawable extends ColorDrawable {

  private final Rect mBounds = getBounds();

  private final Paint mBackgroundPaint = new Paint();
  private final Paint mProgressPaint = new Paint();

  private boolean isBlowed = false;

  /** The upper range of the progress bar */
  private int max = 100;
  /** The current progress */
  private int finalProgress = 0, currentProgress = 0;

  private Runnable mRunnable = new Runnable() {

    @Override public void run() {
      invalidateSelf();
    }
  };

  public ProgressDrawable() {
    mBackgroundPaint.setColor(
        WatchaApp.getInstance().getResources().getColor(R.color.rating_toolbar_color));
    mBackgroundPaint.setAntiAlias(true);

    mProgressPaint.setColor(
        WatchaApp.getInstance().getResources().getColor(R.color.rating_toolbar_progressed_color));
    mProgressPaint.setAntiAlias(true);
  }

  @Override public void draw(Canvas canvas) {
    if (currentProgress < finalProgress) {

      if ((finalProgress - currentProgress) > 100) {
        currentProgress += 5;
      } else {
        currentProgress++;
      }
    } else if (finalProgress < currentProgress) {

      if ((currentProgress - finalProgress) > 100) {
        currentProgress -= 5;
      } else {
        currentProgress--;
      }
    }

    int right = mBounds.right * currentProgress / max;
    canvas.drawRect(mBounds.left, mBounds.top, right, mBounds.bottom, mProgressPaint);

    canvas.drawRect(right, mBounds.top, mBounds.right, mBounds.bottom, mBackgroundPaint);

    if (currentProgress != finalProgress) new Handler().postDelayed(mRunnable, 1);
  }

  public void setMax(int max) {
    if (max < 100) {
      max *= 5;
      isBlowed = true;
    } else {
      isBlowed = false;
    }

    this.max = max;
  }

  public void setProgress(int progress) {
    finalProgress = progress;
    if (isBlowed) finalProgress *= 5;

    invalidateSelf();
  }
}
