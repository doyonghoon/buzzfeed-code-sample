package com.frograms.watcha.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

public class ObservableHorizontalScrollView extends HorizontalScrollView {
  private ScrollViewListener scrollViewListener = null;

  @Override public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    scrollViewListener = null;
  }

  public interface ScrollViewListener {
    void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldx,
        int oldy);
  }

  public ObservableHorizontalScrollView(Context context) {
    super(context);
    init();
  }

  public ObservableHorizontalScrollView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public ObservableHorizontalScrollView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  public void setOnScrollListener(ScrollViewListener scrollViewListener) {
    this.scrollViewListener = scrollViewListener;
  }

  public boolean hasScrollViewListener() {
    return this.scrollViewListener != null;
  }

  private void init() {

  }

  @Override protected void onScrollChanged(int x, int y, int oldx, int oldy) {
    super.onScrollChanged(x, y, oldx, oldy);
    if (scrollViewListener != null) {
      scrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
    }
  }
}