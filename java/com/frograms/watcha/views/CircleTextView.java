package com.frograms.watcha.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;

/**
 * 둥근 배경을 가진 텍스트뷰.
 * 타이틀, 서브타이틀을 올려놓을 수 있음.
 * https://github.com/pavlospt/CircleView
 */
public class CircleTextView extends View {
  private static int DEFAULT_TITLE_COLOR = Color.BLACK;
  private static int DEFAULT_SUBTITLE_COLOR = Color.WHITE;

  private static String DEFAULT_TITLE = "";
  private static String DEFAULT_SUBTITLE = "";

  private static float DEFAULT_TITLE_SIZE = 25f;
  private static float DEFAULT_SUBTITLE_SIZE = 20f;

  private static int DEFAULT_STROKE_COLOR = Color.TRANSPARENT;
  private static int DEFAULT_BACKGROUND_COLOR = Color.WHITE;
  private static int DEFAULT_FILL_COLOR = Color.DKGRAY;

  private static float DEFAULT_STROKE_WIDTH = 5f;
  private static float DEFAULT_FILL_RADIUS = 0.9f;

  private static final int DEFAULT_VIEW_SIZE = 96;

  private int mTitleColor = DEFAULT_TITLE_COLOR;
  private int mSubtitleColor = DEFAULT_SUBTITLE_COLOR;
  private int mStrokeColor = DEFAULT_STROKE_COLOR;
  private int mBackgroundColor = DEFAULT_BACKGROUND_COLOR;
  private int mFillColor = DEFAULT_FILL_COLOR;

  private String mTitleText = DEFAULT_TITLE;
  private String mSubtitleText = DEFAULT_SUBTITLE;

  private float mTitleSize = DEFAULT_TITLE_SIZE;
  private float mSubtitleSize = DEFAULT_SUBTITLE_SIZE;
  private float mStrokeWidth = DEFAULT_STROKE_WIDTH;
  private float mFillRadius = DEFAULT_FILL_RADIUS;

  private TextPaint mTitleTextPaint;
  private TextPaint mSubTextPaint;

  private Paint mStrokePaint;
  private Paint mBackgroundPaint;
  private Paint mFillPaint;

  private RectF mInnerRectF;

  private int mViewSize;

  private boolean mIsCircleFontIcon = false;

  public CircleTextView(Context context) {
    super(context);
    init(null, 0);
  }

  public CircleTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(attrs, 0);
  }

  public CircleTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(attrs, defStyle);
  }

  private void init(AttributeSet attrs, int defStyle) {
    final TypedArray a =
        getContext().obtainStyledAttributes(attrs, R.styleable.CircleTextView, defStyle, 0);

    if (a.hasValue(R.styleable.CircleTextView_circleTitleText)) {
      mTitleText = a.getString(R.styleable.CircleTextView_circleTitleText);
    }

    if (a.hasValue(R.styleable.CircleTextView_circleSubtitleText)) {
      mSubtitleText = a.getString(R.styleable.CircleTextView_circleSubtitleText);
    }

    mTitleColor = a.getColor(R.styleable.CircleTextView_circleTitleColor, DEFAULT_TITLE_COLOR);
    mSubtitleColor =
        a.getColor(R.styleable.CircleTextView_circleSubtitleColor, DEFAULT_SUBTITLE_COLOR);
    mBackgroundColor =
        a.getColor(R.styleable.CircleTextView_circleBackgroundColorValue, DEFAULT_BACKGROUND_COLOR);
    mStrokeColor =
        a.getColor(R.styleable.CircleTextView_circleStrokeColorValue, DEFAULT_STROKE_COLOR);
    mFillColor = a.getColor(R.styleable.CircleTextView_circleFillColor, DEFAULT_FILL_COLOR);

    mTitleSize = a.getDimension(R.styleable.CircleTextView_circleTitleSize, DEFAULT_TITLE_SIZE);
    mSubtitleSize =
        a.getDimension(R.styleable.CircleTextView_circleSubtitleSize, DEFAULT_SUBTITLE_SIZE);

    mStrokeWidth =
        a.getFloat(R.styleable.CircleTextView_circleStrokeWidthSize, DEFAULT_STROKE_WIDTH);
    mFillRadius = a.getFloat(R.styleable.CircleTextView_circleFillRadius, DEFAULT_FILL_RADIUS);
    mIsCircleFontIcon = a.getBoolean(R.styleable.CircleTextView_circleFontIcon, false);

    a.recycle();

    // Font Icon
    if (mIsCircleFontIcon && !isInEditMode()) {
      mTitleTypeface = FontHelper.FontType.FONT_ICON.getTypeface();
      mSubtitleTypeface = FontHelper.FontType.FONT_ICON.getTypeface();
    }

    //Title TextPaint
    mTitleTextPaint = new TextPaint();
    mTitleTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    mTitleTextPaint.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    mTitleTextPaint.setTextAlign(Paint.Align.CENTER);
    mTitleTextPaint.setLinearText(true);
    mTitleTextPaint.setColor(mTitleColor);
    mTitleTextPaint.setTextSize(mTitleSize);
    setTypeface(mTitleTextPaint, mTitleTypeface);

    //Subtitle TextPaint
    mSubTextPaint = new TextPaint();
    mSubTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    mSubTextPaint.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    mSubTextPaint.setTextAlign(Paint.Align.CENTER);
    mSubTextPaint.setLinearText(true);
    mSubTextPaint.setColor(mSubtitleColor);
    mSubTextPaint.setTextSize(mSubtitleSize);
    setTypeface(mSubTextPaint, mSubtitleTypeface);

    //Stroke Paint
    mStrokePaint = new Paint();
    mStrokePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    mStrokePaint.setStyle(Paint.Style.STROKE);
    mStrokePaint.setColor(mStrokeColor);
    mStrokePaint.setStrokeWidth(mStrokeWidth);

    //Background Paint
    mBackgroundPaint = new Paint();
    mBackgroundPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    mBackgroundPaint.setStyle(Paint.Style.FILL);
    mBackgroundPaint.setColor(mBackgroundColor);

    //Fill Paint
    mFillPaint = new Paint();
    mFillPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
    mFillPaint.setStyle(Paint.Style.FILL);
    mFillPaint.setColor(mFillColor);

    mInnerRectF = new RectF();
  }

  private void setTypeface(@NonNull Paint paint, @Nullable Typeface typeface) {
    if (typeface != null) {
      paint.setTypeface(typeface);
    }
  }

  private void invalidateTextPaints() {
    mTitleTextPaint.setTextSize(mTitleSize);
    mSubTextPaint.setTextSize(mSubtitleSize);
    invalidate();
  }

  private void invalidatePaints() {
    mBackgroundPaint.setColor(mBackgroundColor);
    mStrokePaint.setColor(mStrokeColor);
    mFillPaint.setColor(mFillColor);
    invalidate();
  }

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    int width = resolveSize(DEFAULT_VIEW_SIZE, widthMeasureSpec);
    int height = resolveSize(DEFAULT_VIEW_SIZE, heightMeasureSpec);
    mViewSize = Math.min(width, height);

    setMeasuredDimension(width, height);
  }

  @Override protected void onDraw(Canvas canvas) {

    mInnerRectF.set(0, 0, mViewSize, mViewSize);
    mInnerRectF.offset((getWidth() - mViewSize) / 2, (getHeight() - mViewSize) / 2);

    final int halfBorder = (int) (mStrokePaint.getStrokeWidth() / 2f + 0.5f);

    mInnerRectF.inset(halfBorder, halfBorder);

    float centerX = mInnerRectF.centerX();
    float centerY = mInnerRectF.centerY();

    canvas.drawArc(mInnerRectF, 0, 360, true, mBackgroundPaint);

    float radius = (mViewSize / 2) * mFillRadius;

    canvas.drawCircle(centerX, centerY, radius + 0.5f - mStrokePaint.getStrokeWidth(), mFillPaint);

    int xPos = (int) centerX;
    int yPos = (int) (centerY - (mTitleTextPaint.descent() + mTitleTextPaint.ascent()) / 2);

    canvas.drawOval(mInnerRectF, mStrokePaint);

    canvas.drawText(mTitleText, xPos, yPos, mTitleTextPaint);

    canvas.drawText(mSubtitleText, xPos, yPos + 20, mSubTextPaint);
  }

  private Typeface mTitleTypeface = null;
  private Typeface mSubtitleTypeface = null;

  public void setTitleFont(FontHelper.FontType type) {
    mTitleTypeface = type.getTypeface();
  }

  public void setSubtitleFont(FontHelper.FontType type) {
    mSubtitleTypeface = type.getTypeface();
  }

  public String getTitleText() {
    return mTitleText;
  }

  public void setTitleText(String title) {
    mTitleText = title == null ? "" : title;
    invalidate();
  }

  public String getSubtitleText() {
    return mSubtitleText;
  }

  public void setSubtitleText(String subtitle) {
    mSubtitleText = subtitle;
    invalidate();
  }

  public int getStrokeColor() {
    return mStrokeColor;
  }

  public int getBackgroundColor() {
    return mBackgroundColor;
  }

  public int getFillColor() {
    return mStrokeColor;
  }

  public void setStrokeColor(int strokeColor) {
    mStrokeColor = strokeColor;
    invalidatePaints();
  }

  public void setBackgroundColor(int backgroundColor) {
    mBackgroundColor = backgroundColor;
    invalidatePaints();
  }

  public void setFillColor(int fillColor) {
    mFillColor = fillColor;
    invalidatePaints();
  }

  public float getStrokeWidth() {
    return mStrokeWidth;
  }

  public void setBackgroundColor(float strokeWidth) {
    mStrokeWidth = strokeWidth;
    invalidate();
  }

  public float getFillRadius() {
    return mFillRadius;
  }

  public void setFillRadius(float fillRadius) {
    mFillRadius = fillRadius;
    invalidate();
  }

  public float getTitleSize() {
    return mTitleSize;
  }

  public void setTitleSize(float titleSize) {
    mTitleSize = titleSize;
    invalidateTextPaints();
  }

  public float getSubtitleSize() {
    return mSubtitleSize;
  }

  public void setSubtitleSize(float subtitleSize) {
    mSubtitleSize = subtitleSize;
    invalidateTextPaints();
  }
}
