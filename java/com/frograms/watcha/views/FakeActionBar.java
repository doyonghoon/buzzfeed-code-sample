package com.frograms.watcha.views;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;

public class FakeActionBar extends FrameLayout {

  private TextView mTitleView;
  private Button mButton1, mButton2;
  private ViewGroup mFrame;

  public FakeActionBar(Context context) {
    super(context);
    doInflate();
    init();
  }

  private void doInflate() {
    LayoutInflater.from(getContext()).inflate(R.layout.dialog_actionbar, this);
  }

  private void init() {
    mFrame = (ViewGroup) findViewById(R.id.fake_ac_frame);
    mTitleView = (TextView) findViewById(R.id.fake_ac_title);
    mButton1 = (Button) findViewById(R.id.fake_ac_btn1);
    mButton2 = (Button) findViewById(R.id.fake_ac_btn2);
    changeTextColorWhenTouch(mButton2);

    FontHelper.RobotoRegular(mTitleView);
    FontHelper.RobotoRegular(mButton1);
    FontHelper.RobotoRegular(mButton2);
  }

  public void setFakeBackgroundColor(int color) {
    mFrame.setBackgroundColor(color);
  }

  public TextView getTitle() {
    return mTitleView;
  }

  public void setTitle(String title) {
    mTitleView.setText(title);
  }

  public Button getButton1() {
    return mButton1;
  }

  public Button getButton2() {
    return mButton2;
  }

  private void changeTextColorWhenTouch(Button view) {
    view.setOnTouchListener(new OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
          case R.id.fake_ac_btn2:

            switch (event.getAction()) {
              case MotionEvent.ACTION_DOWN:
                ((Button) v).setTextColor(Color.WHITE);
                break;

              case MotionEvent.ACTION_CANCEL:
              case MotionEvent.ACTION_MOVE:
              case MotionEvent.ACTION_UP:
                ((Button) v).setTextColor(getContext().getResources().getColor(R.color.light_gray));
                break;
            }

            break;
        }
        return false;
      }
    });
  }
}
