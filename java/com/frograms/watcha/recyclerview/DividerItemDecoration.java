package com.frograms.watcha.recyclerview;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.utils.WLog;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

  private static final int topPadding = WatchaApp.getInstance()
      .getResources()
      .getDimensionPixelSize(R.dimen.card_top_padding_in_grid);
  private static final int botPadding = WatchaApp.getInstance()
      .getResources()
      .getDimensionPixelSize(R.dimen.card_bottom_padding_in_grid);

  private static final int lrPadding = WatchaApp.getInstance()
      .getResources()
      .getDimensionPixelSize(R.dimen.card_left_right_padding_in_grid);
  private static final int middlePadding = WatchaApp.getInstance()
      .getResources()
      .getDimensionPixelSize(R.dimen.card_middle_padding_in_grid);

  private int[] position;
  private int maxSpan;

  public DividerItemDecoration(int maxSpan) {
    this.maxSpan = maxSpan;
    position = new int[maxSpan];
  }

  //@Override
  //public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
  //  super.onDraw(c, parent, state);
  //
  //  StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager)parent.getLayoutManager();
  //  final int childCount = parent.getChildCount();
  //  for (int i = 0; i < childCount; i++) {
  //    WLog.e("onDraw " + layoutManager.getDecoratedTop(parent.getChildAt(i)) + " " + layoutManager.getDecoratedBottom(parent.getChildAt(i)));
  //  }
  //}
  //
  //@Override
  //public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
  //  super.onDrawOver(c, parent, state);
  //
  //  StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager)parent.getLayoutManager();
  //
  //  final int childCount = parent.getChildCount();
  //  for (int i = 0; i < childCount; i++) {
  //    WLog.e("onDrawOver " + layoutManager.getDecoratedTop(parent.getChildAt(i)) + " " + layoutManager.getDecoratedBottom(parent.getChildAt(i)));
  //  }
  //}

  private void setToBottom(View view, RecyclerView parent,
      StaggeredGridLayoutManager layoutManager) {
    final int childCount = parent.getChildCount();

    for (int i = 0; i < childCount - 1; i++) {
      if (parent.getChildAt(i).getTag(R.id.TAG_FULL_SPAN) == null || (Boolean) parent.getChildAt(i)
          .getTag(R.id.TAG_FULL_SPAN)) {
        position = new int[maxSpan];
      } else {
        int bottom = layoutManager.getDecoratedBottom(parent.getChildAt(i));
        int lane = (Integer) parent.getChildAt(i).getTag(R.id.TAG_LANE);

        if (bottom > position[lane]) position[lane] = bottom;
      }
    }

    int lastIndex = 0, lastItem = position[0];
    for (int Loop1 = 0; Loop1 < maxSpan; Loop1++) {
      if (lastItem > position[Loop1]) {
        lastItem = position[Loop1];
        lastIndex = Loop1;
      }
    }

    view.setTag(R.id.TAG_LANE, lastIndex);

    if (lastIndex == 0) {
      view.setPadding(lrPadding, topPadding, middlePadding, botPadding);
      WLog.e(layoutManager.getDecoratedBottom(view) + "");
    } else if (lastIndex == maxSpan - 1) {
      view.setPadding(middlePadding, topPadding, lrPadding, botPadding);
    } else {
      view.setPadding(middlePadding, topPadding, middlePadding, botPadding);
    }
  }

  private void setToTop(View view, RecyclerView parent, StaggeredGridLayoutManager layoutManager) {
    final int childCount = parent.getChildCount();

    for (int i = 1; i < childCount; i++) {
      if (parent.getChildAt(i).getTag(R.id.TAG_FULL_SPAN) == null || (Boolean) parent.getChildAt(i)
          .getTag(R.id.TAG_FULL_SPAN)) {
        position = new int[maxSpan];
      } else {
        int top = layoutManager.getDecoratedTop(parent.getChildAt(i));
        int lane =
            parent.getChildAt(i) != null && parent.getChildAt(i).getTag(R.id.TAG_LANE) != null
                ? (Integer) parent.getChildAt(i).getTag(R.id.TAG_LANE) : 0;

        if (top < position[lane] || position[lane] == 0) position[lane] = top;
      }
    }

    int lastIndex = 0, lastItem = position[0];
    for (int Loop1 = 0; Loop1 < maxSpan; Loop1++) {
      if (lastItem < position[Loop1]) {
        lastItem = position[Loop1];
        lastIndex = Loop1;
      }
    }

    view.setTag(R.id.TAG_LANE, lastIndex);

    if (lastIndex == 0) {
      view.setPadding(lrPadding, topPadding, middlePadding, botPadding);
      WLog.e(layoutManager.getDecoratedBottom(view) + "");
    } else if (lastIndex == maxSpan - 1) {
      view.setPadding(middlePadding, topPadding, lrPadding, botPadding);
    } else {
      view.setPadding(middlePadding, topPadding, middlePadding, botPadding);
    }
  }

  @Override public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
      RecyclerView.State state) {
    if (view.getTag() == null) {
      return;
    }

    if (!(Boolean) view.getTag(R.id.TAG_FULL_SPAN)) {

      StaggeredGridLayoutManager layoutManager =
          (StaggeredGridLayoutManager) parent.getLayoutManager();

      final int childCount = parent.getChildCount();
      if (layoutManager.getDecoratedBottom(parent.getChildAt(childCount - 1)) == 0) {
        setToBottom(view, parent, layoutManager);
      } else {
        setToTop(view, parent, layoutManager);
      }
    } else {
      position = new int[maxSpan];
    }

    //if (view.getTag(R.id.TAG_DIVIDER) != null) {
    //
    //  switch ((Integer)view.getTag(R.id.TAG_DIVIDER)) {
    //    case R.id.TAG_DIVIDER_BOT:
    //        outRect.set(0, 0, 0, 1);
    //        break;
    //    case R.id.TAG_DIVIDER_NONE:
    //        outRect.set(0, 0, 0, 0);
    //        break;
    //  }
    //}
  }
}
