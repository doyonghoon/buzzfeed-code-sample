package com.frograms.watcha.utils;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

/**
 * xml로 만들기 짜증나서 걍 코드로 만드는거 쓰자..
 */
public class DrawableUtil {
  public static Drawable getClickableDrawable(int defaultColor, int focusedColor, int radius) {
    // Default Drawable
    GradientDrawable defaultDrawable = new GradientDrawable();
    defaultDrawable.setCornerRadius(radius);
    defaultDrawable.setColor(defaultColor);

    GradientDrawable focusedDrawable = new GradientDrawable();
    focusedDrawable.setCornerRadius(radius);
    focusedDrawable.setColor(focusedColor);

    StateListDrawable states = new StateListDrawable();
    if (focusedColor != 0) {
      states.addState(new int[] { android.R.attr.state_pressed }, focusedDrawable);
      states.addState(new int[] { android.R.attr.state_focused }, focusedDrawable);
    }
    states.addState(new int[] {}, defaultDrawable);

    return states;
  }
}
