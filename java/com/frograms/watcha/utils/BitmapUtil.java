package com.frograms.watcha.utils;

import android.graphics.Bitmap;

/**
 * 비트맵 관련된 메소드들.
 */
public class BitmapUtil {
  /**
   * 이미지 비율은 그대로 두면서, 이미지 최대 크기만 변경 함.
   *
   * @param bm 크기 변경할 이미지
   * @param maxImageSize 변경할 크기 px
   */
  public static Bitmap scaleDown(Bitmap bm, float maxImageSize) {
    if (bm != null) {
      float ratio = Math.min(maxImageSize / bm.getWidth(), maxImageSize / bm.getHeight());
      int width = Math.round(ratio * bm.getWidth());
      int height = Math.round(ratio * bm.getHeight());
      return Bitmap.createScaledBitmap(bm, width, height, true);
    }
    return null;
  }
}
