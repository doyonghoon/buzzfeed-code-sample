package com.frograms.watcha.utils;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;

/**
 * 이메일, 비번, 유저이름, 코멘트 100자평 등 유저로부터 값을 잘 입력 받았는지 확인해주는 클래스.
 */
public class FormValidator {

  /**
   * 등록해놓은 {@link TextWatcher} 에서 콜백받은 값을 중간에 가로채서 값이 올바르게 입력되었는지 확인해줌.
   */
  public interface FormValidationListener {
    void onValidation(FormStyle style, EditText v, String text, boolean isValid);
  }

  private interface FormValidationMethod {
    boolean isValid(String text);

    int getMaxCount();

    int getMinCount();
  }

  /**
   * 확인할 수 있는 form 목록.
   */
  public enum FormStyle implements FormValidationMethod {
    EMAIL {
      @Override public boolean isValid(String text) {
        final String emailRegex =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        return !TextUtils.isEmpty(text) && text.matches(emailRegex);
      }

      @Override public int getMaxCount() {
        return 0;
      }

      @Override public int getMinCount() {
        return 0;
      }
    },
    PASSWORD {
      @Override public boolean isValid(String text) {
        return !TextUtils.isEmpty(text) && !(text.length() < getMinCount()
            || text.length() > getMaxCount());
      }

      @Override public int getMaxCount() {
        return 20;
      }

      @Override public int getMinCount() {
        return 6;
      }
    },
    USERNAME {
      @Override public boolean isValid(String text) {
        final String koreanFirst = WatchaApp.getInstance().getString(R.string.account_korean_first);
        final String koreanLast = WatchaApp.getInstance().getString(R.string.account_korean_last);
        final String nameRegex =
            String.format("^([%s-%s]*[A-Za-z0-9]*[\\s]*)*$", koreanFirst, koreanLast);
        return !(text.length() < getMinCount() || text.length() > getMaxCount() || !text.matches(
            nameRegex));
      }

      @Override public int getMaxCount() {
        return 20;
      }

      @Override public int getMinCount() {
        return 2;
      }
    },
    COMMENT {
      @Override public boolean isValid(String text) {
        final int max = 100;
        return !TextUtils.isEmpty(text) && text.length() <= max;
      }

      @Override public int getMaxCount() {
        return 100;
      }

      @Override public int getMinCount() {
        return 0;
      }
    },
    DECK_TITLE {
      @Override public boolean isValid(String text) {
        return !TextUtils.isEmpty(text) && !(text.length() < getMinCount()
            || text.length() > getMaxCount());
      }

      @Override public int getMaxCount() {
        return 80;
      }

      @Override public int getMinCount() {
        return 1;
      }
    },
    DECK_BODY {
      @Override public boolean isValid(String text) {
        return !TextUtils.isEmpty(text) && text.length() <= getMaxCount();
      }

      @Override public int getMaxCount() {
        return 200;
      }

      @Override public int getMinCount() {
        return 0;
      }
    }
  }

  private FormStyle mStyle = null;
  private ValidationWatcher mValidationWatcher = null;

  public FormValidator(@NonNull FormStyle style) {
    mStyle = style;
  }

  public boolean isValid(String text) {
    if (mValidationWatcher == null) {
      mValidationWatcher = new ValidationWatcher(null, mStyle, null);
    }
    return mValidationWatcher.isValid(text, mStyle);
  }

  /**
   * {@link TextWatcher} 를 등록 시켜서 값이 변할때마다 올바른 값인지 콜백해줌.
   *
   * @param v 콜백받을 뷰.
   * @param listener 값 확인해주는 리스너.
   */
  public void registerValidator(EditText v, @NonNull FormValidationListener listener) {
    ValidationWatcher watcher = new ValidationWatcher(v, mStyle, listener);
    v.addTextChangedListener(watcher);
  }

  private static class ValidationWatcher implements TextWatcher {

    private final EditText mEditText;
    private final FormStyle mStyle;
    private final FormValidationListener mListener;

    private ValidationWatcher(EditText editText, @NonNull FormStyle style,
        FormValidationListener listener) {
      mEditText = editText;
      mStyle = style;
      mListener = listener;
    }

    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override public void afterTextChanged(Editable s) {
      if (mListener != null) {
        mListener.onValidation(mStyle, mEditText, s.toString(), isValid(s.toString(), mStyle));
      }
    }

    public boolean isValid(String text, @NonNull FormStyle style) {
      return !TextUtils.isEmpty(text) && style.isValid(text);
    }
  }
}
