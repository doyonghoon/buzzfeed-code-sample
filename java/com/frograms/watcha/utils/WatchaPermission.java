package com.frograms.watcha.utils;

import com.frograms.watcha.listeners.WatchaPermissionsCreator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 왓챠에서 사용되는 페이스북 퍼미션들을 사용 목적에 맞게 그룹으로 묶음.
 */
public enum WatchaPermission implements WatchaPermissionsCreator {
  /**
   * 로그인/회원가입때 수집하는 퍼미션.
   */
  DEFAULT {
    @Override public List<String> getPermissions() {
      return new ArrayList<>(Arrays.asList("public_profile", "email", "user_friends"));
    }
  },
  /**
   * 코멘트 작성할때 사용하는 퍼미션.
   */
  PUBLISH {
    @Override public List<String> getPermissions() {
      return Collections.singletonList("publish_actions");
    }
  },
}
