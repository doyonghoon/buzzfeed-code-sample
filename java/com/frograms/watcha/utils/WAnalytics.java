package com.frograms.watcha.utils;

import android.support.v4.util.LruCache;
import com.frograms.watcha.model.LogAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

/**
 * 서버 로깅에 모든 것.
 */
public class WAnalytics {

  private static class UserActionSet extends LruCache<Integer, LogAction> {
    private static final int MAX_SIZE = 100;

    public UserActionSet() {
      super(MAX_SIZE);
    }
  }

  public interface ScenarioAcitons {
    List<LogAction> getScenario();
  }

  private static enum Scenario implements ScenarioAcitons {
    RATING("rating") {
      @Override public List<LogAction> getScenario() {
        List<LogAction> actions = new ArrayList<>();
        actions.add(LogAction.RATING);
        actions.add(LogAction.LOGIN);
        return actions;
      }
    };

    private String mName;

    Scenario(String name) {
      mName = name;
    }

    public String getName() {
      return mName;
    }
  }

  private static WAnalytics singleton;

  public static void log(LogAction action) {
    if (singleton == null) {
      singleton = new WAnalytics();
    }

    singleton.mUserActionList.add(action);
    List<Scenario> scenarios = singleton.getMatchedScenarioList(singleton.mUserActionList);
    if (scenarios.isEmpty()) {
      WLog.i("scenario not found yet. userActionCount: " + singleton.mUserActionList.size());
    } else {
      for (Scenario s : scenarios) {
        WLog.i("scenarioName: " + s.getName() + ", scenarios: " + s.getScenario().toString());
      }
    }
  }

  //private UserActionSet mUserActionSet;
  private List<LogAction> mUserActionList = new ArrayList<>();

  public WAnalytics() {
    init();
  }

  private void init() {
    //mUserActionSet = new UserActionSet();
    TreeMap<Integer, LogAction> userTree = new TreeMap<>();
    final int lastPosition = userTree.isEmpty() ? 0 : userTree.size() - 1;
    userTree.ceilingEntry(lastPosition);
    userTree.put(lastPosition, null);
  }

  private List<Scenario> getMatchedScenarioList(List<LogAction> userActionList) {
    List<Scenario> scenarioSetList = Arrays.asList(Scenario.values());

    List<Scenario> tmpSetList = new ArrayList<>();
    for (int i = 0; i < scenarioSetList.size(); i++) {
      final Scenario scenarioActions = scenarioSetList.get(i);
      if (userActionList.containsAll(scenarioActions.getScenario())) {
        tmpSetList.add(scenarioSetList.get(i));
      }
    }

    return tmpSetList;
  }

  private int getActionPosition(LogAction action) {
    return singleton.mUserActionList.indexOf(action);
  }
}
