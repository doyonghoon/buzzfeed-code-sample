package com.frograms.watcha.utils;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import java.lang.ref.WeakReference;
import java.util.List;

public class RecycleUtils {

  //public static void clearImage(ImageView imageView) {
  //Drawable d = imageView.getDrawable();
  //
  //	if (imageView instanceof ResizableImageView) {
  //		ResizableImageView view = (ResizableImageView)imageView;
  //		view.setUseResource(true);
  //		if (d != null) {
  //    		if (!view.useResource()) {
  //    			if (d instanceof BitmapDrawable) {
  //     			Bitmap b = ((BitmapDrawable)d).getBitmap();
  //     			b.recycle();
  //     			b = null;
  //    			}
  //    		}
  //
  //         d.setCallback(null);
  //		}
  //	} else {
  //		if (d != null) {
  // 		d.setCallback(null);
  //		}
  //	}
  //	imageView.setImageDrawable(null);
  //	imageView.setTag(R.id.TAG_URL, "");
  //}

  @SuppressWarnings("deprecation") public static void recursiveRecycle(View root) {
    if (root == null) return;

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
      root.setBackgroundDrawable(null);
    } else {
      root.setBackground(null);
    }
    if (root instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) root;
      int count = group.getChildCount();
      for (int i = 0; i < count; i++) {
        if (group.getChildAt(i) != null) recursiveRecycle(group.getChildAt(i));
      }

      if (group != null && !(root instanceof AdapterView)) {
        group.removeAllViews();
      }
    }

    if (root instanceof ImageView) {
      ImageView imageView = (ImageView) root;
      //clearImage(imageView);
    }

    root = null;

    return;
  }

  public static void recursiveRecycle(List<WeakReference<View>> recycleList) {
    for (WeakReference<View> ref : recycleList) {
      recursiveRecycle(ref.get());
    }
    recycleList.clear();
  }
}