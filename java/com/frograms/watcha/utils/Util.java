package com.frograms.watcha.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class Util {
  public static String calTime(long create, long finish) {
    Context context = WatchaApp.getInstance().getApplicationContext();
    int[] time = { 60, 24, 30, 12 };
    String[] timeString = {
        context.getString(R.string.minute), context.getString(R.string.hour),
        context.getString(R.string.day), context.getString(R.string.month2)
    };

    float diff = ((float) (finish - create) / 1000 / 60);
    String text = "";

    if (diff > 0) {
      for (int Loop1 = 0; Loop1 < 4; Loop1++) {
        text = (String.valueOf((long) (diff % time[Loop1])) + timeString[Loop1]);
        // text;
        diff = diff / time[Loop1];
        if (((long) diff) == 0) break;
      }
      if ((long) diff > 0) {
        text = (String.valueOf((long) diff) + context.getString(R.string.year));
      }
    } else {
      text = context.getString(R.string.now);
    }

    return text;
  }

  public static int getNavigationBarHeight(Context context, int orientation) {
    Resources resources = context.getResources();
    int id = resources.getIdentifier(
        orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height"
            : "navigation_bar_height_landscape", "dimen", "android");
    if (id > 0) {
      return resources.getDimensionPixelSize(id);
    }
    return 0;
  }

  public static int getStatusBarHeight(Context c) {
    int result = 0;
    int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");
    if (resourceId > 0) {
      result = c.getResources().getDimensionPixelSize(resourceId);
    }
    return result;
  }

  public static Point getDisplaySize(Context context) {
    if (Build.VERSION.SDK_INT >= 17) {
      return getDisplaySizeMinSdk17(context);
    } else if (Build.VERSION.SDK_INT >= 13) {
      return getDisplaySizeMinSdk13(context);
    } else {
      return getDisplaySizeMinSdk1(context);
    }
  }

  @TargetApi(17) private static Point getDisplaySizeMinSdk17(Context context) {
    final WindowManager windowManager =
        (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    final Display display = windowManager.getDefaultDisplay();

    final DisplayMetrics metrics = new DisplayMetrics();
    display.getRealMetrics(metrics);

    final Point size = new Point();
    size.x = metrics.widthPixels;
    size.y = metrics.heightPixels;

    return size;
  }

  @TargetApi(13) private static Point getDisplaySizeMinSdk13(Context context) {

    final WindowManager windowManager =
        (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    final Display display = windowManager.getDefaultDisplay();

    final Point size = new Point();
    display.getSize(size);

    return size;
  }

  @SuppressWarnings("deprecation") private static Point getDisplaySizeMinSdk1(Context context) {

    final WindowManager windowManager =
        (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    final Display display = windowManager.getDefaultDisplay();

    final Point size = new Point();
    size.x = display.getWidth();
    size.y = display.getHeight();

    return size;
  }

  public static void openKeyboard(final View view) {

    view.requestFocus();

    new Handler().postDelayed(new Runnable() {

      @Override public void run() {
        InputMethodManager imm =
            (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);

        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
      }
    }, 500);
  }

  public static void closeKeyboard(final View view) {
    if (view != null && view.getContext() != null) {
      InputMethodManager imm =
          (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }

  //public static URI getDomainURI() {
  //  try {
  //    return new URI(WatchaApp.getInstance().DOMAIN);
  //  } catch (URISyntaxException e) {
  //    e.printStackTrace();
  //  }
  //  return null;
  //}

  /**
   * 최신 앱인지 알려줌.
   *
   * @param version 버전
   * @return true면 업데이트가 필요함. false면 최신임.
   */
  public static boolean needToUpdateApplication(String version) {

    boolean check = false;

    try {
      WatchaApp app = WatchaApp.getInstance();
      PackageInfo pi = app.getPackageManager().getPackageInfo(app.getPackageName(), 0);

      String versionNow = pi.versionName;
      WLog.i("Check Version " + version + " with " + versionNow);

      pi = null;
      int start1 = 0, start2 = 0;

      while (true) {
        boolean fin = false;
        int temp, temp2;
        if (versionNow.indexOf(".", start1) != -1) {
          temp = Integer.valueOf(versionNow.substring(start1, versionNow.indexOf(".", start1)));
        } else {
          temp = Integer.valueOf(versionNow.substring(start1));
        }

        if (version.indexOf(".", start2) != -1) {
          temp2 = Integer.valueOf(version.substring(start2, version.indexOf(".", start2)));
        } else {
          temp2 = Integer.valueOf(version.substring(start2));
          fin = true;
        }

        Log.d("Temp", temp + " " + temp2);

        if (temp < temp2) {
          check = true;
          break;
        } else if (temp > temp2) {
          check = false;
          break;
        } else {
          start1 = versionNow.indexOf(".", start1) + 1;
          start2 = versionNow.indexOf(".", start2) + 1;
        }

        if (fin) break;
      }
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }

    return check;
  }

  public static Bitmap getBitmapFromFile(String path) {
    //		BitmapFactory.Options options = new BitmapFactory.Options();
    //        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    //		options.inJustDecodeBounds = true;
    //
    //		File file = new File(path);
    //		Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.RGB_565;
    Bitmap bitmap = BitmapFactory.decodeFile(path, options);
    return bitmap;
  }

  public static File getOptimizedImageFile(String path) {
    final int maxSize = 70000;
    File resultFile = new File(path);
    Log.i("MyMenuFragment", "fileSize: " + resultFile.length());

    Bitmap bitmap = getBitmapFromFile(path);

    if ((bitmap.getWidth() >= 1024) && (bitmap.getHeight() >= 1024)) {
      BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
      bmpOptions.inSampleSize = 1;
      while ((bitmap.getWidth() >= 1024) && (bitmap.getHeight() >= 1024)) {
        bmpOptions.inSampleSize++;
        bitmap = BitmapFactory.decodeFile(path, bmpOptions);
      }

      Log.i("MyMenuFragment", "Resize: " + bmpOptions.inSampleSize);
    }

    int compressQuality = 104;
    int streamLength = maxSize;
    while (streamLength >= maxSize) {
      ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
      compressQuality -= 5;
      Log.d("MyMenuFragment", "Quality: " + compressQuality);
      bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
      byte[] bmpPicByteArray = bmpStream.toByteArray();
      streamLength = bmpPicByteArray.length;
      Log.d("MyMenuFragment", "Size: " + streamLength);
    }
    try {
      FileOutputStream bmpFile = new FileOutputStream(path);
      bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile);
      bmpFile.flush();
      bmpFile.close();

      return new File(path);
    } catch (Exception e) {
      Log.e("MyMenuFragment", "Error on saving file");
    }

    return resultFile;
  }

  public static String getLargeNum(String num) {
    String largeNum = "";
    if (!TextUtils.isEmpty(num) && TextUtils.isDigitsOnly(num)) {

      int count = 0;
      for (int Loop1 = num.length() - 1; Loop1 >= 0; Loop1--) {
        count++;

        largeNum = num.charAt(Loop1) + largeNum;
        if (count % 3 == 0) largeNum = "," + largeNum;
      }
    }
    return largeNum;
  }

  public static String getEllipsipSizeStringByLength(String s, int max) {
    if (s.length() > max) return s.substring(0, max) + "...";
    return s;
  }
}
