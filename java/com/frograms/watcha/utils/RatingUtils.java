package com.frograms.watcha.utils;

import com.frograms.watcha.application.WatchaApp;
import com.google.gson.GsonBuilder;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * 엄청 정확한 별점 값을 좀 덜 정확하게 바꿔주는 클래스.
 */
public class RatingUtils {

  /**
   * 간혹 별점일 수도 있고, 아닐 수도 있는 값을 서버가 보내줄 때가 있는데
   * try catch 로 double 값인지 아닌지 알아내는 건 별로 안좋아서
   * apache 라이브러리로 double 일 경우에만 적절히 처리함.
   *
   * @param text 별점일 수도 있고, 아닐 수도 있는 값.
   * @return 소수점 1번째 자리로 리턴해주거나, 파라미터로 받았던 텍스트를 그대로 리턴해줌.
   */
  public static String getRatingNumber(String text) {
    if (NumberUtils.isNumber(text)) {
      double ratingValue = Double.parseDouble(text);
      DecimalFormat df = new DecimalFormat("0.0");
      return df.format(round(ratingValue, 1));
    }
    return text;
  }

  /**
   * 소수점 몇째 자리로 잘라줌.
   *
   * @param value 자를 값.
   * @param places 자르고 싶은 위치.
   */
  public static double round(double value, int places) {
    if (places < 0) {
      throw new IllegalArgumentException();
    }
    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }

  /**
   * 날짜를 ISO1601 규격을 준수하여 파싱함.
   *
   * @link http://developer.android.com/reference/java/text/SimpleDateFormat.html
   * @see WatchaApp#getGson()
   * @see GsonBuilder#setDateFormat(int, int)
   */
  public static String getHumanReadableDate(Date d) {
    Calendar c = Calendar.getInstance();
    c.setTime(d);
    return String.format("%d년 %d월 %d일", c.get(Calendar.YEAR), c.get(Calendar.MONTH)+1,
        c.get(Calendar.DAY_OF_MONTH));
  }
}
