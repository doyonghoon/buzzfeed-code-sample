package com.frograms.watcha.utils;

import android.graphics.Color;
import android.support.annotation.ColorInt;

/**
 * Created by doyonghoon on 15. 5. 27..
 */
public class ColorUtils {

  private static final float ACCENT_COLOR_FACTOR = 0.75f;

  public static int getAccentColor(@ColorInt int primaryColor) {
    int r = (int) (Color.red(primaryColor) * ACCENT_COLOR_FACTOR);
    int b = (int) (Color.blue(primaryColor) * ACCENT_COLOR_FACTOR);
    int g = (int) (Color.green(primaryColor) * ACCENT_COLOR_FACTOR);
    return Color.rgb(r, g, b);
  }
}
