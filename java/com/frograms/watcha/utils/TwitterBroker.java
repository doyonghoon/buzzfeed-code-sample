package com.frograms.watcha.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.SettingHelper;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

/**
 * 트위터 연동/해제를 관리하는 클래스.
 *
 * 페이스북은 {@link FacebookBroker} 를 참고..
 */
public class TwitterBroker {

  private static TwitterBroker singleton;

  /**
   * 여기서만 생성할 수 있음.
   */
  private TwitterBroker() {
  }

  /**
   * 여기서만 접근하는 singleton 인스턴스.
   */
  private static TwitterBroker getInstance() {
    if (singleton == null) {
      singleton = new TwitterBroker();
    }
    return singleton;
  }

  /**
   * 기기에 트위터 세션이 있는지 확인함.
   */
  public static boolean hasSession() {
    TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
    String token = null;
    if (session != null && session.getAuthToken() != null) {
      token = session.getAuthToken().token;
    }
    return !TextUtils.isEmpty(token);
  }

  /**
   * 트위터 sdk 를 통해서 세션을 지움.
   */
  public static void logOutTwitter() {
    if (hasSession()) {
      TwitterCore.getInstance().logOut();
    }
  }

  /**
   * 기기에서 트위터 세션을 가져옴.
   */
  public static TwitterAuthToken getToken() {
    if (hasSession()) {
      return TwitterCore.getInstance().getSessionManager().getActiveSession().getAuthToken();
    }
    return null;
  }

  /**
   * {@link TwitterBroker#logOutTwitter()} 을 진행한 후에 왓챠 서버에 트위터 연동을 해제함.
   */
  public static void removeTwitterSession(Activity activity,
      SettingHelper.OnSettingResponseListener listener) {
    logOutTwitter();
    SettingHelper.deleteTwt(activity, listener);
  }

  public static void connectTwitter(final BaseActivity activity,
      final SettingHelper.OnSettingResponseListener listener) {
    // ui thread 에서 띄워야함.
    Handler handler = new Handler(Looper.getMainLooper());
    handler.postDelayed(new Runnable() {
      @Override public void run() {
        new MaterialDialog.Builder(activity).title(R.string.twt_connect)
            .content(R.string.want_connect_twt)
            .theme(Theme.LIGHT)
            .typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
                FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
            .positiveText(R.string.yes)
            .negativeText(R.string.no)
            .callback(new MaterialDialog.ButtonCallback() {
              @Override public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                loadTwitterSession(activity, listener);
              }

              @Override public void onNegative(MaterialDialog dialog) {
                super.onNegative(dialog);
                dialog.dismiss();
              }
            })
            .build()
            .show();
      }
    }, 1);
  }

  /**
   * 트위터 sdk 를 통해서 세션 받아오면 왓챠 서버에 트위터 계정 연동을 요청하고 응답 콜백을 리턴해줌.
   */
  public static void loadTwitterSession(final BaseActivity activity,
      final SettingHelper.OnSettingResponseListener listener) {
    TwitterLoginButton b = new TwitterLoginButton(activity);
    activity.setTwitterButton(b);
    b.setCallback(new Callback<TwitterSession>() {
      @Override public void success(Result<TwitterSession> twitterSessionResult) {
        SettingHelper.connectTwt(activity, twitterSessionResult.data.getAuthToken().token,
            twitterSessionResult.data.getAuthToken().secret, listener);
      }

      @Override public void failure(TwitterException e) {
        WLog.i("twitterError: " + e.getMessage());
        Toast.makeText(activity, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        e.printStackTrace();
      }
    });
    b.performClick();
  }
}
