package com.frograms.watcha.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;

/**
 * Created by doyonghoon on 15. 6. 30..
 */
public class ScreenNameUtils {
  @Nullable
  @AnalyticsManager.ScreenNameType
  public static String getScreenName(@Nullable Context context) {
    if (context != null && context instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) context;
      BaseFragment baseFragment = baseActivity.getFragment();
      if (baseFragment != null && baseFragment instanceof AbsPagerFragment) {
        return ((AbsPagerFragment) baseFragment).getCurrentFragment().getCurrentScreenName();
      } else if (baseFragment != null) {
        baseFragment.getCurrentScreenName();
      }
    }
    return null;
  }
}
