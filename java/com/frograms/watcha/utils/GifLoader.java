package com.frograms.watcha.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.ImageViewTarget;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * GIF파일을 불러올 때 사용되는 클래스
 *
 * 향후 기능 확장(load from url, load from localPath 등)은 나중에 생각하자.
 *
 * Created by jahun on 2015. 6. 9..
 */
public class GifLoader {

  private Context mContext;
  private GifAsyncTask mLoadTask;

  /**
   * Context를 정보를 담으면서 GifLoader를 새로 생성한다.
   * @param context
   * @return
   */
  public static GifLoader with(Context context){
    GifLoader loader = new GifLoader();
    loader.mContext = context;
    return loader;
  }

  /**
   * asset폴더에 있는 gif파일을 지정한다.
   * @param assetManager
   * @param fileName
   * @return
   */
  public GifLoader load(AssetManager assetManager, String fileName){
    mLoadTask = new GifAsyncTask(mContext, assetManager, fileName);
    return this;
  }

  /**
   * gif가 보여질 이미지뷰를 담으며 로드한다.
   * @param target
   * @return
   */
  public void into(ImageView target){
    mLoadTask.setTargetView(target);
    mLoadTask.execute();
  }


  private static class GifAsyncTask extends AsyncTask<Void, Void, Void> {

    private final String CACHE_FILE_NAME = "tmp.gif";

    private Context mContext;
    private String mFileName;
    private AssetManager mAssetManager;

    private File mCachedFile;
    private ImageView mTargetImageView;

    public GifAsyncTask(Context context, AssetManager assetManager, String fileName) {
      this.mContext = context;
      this.mAssetManager = assetManager;
      this.mFileName = fileName;
    }

    @Override protected Void doInBackground(Void... params) {

      InputStream inputStream = null;

      //asset으로부터 gif파일을 로드
      if(mAssetManager != null){
        try {
          inputStream = mAssetManager.open(mFileName);
        } catch (IOException e) {
          e.printStackTrace();
          WLog.e(e.getMessage());
        }
      }

      mCachedFile = createFileFromInputStream(inputStream);
      return null;
    }

    private File createFileFromInputStream(InputStream inputStream) {

      try{
        File file = new File(mContext.getCacheDir(), CACHE_FILE_NAME);
        OutputStream outputStream = new FileOutputStream(file);

        BufferedInputStream bis = new BufferedInputStream(inputStream);
        byte buffer[] = new byte[1024];
        int length = 0;

        while( (length = bis.read(buffer)) > 0 ){
          outputStream.write(buffer,0,length);
        }

        outputStream.close();
        inputStream.close();

        return file;
      }catch (IOException e) {
        e.printStackTrace();
        WLog.e(e.getMessage());
      }

      return null;
    }

    @Override protected void onPostExecute(Void aVoid) {

      Glide.with(mContext)
          .load(mCachedFile)
          .asGif()
          .into(new ImageViewTarget<GifDrawable>(mTargetImageView) {
            @Override protected void setResource(GifDrawable resource) {
              ImageView targetView = this.view;

              if(targetView.getVisibility() != View.VISIBLE){
                targetView.setVisibility(View.VISIBLE);
              }
              resource.start();
              targetView.setImageDrawable(resource);
            }
          });
    }

    public void setTargetView(ImageView target) {
      this.mTargetImageView = target;
    }
  }
}