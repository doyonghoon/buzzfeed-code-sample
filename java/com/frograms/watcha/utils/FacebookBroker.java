package com.frograms.watcha.utils;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.DefaultAudience;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.models.FacebookProfile;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * 어느 곳이던 (Activity,Fragment,View) 페이스북 세션이 필요한 경우, 페이스북 연결을 해줌.
 * <p/>
 * 포스팅이나 공유같은 것도 이 클래스를 통해서 수행할 수 있음.
 * 인증 절차가 완료된 후엔 반드시 UiThread로 콜백해줌.
 */
public class FacebookBroker {

  private static FacebookBroker mSingleton;
  private CallbackManager mCallback = null;

  private static class Permission {

    enum Status {
      @SerializedName("granted")GRANTED,
      @SerializedName("declined")DECLINED
    }

    private @SerializedName("status") Status status;
    private @SerializedName("permission") String permission;

    public Status getStatus() {
      return status;
    }

    public String getPermission() {
      return permission;
    }

    @Override public String toString() {
      return Item.gson.toJson(this);
    }
  }

  public static AccessToken createAccessToken(@NonNull String tokenString, @NonNull String userId) {
    return createAccessToken(tokenString, userId, WatchaPermission.DEFAULT.getPermissions(), null);
  }

  public static AccessToken createAccessToken(@NonNull String tokenString, @NonNull String userId,
      List<String> defPermissions, List<String> declinedPermissions) {
    return new AccessToken(tokenString, BuildConfig.FACEBOOK_ID, userId, defPermissions,
        declinedPermissions, AccessTokenSource.FACEBOOK_APPLICATION_NATIVE, null, null);
  }

  private static Observable<List<Permission>> parsePermissions(final String json) {
    return Observable.create(new Observable.OnSubscribe<List<Permission>>() {
      @Override public void call(Subscriber<? super List<Permission>> subscriber) {
        JsonArray data = WatchaApp.getInstance()
            .getGson()
            .fromJson(json, JsonObject.class)
            .getAsJsonArray("data");
        List<Permission> permissions =
            WatchaApp.getInstance().getGson().fromJson(data, new TypeToken<List<Permission>>() {
            }.getType());
        subscriber.onNext(permissions);
      }
    });
  }

  public static Observable<AccessToken> getAccessTokenWithGrantedPermissions(
      @NonNull final AccessToken accessToken, @NonNull final Activity activity) {
    return Observable.create(new Observable.OnSubscribe<AccessToken>() {
      @Override public void call(final Subscriber<? super AccessToken> subscriber) {
        GraphRequest r = GraphRequest.newGraphPathRequest(accessToken, "me/permissions",
            new GraphRequest.Callback() {
              @Override public void onCompleted(GraphResponse response) {
                if (response != null) {
                  if (!TextUtils.isEmpty(response.getRawResponse())) {
                    parsePermissions(response.getRawResponse()).subscribe(
                        new Action1<List<Permission>>() {
                          @Override public void call(List<Permission> permissions) {
                            List<String> granted = new ArrayList<>();
                            List<String> declined = new ArrayList<>();
                            for (Permission p : permissions) {
                              switch (p.getStatus()) {
                                case DECLINED:
                                  declined.add(p.getPermission());
                                  break;
                                case GRANTED:
                                  granted.add(p.getPermission());
                                  break;
                              }
                            }
                            AccessToken.setCurrentAccessToken(
                                createAccessToken(AccessToken.getCurrentAccessToken().getToken(),
                                    AccessToken.getCurrentAccessToken().getUserId(), granted,
                                    declined));
                            subscriber.onNext(AccessToken.getCurrentAccessToken());
                          }
                        });
                  } else {
                    if (response.getError() != null) {
                      WLog.i("error: " + response.getError().toString());
                      destroyAccessToken();
                      accessTokenObservable(activity).subscribe(new Action1<AccessToken>() {
                        @Override public void call(AccessToken accessToken) {
                          AccessToken.setCurrentAccessToken(accessToken);
                          subscriber.onNext(accessToken);
                          subscriber.onCompleted();
                        }
                      });
                    }
                  }
                }
              }
            });
        r.executeAsync();
      }
    });
  }

  public static FacebookBroker getInstance() {
    if (mSingleton == null) {
      mSingleton = new FacebookBroker();
    }
    return mSingleton;
  }

  public FacebookBroker() {
    FacebookSdk.sdkInitialize(WatchaApp.getInstance());
    mCallback = CallbackManager.Factory.create();
  }

  public CallbackManager getCallbackManager() {
    return mCallback;
  }

  /**
   * 페이스북에서 세션 가져오기.
   * <p/>
   * 권한도 가져오기.
   *
   * @param activity 페이스북 연동을 하는 액티비티.
   */
  public static Observable<AccessToken> accessTokenObservable(@NonNull final Activity activity) {
    return Observable.create(new Observable.OnSubscribe<AccessToken>() {
      @Override public void call(final Subscriber<? super AccessToken> subscriber) {
        if (AccessToken.getCurrentAccessToken() != null) {
          subscriber.onNext(AccessToken.getCurrentAccessToken());
          subscriber.onCompleted();
          return;
        }

        LoginManager manager = LoginManager.getInstance();
        manager.logInWithReadPermissions(activity, WatchaPermission.DEFAULT.getPermissions());
        manager.registerCallback(getInstance().mCallback, new FacebookCallback<LoginResult>() {
          @Override public void onSuccess(LoginResult loginResult) {
            subscriber.onNext(loginResult.getAccessToken());
            subscriber.onCompleted();
          }

          @Override public void onCancel() {
            subscriber.onError(null);
            subscriber.onCompleted();
          }

          @Override public void onError(FacebookException e) {
            e.printStackTrace();
          }
        });
      }
    });
  }

  /**
   * 페이스북 유저의 프로필을 가져옴.
   * 세션이 없으면, 알아서 척척 연결하고 프로필을 줌.
   *
   * @param activity 세션을 얻기 위해서 필요한 context.
   */
  public Observable<FacebookProfile> profileObservable(Activity activity) {
    return accessTokenObservable(activity).flatMap(
        new Func1<AccessToken, Observable<FacebookProfile>>() {
          @Override public Observable<FacebookProfile> call(final AccessToken accessToken) {
            return Observable.create(new Observable.OnSubscribe<FacebookProfile>() {
              @Override public void call(final Subscriber<? super FacebookProfile> subscriber) {
                Profile.fetchProfileForCurrentAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                      @Override public void onCompleted(JSONObject object, GraphResponse response) {
                        FacebookProfile p = WatchaApp.getInstance()
                            .getGson()
                            .fromJson(response.getRawResponse(), FacebookProfile.class);
                        subscriber.onNext(p);
                        subscriber.onCompleted();
                      }
                    });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,cover,picture,email");
                request.setParameters(parameters);
                request.executeAsync();
              }
            });
          }
        });
  }

  public Observable<ShareLinkContent> publish(@NonNull final Activity activity,
      @Nullable final String url) {
    return canPublishAccessToken(activity).flatMap(
        new Func1<AccessToken, Observable<ShareLinkContent>>() {
          @Override public Observable<ShareLinkContent> call(AccessToken accessToken) {
            ShareLinkContent linkContent =
                new ShareLinkContent.Builder().setContentUrl(Uri.parse(url)).build();
            return Observable.just(linkContent);
          }
        });
  }

  public Observable<AccessToken> canPublishAccessToken(@NonNull final Activity activity) {
    return accessTokenObservable(activity).flatMap(
        new Func1<AccessToken, Observable<AccessToken>>() {
          @Override public Observable<AccessToken> call(AccessToken accessToken) {
            if (accessToken != null) {
              boolean canPublish = accessToken.getPermissions()
                  .contains(WatchaPermission.PUBLISH.getPermissions().get(0));
              if (canPublish) {
                return Observable.just(accessToken);
              } else {
                AccessToken.setCurrentAccessToken(accessToken);
                return Observable.create(new Observable.OnSubscribe<AccessToken>() {
                  @Override public void call(final Subscriber<? super AccessToken> subscriber) {
                    LoginManager manager = LoginManager.getInstance();
                    manager.setDefaultAudience(DefaultAudience.EVERYONE);
                    manager.logInWithPublishPermissions(activity,
                        WatchaPermission.PUBLISH.getPermissions());
                    manager.registerCallback(getInstance().mCallback,
                        new FacebookCallback<LoginResult>() {
                          @Override public void onSuccess(LoginResult loginResult) {
                            if (loginResult != null && loginResult.getAccessToken() != null) {
                              AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                              subscriber.onNext(loginResult.getAccessToken());
                            }
                            subscriber.onCompleted();
                          }

                          @Override public void onCancel() {
                            subscriber.onCompleted();
                          }

                          @Override public void onError(FacebookException e) {
                            e.printStackTrace();
                          }
                        });
                  }
                });
              }
            }
            return null;
          }
        });
  }

  public static boolean hasGrantedFBPublishPermission(WatchaPermission watchaPermission) {
    boolean result = true;
    for (String s : watchaPermission.getPermissions()) {
      if (!AccessToken.getCurrentAccessToken().getPermissions().contains(s)) {
        result = false;
        break;
      }
    }
    return result;
  }

  public static void destroyAccessToken() {
    LoginManager.getInstance().logOut();
  }
}