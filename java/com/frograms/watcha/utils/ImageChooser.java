package com.frograms.watcha.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.views.FakeActionBar;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * 기기에서 이미지 불러오는 클래스.
 */
public final class ImageChooser implements ImageChooserListener {

  public interface OnImageChosenListener {
    void onImageChosenCallback(int fileType, Uri bitmapUri, ImageUriParser parser);
  }

  /**
   * ImageChooserManager 를 통해서 이미지를 가져올때 임시로 저장할 파일 경로.
   */
  protected String mImageFilePath;

  /**
   * ImageChooserManager 를 통해서 이미지를 가져올떄 사진찍기와 앨범선택 둘 중 하나의 값.
   */
  protected int mChooserType;

  protected int mFileType = QueryFile.PROFILE_PHOTO;
  /**
   * 이미지를 내부 저장소에서 가져오거나 직접 찍기로 가져오기 위한 라이브러리.
   */
  private static ImageChooserManager mImageChooserManager;

  /**
   * 이미지를 앱으로 가져올때, 이미지 파일의 임시 경로.
   * 이미지 자르기 앱으로 보내거나 받을때 이 uri의 이미지가 바뀜.
   */
  protected static Uri mImageCaptureUri;

  /**
   * 이미지를 서버로 보낼때 사용할 파일.
   * ProfileFragment, TabInfoRidingFragment 에서 사용함.
   */
  protected File mTempImageFile;

  public final static int REQUEST_CROP = 2;

  /**
   * 크롭할때 고정 비율.
   */
  public static final int PROFILE_ASPECT_X = 190;
  public static final int PROFILE_ASPECT_Y = 190;
  public static final int COVER_ASPECT_X = 720;
  public static final int COVER_ASPECT_Y = 250;

  private OnImageChosenListener mImageChosenListener;
  private final BaseActivity mActivity;

  public ImageChooser(BaseActivity activity) {
    this(activity, QueryFile.PROFILE_PHOTO);
  }

  public ImageChooser(BaseActivity activity, @QueryFile.QueryFileKey int fileType) {
    mActivity = activity;
    mFileType = fileType;
    if (mImageChooserManager != null) {
      mImageChooserManager = null;
    }
  }

  public int getFileType() {
    return mFileType;
  }

  public String getImageFilePath() {
    return mImageFilePath;
  }

  public void setImageFilePath(String path) {
    mImageFilePath = path;
  }

  public void requestTakePicture() {
    try {
      mChooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
      reinitializeImageChooser();
      mImageFilePath = mImageChooserManager.choose();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void requestBringPictureFromGallery() {
    try {
      mChooserType = ChooserType.REQUEST_PICK_PICTURE;
      reinitializeImageChooser();
      mImageFilePath = mImageChooserManager.choose();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 이미지를 내부 저장소에서 꺼내올지, 직접 찍을지 유저에게 선택 권한을 주는 다이얼로그를 띄움.
   * index[0] 사진찍기
   * index[1] 앨범에서 선택
   */
  public void showCaptureImageDialog(String title) {
    getCaptureImageDialog(title).show();
  }

  public DialogPlus captureImageDialog = null;

  private DialogPlus getCaptureImageDialog(String title) {
    if (captureImageDialog == null) {
      String[] items = { "사진찍기", "사진첩에서 가져오기" };
      FakeActionBar header = new FakeActionBar(mActivity);
      header.setTitle(title);
      ArrayAdapter<String> simpleAdapter =
          new ArrayAdapter<>(mActivity, R.layout.view_list_item_basic, R.id.text1, items);
      captureImageDialog = new DialogPlus.Builder(mActivity).setHeader(header)
          .setContentHolder(new ListHolder())
          .setAdapter(simpleAdapter)
          .setGravity(DialogPlus.Gravity.BOTTOM)
          .setCancelable(true)
          .setOnItemClickListener((dialogPlus, o, view, which) -> {
            dialogPlus.dismiss();
            switch (which - 1) {
              case 0:
                requestTakePicture();
                break;
              case 1:
                requestBringPictureFromGallery();
                break;
            }
          })
          .create();
    }
    return captureImageDialog;
  }

  private void reinitializeImageChooser() {
    mImageChooserManager =
        new ImageChooserManager(mActivity.getFragment(), mChooserType, "w", true);
    mImageChooserManager.setImageChooserListener(this);
    mImageChooserManager.reinitialize(mImageFilePath);
  }

  /**
   * 가져온 사진을 자를 수 있는 외부 앱으로 보냄.
   */
  protected void doCrop(int aspectX, int aspectY) {
    Intent intent = new Intent("com.android.camera.action.CROP");
    intent.setDataAndType(mImageCaptureUri, "image/*");
    List<ResolveInfo> list = mActivity.getPackageManager().queryIntentActivities(intent, 0);
    if (list != null && !list.isEmpty()) {
      intent.putExtra("return-data", false);
      intent.putExtra("scale", true);
      if (aspectX > 0 && aspectY > 0) {
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        intent.putExtra("outputX", aspectX);
        intent.putExtra("outputY", aspectY);
      }
      intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
      intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
      mActivity.startActivityForResult(intent, REQUEST_CROP);
    }
  }

  private void callback(Uri uri) {
    if (mActivity != null && mActivity.getFragment() != null) {
      if (mActivity.getFragment() instanceof OnImageChosenListener) {
        ((OnImageChosenListener) mActivity.getFragment()).onImageChosenCallback(mFileType, uri,
            new ImageUriParser());
      }
    }
  }

  /**
   * 크롭까지 마친 완성된 이미지를 콜백해줌.
   */
  public void onImageChosenResult(int requestCode, int resultCode, Intent data) {
    if (mImageChooserManager == null) {
      reinitializeImageChooser();
    }
    WLog.i("requestCode: " + requestCode + ", resultOk: " + (resultCode == Activity.RESULT_OK));
    switch (requestCode) {
      case REQUEST_CROP:
        if (resultCode == Activity.RESULT_OK) {
          callback(mImageCaptureUri);
        }
        break;
      case ChooserType.REQUEST_CAPTURE_PICTURE:
      case ChooserType.REQUEST_PICK_PICTURE:
        mImageChooserManager.submit(requestCode, data);
        break;
    }
  }

  @Override public void onImageChosen(final ChosenImage chosenImage) {
    mActivity.runOnUiThread(() -> {
      if (chosenImage != null && !TextUtils.isEmpty(chosenImage.getFilePathOriginal())) {
        Toast.makeText(mActivity, "사진 불러오는 중..", Toast.LENGTH_SHORT).show();
        mTempImageFile = new File((chosenImage.getFilePathOriginal()));
        mImageCaptureUri = Uri.fromFile(mTempImageFile);
        switch (mFileType) {
          case QueryFile.COVER_PHOTO:
            doCrop(COVER_ASPECT_X, COVER_ASPECT_Y);
            break;
          default:
            doCrop(PROFILE_ASPECT_X, PROFILE_ASPECT_Y);
            break;
        }
      } else {
        Toast.makeText(mActivity, "사진을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override public void onError(final String s) {
    if (mActivity == null) return;
    mActivity.runOnUiThread(() -> {
      WLog.i("error: " + s);
      Toast.makeText(mActivity, "사진을 불러오지 못했습니다", Toast.LENGTH_SHORT).show();
    });
  }

  public static class ImageUriParser {
    private ImageUriParser() {
    }

    public Bitmap getBitmapFromUri(Activity activity, Uri bitmapUri) {
      try {
        Bitmap bm = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), bitmapUri);
        if (bm != null) {
          return bm;
        } else {
          return getBitmapFromFile(new File(bitmapUri.getPath()));
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
      return null;
    }

    private Bitmap getBitmapFromFile(File f) {
      BitmapFactory.Options options = new BitmapFactory.Options();
      return BitmapFactory.decodeFile(f.getAbsolutePath(), options);
    }

    public File getFileFromUri(Activity activity, String name, Uri bitmapUri) {
      File filesDir = WatchaApp.getInstance().getFilesDir();
      File imageFile = new File(filesDir, name + ".jpg");

      OutputStream os;
      try {
        os = new FileOutputStream(imageFile);
        getBitmapFromUri(activity, bitmapUri).compress(Bitmap.CompressFormat.JPEG, 100, os);
        os.flush();
        os.close();
        return imageFile;
      } catch (Exception e) {
        e.printStackTrace();
      }
      return null;
    }

    public File getFileFromBitmap(String name, Bitmap bitmap) {
      File filesDir = WatchaApp.getInstance().getFilesDir();
      File imageFile = new File(filesDir, name + ".jpg");

      OutputStream os;
      try {
        os = new FileOutputStream(imageFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        os.flush();
        os.close();
        return imageFile;
      } catch (Exception e) {
        e.printStackTrace();
      }
      return null;
    }
  }
}
