package com.frograms.watcha.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import com.frograms.watcha.R;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ViewUtil {

  /**
   * Visibility 값.
   */
  @Retention(RetentionPolicy.SOURCE) @IntDef(
      flag = true,
      value = { View.VISIBLE, View.INVISIBLE, View.GONE }) public @interface VisibilityValue {
  }

  public static float LDPI = 0.75f;
  public static float MDPI = 1.0f;
  public static float HDPI = 1.5f;
  public static float XHDPI = 2.0f;
  public static float XXHDPI = 3.0f;
  public static float XXXHDPI = 4.0f;

  public static float getDensity(@NonNull Context context) {
    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
    return metrics.density;
  }

  public static int calculateByDensity(@NonNull Context context, int targetPX, float baseDensity) {
    return (int) (targetPX * (getDensity(context) / baseDensity));
  }

  public static int convertHexFrom(int integer) {
    return Integer.valueOf(String.valueOf(integer), 16);
  }

  public static float convertPixelsToDp(Context context, float px) {
    Resources resources = context.getResources();
    DisplayMetrics metrics = resources.getDisplayMetrics();
    return px / (metrics.densityDpi / 160f);
  }

  public static float convertDpToPixel(@NonNull Context context, float dp) {
    Resources resources = context.getResources();
    DisplayMetrics metrics = resources.getDisplayMetrics();
    float px = dp * (metrics.densityDpi / 160f);
    return px;
  }

  public static Animation getFadeInAnimation(@NonNull Context context) {
    return AnimationUtils.loadAnimation(context, R.anim.anim_fade_in);
  }

  public static Animation getFadeOutAnimation(@NonNull Context context) {
    return AnimationUtils.loadAnimation(context, R.anim.anim_fade_out);
  }

  public static void animateFadeIn(View v, int duration, int delay) {
    if (v == null) return;
    v.setAlpha(0.0f);
    v.animate().alpha(1.0f).setDuration(duration).setStartDelay(delay).start();
  }

  public static Animation getScaleAnimation(Context context) {
    Animation scale = AnimationUtils.loadAnimation(context, R.anim.scale);
    scale.setDuration(300);
    return scale;
  }

  /**
   * 키보드 사라지게 함.
   *
   * @param context EditText가 붙어있는 context.
   * @param view 키보드 없앨 EditText.
   */
  public static void hideKeyboard(Context context, EditText view) {
    InputMethodManager imm =
        (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

  /**
   * 뷰의 visibility 속성을 바꿈.
   *
   * 카드뷰같이 반복적으로 뷰를 초기화 해야할 경우에 visibility도 초기화 해야하는데 코스트가 크니까 바꿀 visibility가 아닐때만 바꾸게 함.
   *
   * @param v visibility바꿀 뷰
   * @param visibility 바꿀 visibility.
   */
  public static void setVisibility(View v, @VisibilityValue int visibility) {
    if (v == null) {
      WLog.e("view is not initiated!!");
      return;
    }

    if (v.getVisibility() != visibility) v.setVisibility(visibility);
  }

  /**
   * 젤리빈 Api level 16 아래는 setBackground()가 없어서 버전으로 분기해줘야 함.
   *
   * @param v 배경 바꿀 뷰.
   * @param d 바꿀 배경.
   */
  public static void setBackground(View v, Drawable d) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      v.setBackground(d);
    } else {
      v.setBackgroundDrawable(d);
    }
  }

  public ImageView setFilterForTouch(ImageView imageView) {
    imageView.setOnTouchListener(new View.OnTouchListener() {
      @Override public boolean onTouch(View v, MotionEvent event) {
        ImageView view = (ImageView) v;
        switch (event.getAction()) {
          case MotionEvent.ACTION_DOWN:
          case MotionEvent.ACTION_MOVE:
            if (view.getDrawable() != null) {
              view.getDrawable().setColorFilter(0x5F000000, PorterDuff.Mode.SRC_ATOP);
              view.invalidate();
            }
            break;
          case MotionEvent.ACTION_UP:
          default:
            if (view.getDrawable() != null) {
              view.getDrawable().clearColorFilter();
              view.invalidate();
            }
            break;
        }
        return false;
      }
    });

    return imageView;
  }
}
