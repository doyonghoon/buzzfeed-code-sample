package com.frograms.watcha.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.models.ShareApp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by doyonghoon on 15. 3. 3..
 */
public class ShareAppLoader {

  /**
   * 정렬이 필요한 앱들
   */
  public static final String PACKAGE_FACEBOOK = "com.facebook.katana";
  public static final String PACKAGE_TWITTER = "com.twitter.android";
  public static final String PACKAGE_KAKAO_TALK = "com.kakao.talk";
  private static final String PACKAGE_KAKAO_STORY = "com.kakao.story";
  private static final String PACKAGE_BAND = "com.nhn.android.band";
  private static final String PACKAGE_FACEBOOK_MESSENGER = "com.facebook.orca";
  private static final String PACKAGE_BETWEEN = "kr.co.vcnc.android.couple";
  private static final String PACKAGE_COPY = "com.google.android.apps.docs";
  private static final String PACKAGE_LINE = "jp.naver.line.android";
  private static final String PACKAGE_TUMBLR = "com.tumblr";
  private static final String PACKAGE_MYPEOPLE = "net.daum.android.air";

  /**
   * 아래 앱 순서로 리스트가 보여짐
   */
  private static final String[] mAppSortOrders = {
      PACKAGE_FACEBOOK, PACKAGE_TWITTER, PACKAGE_KAKAO_TALK, PACKAGE_KAKAO_STORY, PACKAGE_BAND,
      PACKAGE_FACEBOOK_MESSENGER, PACKAGE_BETWEEN, PACKAGE_COPY, PACKAGE_LINE, PACKAGE_TUMBLR,
      PACKAGE_MYPEOPLE
  };

  /**
   * 일단 눈으로 확인하기 전에 앱 순위는 무조건 꼴등으로 설정
   */
  public static final int LAST_ORDER_PRIORITY = mAppSortOrders.length;

  private Context mContext;

  public ShareAppLoader(Context context) {
    mContext = context;
  }

  public List<ShareApp> loadApps() {
    List<ShareApp> apps = new ArrayList<>();
    Intent share = new Intent(android.content.Intent.ACTION_SEND);
    share.setType("text/plain");
    List<ResolveInfo> resInfos = mContext.getPackageManager().queryIntentActivities(share, 0);
    if (resInfos != null) {
      for (ResolveInfo info : resInfos) {
        ShareApp app = new ShareApp(LAST_ORDER_PRIORITY);
        app.setIconId(info.icon);
        app.setIconDrawable(info.activityInfo.loadIcon(mContext.getPackageManager()));
        app.setName(info.activityInfo.loadLabel(mContext.getPackageManager()).toString());
        app.setPackageName(info.activityInfo.packageName);
        for (int i = 0; i < mAppSortOrders.length; i++) {
          if (info.activityInfo.packageName.equals(mAppSortOrders[i])) {
            app.setOrderPriority(i);
            break;
          }
        }
        apps.add(app);
      }
    }
    orderAppsByWonzeeDecision(apps);
    return apps;
  }

  private void orderAppsByWonzeeDecision(List<ShareApp> apps) {
    Collections.sort(apps, (a1, a2) -> a1.getOrderPriority() - a2.getOrderPriority());
  }

  public void goChosenApp(String message, ShareApp selectedApp) {
    if (!TextUtils.isEmpty(message)) {
      @AnalyticsEvent.AnalyticsEventLabel final String selectedAppName = selectedApp.getName();
      AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL, AnalyticsActionType.SHARE_TO)
          .setLabel(selectedAppName)
          .build());
      mContext.startActivity(Intent.createChooser(selectedApp.getShareIntent(message),
          mContext.getString(R.string.do_share)));
    }
  }
}
