package com.frograms.watcha.utils;

import android.text.TextUtils;

/**
 * youtube id 로 웹에서 영상 이미지를 가져옴.
 */
public class YoutubeThumbnailUtils {

  public enum Quality {
    DEFAULT("default"),
    HQ_DEFAULT("hqdefault"),
    MQ_DEFAULT("mqdefault"),
    SD_DEFAULT("sddefault"),
    MAX_RES_DEFAULT("maxresdefault");

    private final String mPath;

    Quality(String path) {
      mPath = path;
    }

    private String getPath() {
      return mPath;
    }
  }

  public static String getUrl(String youtubeId, Quality quality) {
    if (!TextUtils.isEmpty(youtubeId) && quality != null) {
      return buildYoutubeUrl(youtubeId, quality.getPath());
    }
    return null;
  }

  private static String buildYoutubeUrl(String youtubeId, String qualityPath) {
    return String.format("http://img.youtube.com/vi/%s/%s.jpg", youtubeId, qualityPath);
  }
}
