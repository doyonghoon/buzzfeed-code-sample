package com.frograms.watcha.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.models.FacebookProfile;
import com.frograms.watcha.retrofit.QueryFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by doyonghoon on 15. 6. 2..
 */
public class FacebookProfileUpdateHelper {

  private final Builder builder;

  public FacebookProfileUpdateHelper(final Builder builder) {
    this.builder = builder;
  }

  public void load() {
    FacebookBroker.getInstance().profileObservable(builder.mActivity).doOnSubscribe(new Action0() {
      @Override public void call() {
        if (builder.mDialog != null) {
          builder.mDialog.show();
        }
      }
    })
        //.filter(getProfileFilter(builder.mType))
        .flatMap(new Func1<FacebookProfile, Observable<String>>() {
          @Override public Observable<String> call(FacebookProfile facebookProfile) {
            switch (builder.mType) {
              case QueryFile.PROFILE_PHOTO:
                return Observable.just(facebookProfile.getPictureUrl());
              case QueryFile.COVER_PHOTO:
                return Observable.just(facebookProfile.getCoverUrl());
            }
            return null;
          }
        }).subscribe(new Action1<String>() {
      @Override public void call(String url) {
        Glide.with(builder.mActivity).load(url).asBitmap().into(new SimpleTarget<Bitmap>() {
          @Override public void onResourceReady(Bitmap resource,
              GlideAnimation<? super Bitmap> glideAnimation) {
            if (builder.mDialog != null && builder.mDialog.isShowing()) {
              builder.mDialog.dismiss();
            }

            File filesDir = WatchaApp.getInstance().getFilesDir();
            File imageFile = new File(filesDir, "photo_" + System.currentTimeMillis() + ".jpg");

            OutputStream os;
            try {
              os = new FileOutputStream(imageFile);
              resource.compress(Bitmap.CompressFormat.JPEG, 95, os);
              os.flush();
              os.close();
              switch (builder.mType) {
                case QueryFile.COVER_PHOTO:
                  SettingHelper.uploadCover(builder.mActivity,
                      new QueryFile(builder.mType, imageFile), builder.mListener);
                  break;
                case QueryFile.PROFILE_PHOTO:
                  SettingHelper.uploadPhoto(builder.mActivity,
                      new QueryFile(builder.mType, imageFile), builder.mListener);
                  break;
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
          }

          @Override public void onLoadFailed(Exception e, Drawable errorDrawable) {
            if (builder.mDialog != null && builder.mDialog.isShowing()) {
              builder.mDialog.dismiss();
            }

            if (builder.mActivity != null) {
              int resId = R.string.no_fb_profile;
              switch (builder.mType) {
                case QueryFile.COVER_PHOTO:
                  resId = R.string.no_fb_cover;
                  break;
                case QueryFile.PROFILE_PHOTO:
                  resId = R.string.no_fb_profile;
                  break;
              }
              getRequest().clear();

              Toast.makeText(builder.mActivity, resId, Toast.LENGTH_SHORT).show();
            }
          }
        });
      }

    }, new Action1<Throwable>(){ //onError
      @Override public void call(Throwable e){
        WLog.d("Jiho OnError");
        if (builder.mDialog != null && builder.mDialog.isShowing()) {
          builder.mDialog.dismiss();
        }
      }
    });
  }

  private Func1<FacebookProfile, Boolean> getProfileFilter(@QueryFile.QueryFileKey final int type) {
    return new Func1<FacebookProfile, Boolean>() {
      @Override public Boolean call(FacebookProfile facebookProfile) {
        if (facebookProfile != null) {
          switch (type) {
            case QueryFile.COVER_PHOTO:
              return !TextUtils.isEmpty(facebookProfile.getCoverUrl());
            case QueryFile.PROFILE_PHOTO:
              return !TextUtils.isEmpty(facebookProfile.getPictureUrl());
          }
        }
        return false;
      }
    };
  }

  public static class Builder {

    final Activity mActivity;
    @QueryFile.QueryFileKey int mType;
    MaterialDialog mDialog;
    SettingHelper.OnSettingResponseListener mListener;

    public Builder(Activity activity, @QueryFile.QueryFileKey int type) {
      mActivity = activity;
      setQueryFileType(type);
    }

    public Builder setDialog(MaterialDialog dialog) {
      mDialog = dialog;
      return this;
    }

    public Builder setQueryFileType(@QueryFile.QueryFileKey int type) {
      mType = type;
      return this;
    }

    public Builder setSettingResponseListener(SettingHelper.OnSettingResponseListener listener) {
      mListener = listener;
      return this;
    }

    public FacebookProfileUpdateHelper build() {
      return new FacebookProfileUpdateHelper(this);
    }
  }
}
