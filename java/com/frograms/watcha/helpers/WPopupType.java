package com.frograms.watcha.helpers;

import com.frograms.watcha.R;
import com.frograms.watcha.view.WUserActionPopupViewLayout;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link WPopupRateManager} 가 다이얼로그를 띄울때 포함시킬 메뉴버튼들을 화면단위/용도단위로 정리하기 위해서 사용함.
 *
 * 개영페 평가하기, 개영페 더보기, 카드 더보기 등 사용 용도에 맞게 보여줄 메뉴가 다른데 이것들을 enum 으로 묶어서 관리함.
 */
public enum WPopupType {
  // 개영페 평가하기
  DETAIL_RATING(R.id.wpopup_ratingbar),
  // 개영페 더보기
  DETAIL_MORE(R.id.wpopup_add_collection, R.id.wpopup_meh),
  // 걍 대부분의 카드 아이템 ;;
  CARD_ITEM(R.id.wpopup_wish, R.id.wpopup_add_collection, R.id.wpopup_meh),
  // 인기
  CARD_ITEM_POPULAR(R.id.wpopup_ratingbar, R.id.wpopup_wish, R.id.wpopup_add_collection,
      R.id.wpopup_comment, R.id.wpopup_meh),
  // 검색결과
  CARD_ITEM_DISCOVERY(R.id.wpopup_ratingbar, R.id.wpopup_wish, R.id.wpopup_add_collection,
      R.id.wpopup_comment, R.id.wpopup_meh),
  // 코멘트 쓰기
  WRITE_COMMENT(R.id.wpopup_ratingbar, R.id.wpopup_wish, R.id.wpopup_meh),
  // 영평늘
  CARD_ITEM_RATABLE(R.id.wpopup_wish, R.id.wpopup_add_collection, R.id.wpopup_comment,
      R.id.wpopup_meh);

  private List<Integer> mIds = new ArrayList<>();

  /**
   * enum 을 생성할때 보여줄 메뉴 id 들을 추가해야 뷰가 보임.
   * {@link WUserActionPopupViewLayout} 에 사용된 레이아웃 id 들을 참고하면 됨.
   */
  WPopupType(int... ids) {
    if (ids != null && ids.length > 0) {
      for (int i : ids) {
        mIds.add(i);
      }
    }
  }

  public List<Integer> getViewIds() {
    return mIds;
  }
}
