package com.frograms.watcha.helpers;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.TutorialFragment;
import com.frograms.watcha.fragment.WriteCommentFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.RatingUtils;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.utils.WatchaPermission;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;

/**
 * Created by doyonghoon on 15. 6. 8..
 */
public class UserActionOperator {

  public enum ActionType {
    RATING("rate"),
    RATING_CANCEL("rate"),
    WISH("wish"),
    WISH_CANCEL("wish"),
    MEH("meh"),
    MEH_CANCEL("meh");

    private String mType;

    ActionType(String type) {
      mType = type;
    }

    public String getType() {
      return mType;
    }
  }

  private final Builder mBuilder;

  private UserActionOperator(@NonNull Builder builder) {
    mBuilder = builder;
    UserAction defaultUserAction = CacheManager.getMyUserAction(mBuilder.mContentCode);
    mBuilder.mListener.setDefaultUserAction(mBuilder.mCategoryType.getApiPath(), defaultUserAction);
  }

  public void request(Action1<RatingData> ratingDataAction1) {
    requestUserAction().subscribe(ratingDataAction1);
  }

  private Observable<RatingData> requestUserAction() {
    return getQueryType(mBuilder.mActionType, mBuilder.mCategoryType, mBuilder.mContentCode)
        .flatMap(queryType -> {
          if (mBuilder.mActionType == ActionType.RATING) {
            return doBeforeRequestRating(queryType, mBuilder.mContentCode);
          } else if (mBuilder.mActionType == ActionType.WISH) {
            return doBeforeRequestWish(queryType);
          } else {
            return Observable.just(queryType);
          }
        }).flatMap(this::getQueryTypeBasedOnReleasedAt)
        .flatMap(this::getCancelQueryType).doOnNext(new Action1<QueryType>() {
          @Nullable
          private AnalyticsActionType getAnalyticsActionType() {
            switch (mBuilder.mActionType) {
              case WISH:
                return AnalyticsActionType.WISH;
              case MEH:
                return AnalyticsActionType.MEH;
              case RATING:
                return AnalyticsActionType.RATE;
            }
            return null;
          }
          @Override public void call(QueryType queryType) {
            AnalyticsActionType actionType = getAnalyticsActionType();
            if (actionType != null) {
              AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.USER_ACTION, actionType)
                  .build());
            }
          }
        }).flatMap(queryType -> Observable.create(new Observable.OnSubscribe<RatingData>() {
          @Override public void call(final Subscriber<? super RatingData> subscriber) {
            DataProvider dataProvider =
                new DataProvider<BaseResponse<RatingData>>(mBuilder.mActivity, queryType,
                    mBuilder.createReferrerBuilder()).responseTo((queryType1, result) -> {
                  mBuilder.mListener.onSuccess(queryType1, result);
                  if (result.getData() != null) {
                    subscriber.onNext(result.getData());
                    subscriber.onCompleted();
                  } else {
                    subscriber.onError(new Throwable(
                        "RatingData 가 null 임.. 파싱이 잘못 됐을 수도 있고 진짜 데이터가 null 일 수도 있음."));
                  }
                });
            Map<String, String> requestParams = new HashMap<>();
            if (mBuilder.mParams != null) {
              requestParams.putAll(mBuilder.mParams);
            }
            dataProvider.withParams(requestParams);
            dataProvider.request();
          }
        }));
  }

  private Observable<QueryType> getQueryType(final ActionType actionType,
      final CategoryType categoryType, final String contentCode) {
    return Observable.create(new Observable.OnSubscribe<QueryType>() {
      @Override public void call(Subscriber<? super QueryType> subscriber) {
        final QueryType query = getBaseQueryType(actionType);
        if (query != null) {
          query.setApi(categoryType.getApiPath(), contentCode, actionType.getType());
          subscriber.onNext(query);
          subscriber.onCompleted();
        } else {
          subscriber.onError(new Throwable("QueryType 이 null 임.."));
        }
      }
    });
  }

  private Observable<QueryType> doBeforeRequestWish(final QueryType queryType) {
    return Observable.create(new Observable.OnSubscribe<QueryType>() {
      @Override public void call(Subscriber<? super QueryType> subscriber) {

        int count = PrefHelper.getCount(mBuilder.mActivity, PrefHelper.PrefType.COUNT,
            PrefHelper.PrefKey.WISH);
        if (count <= 10) {
          PrefHelper.addCount(mBuilder.mActivity, PrefHelper.PrefType.COUNT,
              PrefHelper.PrefKey.WISH);
        }
        switch (count) {
          case 2:
            AlertHelper.showAlert(mBuilder.mActivity, AlertHelper.AlertType.PLAY_RATING);
            break;
          case 9:
            AlertHelper.showAlert(mBuilder.mActivity, AlertHelper.AlertType.INVITE);
            break;
          default:
            break;
        }
        subscriber.onNext(queryType);
        subscriber.onCompleted();
      }
    });
  }

  private Observable<QueryType> doBeforeRequestRating(final QueryType queryType,
      final String contentCode) {
    return Observable.create(new Observable.OnSubscribe<QueryType>() {
      @Override public void call(Subscriber<? super QueryType> subscriber) {

        //페북 연동 이후 첫 평가일 때(튜토리얼 제외) 페북 타임라인 publish 권한이 없다면
        WLog.e("abc");
        if (!(((BaseActivity) mBuilder.mActivity).getFragment() instanceof TutorialFragment) &&
            WatchaApp.getUser().isFbConnected() &&
            mBuilder.mActionType.equals(ActionType.RATING) &&
            !FacebookBroker.hasGrantedFBPublishPermission(WatchaPermission.PUBLISH) &&
            PrefHelper.getBoolean(mBuilder.mActivity, PrefHelper.PrefType.FIRST_VISIT,
                PrefHelper.PrefKey.FIRST_RATING_AFTER_FB_CONNECT.toString(), true)) {
          //페북 타임라인 publish를 위한 alert를 띄운다
          WLog.e("def");
          AlertHelper.showAlert(mBuilder.mActivity, AlertHelper.AlertType.FB_PUBLISH_PERMISSION);
          PrefHelper.setBoolean(mBuilder.mActivity, PrefHelper.PrefType.FIRST_VISIT,
              PrefHelper.PrefKey.FIRST_RATING_AFTER_FB_CONNECT.toString(), false);
        } else {
          if (!(((BaseActivity) mBuilder.mActivity).getFragment() instanceof WriteCommentFragment)
              && mBuilder.mParams.containsKey("value")
              && Float.valueOf(mBuilder.mParams.get("value")) == 5.0f
              && canShowAlert(mBuilder.mActivity)) {
            AlertHelper.showAlert(mBuilder.mActivity,
                AlertHelper.AlertType.WRITE_COMMENT.setContentCode(contentCode));
          }
        }

        subscriber.onNext(queryType);
        subscriber.onCompleted();
      }
    });
  }

  private Observable<QueryType> getQueryTypeBasedOnReleasedAt(final QueryType queryType) {
    return Observable.create(new Observable.OnSubscribe<QueryType>() {

      private void callback(ActionType type, final Subscriber<? super QueryType> subscriber) {
        mBuilder.mParams.clear();
        getQueryType(type, mBuilder.mCategoryType, mBuilder.mContentCode).subscribe(queryType1 -> {
          subscriber.onNext(queryType1);
          subscriber.onCompleted();
        });
      }

      @Override public void call(final Subscriber<? super QueryType> subscriber) {
        if (mBuilder.mActionType == ActionType.RATING
            && mBuilder.mParams.containsKey("value")
            && mBuilder.mReleasedAt != null
            && mBuilder.mReleasedAt.compareTo(new Date(System.currentTimeMillis())) > 0) {
          String value = RatingUtils.getRatingNumber(mBuilder.mParams.get("value"));
          MaterialDialog.Builder builder = new MaterialDialog.Builder(mBuilder.mActivity);
          builder.theme(Theme.LIGHT);
          builder.typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
              FontHelper.FontType.ROBOTO_REGULAR.getTypeface());
          if (NumberUtils.isNumber(value)) {
            final float ratingValue = NumberUtils.toFloat(value);
            builder.title("아직 세상에 나오지 않은 작품이예요");
            builder.content(String.format("정말 감상하신 작품이 맞나요?:)\n" + "아니면 %s 해주세요!",
                ratingValue >= 3.0 ? "보고싶어요" : "관심없어요"));
            builder.positiveText("정말 본 작품이에요");
            builder.negativeText(ratingValue >= 3.0 ? "보고싶어요 하기" : "관심없어요 하기");
            builder.callback(new MaterialDialog.ButtonCallback() {
              @Override public void onNegative(MaterialDialog dialog) {
                super.onNegative(dialog);
                dialog.dismiss();
                callback(ratingValue >= 3.0 ? ActionType.WISH : ActionType.MEH, subscriber);
              }

              @Override public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                subscriber.onNext(queryType);
                subscriber.onCompleted();
              }
            });
            builder.show();
          } else {
            subscriber.onNext(queryType);
            subscriber.onCompleted();
          }
        } else {
          subscriber.onNext(queryType);
          subscriber.onCompleted();
        }
      }
    });
  }

  private Observable<QueryType> getCancelQueryType(final QueryType queryType) {
    return Observable.create(new Observable.OnSubscribe<QueryType>() {
      @Override public void call(final Subscriber<? super QueryType> subscriber) {
        switch (queryType) {
          case USER_ACTION_CANCEL:
            UserAction defaultUserAction = CacheManager.getMyUserAction(mBuilder.mContentCode);
            if (defaultUserAction != null && defaultUserAction.getComment() != null) {
              MaterialDialog.Builder builder = new MaterialDialog.Builder(mBuilder.mActivity);
              builder.theme(Theme.LIGHT);
              builder.typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
                  FontHelper.FontType.ROBOTO_REGULAR.getTypeface());
              builder.content(R.string.delete_comment_by_cancelinng_rating);
              builder.positiveText(R.string.yes);
              builder.negativeText(R.string.no);
              builder.callback(new MaterialDialog.ButtonCallback() {
                @Override public void onNegative(MaterialDialog dialog) {
                  super.onNegative(dialog);
                  dialog.dismiss();
                }

                @Override public void onPositive(MaterialDialog dialog) {
                  super.onPositive(dialog);
                  subscriber.onNext(queryType);
                  subscriber.onCompleted();
                }
              });
              builder.show();
            } else {
              subscriber.onNext(queryType);
              subscriber.onCompleted();
            }
            break;
          default:
            subscriber.onNext(queryType);
            subscriber.onCompleted();
            break;
        }
      }
    });
  }

  private QueryType getBaseQueryType(ActionType actionType) {
    switch (actionType) {
      case RATING:
      case WISH:
      case MEH:
        return QueryType.USER_ACTION;
      case RATING_CANCEL:
      case WISH_CANCEL:
      case MEH_CANCEL:
        return QueryType.USER_ACTION_CANCEL;
      default:
        return null;
    }
  }

  public static class Builder {

    private final Activity mActivity;
    private final ActionType mActionType;
    private String mContentCode;
    private CategoryType mCategoryType;
    private Date mReleasedAt;
    private Map<String, String> mParams = new HashMap<>();
    private UserActionHelper.OnBaseRatingResponseListener mListener;

    public Builder(@NonNull Activity activity, @NonNull ActionType actionType) {
      mActivity = activity;
      mActionType = actionType;
    }

    public Builder setContentCode(String contentCode) {
      mContentCode = contentCode;
      return this;
    }

    public Builder setReleasedAt(@Nullable Date releasedAt) {
      mReleasedAt = releasedAt;
      return this;
    }

    public Builder setParams(@NonNull Map<String, String> params) {
      mParams.putAll(params);
      return this;
    }

    public Builder setCategoryType(CategoryType categoryType) {
      mCategoryType = categoryType;
      return this;
    }

    public Builder setRatingResponseListener(
        UserActionHelper.OnBaseRatingResponseListener listener) {
      mListener = listener;
      return this;
    }

    private ReferrerBuilder createReferrerBuilder() {
      if (mActivity instanceof BaseActivity) {
        BaseFragment baseFragment = ((BaseActivity) mActivity).getFragment();
        return new ReferrerBuilder(baseFragment.getCurrentScreenName());
      }
      return null;
    }

    public UserActionOperator build() {
      return new UserActionOperator(this);
    }
  }

  private static boolean canShowAlert(@NonNull Activity activity) {
    if (activity instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) activity;
      return !(baseActivity.getFragment() instanceof TutorialFragment);
    }
    return false;
  }
}
