package com.frograms.watcha.helpers;

import android.app.Activity;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import java.util.HashMap;
import java.util.Map;

public class CommentActionHelper {

  public enum ActionType {
    DELETE("delete"),
    LIKE("like"),
    IMPROPER("improper_caution"),
    SPOILER("spoiler_caution");
    private String type;

    ActionType(String type) {
      this.type = type;
    }

    public String getType() {
      return type;
    }
  }

  private static ActionType mActionType;

  public static QueryType makeQueryType(ActionType type, boolean isRating, String code) {
    QueryType queryType;

    mActionType = type;
    switch (type) {
      case DELETE:
        queryType = QueryType.DELETE_COMMENT;
        queryType.setApi(code);
        break;

      default:
        if (isRating) {
          queryType = QueryType.COMMENT_ACTION;
        } else {
          queryType = QueryType.COMMENT_ACTION_CANCEL;
        }

        queryType.setApi(code, type.getType());
        break;
    }

    return queryType;
  }

  public static void requestComment(Activity activity, QueryType queryType,
      Map<String, String> params, ApiResponseListener<BaseResponse<CommentReportData>> listener) {
    if (mActionType != null && mActionType.equals(ActionType.LIKE)) {
      int count = PrefHelper.getCount(activity, PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.LIKE);

      if (count <= 15) {
        PrefHelper.addCount(activity, PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.LIKE);
      }

      if (count == 5) {
        AlertHelper.showAlert(activity, AlertHelper.AlertType.PLAY_RATING);
      } else if (count == 15) {
        AlertHelper.showAlert(activity, AlertHelper.AlertType.INVITE);
      }
    }

    ReferrerBuilder refererBuilder = createReferrerBuilder(activity);
    DataProvider mDataProvider =
        new DataProvider<BaseResponse<CommentReportData>>(activity, queryType, refererBuilder)
            .responseTo(listener);

    Map<String, String> requestParams = new HashMap<>();
    if (params != null) {
      requestParams.putAll(params);
    }

    mDataProvider.withParams(requestParams);
    mDataProvider.request();
  }

  private static ReferrerBuilder createReferrerBuilder(Activity activity) {
    if (activity != null && activity instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) activity).getFragment();
      if (baseFragment instanceof AbsPagerFragment) {
        return new ReferrerBuilder(((AbsPagerFragment) baseFragment).getCurrentFragment().getCurrentScreenName());
      } else {
        return new ReferrerBuilder(baseFragment.getCurrentScreenName());
      }
    }
    return null;
  }
}
