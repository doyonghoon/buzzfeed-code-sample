package com.frograms.watcha.helpers;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.viewHolders.ViewHolderMeta;
import java.util.List;

/**
 * 이벤트 처리하는 클래스.
 * 알러트, 스키마, 링크 순서로 실행함.
 */
public class EventOperator {

  private final Context mContext;
  private final Event mEvent;
  private final String mCardItemName;
  private String mPreviousScreenName;

  /**
   * {@link Builder} 를 통해서만 사용할 수 있음.
   */
  private EventOperator(@NonNull Builder builder) {
    mContext = builder.mContext;
    mEvent = builder.mEvent;
    mCardItemName = builder.mCardItemName;
    mPreviousScreenName = builder.mPreviousScreenName;
  }

  public void start() {
    // 띄울 알러트가 있으면 띄움.
    if (mEvent != null && mEvent.getAlert() != null) {
      processAlert(mEvent);
      return;
    }
    if (mEvent != null) {
      processEventActionsByOrder(mEvent);
    }
  }

  /**
   * 알러트를 실행함.
   * 만약 ok, cancel 값 둘 다 없으면, {@link Toast} 만 실행한 후에 {@link #processEventActionsByOrder(Event)} 를
   * 실행함.
   * 반대로 ok 나 cancel 값이 있을 경우엔, ok 버튼을 눌러야 {@link #processEventActionsByOrder(Event)} 를 실행함.
   */
  private void processAlert(final Event event) {
    Event.Alert alert = event.getAlert();
    if (!TextUtils.isEmpty(alert.getMessage())
        && TextUtils.isEmpty(alert.getOk())
        && TextUtils.isEmpty(alert.getCancel())) {
      if (processEventActionsByOrder(event)) //이벤트 processing에 에러가 난 경우에는 토스트 보여주지 않음
        Toast.makeText(mContext, alert.getMessage(), Toast.LENGTH_SHORT).show();
      return;
    }

    MaterialDialog.Builder builder = new MaterialDialog.Builder(mContext);
    builder.theme(Theme.LIGHT);
    builder.typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(),
        FontHelper.FontType.ROBOTO_REGULAR.getTypeface());
    builder.callback(new MaterialDialog.ButtonCallback() {
      @Override public void onPositive(MaterialDialog dialog) {
        super.onPositive(dialog);
        dialog.dismiss();
        processEventActionsByOrder(event);
      }

      @Override public void onNegative(MaterialDialog dialog) {
        super.onNegative(dialog);
        dialog.dismiss();
      }
    });

    if (!TextUtils.isEmpty(alert.getOk())) {
      builder.positiveText(alert.getOk());
    }

    if (!TextUtils.isEmpty(alert.getCancel())) {
      builder.negativeText(alert.getCancel());
    }
    builder.build().show();
  }

  /**
   * 스키마>웹 순서로 Event 를 실행함.
   */
  private boolean processEventActionsByOrder(@NonNull Event event) {
    String schemeUri = event.getScheme();
    Event.Link link = event.getLink();

    // 스키마가 있으면 앱 실행함.
    if (!TextUtils.isEmpty(schemeUri) && Uri.parse(schemeUri).getScheme().contains("watcha")) {
      sendAnalyticsEventIfNeeded();
      ActivityStarter.with(mContext, schemeUri)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_HORIZONTAL)
          .addBundle(new BundleSet.Builder().putPreviousScreenName(mPreviousScreenName)
              .build()
              .getBundle())
          .start();
    } else if (isAppInstalled(schemeUri)) {
      processScheme(schemeUri);
    } else if (link != null) {
    // 웹 링크가 있으면 실행함.
      return processLink(link);
    }
    return true;
  }

  private void sendAnalyticsEventIfNeeded() {
    if (!TextUtils.isEmpty(mCardItemName)) {
      new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD, AnalyticsActionType.ACTION_CARD_EVENT)
          .setLabel(mCardItemName)
          .build();
    }
  }

  /**
   * 스키마를 실행할 앱이 기기에 설치되었는지 확인하고 외부 앱을 실행함.
   *
   * @param schemeString 스키마 형식의 값. (hoppin://)
   */
  private void processScheme(String schemeString) {
    Intent i = getSchemeIntent(schemeString);
    if (i != null) {
      mContext.startActivity(i);
    }
  }

  /**
   * 웹 내부/외부 링크를 구별하고 실행함.
   */
  private boolean processLink(Event.Link link) {
    if (link.isExternal()) {
      // 링크가 외부 브라우저에서 실행되어야 하면 브라우저 실행.
      try {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getUrl()));
        mContext.startActivity(intent);
      }catch(ActivityNotFoundException e){
        WLog.e("Wrong Intent URI " +link.getUrl());
        return false;
      }
    } else {
      // 링크가 왓챠 내에서 처리되어야 하면 웹뷰를 띄움.
      Bundle b = new BundleSet.Builder().putPreviousScreenName(mPreviousScreenName)
          .putUrl(link.getUrl(), null)
          .build()
          .getBundle();
      ActivityStarter.with(mContext, FragmentTask.WEBVIEW)
          .addBundle(b)
          .setAnimationType(ActivityStarter.AnimationType.SLIDE_UP)
          .start();
    }
    return true;
  }

  /**
   * 스키마 string 값을 uri 로 변환해서 Intent 로 넘겨줌.
   *
   * @param schemeString 만약 값이 null 이면, Intent 도 null 임.
   */
  private Intent getSchemeIntent(@Nullable String schemeString) {
    if (!TextUtils.isEmpty(schemeString)) {
      Uri uri = Uri.parse(schemeString);
      return new Intent(Intent.ACTION_VIEW, uri);
    }
    return null;
  }

  /**
   * 주어진 스키마 값을 실행할 수 있는 앱이 기기에 설치 되었는지 확인함.
   *
   * @param schemeString 값이 null 이어도 되는데, 그럼 false 를 반환함.
   */
  private boolean isAppInstalled(@Nullable String schemeString) {
    Intent intent = getSchemeIntent(schemeString);
    if (intent != null) {
      List<ResolveInfo> list = mContext.getPackageManager()
          .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
      return list != null && list.size() > 0;
    }
    return false;
  }

  public static class Builder {

    private final Context mContext;
    private final Event mEvent;
    @AnalyticsEvent.AnalyticsEventLabel private String mCardItemName;

    /**
     * {@link Builder} 를 생성하는 Context 의 화면 이름.
     * */
    private String mPreviousScreenName = null;

    public Builder(@NonNull Context context, @NonNull Event event) {
      mContext = context;
      mEvent = event;
    }

    /**
     * 통계 목적으로 어떤 카드에서 이벤트가 발생하는지 추적해야 함.
     *
     * @param itemView 카드 아이템뷰.
     * */
    public <T extends ItemView> Builder setCardItem(T itemView) {
      ViewHolderMeta m = ViewHolderMeta.getViewHolderMeta(itemView.getClass());
      if (m != null) {
        @AnalyticsEvent.AnalyticsEventLabel String name = m.name();
        mCardItemName = name;
      }
      return this;
    }

    public Builder setPreviousScreenName(String screenName) {
      mPreviousScreenName = screenName;
      return this;
    }

    public EventOperator build() {
      return new EventOperator(this);
    }
  }
}
