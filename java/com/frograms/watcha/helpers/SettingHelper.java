package com.frograms.watcha.helpers;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.UnreadData;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryFile;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.WLog;
import java.util.HashMap;
import java.util.Map;

public class SettingHelper {

  public static class OnSettingResponseListener extends BaseApiResponseListener<BaseResponse<User>> {

    public OnSettingResponseListener(@NonNull Context context) {
      super(context);
    }

    @Override
    public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
      super.onSuccess(queryType, result);
      WatchaApp.getInstance().setUser(result.getData());
    }
  }

  public static void unreadNotices(Activity activity,
      ApiResponseListener<BaseResponse<UnreadData>> listener) {
    DataProvider<BaseResponse<UnreadData>> provider =
        new DataProvider<>(activity, QueryType.UNREAD_NOTICES, createReferrerBuilder(activity));
    provider.responseTo(listener);

    provider.request();
  }

  public static void setting(Activity activity, Map<String, String> params,
      OnSettingResponseListener listener) {

    DataProvider<BaseResponse<User>> provider =
        new DataProvider<>(activity, QueryType.PROFILE_SETTING, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.changing))
        .withParams(params)
        .responseTo(listener);

    provider.request();
  }

  public static void alert(Activity activity, String id) {

    DataProvider<BaseResponse> provider =
        new DataProvider<>(activity, QueryType.ALERT_DISMISS.setApi(id), createReferrerBuilder(activity)).responseTo((queryType, result) -> {});
    provider.request();
  }

  public static void deActivate(Activity activity, OnSettingResponseListener listener) {
    QueryType queryType = QueryType.SETTING.setApi("deactivate");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.changing)).responseTo(listener);
    provider.request();
  }

  public static void resetActions(Activity activity, String password, String content_types,
      String action_types, OnSettingResponseListener listener) {
    Map<String, String> params = new HashMap<>();
    params.put("id", WatchaApp.getUser().getCode());
    params.put("password", password);
    params.put("content_types", content_types);
    params.put("action_types", action_types);

    QueryType queryType = QueryType.SETTING.setApi("reset_actions");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.restoring))
        .withParams(params)
        .responseTo(listener);

    provider.request();
  }

  public static void connectFb(Activity activity, String token,
      OnSettingResponseListener listener) {
    Map<String, String> params = new HashMap<>();
    params.put("token", token);

    WLog.e(token);
    QueryType queryType = QueryType.SETTING.setApi("connections/facebook");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.connecting_fb))
        .withParams(params)
        .responseTo(listener);

    provider.request();
  }

  public static void deleteFb(Activity activity, OnSettingResponseListener listener) {
    QueryType queryType = QueryType.DELETE_SETTING.setApi("connections/facebook");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.removing_fb)).responseTo(listener);

    provider.request();
  }

  public static void connectTwt(Activity activity, String token, String secret,
      OnSettingResponseListener listener) {
    Map<String, String> params = new HashMap<>();
    params.put("token", token);
    params.put("secret", secret);

    QueryType queryType = QueryType.SETTING.setApi("connections/twitter");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.connecting_twt))
        .withParams(params)
        .responseTo(listener);

    provider.request();
  }

  public static void deleteTwt(Activity activity, OnSettingResponseListener listener) {
    QueryType queryType = QueryType.DELETE_SETTING.setApi("connections/twitter");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.removing_twt)).responseTo(listener);

    provider.request();
  }

  public static void uploadCover(Activity activity, QueryFile file,
      OnSettingResponseListener listener) {
    QueryType queryType = QueryType.SETTING.setApi("cover");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.changing_photo)).responseTo(listener);

    provider.addFile(file);

    provider.request();
  }

  public static void deleteCover(Activity activity, OnSettingResponseListener listener) {
    QueryType queryType = QueryType.DELETE_SETTING.setApi("cover");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.changing_photo)).responseTo(listener);

    provider.request();
  }

  public static void uploadPhoto(Activity activity, QueryFile file,
      OnSettingResponseListener listener) {
    QueryType queryType = QueryType.SETTING.setApi("photo");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.changing_photo)).responseTo(listener);

    provider.addFile(file);

    provider.request();
  }

  public static void deletePhoto(Activity activity, OnSettingResponseListener listener) {
    QueryType queryType = QueryType.DELETE_SETTING.setApi("photo");
    DataProvider<BaseResponse<User>> provider = new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
    provider.withDialogMessage(activity.getString(R.string.changing_photo)).responseTo(listener);

    provider.request();
  }

  private static ReferrerBuilder createReferrerBuilder(Activity activity) {
    if (activity != null && activity instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) activity).getFragment();
      if (baseFragment != null) {
        return new ReferrerBuilder(baseFragment.getCurrentScreenName());
      }
    }
    return null;
  }
}
