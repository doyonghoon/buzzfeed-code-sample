package com.frograms.watcha.helpers;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;
import com.frograms.watcha.application.WatchaApp;

public class FontHelper {

  /**
   * assets 폴더에 있는 폰트 파일들을 이름이랑 경로를 같이 묶어서 관리하기 쉽게 함.
   */
  public enum FontType {
    ROBOTO_LIGHT("Roboto-Light.ttf"),
    ROBOTO_THIN("Roboto-Thin.ttf"),
    ROBOTO_REGULAR("Roboto-Regular.ttf"),
    ROBOTO_MEDIUM("Roboto-Medium.ttf"),
    ROBOTO_BOLD("Roboto-Bold.ttf"),
    ROBOTO_BOLD_CONDENSED("Roboto-BoldCondensed.ttf"),
    ROBOTO_CONDENSED("Roboto-Condensed.ttf"),
    FONT_ICON("icon.ttf");

    private String mFontFileName;
    private Typeface mTypeface;

    private Typeface createFont(Context context, String fontName) {
      return Typeface.createFromAsset(context.getAssets(), fontName);
    }

    public String getFontFileName() {
      return mFontFileName;
    }

    public Typeface getTypeface() {
      if (mTypeface == null) {
        mTypeface = createFont(WatchaApp.getInstance().getApplicationContext(), getFontFileName());
      }
      return mTypeface;
    }

    FontType(String fontName) {
      mFontFileName = fontName;
    }
  }

  public static <T extends TextView> T RobotoThin(T v) {
    v.setTypeface(FontType.ROBOTO_THIN.getTypeface());
    return v;
  }

  public static <T extends TextView> T RobotoLight(T v) {
    v.setTypeface(FontType.ROBOTO_LIGHT.getTypeface());
    return v;
  }

  public static <T extends TextView> T RobotoRegular(T v) {
    v.setTypeface(FontType.ROBOTO_REGULAR.getTypeface());
    return v;
  }

  public static <T extends TextView> T RobotoMedium(T v) {
    v.setTypeface(FontType.ROBOTO_MEDIUM.getTypeface());
    return v;
  }

  public static <T extends TextView> T RobotoBoldCondensed(T v) {
    v.setTypeface(FontType.ROBOTO_BOLD_CONDENSED.getTypeface());
    return v;
  }

  public static <T extends TextView> T RobotoBold(T v) {
    v.setTypeface(FontType.ROBOTO_BOLD.getTypeface());
    return v;
  }

  public static <T extends TextView> T RobotoCondensed(T v) {
    v.setTypeface(FontType.ROBOTO_CONDENSED.getTypeface());
    return v;
  }

  public static <T extends TextView> T FontIcon(T v) {
    v.setTypeface(FontType.FONT_ICON.getTypeface());
    return v;
  }

  public static void Bold(TextView v) {
    if (v != null) v.setPaintFlags(v.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);
  }

  public static void Unbold(TextView v) {
    v.setPaintFlags(v.getPaintFlags() & 0xFFFFFFDF);
  }
}
