package com.frograms.watcha.helpers;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.view.WUserActionPopupDialog;
import com.frograms.watcha.view.WUserActionPopupViewLayout;
import com.frograms.watcha.view.intro.RoundCornerDialog;

/**
 * 평가, 보고싶어요, 코멘트, 컬렉션담기, 관심없어요 등의 컨텐츠 반응 피쳐들 관리하는 클래스.
 *
 * 컨텐츠가 보이는 장소마다 (리스트뷰나 개영페 등) 보여지는 기능이 다름.
 * 예를들면, 개영페는 평가하기만 보인다던가 카드뷰의 더보기 버튼을 누르면 모든 메뉴가 다 뜨던가 등등.
 *
 * 유저가 액션을하면 다이얼로그를 호출한 곳으로 잘 콜백해주는게 핵심.
 */
public class WPopupRateManager implements RoundCornerDialog.RoundCornerListener {

  private final Context mContext;
  private final WPopupType mType;
  private String mTitle, mSubtitle;
  private UserAction mUserAction;
  private WUserActionPopupDialog mDialog = null;
  private WUserActionPopupViewLayout mLayout = null;
  private UserActionPopupListener mListener = null;

  private WPopupRateManager(Builder builder) {
    mContext = builder.mContext;
    mType = builder.mType;

    if (!TextUtils.isEmpty(builder.mTitle)) {
      mTitle = builder.mTitle;
    }
    if (!TextUtils.isEmpty(builder.mSubtitle)) {
      mSubtitle = builder.mSubtitle;
    }
    if (builder.mUserAction != null) {
      mUserAction = builder.mUserAction;
    }
    if (builder.mListener != null) {
      mListener = builder.mListener;
    }
    mDialog = new WUserActionPopupDialog(mContext, this);
  }

  public void show() {
    if (mDialog != null) {
      mDialog.show();
    }
  }

  /**
   * 다이얼로그에 보여질 레이아웃.
   */
  @Override public View getView(RoundCornerDialog d) {
    if (mLayout == null) {
      mLayout = new WUserActionPopupViewLayout(mContext, mType, mListener);
    }
    mLayout.setTitles(mTitle, mSubtitle);
    mLayout.setUserAction(mUserAction);
    mLayout.setDialog(d);
    return mLayout;
  }

  public void setUserAction(UserAction userAction) {
    mUserAction = userAction;
    if (mLayout != null) {
      mLayout.setUserAction(userAction);
    }
  }

  public static class Builder {

    private final Context mContext;
    private final WPopupType mType;
    private String mTitle;
    private String mSubtitle;
    private UserAction mUserAction;
    private UserActionPopupListener mListener;

    public Builder(Context context, WPopupType type) {
      mContext = context;
      mType = type;
    }

    public Builder setTitle(String title) {
      mTitle = title;
      return this;
    }

    public Builder setSubtitle(String subtitle) {
      mSubtitle = subtitle;
      return this;
    }

    public Builder setUserAction(UserAction userAction) {
      mUserAction = userAction;
      return this;
    }

    public Builder setUserActionPopupListener(UserActionPopupListener listener) {
      mListener = listener;
      return this;
    }

    public WPopupRateManager build() {
      return new WPopupRateManager(this);
    }
  }
}
