package com.frograms.watcha.helpers;

/**
 * Created by doyonghoon on 15. 6. 18..
 */
public enum AnalyticsCategoryType {
  SOCIAL("Social"),
  CONTENT("Content"),
  USER_ACTION("UserAction"),
  TARGET_CAMPAIGN("TargetCampaign"),
  CARD("Card");

  private String mValue;

  AnalyticsCategoryType(String value) {
    mValue = value;
  }

  public String getValue() {
    return mValue;
  }
}
