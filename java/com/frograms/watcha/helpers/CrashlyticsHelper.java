package com.frograms.watcha.helpers;

import com.crashlytics.android.Crashlytics;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.User;

public class CrashlyticsHelper {

  public static void initialize() {
    //if (!isInitialized() && BuildConfig.REPORT_CRASHES) {
    //       Crashlytics.start(WatchaApp.getInstance());
    //       setUser();
    //   }
  }

  private static boolean isInitialized() {
    //return Crashlytics.getInstance().isInitialized();
    return true;
  }

  public static void setUser() {
    final User user = WatchaApp.getUser();
    if (user == null) return;

    final String email = user.getEmail();
    final String name = user.getName();
    final String code = user.getCode();

    Crashlytics.setUserIdentifier(code);
    Crashlytics.setUserEmail(email);
    Crashlytics.setUserName(name);
  }

  public static void log(int mode, String className, String msg) {
    if (!isInitialized()) initialize();

    Crashlytics.log(mode, className, msg);
  }
}
