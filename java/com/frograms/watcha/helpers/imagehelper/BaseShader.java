package com.frograms.watcha.helpers.imagehelper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import com.frograms.watcha.R;
import com.frograms.watcha.view.widget.WImageType;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.frograms.watcha.view.widget.wImages.WImageAnimationType;

/**
 * Created by doyonghoon on 15. 5. 22..
 */
public abstract class BaseShader {
  private final static int ALPHA_MAX = 255;

  protected int mViewWidth;
  protected int mViewHeight;

  protected int mBorderColor = Color.WHITE;
  protected int mBorderWidth = 0;
  protected float mBorderAlpha = 1f;
  protected boolean mSquare = false;
  protected int mPlaceholderResId = 0, mErrorResId = 0;
  protected RatioType mRatioType = RatioType.NONE;
  protected WImageType mImageType = WImageType.NONE;
  protected WImageAnimationType mAnimationType = WImageAnimationType.SATURATION;

  protected final Paint mBorderPaint;
  protected final Paint mImagePaint;
  protected BitmapShader mShader;
  protected Drawable mDrawable;
  protected final Matrix mMatrix = new Matrix();

  public BaseShader() {
    mBorderPaint = new Paint();
    mBorderPaint.setStyle(Paint.Style.STROKE);
    mBorderPaint.setAntiAlias(true);

    mImagePaint = new Paint();
    mImagePaint.setAntiAlias(true);
  }

  public abstract void draw(Canvas canvas, Paint imagePaint, Paint borderPaint);

  public abstract void reset();

  @SuppressWarnings("UnusedParameters")
  public abstract void calculate(int bitmapWidth, int bitmapHeight, float width, float height,
      float scale, float translateX, float translateY);

  @SuppressWarnings("SameParameterValue")
  protected final int dpToPx(DisplayMetrics displayMetrics, int dp) {
    return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
  }

  public boolean ismSquare() {
    return mSquare;
  }

  public void init(Context context, AttributeSet attrs, int defStyle) {
    if (attrs != null) {
      TypedArray typedArray =
          context.obtainStyledAttributes(attrs, R.styleable.WImageView, defStyle, 0);
      mBorderColor = typedArray.getColor(R.styleable.WImageView_borderColor, mBorderColor);
      mBorderWidth = typedArray.getDimensionPixelSize(R.styleable.WImageView_border, mBorderWidth);
      mBorderAlpha = typedArray.getFloat(R.styleable.WImageView_borderAlpha, mBorderAlpha);
      mSquare = typedArray.getBoolean(R.styleable.WImageView_square, mSquare);
      mPlaceholderResId =
          typedArray.getResourceId(R.styleable.WImageView_placeholder, mPlaceholderResId);
      mErrorResId = typedArray.getResourceId(R.styleable.WImageView_error, mErrorResId);
      mImageType = WImageType.getType(
          typedArray.getInt(R.styleable.WImageView_imageType, mImageType.getAttributeValue()));
      mRatioType = RatioType.getType(
          typedArray.getInt(R.styleable.WImageView_ratioType, mRatioType.getAttributeValue()));
      mAnimationType = WImageAnimationType.getAnimationType(
          typedArray.getInt(R.styleable.WImageView_animation, mAnimationType.getAttributeValue()));
      typedArray.recycle();
    }

    mBorderPaint.setColor(mBorderColor);
    mBorderPaint.setAlpha(Float.valueOf(mBorderAlpha * ALPHA_MAX).intValue());
    mBorderPaint.setStrokeWidth(mBorderWidth);
  }

  public boolean onDraw(Canvas canvas) {
    if (mShader == null) {
      createShader();
    }
    if (mShader != null && mViewWidth > 0 && mViewHeight > 0) {
      draw(canvas, mImagePaint, mBorderPaint);
      return true;
    }

    return false;
  }

  public void onSizeChanged(int width, int height) {
    mViewWidth = width;
    mViewHeight = height;
    if (ismSquare()) {
      mViewWidth = mViewHeight = Math.min(width, height);
    }
    if (mShader != null) {
      calculateDrawableSizes();
    }
  }

  public Bitmap calculateDrawableSizes() {
    Bitmap bitmap = getBitmap();
    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
      int bitmapWidth = bitmap.getWidth();
      int bitmapHeight = bitmap.getHeight();

      float width = Math.round(mViewWidth - 2f * mBorderWidth);
      float height = Math.round(mViewHeight - 2f * mBorderWidth);

      float scale;
      float translateX = 0;
      float translateY = 0;

      if (bitmapWidth * height > width * bitmapHeight) {
        scale = height / bitmapHeight;
        translateX = Math.round((width / scale - bitmapWidth) / 2f);
      } else {
        scale = width / (float) bitmapWidth;
        translateY = Math.round((height / scale - bitmapHeight) / 2f);
      }

      mMatrix.setScale(scale, scale);
      mMatrix.preTranslate(translateX, translateY);
      mMatrix.postTranslate(mBorderWidth, mBorderWidth);

      calculate(bitmapWidth, bitmapHeight, width, height, scale, translateX, translateY);

      return bitmap;
    }

    reset();
    return null;
  }

  public final void onImageDrawableReset(Drawable drawable) {
    mDrawable = drawable;
    mShader = null;
    mImagePaint.setShader(null);
  }

  protected void createShader() {
    Bitmap bitmap = calculateDrawableSizes();
    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
      mShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
      mImagePaint.setShader(mShader);
    }
  }

  protected Bitmap getBitmap() {
    Bitmap bitmap = null;
    if (mDrawable != null) {
      if (mDrawable instanceof BitmapDrawable) {
        bitmap = ((BitmapDrawable) mDrawable).getBitmap();
      }
    }

    return bitmap;
  }
}
