package com.frograms.watcha.helpers.imagehelper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.frograms.watcha.R;

/**
 * Created by doyonghoon on 15. 5. 22..
 */
public class RoundedShader extends BaseShader {

  private final RectF mBorderRect = new RectF();
  private final RectF mImageRect = new RectF();

  private int mRadius = 0;
  private int mBitmapRadius;

  public RoundedShader() {
  }

  @Override public void init(Context context, AttributeSet attrs, int defStyle) {
    super.init(context, attrs, defStyle);
    mBorderPaint.setStrokeWidth(mBorderWidth * 2);
    if (attrs != null) {
      TypedArray typedArray =
          context.obtainStyledAttributes(attrs, R.styleable.WImageView, defStyle, 0);
      if (typedArray.getBoolean(R.styleable.WImageView_radius, false)) {
        mRadius = context.getResources().getDimensionPixelSize(R.dimen.image_radius);
      }
      typedArray.recycle();
    }
  }

  @Override public void draw(Canvas canvas, Paint imagePaint, Paint borderPaint) {
    canvas.drawRoundRect(mBorderRect, mRadius, mRadius, borderPaint);
    canvas.save();
    canvas.concat(mMatrix);
    canvas.drawRoundRect(mImageRect, mBitmapRadius, mBitmapRadius, imagePaint);
    canvas.restore();
  }

  @SuppressWarnings("SuspiciousNameCombination") @Override
  public void onSizeChanged(int width, int height) {
    super.onSizeChanged(width, height);
    mBorderRect.set(mBorderWidth, mBorderWidth, mViewWidth - mBorderWidth,
        mViewHeight - mBorderWidth);
  }

  @Override
  public void calculate(int bitmapWidth, int bitmapHeight, float width, float height, float scale,
      float translateX, float translateY) {
    mImageRect.set(-translateX, -translateY, bitmapWidth + translateX, bitmapHeight + translateY);
    mBitmapRadius = Math.round(mRadius / scale);
  }

  @Override public void reset() {
    mImageRect.set(0, 0, 0, 0);
    mBitmapRadius = 0;
  }
}
