package com.frograms.watcha.helpers.imagehelper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

/**
 * Created by doyonghoon on 15. 5. 22..
 */
public class CircleShader extends BaseShader {
  private float mCenter;
  private float mBitmapCenterX;
  private float mBitmapCenterY;
  private float mBorderRadius;
  private int mBitmapRadius;

  public CircleShader() {
  }

  @Override public void init(Context context, AttributeSet attrs, int defStyle) {
    super.init(context, attrs, defStyle);
    mSquare = true;
  }

  @Override public void draw(Canvas canvas, Paint imagePaint, Paint borderPaint) {
    canvas.drawCircle(mCenter, mCenter, mBorderRadius, borderPaint);
    canvas.save();
    canvas.concat(mMatrix);
    canvas.drawCircle(mBitmapCenterX, mBitmapCenterY, mBitmapRadius, imagePaint);
    canvas.restore();
  }

  @Override public void onSizeChanged(int width, int height) {
    super.onSizeChanged(width, height);
    mCenter = Math.round(mViewWidth / 2f);
    mBorderRadius = Math.round((mViewWidth - mBorderWidth) / 2f);
  }

  @Override
  public void calculate(int bitmapWidth, int bitmapHeight, float width, float height, float scale,
      float translateX, float translateY) {
    mBitmapCenterX = Math.round(bitmapWidth / 2f);
    mBitmapCenterY = Math.round(bitmapHeight / 2f);
    mBitmapRadius = Math.round(width / scale / 2f);
  }

  @Override public void reset() {
    mBitmapRadius = 0;
    mBitmapCenterX = 0;
    mBitmapCenterY = 0;
  }
}
