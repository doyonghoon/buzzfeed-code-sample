package com.frograms.watcha.helpers;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.GridDrawerAdapter;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.ShareMessageData;
import com.frograms.watcha.models.ShareApp;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.FacebookBroker;
import com.frograms.watcha.utils.ShareAppLoader;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.List;

/**
 * Created by Larc21 on 15. 5. 22..
 */
public class AlertHelper {

  public interface OnDismissListener {
    void onDismiss();
  }

  public enum AlertType {
    PLAY_RATING(R.string.alert_rating_title, R.string.alert_rating_msg, R.string.alert_rating_yes,
        R.string.alert_rating_no),
    INVITE(R.string.alert_invite_title, R.string.alert_invite_msg, R.string.alert_invite_yes,
        R.string.alert_invite_no),
    WRITE_COMMENT(R.string.alert_comment_title, R.string.alert_comment_msg,
        R.string.alert_comment_yes, R.string.alert_comment_no),
    SHARE_COLLECTION(R.string.alert_share_title, R.string.alert_share_msg, R.string.alert_share_yes,
        R.string.alert_share_no),
    TASTE(R.string.alert_taste_title, R.string.alert_taste_msg, R.string.alert_taste_yes,
        R.string.alert_taste_no),
    FB_PUBLISH_PERMISSION(R.string.alert_fb_publish_permission_title,
        R.string.alert_fb_publish_permission_msg, R.string.alert_fb_publish_permission_yes,
        R.string.alert_fb_publish_permission_no);

    private int mTitleResId, mMsgResId, mYesResId, mNoResId;
    private String mContentCode, mUserCode;
    private DeckBase mDeck;

    AlertType(int titleResId, int msgResId, int yesResId, int noResId) {
      mTitleResId = titleResId;
      mMsgResId = msgResId;
      mYesResId = yesResId;
      mNoResId = noResId;
    }

    public int getTitleResId() {
      return mTitleResId;
    }

    public int getMsgResId() {
      return mMsgResId;
    }

    public int getYesResId() {
      return mYesResId;
    }

    public int getNoResId() {
      return mNoResId;
    }

    public AlertType setContentCode(String code) {
      mContentCode = code;
      return this;
    }

    public String getContentCode() {
      return mContentCode;
    }

    public AlertType setUserCode(String code) {
      mUserCode = code;
      return this;
    }

    public String getUserCode() {
      return mUserCode;
    }

    public AlertType setDeck(DeckBase deck) {
      mDeck = deck;
      return this;
    }

    public DeckBase getDeck() {
      return mDeck;
    }
  }

  public static AlertDialog.Builder showAlert(final Context context, final AlertType alertType,
      final OnDismissListener listener) {
    if (PrefHelper.getBoolean(context, PrefHelper.PrefType.ALERT, alertType.name())
        || context == null) {
      return null;
    }

    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle(alertType.getTitleResId())
        .setMessage(alertType.getMsgResId())
        .setPositiveButton(alertType.getYesResId(), (dialogInterface, i) -> {
          switch (alertType) {
            case PLAY_RATING:
              ActivityStarter.with(context, context.getString(R.string.market)).start();
              break;

            case INVITE:
              ActivityStarter.with(context, FragmentTask.INVITE_FRIENDS)
                  .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName(context))
                      .build()
                      .getBundle())
                  .start();
              break;

            case WRITE_COMMENT:
              ActivityStarter.with(context, FragmentTask.WRITE_COMMENT)
                  .addBundle(new BundleSet.Builder().putContentCode(alertType.getContentCode()).putPreviousScreenName(getScreenName(context))
                      .build()
                      .getBundle())
                  .start();
              break;

            case SHARE_COLLECTION:
              getShareUrl(context, alertType.getDeck());
              break;

            case TASTE:
              ActivityStarter.with(context, FragmentTask.WEBVIEW)
                  .addBundle(new BundleSet.Builder().putUrl("http://yujun.watcha.net/users/"
                      + alertType.getUserCode()
                      + "/taste", context.getResources().getString(R.string.taste_more)).putPreviousScreenName(getScreenName(context))
                      .build()
                      .getBundle())
                  .start();
              break;

            case FB_PUBLISH_PERMISSION:
              if (context instanceof Activity) {
                FacebookBroker.getInstance()
                    .canPublishAccessToken((Activity) context)
                    .subscribe();
              }
              break;
          }
        })
        .setNegativeButton(alertType.getNoResId(), (dialogInterface, i) -> {
          if (listener != null) {
            listener.onDismiss();
          }
        });

    builder.show();

    PrefHelper.setBoolean(context, PrefHelper.PrefType.ALERT, alertType.name(), true);

    return builder;
  }

  @Nullable private static String getScreenName(@Nullable Context context) {
    if (context != null && context instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) context;
      BaseFragment baseFragment = baseActivity.getFragment();
      if (baseFragment instanceof AbsPagerFragment) {
        return ((AbsPagerFragment) baseFragment).getCurrentFragment().getCurrentScreenName();
      } else {
        return baseFragment.getCurrentScreenName();
      }
    }
    return null;
  }

  public static AlertDialog.Builder showAlert(final Context context, final AlertType alertType) {
    return showAlert(context, alertType, null);
  }

  protected static void getShareUrl(final Context context, final DeckBase mDeck) {
    GridDrawerAdapter adapter = new GridDrawerAdapter(context);
    final ShareAppLoader loader = new ShareAppLoader(context);
    List<ShareApp> apps = loader.loadApps();
    adapter.addAll(apps);
    adapter.notifyDataSetChanged();
    FakeActionBar header = new FakeActionBar(context);
    header.setTitle(context.getResources().getString(R.string.do_share));
    DialogPlus dialog = new DialogPlus.Builder(context).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(adapter)
        .setGravity(DialogPlus.Gravity.BOTTOM)
        .setCancelable(true)
        .setOnItemClickListener((dialog1, item, view, position) -> {
          if (item instanceof ShareApp) {
            dialog1.dismiss();
            final ShareApp app = (ShareApp) item;
            WLog.i("item: " + app.getName());

            BaseApiResponseListener<BaseResponse<ShareMessageData>> listener =
                new BaseApiResponseListener<BaseResponse<ShareMessageData>>(context) {
                  @Override
                  public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<ShareMessageData> result) {
                    super.onSuccess(queryType, result);
                    if (mDeck != null) {
                      ShareMessageData data = result.getData();
                      String message = String.format("%s #왓챠 %s", mDeck.getName(), data.getUrl());
                      if (app.getPackageName().equals(ShareAppLoader.PACKAGE_FACEBOOK)) {
                        message = data.getUrl();
                      }
                      loader.goChosenApp(message, app);
                    }
                  }
                };

            DataProvider<BaseResponse<ShareMessageData>> provider =
                new DataProvider<>((BaseActivity) context, QueryType.DECK_SHARE_MESSAGE.setApi(mDeck
                    .getCode()), createReferrerBuilder(context).appendArgument(mDeck.getCode()));
            provider.withDialogMessage(context.getResources().getString(R.string.loading));
            provider.responseTo(listener);
            provider.request();
          }
        })
        .create();
    dialog.show();
  }

  private static ReferrerBuilder createReferrerBuilder(Context context) {
    if (context != null && context instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) context).getFragment();
      return new ReferrerBuilder(baseFragment.getCurrentScreenName());
    }
    return null;
  }
}
