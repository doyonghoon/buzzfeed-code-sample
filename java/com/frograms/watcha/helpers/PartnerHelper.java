package com.frograms.watcha.helpers;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import java.util.HashMap;
import java.util.Map;

public class PartnerHelper {

  public static class OnBasePartnerResponseListener
      extends BaseApiResponseListener<BaseResponse<User>> {

    public OnBasePartnerResponseListener(@NonNull Context context) {
      super(context);
    }

    @Override
    public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
      super.onSuccess(queryType, result);
      switch (queryType) {
        case SET_FRIEND:
          WatchaApp.getUser().setPartner(result.getData());
          break;

        case DELETE_FRIEND:
          WatchaApp.getUser().deleteMoviePartner();
          break;
      }
    }
  }

  public static class OnBaseFollowResponseListener
      extends BaseApiResponseListener<BaseResponse<User>> {

    public OnBaseFollowResponseListener(@NonNull Context context) {
      super(context);
    }

    @Override
    public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
      super.onSuccess(queryType, result);
      switch (queryType) {
        case SET_FRIEND:
          WatchaApp.getUser().changeFriendsCount(true);
          break;

        case DELETE_FRIEND:
          WatchaApp.getUser().changeFriendsCount(false);
          break;
      }
    }
  }

  public static void setMoviePartner(Activity activity, @NonNull ReferrerBuilder referrerBuilder, String userCode, boolean isPartner, OnBasePartnerResponseListener listener) {
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
        isPartner ? AnalyticsActionType.MOVIE_PARTNER
            : AnalyticsActionType.CANCEL_MOVIE_PARTNER).build());
    QueryType queryType = (isPartner) ? QueryType.PARTNERIZE : QueryType.DELETE_PARTNER;
    Map<String, String> params = new HashMap<>();
    if (isPartner) {
      params.put("id", userCode);
    }
    DataProvider dataProvider =
        new DataProvider<BaseResponse<User>>(activity, queryType, referrerBuilder).withParams(params)
            .responseTo(listener);
    dataProvider.request();
  }

  public static void setFollow(Activity activity, @NonNull ReferrerBuilder referrerBuilder, String userCode, boolean isFollow, OnBaseFollowResponseListener listener) {
    QueryType queryType = (isFollow) ? QueryType.SET_FRIEND : QueryType.DELETE_FRIEND;
    queryType.setApi(userCode, "follow");

    DataProvider dataProvider =
        new DataProvider<BaseResponse<User>>(activity, queryType, referrerBuilder).responseTo(listener);
    dataProvider.request();
  }
}
