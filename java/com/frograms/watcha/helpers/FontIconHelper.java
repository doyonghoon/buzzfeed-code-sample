package com.frograms.watcha.helpers;

import android.content.Context;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

public class FontIconHelper {

  private static FontIconHelper sHelper;

  private FontIconDrawable mDrawable;

  public static FontIconHelper setFontIconDrawable(Context context, int xmlId) {
    if (sHelper == null) {
      synchronized (FontIconHelper.class) {
        if (sHelper == null) {
          sHelper = new FontIconHelper();
        }
      }
    }

    sHelper.mDrawable = FontIconDrawable.inflate(context, xmlId);

    return sHelper;
  }

  public FontIconHelper setSize(float textSize) {
    mDrawable.setTextSize(textSize);

    return this;
  }

  public FontIconHelper setColor(int color) {
    mDrawable.setTextColor(color);

    return this;
  }

  public FontIconDrawable getDrawable() {
    return mDrawable;
  }
}
