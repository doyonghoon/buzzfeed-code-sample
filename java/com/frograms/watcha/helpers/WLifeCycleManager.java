package com.frograms.watcha.helpers;

import android.app.ActivityManager;
import android.content.Context;
import android.os.AsyncTask;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.listeners.LifecycleCallback;
import com.frograms.watcha.utils.WLog;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * 앱이 백그라운드로 가거나 백그라운드에서 실행되는지 알려주는 클래스.
 */
public class WLifeCycleManager {

  private static final String KEY_APP_STATUS = "AppLifeCycleStatus";

  private final Context mContext;
  private LifecycleCallback mCallback = null;

  public WLifeCycleManager(Context context, LifecycleCallback callback) {
    mContext = context;
    setCallback(callback);
  }

  public void setCallback(LifecycleCallback callback) {
    mCallback = callback;
  }

  public boolean isForeground() {
    if (WatchaApp.getInstance() == null || mContext == null) {
      return false;
    }

    try {
      return new ForegroundCheckTask().execute(mContext).get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }

    return false;
  }

  public void onResume() {
    if (WatchaApp.getUser() != null && !getAppStatusForeground() && mCallback != null) {
      WLog.i("왓챠가 백그라운드에서 실행됨. 유저 세션을 다시 확인해볼 필요가 있음.");
      mCallback.onAppBackFromBackground();
    }

    // reset the value.
    setAppStatusForeground(true);
  }

  public void onStop() {
    boolean isForeground = isForeground();
    setAppStatusForeground(isForeground);

    if (!isForeground && mCallback != null) {
      WLog.i("왓챠가 백그라운드로 진입함.");
      mCallback.onAppGoesBackground();
    }
  }

  public void onPause() {
    WatchaApp.getInstance().startActivityTransitionTimer();
    setAppStatusForeground(isForeground());
  }

  /**
   * 앱 foreground, background 확인용 값
   */
  private boolean setAppStatusForeground(boolean value) {
    return mContext.getSharedPreferences(KEY_APP_STATUS, Context.MODE_PRIVATE)
        .edit()
        .putBoolean(KEY_APP_STATUS, value)
        .commit();
  }

  private boolean getAppStatusForeground() {
    return mContext.getSharedPreferences(KEY_APP_STATUS, Context.MODE_PRIVATE)
        .getBoolean(KEY_APP_STATUS, false);
  }

  private static class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {
    @Override protected Boolean doInBackground(Context... params) {
      final Context context = params[0].getApplicationContext();
      return isAppOnForeground(context);
    }

    private boolean isAppOnForeground(Context context) {
      ActivityManager activityManager =
          (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
      List<ActivityManager.RunningAppProcessInfo> appProcesses =
          activityManager.getRunningAppProcesses();
      if (appProcesses == null) {
        return false;
      }
      final String packageName = context.getPackageName();
      for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
        if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
            && appProcess.processName.equals(packageName)) {
          return true;
        }
      }
      return false;
    }
  }
}
