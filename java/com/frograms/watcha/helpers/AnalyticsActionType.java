package com.frograms.watcha.helpers;

/**
 * Created by doyonghoon on 15. 6. 18..
 */
public enum AnalyticsActionType {
  REFRESH("Refresh"), // O
  PAGINATION("Pagination"), // O
  LIKE_DECK("LikeDeck"), // O
  CANCEL_LIKE_DECK("CancelLikeDeck"), // O
  LIKE_COMMENT("LikeComment"), // O
  CANCEL_LIKE_COMMENT("CancelLikeComment"), // O
  SHARE_DECK("ShareDeck"), // O
  SHARE_CONTENT("ShareContent"), // O
  SHARE_TASTE("ShareTaste"), // O
  SHARE_WATCHA("ShareWatcha"),
  SHARE_TO("ShareTo"), // O
  SHARE_PROMOTION("SharePromotion"), // O
  WATCH("Watch"), // O
  FOLLOW("Follow"), // O
  UNFOLLOW("Unfollow"), // O
  PLAY_YOUTUBE("PlayYoutube"), // O
  COMMENT("Comment"), // O
  MEH("Meh"), // O
  RATE("Rate"), // O
  WISH("Wish"), // O
  EXPAND_STORY("ExpandStory"), // O
  CATEGORY_IN_RATE_MORE("CategoryInRateMore"), // O
  SWITCH_FACEBOOK_IN_COMMENT_EDITOR("SwitchFacebookInCommentEditor"), // O
  SWITCH_TWITTER_IN_COMMENT_EDITOR("SwitchTwitterInCommentEditor"), // O
  BUTTON_CALENDAR_IN_COMMENT_EDITOR("ButtonCalendarInCommentEditor"), // O
  BUTTON_PROFILE_PICTURE("ButtonProfilePicture"), // O
  BUTTON_PROFILE_COVER("ButtonProfileCover"), // O
  BUTTON_MY_PROFILE_PICTURE("ButtonMyProfilePicture"), // O
  BUTTON_MY_PROFILE_COVER("ButtonMyProfileCover"), // O
  SORT_IN_COMMENT("SortInComment"), // O
  SORT_IN_WISH("SortInWish"), // O
  SORT_IN_RATE("SortInRate"), // O
  CLICK_AUTOCOMPLETE("ClickAutocompleteText"), // O
  CLICK_LATEST_SEARCH_QUERY("ClickLatestSearchQuery"), // O
  COPY_PASTEBOARD("CopyPasteBoard"), // O
  MOVIE_PARTNER("MoviePartner"), // O
  CANCEL_MOVIE_PARTNER("CancelMoviePartner"), // O
  SORT_SELECTED_MOVIE_TO_CREATE_DECK("SortSelectedMovieToCreateDeck"), // O
  SORT_SELECTED_MOVIE_TO_EDIT_DECK("SortSelectedMovieToEditDeck"), // O
  REMOVE_SELECTED_MOVIE_TO_CREATE_DECK("RemoveSelectedMovieToCreateDeck"), // O
  REMOVE_SELECTED_MOVIE_TO_EDIT_DECK("RemoveSelectedMovieToEditDeck"), // O
  SPOILER_COMMENT("SpoilerComment"), // O
  CANCEL_SPOILER_COMMENT("CancelSpoilerComment"), // O
  IMPROPER_COMMENT("ImproperComment"), // O
  CANCEL_IMPROPER_COMMENT("CancelImproperComment"), // O
  CANCEL_REPORT_COMMENT("CancelImproperComment"), // O
  BUTTON_RESET_USER_ACTIONS("ButtonResetUserActions"), // O
  BUTTON_SELECT_CATEGORY_TO_RESET_USER_ACTIONS("ButtonSelectCategoryToResetUserActions"), // O
  CONFIRM_RESET_USER_ACTIONS("ConfirmSwipeUserActions"), // O
  WATCHA_SNS_LIST("WatchaSNSList"), // O
  BUTTON_EMAIL_FOR_FEEDBACK("ButtonEmailForFeedback"), // O
  SWITCH_CONNECT_FACEBOOK("ConnectFacebook"), // O
  SWITCH_DISCONNECT_FACEBOOK("DisconnectFacebook"), // O
  SWITCH_ON_POSTWALL_FACEBOOK("SwitchOnPostWallFacebook"), // O
  SWITCH_OFF_POSTWALL_FACEBOOK("SwitchOffPostWallFacebook"), // O
  SWITCH_CONNECT_TWITTER("SwitchConnectTwitter"), // O
  SWITCH_DISCONNECT_TWITTER("SwitchDisconnectTwitter"), // O
  BUTTON_INVITE_FRIENDS_FROM_KAKAO("ButtonInviteFriendsFromKakao"),
  BUTTON_LOGOUT("ButtonLogout"), // O
  BUTTON_TERMINATE_ACCOUNT("ButtonTerminateAccount"), // O
  CONFIRM_TERMINIATE_ACCOUNT("ConfirmTerminateAccount"), // O
  ACTION_CARD_EVENT("ActionCardEvent"), // O
  SEND_REPLY("SendReply"), // O
  ;

  private String mValue;

  AnalyticsActionType(String value) {
    mValue = value;
  }

  public String getValue() {
    return mValue;
  }
}
