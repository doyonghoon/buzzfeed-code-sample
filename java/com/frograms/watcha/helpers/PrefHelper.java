package com.frograms.watcha.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefHelper {

  public enum PrefType {
    FIRST_VISIT, COMMENT_SHARE, ALERT, COUNT, VERSION, COACHMARK
  }

  public enum PrefKey {
    FACEBOOK, TWITTER, VISIT, WISH, LIKE, RATING_5, MAKE_COLLECTION, FIRST_RATING_AFTER_FB_CONNECT, PrefKey, LATEST,
    COACHMARK_POPULAR_DRAMA_FILTER, COACHMARK_PRE_RATING, COACHMARK_TASTE_MATCH
  }

  //BOOLEAN
  public static boolean getBoolean(Context context, PrefType type, String key) {
    return getBoolean(context, type, key, false);
  }

  public static boolean getBoolean(Context context, PrefType type, String key, boolean def) {
    SharedPreferences pref = context.getSharedPreferences(type.name(), Context.MODE_PRIVATE);
    return pref.getBoolean(key, def);
  }

  public static void setBoolean(Context context, PrefType type, String key, boolean value) {
    SharedPreferences pref = context.getSharedPreferences(type.name(), Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putBoolean(key, value);
    editor.commit();
  }

  //COUNT
  public static int getCount(Context context, PrefType type, PrefKey key) {
    SharedPreferences pref = context.getSharedPreferences(type.name(), Context.MODE_PRIVATE);
    return pref.getInt(key.name(), 0);
  }

  public static void addCount(Context context, PrefType type, PrefKey key) {
    SharedPreferences pref = context.getSharedPreferences(type.name(), Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putInt(key.name(), pref.getInt(key.name(), 0) + 1);
    editor.commit();
  }

  // STRING
  public static String getString(Context context, PrefType type, String key) {
    SharedPreferences pref = context.getSharedPreferences(type.name(), Context.MODE_PRIVATE);
    return pref.getString(key, null);
  }

  public static void setString(Context context, PrefType type, String key, String value) {
    SharedPreferences pref = context.getSharedPreferences(type.name(), Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(key, value);
    editor.commit();
  }
}
