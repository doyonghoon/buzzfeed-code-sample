package com.frograms.watcha.helpers;

import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.utils.WLog;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * 통계 로그 남길 떄 공통적으로 처리해야할 로직이 있거나 키 값을 모아 놓은 곳.
 */
public class AnalyticsManager {

  private static AnalyticsManager singleton;

  @StringDef({
      NONE, INIT, INTRO, SIGNUP, LOGIN, RESET_PASSWORD, RECOMMENDATION_MOVIE, RECOMMENDATION_DRAMA,
      POPULAR_MOVIE, POPULAR_DRAMA, LEGAL, TASTE, TUTORIAL_MOVIE, TUTORIAL_DRAMA, CREATE_DECK,
      EDIT_DECK, ADD_MOVIE_FROM_CREATE_DECK, ADD_DRAMA_FROM_CREATE_DECK,
      EDIT_MOVIE_FROM_CREATE_DECK, SELECT_MOVIE_FOR_WRITING_COMMENT,
      SELECT_DRAMA_FOR_WRITING_COMMENT, RATE_MORE_MOVIE, RATE_MORE_DRAMA, TAG_SETTING_MOVIE,
      TAG_SETTING_DRAMA, TAG_SETTING_MOVIE_VOD, EXPLORE, SEARCH_RESULT, NOTIFICATION, MY_PROFILE,
      USER_PROFILE, CONTENT_WATCHED_MOVIE, CONTENT_WATCHED_DRAMA, CONTENT_WRITTEN_MOVIE_COMMENT,
      CONTENT_WRITTEN_DRAMA_COMMENT, CONTENT_WISHED_MOVIE, CONTENT_WISHED_DRAMA,
      CONTENT_ONES_DECKS_FOR_MOVIE, CONTENT_MEH_FOR_MOVIES, GIFT, CANDIDATE_MOVIE_PARTNERS,
      FOLLOWERS, FOLLOWINGS, SETTING_FOR_PROFILE, SETTING, GALLERY_MOVIE, GALLERY_DRAMA,
      GALLERY_PROFILE_COVER, GALLERY_PROFILE_PICTURE, WELCOME, ALTER_NAME, ALTER_EMAIL,
      ALTER_PASSWORD, ASSIGNING_PASSWORD, VERIFY_PASSWORD, SETTING_PRIVACY, SETTING_ALARM, FAQ,
      FIND_FRIENDS, SEARCH_USER, INVITE_OTHERS, MOVIE_DETAIL, DECK_DETAIL, DRAMA_DETAIL,
      ADD_MOVIE_INTO_DECK, EVERY_COMMENTS_FILTERED_BY_FRIEND, EVERY_COMMENTS_FILTERED_BY_CONTENT,
      EVERY_STAFFS_FILTERED_BY_CONTENT, EVERY_SERIES_OF_CONTENTS_FILTERED_BY_SINGLE_CONTENT,
      FELLOWS_FROM_TWITTER, FELLOWS_FROM_FACEBOOK, POPULAR_DECKS, AWARDED_MOVIES, QUIZ, MAGAZINE,
      EVERY_CONTENTS_FILTERED_BY_TAGS, EVERY_CONTENTS_FILTERED_BY_ACTORS,
      EVERY_CONTENTS_FILTERED_BY_DIRECTOR, EVERY_CONTENTS_FILTERED_BY_SIMILARITIES,
      EVERY_CONTENTS_FILTERED_BY_FRIENDS, LIKERS, COMMENT_DETAIL, USER_ACTION_POPUP, WRITE_COMMENT,
      EDIT_COMMENT, FEED, YOUTUBE, SORT_MOVIE_ORDER_TO_CREATE_DECK, SORT_MOVIE_ORDER_TO_EDIT_DECK,
      REPLY, EVALUATE_CATEGORIES_MOVIE, EVALUATE_CATEGORIES_DRAMA, BOOK, ARTICLES
  })
  public @interface ScreenNameType {}
  public static final String NONE = "";
  public static final String BOOK = "Book";
  public static final String INTRO = "Intro";
  public static final String INIT = "INIT";
  public static final String FEED = "Feed";
  public static final String REPLY = "Reply";
  public static final String YOUTUBE = "Youtube";
  public static final String SIGNUP = "SignUp";
  public static final String LOGIN = "Login";
  public static final String ALTER_NAME = "AlterName";
  public static final String ALTER_EMAIL = "AlterEmail";
  public static final String ALTER_PASSWORD = "AlterPassword";
  public static final String ASSIGNING_PASSWORD = "AssigningPassword";
  public static final String VERIFY_PASSWORD = "VerifyPassword";
  public static final String GIFT = "Gift";
  public static final String SETTING_PRIVACY = "SettingPrivacy";
  public static final String SETTING_ALARM = "SettingAlarm";
  public static final String FAQ = "FAQ";
  public static final String RESET_PASSWORD = "ResetPassword";
  public static final String RECOMMENDATION_MOVIE = "RecommendationMovie";
  public static final String RECOMMENDATION_DRAMA = "RecommendationDrama";
  public static final String POPULAR_MOVIE = "PopularMovie";
  public static final String POPULAR_DRAMA = "PopularDrama";
  public static final String LEGAL = "Legal";
  public static final String TASTE = "Taste";
  public static final String TUTORIAL_MOVIE = "EvalMoreJoinMovie";
  public static final String TUTORIAL_DRAMA = "EvalMoreJoinTV";
  public static final String CREATE_DECK = "CreateDeck";
  public static final String EDIT_DECK = "EditDeck";
  public static final String SORT_MOVIE_ORDER_TO_CREATE_DECK = "SortMovieOrderToCreateDeck";
  public static final String SORT_MOVIE_ORDER_TO_EDIT_DECK = "SortMovieOrderToEditDeck";
  public static final String ADD_MOVIE_FROM_CREATE_DECK = "AddMovieFromCreateDeck";
  public static final String ADD_DRAMA_FROM_CREATE_DECK = "AddDramaFromCreateDeck";
  public static final String EDIT_MOVIE_FROM_CREATE_DECK = "EditMovieFromCreateDeck";
  public static final String SELECT_MOVIE_FOR_WRITING_COMMENT = "ChoosingMovieBeforeEnteringWriteComment";
  public static final String SELECT_DRAMA_FOR_WRITING_COMMENT = "ChoosingDramaBeforeEnteringWriteComment";
  public static final String WRITE_COMMENT = "WriteComment";
  public static final String EDIT_COMMENT = "EditComment";
  public static final String RATE_MORE_MOVIE = "RateMoreMovie";
  public static final String RATE_MORE_DRAMA = "RateMoreDrama";
  public static final String TAG_SETTING_MOVIE = "TagSettingMovie";
  public static final String TAG_SETTING_MOVIE_VOD = "TagSettingMovieVOD";
  public static final String TAG_SETTING_DRAMA = "TagSettingDrama";
  public static final String EXPLORE = "Explore";
  public static final String SEARCH_RESULT = "SearchResult";
  public static final String NOTIFICATION = "Notification";
  public static final String MY_PROFILE = "MyProfile";
  public static final String USER_PROFILE = "UserProfile";
  public static final String CONTENT_WATCHED_MOVIE = "WatchedMovies";
  public static final String CONTENT_WATCHED_DRAMA = "WatchedDramas";
  public static final String CONTENT_WISHED_MOVIE = "WishToWatchMovies";
  public static final String CONTENT_WISHED_DRAMA = "WishToWatchDrama";
  public static final String CONTENT_ONES_DECKS_FOR_MOVIE = "OnesDecksForMovie";
  public static final String CONTENT_MEH_FOR_MOVIES = "CONTENT_MEH_FOR_MOVIES";
  public static final String CONTENT_WRITTEN_MOVIE_COMMENT = "WrittenCommentsForMovie";
  public static final String CONTENT_WRITTEN_DRAMA_COMMENT = "WrittenCommentsForDrama";
  public static final String CANDIDATE_MOVIE_PARTNERS = "CandidateMoviePartners";
  public static final String FOLLOWERS = "Followers";
  public static final String FOLLOWINGS = "Followings";
  public static final String SETTING_FOR_PROFILE = "SettingForProfile";
  public static final String SETTING = "Setting";
  public static final String GALLERY_MOVIE = "MovieGallery";
  public static final String GALLERY_DRAMA = "DramaGallery";
  public static final String GALLERY_PROFILE_COVER = "GalleryWithFullSizedCover";
  public static final String GALLERY_PROFILE_PICTURE = "GalleryWithFullSizedProfile";
  public static final String WELCOME = "Welcome";
  public static final String FIND_FRIENDS = "FindFriends";
  public static final String SEARCH_USER = "SearchUserByEmailOrNickname";
  public static final String INVITE_OTHERS = "InviteOthers";
  public static final String MOVIE_DETAIL = "MovieDetail";
  public static final String DRAMA_DETAIL = "DramaDetail";
  public static final String DECK_DETAIL = "DeckDetail";
  public static final String ADD_MOVIE_INTO_DECK = "AddMovieIntoDeck";
  public static final String EVERY_COMMENTS_FILTERED_BY_FRIEND = "EveryCommentsFilteredByFriend";
  public static final String EVERY_COMMENTS_FILTERED_BY_CONTENT = "EveryCommentsFilteredByContent";
  public static final String EVERY_STAFFS_FILTERED_BY_CONTENT = "EveryStaffsFilteredByContent";
  public static final String EVERY_SERIES_OF_CONTENTS_FILTERED_BY_SINGLE_CONTENT = "EverySeriesOfContentsFilteredBySingleContent";
  public static final String FELLOWS_FROM_FACEBOOK = "FellowsFromFacebook";
  public static final String FELLOWS_FROM_TWITTER = "FellowsFromTwitter";
  public static final String POPULAR_DECKS = "PopularDecks";
  public static final String AWARDED_MOVIES = "AwardedMovies";
  public static final String QUIZ = "Quiz";
  public static final String ARTICLES = "Articles";
  public static final String MAGAZINE = "Magazine";
  public static final String EVERY_CONTENTS_FILTERED_BY_TAGS = "EveryContentsFilteredByTags";
  public static final String EVERY_CONTENTS_FILTERED_BY_ACTORS = "EveryContentsFilteredByActors";
  public static final String EVERY_CONTENTS_FILTERED_BY_DIRECTOR = "EveryContentsFilteredByDirector";
  public static final String EVERY_CONTENTS_FILTERED_BY_SIMILARITIES = "EveryContentsFilteredBySimilarities";
  public static final String EVERY_CONTENTS_FILTERED_BY_FRIENDS = "EveryContentsFilteredByFriend";
  public static final String LIKERS = "Likers";
  public static final String COMMENT_DETAIL = "CommentDetail";
  public static final String USER_ACTION_POPUP = "UserActionPopup";
  public static final String EVALUATE_CATEGORIES_MOVIE = "EvaluateCategoriesMovie";
  public static final String EVALUATE_CATEGORIES_DRAMA = "EvaluateCategoriesDrama";

  @IntDef({
      JOIN_YEAR_MONTH, JOIN_DAY, MEMBER_ID, SERVICE_COUNTRY
  })
  public @interface CustomDimension {}
  public static final int JOIN_YEAR_MONTH = 2;
  public static final int JOIN_DAY = 3;
  public static final int MEMBER_ID = 4;
  public static final int SERVICE_COUNTRY = 8;

  private GoogleAnalytics mGoogleAnalytics = null;
  private Tracker mTracker = null;

  public static AnalyticsManager getInstance() {
    if (singleton == null) {
      singleton = new AnalyticsManager();
    }
    return singleton;
  }

  public Tracker getTracker() {
    return mTracker;
  }

  public AnalyticsManager() {
    mGoogleAnalytics = GoogleAnalytics.getInstance(WatchaApp.getInstance());
    mGoogleAnalytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
    mGoogleAnalytics.setLocalDispatchPeriod(300);
    mTracker = mGoogleAnalytics.newTracker(BuildConfig.GA_ID);
    mTracker.enableExceptionReporting(true);
    mTracker.enableAdvertisingIdCollection(true);
  }

  public static void sendEvent(AnalyticsEvent analyticsEvent) {
    Map<String, String> event = analyticsEvent.toMap();
    getInstance().mTracker.send(event);
    WLog.i("event: " + event);
  }

  public static void sendScreenName(@Nullable @ScreenNameType String screenName) {
    if (!TextUtils.isEmpty(screenName)) {
      final String screenNameHead = discardTails(screenName);
      getInstance().mTracker.setScreenName(screenNameHead);
      getInstance().mTracker.send(getCustomDimensionedScreenViewBuilder().build());
      WLog.i("screenName: " + screenNameHead);
    }
  }

  /**
   * GA 에 보낼 ScreenName 은 0번째 index 만 쓰고,
   * RnD 통계에 사용하는 ScreenName 은 '/' 로 arguments 들과 함께 보냄.
   *
   * @param screenName GA 와 RnD 용 로그에 사용하는 화면 이름.
   * @return ScreenName 이 '/' 로 나뉘어져 있다면, '/' 로 분할하고 첫번째 값만 사용함.
   * 아니면 파라미터로 받은 값 그대로 리턴함.
   * */
  @ScreenNameType private static String discardTails(@ScreenNameType String screenName) {
    if (!TextUtils.isEmpty(screenName) && screenName.contains("/")) {
      @ScreenNameType String screenNameHead = screenName.split("/")[0];
      return screenNameHead;
    }
    return screenName;
  }

  private HitBuilders.ScreenViewBuilder mScreenViewBuilder = null;

  private static HitBuilders.ScreenViewBuilder getCustomDimensionedScreenViewBuilder() {
    if (getInstance().mScreenViewBuilder == null) {
      getInstance().mScreenViewBuilder = new HitBuilders.ScreenViewBuilder();
      WatchaApp app = WatchaApp.getInstance();
      if (app != null && WatchaApp.getUser() != null) {
        getInstance().mScreenViewBuilder.setCustomDimension(JOIN_YEAR_MONTH, getJoinYearAndMonth(WatchaApp.getUser().getCreatedAt()));
        getInstance().mScreenViewBuilder.setCustomDimension(JOIN_DAY, getJoinDay(WatchaApp.getUser().getCreatedAt()));
        getInstance().mScreenViewBuilder.setCustomDimension(MEMBER_ID, WatchaApp.getUser().getCode());
        getInstance().mScreenViewBuilder.setCustomDimension(SERVICE_COUNTRY, "Korea");
      }
    }
    return getInstance().mScreenViewBuilder;
  }

  public static HitBuilders.EventBuilder getCustomDimensionedBuilder(HitBuilders.EventBuilder builder) {
    if (builder != null) {
      WatchaApp app = WatchaApp.getInstance();
      if (app != null && WatchaApp.getUser() != null) {
        builder.setCustomDimension(JOIN_YEAR_MONTH, getJoinYearAndMonth(WatchaApp.getUser().getCreatedAt()));
        builder.setCustomDimension(JOIN_DAY, getJoinDay(WatchaApp.getUser().getCreatedAt()));
        builder.setCustomDimension(MEMBER_ID, WatchaApp.getUser().getCode());
        builder.setCustomDimension(SERVICE_COUNTRY, "Korea");
      }
    }
    return builder;
  }

  private static String mJoinYearAndMonth = null;
  private static String mJoinDay = null;

  private static String getJoinYearAndMonth(Date createdAt) {
    if (TextUtils.isEmpty(mJoinYearAndMonth)) {
      Calendar c = Calendar.getInstance();
      c.setTime(createdAt);
      return mJoinYearAndMonth = c.get(Calendar.YEAR) + "/" + Calendar.DAY_OF_MONTH;
    }
    return mJoinYearAndMonth;
  }

  private static String getJoinDay(Date createdAt) {
    if (TextUtils.isEmpty(mJoinDay)) {
      Calendar c = Calendar.getInstance();
      c.setTime(createdAt);
      return mJoinDay = c.get(Calendar.DAY_OF_MONTH) + "";
    }
    return mJoinDay;
  }
}
