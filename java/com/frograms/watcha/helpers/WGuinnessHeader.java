package com.frograms.watcha.helpers;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.AccessToken;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.application.WatchaApp;
import com.google.android.gcm.GCMRegistrar;
import retrofit.RequestInterceptor;

/**
 * 왓챠에서 사용하는 커스텀 헤더.
 */
public class WGuinnessHeader {

  public static WGuinnessHeader get(Type type) {
    return new WGuinnessHeader(type);
  }

  public static WGuinnessHeader get(String name) {
    Type type = null;
    for (Type t : Type.values()) {
      if (t.key().equals(name)) {
        type = t;
        break;
      }
    }

    if (type != null) {
      return get(type);
    }
    return null;
  }

  /**
   * 헤더를 직접 넣어줌.
   *
   * 값이 없는 헤더는 무시함.
   */
  public static void setRequestHeader(Type type, RequestInterceptor.RequestFacade request) {
    WGuinnessHeader h = new WGuinnessHeader(type);
    String value = h.getValue();
    if (!TextUtils.isEmpty(value)) {
      request.addHeader(h.getKey(), h.getValue());
    }
  }

  /**
   * 왓챠 헤더 종류.
   */
  public enum Type {
    FACEBOOK_ACCESS_TOKEN("X-Watcha-Facebook-Access-Token"),
    FACEBOOK_USER_ID("X-Watcha-Facebook-User-Id"),
    USER_AGENT("X-Watcha-Client"),
    VERSION("X-Watcha-Client-Version"),
    DEVICE_ID("X-Watcha-Client-Device-Id"),
    REGISTRATION_ID("X-Watcha-Client-Registration-Id"),
    STAGING_FROG("Authorization"),
    LATEST_VERSION("X-Watcha-Client-Latest-Version"),
    REFERRER("X-Watcha-Referer");

    private String mKey;

    Type(String key) {
      mKey = key;
    }

    public String key() {
      return mKey;
    }
  }

  public static String getGCMRegistrationId() {
    return GCMRegistrar.getRegistrationId(WatchaApp.getInstance());
  }

  private Type mType;
  private String mKey, mValue;

  public WGuinnessHeader(Type type) {
    mKey = type.key();
    switch (mType = type) {
      case FACEBOOK_ACCESS_TOKEN:
        if (AccessToken.getCurrentAccessToken() != null && !TextUtils.isEmpty(
            AccessToken.getCurrentAccessToken().getToken())) {
          mValue = AccessToken.getCurrentAccessToken().getToken();
        }
        break;
      case FACEBOOK_USER_ID:
        if (AccessToken.getCurrentAccessToken() != null && !TextUtils.isEmpty(
            AccessToken.getCurrentAccessToken().getUserId())) {
          mValue = AccessToken.getCurrentAccessToken().getUserId();
        }
        break;
      case DEVICE_ID:
        mValue = getDeviceId();
        break;

      case REGISTRATION_ID:
        mValue = getGCMRegistrationId();
        break;

      case USER_AGENT:
        mValue = getUserAgent();
        break;

      case VERSION:
        try {
          mValue = getAppVersion();
        } catch (PackageManager.NameNotFoundException e) {
          e.printStackTrace();
        }
        break;

      case STAGING_FROG:
        if (!TextUtils.isEmpty(BuildConfig.STAGING_HEADER)) {
          mValue = "Basic " + Base64.encodeToString(BuildConfig.STAGING_HEADER.getBytes(),
              Base64.NO_WRAP);
        }
        break;
    }
  }

  public Type getType() {
    return mType;
  }

  /**
   * 헤더 값.
   */
  public String getValue() {
    return mValue;
  }

  public void setValue(String value) {
    mValue = value;
  }

  /**
   * 헤더 키.
   */
  public String getKey() {
    return mKey;
  }

  private String getDeviceId() {
    String deviceId;
    TelephonyManager telManager =
        (TelephonyManager) WatchaApp.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
    deviceId = telManager.getDeviceId();

    if (TextUtils.isEmpty(deviceId)) {
      deviceId = Settings.Secure.getString(WatchaApp.getInstance().getContentResolver(),
          Settings.Secure.ANDROID_ID);
    }
    return deviceId;
  }

  private String getUserAgent() {
    return "Watcha-Android";
  }

  private String getAppVersion() throws PackageManager.NameNotFoundException {
    PackageInfo pi = WatchaApp.getInstance()
        .getPackageManager()
        .getPackageInfo(WatchaApp.getInstance().getPackageName(), 0);
    return pi.versionName;
  }

  private String getNetworkOperatorName() {
    TelephonyManager telManager =
        (TelephonyManager) WatchaApp.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
    if (telManager == null) {
      return null;
    }
    return telManager.getNetworkOperator();
  }

  @Override public String toString() {
    return mKey + "=" + mValue;
  }
}
