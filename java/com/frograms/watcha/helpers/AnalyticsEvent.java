package com.frograms.watcha.helpers;

import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import com.google.android.gms.analytics.HitBuilders;
import java.util.Map;

/**
 * GA 에 기록 남기는 Event 종류.
 */
public class AnalyticsEvent {

  @StringDef({
      PAGE, FEED, FACEBOOK, TWITTER, KAKAOTALK, PROMOTION, CONTACT
  })
  public @interface AnalyticsEventLabel {}
  public static final String PAGE = "Page";
  public static final String FEED = "Feed";
  public static final String FACEBOOK = "Facebook";
  public static final String TWITTER = "Twitter";
  public static final String KAKAOTALK = "KakaoTalk";
  public static final String CONTACT = "Contact";
  public static final String PROMOTION = "Promotion";


  private final Builder mBuilder;

  private AnalyticsEvent(Builder builder) {
    mBuilder = builder;
  }

  public Map<String, String> toMap() {
    HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder(mBuilder.mCategoryType.getValue(), mBuilder.mActionType.getValue())
        .setValue(mBuilder.mValue);
    if (!TextUtils.isEmpty(mBuilder.mLabel)) {
      builder.setLabel(mBuilder.mLabel);
    }
    return AnalyticsManager.getCustomDimensionedBuilder(builder).build();
  }

  public static class Builder {

    AnalyticsCategoryType mCategoryType;
    AnalyticsActionType mActionType;
    @Nullable @AnalyticsEventLabel String mLabel;
    long mValue;

    public Builder(AnalyticsCategoryType categoryType, AnalyticsActionType actionType) {
      setCategory(categoryType);
      setAction(actionType);
    }

    public Builder setCategory(AnalyticsCategoryType categoryType) {
      mCategoryType = categoryType;
      return this;
    }

    public Builder setAction(AnalyticsActionType actionType) {
      mActionType = actionType;
      return this;
    }

    public Builder setLabel(@Nullable @AnalyticsEventLabel String label) {
      mLabel = label;
      return this;
    }

    public Builder setValue(long value) {
      mValue = value;
      return this;
    }

    public AnalyticsEvent build() {
      return new AnalyticsEvent(this);
    }
  }
}
