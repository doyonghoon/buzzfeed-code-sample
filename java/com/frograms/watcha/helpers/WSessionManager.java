package com.frograms.watcha.helpers;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.utils.WLog;
import com.squareup.okhttp.OkHttpClient;
import java.net.CookieHandler;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 기네스 세션 확인, 유지, 끊기 등 유저가 왓챠 서버에 api를 호출할 수 있게 도와주는 매니저 클래스.
 */
public class WSessionManager {

  private static final String KEY_SESSION = "SessionManagerCookies";
  private static final String FIRST_VISIT_IN_V3 = "FirstVisitInV3";

  //private Context mContext;
  private java.net.CookieManager mCookieManager = null;

  public WSessionManager() {
    //mContext = context;
  }

  /**
   * 기네스 세션 포함해서 몇가지 서버가 날려주는 쿠키들이 존재하는지 확인하고 있으면 왓챠 유저임.
   */
  public boolean hasSession(Context c) {
    return c != null && bringCookiesFromLocalStorage(c).length > 0;
  }

  /**
   * 세션을 파기 시킴.
   */
  public boolean destroySession(Context context) {
    try {
      WatchaApp.getInstance().setUser(null);
      CookieSyncManager.createInstance(context);
      CookieSyncManager.getInstance().resetSync();
      android.webkit.CookieManager.getInstance().removeAllCookies(value -> WLog.i("session removed."));
    } catch (NoSuchMethodError e) {
      e.printStackTrace();
    }
    return context.getSharedPreferences(KEY_SESSION, Context.MODE_PRIVATE).edit().clear().commit();
  }

  /**
   * 왓챠가 http 사용을 위해서 초기화할때 쿠키가 기기에 저장되어 있다면 쿠키매니저에 추가시킴.
   *
   * @param client http 요청하는 본체.
   */
  public void setUpCookie(Context context, OkHttpClient client) {
    URI domain = getDomainURI();
    if (domain != null) {
      for (HttpCookie c : bringCookiesFromLocalStorage(context)) {
        getCookieManager().getCookieStore().add(domain, c);
      }
    }

    CookieHandler.setDefault(getCookieManager());
    getCookieManager().setCookiePolicy(CookiePolicy.ACCEPT_ALL);
    CookieSyncManager.createInstance(context);
    android.webkit.CookieManager.getInstance().setAcceptCookie(true);
    client.setCookieHandler(getCookieManager());
    WLog.i("setting cookie..");
  }

  public Map<String, String> getGuinnessSessionTokens(Context context) {
    Map<String, String> tokens = new HashMap<>();
    for (HttpCookie c : bringCookiesFromLocalStorage(context)) {
      tokens.put(c.getName(), c.getValue());
    }
    return tokens;
  }

  /**
   * http 요청할때 쉽게 쿠키를 헤더에 추가해주는 클래스.
   */
  public java.net.CookieManager getCookieManager() {
    if (mCookieManager == null) {
      mCookieManager = new java.net.CookieManager();
    }
    return mCookieManager;
  }

  /**
   * 왓챠 서버가 인증한 토큰값 쿠키 형태로 저장함.
   */
  public void storeWatchaSessionInLocalStorage(Context context) {
    List<HttpCookie> httpCookies = getCookieManager().getCookieStore().getCookies();
    for (int i = 0; i < httpCookies.size(); i++) {
      setClassPref(context, "Cookie" + i, httpCookies.get(i));
      android.webkit.CookieManager.getInstance()
          .setCookie(BuildConfig.END_POINT,
              httpCookies.get(i).getName() + "=" + httpCookies.get(i).getValue());
    }
  }

  /**
   * 기기에 저장된 쿠키 값들을 읽어옴.
   */
  private HttpCookie[] bringCookiesFromLocalStorage(Context c) {
    List<HttpCookie> cookies = new ArrayList<>();
    int i = 0;
    while (true) {
      HttpCookie httpCookie = getClassPref(c, "Cookie" + i, HttpCookie.class);
      if (httpCookie != null) {
        WLog.i("cookie: " + httpCookie);
        cookies.add(httpCookie);
      } else {
        break;
      }
      i++;
    }
    return cookies.toArray(new HttpCookie[cookies.size()]);
  }

  /**
   * 왓챠 api 도메인을 URI 형태로 반환함.
   * {@link #setUpCookie(Context, OkHttpClient)} 쿠키매니저에 쿠키 추가할때 URI가 필요함..
   */
  public static URI getDomainURI() {
    try {
      return new URI(BuildConfig.END_POINT);
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static <T> T getClassPref(Context c, String key, Class<T> a) {
    if (TextUtils.isEmpty(key)) return null;
    String result = c.getSharedPreferences(KEY_SESSION, Context.MODE_PRIVATE).getString(key, null);
    if (result == null) return null;
    try {
      return WatchaApp.getInstance().getGson().fromJson(result, a);
    } catch (Exception e) {
      return null;
    }
  }

  public static boolean setClassPref(Context c, String key, Object value) {
    return !(TextUtils.isEmpty(key) || value == null) && c.getSharedPreferences(KEY_SESSION,
        Context.MODE_PRIVATE)
        .edit()
        .putString(key, WatchaApp.getInstance().getGson().toJson(value))
        .commit();
  }

  public static boolean isFirstVisitInV3(Context c) {
    if (c == null) return false;

    return c.getSharedPreferences(KEY_SESSION, Context.MODE_PRIVATE)
        .getBoolean(FIRST_VISIT_IN_V3, true);
  }

  public static void setIsFirstVisitInV3(Context c) {
    if (c == null) return;

    c.getSharedPreferences(KEY_SESSION, Context.MODE_PRIVATE)
        .edit()
        .putBoolean(FIRST_VISIT_IN_V3, false)
        .commit();
  }
}
