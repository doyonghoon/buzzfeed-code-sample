package com.frograms.watcha.helpers;

import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.SearchView;
import com.frograms.watcha.R;

public class ToolbarHelper implements SearchView.OnQueryTextListener {

  public static enum ToolbarActionType {NONE, TRANSPARENT, SCROLL}

  protected static ToolbarHelper instance;

  protected AppCompatActivity mActivity;
  protected Toolbar mToolbar;
  protected ActionBar mActionbar;
  protected Menu mMenu;

  protected ToolbarActionType mToolbarActionType;

  protected SearchView mSearchView;

  protected int mBackgroundColorId;

  public ToolbarHelper(AppCompatActivity activity) {
    mActivity = activity;
  }

  public static ToolbarHelper getInstance(AppCompatActivity activity) {
    if (activity != null) {
      instance = new ToolbarHelper(activity);
    }
    return instance;
  }

  public void clearHelper() {
    if (mSearchView != null && !mSearchView.isIconified()) {
      mSearchView.setIconified(true);
    }
    if (mSearchView != null) mSearchView.setOnQueryTextListener(null);
  }

  public void setToolbar() {
    if (mActivity != null) {
      mToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
      mActivity.setSupportActionBar(mToolbar);
      mActionbar = mActivity.getSupportActionBar();
    }
  }

  public Toolbar getToolbar() {
    if (mActivity != null && mToolbar == null) {
      setToolbar();
    }

    return mToolbar;
  }

  public ActionBar getActionbar() {
    if (mActivity != null && mActionbar == null) {
      setToolbar();
    }
    return mActionbar;
  }

  public void setToolbarActionType(ToolbarActionType type) {
    if (mToolbarActionType == null) {
      mToolbarActionType = type;
    }
  }

  public void clearToolbarActionType() {
    mToolbarActionType = null;
  }

  public ToolbarActionType getToolbarActionType() {
    return mToolbarActionType;
  }

  public void setToolbarTheme() {
    if (mActivity == null) return;
  }

  public void setToolbarHeight(int height) {
    getToolbar().getLayoutParams().height = height;
  }

  public void setToolbarTitle(String title) {
    setToolbarTitle(title, null);
  }

  public void setToolbarSubtitle(String subtitle) {
    if (getActionbar() == null) return;
    getActionbar().setSubtitle(subtitle);
  }

  public void setToolbarTitle(String title, String subTitle) {
    if (getActionbar() == null) {
      return;
    }

    if (title == null) {
      title = "";
    }

    if (subTitle == null) {
      subTitle = "";
    }

    getActionbar().setTitle(title);
    getActionbar().setSubtitle(subTitle);
  }

  public void setTitleColor(@ColorRes int id) {
    getToolbar().setTitleTextColor(mActivity.getResources().getColor(id));
  }

  public void setBackground(int id) {
    int sdkVersion = Build.VERSION.SDK_INT;
    if (sdkVersion < Build.VERSION_CODES.LOLLIPOP_MR1) {
      getToolbar().setBackground(mActivity.getResources().getDrawable(id));
    } else {
      getToolbar().setBackground(mActivity.getResources().getDrawable(id, mActivity.getTheme()));
    }
    mBackgroundColorId = id;
  }

  public int getBackgroundColorId() {
    return mBackgroundColorId;
  }

  public void setNavigationIcon(int id) {
    int sdkVersion = Build.VERSION.SDK_INT;
    if (sdkVersion < Build.VERSION_CODES.LOLLIPOP_MR1) {
      getToolbar().setBackground(mActivity.getResources().getDrawable(id));
    } else {
      getToolbar().setBackground(mActivity.getResources().getDrawable(id, mActivity.getTheme()));
    }
  }

  public void updateMenuItemText(int menuId, String title) {
    if (mMenu != null && title != null) {
      mMenu.findItem(menuId).setTitle(title);
    }
  }

  /**
   * ActionBar SearchView
   */
  public void clearSearchViewFocus() {
    if (mSearchView != null) {
      mSearchView.clearFocus();
    }
  }

  public void clearSearchView() {
    if (mSearchView != null) {
      if (mSearchView.getQuery().length() > 0) {
        mSearchView.setQuery(null, false);
      }
      clearSearchViewFocus();
    }
  }

  public void setToolbarSearchView(SearchView searchView) {
    if (searchView == null) return;
    mSearchView = searchView;
    mSearchView.setOnQueryTextListener(this);
  }

  @Override public boolean onQueryTextSubmit(String query) {
    //        if (activity == null)
    //            Util.closeKeyboard(activity);
    return false;
  }

  @Override public boolean onQueryTextChange(String query) {
    return false;
  }
  /**
   * ActionBar SearchView
   */

  /**
   * 대안이 있을까.,,
   */
  public int getActionBarTitleId() {
    int actionBarTitleID = 0;
    try {
      actionBarTitleID =
          Class.forName("com.android.internal.R$id").getField("action_bar_title").getInt(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return actionBarTitleID;
  }
}
