package com.frograms.watcha.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.RatingTabFragment;
import com.frograms.watcha.fragment.TutorialFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.QueryType;

public class UserActionHelper {

  //public enum ActionType {
  //  RATING("rate"),
  //  WISH("wish"),
  //  MEH("meh");
  //
  //  private String type;
  //
  //  ActionType(String type) {
  //    this.type = type;
  //  }
  //
  //  public String getType() {
  //    return type;
  //  }
  //}
  //
  //private static String mCategory, mCode;
  //private static ActionType mActionType;
  //
  public static class OnBaseRatingResponseListener
      extends BaseApiResponseListener<BaseResponse<RatingData>> {
    private String mCategory;
    private UserAction mDefaultUserAction;
    //private Context mContext;

    public OnBaseRatingResponseListener(Context context) {
      super(context);
    }

    @Override
    public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<RatingData> result) {
      UserAction action = result.getData().getUserAction();

      boolean hasComment = false;

      if (mDefaultUserAction != null) {
        int count = WatchaApp.getUser().getActionCount(mCategory).getWishes();

        if (mDefaultUserAction.isWished()) {
          WatchaApp.getUser().getActionCount(mCategory).setWishes(count - 1);
        }

        count = WatchaApp.getUser().getActionCount(mCategory).getMehs();

        if (mDefaultUserAction.isMehed()) {
          WatchaApp.getUser().getActionCount(mCategory).setMehs(count - 1);
        }

        if (mDefaultUserAction.getComment() != null) hasComment = true;

        count = WatchaApp.getUser().getActionCount(mCategory).getRatings();

        if (mDefaultUserAction.getRating() > 0) {
          WatchaApp.getUser().getActionCount(mCategory).setRatings(count - 1);
        }
      }

      int count = WatchaApp.getUser().getActionCount(mCategory).getWishes();

      if (action.isWished()) WatchaApp.getUser().getActionCount(mCategory).setWishes(count + 1);

      count = WatchaApp.getUser().getActionCount(mCategory).getMehs();

      if (action.isMehed()) WatchaApp.getUser().getActionCount(mCategory).setMehs(count + 1);

      count = WatchaApp.getUser().getActionCount(mCategory).getRatings();

      if (action.getRating() > 0) {
        WatchaApp.getUser().getActionCount(mCategory).setRatings(count + 1);
      } else if (hasComment) {
        WatchaApp.getUser()
            .getActionCount(mCategory)
            .setComments(WatchaApp.getUser().getActionCount(mCategory).getRatings() - 1);
      }

      if (getContext() != null && getContext() instanceof BaseActivity) {
        BaseFragment frag = ((BaseActivity) getContext()).getFragment();
        if (frag != null
            && !(frag instanceof RatingTabFragment)
            && !(frag instanceof TutorialFragment)) {
          Toast.makeText(WatchaApp.getInstance(), "저장되었어요", Toast.LENGTH_SHORT).show();
        }
      }
    }

    public void setDefaultUserAction(String category, UserAction action) {
      mCategory = category;
      mDefaultUserAction = action;
    }
  }
  //}
  //
  //public static QueryType makeQueryType(ActionType type, boolean isRating, String category, String code) {
  //  mCategory = category;
  //  mCode = code;
  //  mActionType = type;
  //
  //  QueryType queryType;
  //  queryType = isRating ? QueryType.USER_ACTION : QueryType.USER_ACTION_CANCEL;
  //  queryType.setApi(CategoryType.getCategory(category).getApiPath(), code, type.getType());
  //  return queryType;
  //}
  //
  //private static boolean canShowAlert(@NonNull Activity activity) {
  //  if (activity instanceof BaseActivity) {
  //    BaseActivity baseActivity = (BaseActivity) activity;
  //    return !(baseActivity.getFragment() instanceof TutorialFragment);
  //  }
  //  return false;
  //}
  //
  //public static void requestUserAction(final Activity activity,final String contentTitle, final boolean isUnreleased,
  //    final String contentCode, final QueryType queryType, final ActionType actionType, final boolean isRating,
  //    final Map<String, String> params, final OnBaseRatingResponseListener listener) {
  //  if (mActionType != null) {
  //    if (mActionType.equals(ActionType.WISH)) {
  //      int count = PrefHelper.getCount(activity, PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.WISH);
  //
  //      if (count <= 10) {
  //        PrefHelper.addCount(activity, PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.WISH);
  //      }
  //
  //      if (count == 3) {
  //        AlertHelper.showAlert(activity, AlertHelper.AlertType.PLAY_RATING);
  //      } else if (count == 10) {
  //        AlertHelper.showAlert(activity, AlertHelper.AlertType.INVITE);
  //      }
  //    } else if (mActionType.equals(ActionType.RATING) && params != null
  //        && params.containsKey("value") && Float.valueOf(params.get("value")) == 5.0f && canShowAlert(activity)) {
  //      AlertHelper.showAlert(activity, AlertHelper.AlertType.WRITE_COMMENT.setContentCode(mCode));
  //    }
  //  }
  //
  //  UserAction defaultUserAction = CacheManager.getMyUserAction(mCode);
  //  if (listener != null) {
  //    listener.setDefaultUserAction(mCategory, defaultUserAction);
  //  }
  //  if (activity != null
  //      && actionType == UserActionHelper.ActionType.RATING
  //      && isRating
  //      && isUnreleased
  //      && params != null
  //      && params.containsKey("value")) {
  //    String value = RatingUtils.getRatingNumber(params.get("value"));
  //
  //    MaterialDialog.Builder builder = new MaterialDialog.Builder(activity);
  //    builder.theme(Theme.LIGHT);
  //    builder.typeface(FontHelper.FontType.ROBOTO_MEDIUM.getTypeface(), FontHelper.FontType.ROBOTO_REGULAR
  //        .getTypeface());
  //    if (NumberUtils.isNumber(value)) {
  //      final float ratingValue = NumberUtils.toFloat(value);
  //      builder.title("아직 세상에 나오지 않은 작품이예요");
  //      builder.content(String.format("정말 감상하신 작품이 맞나요?:)\n" + "아니면 %s 해주세요!",
  //          ratingValue >= 3.0 ? "보고싶어요" : "관심없어요"));
  //      builder.positiveText("정말 본 작품이에요");
  //      builder.negativeText(ratingValue >= 3.0 ? "보고싶어요 하기" : "관심없어요 하기");
  //      builder.callback(new MaterialDialog.ButtonCallback() {
  //        @Override public void onNegative(MaterialDialog dialog) {
  //          super.onNegative(dialog);
  //          dialog.dismiss();
  //          if (ratingValue > 3.0) {
  //            QueryType wishType = makeQueryType(UserActionHelper.ActionType.WISH, isRating, mCategory, contentCode);
  //            request(activity, wishType, listener, new HashMap<String, String>());
  //          } else {
  //            QueryType wishType = makeQueryType(UserActionHelper.ActionType.MEH, isRating, mCategory, contentCode);
  //            request(activity, wishType, listener, new HashMap<String, String>());
  //          }
  //        }
  //
  //        @Override public void onPositive(MaterialDialog dialog) {
  //          super.onPositive(dialog);
  //          request(activity, queryType, listener, params);
  //      }
  //      });
  //      builder.show();
  //    }
  //    return;
  //  }
  //
  //  if (queryType.equals(QueryType.USER_ACTION_CANCEL) &&
  //      defaultUserAction != null &&
  //      defaultUserAction.getComment() != null) {
  //    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
  //    builder.setMessage(R.string.delete_comment_by_cancelinng_rating)
  //        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
  //          @Override public void onClick(DialogInterface dialogInterface, int i) {
  //            request(activity, queryType, listener, params);
  //          }
  //        }).setNegativeButton(R.string.no, null);
  //    builder.show();
  //  } else {
  //    request(activity, queryType, listener, params);
  //  }
  //}
  //
  //private static void request(Activity activity, QueryType queryType, OnBaseRatingResponseListener listener, Map<String, String> params) {
  //  DataProvider dataProvider = new DataProvider<BaseResponse<RatingData>>(activity, queryType).responseTo(listener);
  //  Map<String, String> requestParams = new HashMap<>();
  //  if (params != null) {
  //    requestParams.putAll(params);
  //  }
  //  dataProvider.withParams(requestParams);
  //  dataProvider.request();
  //}
}
