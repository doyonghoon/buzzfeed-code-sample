package com.frograms.watcha.helpers;

import android.app.Activity;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import java.util.HashMap;
import java.util.Map;

public class DeckActionHelper {

  public enum ActionType {
    LIKE("like");

    private String type;

    ActionType(String type) {
      this.type = type;
    }

    public String getType() {
      return type;
    }
  }

  ;

  private static ActionType mActionType;

  public static QueryType makeQueryType(ActionType type, boolean isRating, String code) {
    mActionType = type;
    QueryType queryType;
    if (type == null) {
      if (!isRating) {
        queryType = QueryType.DECK_DELETE;
      } else {
        if (code == null) {
          queryType = QueryType.DECK_CREATE;
          return queryType;
        } else {
          queryType = QueryType.DECK_UPDATE;
        }
      }

      queryType.setApi(code);
    } else {
      if (isRating) {
        queryType = QueryType.DECK_ACTION;
      } else {
        queryType = QueryType.DECK_ACTION_CANCEL;
      }

      queryType.setApi(code, type.getType());
    }

    return queryType;
  }

  //public static void requestDeckUpdate(Activity activity, QueryType queryType, Map<String, String> params, ApiResponseListener<BaseResponse<DeckData>> listener) {
  //  DataProvider mDataProvider = new DataProvider<BaseResponse<DeckData>>(activity, queryType).responseTo(listener);
  //
  //  Map<String, String> requestParams = new HashMap<>();
  //  if (params != null) {
  //    requestParams.putAll(params);
  //  }
  //
  //  mDataProvider.withParams(requestParams);
  //  mDataProvider.request();
  //}

  public static void requestDeck(Activity activity, QueryType queryType, String deckCode, Map<String, String> params,
      ApiResponseListener<BaseResponse<CommentReportData>> listener) {
    if (mActionType != null && mActionType == ActionType.LIKE) {
      int count = PrefHelper.getCount(activity, PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.LIKE);

      if (count <= 15) {
        PrefHelper.addCount(activity, PrefHelper.PrefType.COUNT, PrefHelper.PrefKey.LIKE);
      }

      if (count == 5) {
        AlertHelper.showAlert(activity, AlertHelper.AlertType.PLAY_RATING);
      } else if (count == 15) {
        AlertHelper.showAlert(activity, AlertHelper.AlertType.INVITE);
      }
    }

    ReferrerBuilder referrerBuilder = createReferrerBuilder(activity);
    DataProvider mDataProvider =
        new DataProvider<BaseResponse<CommentReportData>>(activity, queryType, referrerBuilder)
            .responseTo(listener);

    Map<String, String> requestParams = new HashMap<>();
    if (params != null) {
      requestParams.putAll(params);
    }

    mDataProvider.withParams(requestParams);
    mDataProvider.request();
  }

  private static ReferrerBuilder createReferrerBuilder(Activity activity) {
    if (activity != null && activity instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) activity).getFragment();
      if (baseFragment instanceof AbsPagerFragment) {
        AbsPagerFragment absPagerFragment = (AbsPagerFragment) baseFragment;
        return new ReferrerBuilder(absPagerFragment.getCurrentScreenName());
      } else {
        return new ReferrerBuilder(baseFragment.getCurrentScreenName());
      }
    }
    return null;
  }
}
