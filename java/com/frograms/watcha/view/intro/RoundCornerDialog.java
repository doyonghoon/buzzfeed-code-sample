package com.frograms.watcha.view.intro;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.widget.Space;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import butterknife.ButterKnife;
import carbon.animation.AnimUtils;
import com.frograms.watcha.R;
import com.frograms.watcha.listeners.LightAnimationListener;
import com.nineoldandroids.animation.Animator;

public class RoundCornerDialog extends Dialog {

  public interface RoundCornerDismissAnimationListener {
    void onFinishDismissAnimation();
  }

  private ViewGroup mContentLayout;
  private FrameLayout mRootLayout;
  private Space mSpace;

  private RoundCornerDismissAnimationListener mDismissAnimationListener;
  private RoundCornerListener mCornerListener;

  @Override public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    mCornerListener = null;
  }

  public interface RoundCornerListener {
    View getView(RoundCornerDialog d);
  }

  public RoundCornerDialog(Context context, RoundCornerListener listener) {
    super(context, R.style.NotitleBarHoloLightDialog);
    mCornerListener = listener;
    init();
  }

  @Override public void show() {
    super.show();
    setShowAnimation();
  }

  private void init() {
    mContentLayout =
        (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.round_dialog, null);
    mContentLayout.setClickable(true);
    mSpace = ButterKnife.findById(mContentLayout, R.id.space);
    ((ViewGroup) mContentLayout.getChildAt(0)).addView(mCornerListener.getView(this));
    getRootBackgroundView().addView(mContentLayout);
    findViewIds(mContentLayout);
    setContentView(getRootBackgroundView());
  }

  protected void setShowAnimation() {
    if (mContentLayout != null
        && mContentLayout.getChildCount() > 0
        && getRootBackgroundView() != null) {
      getRootBackgroundView().setAlpha(0.0f);
      if (mContentLayout.getChildAt(0) != null) {
        AnimUtils.fadeIn(getRootBackgroundView(), new LightAnimationListener() {
          @Override public void onAnimationStart(Animator animation) {
            mContentLayout.getChildAt(0).setAlpha(0.0f);
          }

          @Override public void onAnimationEnd(Animator animation) {
            AnimUtils.popIn(mContentLayout.getChildAt(0), null);
          }
        });
      }
    }
  }

  public void setOnDismissAnimationListener(RoundCornerDismissAnimationListener listener) {
    mDismissAnimationListener = listener;
  }

  public void setDismissAnimation() {
    if (mContentLayout != null
        && mContentLayout.getChildCount() > 0
        && getRootBackgroundView() != null) {
      AnimUtils.popOut(mContentLayout.getChildAt(0), new LightAnimationListener() {
        @Override public void onAnimationEnd(Animator animation) {
          AnimUtils.fadeOut(getRootBackgroundView(), new LightAnimationListener() {
            @Override public void onAnimationEnd(Animator animation) {
              if (mDismissAnimationListener != null) {
                mDismissAnimationListener.onFinishDismissAnimation();
              }
              dismiss();
            }
          });
        }
      });
    }
  }

  private void findViewIds(View base) {
    base.setOnClickListener(v -> setDismissAnimation());
  }

  protected FrameLayout getRootBackgroundView() {
    if (mRootLayout == null) {
      mRootLayout = new FrameLayout(getContext());
      FrameLayout.LayoutParams params =
          new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
              FrameLayout.LayoutParams.MATCH_PARENT);
      mRootLayout.setLayoutParams(params);
    }

    return mRootLayout;
  }

  @Override public void onBackPressed() {
    if (isShowing()) {
      setDismissAnimation();
    } else {
      super.onBackPressed();
    }
  }
}
