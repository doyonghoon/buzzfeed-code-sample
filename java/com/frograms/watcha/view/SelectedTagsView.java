package com.frograms.watcha.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.view.fonticon.FontIconView;
import com.frograms.watcha.view.widget.TagTextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by parkjiho on 15. 6. 16..
 */
public class SelectedTagsView extends RelativeLayout implements View.OnClickListener {
  @Bind(R.id.tags_container) LinearLayout mTagsContainerLayout;
  @Bind(R.id.tags_scroll_view) HorizontalScrollView mScrollView;
  @Bind(R.id.tags_delete) FontIconView mTagDelete;

  private ArrayList<TagItems.Tag> mTagsShown = new ArrayList<>();
  private boolean isShown = false;
  private Context mContext = null;
  private List<OnTagItemResetListener> mOnTagItemResetListeners = null;

  public SelectedTagsView(Context context) {
    this(context, null, 0);
  }

  public SelectedTagsView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SelectedTagsView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    mContext = context;
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_selected_tags, this);
    ButterKnife.bind(this);
    mTagDelete.setOnClickListener(this);
  }

  public void addTags(Set<Map.Entry<String, TagItems.Tag>> tags) {
    for (Map.Entry<String, TagItems.Tag> tag : tags)
      addTag(tag.getValue());
  }

  public void addTag(TagItems.Tag tag) {
    //이미 View에 추가 되어있으면 패스
    if (!mTagsShown.contains(tag)) {
      TagTextView v = new TagTextView(mContext, tag);
      v.setFocused(true);
      LinearLayout.LayoutParams params =
          new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
              LinearLayout.LayoutParams.WRAP_CONTENT);
      params.setMargins(0, 0, 20, 0);
      v.setLayoutParams(params);
      mTagsContainerLayout.addView(v);
      autoScrollToEnd();
      mTagsShown.add(tag);
      v.setOnClickListener(this);
    }
  }

  public void removeTag(int tagKey) {
    for (int i = 0; i < mTagsContainerLayout.getChildCount(); i++) {
      if (mTagsContainerLayout.getChildAt(i) instanceof TagTextView) {
        TagTextView tmp = (TagTextView) mTagsContainerLayout.getChildAt(i);
        if (tmp.getTagData().getId() == tagKey) {
          mTagsContainerLayout.removeViewAt(i);
          mTagsShown.remove(i);
          break;
        }
      }
    }
  }

  public void addOnTagItemResetListener(OnTagItemResetListener listener) {
    if (mOnTagItemResetListeners == null) mOnTagItemResetListeners = new ArrayList<>();
    mOnTagItemResetListeners.add(listener);
  }

  public ArrayList<TagItems.Tag> getTagsShown() {
    return mTagsShown;
  }

  public void autoScrollToEnd() {
    mScrollView.postDelayed(new Runnable() {
      public void run() {
        mScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
      }
    }, 100L);
  }

  @Override public void onClick(View v) {
    if (v instanceof TagTextView) {
      TagItems.Tag tag = ((TagTextView) v).getTagData();
      CacheManager.removeTag(tag);
      ArrayList<TagItems.Tag> tmpList = new ArrayList<>();
      tmpList.add(tag);
      notifyOnTagResetListeners(tmpList);
    }
    switch (v.getId()) {
      case R.id.tags_delete:
        notifyOnTagResetListeners(getTagsShown());
        //Cache에서 지우기. (ConcurrentModificationException을 피하기 위해 새로운 ArrayList로)
        for (TagItems.Tag tag : new ArrayList<>(getTagsShown()))
          CacheManager.removeTag(tag);
    }
  }

  private void notifyOnTagResetListeners(List<TagItems.Tag> tags) {
    for (OnTagItemResetListener listener : mOnTagItemResetListeners)
      listener.resetTagItems(tags);
  }

  public boolean isShown() {
    return isShown;
  }

  public void setIsShown(boolean b) {
    isShown = b;
  }

  public interface OnTagItemResetListener {
    //tags에 있는 애들을 reset함
    void resetTagItems(List<TagItems.Tag> tags);
  }
}
