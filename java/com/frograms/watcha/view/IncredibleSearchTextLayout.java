package com.frograms.watcha.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.SearchAutocompleteAdapter;
import com.frograms.watcha.database.LatestSearchTextStore;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.SearchSuggestionText;
import com.frograms.watcha.model.response.Autocomplete;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.ScreenNameUtils;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.fonticon.FontIconView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.HandlerThreadScheduler;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * 커스텀으로 만든 검색뷰.
 */
public class IncredibleSearchTextLayout extends RelativeLayout
    implements TextView.OnEditorActionListener, View.OnFocusChangeListener, TextWatcher {

  private Scheduler mMainThreadScheduler = null;

  @Bind(R.id.search_separate) View mSeparate;
  @Bind(R.id.search_icon) FontIconView mSearchIcon;
  @Bind(R.id.search_text) AutocompleteSearchTextView mSearchView;
  @Bind(R.id.search_delete) FontIconView mDeleteButton;

  private final Builder mBuilder;
  private SearchAutocompleteAdapter mAdapter = null;
  private Subscription mAutocompleteSubscription = null;
  private Subscription mShowDropdownSubscription = null;
  private Action1<String> mSearchTextOnClickAction = null;

  IncredibleSearchTextLayout(@NonNull Builder builder) {
    super(builder.mContext, null, 0);
    mBuilder = builder;
    mMainThreadScheduler = new HandlerThreadScheduler(new Handler(Looper.getMainLooper()));
    mSearchTextOnClickAction = new SearchTextOnClickSubscriber(getContext()).getOnClickSubscriber();
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_search_layout, this);
    ButterKnife.bind(this);
    initStyle(mBuilder.mStyle);

    mAdapter = new SearchAutocompleteAdapter(getContext(), this, mBuilder.mAction);
    mSearchView.setAdapter(mAdapter);
    mSearchView.setThreshold(0);
    mSearchView.setOnEditorActionListener(this);
    mSearchView.setOnFocusChangeListener(this);
    mSearchView.addTextChangedListener(this);
    mSearchView.setHint(mBuilder.mHintText);
  }

  private ReferrerBuilder createReferrerBuilder() {
    if (mSearchView != null
        && mSearchView.getContext() != null
        && mSearchView.getContext() instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) mSearchView.getContext();
      return new ReferrerBuilder(baseActivity.getFragment().getCurrentScreenName());
    }
    return null;
  }

  @Override protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (mAutocompleteSubscription != null && !mAutocompleteSubscription.isUnsubscribed()) {
      mAutocompleteSubscription.unsubscribe();
    }
    if (mShowDropdownSubscription != null && !mShowDropdownSubscription.isUnsubscribed()) {
      mShowDropdownSubscription.unsubscribe();
    }
  }

  public void onResume() {
    if (mAdapter != null && mSearchView != null) {
      mShowDropdownSubscription = createDropdownMenuObservable(
          LatestSearchTextStore.getSearchTexts(getContext())).subscribe(aBoolean -> {
            mSearchView.requestFocus();
            Util.openKeyboard(mSearchView);
          });
    }
  }

  public void closeKeyboard() {
    Util.closeKeyboard(mSearchView);
  }

  public AutocompleteSearchTextView getSearchView() {
    return mSearchView;
  }

  /**
   * 안전하게 메뉴를 보여줌.
   */
  public void dismissDropdown() {
    boolean canDismiss = mSearchView != null
        && mSearchView.getContext() != null
        && mSearchView.getContext() instanceof Activity
        && mSearchView.isPopupShowing();
    if (canDismiss) {
      mSearchView.dismissDropDown();
    }
  }

  @OnClick(R.id.search_delete) void onClickDelete() {
    mSearchView.setText(null);
    Util.openKeyboard(mSearchView);
  }

  private void initStyle(@NonNull Builder.Style style) {
    mSeparate.setBackgroundColor(getResources().getColor(style.getSeparateColor()));
    mSearchIcon.setTextColor(getResources().getColor(style.getTextColor()));
    mSearchView.setTextColor(getResources().getColor(style.getTextColor()));
    mSearchView.setHintTextColor(getResources().getColor(style.getTextHintColor()));
    setBackgroundColor(getResources().getColor(style.getBackgroundColor()));
  }

  public Observable<String> cacheQueryObservable(@NonNull final String query) {
    return Observable.create(new Observable.OnSubscribe<String>() {
      @Override public void call(Subscriber<? super String> subscriber) {
        if (mBuilder.mEnableCache) {
          // 최근검색어 추가.
          LatestSearchTextStore.addText(getContext(), query);
        }
        subscriber.onNext(query);
        subscriber.onCompleted();
      }
    });
  }

  public Observable<Boolean> canGoToSearchResult(final String query) {
    return Observable.create(new Observable.OnSubscribe<Boolean>() {
      @Override public void call(Subscriber<? super Boolean> subscriber) {
        if (!TextUtils.isEmpty(query)) {
          subscriber.onNext(true);
          subscriber.onCompleted();
        } else {
          subscriber.onError(new Throwable("검색어가 비어있음.."));
        }
      }
    });
  }

  private Subscription showDropdown(@NonNull final List<SearchSuggestionText> searchTexts,
      @Nullable Action0 subscriber) {
    Observable<Boolean> observable = createDropdownMenuObservable(searchTexts);
    return mShowDropdownSubscription =
        subscriber != null ? observable.doOnSubscribe(subscriber).subscribe()
            : observable.subscribe();
  }

  private Observable<Boolean> createDropdownMenuObservable(
      @NonNull final List<SearchSuggestionText> searchTexts) {
    return Observable.create(new Observable.OnSubscribe<Boolean>() {
      @Override public void call(Subscriber<? super Boolean> subscriber) {
        if (searchTexts.isEmpty()) {
          subscriber.onNext(false);
        } else {
          mAdapter.setSearchTexts(
              searchTexts.toArray(new SearchSuggestionText[searchTexts.size()]));
          subscriber.onNext(true);
        }
        subscriber.onCompleted();
      }
    }).doOnNext(aBoolean -> mAdapter.getFilter().filter(mSearchView.getText(), count -> {
      if (mAdapter != null && getContext() != null) {
        mAdapter.notifyDataSetChanged();
      }
      if (mSearchView != null
          && mSearchView.getContext() != null
          && mSearchView.getContext() instanceof Activity
          && !mSearchView.isPopupShowing()
          && mSearchView.hasFocus()) {
        mSearchView.showDropDown();
      }
    }));
  }

  private Observable<List<String>> getAutocompleteTexts(final String query) {
    return Observable.create(new Observable.OnSubscribe<List<String>>() {
      @Override public void call(final Subscriber<? super List<String>> subscriber) {
        new DataProvider((Activity) getContext(), QueryType.AUTOCOMPLETE, createReferrerBuilder()).responseTo(
            new BaseApiResponseListener<BaseResponse<Autocomplete>>(getContext()) {
              @Override public void onSuccess(@NonNull QueryType queryType,
                  @NonNull BaseResponse<Autocomplete> result) {
                super.onSuccess(queryType, result);
                String[] texts = result.getData().getSuggestions();
                subscriber.onNext(new ArrayList<>(Arrays.asList(texts)));
              }
            }).withParams(Collections.singletonMap("query", query)).request();
      }
    });
  }

  private Observable<List<SearchSuggestionText>> parseAutocompleteTexts(final List<String> texts) {
    return Observable.create(new Observable.OnSubscribe<List<SearchSuggestionText>>() {
      @Override public void call(Subscriber<? super List<SearchSuggestionText>> subscriber) {
        List<SearchSuggestionText> result = new ArrayList<>();
        for (int i = 0; i < texts.size(); i++) {
          result.add(new SearchSuggestionText(texts.get(i), false, i == 0));
        }
        subscriber.onNext(result);
        subscriber.onCompleted();
      }
    });
  }

  @Override public boolean onEditorAction(final TextView v, int actionId, KeyEvent event) {
    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
      canGoToSearchResult(v.getText().toString()).flatMap(aBoolean -> cacheQueryObservable(v.getText().toString()))
          .doOnNext(s -> {
            mSearchView.clearFocus();
            closeKeyboard();
          })
          .subscribe(mBuilder.mAction == null ? mSearchTextOnClickAction : mBuilder.mAction, Throwable::printStackTrace);
      return true;
    } else if (event.getAction() == KeyEvent.KEYCODE_BACK) {
      mSearchView.clearFocus();
      closeKeyboard();
    }
    return false;
  }

  @Override public void onFocusChange(View v, boolean hasFocus) {
    if (hasFocus && mBuilder.mEnableDropdown) {
      if (mAdapter.isEmpty()) {
        showDropdown(LatestSearchTextStore.getSearchTexts(getContext()), null);
      } else {
        mSearchView.showDropDown();
      }
    } else if (!hasFocus && mBuilder.mEnableDropdown) {
      dismissDropdown();
      closeKeyboard();
    }
  }

  @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

  }

  @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
    mDeleteButton.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
    if (s.length() == 0 && start == 0 && before == 1 && count == 0 && mBuilder.mEnableDropdown) {
      // 글자 수가 0보다 많다가 count가 0이 됨.. 과거 검색어를 보여줘야 함.
      showDropdown(LatestSearchTextStore.getSearchTexts(getContext()), () -> {
        if (mAutocompleteSubscription != null) {
          mAutocompleteSubscription.unsubscribe();
        }
      });
    }
  }

  @Override public void afterTextChanged(Editable s) {
    if (s != null && !TextUtils.isEmpty(s.toString()) && mBuilder.mEnableAutocomplete) {
      String query = s.toString().trim();
      if (mAutocompleteSubscription != null) {
        mAutocompleteSubscription.unsubscribe();
      }
      mAutocompleteSubscription = getAutocompleteTexts(query).flatMap(this::parseAutocompleteTexts)
          .delay(700, TimeUnit.MILLISECONDS)
          .flatMap(this::createDropdownMenuObservable)
          .observeOn(mMainThreadScheduler)
          .subscribeOn(mMainThreadScheduler)
          .subscribe();
    }
  }

  public static final class Builder {

    private final Context mContext;
    private boolean mEnableDropdown = false, mEnableCache = false, mEnableAutocomplete = false;
    private Style mStyle = Style.DARK;
    private String mHintText = null;
    private Action1<String> mAction = null;

    public Builder(@NonNull Context context) {
      mContext = context;
    }

    public Builder enableDropdown(boolean isEnabledDropdown) {
      mEnableDropdown = isEnabledDropdown;
      return this;
    }

    public Builder enableAutocomplete(boolean isEnableAutocomplete) {
      mEnableAutocomplete = isEnableAutocomplete;
      return this;
    }

    public Builder enableQueryCache(boolean isEnableQueryCache) {
      mEnableCache = isEnableQueryCache;
      return this;
    }

    public Builder setHintText(String hintText) {
      mHintText = hintText;
      return this;
    }

    public Builder setStyle(Style style) {
      mStyle = style;
      return this;
    }

    public Builder setOnSearchSubscriber(Action1<String> action) {
      mAction = action;
      return this;
    }

    public IncredibleSearchTextLayout build() {
      return new IncredibleSearchTextLayout(this);
    }

    public enum Style {
      DARK(R.color.heavy_purple, R.color.light_purple, R.color.white, R.color.light_purple,
          R.color.light_purple);

      @ColorRes private int mBackgroundColor = Color.WHITE;
      @ColorRes private int mSeparateColor = Color.WHITE;
      @ColorRes private int mTextColor = Color.WHITE;
      @ColorRes private int mTextHintColor = Color.WHITE;
      @ColorRes private int mDeleteButtonColor = Color.WHITE;

      Style(@ColorRes int backgroundColor, @ColorRes int separateColor, @ColorRes int textColor,
          @ColorRes int hintColor, @ColorRes int deleteButtonColor) {
        mBackgroundColor = backgroundColor;
        mSeparateColor = separateColor;
        mTextColor = textColor;
        mTextHintColor = hintColor;
        mDeleteButtonColor = deleteButtonColor;
      }

      public int getTextHintColor() {
        return mTextHintColor;
      }

      public int getTextColor() {
        return mTextColor;
      }

      public int getSeparateColor() {
        return mSeparateColor;
      }

      public int getBackgroundColor() {
        return mBackgroundColor;
      }

      public int getDeleteButtonColor() {
        return mDeleteButtonColor;
      }
    }
  }

  private static class SearchTextOnClickSubscriber {

    private final Context mContext;

    SearchTextOnClickSubscriber(@NonNull Context context) {
      mContext = context;
    }

    private Action1<String> getOnClickSubscriber() {
      return query -> {
        final Bundle b =
            new BundleSet.Builder().putPreviousScreenName(ScreenNameUtils.getScreenName(mContext))
                .putSearchQuery(query)
                .build()
                .getBundle();
        ActivityStarter.with(mContext, FragmentTask.SEARCH_RESULT)
            .addBundle(b)
            .setRequestCode(ActivityStarter.SEARCH_RESULT_REQUEST)
            .start();
      };
    }
  }
}
