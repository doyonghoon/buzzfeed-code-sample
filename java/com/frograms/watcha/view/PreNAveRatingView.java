package com.frograms.watcha.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.textviews.DefTextView;

public class PreNAveRatingView extends LinearLayout {

  private float mTextSize;

  @Bind(R.id.pre_rating_text) TextView mPreText;
  @Bind(R.id.pre_rating) DefTextView mPreRating;
  @Bind(R.id.ave_rating_text) TextView mAveText;
  @Bind(R.id.ave_rating) DefTextView mAveRating;

  public PreNAveRatingView(Context context) {
    super(context);
    init();
  }

  public PreNAveRatingView(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = context.obtainStyledAttributes(attrs, new int[] { android.R.attr.textSize });
    mTextSize = a.getDimension(0, 0.0f);

    a.recycle();

    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_card_ratings, this);

    ButterKnife.bind(this);

    if (!isInEditMode()) {
      mPreText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
      mPreRating.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);

      mAveText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
      mAveRating.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);

      FontIconDrawable drawable = (FontIconDrawable) mPreRating.getCompoundDrawables()[0];
      drawable.setTextColor(getResources().getColor(R.color.pink));
      drawable.setTextSize(mTextSize);
      mPreRating.updateCompoundDrawables();

      drawable = (FontIconDrawable) mAveRating.getCompoundDrawables()[0];
      drawable.setTextColor(getResources().getColor(R.color.heavy_gray));
      drawable.setTextSize(mTextSize);

      mAveRating.updateCompoundDrawables();
    }
  }

  public void setRating(float preRating, float aveRating) {
    if (preRating == 0) {
      mPreText.setVisibility(View.GONE);
      mPreRating.setVisibility(View.GONE);
    } else {
      mPreText.setVisibility(View.VISIBLE);
      mPreRating.setVisibility(View.VISIBLE);

      mPreRating.setText(preRating + "");
    }

    if (aveRating == 0) {
      mAveText.setVisibility(View.GONE);
      mAveRating.setVisibility(View.GONE);
    } else {
      mAveText.setVisibility(View.VISIBLE);
      mAveRating.setVisibility(View.VISIBLE);

      mAveRating.setText(aveRating + "");
    }
  }
}
