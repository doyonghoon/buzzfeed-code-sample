package com.frograms.watcha.view.drawers;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.model.Card;
import com.frograms.watcha.model.CardTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.list.ListData;
import com.frograms.watcha.model.views.ViewCard;
import com.frograms.watcha.observablescrollview.ObservableRecyclerView;
import com.frograms.watcha.recyclerview.DividerItemDecoration;
import com.frograms.watcha.retrofit.BaseApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.textviews.RobotoRegularView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * 카드 아이템을 그릴 수 있는 드로우어.
 */
public class CardBottomSheet extends FrameLayout {

  @Bind(R.id.list) ObservableRecyclerView mRecyclerView;
  @Bind(R.id.fake_ac_btn1) RobotoRegularView mCreateDeckButton;
  @Bind(R.id.fake_ac_title) RobotoRegularView mTitleView;

  protected RecyclerView.LayoutManager mLayoutManager;
  protected RecyclerAdapter mAdapter;
  protected final Builder mBuilder;

  protected boolean mHasNext;
  protected int page = 1;

  protected int getLayoutId() {
    return R.layout.view_bottom_sheet;
  }

  public CardBottomSheet(@NonNull Builder builder) {
    super(builder.mContext);
    mBuilder = builder;
    inflate(getContext(), getLayoutId(), this);
    ButterKnife.bind(this);
    initCardView();
    mTitleView.setText(mBuilder.mTitle);
    mCreateDeckButton.setOnClickListener(mBuilder.mButtonClickListener);
  }

  protected void initCardView() {
    mRecyclerView.setLayoutManager(getLayoutManager());
    mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getSpans()));
    mAdapter = new RecyclerAdapter(getContext(), null);
    mAdapter.setIsUseGrid((getLayoutManager() instanceof LinearLayoutManager) ? false : true);
    mAdapter.setOnClickCardItemListener(mBuilder.mCardItemClickListener);
    mRecyclerView.setAdapter(mAdapter);
  }

  protected RecyclerView.LayoutManager getLayoutManager() {
    if (mLayoutManager == null) {
      mLayoutManager = new StaggeredGridLayoutManager(mRecyclerView.getSpans(), StaggeredGridLayoutManager.VERTICAL);
      //mLayoutManager = new StaggeredGridLayoutManager(getContext());
    }

    return mLayoutManager;
  }

  public RecyclerAdapter getAdapter() {
    return mAdapter;
  }

  public Builder getBuilder() {
    return mBuilder;
  }

  protected Observable<ListData> getData(@NonNull final Activity activity,
      @NonNull final QueryType queryType, @Nullable final Map<String, String> params) {
    return Observable.create(new Observable.OnSubscribe<ListData>() {
      @Override public void call(final Subscriber<? super ListData> subscriber) {
        DataProvider<BaseResponse<ListData>> provider =
            new DataProvider<>(activity, queryType, createReferrerBuilder(activity));
        provider.withParams(params);
        provider.responseTo(new BaseApiResponseListener<BaseResponse<ListData>>(activity) {
          @Override
          public void onSuccess(@NonNull QueryType queryType1, @NonNull BaseResponse<ListData> result) {
            if (result.getData() != null) {
              subscriber.onNext(result.getData());
              subscriber.onCompleted();
            } else {
              subscriber.onError(new Throwable("데이터를 불러오지 못한듯.."));
            }
          }
        }).request();
      }
    });
  }

  private ReferrerBuilder createReferrerBuilder(Activity activity) {
    if (activity != null && activity instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) activity).getFragment();
      return new ReferrerBuilder(baseFragment.getCurrentScreenName());
    }
    return null;
  }

  public Observable<ListData> setHasNext(@NonNull final ListData data) {
    return Observable.create(new Observable.OnSubscribe<ListData>() {
      @Override public void call(Subscriber<? super ListData> subscriber) {
        mHasNext = data.hasNext();
        subscriber.onNext(data);
        subscriber.onCompleted();
      }
    });
  }

  public Observable<List<ViewCard>> parseCards(@NonNull final ListData data) {
    return Observable.create(new Observable.OnSubscribe<List<ViewCard>>() {
      @Override public void call(Subscriber<? super List<ViewCard>> subscriber) {
        mAdapter.setHasNext(data.hasNext());
        ArrayList<Card> cards = data.getCards();
        CardTask cardTask = new CardTask(cards);
        subscriber.onNext(cardTask.getViewCards());
        subscriber.onCompleted();
      }
    });
  }

  protected Map<String, String> getParams() {
    Map<String, String> map = new HashMap<>();
    if (mBuilder.mParams != null) {
      map.putAll(mBuilder.mParams);
    }
    map.put("page", String.valueOf(page));
    page++;
    return map;
  }

  protected void setData() {
    getData((Activity) getContext(), mBuilder.mQueryType, getParams())
        .flatMap(new Func1<ListData, Observable<? extends ListData>>() {
          @Override public Observable<? extends ListData> call(ListData data) {
            return CardBottomSheet.this.setHasNext(data);
          }
        })
        .flatMap(new Func1<ListData, Observable<? extends List<ViewCard>>>() {
          @Override public Observable<? extends List<ViewCard>> call(ListData data) {
            return CardBottomSheet.this.parseCards(data);
          }
        }).subscribe(new Action1<List<ViewCard>>() {
      @Override public void call(List<ViewCard> viewCards) {
        if (mHasNext) {
          CardBottomSheet.this.addLoadMore(viewCards);
        }
        mAdapter.addItemsToTop(viewCards);
        mAdapter.notifyDataSetChanged();
        if (CardBottomSheet.this.getLayoutManager() instanceof LinearLayoutManager) {
          ((LinearLayoutManager) CardBottomSheet.this.getLayoutManager()).scrollToPositionWithOffset(viewCards
              .size(), 0);
        }
      }
    }, new Action1<Throwable>() {
      @Override public void call(Throwable throwable) {
        throwable.printStackTrace();
      }
    });
  }

  @Override protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    AnalyticsManager.sendScreenName(AnalyticsManager.ADD_MOVIE_INTO_DECK);
    mAdapter.clearItems();
    setData();
  }

  protected boolean getScrollToBottom() {
    return false;
  }

  public void addLoadMore(List<ViewCard> viewCards) {
  }

  private Class<? extends BaseFragment> getContextClass() {
    if (mBuilder.mContext instanceof BaseActivity) {
      return ((BaseActivity) mBuilder.mContext).getFragment().getClass();
    }
    return null;
  }

  public static class Builder {

    public final Context mContext;
    public QueryType mQueryType;
    public Map<String, String> mParams;
    public View.OnClickListener mButtonClickListener;
    public RecyclerAdapter.OnClickCardItemListener mCardItemClickListener;
    public String mTitle;

    public Builder(@NonNull Context context) {
      mContext = context;
    }

    public Builder setQueryType(@NonNull QueryType queryType) {
      mQueryType = queryType;
      return this;
    }

    public Builder setParams(@Nullable Map<String, String> params) {
      mParams = params;
      return this;
    }

    public Builder setTitle(String title) {
      mTitle = title;
      return this;
    }

    public Builder setButtonClickListener(View.OnClickListener listener) {
      mButtonClickListener = listener;
      return this;
    }

    public Builder setOnCardItemClickListener(RecyclerAdapter.OnClickCardItemListener listener) {
      mCardItemClickListener = listener;
      return this;
    }

    public CardBottomSheet build() {
      return new CardBottomSheet(this);
    }
  }
}
