package com.frograms.watcha.view.drawers;

import android.content.Context;
import android.util.AttributeSet;
import com.flipboard.bottomsheet.BottomSheetLayout;

public class WBottomSheetLayout extends BottomSheetLayout {

  private boolean mPassPeek = false;
  private boolean isOpened = false;

  public WBottomSheetLayout(Context context) {
    super(context);
  }

  public WBottomSheetLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public WBottomSheetLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public WBottomSheetLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  public void shouldPassPeek(boolean passPeek) {
    mPassPeek = passPeek;
  }

  @Override public void peekSheet() {
    if (mPassPeek) {
      if (isOpened) {
        dismissSheet();
      } else {
        expandSheet();
        isOpened = true;
      }
    } else {
      super.peekSheet();
    }
  }

  @Override public void dismissSheet() {
    super.dismissSheet();

    isOpened = false;
  }

  @Override public float getPeekSheetTranslation() {
    return this.hasFullHeightSheet() ? (float) (this.getHeight() / 2) : (float) this.getSheetView().getHeight();
  }

  private boolean hasFullHeightSheet() {
    return this.getSheetView() == null || this.getSheetView().getHeight() == this.getHeight();
  }
}
