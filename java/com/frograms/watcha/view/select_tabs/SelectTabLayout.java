package com.frograms.watcha.view.select_tabs;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.R;
import com.frograms.watcha.view.widget.ActionGridLayout;

/**
 * 발견 화면 리스트뷰에 붙는 뷰.
 */
public class SelectTabLayout extends ActionGridLayout {

  public SelectTabLayout(Context context) {
    this(context, null, 0);
  }

  public SelectTabLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SelectTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.toolbar_height));
    setPadding(0, getResources().getDimensionPixelOffset(R.dimen.view_5dp) * 3, 0, 0);
    setBackgroundColor(getResources().getColor(R.color.card_margin_color));
  }

  public void addTab(String name, SelectTabView.OnTabSelectListener listener) {
    SelectTabView view = new SelectTabView(getContext());
    view.setText(name);
    view.setTag(getChildCount());
    view.setOnTabSelectListener(this, listener);

    addView(view);
  }

  public SelectTabView getSelectableButton(int position) {
    if (getChildCount() > position) {
      return (SelectTabView) getChildAt(position);
    }
    return null;
  }

  public void selectTabPosition(int position) {
    if (getChildCount() > position) {
      for (int Loop1 = 0; Loop1 < getChildCount(); Loop1++) {
        ((SelectTabView) getChildAt(Loop1)).setFocused(position == Loop1);
      }
    }
  }

  @Override protected boolean hasWeight() {
    return false;
  }
}
