package com.frograms.watcha.view.select_tabs;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.view.widget.IconActionGridButton;

/**
 * 발견 화면 상단에 고정 탭처럼 붙는 레이아웃의 아이템 뷰.
 */
public class SelectTabView extends IconActionGridButton implements View.OnClickListener {

  public interface OnTabSelectListener {
    void onClickSelectableTab(SelectTabLayout layout, SelectTabView v, int position);
  }

  private OnTabSelectListener mOnTabSelectListener;
  private SelectTabLayout mLayout;

  public SelectTabView(Context context) {
    this(context, null, 0);
  }

  public SelectTabView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SelectTabView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    setTextSize(getResources().getDimension(R.dimen.select_tab_text_font));
    mBackgroundResId = R.color.background;
    mFocusedBackgroundResId = R.color.background;

    setFocused(false);
    setOnClickListener(this);
  }

  @Override protected int getTextColorNormal() {
    return getResources().getColor(R.color.gray);
  }

  @Override protected int getTextColorFocused() {
    return getResources().getColor(R.color.pink);
  }

  @Override public void onClick(View v) {
    if (mOnTabSelectListener != null && v instanceof SelectTabView) {
      mOnTabSelectListener.onClickSelectableTab(mLayout, (SelectTabView) v, (Integer) getTag());
    }
  }

  public void setOnTabSelectListener(SelectTabLayout layout, OnTabSelectListener listener) {
    mLayout = layout;
    mOnTabSelectListener = listener;
  }
}
