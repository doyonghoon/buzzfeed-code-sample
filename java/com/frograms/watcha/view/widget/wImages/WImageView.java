package com.frograms.watcha.view.widget.wImages;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.animation.ViewPropertyAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

public class WImageView extends ImageView {

  public interface OnFinishLoadingImageListener {
    void onFinishLoadingImage(@Nullable Bitmap bitmap, WImageView view);
  }

  private Bitmap mStoredBitmap = null;
  private Bitmap mDefaultBitmap = null, mBorderBitmap = null;
  private boolean hasMadeBitmap = false;
  private boolean canAnimate = true;

  private ImageType mImageType = ImageType.NONE;
  private RatioType mRatioType = RatioType.NONE;

  private ShadowDrawable mShadowDrawable = null;
  private boolean isCircle = false, hasRadius = false;

  private WImageAnimationType mAnimationType = WImageAnimationType.SATURATION;
  private OnFinishLoadingImageListener mListener;
  private BitmapDrawable mPlaceholderDrawable, mErrorDrawable;

  private int mBorderWidth = 0;
  private ScaleType mScaleType = ScaleType.FIT_XY;

  private int mCustomPlaceholderId = 0, mCustomErrorResId = 0;

  public WImageView(Context context) {
    this(context, null, 0);
  }

  public WImageView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public WImageView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(attrs);
  }

  private void init(@Nullable AttributeSet attrs) {
    if (attrs != null && getContext() != null) {
      TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.WImageView);
      if (a.hasValue(R.styleable.WImageView_imageType)) {
        setImageType(ImageType.getType(a.getInt(R.styleable.WImageView_imageType, -1)));
      }

      if (a.hasValue(R.styleable.WImageView_ratioType)) {
        setRatioType(RatioType.getType(a.getInt(R.styleable.WImageView_ratioType, -1)));
      }

      if (a.hasValue(R.styleable.WImageView_animation)) {
        mAnimationType =
            WImageAnimationType.getAnimationType(a.getInt(R.styleable.WImageView_animation, 0));
      }

      if (a.hasValue(R.styleable.WImageView_shadow)) {
        setShadow(ShadowDrawable.Orientation.getOrientation(a.getInt(R.styleable.WImageView_shadow,
            ShadowDrawable.Orientation.BOTTOM.getAttributeValue())));
      }

      if (a.hasValue(R.styleable.WImageView_border)) {
        int defaultSelectorSize =
            (int) (2 * getContext().getResources().getDisplayMetrics().density + 0.5f);
        setBorderWidth(
            a.getDimensionPixelOffset(R.styleable.WImageView_border, defaultSelectorSize));
      }

      if (a.hasValue(R.styleable.WImageView_circle)) {
        setCircle(a.getBoolean(R.styleable.WImageView_circle, isCircle));
      }

      if (a.hasValue(R.styleable.WImageView_radius)) {
        setRadius(a.getBoolean(R.styleable.WImageView_radius, hasRadius));
      }

      if (a.hasValue(R.styleable.WImageView_placeholder)) {
        mCustomPlaceholderId = a.getResourceId(R.styleable.WImageView_placeholder, 0);
      }

      if (a.hasValue(R.styleable.WImageView_error)) {
        mCustomErrorResId = a.getResourceId(R.styleable.WImageView_error, 0);
      }

      a.recycle();
    }
  }

  public WImageView setImageType(ImageType type) {
    mImageType = type;
    setScaleType(ScaleType.FIT_XY);
    switch (mImageType) {
      case PROFILE:
        setScaleType(ScaleType.FIT_XY);
        setRatioType(RatioType.EQUALS);
        break;

      case POSTER:
        setScaleType(ScaleType.FIT_XY);
        setRatioType(RatioType.POSTER);
        break;

      case STILLCUT:
        setScaleType(ScaleType.CENTER_CROP);
        setRatioType(RatioType.STILLCUT);
        break;

      case CAMPAIGN:
        setScaleType(ScaleType.CENTER_CROP);
        setRatioType(RatioType.CAMPAIGN);
      case NONE:
      default:
        break;
    }

    mPlaceholderDrawable = null;
    mErrorDrawable = null;
    return this;
  }

  public WImageView setRatioType(RatioType type) {
    mRatioType = type;
    return this;
  }

  public WImageView setAnimation(WImageAnimationType animationType) {
    mAnimationType = animationType;
    return this;
  }

  public WImageView setShadow(ShadowDrawable.Orientation orientation) {
    mShadowDrawable = new ShadowDrawable(getContext(), orientation);
    return this;
  }

  public WImageView setBorderWidth(int width) {
    mBorderWidth = width;
    return this;
  }

  //public WImageView setBorderColor(int parsedColor) {
  //  mBorderColor = parsedColor;
  //  return this;
  //}

  public WImageView setCircle(boolean isCircle) {
    this.isCircle = isCircle;
    return this;
  }

  public WImageView setRadius(boolean hasRadius) {
    this.hasRadius = hasRadius;
    return this;
  }

  public boolean getRadius() {
    return hasRadius;
  }

  @Override public void setScaleType(ScaleType type) {
    super.setScaleType(type);
    if (type != ScaleType.CENTER) mScaleType = type;
  }

  public void setOnFinishLoadingImageListener(OnFinishLoadingImageListener listener) {
    mListener = listener;
  }

  //public void enableRipple() {
  //  RippleDrawable rippleDrawable = new RippleDrawableCompat(0x42ffffff, null, getContext(), RippleDrawable.Style.Over);
  //  rippleDrawable.setCallback(this);
  //  rippleDrawable.setHotspotEnabled(true);
  //  setRippleDrawable(rippleDrawable);
  //}

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    final int width = MeasureSpec.getSize(widthMeasureSpec);
    if (width > 0 && mRatioType != null && mRatioType != RatioType.NONE) {
      // 타입이 정해진 경우만 비율에 맞춰서 높이 값을 변경함.
      final int height = (int) (width / mRatioType.getSizeRatio());
      setMeasuredDimension(width, height);
    } else {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    if (mStoredBitmap != null) {
      setImageBitmap(mStoredBitmap);
      mStoredBitmap = null;
    }
  }

  private void drawShadowIfNeeded(Canvas canvas, Drawable d) {
    if (d != null && mShadowDrawable != null) {
      Drawable shadow = mShadowDrawable.getDrawable();
      if (shadow != null) {
        Rect rect = new Rect(0, 0, getWidth(), getHeight());
        shadow.setBounds(rect);
        shadow.draw(canvas);
        canvas.save();
        canvas.restore();
      }
    }
  }

  private BitmapDrawable getPlaceholderDrawable(int width, int height) {
    if (mPlaceholderDrawable == null) {
      int resId =
          (mCustomPlaceholderId == 0) ? mImageType.getPlaceholderId() : mCustomPlaceholderId;
      if (resId != 0) mPlaceholderDrawable = DrawableHelper.getDrawable(resId, width, height);
    }

    return mPlaceholderDrawable;
  }

  private BitmapDrawable getErrorDrawable(int width, int height) {
    if (mErrorDrawable == null) {
      int resId = (mCustomErrorResId == 0) ? mImageType.getErrorId() : mCustomErrorResId;

      if (resId != 0) mErrorDrawable = DrawableHelper.getDrawable(resId, width, height);
    }

    return mErrorDrawable;
  }

  public void load(String url) {
    boolean canLoad = false;
    if (getTag(R.id.TAG_URL) == null) {
      canLoad = true;
      canAnimate = true;
    } else if (!getTag(R.id.TAG_URL).equals(url)) {
      canLoad = true;
      canAnimate = false;
    }

    if (canLoad) {
      setTag(R.id.TAG_URL, url);
      load(Glide.with(getContext()).load(url), url);
    }
  }

  public void loadFromMediaStore(Uri uri) {
    load(Glide.with(getContext()).loadFromMediaStore(uri), uri.getPath());
  }

  public void loadResource(@DrawableRes int resourceId) {
    load(Glide.with(getContext()).load(resourceId), resourceId + ":" + System.currentTimeMillis());
  }

  private void load(DrawableTypeRequest request, String transformerKey) {
    BitmapRequestBuilder builder = request.asBitmap();
    if (canAnimate) {
      ViewPropertyAnimation.Animator animator = mAnimationType.getAnimator();
      builder.animate(animator);
    }

    builder.placeholder(getPlaceholderDrawable(getMeasuredWidth(), getMeasuredHeight()));
    builder.error(getErrorDrawable(getMeasuredWidth(), getMeasuredHeight()));

    builder.into(new BitmapImageViewTarget(this) {
      private void placeIconAsErrorImage(@DimenRes int dimenResId) {
        setBackgroundColor(getResources().getColor(R.color.placeholder));
        setScaleType(ScaleType.CENTER);
        FontIconDrawable drawable = FontIconDrawable.inflate(getContext(), R.xml.icon_image);
        drawable.setTextSize(getResources().getDimension(dimenResId));
        drawable.setTextColor(getResources().getColor(R.color.white));
        setImageDrawable(drawable);
        canAnimate = false;
      }

      @Override protected void setResource(Bitmap resource) {
        super.setResource(resource);
        canAnimate = false;
      }

      @Override public void onLoadFailed(Exception e, Drawable errorDrawable) {
        super.onLoadFailed(e, errorDrawable);
        if (mListener != null) {
          mListener.onFinishLoadingImage(null, WImageView.this);
        }

        if (mErrorDrawable == null) {
          switch (mImageType) {
            case POSTER:
              placeIconAsErrorImage(R.dimen.error_image_font);
              break;
            case STILLCUT:
            case CAMPAIGN:
              placeIconAsErrorImage(R.dimen.error_image_stillcut_font);
              break;
          }
        }
        canAnimate = false;
      }

      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        if (mListener != null) {
          mListener.onFinishLoadingImage(resource, WImageView.this);
        }

        if (mScaleType != null) setScaleType(mScaleType);

        super.onResourceReady(resource, glideAnimation);
      }
    });
  }

  public Bitmap getBitmap() {
    if (getDrawable() != null && (getDrawable() instanceof BitmapDrawable)) {
      return ((BitmapDrawable) getDrawable()).getBitmap();
    }
    return null;
  }

  @Override public void setImageDrawable(Drawable drawable) {
    if (!hasMadeBitmap && drawable instanceof BitmapDrawable && getScaleType().equals(
        ScaleType.FIT_XY)) {
      setImageBitmap(((BitmapDrawable) drawable).getBitmap());
      return;
    }

    hasMadeBitmap = false;
    super.setImageDrawable(drawable);
  }

  private Bitmap getDefaultBitmap(int width, int height) {
    if (mDefaultBitmap == null) {
      mDefaultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

      Canvas canvas = new Canvas(mDefaultBitmap);
      canvas.drawARGB(255, 255, 255, 255);

      final Paint paint = new Paint();
      paint.setColor(getResources().getColor(android.R.color.transparent));
      paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

      if (isCircle) {
        canvas.drawCircle(width / 2, height / 2, width / 2, paint);
      } else if (hasRadius) {
        Rect rect = new Rect(0, 0, width, height);
        RectF rectF = new RectF(rect);

        float radius = getResources().getDimension(R.dimen.image_radius);
        canvas.drawRoundRect(rectF, radius, radius, paint);
      } else {
        canvas.drawRect(0, 0, width, height, paint);
      }
    }

    return mDefaultBitmap;
  }

  private Bitmap getBorderBitmap(int width, int height) {
    if (mBorderBitmap == null) {
      mBorderBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

      Canvas canvas = new Canvas(mBorderBitmap);
      canvas.drawARGB(0, 0, 0, 0);

      canvas.drawBitmap(getDefaultBitmap(width, height), 0, 0, null);

      final Paint paint = new Paint();
      paint.setColor(0xffffffff);

      if (isCircle) {
        canvas.drawCircle(width / 2, height / 2, width / 2 - mBorderWidth, paint);
      } else if (hasRadius) {
        Rect rect =
            new Rect(mBorderWidth, mBorderWidth, width - mBorderWidth, height - mBorderWidth);
        RectF rectF = new RectF(rect);

        float radius = getResources().getDimension(R.dimen.image_radius);
        canvas.drawRoundRect(rectF, radius, radius, paint);
      } else {
        canvas.drawRect(mBorderWidth, mBorderWidth, width - mBorderWidth, height - mBorderWidth,
            paint);
      }

      paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
      canvas.drawRect(0, 0, width, height, paint);
    }

    return mBorderBitmap;
  }

  public BitmapShader createBitmapShader(@NonNull Bitmap image, int canvasWidth,
      boolean hasBorder) {
    BitmapShader shader = null;
    if (image != null) {
      final int w = (hasBorder ? canvasWidth : image.getWidth());
      final int h = (hasBorder ? canvasWidth : image.getHeight());
      shader =
          new BitmapShader(Bitmap.createScaledBitmap(image, w, h, false), Shader.TileMode.CLAMP,
              Shader.TileMode.CLAMP);

      if (!hasBorder) {
        RectF drawableRect = new RectF(0, 0, w, h);
        RectF viewRect = new RectF(((canvasWidth - (w / 2)) / 2) - (w / 2),
            ((canvasWidth - (h / 2)) / 2) - (h / 2), canvasWidth, canvasWidth);
        Matrix m = new Matrix();
        m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
        shader.setLocalMatrix(m);
      }
    }
    return shader;
  }

  @Override protected void onDraw(@NonNull Canvas canvas) {
    super.onDraw(canvas);

    if (mBorderWidth > 0) {
      canvas.drawBitmap(getBorderBitmap(canvas.getWidth(), canvas.getHeight()), 0, 0, null);
    }
    canvas.save();
    canvas.restore();
    drawShadowIfNeeded(canvas, getDrawable());
  }

  @Override public void setImageBitmap(Bitmap bitmap) {

    if (!getScaleType().equals(ScaleType.FIT_XY)) {
      super.setImageBitmap(bitmap);
      return;
    }

    if (getMeasuredWidth() == 0 || getMeasuredHeight() == 0) {
      mStoredBitmap = bitmap;
      return;
    }

    int width = getMeasuredWidth();
    int height = getMeasuredHeight();
    bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
    Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

    Canvas canvas = new Canvas(output);
    canvas.drawBitmap(bitmap, 0, 0, null);

    Paint paint = new Paint();
    paint.setShader(createBitmapShader(bitmap, width, false));
    paint.setAntiAlias(true);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    canvas.drawBitmap(getDefaultBitmap(width, height), 0, 0, paint);

    hasMadeBitmap = true;
    super.setImageBitmap(output);
  }

  public static class ShadowDrawable {

    public enum Orientation {
      TOP(0),
      BOTTOM(1);

      private int mAttributeValue;

      Orientation(int attributeValue) {
        mAttributeValue = attributeValue;
      }

      int getAttributeValue() {
        return mAttributeValue;
      }

      public static Orientation getOrientation(int attributeValue) {
        for (Orientation o : Orientation.values()) {
          if (o.mAttributeValue == attributeValue) {
            return o;
          }
        }
        return null;
      }
    }

    private Context mContext;
    private Orientation mOrientation = Orientation.BOTTOM;

    ShadowDrawable(Context context, Orientation orientation) {
      mContext = context;
      mOrientation = orientation;
    }

    public Drawable getDrawable() {
      switch (mOrientation) {
        case TOP:
          return mContext.getResources().getDrawable(R.drawable.shadow_vertical_linear_top);
        default:
        case BOTTOM:
          return mContext.getResources().getDrawable(R.drawable.shadow_vertical_linear);
      }
    }
  }
}