package com.frograms.watcha.view.widget.wImages;

import android.animation.ValueAnimator;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import com.bumptech.glide.request.animation.ViewPropertyAnimation;
import com.frograms.watcha.view.widget.WImageAnimationListener;
import com.nineoldandroids.view.ViewHelper;

/**
 * 이미지뷰가 로드될때 적용할 애니메이션.
 */
public enum WImageAnimationType implements WImageAnimationListener {
  SATURATION(0) {
    @Override public ViewPropertyAnimation.Animator getAnimator() {
      return new ViewPropertyAnimation.Animator() {
        @Override public void animate(final View view) {
          final ValueAnimator animator = ValueAnimator.ofFloat(0.0F, 1.0F);
          final AccelerateDecelerateInterpolator interpolator =
              new AccelerateDecelerateInterpolator();
          animator.setInterpolator(interpolator);
          animator.setDuration(800L);
          animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            ColorMatrix saturationMatrix = new ColorMatrix();
            ColorMatrix brightnessMatrix = new ColorMatrix();

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
              float fraction = animator.getAnimatedFraction();
              this.saturationMatrix.setSaturation((Float) animator.getAnimatedValue());
              float scale =
                  2.0F - interpolator.getInterpolation(Math.min(fraction * 4.0F / 3.0F, 1.0F));
              this.brightnessMatrix.setScale(scale, scale, scale,
                  interpolator.getInterpolation(Math.min(fraction * 2.0F, 1.0F)));
              this.saturationMatrix.preConcat(this.brightnessMatrix);
              if (view instanceof WImageView) {
                WImageView v = (WImageView) view;
                v.setColorFilter(new ColorMatrixColorFilter(this.saturationMatrix));
              }
            }
          });
          animator.start();
        }
      };
    }
  },
  POP(1) {
    @Override public ViewPropertyAnimation.Animator getAnimator() {
      return new ViewPropertyAnimation.Animator() {
        @Override public void animate(final View view) {
          if (view.getVisibility() != View.VISIBLE) {
            ViewCompat.setAlpha(view, 0.0F);
          }

          ValueAnimator animator = ValueAnimator.ofFloat(ViewHelper.getAlpha(view), 1.0F);
          animator.setDuration(200L);
          animator.setInterpolator(new DecelerateInterpolator());
          animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator valueAnimator) {
              float value = (Float) valueAnimator.getAnimatedValue();
              ViewCompat.setAlpha(view, value);
              ViewCompat.setScaleX(view, value);
              ViewCompat.setScaleY(view, value);
            }
          });
          animator.start();
        }
      };
    }
  },
  FADE(2) {
    @Override public ViewPropertyAnimation.Animator getAnimator() {
      return new ViewPropertyAnimation.Animator() {
        @Override public void animate(final View view) {
          if (view.getVisibility() != View.VISIBLE) {
            ViewCompat.setAlpha(view, 0.0F);
          }
          com.nineoldandroids.animation.ValueAnimator animator =
              com.nineoldandroids.animation.ValueAnimator.ofFloat(ViewHelper.getAlpha(view), 1.0F);
          animator.setDuration(200L);
          animator.setInterpolator(new DecelerateInterpolator());
          animator.addUpdateListener(
              new com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(
                    com.nineoldandroids.animation.ValueAnimator valueAnimator) {
                  float value = (Float) valueAnimator.getAnimatedValue();
                  ViewCompat.setAlpha(view, value);
                }
              });
          animator.start();
        }
      };
    }
  },
  FLY(3) {
    @Override public ViewPropertyAnimation.Animator getAnimator() {
      return new ViewPropertyAnimation.Animator() {
        @Override public void animate(final View view) {
          if (view.getVisibility() != View.VISIBLE) {
            ViewCompat.setAlpha(view, 0.0F);
          }
          ValueAnimator animator = ValueAnimator.ofFloat(ViewHelper.getAlpha(view), 1.0F);
          animator.setDuration(200L);
          animator.setInterpolator(new DecelerateInterpolator());
          animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override public void onAnimationUpdate(ValueAnimator valueAnimator) {
              float value = (Float) valueAnimator.getAnimatedValue();
              ViewCompat.setAlpha(view, value);
              ViewCompat.setTranslationY(view, (float) view.getHeight() / 2.0F * (1.0F - value));
            }
          });
          animator.start();
        }
      };
    }
  };

  private int mAttributeValue = -1;

  WImageAnimationType(int attributeValue) {
    mAttributeValue = attributeValue;
  }

  public int getAttributeValue() {
    return mAttributeValue;
  }

  public static WImageAnimationType getAnimationType(int attributeValue) {
    for (WImageAnimationType t : WImageAnimationType.values()) {
      if (t.getAttributeValue() == attributeValue) {
        return t;
      }
    }
    return null;
  }
}
