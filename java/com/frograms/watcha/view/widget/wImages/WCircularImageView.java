package com.frograms.watcha.view.widget.wImages;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.imagehelper.BaseShader;
import com.frograms.watcha.helpers.imagehelper.CircleShader;

/**
 * Created by doyonghoon on 15. 5. 22..
 */
public class WCircularImageView extends ShaderImageView {

  public WCircularImageView(Context context) {
    this(context, null, 0);
  }

  public WCircularImageView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public WCircularImageView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context, attrs, defStyle);
  }

  @Override public BaseShader createImageViewShader() {
    return new CircleShader();
  }
}
