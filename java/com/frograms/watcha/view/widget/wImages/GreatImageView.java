package com.frograms.watcha.view.widget.wImages;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.DimenRes;
import android.util.AttributeSet;
import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.imagehelper.BaseShader;
import com.frograms.watcha.helpers.imagehelper.CircleShader;
import com.frograms.watcha.helpers.imagehelper.RoundedShader;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

/**
 * 이미지 변형이나 캐싱이나 뭐든 다 해주는 이미지뷰.
 */
public class GreatImageView extends ShaderImageView {

  private ImageType mImageType = ImageType.NONE;
  private RatioType mRatioType = RatioType.NONE;
  private boolean mIsCircle = false, mHasRadius = false;
  protected int mPlaceholderResId = 0, mErrorResId = 0;

  private BitmapImageViewTarget mListener;

  public GreatImageView(Context context) {
    this(context, null, 0);
  }

  public GreatImageView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public GreatImageView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    if (attrs != null) {
      TypedArray typedArray =
          context.obtainStyledAttributes(attrs, R.styleable.WImageView, defStyleAttr, 0);

      mImageType = ImageType.getType(
          typedArray.getInt(R.styleable.WImageView_imageType, mImageType.getAttributeValue()));
      mRatioType = RatioType.getType(
          typedArray.getInt(R.styleable.WImageView_ratioType, mRatioType.getAttributeValue()));
      mIsCircle = typedArray.getBoolean(R.styleable.WImageView_circle, mIsCircle);
      mHasRadius = typedArray.getBoolean(R.styleable.WImageView_radius, mHasRadius);
      mPlaceholderResId =
          typedArray.getResourceId(R.styleable.WImageView_placeholder, mPlaceholderResId);
      mErrorResId = typedArray.getResourceId(R.styleable.WImageView_error, mErrorResId);
      typedArray.recycle();
    }

    init(context, attrs, defStyleAttr);
  }

  @Override protected BaseShader createImageViewShader() {
    return mIsCircle ? new CircleShader() : new RoundedShader();
  }

  public void setOnFinishLoadingImageListener(BitmapImageViewTarget listener) {
    mListener = listener;
  }

  public GreatImageView setImageType(ImageType type) {
    mImageType = type;
    invalidate();
    requestLayout();
    return this;
  }

  public GreatImageView setRatioType(RatioType type) {
    mRatioType = type;
    invalidate();
    requestLayout();
    return this;
  }

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    final int width = MeasureSpec.getSize(widthMeasureSpec);
    if (width > 0 && mRatioType != null && mRatioType != RatioType.NONE) {
      // 타입이 정해진 경우만 비율에 맞춰서 높이 값을 변경함.
      final int height = (int) (width / mRatioType.getSizeRatio());
      setMeasuredDimension(width, height);
    } else {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
  }

  private int getPlaceholderDrawableId() {
    if (mPlaceholderResId == 0) {
      mPlaceholderResId = mImageType.getPlaceholderId();
    }

    return mPlaceholderResId;
  }

  private int getErrorDrawableId() {
    if (mErrorResId == 0) {
      mErrorResId = mImageType.getErrorId();
    }

    return mErrorResId;
  }

  public void load(String url) {
    BitmapRequestBuilder builder = Glide.with(getContext()).load(url).asBitmap();

    builder.placeholder(DrawableHelper.getDrawable(getPlaceholderDrawableId(), 0, 0));
    builder.error(getErrorDrawableId());

    builder.into(new BitmapImageViewTarget(this) {

      private void placeIconAsErrorImage(@DimenRes int dimenResId) {
        setBackgroundColor(getResources().getColor(R.color.placeholder));
        setScaleType(ScaleType.CENTER);
        FontIconDrawable drawable = FontIconDrawable.inflate(getContext(), R.xml.icon_image);
        drawable.setTextSize(getResources().getDimension(dimenResId));
        drawable.setTextColor(getResources().getColor(R.color.white));
        setImageDrawable(drawable);
      }

      @Override public void onLoadFailed(Exception e, Drawable errorDrawable) {
        super.onLoadFailed(e, errorDrawable);
        if (mListener != null) {
          mListener.onLoadFailed(e, errorDrawable);
          return;
        }

        if (mErrorResId == 0) {
          switch (mImageType) {
            case POSTER:
              placeIconAsErrorImage(R.dimen.error_image_font);
              break;
            case STILLCUT:
            case CAMPAIGN:
              placeIconAsErrorImage(R.dimen.error_image_stillcut_font);
              break;
          }
        }
      }

      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        super.onResourceReady(resource, glideAnimation);
        if (mListener != null) {
          mListener.onResourceReady(resource, glideAnimation);
        }
      }

      @Override public void onLoadCleared(Drawable placeholder) {
        super.onLoadCleared(placeholder);
        if (mListener != null) {
          mListener.onLoadCleared(placeholder);
        }
      }

      @Override public void onLoadStarted(Drawable placeholder) {
        super.onLoadStarted(placeholder);
        if (mListener != null) {
          mListener.onLoadStarted(placeholder);
        }
      }
    });
  }
}
