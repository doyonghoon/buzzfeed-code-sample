package com.frograms.watcha.view.widget.wImages;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.imagehelper.BaseShader;
import com.frograms.watcha.helpers.imagehelper.RoundedShader;

/**
 * Created by doyonghoon on 15. 5. 22..
 */
public class RoundedImageView extends ShaderImageView {

  public RoundedImageView(Context context) {
    super(context);
  }

  public RoundedImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override public BaseShader createImageViewShader() {
    return new RoundedShader();
  }
}
