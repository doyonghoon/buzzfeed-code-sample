package com.frograms.watcha.view.widget.wImages;

public enum RatioType {
  EQUALS(0, 1),
  POSTER(1, 0.7),
  STILLCUT(2, 1.8),
  STILLCUT_COVER(3, 1.565),
  STILLCUT_GALLERY(4, 1.427),
  BANNER(5, 2.666),
  CAMPAIGN(6, 1.7778),
  COVER_SMALL(7, 1.895),
  COVER_LARGE(8, 1.565),
  NONE(9, 0);

  private int mAttributeValue = -1;
  private double mRatio;

  RatioType(int attrValue, double ratio) {
    mAttributeValue = attrValue;
    mRatio = ratio;
  }

  public double getSizeRatio() {
    return mRatio;
  }

  public int getAttributeValue() {
    return mAttributeValue;
  }

  public static RatioType getType(int attributeValue) {
    for (RatioType t : values()) {
      if (t.getAttributeValue() == attributeValue) {
        return t;
      }
    }
    return null;
  }
}
