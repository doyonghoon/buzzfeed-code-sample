package com.frograms.watcha.view.widget.wImages;

import com.frograms.watcha.R;

public enum ImageType {
  PROFILE(0, R.color.placeholder, R.drawable.placeholder_profile),

  POSTER(1, R.color.placeholder, 0),

  STILLCUT(2, R.color.placeholder, 0),

  CAMPAIGN(3, R.color.placeholder, 0),

  NONE(4, 0, 0);

  private int mAttributeValue = -1;
  private int mPlaceholderId, mErrorId;

  ImageType(int attrValue, int placeholderId, int errorId) {
    mAttributeValue = attrValue;
    mPlaceholderId = placeholderId;
    mErrorId = errorId;
  }

  public int getAttributeValue() {
    return mAttributeValue;
  }

  public int getPlaceholderId() {
    return mPlaceholderId;
  }

  public int getErrorId() {
    return mErrorId;
  }

  public static ImageType getType(int attributeValue) {
    for (ImageType t : values()) {
      if (t.getAttributeValue() == attributeValue) {
        return t;
      }
    }
    return null;
  }
}
