package com.frograms.watcha.view.widget.wImages;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.frograms.watcha.helpers.imagehelper.BaseShader;

/**
 * Created by doyonghoon on 15. 5. 22..
 */
public abstract class ShaderImageView extends ImageView {

  private final static boolean DEBUG = false;
  private BaseShader pathShader;

  public ShaderImageView(Context context) {
    super(context, null, 0);
  }

  public ShaderImageView(Context context, AttributeSet attrs) {
    super(context, attrs, 0);
  }

  public ShaderImageView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  /**
   * Shader 생성함.
   *
   * @see #createImageViewShader()
   * @see BaseShader#init(Context, AttributeSet, int)
   */
  protected void init(Context context, AttributeSet attrs, int defStyle) {
    pathShader = null;
    getPathShader().init(context, attrs, defStyle);
  }

  protected BaseShader getPathShader() {
    if (pathShader == null) {
      pathShader = createImageViewShader();
    }
    return pathShader;
  }

  protected abstract BaseShader createImageViewShader();

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    if (getPathShader().ismSquare()) {
      int width = getMeasuredWidth();
      int height = getMeasuredHeight();
      int dimen = Math.min(width, height);
      setMeasuredDimension(dimen, dimen);
    }
  }

  //Required by path helper
  @Override public void setImageBitmap(Bitmap bm) {
    super.setImageBitmap(bm);
    getPathShader().onImageDrawableReset(getDrawable());
  }

  @Override public void setImageDrawable(Drawable drawable) {
    super.setImageDrawable(drawable);
    getPathShader().onImageDrawableReset(getDrawable());
  }

  @Override public void setImageResource(int resId) {
    super.setImageResource(resId);
    getPathShader().onImageDrawableReset(getDrawable());
  }

  @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    getPathShader().onSizeChanged(w, h);
  }

  @Override public void onDraw(Canvas canvas) {
    if (DEBUG) {
      canvas.drawRGB(10, 200, 200);
    }

    if (!getPathShader().onDraw(canvas)) {
      super.onDraw(canvas);
    }
  }

  public void load(String url) {
    BitmapRequestBuilder builder = Glide.with(getContext())
        .load(url)
        .asBitmap()
        .animate(WImageAnimationType.SATURATION.getAnimator());

    //if (mPlaceholderResId > 0) {
    //  builder.placeholder(mPlaceholderResId);
    //} else {
    //  builder.placeholder(DrawableHelper.getDrawable(mImageType.getPlaceholderId(),
    //      getMeasuredWidth() > 0 ? 50 : 0, getMeasuredHeight() > 0 ? 50 : 0));
    //}

    //if (mErrorResId > 0) {
    //  builder.error(mErrorResId);
    //} else {
    //  builder.error(DrawableHelper.getDrawable(mImageType.getPlaceholderId(),
    //      getMeasuredWidth() > 0 ? 50 : 0, getMeasuredHeight() > 0 ? 50 : 0));
    //}
    builder.fitCenter();
    builder.into(new BitmapImageViewTarget(this) {
      @Override public void onLoadFailed(Exception e, Drawable errorDrawable) {
        super.onLoadFailed(e, errorDrawable);
        //if (mListener != null) {
        //  mListener.onFinishLoadingImage(null, GreatImageView.this);
        //}
      }

      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        //if (mListener != null) {
        //  mListener.onFinishLoadingImage(resource, GreatImageView.this);
        //}
        super.onResourceReady(resource, glideAnimation);
      }
    });
  }
}
