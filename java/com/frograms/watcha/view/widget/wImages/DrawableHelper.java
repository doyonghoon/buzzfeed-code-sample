package com.frograms.watcha.view.widget.wImages;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import com.frograms.watcha.application.WatchaApp;

public class DrawableHelper {

  public static BitmapDrawable getDrawable(int resId, int w, int h) {
    if (resId == 0) return null;

    Drawable drawable = WatchaApp.getInstance().getResources().getDrawable(resId);

    if (drawable instanceof BitmapDrawable) {
      return (BitmapDrawable) drawable;
    } else if (drawable instanceof ColorDrawable) {
      Bitmap bitmap =
          Bitmap.createBitmap(w > 0 ? w : 100, h > 0 ? h : 100, Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bitmap);
      drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
      drawable.draw(canvas);

      return new BitmapDrawable(WatchaApp.getInstance().getResources(), bitmap);
    }

    return null;
  }
}
