package com.frograms.watcha.view.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.graphics.Palette;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class GradationPosterView extends RelativeLayout {

  @Bind(R.id.poster) WImageView mPosterView;
  @Bind(R.id.palette1) View mPaletteView1;
  @Bind(R.id.palette2) View mPaletteView2;

  private String mUrl;
  private GradientDrawable mDrawable;

  public GradationPosterView(Context context) {
    this(context, null, 0);
  }

  public GradationPosterView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public GradationPosterView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  protected void init() {
    inflate(getContext(), R.layout.view_gradation_poster, this);
    ButterKnife.bind(this);

    //setBackgroundColor(getResources().getColor(R.color.white));
    //this.getViewTreeObserver().addOnGlobalLayoutListener(this);
  }

  public void load(String url) {
    mUrl = url;
    mPosterView.load(url);

    Glide.with(getContext()).load(url).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>() {
      @Override
      public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
        drawPalette(bitmap);
      }
    });
  }

  protected void drawPalette(Bitmap bitmap) {
    Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
      @Override public void onGenerated(Palette palette) {
        int defaultColor = getResources().getColor(R.color.black);

        int color =
            palette.getLightMutedSwatch() != null ? palette.getLightMutedColor(defaultColor)
                : palette.getDarkMutedColor(defaultColor);

        mPaletteView1.setBackgroundColor(color);

        mDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
            new int[] { color, 0x00000000 });
        mPaletteView2.setBackground(mDrawable);
      }
    });
  }

  //@Override public void onGlobalLayout() {
  //  WLog.e(this.getMeasuredWidth() + "");
  //  mPosterView.getLayoutParams().width = (int)(this.getMeasuredWidth() * 0.75);
  //  mPaletteView1.getLayoutParams().width = (int)(this.getMeasuredWidth() * 0.4);
  //  mPaletteView2.getLayoutParams().width = (int)(this.getMeasuredWidth() * 0.2);
  //  this.invalidate();
  //  //mPosterView.invalidate();
  //  //mPaletteView1.invalidate();
  //  //mPaletteView2.invalidate();
  //  this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
  //}

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    final int width = MeasureSpec.getSize(widthMeasureSpec);
    final int height = (int) (width / RatioType.STILLCUT.getSizeRatio());
    mPosterView.getLayoutParams().width = (int) (width * 0.75);
    mPaletteView1.getLayoutParams().width = (int) (width * 0.4);
    mPaletteView2.getLayoutParams().width = (int) (width * 0.2);

    setMeasuredDimension(width, height);
  }
}