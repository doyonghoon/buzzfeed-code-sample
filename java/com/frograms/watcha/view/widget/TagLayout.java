package com.frograms.watcha.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.utils.WLog;

/**
 * 태그를 그룹으로 묶어놓은 레이아웃.
 */
public class TagLayout extends RelativeLayout implements View.OnClickListener {

  public enum Align {
    LEFT(0),
    CENTER(1);

    private int mId;

    Align(int id) {
      mId = id;
    }

    public static Align getAlign(int id) {
      for (Align a : values()) {
        if (a.mId == id) {
          return a;
        }
      }
      return LEFT;
    }

    public int getRule() {
      switch (mId) {
        default:
        case 0:
          return RelativeLayout.ALIGN_PARENT_LEFT;
        case 1:
          return RelativeLayout.CENTER_HORIZONTAL;
      }
    }
  }

  public interface OnTagItemClickListener {
    void onClickTagItem(TagLayout layout, TagTextView v, TagItems.Tag tag);
  }

  @Bind(R.id.tag_layout_title) TextView mTitle;
  @Bind(R.id.tags_layout) RowLayout mRowLayout;
  @Bind(R.id.tags_divider) View mDivider;

  private Align mAlign = Align.LEFT;
  private String mTitleText = null;
  private OnTagItemClickListener mClickListener = null;

  public TagLayout(Context context) {
    this(context, null, 0);
  }

  public TagLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TagLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(attrs);
  }

  private void init(AttributeSet attrs) {
    inflate(getContext(), R.layout.view_tag_layout, this);
    ButterKnife.bind(this);
    if (attrs != null && getContext() != null) {
      TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TagLayout);
      mTitleText = a.getString(R.styleable.TagLayout_tag_title);
      boolean hasDivider = a.getBoolean(R.styleable.TagLayout_tag_divider, true);
      mDivider.setVisibility(hasDivider ? View.VISIBLE : View.GONE);

      if (a.hasValue(R.styleable.TagLayout_tag_align)) {
        mAlign = Align.getAlign(a.getInt(R.styleable.TagLayout_tag_align, 0));
        WLog.i("align: " + mAlign.getRule());
      }
      a.recycle();
    }
    setTitle(mTitleText);
    RelativeLayout.LayoutParams params =
        new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    params.addRule(RelativeLayout.BELOW, mTitle.getId());
    params.addRule(mAlign.getRule());
    mRowLayout.setLayoutParams(params);
  }

  public void setOnClickTagItemListener(OnTagItemClickListener listener) {
    mClickListener = listener;
  }

  public void setTitle(String title) {
    mTitle.setText(mTitleText = title);
    mTitle.setVisibility(TextUtils.isEmpty(mTitleText) ? View.GONE : View.VISIBLE);
  }

  public void addTag(TagItems.Tag tag, boolean isFocused) {
    TagTextView v = createTagTextView(tag, isFocused);
    v.setOnClickListener(this);
    mRowLayout.addView(v);
  }

  public TagTextView getTagTextView(TagItems.Tag tag) {
    for (int i = 0; i < mRowLayout.getChildCount(); i++) {
      if (mRowLayout.getChildAt(i) instanceof TagTextView) {
        TagTextView tmp = (TagTextView) mRowLayout.getChildAt(i);
        if (tmp.getTagData().equals(tag)) {
          return tmp;
        }
      }
    }
    return null;
  }

  public void removeTagTextView(TagItems.Tag tag) {
    for (int i = 0; i < mRowLayout.getChildCount(); i++) {
      if (mRowLayout.getChildAt(i) instanceof TagTextView) {
        TagTextView tmp = (TagTextView) mRowLayout.getChildAt(i);
        if (tmp.getTagData().equals(tag)) {
          mRowLayout.removeViewAt(i);
        }
      }
    }
  }

  private TagTextView createTagTextView(TagItems.Tag tag, boolean isFocused) {
    TagTextView v = new TagTextView(getContext(), tag);
    v.setFocused(isFocused);
    return v;
  }

  @Override public void onClick(View v) {
    if (v instanceof TagTextView && mClickListener != null) {
      TagTextView tagTextView = (TagTextView) v;
      TagItems.Tag tagData = tagTextView.getTagData();
      mClickListener.onClickTagItem(TagLayout.this, tagTextView, tagData);
    }
  }
}
