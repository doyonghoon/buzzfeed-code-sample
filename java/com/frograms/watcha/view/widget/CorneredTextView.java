package com.frograms.watcha.view.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.Button;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;

/**
 * 텍스트를 cornered-radius 로 둘러싼 stroke 이 있는 뷰.
 */
public class CorneredTextView extends Button {

  private boolean isSelected = false;
  private int mCornerRadius = 0;
  private int mBackgroundColor = Color.TRANSPARENT;
  private int mBackgroundSelectedColor = Color.TRANSPARENT;
  private int mTextSelectedColor = Color.BLACK;
  private ColorStateList mTextColor = null;

  public CorneredTextView(Context context) {
    this(context, null, 0);
  }

  public CorneredTextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public CorneredTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(attrs);
    if (!isInEditMode()) {
      FontHelper.RobotoRegular(this);
    }
  }

  private void init(AttributeSet attrs) {
    if (getContext() != null && attrs != null) {
      TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CorneredTextView);
      if (a != null) {
        if (a.hasValue(R.styleable.CorneredTextView_cornered_radius)) {
          setCornerRadius(a.getDimensionPixelSize(R.styleable.CorneredTextView_cornered_radius, 0));
        }

        if (a.hasValue(R.styleable.CorneredTextView_cornered_background_color)) {
          setBackgroundColor(
              a.getColor(R.styleable.CorneredTextView_cornered_background_color, Color.WHITE));
        }

        if (a.hasValue(R.styleable.CorneredTextView_cornered_background_selected_color)) {
          setBackgroundSelectedColor(
              a.getColor(R.styleable.CorneredTextView_cornered_background_selected_color,
                  Color.WHITE));
        }

        if (a.hasValue(R.styleable.CorneredTextView_cornered_text_selected_color)) {
          setTextSelectedColor(
              a.getColor(R.styleable.CorneredTextView_cornered_text_selected_color, Color.BLACK));
        }
        a.recycle();
      }
    }
    mTextColor = getTextColors();
  }

  @Override public void setTextColor(int color) {
    this.mTextColor = ColorStateList.valueOf(color);
    super.setTextColor(color);
  }

  @Override public void setTextColor(ColorStateList colors) {
    this.mTextColor = colors;
    super.setTextColor(colors);
  }

  public void setCornerRadius(int px) {
    //super.setCornerRadius(px);
    mCornerRadius = px;
  }

  @Override public void setBackgroundColor(int parsedColor) {
    mBackgroundColor = parsedColor;
  }

  public void setBackgroundSelectedColor(int parsedColor) {
    mBackgroundSelectedColor = parsedColor;
  }

  public void setTextSelectedColor(int parsedColor) {
    mTextSelectedColor = parsedColor;
  }

  @Override protected void onDraw(@NonNull Canvas canvas) {
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
    final RectF rectF = new RectF(rect);

    paint.setAntiAlias(true);
    paint.setColor(isSelected ? mBackgroundSelectedColor : mBackgroundColor);
    paint.setStyle(Paint.Style.FILL);
    canvas.drawColor(Color.TRANSPARENT);
    canvas.drawRoundRect(rectF, mCornerRadius, mCornerRadius, paint);

    paint.setColor(Color.TRANSPARENT);
    paint.setStyle(Paint.Style.STROKE);
    canvas.drawRoundRect(rectF, mCornerRadius, mCornerRadius, paint);
    super.onDraw(canvas);
  }

  //@Override public boolean dispatchTouchEvent(@NonNull MotionEvent event) {
  //  if (!this.isClickable()) {
  //    this.isSelected = false;
  //    return super.onTouchEvent(event);
  //  }
  //
  //  switch (event.getAction()) {
  //    case MotionEvent.ACTION_DOWN:
  //      this.isSelected = true;
  //      break;
  //    case MotionEvent.ACTION_UP:
  //    case MotionEvent.ACTION_SCROLL:
  //    case MotionEvent.ACTION_OUTSIDE:
  //    case MotionEvent.ACTION_CANCEL:
  //    case MotionEvent.ACTION_HOVER_EXIT:
  //      this.isSelected = false;
  //      break;
  //  }
  //  // invalidate() 는 텍스트 컬러가 바뀔때 어차피 불리니까 여기선 안불러도 됨. 중복으로 부를 필요없음.
  //  setTextColor(isSelected ? ColorStateList.valueOf(mTextSelectedColor) : mTextColor);
  //  return super.dispatchTouchEvent(event);
  //}
}
