package com.frograms.watcha.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.utils.ViewUtil;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

/**
 * 검색, 프로필페이지에 붙는 컨텐츠 버튼.
 */
public class ContentShortcutView extends FrameLayout {

  @Bind(R.id.shortcut_cover) GreatImageView mCover;
  @Bind(R.id.shortcut_title) TextView mTitle;
  @Bind(R.id.shortcut_icon) FontIconTextView mIcon;
  @Bind(R.id.shortcut_shadow) View mShadowView;

  private String mDefaultIcon = null;
  private String mTitleText = null;
  private int mIconSizeInSp = 0;
  private int mTitleSizeInSp = 0;
  private int mTitleColor;
  private Drawable mBackgroundDrawable;

  public ContentShortcutView(Context context) {
    this(context, null, 0);
  }

  public ContentShortcutView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ContentShortcutView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Shortcut, defStyleAttr, 0);
    if (a.hasValue(R.styleable.Shortcut_shortcut_default_icon)) {
      mDefaultIcon = a.getString(R.styleable.Shortcut_shortcut_default_icon);
    }

    if (a.hasValue(R.styleable.Shortcut_shortcut_title)) {
      mTitleText = a.getString(R.styleable.Shortcut_shortcut_title);
    }

    if (a.hasValue(R.styleable.Shortcut_shortcut_default_icon_size)) {
      mIconSizeInSp =
          a.getDimensionPixelSize(R.styleable.Shortcut_shortcut_default_icon_size, mIconSizeInSp);
    }

    if (a.hasValue(R.styleable.Shortcut_shortcut_title_size)) {
      mTitleSizeInSp = a.getDimensionPixelSize(R.styleable.Shortcut_shortcut_title_size, 23);
    }

    if (a.hasValue(R.styleable.Shortcut_shortcut_title_color)) {
      mTitleColor = a.getColor(R.styleable.Shortcut_shortcut_title_color, Color.WHITE);
    }

    if (a.hasValue(R.styleable.Shortcut_shortcut_background)) {
      mBackgroundDrawable = a.getDrawable(R.styleable.Shortcut_shortcut_background);
    }

    init();
    a.recycle();
  }

  private void init() {
    inflate(getContext(), R.layout.view_item_shortcut, this);
    ButterKnife.bind(this);

    ViewUtil.setBackground(this, mBackgroundDrawable);
    setTitle(mTitleText);
    setTitleSize(mTitleSizeInSp);
    setTitleColor(mTitleColor);
    setIcon(mDefaultIcon);
    setIconTextSize(mIconSizeInSp);
    mCover.setOnFinishLoadingImageListener(new BitmapImageViewTarget(mCover) {
      @Override
      public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        super.onResourceReady(resource, glideAnimation);

        boolean showIcon =
            !(resource != null && resource.getWidth() > 0 && resource.getHeight() > 0);
        mShadowView.setVisibility(showIcon ? View.GONE : View.VISIBLE);
        showIcon(showIcon);
      }
    });

    mCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
  }

  public void setCover(String url) {
    mCover.load(url);
  }

  public void showIcon(boolean show) {
    mIcon.setVisibility(show ? View.VISIBLE : View.GONE);
  }

  public void setIcon(String icon) {
    mIcon.setText(icon);
  }

  public void setIconTextSize(int sizeInSp) {
    mIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInSp);
  }

  public void setTitle(String title) {
    mTitle.setText(title);
  }

  public void setTitleSize(int sizeInSp) {
    mTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInSp);
  }

  public void setTitleColor(int color) {
    mTitle.setTextColor(color);
    mIcon.setTextColor(color);
  }
}
