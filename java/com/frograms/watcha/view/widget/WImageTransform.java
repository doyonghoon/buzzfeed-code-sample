package com.frograms.watcha.view.widget;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;

/**
 * {@Wlink WImageView} 에서 사용하는 비트맵 변형하는 클래스.
 */
public abstract class WImageTransform implements Transformation<Bitmap> {
  public abstract Bitmap transform(Bitmap source);
}
