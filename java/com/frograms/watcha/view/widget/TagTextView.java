package com.frograms.watcha.view.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.XmlRes;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import com.facebook.rebound.BaseSpringSystem;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.TagSettingFragment;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.utils.WLog;

/**
 * {@link TagSettingFragment} 화면에서 사용하거나, TAGS 카드에서 사용하는 뷰.
 */
public class TagTextView extends CorneredTextView {

  private static int PADDING = 0;
  private static int TEXT_NORMAL_COLOR = Color.WHITE;
  private static int TEXT_NORMAL_SELECTED_COLOR = Color.WHITE;

  private static int TEXT_FOCUSED_COLOR = Color.WHITE;
  private static int TEXT_FOCUSED_SELECTED_COLOR = Color.WHITE;

  private static int BACKGROUND_COLOR = Color.WHITE;
  private static int BACKGROUND_SELECTED_COLOR = Color.WHITE;

  private static int BACKGROUND_FOCUSED_COLOR = Color.WHITE;
  private static int BACKGROUND_FOCUSED_SELECTED_COLOR = Color.WHITE;

  private boolean isFocused = false;
  public TagItems.Tag mTag = null;

  /**
   * touch-feedback 애니메이션.
   */
  private final BaseSpringSystem mSpringSystem = SpringSystem.create();
  private Spring mScaleSpring = null;
  private TagTouchFeedbackSpringListener mSpringListener = null;
  private GestureDetector mGestureDetector = null;

  public TagTextView(Context context) {
    this(context, null, 0);
  }

  public TagTextView(Context context, TagItems.Tag tag) {
    this(context, null, 0);
    setTag(tag);
  }

  public TagTextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TagTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    TEXT_NORMAL_COLOR = context.getResources().getColor(R.color.heavy_gray);
    TEXT_NORMAL_SELECTED_COLOR = context.getResources().getColor(R.color.white);

    TEXT_FOCUSED_COLOR = context.getResources().getColor(R.color.white);
    TEXT_FOCUSED_SELECTED_COLOR = context.getResources().getColor(R.color.white);

    BACKGROUND_COLOR = context.getResources().getColor(R.color.background);
    BACKGROUND_SELECTED_COLOR = context.getResources().getColor(R.color.background);

    BACKGROUND_FOCUSED_COLOR = context.getResources().getColor(R.color.pink);
    BACKGROUND_FOCUSED_SELECTED_COLOR = context.getResources().getColor(R.color.pink);

    PADDING = context.getResources().getDimensionPixelSize(R.dimen.view_3dp);
    int PADDING1 = context.getResources().getDimensionPixelSize(R.dimen.view_5dp) / 5;
    int padding8 = PADDING + (PADDING1 * 3);

    setGravity(Gravity.CENTER);
    setPadding(PADDING * 2, padding8, PADDING * 2, padding8);
    setTextSize(13.5f);
    setCornerRadius(getResources().getDimensionPixelSize(R.dimen.view_5dp));
    setFocused(false);
    mGestureDetector = new GestureDetector(getContext(), new SingleTapConfirm());

    // 애니메이션 초기화
    mScaleSpring = mSpringSystem.createSpring();
  }

  @Override protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (mSpringListener == null) {
      mSpringListener = new TagTouchFeedbackSpringListener(this);
    }
    mScaleSpring.addListener(mSpringListener);
  }

  @Override protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (mSpringListener != null) {
      mScaleSpring.removeListener(mSpringListener);
    }
  }

  public void setTag(TagItems.Tag tag) {
    mTag = tag;
    if (mTag != null) {
      setId(mTag.getId());
      setText(mTag.getName());
    }
  }

  public boolean isFocused() {
    return this.isFocused;
  }

  private ColorStateList getTextColor(boolean isFocused) {
    XmlResourceParser xrp =
        getResources().getXml(isFocused ? R.drawable.text_tag_sel : R.drawable.text_tag_nor);
    try {
      return ColorStateList.createFromXml(getResources(), xrp);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public void setFocused(boolean isFocused) {
    this.isFocused = isFocused;
    ColorStateList textColor = getTextColor(isFocused);
    if (this.isFocused) {
      setTextColor(TEXT_FOCUSED_COLOR);
      setTextSelectedColor(TEXT_FOCUSED_SELECTED_COLOR);
      setBackgroundColor(BACKGROUND_FOCUSED_COLOR);
      setBackgroundSelectedColor(BACKGROUND_FOCUSED_SELECTED_COLOR);
    } else {
      setTextColor(TEXT_NORMAL_COLOR);
      setTextSelectedColor(TEXT_NORMAL_SELECTED_COLOR);
      setBackgroundColor(BACKGROUND_COLOR);
      setBackgroundSelectedColor(BACKGROUND_SELECTED_COLOR);
    }
    if (textColor != null) {
      setTextColor(textColor);
    }
    invalidate();
  }

  public TagItems.Tag getTagData() {
    return mTag;
  }

  @Override public boolean onTouchEvent(@NonNull MotionEvent event) {
    boolean result = super.onTouchEvent(event);
    if (mGestureDetector.onTouchEvent(event)) {
      mScaleSpring.setEndValue(0);
      return true;
    } else {
      switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
          mScaleSpring.setEndValue(1);
          return true;
        case MotionEvent.ACTION_UP:
        case MotionEvent.ACTION_CANCEL:
          mScaleSpring.setEndValue(0);
          return true;
      }
    }
    return result;
  }

  private static class TagTouchFeedbackSpringListener extends SimpleSpringListener {

    private final TagTextView mTargetView;

    private TagTouchFeedbackSpringListener(TagTextView v) {
      mTargetView = v;
    }

    @Override public void onSpringUpdate(Spring spring) {
      float mappedValue =
          (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, 1, 0.85);
      mTargetView.setScaleX(mappedValue);
      mTargetView.setScaleY(mappedValue);
    }
  }

  private static class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
    @Override public boolean onSingleTapConfirmed(MotionEvent e) {
      WLog.i("onClick ");
      return true;
    }
  }
}
