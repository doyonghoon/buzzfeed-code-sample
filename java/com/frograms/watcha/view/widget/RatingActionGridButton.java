package com.frograms.watcha.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.view.textviews.RobotoMediumView;

/**
 * {@link ActionGridLayout} 에 들어가는 평가에 특화된 버튼.
 */
public class RatingActionGridButton extends AbsActionGridButton {

  @Bind(R.id.grid_rating) protected RobotoMediumView mRatingTextView;
  @Bind(R.id.grid_text) protected TextView mTitle;

  private float mRating = 0.0f;

  public RatingActionGridButton(Context context) {
    this(context, null, 0);
  }

  public RatingActionGridButton(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public RatingActionGridButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    //mTitle.setText(mTextStr);
    mTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);

    mRatingTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mIconSize);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_rating_grid_button;
  }

  @Override protected void init() {
    super.init();
    if (!isInEditMode()) {
      FontHelper.RobotoRegular(mTitle);
      FontHelper.FontIcon(mRatingTextView);
    }
    mRatingTextView.setText(R.string.icon_rate_action);
    mRatingTextView.setTextColor(mParsedTextColorNormal);
    mRatingTextView.setBackgroundResource(0);
  }

  public float getRating() {
    return mRating;
  }

  public void setRating(float rating) {
    mRating = rating;
    if (rating > 0) {
      if (!isInEditMode()) {
        FontHelper.RobotoMedium(mRatingTextView);
      }
      mRatingTextView.setText(rating + "");
      mRatingTextView.setTextColor(getResources().getColor(R.color.white));
      mRatingTextView.setBackgroundResource(R.drawable.bg_pink_rating);
      mTitle.setText(R.string.rated);
    } else {
      if (!isInEditMode()) {
        FontHelper.FontIcon(mRatingTextView);
      }
      mRatingTextView.setText(R.string.icon_rate_action);
      mRatingTextView.setTextColor(mParsedTextColorNormal);
      mRatingTextView.setBackgroundResource(0);
      mTitle.setText(R.string.rating);
    }
    setFocused(rating > 0);
  }

  @Override protected int getTextColorNormal() {
    return getResources().getColor(R.color.heavy_gray);
  }

  /**
   * 버튼이 눌려지게 함.
   *
   * @param focused 버튼이 눌려지냐 안눌려지냐를 결정함.
   */
  @Override public void setFocused(boolean focused) {
    super.setFocused(focused);

    mTitle.setTextColor(focused ? mParsedTextColorFocused : mParsedTextColorNormal);

    //Drawable iconDrawable = getResources().getDrawable(R.drawable.icon_card_rate_nor);
    //if (focused) {
    //  iconDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    //}
    //mRatingTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, iconDrawable, null);
  }
}