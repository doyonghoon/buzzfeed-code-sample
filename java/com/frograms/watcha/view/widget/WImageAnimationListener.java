package com.frograms.watcha.view.widget;

import com.bumptech.glide.request.animation.ViewPropertyAnimation;

/**
 * Created by doyonghoon on 15. 4. 13..
 */
public interface WImageAnimationListener {
  ViewPropertyAnimation.Animator getAnimator();
}
