package com.frograms.watcha.view.widget;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;

/**
 * Glide 로 이미지 불러올때, 원형으로 그림.
 */
public class WImageCircleTransform extends WImageTransform {

  private static final String ID = "WImageView_Circular_ImageView_Transformation";

  private BitmapPool mBitmapPool;
  private String mUrl;
  private int mBorderWidth;
  private int mBorderColor;

  public WImageCircleTransform(String url, BitmapPool pool, int borderWidth, int borderColor) {
    mBitmapPool = pool;
    mUrl = url;
    mBorderWidth = borderWidth;
    mBorderColor = borderColor;
  }

  @Override
  public Resource<Bitmap> transform(Resource<Bitmap> resource, int outWidth, int outHeight) {
    return BitmapResource.obtain(transform(resource.get()), mBitmapPool);
  }

  @Override public Bitmap transform(Bitmap source) {
    Bitmap output = Bitmap.createBitmap(source.getWidth() + (mBorderWidth * 2),
        source.getHeight() + (mBorderWidth * 2), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    int circleCenter = source.getWidth() / 2;

    Paint paint = new Paint();
    paint.setAntiAlias(true);
    BitmapShader shader = new BitmapShader(
        Bitmap.createScaledBitmap(source, output.getWidth(), output.getHeight(), false),
        Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
    paint.setShader(shader);

    Paint paintBorder = new Paint();
    paintBorder.setAntiAlias(true);
    paintBorder.setColor(mBorderColor);
    canvas.drawCircle(circleCenter + mBorderWidth, circleCenter + mBorderWidth,
        circleCenter + mBorderWidth - 4.0f, paintBorder);
    canvas.drawCircle(circleCenter + mBorderWidth, circleCenter + mBorderWidth, circleCenter - 4.0f,
        paint);

    return output;
  }

  @Override public String getId() {
    return ID + "," + mBorderWidth + "," + mBorderColor + ":" + mUrl;
  }
}
