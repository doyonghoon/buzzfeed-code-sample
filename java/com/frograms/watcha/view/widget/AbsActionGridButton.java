package com.frograms.watcha.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import carbon.drawable.RippleDrawable;
import carbon.drawable.RippleDrawableCompat;
import com.frograms.watcha.R;

/**
 * 액션 그리드 버튼.
 */
public abstract class AbsActionGridButton extends carbon.widget.LinearLayout {

  protected int mParsedTextColorNormal = 0;
  protected int mParsedTextColorFocused = 0;

  protected boolean mIsFocused = false;

  protected String mTextStr, mIconStr;
  protected int mTextSize, mIconSize;

  protected int mBackgroundResId = Color.TRANSPARENT, mFocusedBackgroundResId;
  protected int mIconColor;
  protected boolean mEnableRipple = true;

  public AbsActionGridButton(Context context) {
    this(context, null, 0);
  }

  public AbsActionGridButton(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public AbsActionGridButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    inflate(getContext(), getLayoutId(), this);
    ButterKnife.bind(this);

    int[] textSizeAttr = new int[] { android.R.attr.background };
    final TypedArray b = getContext().obtainStyledAttributes(attrs, textSizeAttr);
    mBackgroundResId = b.getResourceId(0, 0);
    b.recycle();

    final TypedArray a =
        getContext().obtainStyledAttributes(attrs, R.styleable.ActionGridButton, defStyle, 0);

    setGravity(Gravity.CENTER);
    for (int Loop1 = 0; Loop1 < getChildCount(); Loop1++) {
      ((LinearLayout.LayoutParams) getChildAt(Loop1).getLayoutParams()).gravity = Gravity.CENTER;
    }

    if (a.hasValue(R.styleable.ActionGridButton_btn_orientation)) {
      String orientation = a.getString(R.styleable.ActionGridButton_btn_orientation);
      setOrientation(orientation.equals("vertical") ? VERTICAL : HORIZONTAL);
    }

    if (a.hasValue(R.styleable.ActionGridButton_btn_text)) {
      mTextStr = a.getString(R.styleable.ActionGridButton_btn_text);
    }

    if (a.hasValue(R.styleable.ActionGridButton_btn_text_size)) {
      mTextSize = a.getDimensionPixelSize(R.styleable.ActionGridButton_btn_text_size, 0);
    }

    if (a.hasValue(R.styleable.ActionGridButton_btn_icon)) {
      mIconStr = a.getString(R.styleable.ActionGridButton_btn_icon);
    }

    if (a.hasValue(R.styleable.ActionGridButton_btn_icon_size)) {
      mIconSize = a.getDimensionPixelSize(R.styleable.ActionGridButton_btn_icon_size, 0);
    }

    if (a.hasValue(R.styleable.ActionGridButton_btn_icon_color)) {
      mIconColor = a.getColor(R.styleable.ActionGridButton_btn_icon_color, 0);
    } else {
      mIconColor = getTextColorNormal();
    }

    //if (a.hasValue(R.styleable.ActionGridButton_btn_text_color)) {
    //  mIconColor = a.getColor(R.styleable.ActionGridButton_btn_text_color, 0);
    //} else {
    //  mIconColor = getTextColorNormal();
    //}

    if (a.hasValue(R.styleable.ActionGridButton_btn_focus_backgrond)) {
      mFocusedBackgroundResId =
          a.getResourceId(R.styleable.ActionGridButton_btn_focus_backgrond, 0);
    } else {
      mFocusedBackgroundResId = mBackgroundResId;
    }
    mEnableRipple = a.getBoolean(R.styleable.ActionGridButton_btn_ripple, true);

    a.recycle();
    init();
    this.showRipple(mEnableRipple);
  }

  protected abstract int getLayoutId();

  protected int getTextColorNormal() {
    return getResources().getColor(R.color.gray);
  }

  protected int getTextColorFocused() {
    return getResources() != null ? getResources().getColor(R.color.pink) : Color.WHITE;
  }

  protected void init() {
    mParsedTextColorFocused = getTextColorFocused();
    mParsedTextColorNormal = getTextColorNormal();

    setFocused(false);
  }

  public void showRipple(boolean enable) {
    mEnableRipple = enable;
    if (mEnableRipple) {
      RippleDrawable rippleDrawable =
          new RippleDrawableCompat(0x4263616d, null, getContext(), RippleDrawable.Style.Over);
      rippleDrawable.setCallback(this);
      rippleDrawable.setHotspotEnabled(true);
      this.setRippleDrawable(rippleDrawable);
    } else {
      this.setRippleDrawable(null);
    }
  }

  /**
   * 버튼이 눌려져 있나 확인.
   */
  public boolean isFocused() {
    return mIsFocused;
  }

  /**
   * 버튼이 눌려지게 함.
   *
   * @param focused 버튼이 눌려지냐 안눌려지냐를 결정함.
   */
  public void setFocused(boolean focused) {
    setBackgroundResource((mIsFocused = focused) ? mFocusedBackgroundResId : mBackgroundResId);
  }
}