package com.frograms.watcha.view.widget;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.view.textviews.FontIconTextView;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * 액션 그리드 버튼.
 */
public class IconActionGridButton extends AbsActionGridButton {

  @Bind(R.id.grid_icon) protected FontIconTextView mIcon;
  @Bind(R.id.grid_text) protected TextView mTitle;

  public IconActionGridButton(Context context) {
    this(context, null, 0);
  }

  public IconActionGridButton(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public IconActionGridButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    mTitle.setText(mTextStr);
    mTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);

    if (TextUtils.isEmpty(mIconStr)) {
      mIcon.setVisibility(View.GONE);
    } else {
      mIcon.setText(mIconStr);
      mIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX, mIconSize);
    }
  }

  @Override protected int getLayoutId() {
    return R.layout.view_icon_grid_button;
  }

  @Override protected void init() {
    super.init();
  }

  public void setText(String text) {
    mTitle.setText(text);
  }

  public void setTextSize(float size) {
    mTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
  }

  public void setIcon(String icon) {
    mIconStr = icon;
    if (TextUtils.isEmpty(mIconStr)) {
      mIcon.setVisibility(View.GONE);
    } else {
      mIcon.setVisibility(View.VISIBLE);
      mIcon.setText(mIconStr);
      if (NumberUtils.isNumber(icon)) {
        FontHelper.RobotoMedium(mIcon);
      }
    }
  }

  public void setIconColor(@ColorInt int color) {
    mIcon.setTextColor(mIconColor = color);
  }

  public void setTextNormalColor(@ColorInt int color) {
    mTitle.setTextColor(mParsedTextColorNormal = color);
  }

  public void setIconSize(float size) {
    mIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
  }

  public FontIconTextView getIcon() {
    return mIcon;
  }

  public TextView getTitle() {
    return mTitle;
  }

  /**
   * 버튼이 눌려지게 함.
   *
   * @param focused 버튼이 눌려지냐 안눌려지냐를 결정함.
   */
  @Override public void setFocused(boolean focused) {
    super.setFocused(focused);
    mIcon.setTextColor(focused ? mParsedTextColorFocused : mIconColor);
    mTitle.setTextColor(focused ? mParsedTextColorFocused : mParsedTextColorNormal);
  }
}