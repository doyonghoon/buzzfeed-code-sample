package com.frograms.watcha.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.frograms.watcha.R;

/**
 * 버튼들을 가로로 추가하는 그리드 뷰.
 */
public class ActionGridLayout extends LinearLayout {

  public ActionGridLayout(Context context) {
    this(context, null, 0);
  }

  public ActionGridLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ActionGridLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    for (int Loop1 = 0; Loop1 < getChildCount(); Loop1++) {
      View view = getChildAt(Loop1);
      LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
      if (hasWeight()) {
        params.width = 0;
        params.height = LayoutParams.MATCH_PARENT;
        params.weight = 1;
      } else {
        final int padding = getContext().getResources().getDimensionPixelSize(R.dimen.view_5dp) * 3;
        params.width = LayoutParams.WRAP_CONTENT;
        params.height = LayoutParams.MATCH_PARENT;
        view.setPadding(padding, 0, padding, 0);
      }
    }
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }

  protected boolean hasWeight() {
    return true;
  }
}