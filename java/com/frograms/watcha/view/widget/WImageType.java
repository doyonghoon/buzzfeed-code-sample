package com.frograms.watcha.view.widget;

/**
 * {@link wImageView} 에서 사용하는 이미지 종류.
 */
public enum WImageType {
  POSTER(0, 0.7),
  STILLCUT(1, 1.915),
  /**
   * 720x500
   */
  COVER(2, 0.3472222222222222),
  PROFILE(3, 1),
  /**
   * 97x87
   */
  SHORTCUT(4, 0.8969072164948454),
  STILLCUT_SMALL(6, 2.666),
  /**
   * 비율 없음
   */
  NONE(5, 0),
  DEFAULT_STILLCUT(9, 1.4),
  POSTER_SMALL(7, 0.7),
  BANNER(8, 2);

  private int mAttributeValue = -1;
  private double mRatio;

  WImageType(int attrValue, double ratio) {
    mAttributeValue = attrValue;
    mRatio = ratio;
  }

  public double getSizeRatio() {
    return mRatio;
  }

  public int getAttributeValue() {
    return mAttributeValue;
  }

  public static WImageType getType(int attributeValue) {
    for (WImageType t : values()) {
      if (t.getAttributeValue() == attributeValue) {
        return t;
      }
    }
    return null;
  }
}
