package com.frograms.watcha.view.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;

/**
 * 이미지 둘러싼 보더가 있을때 사용함.
 */
public class WImageBorderTransformation extends WImageTransform {

  private static final String ID = "WImageView_Border_ImageView_Resource_Transformation";

  private BitmapPool mBitmapPool;
  private String mUrl;
  private int mCornerRadius;
  private int mBorderColor;
  private int mBorderWidth;

  public WImageBorderTransformation(String url, BitmapPool pool, int cornerRadius, int borderWidth,
      int borderColor) {
    mUrl = url;
    mBitmapPool = pool;
    mCornerRadius = cornerRadius;
    mBorderWidth = borderWidth;
    mBorderColor = borderColor;
  }

  @Override
  public Resource<Bitmap> transform(Resource<Bitmap> resource, int outWidth, int outHeight) {
    return BitmapResource.obtain(transform(resource.get()), mBitmapPool);
  }

  @Override public Bitmap transform(Bitmap bitmap) {
    Bitmap output =
        Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    final RectF rectF = new RectF(rect);

    // prepare canvas for transfer
    paint.setAntiAlias(true);
    paint.setColor(0xFFFFFFFF);
    paint.setStyle(Paint.Style.FILL);
    canvas.drawARGB(0, 0, 0, 0);
    canvas.drawRoundRect(rectF, mCornerRadius, mCornerRadius, paint);

    // draw bitmap
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);

    // draw border
    if (mBorderWidth > 0) {
      paint.setColor(mBorderColor);
      paint.setStyle(Paint.Style.STROKE);
      paint.setStrokeWidth((float) mBorderWidth);
      canvas.drawRoundRect(rectF, mCornerRadius, mCornerRadius, paint);
    }
    return output;
  }

  @Override public String getId() {
    return ID + "," + mBorderWidth + "," + mBorderColor + "," + mCornerRadius + ":" + mUrl;
  }
}
