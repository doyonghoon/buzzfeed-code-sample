package com.frograms.watcha.view.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.animation.AnimUtils;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.TagSettingFragment;
import com.frograms.watcha.listeners.LightAnimationListener;
import com.frograms.watcha.views.CircleTextView;
import com.nineoldandroids.animation.Animator;

/**
 * {@link TagSettingFragment} 태그설정하는 화면의 툴바.
 *
 * 현재 선택된 태그들의 수를 툴바에서 보여줌.
 */
public class TagSettingToolbarView extends RelativeLayout {

  public interface OnTagOkClickListener {
    void onClickOk(int tagCount);
  }

  @Bind(R.id.tag_count) CircleTextView mCountTextView;

  private SpringSystem mSpringSystem = null;
  private OnTagOkClickListener mListener;
  private int mCurrentCount = 0;

  public TagSettingToolbarView(Context context) {
    this(context, null, 0);
  }

  public TagSettingToolbarView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TagSettingToolbarView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    inflate(getContext(), R.layout.view_toolbar_tag, this);
    ButterKnife.bind(this);
    init();
  }

  private void init() {
    mSpringSystem = SpringSystem.create();
  }

  public void setOnTagOkClickListener(OnTagOkClickListener listener) {
    mListener = listener;
  }

  public void setTagCount(int count) {
    mCurrentCount = count;
    String result = mCurrentCount + "";
    if (mCurrentCount > 9) {
      result = "9+";
    }
    mCountTextView.setTitleText(result);
    animatePopup(mCurrentCount);
  }

  private void animatePopup(int count) {
    if (count > 0 && mCountTextView.getVisibility() != View.VISIBLE) {
      // 보임.
      AnimUtils.popIn(mCountTextView, new LightAnimationListener() {
        @Override public void onAnimationStart(Animator animation) {
          mCountTextView.setVisibility(View.VISIBLE);
          mCountTextView.setAlpha(0.0f);
        }
      });
    } else if (count < 1) {
      // 사라짐.
      AnimUtils.popOut(mCountTextView, new LightAnimationListener() {
        @Override public void onAnimationEnd(Animator animation) {
          mCountTextView.setVisibility(View.GONE);
        }
      });
    } else {
      scaleSpringAnimation(mCountTextView);
    }
  }

  protected void scaleSpringAnimation(final View view) {
    final Spring spring = mSpringSystem.createSpring();
    spring.setSpringConfig(new SpringConfig(250f, 20));
    spring.addListener(new SimpleSpringListener() {
      @Override public void onSpringUpdate(Spring spring) {
        float value = (float) spring.getCurrentValue();
        float scale = 1f + (value * 0.4f);
        view.setScaleX(scale);
        view.setScaleY(scale);
      }
    });

    spring.setEndValue(1);

    Handler handler = new Handler();
    handler.postDelayed(() -> {
      if (getContext() != null) {
        spring.setEndValue(0);
      }
    }, 150);
  }

  @OnClick(R.id.tag_ok) public void onClickTagOk() {
    if (mListener != null) {
      mListener.onClickOk(mCurrentCount);
    }
  }
}
