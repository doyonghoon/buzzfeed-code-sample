package com.frograms.watcha.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.rey.material.widget.ProgressView;

public class LoadingView extends FrameLayout {

  @Bind(R.id.progress_loading) ProgressView mBar;

  public LoadingView(Context context) {
    this(context, null);
  }

  public LoadingView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public LoadingView(Context context, AttributeSet attrs, int def) {
    super(context, attrs, def);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_loading, this);
    ButterKnife.bind(this);
  }

  public void onLoading() {
    mBar.start();
    mBar.setVisibility(View.VISIBLE);
  }

  public void onEnd() {
    mBar.stop();
    mBar.setVisibility(View.GONE);
  }
}
