package com.frograms.watcha.view.textviews;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;
import com.frograms.watcha.view.fonticon.CompoundDrawables;

public class DefTextView extends TextView {
  public DefTextView(Context context) {
    this(context, null, 0);
  }

  public DefTextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public DefTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    if (!isInEditMode()) {
      CompoundDrawables.init(context, attrs, this);
    }
  }

  public void updateCompoundDrawables() {
    CompoundDrawables.update(this);
  }

  @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1) public void updateCompoundDrawablesRelative() {
    CompoundDrawables.updateRelative(this);
  }
}
