package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.FontHelper;

public class RobotoBoldView extends CustomFontView {

  public RobotoBoldView(Context context) {
    super(context);
  }

  public RobotoBoldView(Context context, AttributeSet attr) {
    super(context, attr);
  }

  public RobotoBoldView(Context context, AttributeSet attr, int defStyle) {
    super(context, attr, defStyle);
  }

  @Override protected FontHelper.FontType getFontType() {
    return FontHelper.FontType.ROBOTO_BOLD;
  }
}
