package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.FontHelper;

public class RobotoBoldCondensedView extends CustomFontView {

  public RobotoBoldCondensedView(Context context) {
    super(context);
  }

  public RobotoBoldCondensedView(Context context, AttributeSet attr) {
    super(context, attr);
  }

  public RobotoBoldCondensedView(Context context, AttributeSet attr, int defStyle) {
    super(context, attr, defStyle);
  }

  @Override protected FontHelper.FontType getFontType() {
    return FontHelper.FontType.ROBOTO_BOLD_CONDENSED;
  }
}
