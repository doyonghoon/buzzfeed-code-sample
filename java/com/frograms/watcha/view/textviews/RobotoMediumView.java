package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.FontHelper;

public class RobotoMediumView extends CustomFontView {

  public RobotoMediumView(Context context) {
    super(context);
  }

  public RobotoMediumView(Context context, AttributeSet attr) {
    super(context, attr);
  }

  public RobotoMediumView(Context context, AttributeSet attr, int defStyle) {
    super(context, attr, defStyle);
  }

  @Override protected FontHelper.FontType getFontType() {
    return FontHelper.FontType.ROBOTO_MEDIUM;
  }
}
