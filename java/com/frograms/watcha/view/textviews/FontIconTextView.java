package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.FontHelper;

public class FontIconTextView extends CustomFontView {
  public FontIconTextView(Context context) {
    super(context);
  }

  public FontIconTextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public FontIconTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override protected FontHelper.FontType getFontType() {
    return FontHelper.FontType.FONT_ICON;
  }
}
