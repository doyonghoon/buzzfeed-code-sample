package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.utils.ViewUtil;
import com.frograms.watcha.view.fonticon.FontIconDrawable;

public class NameTextView extends DefTextView {

  private static final int MAX_NAME_LENGTH = 13;

  private float mTextSize;
  private FontIconDrawable mOfficialDrawable;

  public NameTextView(Context context) {
    this(context, null, 0);
  }

  public NameTextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public NameTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    TypedArray a = context.obtainStyledAttributes(attrs, new int[] { android.R.attr.textSize });
    mTextSize = a.getDimension(0, 0.0f);
    setCompoundDrawablePadding((int) ViewUtil.convertDpToPixel(context, 6));
    setGravity(Gravity.CENTER_VERTICAL);
    a.recycle();
  }

  private FontIconDrawable getOfficialDrawable() {
    if (mOfficialDrawable == null) {
      mOfficialDrawable = FontIconDrawable.inflate(getContext(), R.xml.icon_check_on);
      mOfficialDrawable.setTextColor(getResources().getColor(R.color.pink));
      mOfficialDrawable.setTextSize(mTextSize);
    }

    return mOfficialDrawable;
  }

  public void setUser(UserBase user) {
    setUser(user, true);
  }

  public void setUser(UserBase user, boolean shouldEllipsize) {
    if (user.isOfficialAccount()) {
      setCompoundDrawables(null, null, getOfficialDrawable(), null);
    } else {
      setCompoundDrawables(null, null, null, null);
    }
    if (shouldEllipsize) setText(Util.getEllipsipSizeStringByLength(user.getName(), MAX_NAME_LENGTH));
    else setText(user.getName());
  }
}
