package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.FontHelper;

/**
 * 커스텀 폰트는 하나로 관리하자.
 */
abstract public class CustomFontView extends DefTextView {
  public CustomFontView(Context context) {
    this(context, null, 0);
  }

  public CustomFontView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public CustomFontView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  private void init() {
    if (getFontType() != null && !isInEditMode()) {
      setTypeface(getFontType().getTypeface());
    }
  }

  protected abstract FontHelper.FontType getFontType();
}
