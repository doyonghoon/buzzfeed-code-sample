package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.FontHelper;

public class RobotoThinView extends CustomFontView {

  public RobotoThinView(Context context) {
    super(context);
  }

  public RobotoThinView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public RobotoThinView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override protected FontHelper.FontType getFontType() {
    return FontHelper.FontType.ROBOTO_THIN;
  }
}
