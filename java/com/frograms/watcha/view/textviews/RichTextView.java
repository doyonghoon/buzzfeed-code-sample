package com.frograms.watcha.view.textviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.LinkText;
import com.frograms.watcha.utils.WLog;

public class RichTextView extends TextView {

  private class URLSpanNoUnderline extends URLSpan {
    private String category;
    private String action;
    private String label;
    private String event = null;
    //		private OnCellClickListener listener;
    private int cellPosition = -1;

    public URLSpanNoUnderline(String url, String category, String action, String label) {
      super(url);
      this.category = category;
      this.action = action;
      this.label = label;
      this.event = null;
    }

    //		public void setPlainTextClickListener(int cellPosition, OnCellClickListener listener) {
    //			this.listener = listener;
    //			this.cellPosition = cellPosition;
    //		}

    public void setEvent(String event) {
      this.event = event;
    }

    @Override public void updateDrawState(TextPaint ds) {
      super.updateDrawState(ds);
      ds.setUnderlineText(false);
    }

    @Override public void onClick(View v) {
      //            if (this.listener != null) {
      //            	try {
      //            		this.listener.onClickSelectableTab(cellPosition);
      //            		return;
      //            	} catch (Exception e) {e.printStackTrace();}
      //            }

      if (event == null) {
        WLog.i("Event : " + event);
        super.onClick(v);
      } else {
        try {
          //	            	new ModalEvent(getContext(), event, null, category).start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  public RichTextView(Context context) {
    super(context);
  }

  public RichTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public RichTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  public void setRichText(String text) {
    this.setText(text);
  }

  public void toLargeText(String key, float size) {
    if (key.length() == 0) return;

    SpannableStringBuilder span = new SpannableStringBuilder(key);
    span.setSpan(new RelativeSizeSpan(size), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    append(span);
  }

  public void toBoldText(String key) {
    if (key.length() == 0) return;

    SpannableStringBuilder span = new SpannableStringBuilder(key);
    span.setSpan(new StyleSpan(Typeface.BOLD), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(Color.rgb(112, 112, 112)), 0, key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    append(span);
  }

  public void toBoldText(String key, int color) {
    if (key.length() == 0) return;

    SpannableStringBuilder span = new SpannableStringBuilder(key);
    span.setSpan(new StyleSpan(Typeface.BOLD), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(color), 0, key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    append(span);
  }

  @SuppressLint("DefaultLocale") public boolean toBoldText(String text, String key) {
    String lower = text.toLowerCase();

    String regex = "";
    regex += key.charAt(0) + "([:space:])*";
    for (int Loop1 = 1; Loop1 < key.length() - 1; Loop1++) {
      regex += key.charAt(Loop1) + "([:space:])*";
    }
    regex += key.charAt(key.length() - 1) + "(.)*";
    String str = lower;
    int index, realIndex = 0;
    while ((index = str.indexOf(String.valueOf(key.charAt(0)))) != -1) {
      str = str.substring(index);
      realIndex += index;
      if (str.matches(regex)) {
        regex = regex.substring(0, regex.length() - 4);
        for (int Loop1 = key.length(); Loop1 <= str.length(); Loop1++) {
          if (str.substring(0, Loop1).matches(regex) && Loop1 != 0) {

            SpannableStringBuilder span = new SpannableStringBuilder(text);
            span.setSpan(new StyleSpan(Typeface.BOLD), realIndex, realIndex + Loop1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            span.setSpan(new ForegroundColorSpan(Color.rgb(112, 112, 112)), realIndex,
                realIndex + Loop1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            append(span);
            return true;
          }
        }
      }
      str = str.substring(1);
      realIndex++;
    }
    return false;
  }

  public void setLinkText(LinkText link_text, String category) {
    String text = link_text.getText();
    int index = 0, count = 0;

    setText("");
    while (text.indexOf("%@") != -1) {
      index = text.indexOf("%@");
      append(text.substring(0, index));
      if (link_text.getLinks().size() > 0) {
        if (!TextUtils.isEmpty(link_text.getLinks().get(count).getUrl())) {
          setLink(link_text.getLinks().get(count).getUrl(),
              link_text.getLinks().get(count).getText(), category, null, null);
        } else {
          toBoldText(link_text.getLinks().get(count).getText());
        }
      }
      text = text.substring(index + 2);
      count++;
    }

    append(text);
  }

  public void toLinkText(String text, String key) {
    SpannableStringBuilder span = new SpannableStringBuilder(text);
    int index = text.indexOf(key);
    span.setSpan(new StyleSpan(Typeface.BOLD), index, index + key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(Color.rgb(102, 122, 155)), index, index + key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    URLSpanNoUnderline urlSpan = new URLSpanNoUnderline("agree://", null, null, null);
    span.setSpan(urlSpan, index, index + key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    append(span);
    setMovementMethod(LinkMovementMethod.getInstance());
  }

  public void setLink(String url, String key, String category, String action, String label) {
    if (key.length() == 0) return;

    final String appUri = url;// + "?from=" + category;

    WLog.i(appUri);
    SpannableStringBuilder span = new SpannableStringBuilder(key);
    URLSpanNoUnderline urlSpan = new URLSpanNoUnderline(appUri, category, action, label);
    span.setSpan(urlSpan, 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new StyleSpan(Typeface.BOLD), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(Color.rgb(102, 122, 155)), 0, key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    if (category.contains("MyMenu") || category.contains("Personal") && action == null) {
      span.setSpan(new ForegroundColorSpan(Color.rgb(30, 30, 30)), 0, key.length(),
          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      span.setSpan(new AbsoluteSizeSpan(getResources().getDimensionPixelSize(R.dimen.normal_text)),
          0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    append(span);
    setMovementMethod(LinkMovementMethod.getInstance());
  }

  //	public void setPlainTextLink(String category, String text, int pos, OnCellClickListener plainTextClickListener) {
  //		if (pos < 0 && plainTextClickListener == null) return;
  //		SpannableStringBuilder span = new SpannableStringBuilder(text);
  //		URLSpanNoUnderline urlSpan = new URLSpanNoUnderline(null, category, null, null);
  //		urlSpan.setPlainTextClickListener(pos, plainTextClickListener);
  //		span.setSpan(urlSpan, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
  //		span.setSpan(new ForegroundColorSpan(WatchaApp.getInstance().getResources().getColorId(R.color.heavy_gray)), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
  //		setMovementMethod(LinkMovementMethod.getInstance());
  //		append(span);
  //	}

  public void setPlainLink(String event, String text, String category) {
    SpannableStringBuilder span = new SpannableStringBuilder(text);
    URLSpanNoUnderline urlSpan = new URLSpanNoUnderline(null, category, null, null);
    span.setSpan(urlSpan, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(
            WatchaApp.getInstance().getResources().getColor(R.color.heavy_gray)), 0, text.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    setMovementMethod(LinkMovementMethod.getInstance());
    append(span);
  }

  public void setLink(String url, String key, String category, String action, String label,
      int color) {
    setLink(url, key, category, action, label, color, true);
    //		if (key.length() == 0) return;
    //
    //		SpannableStringBuilder span = new SpannableStringBuilder(key);
    //		URLSpanNoUnderline urlSpan = new URLSpanNoUnderline(url, category, action, label);
    //		span.setSpan(urlSpan, 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    //		span.setSpan(new StyleSpan(Typeface.BOLD), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    //		span.setSpan(new ForegroundColorSpan(color), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    //		setMovementMethod(LinkMovementMethod.getInstance());
    //		append(span);
  }

  public void setLink(String url, String key, String category, String action, String label,
      int color, boolean isBold) {
    if (key.length() == 0) return;

    SpannableStringBuilder span = new SpannableStringBuilder(key);
    URLSpanNoUnderline urlSpan = new URLSpanNoUnderline(url, category, action, label);
    span.setSpan(urlSpan, 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    if (isBold) {
      span.setSpan(new StyleSpan(Typeface.BOLD), 0, key.length(),
          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    } else {
      span.setSpan(new StyleSpan(Typeface.NORMAL), 0, key.length(),
          Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
    span.setSpan(new ForegroundColorSpan(color), 0, key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    setMovementMethod(LinkMovementMethod.getInstance());
    append(span);
  }

  public void toAgree(String key) {
    if (key.length() == 0) return;

    SpannableStringBuilder span = new SpannableStringBuilder(key);
    URLSpanNoUnderline urlSpan = new URLSpanNoUnderline("agree://", null, null, null);
    span.setSpan(urlSpan, 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new StyleSpan(Typeface.BOLD), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new ForegroundColorSpan(Color.rgb(102, 122, 155)), 0, key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    span.setSpan(new RelativeSizeSpan(1.2f), 0, key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    append(span);
    setMovementMethod(LinkMovementMethod.getInstance());
  }
}