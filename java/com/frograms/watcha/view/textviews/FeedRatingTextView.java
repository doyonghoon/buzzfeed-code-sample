package com.frograms.watcha.view.textviews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.utils.RatingUtils;
import com.frograms.watcha.utils.ViewUtil;

public class FeedRatingTextView extends DefTextView {

  private static final Typeface TYPE_FACE =
      Typeface.createFromAsset(WatchaApp.getInstance().getAssets(), "icon.ttf");

  private static final Typeface TYPE_FACE_ROBOTO =
      Typeface.createFromAsset(WatchaApp.getInstance().getAssets(), "Roboto-Medium.ttf");

  public FeedRatingTextView(Context context) {
    this(context, null, 0);
  }

  public FeedRatingTextView(Context context, AttributeSet attr) {
    this(context, attr, 0);
  }

  public FeedRatingTextView(Context context, AttributeSet attr, int defStyle) {
    super(context, attr, defStyle);
    setCompoundDrawablePadding((int) (ViewUtil.convertDpToPixel(context, 2)));
    setGravity(Gravity.CENTER_VERTICAL);
    setTextColor(getResources().getColor(R.color.black));
    setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_title_font));
  }

  private static void setRatingTextSpan(TextView view, String key, int colorResId, int iconResId) {
    SpannableStringBuilder span = new SpannableStringBuilder(key);
    span.setSpan(new CustomTypefaceSpan("", TYPE_FACE_ROBOTO), 0, key.length(),
        Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    span.setSpan(new ForegroundColorSpan(view.getResources().getColor(colorResId)), 0, key.length(),
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    SpannableStringBuilder drawable =
        new SpannableStringBuilder(view.getResources().getString(iconResId));
    drawable.setSpan(new CustomTypefaceSpan("", TYPE_FACE), 0, drawable.length(),
        Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    drawable.setSpan(new ForegroundColorSpan(view.getResources().getColor(colorResId)), 0,
        drawable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    view.setText(drawable);
    view.append(span);
    view.setVisibility(View.VISIBLE);
  }

  public static boolean getRatingText(TextView view, UserAction userAction) {
    if (userAction == null || view == null) return false;

    String stringKey = null;
    int colorResId = 0, iconXmlResId = 0;

    SpannableStringBuilder span;
    if (userAction.isWished()) {
      stringKey = view.getResources().getString(R.string.wish);
      colorResId = R.color.pink;
      iconXmlResId = R.string.icon_wish;
    }

    if (userAction.isMehed()) {
      stringKey = view.getResources().getString(R.string.ignore);
      colorResId = R.color.light_gray;
      iconXmlResId = R.string.icon_no;
    }

    if (userAction.getRating() > 0) {
      stringKey = userAction.getRating() + "줌";
      colorResId = R.color.gold;
      iconXmlResId = R.string.icon_rate_action;
    }

    if (stringKey == null) return false;

    setRatingTextSpan(view, stringKey, colorResId, iconXmlResId);
    return true;
  }

  public static void getWishText(TextView view) {
    String stringKey = null;
    int colorResId = 0, iconXmlResId = 0;

    stringKey = view.getResources().getString(R.string.wish);
    colorResId = R.color.pink;
    iconXmlResId = R.string.icon_wish;

    setRatingTextSpan(view, stringKey, colorResId, iconXmlResId);
  }

  public static void getMehText(TextView view) {
    String stringKey = null;
    int colorResId = 0, iconXmlResId = 0;

    stringKey = view.getResources().getString(R.string.ignore);
    colorResId = R.color.light_gray;
    iconXmlResId = R.string.icon_no;

    setRatingTextSpan(view, stringKey, colorResId, iconXmlResId);
  }

  public static void getRatingText(TextView view, String rating) {
    rating = String.valueOf(((int) (Float.valueOf(rating) * 10)) / 10.0f);
    String stringKey = null;
    int colorResId = 0, iconXmlResId = 0;

    stringKey = rating + "줌";
    colorResId = R.color.gold;
    iconXmlResId = R.string.icon_rate_action;

    setRatingTextSpan(view, stringKey, colorResId, iconXmlResId);
  }

  public static void getPreRatingText(TextView view, String preRating) {
    preRating = (preRating != null) ? String.valueOf(RatingUtils.round(Float.valueOf(preRating), 1))
        : "0.0";

    if (view == null || preRating == null) return;

    setRatingTextSpan(view, preRating, R.color.pink, R.string.icon_rate_action);
  }

  public boolean setUserAction(UserAction userAction) {
    if (userAction == null) {
      setVisibility(View.GONE);
      return false;
    }

    if (userAction.getRating() == 0.0f && !userAction.isWished() && !userAction.isMehed()) {
      setVisibility(View.GONE);
      return false;
    }

    getRatingText(this, userAction);
    return true;
  }
}
