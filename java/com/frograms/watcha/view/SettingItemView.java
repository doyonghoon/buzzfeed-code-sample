package com.frograms.watcha.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import carbon.drawable.RippleDrawable;
import carbon.drawable.RippleDrawableCompat;
import com.frograms.watcha.R;
import com.frograms.watcha.view.textviews.FontIconTextView;

public class SettingItemView extends carbon.widget.RelativeLayout {

  @Bind(R.id.setting_icon) FontIconTextView mIconView;
  @Bind(R.id.setting_title) TextView mTitleView;
  @Bind(R.id.setting_description) TextView mDescView;
  @Bind(R.id.setting_switch) FontIconTextView mSwitchView;

  private String mIcon;
  private String mTitle, mDesc;
  private boolean mHasSwitch;

  private boolean mIsChecked;

  public SettingItemView(Context context) {
    this(context, null, 0);
  }

  public SettingItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SettingItemView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Setting);

    mIcon = a.getString(R.styleable.Setting_settingIcon);

    mTitle = a.getString(R.styleable.Setting_settingText);
    mDesc = a.getString(R.styleable.Setting_settingDescription);

    mHasSwitch = a.getBoolean(R.styleable.Setting_settingWithSwitch, false);

    setPadding(getResources().getDimensionPixelSize(R.dimen.card_padding), 0,
        getResources().getDimensionPixelSize(R.dimen.card_padding), 0);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.layer_setting_item, this);
    ButterKnife.bind(this);

    if (mIcon != null) {
      mIconView.setVisibility(View.VISIBLE);
      mIconView.setText(mIcon);
    } else {
      mIconView.setVisibility(View.GONE);
    }

    mTitleView.setText(mTitle);

    if (mDesc != null) {
      mDescView.setVisibility(VISIBLE);
      mDescView.setText(mDesc);
    } else {
      mDescView.setVisibility(GONE);
    }

    if (mHasSwitch) {
      mSwitchView.setVisibility(View.VISIBLE);
    } else {
      mSwitchView.setVisibility(View.GONE);
    }

    enableRipple();
  }

  public FontIconTextView getIconView() {
    return mIconView;
  }

  private void enableRipple() {
    RippleDrawable rippleDrawable =
        new RippleDrawableCompat(0x4263616d, null, getContext(), RippleDrawable.Style.Over);
    rippleDrawable.setCallback(this);
    rippleDrawable.setHotspotEnabled(true);
    setRippleDrawable(rippleDrawable);
  }

  public void setTitle(String title) {
    mTitle = title;
    mTitleView.setText(mTitle);
  }

  public void setDescription(String desc) {
    mDescView.setVisibility(View.VISIBLE);

    mDesc = desc;
    mDescView.setText(desc);
  }

  public void setChecked(boolean check) {
    mIsChecked = check;
    mSwitchView.setText((mIsChecked) ? R.string.icon_on : R.string.icon_off);
    mSwitchView.setTextColor(getResources().getColor((mIsChecked) ? R.color.pink : R.color.light_gray));
  }
}
