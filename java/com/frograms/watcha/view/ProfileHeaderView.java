package com.frograms.watcha.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.frograms.watcha.BuildConfig;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.PrivacyLevel;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.widget.ContentShortcutView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.frograms.watcha.view.widget.wImages.WImageView;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

/**
 * 프로필페이지 상단 헤더 뷰
 */
public class ProfileHeaderView extends RelativeLayout {

  //@Bind(R.id.action_grid_layout) View mShorcutLayout;
  @Bind(R.id.profile_comment_grid) IconActionGridButton mCommentButton;
  @Bind(R.id.profile_deck_grid) IconActionGridButton mDeckButton;
  @Bind(R.id.profile_wish_grid) IconActionGridButton mWishButton;
  @Bind(R.id.profile_name) NameTextView mName;
  @Bind(R.id.profile_message) TextView mMessage;
  @Bind(R.id.profile_description) TextView mDescription;
  @Bind(R.id.profile_taste_percent) TextView mTastePercentage;
  @Bind(R.id.profile_taste_percent_layout) View mTastePercentageLayout;
  @Bind(R.id.profile_taste) TextView mTasteAnalyzeButton;
  @Bind(R.id.profile_image) GreatImageView mProfileImage;
  @Bind(R.id.profile_profile_edit) View mProfileEditButton;
  @Bind(R.id.profile_cover_image) WImageView mCoverImage;
  @Bind(R.id.profile_cover_layout) RelativeLayout mCoverLayout;
  @Bind(R.id.profile_content_shortcut_layout) RelativeLayout mShortcutLayout;
  @Bind(R.id.profile_shortcut_movie) ContentShortcutView mShortcutMovie;
  @Bind(R.id.profile_shortcut_tv_season) ContentShortcutView mShortcutDrama;
  @Bind(R.id.profile_shortcut_book) ContentShortcutView mShortcutBook;

  private User mUser = null;
  private String mUserProfileImageUrl = null;
  private String mUserCoverImageUrl = null;

  public ProfileHeaderView(Context context) {
    this(context, null, 0);
  }

  public ProfileHeaderView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ProfileHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_profile_header, this);
    ButterKnife.bind(this);

    FontHelper.RobotoBold(mName);
  }

  //private Action1<User> mUpdateUiAction = new Action1<User>() {
  //  @Override public void call(User user) {
  //    mUser = user;
  //    updateUi();
  //  }
  //};

  //public Action1<User> getUserUpdateAction() {
  //  return mUpdateUiAction;
  //}

  public void setUser(User user) {
    mUser = user;
    updateUi();
  }

  private TourGuide mCoachmarkHandler = null;

  View.OnClickListener mTooltipDismissClickListener = new OnClickListener() {
    @Override public void onClick(View v) {
      if (mCoachmarkHandler != null) {
        mCoachmarkHandler.cleanUp();
        mCoachmarkHandler = null;
      }
    }
  };

  private void updateUi() {
    if (mUser != null && !TextUtils.isEmpty(mUser.getCode())) {
      // 내 프로필 화면과 유저 프로필 커버 이미지 높이가 다름.
      setCoverHeight();

      if (!mUser.getCode().equals(WatchaApp.getUser().getCode()) && ableToEnter(
          mUser.getPrivacyLevel())) {
        // 취향 매칭율
        mTastePercentage.setText(String.format("%.1f", mUser.getTasteMatch()) + "%");
        mTastePercentage.setVisibility(View.VISIBLE);
        mTastePercentageLayout.setVisibility(View.VISIBLE);

        if (getContext() instanceof Activity
            && !PrefHelper.getBoolean(getContext(), PrefHelper.PrefType.COACHMARK, PrefHelper.PrefKey.COACHMARK_TASTE_MATCH
            .name())) {
          PrefHelper.setBoolean(getContext(), PrefHelper.PrefType.COACHMARK, PrefHelper.PrefKey.COACHMARK_TASTE_MATCH
              .name(), true);
          ToolTip tooltip = new ToolTip().setGravity(Gravity.BOTTOM)
              .setTitleTextSize(21).setDescriptionTextSize(20)
              .setTitle("이 회원과 얼마나 취향이 비슷한 지 이곳에 표시돼요")
              .setDescription("알겠어요!")
              .setBackgroundColor(Color.TRANSPARENT)
              .setTextColor(Color.WHITE)
              .setClickListener(mTooltipDismissClickListener)
              .setTypeface(FontHelper.FontType.ROBOTO_REGULAR.getTypeface())
              .setShadow(false);

          mCoachmarkHandler = TourGuide.init((Activity) getContext())
              .with(TourGuide.Technique.Click)
              .setToolTip(tooltip)
              .setOverlay(new Overlay().setBackgroundColor(getContext().getResources()
                  .getColor(R.color.skyblue_95)))
              .playOn(mTastePercentage);

          mTastePercentage.setOnClickListener(mTooltipDismissClickListener);
          mTastePercentageLayout.setOnClickListener(mTooltipDismissClickListener);
        }
      } else {
        mTastePercentageLayout.setVisibility(View.GONE);
        mTastePercentage.setVisibility(View.GONE);
      }
      final String name = mUser.getName();
      mName.setUser(mUser, false);
      if (!TextUtils.isEmpty(mUser.getBio())) {
        mMessage.setVisibility(VISIBLE);
        mMessage.setText(mUser.getBio());
      } else {
        mMessage.setVisibility(GONE);
      }
      mUserProfileImageUrl = mUser.getPhoto() != null ? mUser.getPhoto().getLarge() : null;
      mUserCoverImageUrl = mUser.getCover();
      mProfileImage.load(mUserProfileImageUrl);

      RatioType type;
      if (mUser.getCode().equals(WatchaApp.getUser().getCode()))
        type = RatioType.COVER_SMALL;
      else
        type = RatioType.COVER_LARGE;

      mCoverImage.setRatioType(type).load(mUser.getCover());
      mCoverImage.setOnFinishLoadingImageListener(new WImageView.OnFinishLoadingImageListener() {
        @Override public void onFinishLoadingImage(@Nullable Bitmap bitmap, WImageView view) {
          if (bitmap != null) {
            view.setAlpha(0.8f);
          }
        }
      });
      mCoverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
      mDescription.setText(getDescription(mUser.getFollowersCount(), mUser.getFriendsCount()));

      mShortcutLayout.setVisibility(
          ableToEnter(mUser.getPrivacyLevel()) ? View.VISIBLE : View.GONE);
      mTastePercentageLayout.setVisibility(
          ableToEnter(mUser.getPrivacyLevel()) ? View.VISIBLE : View.GONE);
      mTastePercentage.setVisibility(
          ableToEnter(mUser.getPrivacyLevel()) ? View.VISIBLE : View.GONE);
      mTasteAnalyzeButton.setVisibility(
          ableToEnter(mUser.getPrivacyLevel()) ? View.VISIBLE : View.GONE);

      @ColorInt final int buttonColor = getResources().getColor(
          ableToEnter(mUser.getPrivacyLevel()) ? R.color.heavy_gray : R.color.light_gray);
      int commentCount = mUser.getCommentCounts();
      int deckCount = mUser.getDecksCount();
      int wishCount = mUser.getWishCounts();
      mCommentButton.setIcon(commentCount + "");
      mDeckButton.setIcon(deckCount + "");
      mWishButton.setIcon(wishCount + "");
      mCommentButton.setIconColor(buttonColor);
      mCommentButton.setTextNormalColor(buttonColor);
      mDeckButton.setIconColor(buttonColor);
      mDeckButton.setTextNormalColor(buttonColor);
      mWishButton.setIconColor(buttonColor);
      mWishButton.setTextNormalColor(buttonColor);

      mProfileEditButton.setVisibility(
          mUser.getCode().equals(WatchaApp.getUser().getCode()) ? View.VISIBLE : View.GONE);

      if (mUser.getRatings() != null) {
        showShortcut(mUser.getRatings().getMoviesRating(), mShortcutMovie, CategoryType.MOVIES);
        showShortcut(mUser.getRatings().getTvSeasonsRating(), mShortcutDrama,
            CategoryType.TV_SEASONS);
      }
    }
  }

  public void updateShortcutCount(int movieCount, int dramaCount) {
    if (mShortcutMovie != null && mShortcutDrama != null) {
      String movieText =
          String.format("%s %d", getResources().getString(CategoryType.MOVIES.getStringId()),
              movieCount);
      String dramaText =
          String.format("%s %d", getResources().getString(CategoryType.TV_SEASONS.getStringId()),
              dramaCount);
      mShortcutMovie.setTitle(movieText);
      mShortcutDrama.setTitle(dramaText);
    }
  }

  private void showShortcut(User.Ratings.Rating data, ContentShortcutView v, CategoryType type) {
    if (data != null) {
      v.setTitle(getResources().getString(type.getStringId()) + " " + data.getCount());
      if (!ableToEnter(mUser.getPrivacyLevel())) {
        v.setIcon(getResources().getString(R.string.icon_privacy_onlyme));
        v.showIcon(true);
        return;
      }

      if (data.getPhoto() != null && !TextUtils.isEmpty(data.getPhoto().getLarge())) {
        v.setCover(data.getPhoto().getLarge());
      } else {
        v.showIcon(true);
      }
    }
  }

  public GreatImageView getUserImageProfile() {
    return mProfileImage;
  }

  public WImageView getCover() {
    return mCoverImage;
  }

  private void setCoverHeight() {
    boolean isMe = mUser.getCode().equals(WatchaApp.getUser().getCode());
    int height = getResources().getDimensionPixelSize(
        isMe ? R.dimen.my_profile_cover_height : R.dimen.user_profile_cover_height);
    LayoutParams p1 = (LayoutParams) mCoverImage.getLayoutParams();
    LayoutParams p2 = (LayoutParams) mCoverLayout.getLayoutParams();
    p1.height = height;
    p2.height = height;
  }

  private String getDescription(int followerCount, int follwingCount) {
    return getResources().getString(R.string.follower)
        + followerCount
        + " / "
        + getResources().getString(R.string.following)
        + follwingCount;
  }

  private boolean ableToEnter(PrivacyLevel privacy) {
    if (privacy != null) {
      switch (privacy) {
        case ALL:
          return true;
        case FRIENDS:
          if (mUser == null || WatchaApp.getUser() == null) {
            // 유저 모델이 없는 건 비정상적인 접근.
            return false;
          }
          // 친구일 때.
          return WatchaApp.getUser().getCode().equals(mUser.getCode()) || mUser.isFollower();
        // 나 자신.
        case ONLY_ME:
          // 유저 모델이 존재하고, 유저가 나일 때만 열림.
          return !(mUser == null || WatchaApp.getUser() == null) && WatchaApp.getUser()
              .getCode()
              .equals(mUser.getCode());
      }
    }
    return true;
  }

  private String getPrivacyMessage(String username, PrivacyLevel privacy) {
    return String.format("%s 님이 %s 하셨습니다", username,
        getResources().getString(privacy.getStringId()));
  }

  @OnClick(R.id.profile_description) void onClickFollow() {
    if (ableToEnter(mUser.getPrivacyLevel())) {
      ActivityStarter.with(getContext(), FragmentTask.FOLLOW)
          .addBundle(new BundleSet.Builder().putUserCode(mUser.getCode())
              .putFollowersCount(mUser.getFollowersCount())
              .putFriendsCount(mUser.getFriendsCount())
              .build()
              .getBundle())
          .start();
    } else {
      Toast.makeText(getContext(), getPrivacyMessage(mUser.getName(), mUser.getPrivacyLevel()),
          Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.profile_taste) void onClickTaste() {
    String webViewTitle = new StringBuilder().append(mUser.getName())
        .append("님의 ")
        .append(getResources().getString(R.string.taste_more))
        .toString();

    //build bundle
    Bundle bundle = new BundleSet.Builder().putUrl(
        BuildConfig.END_POINT + "/users/" + mUser.getCode() + "/taste", webViewTitle)
        .putUserCode(mUser.getCode())
        .putUserName(mUser.getName())
        .build()
        .getBundle();

    ActivityStarter.with(getContext(), FragmentTask.WEBVIEW).addBundle(bundle).start();
  }

  @OnClick(R.id.profile_comment_grid) void onClickComment() {
    if (ableToEnter(mUser.getPrivacyLevel())) {
      ActivityStarter.with(getContext(), FragmentTask.USER_CONTENT_CATEGORY_LIST)
          .addBundle(new BundleSet.Builder().putUserActionType(UserActionType.COMMENT)
              .putShowUnderbarView(true)
              .putPrivacyLevel(mUser.getPrivacyLevel())
              .putActionCount(mUser.getActionCount(CategoryType.MOVIES.getApiPath()).getComments())
              .putUserCode(mUser.getCode())
              .putMovieCount(mUser.getActionCount(CategoryType.MOVIES.getApiPath()).getComments())
              .putDramaCount(
                  mUser.getActionCount(CategoryType.TV_SEASONS.getApiPath()).getComments())
              .build()
              .getBundle())
          .start();
    } else {
      Toast.makeText(getContext(), getPrivacyMessage(mUser.getName(), mUser.getPrivacyLevel()),
          Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.profile_deck_grid) void onClickDeck() {
    if (ableToEnter(mUser.getPrivacyLevel())) {
      ActivityStarter.with(getContext(), FragmentTask.USER_CONTENT_LIST)
          .addBundle(new BundleSet.Builder().putUserActionType(UserActionType.DECK)
              .putShowUnderbarView(
                  WatchaApp.getUser() != null && mUser != null && WatchaApp.getUser()
                      .getCode()
                      .equals(mUser.getCode()))
              .putUserCode(mUser.getCode())
              .build()
              .getBundle())
          .start();
    } else {
      Toast.makeText(getContext(), getPrivacyMessage(mUser.getName(), mUser.getPrivacyLevel()),
          Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.profile_wish_grid) void onClickWish() {
    if (ableToEnter(mUser.getPrivacyLevel())) {
      ActivityStarter.with(getContext(), FragmentTask.USER_CONTENT_CATEGORY_LIST)
          .addBundle(new BundleSet.Builder().putUserActionType(UserActionType.WISH)
              .putPrivacyLevel(mUser.getPrivacyLevel())
              .putShowUnderbarView(true)
              .putActionCount(mUser.getActionCount(CategoryType.MOVIES.getApiPath()).getWishes())
              .putUserCode(mUser.getCode())
              .putMovieCount(mUser.getActionCount(CategoryType.MOVIES.getApiPath()).getWishes())
              .putDramaCount(mUser.getActionCount(CategoryType.TV_SEASONS.getApiPath()).getWishes())
              .build()
              .getBundle())
          .start();
    } else {
      Toast.makeText(getContext(), getPrivacyMessage(mUser.getName(), mUser.getPrivacyLevel()),
          Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.profile_shortcut_movie) void onClickShortcutMovie() {
    if (ableToEnter(mUser.getPrivacyLevel())) {
      ActivityStarter.with(getContext(), FragmentTask.USER_CONTENT_LIST)
          .addBundle(new BundleSet.Builder().putUserActionType(UserActionType.RATE)
              .putPrivacyLevel(mUser.getPrivacyLevel())
              .putActionCount(mUser.getActionCount(CategoryType.MOVIES.getApiPath()).getRatings())
              .putCategoryType(CategoryType.MOVIES)
              .putUserCode(mUser.getCode())
              .build()
              .getBundle())
          .start();
    } else {
      Toast.makeText(getContext(), getPrivacyMessage(mUser.getName(), mUser.getPrivacyLevel()),
          Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.profile_shortcut_tv_season) void onClickShortcutTvSeason() {
    if (ableToEnter(mUser.getPrivacyLevel())) {
      ActivityStarter.with(getContext(), FragmentTask.USER_CONTENT_LIST)
          .addBundle(new BundleSet.Builder().putUserActionType(UserActionType.RATE)
              .putPrivacyLevel(mUser.getPrivacyLevel())
              .putActionCount(
                  mUser.getActionCount(CategoryType.TV_SEASONS.getApiPath()).getRatings())
              .putCategoryType(CategoryType.TV_SEASONS)
              .putUserCode(mUser.getCode())
              .build()
              .getBundle())
          .start();
    } else {
      Toast.makeText(getContext(), getPrivacyMessage(mUser.getName(), mUser.getPrivacyLevel()),
          Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.profile_shortcut_book) void onClickShortcutBook() {
    ActivityStarter.with(getContext(), FragmentTask.EMPTY_BOOK).start();
  }
}
