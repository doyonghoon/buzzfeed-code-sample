package com.frograms.watcha.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.DetailFragment;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.RobotoRegularView;
import rx.functions.Action1;

/**
 * 개영페의 타이틀과 공유 버튼을 가진 뷰.
 *
 * @see DetailFragment#getCustomToolbarView()
 */
public class ContentToolbarView extends RelativeLayout {

  @Bind(R.id.content_toolbar_title) RobotoRegularView mTitle;
  @Bind(R.id.content_toolbar_icon) FontIconTextView mIcon;

  private Action1<SimpleSwatch> mColorUpdateAction = new Action1<SimpleSwatch>() {
    @Override public void call(SimpleSwatch simplePalette) {
      mTitle.setTextColor(simplePalette.getTextColor());
      mIcon.setTextColor(simplePalette.getTextColor());
    }
  };

  private Action1<String> mTitleUpdateAction = new Action1<String>() {
    @Override public void call(String title) {
      mTitle.setText(title);
      mIcon.setText(R.string.icon_share);
    }
  };

  public ContentToolbarView(Context context) {
    this(context, null, 0);
  }

  public ContentToolbarView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ContentToolbarView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_content_toolbar, this);
    ButterKnife.bind(this);
  }

  public Action1<SimpleSwatch> getColorUpdateAction() {
    return mColorUpdateAction;
  }

  public Action1<String> getTitleUpdateAction() {
    return mTitleUpdateAction;
  }

  public FontIconTextView getIcon() {
    return mIcon;
  }

  public static class SimpleSwatch {

    private int mTextColor;
    private int mBackgroundColor;
    private int mStatusbarColor;

    public SimpleSwatch(int textColor, int backgroundColor, int statusbarColor) {
      mTextColor = textColor;
      mBackgroundColor = backgroundColor;
      mStatusbarColor = statusbarColor;
    }

    public int getTextColor() {
      return mTextColor;
    }

    public int getBackgroundColor() {
      return mBackgroundColor;
    }

    public int getStatusbarColor() {
      return mStatusbarColor;
    }
  }
}
