package com.frograms.watcha.view;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.RobotoRegularView;

public class SimpleEmptyView extends RelativeLayout {

  @Bind(R.id.empty_simple_icon) FontIconTextView mIconView;
  @Bind(R.id.empty_simple_description) RobotoRegularView mTextView;

  public SimpleEmptyView(Context context, @StringRes int iconResId, String text) {
    this(context, null, 0);
    init(iconResId, text);
  }

  public SimpleEmptyView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SimpleEmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    inflate(getContext(), R.layout.view_empty_simple, this);
    ButterKnife.bind(this);
  }

  private void init(@StringRes int iconResId, String text) {
    mIconView.setText(iconResId);
    mTextView.setText(text);
  }

  public FontIconTextView getIconView() {
    return mIconView;
  }

  public RobotoRegularView getTextView() {
    return mTextView;
  }
}
