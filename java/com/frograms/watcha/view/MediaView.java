package com.frograms.watcha.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.Media;
import com.frograms.watcha.utils.YoutubeThumbnailUtils;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.frograms.watcha.view.widget.wImages.ImageType;
import com.frograms.watcha.view.widget.wImages.RatioType;

public class MediaView extends FrameLayout implements View.OnClickListener {

  public interface OnClickMediaListener {
    void onClickMedia(Media media);
  }

  private GreatImageView mImage;
  private FontIconTextView mPlay;

  private Media mMedia;
  private String youtubeId;

  private OnClickMediaListener mGalleryOnClickListener = null;

  public MediaView(Context context, Media media, OnClickMediaListener galleryClickListener) {
    super(context);
    mGalleryOnClickListener = galleryClickListener;
    init();
    setItem(media);
  }

  @Override public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.setOnClickListener(null);
    mMedia = null;
  }

  @Override public void onClick(View v) {
    if (mMedia == null) {
      return;
    }
    switch (mMedia.getType()) {
      case YOUTUBE:
        WatchaApp.getInstance().goYoutube(getContext(), youtubeId);
        break;
      case PHOTO:
      case COVER:
        if (mGalleryOnClickListener != null) {
          mGalleryOnClickListener.onClickMedia(mMedia);
        }
        break;
    }
  }

  public void setItem(Media media) {
    this.mMedia = media;
    if (media == null) {
      return;
    }

    Media.MediaType type = media.getType();
    switch (type) {
      case PHOTO:
        if (media.getPhoto() != null) {
          Glide.with(getContext())
              .load(media.getPhoto().getLarge())
              .asBitmap()
              .into(getImageTarget());
          //mImage.getLayoutParams().width =
          //    getContext().getResources().getDimensionPixelSize(R.dimen.gallery_poster_width);
          //mImage.getLayoutParams().height =
          //    (int) WImageType.POSTER.getSizeRatio() * mImage.getLayoutParams().width;
          //mImage.setImageType(ImageType.POSTER);
          //mImage.setRatioType(RatioType.POSTER);
          //mImage.load(media.getPhoto().getDefault());
          //mImage.setOnFinishLoadingImageListener(getImageTarget());
          mPlay.setVisibility(View.GONE);
        }
        break;
      case YOUTUBE:
        if (!TextUtils.isEmpty(media.getUrl())) {
          mImage.setImageType(ImageType.STILLCUT);
          mImage.setRatioType(RatioType.STILLCUT_GALLERY);
          mImage.setScaleType(ImageView.ScaleType.FIT_XY);
          mImage.getLayoutParams().width =
              (int) (getContext().getResources().getDimensionPixelSize(R.dimen.gallery_poster_width)
                  * 2.0385);
          mImage.load(YoutubeThumbnailUtils.getUrl(media.getUrl(),
              YoutubeThumbnailUtils.Quality.HQ_DEFAULT));
          mImage.setOnFinishLoadingImageListener(new BitmapImageViewTarget(mImage){
            @Override public void onLoadFailed(Exception e, Drawable errorDrawable) {

              //View를 GONE 시키더라도 위에서 설정된 width값이 남아있다.
              getLayoutParams().width = 0;
              mImage.setVisibility(View.GONE);
            }

          });

          youtubeId = media.getUrl();
          mPlay.setVisibility(View.VISIBLE);
        }
        break;
      case COVER:
        if (media.getPhoto() != null) {
          Glide.with(getContext())
              .load(media.getPhoto().getSmall())
              .asBitmap()
              .into(getImageTarget());
          //mImage.setImageType(ImageType.STILLCUT);
          //mImage.setRatioType(RatioType.STILLCUT_GALLERY);
          //mImage.setScaleType(ImageView.ScaleType.FIT_XY);
          //mImage.getLayoutParams().width =
          //    (int) (getContext().getResources().getDimensionPixelSize(R.dimen.gallery_poster_width)
          //        * 2.0385);
          //mImage.load(media.getPhoto().getSmall());
          //mImage.setOnFinishLoadingImageListener(getImageTarget());
          mPlay.setVisibility(View.GONE);
        }
        break;
    }
  }

  private void init() {
    inflate(getContext(), R.layout.layer_media, this);
    setOnClickListener(this);

    mImage = (GreatImageView) findViewById(R.id.media_image);
    mPlay = (FontIconTextView) findViewById(R.id.media_play);
  }

  private BitmapImageViewTarget mImageTarget = null;

  private BitmapImageViewTarget getImageTarget() {
    if (mImageTarget == null) {
      mImageTarget = new BitmapImageViewTarget(mImage) {
        @Override
        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
          if (mMedia != null && bitmap != null && bitmap.getWidth() > bitmap.getHeight()) {
            mImage.setImageType(ImageType.STILLCUT)
                .setRatioType(RatioType.STILLCUT_GALLERY)
                .setScaleType(ImageView.ScaleType.FIT_XY);

            float width =
                getContext().getResources().getDimensionPixelSize(R.dimen.gallery_poster_width)
                    * 2.0385f;
            mImage.getLayoutParams().width = (int) width;
            //mImage.getLayoutParams().height = (int) (width / RatioType.STILLCUT_GALLERY.getSizeRatio());
            //mImage.invalidate();
            mMedia.setType(Media.MediaType.COVER);
          } else if (mMedia != null) {
            mImage.setImageType(ImageType.POSTER);
            mImage.setRatioType(RatioType.POSTER);

            int width =
                getContext().getResources().getDimensionPixelSize(R.dimen.gallery_poster_width);
            mImage.getLayoutParams().width = width;
            //mImage.getLayoutParams().height = (int) (width / RatioType.POSTER.getSizeRatio());
            //mImage.invalidate();
            mMedia.setType(Media.MediaType.PHOTO);
          }

          super.onResourceReady(bitmap, glideAnimation);
        }
      };
    }
    return mImageTarget;
  }

}
