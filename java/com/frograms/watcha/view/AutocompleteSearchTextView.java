package com.frograms.watcha.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * 검색뷰.
 */
public class AutocompleteSearchTextView extends AutoCompleteTextView {

  public AutocompleteSearchTextView(Context context) {
    super(context);
  }

  public AutocompleteSearchTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public AutocompleteSearchTextView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override public boolean enoughToFilter() {
    return true;
  }

  @Override
  protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
    super.onFocusChanged(focused, direction, previouslyFocusedRect);
    if (focused && getAdapter() != null) {
      performFiltering(getText(), 0);
    }
  }
}
