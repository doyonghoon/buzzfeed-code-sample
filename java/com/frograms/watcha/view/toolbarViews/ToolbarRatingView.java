package com.frograms.watcha.view.toolbarViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import me.grantland.widget.AutofitTextView;

public class ToolbarRatingView extends RelativeLayout {

  @Bind(R.id.rating_back) View mBackView;
  @Bind(R.id.rating_num) TextView mRatingNumView;
  @Bind(R.id.rating_text) AutofitTextView mRatingTextView;
  @Bind(R.id.rating_next) TextView mRatingNextView;

  public ToolbarRatingView(Context context) {
    this(context, null);
  }

  public ToolbarRatingView(Context context, AttributeSet attr) {
    this(context, attr, 0);
  }

  public ToolbarRatingView(Context context, AttributeSet attr, int defStyle) {
    super(context, attr, defStyle);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_toolbar_rating, this);
    ButterKnife.bind(this);
    if (!isInEditMode()) {
      FontHelper.RobotoLight(mRatingTextView);
    }
  }

  public TextView getRatingTextView() {
    return mRatingTextView;
  }

  public TextView getNextView() {
    return mRatingNextView;
  }

  public TextView getRatingCountTextView() {
    return mRatingNumView;
  }

  public View getBackView() {
    return mBackView;
  }
}
