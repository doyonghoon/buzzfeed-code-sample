package com.frograms.watcha.view;

import android.content.Context;
import com.frograms.watcha.view.intro.RoundCornerDialog;

/**
 * Created by doyonghoon on 15. 2. 27..
 */
public class WUserActionPopupDialog extends RoundCornerDialog {
  public WUserActionPopupDialog(Context context, RoundCornerListener listener) {
    super(context, listener);
  }
}
