package com.frograms.watcha.view;

import android.content.Context;
import android.support.annotation.XmlRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.intro.RoundCornerDialog;
import com.frograms.watcha.view.textviews.DefTextView;

/**
 * 팝업 다이얼로그에 들어가는 기본 배경 뷰.
 */
public class WUserActionPopupViewLayout extends LinearLayout
    implements ResizableRatingBar.OnRatingBarChangeListener,
    ResizableRatingBar.OnRatingBarClickListener {

  private WPopupType mType;
  private UserAction mUserAction;
  private UserActionPopupListener mListener;
  private RoundCornerDialog mDialog = null;

  //@IntDef({ WISH_LAYOUT_ID, COMMENT_LAYOUT_ID, ADD_COLLECTION_LAYOUT_ID, MEH_LAYOUT_ID })
  //public @interface ButtonGroupIdentifier {
  //}
  //private static final int WISH_LAYOUT_ID = R.id.wpopup_wish;
  //private static final int COMMENT_LAYOUT_ID = R.id.wpopup_comment;
  //private static final int ADD_COLLECTION_LAYOUT_ID = R.id.wpopup_add_collection;
  //private static final int MEH_LAYOUT_ID = R.id.wpopup_meh;

  @Bind(R.id.root) LinearLayout mRootView;
  @Bind(R.id.wpopup_content_title) TextView mContentTitle;
  @Bind(R.id.wpopup_content_subtitle) TextView mContentSubTitle;
  @Bind(R.id.wpopup_ratingbar) ResizableRatingBar mRatingbar;
  @Bind(R.id.wpopup_wish) LinearLayout mWishLayout;
  @Bind(R.id.wpopup_comment) LinearLayout mCommentLayout;
  @Bind(R.id.wpopup_add_collection) LinearLayout mAddCollectionLayout;
  @Bind(R.id.wpopup_meh) LinearLayout mMehLayout;

  private View[] mComponentGroup = null;
  private DefTextView mWishTitle, mCommentTitle, mAddCollectionTitle, mMehTitle;

  public WUserActionPopupViewLayout(Context context, WPopupType type,
      UserActionPopupListener listener) {
    this(context, null, 0);
    mListener = listener;
    setType(type);
  }

  public WUserActionPopupViewLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public WUserActionPopupViewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_wpopup_layout, this);
    ButterKnife.bind(this);
    setClickable(true);

    mWishTitle = ButterKnife.findById(mWishLayout, R.id.wpopup_btn_title);
    mCommentTitle = ButterKnife.findById(mCommentLayout, R.id.wpopup_btn_title);
    mAddCollectionTitle = ButterKnife.findById(mAddCollectionLayout, R.id.wpopup_btn_title);
    mMehTitle = ButterKnife.findById(mMehLayout, R.id.wpopup_btn_title);
    mWishTitle.setText("보고싶어요");
    mCommentTitle.setText("코멘트");
    mAddCollectionTitle.setText("컬렉션 담기");
    mMehTitle.setText("관심없어요");

    mWishTitle.setCompoundDrawables(getIconDrawable(R.xml.icon_wish), null, null, null);
    mCommentTitle.setCompoundDrawables(getIconDrawable(R.xml.icon_comment), null, null, null);
    mAddCollectionTitle.setCompoundDrawables(getIconDrawable(R.xml.icon_collection), null, null,
        null);
    mMehTitle.setCompoundDrawables(getIconDrawable(R.xml.icon_meh), null, null, null);

    mRatingbar.setOnRatingBarChangeListener(this);
    mRatingbar.setOnRatingBarClickListener(this);

    mComponentGroup = new View[5];
    mComponentGroup[0] = mRatingbar;
    mComponentGroup[1] = mWishLayout;
    mComponentGroup[2] = mCommentLayout;
    mComponentGroup[3] = mAddCollectionLayout;
    mComponentGroup[4] = mMehLayout;

    FontHelper.RobotoRegular(mContentTitle);
    FontHelper.RobotoRegular(mContentSubTitle);
    FontHelper.RobotoRegular(mWishTitle);
    FontHelper.RobotoRegular(mCommentTitle);
    FontHelper.RobotoRegular(mAddCollectionTitle);
    FontHelper.RobotoRegular(mMehTitle);
  }

  private Class<? extends BaseFragment> getFragmentClass() {
    if (getContext() != null && getContext() instanceof BaseActivity) {
      return ((BaseActivity) getContext()).getFragment().getClass();
    }
    return null;
  }

  private FontIconDrawable getIconDrawable(@XmlRes int xmlId) {
    FontIconDrawable drawable = FontIconDrawable.inflate(getContext(), xmlId);
    drawable.setTextSize(getResources().getDimension(R.dimen.popup_menu_font));
    drawable.setTextColor(getResources().getColor(R.color.light_gray));
    return drawable;
  }

  public void setTitles(String title, String subtitle) {
    mContentTitle.setText(title);
    mContentSubTitle.setText(subtitle);
  }

  public void setUserAction(UserAction userAction) {
    mUserAction = userAction;
    float rating = (mUserAction == null) ? 0 : mUserAction.getRating();
    boolean isWished = (mUserAction != null) && mUserAction.isWished();
    boolean hasComment = (mUserAction != null) && mUserAction.getComment() != null;
    boolean isMehed = (mUserAction != null) && mUserAction.isMehed();

    //rating
    mRatingbar.setRating(rating);

    //wish
    mWishTitle.setText(isWished ? "보고싶어요 취소" : "보고싶어요");
    setHighlighted(mWishTitle, isWished);

    //comment
    setHighlighted(mCommentTitle, hasComment);

    //meh
    mMehTitle.setText(isMehed ? "관심없어요 취소" : "관심없어요");
    setHighlighted(mMehTitle, isMehed);
  }

  public void setDialog(RoundCornerDialog dialog) {
    mDialog = dialog;
  }

  public void setType(WPopupType type) {
    mType = type;
    for (View v : mComponentGroup) {
      boolean canShow = mType.getViewIds().contains(v.getId());
      v.setVisibility(canShow ? View.VISIBLE : View.GONE);
    }
  }

  private void dismissDialog() {
    if (mDialog != null) {
      mDialog.setDismissAnimation();
    }
  }

  private void setHighlighted(DefTextView textview, boolean highlight) {

    FontIconDrawable drawable = (FontIconDrawable) textview.getCompoundDrawables()[0];
    drawable.setTextColor(getResources().getColor(highlight ? R.color.pink : R.color.light_gray));
  }

  @OnClick(R.id.wpopup_wish) public void onClickWish() {
    if (mListener != null) {
      mListener.onClickWish();
    }
    dismissDialog();
  }

  @OnClick(R.id.wpopup_comment) public void onClickComment() {
    if (mListener != null) {
      mListener.onClickComment();
    }
    dismissDialog();
  }

  @OnClick(R.id.wpopup_add_collection) public void onClickAddCollection() {
    if (mListener != null) {
      mListener.onClickAddCollection();
    }
    dismissDialog();
  }

  @OnClick(R.id.wpopup_meh) public void onClickMeh() {
    if (mListener != null) {
      mListener.onClickMeh();
    }
    dismissDialog();
  }

  @Override public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
    if (fromUser && mListener != null) {
      mListener.onChangedRating(rating);
    }
    dismissDialog();
  }

  @Override public void onRatingClick(RatingBar ratingBar, float rating) {
    if (mListener != null) {
      mListener.onChangedRating(rating);
    }
    dismissDialog();
  }
}
