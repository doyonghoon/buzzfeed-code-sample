package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.RatingTabFragment;
import com.frograms.watcha.fragment.TutorialFragment;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.ResizableRatingBar;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsUserActionView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.WImageView;
import java.util.Date;

public class ItemContentTutorialView extends ItemAbsUserActionView<Content> {

  @Bind(R.id.photo) WImageView mImage;
  @Bind(R.id.more_btn) FontIconTextView mMore;
  @Bind(R.id.title) TextView mTitleView;
  @Bind(R.id.description) DefTextView mDescription;
  @Bind(R.id.rating) ResizableRatingBar mRatingBar;

  private static FontIconDrawable mWishDrawable, mMehDrawable;

  private String description;
  private String mTitle;
  private Date mReleasedAt;

  public ItemContentTutorialView(Context context) {
    super(context);
  }

  public ItemContentTutorialView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void finishRating(RatingData data) {
    setUserAction();

    if (getContext() != null) {
      if (((BaseActivity) getContext()).getFragment() instanceof RatingTabFragment) {
        ((RatingTabFragment) ((BaseActivity) getContext()).getFragment()).setRatingsCount();
      } else if (((BaseActivity) getContext()).getFragment() instanceof TutorialFragment) {
        ((TutorialFragment) ((BaseActivity) getContext()).getFragment()).setRatingsCount();
      }
    }
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_ratable;
  }

  @Override protected void init() {
    super.init();

    if (mWishDrawable == null) {
      mWishDrawable = FontIconDrawable.inflate(getContext(), R.xml.icon_wish);
      mWishDrawable.setTextColor(getResources().getColor(R.color.pink));
      mWishDrawable.setTextSize(getResources().getDimension(R.dimen.small_desc_font));
    }

    if (mMehDrawable == null) {
      mMehDrawable = FontIconDrawable.inflate(getContext(), R.xml.icon_meh);
      mMehDrawable.setTextColor(getResources().getColor(R.color.pink));
      mMehDrawable.setTextSize(getResources().getDimension(R.dimen.small_desc_font));
    }

    mMore.setVisibility(GONE);

    mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

      @Override public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        if (fromUser) {
          if (mUserAction != null) {
            if (mUserAction.getRating() == rating) {
              rating = 0.0f;
            }
          }

          if (rating > 0) {
            requestRating(mReleasedAt, UserActionOperator.ActionType.RATING,
                String.valueOf(rating));
          } else {
            requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
          }
        }
      }
    });

    mRatingBar.setOnRatingBarClickListener(new ResizableRatingBar.OnRatingBarClickListener() {
      @Override public void onRatingClick(RatingBar ratingBar, float rating) {
        if (mUserAction == null) return;
        if (mUserAction.getRating() == rating) {
          requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
        }
      }
    });
  }

  @Override public void onClick(View v) {
    super.onClick(v);
  }

  @Override public boolean setItem(@NonNull Content item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    setBackgroundResource(R.drawable.bg_item);
    mCategory = item.getCategory();
    mCode = item.getCode();
    mTitle = item.getTitle();
    mReleasedAt = item.getReleasedAt();

    String url = (item.getPoster() != null) ? item.getPoster().getMedium() : null;
    mImage.load(url);

    mTitleView.setText(item.getTitle());
    description = item.getDescription();
    setUserAction();

    return true;
  }

  private void setUserAction() {

    mUserAction = CacheManager.getMyUserAction(mCode);

    mDescription.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    mDescription.setText(description);

    if (mUserAction == null) {
      WLog.i("UserAction is Null");
      mRatingBar.setRating(0);
      return;
    }

    if (mUserAction.isWished()) {
      mDescription.setCompoundDrawables(mWishDrawable, null, null, null);
      mDescription.setText(getContext().getString(R.string.wish));
    }

    if (mUserAction.isMehed()) {
      mDescription.setCompoundDrawables(mMehDrawable, null, null, null);
      mDescription.setText(getContext().getString(R.string.ignore));
    }

    if (mUserAction.getRating() > 0) {
      mRatingBar.setRating(mUserAction.getRating());
    } else {
      mRatingBar.setRating(0);
    }
  }
}
