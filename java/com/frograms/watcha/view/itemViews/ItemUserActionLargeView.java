package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.views.ItemUserActionView;

public class ItemUserActionLargeView extends ItemUserActionView<UserAction> {

  public ItemUserActionLargeView(Context context) {
    super(context);
  }

  public ItemUserActionLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public boolean setItem(UserAction item) {
    if (!super.setItem(item)) return false;
    return setUserActionItem(item);
  }
}
