package com.frograms.watcha.view.itemViews.abstracts;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.CommentActionHelper;
import com.frograms.watcha.helpers.CommentActionHelper.ActionType;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Comment;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import java.util.Map;

public abstract class ItemAbsCommentActionView<T extends Item> extends ItemView<T> {

  protected String mContentCode;
  protected Comment mComment;
  protected String mName;
  private static int itemClicked = -1;

  //private WBottomSheetLayout mBottomSheetLayout;

  private ApiResponseListener<BaseResponse<CommentReportData>> mListener;

  //protected ReplyCardBottomSheet mBottomSheet;

  public ItemAbsCommentActionView(Context context) {
    super(context);
  }

  public ItemAbsCommentActionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  protected void requestRating(ActionType actionType, boolean isRating,
      Map<String, String> params) {
    CommentActionHelper.requestComment((BaseActivity) getContext(),
        CommentActionHelper.makeQueryType(actionType, isRating, mComment.getCode()), params,
        getReportListener());
  }

  protected void requestLike() {
    boolean isRequestLike = mComment == null || !mComment.isLiked();
    requestRating(ActionType.LIKE, isRequestLike, null);
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD,
        isRequestLike ? AnalyticsActionType.LIKE_COMMENT
            : AnalyticsActionType.CANCEL_LIKE_COMMENT).setLabel(getCardItemName()).build());
  }

  protected void requestImproper() {
    boolean isRequestImproper = mComment == null || !mComment.isImproper();
    requestRating(ActionType.IMPROPER, isRequestImproper, null);
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD,
        isRequestImproper ? AnalyticsActionType.IMPROPER_COMMENT
            : AnalyticsActionType.CANCEL_IMPROPER_COMMENT).setLabel(getCardItemName()).build());
  }

  protected void requestSpoiler() {
    boolean isRequestSpoiler = mComment == null || !mComment.isSpoiler();
    requestRating(ActionType.SPOILER, isRequestSpoiler, null);
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD,
        isRequestSpoiler ? AnalyticsActionType.SPOILER_COMMENT
            : AnalyticsActionType.CANCEL_SPOILER_COMMENT).setLabel(getCardItemName()).build());
  }

  protected ApiResponseListener<BaseResponse<CommentReportData>> getReportListener() {
    if (mListener == null) {
      mListener = (queryType, result) -> {
        if (queryType.equals(QueryType.DELETE_COMMENT)) {
          UserAction action = CacheManager.getMyUserAction(mContentCode);
          action.setComment(null);
          CacheManager.putMyUserAction(action);

          int count;
          CategoryType type = CategoryType.MOVIES;
          switch (mContentCode.charAt(0)) {
            case 'm':
              type = CategoryType.MOVIES;
              break;

            case 't':
              type = CategoryType.TV_SEASONS;
              break;
          }

          count = WatchaApp.getUser().getActionCount(type.getApiPath()).getComments();
          WatchaApp.getUser().getActionCount(type.getApiPath()).setComments(count - 1);

          mComment = null;

          if (onRemoveItemListener != null) {
            onRemoveItemListener.onRemove(getPosition());
          }
          return;
        }

        if (result.getData().isLiked() != null) {
          boolean islike = Boolean.valueOf(result.getData().isLiked());
          AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
              islike ? AnalyticsActionType.LIKE_COMMENT : AnalyticsActionType.CANCEL_LIKE_COMMENT)
              .build());
          if (islike != mComment.isLiked()) {
            mComment.addLike(islike);
          }
          mComment.setIsLiked(islike);
          setLiked();
        }

        String message = null;
        if (result.getData().isSpoilerCautioned() != null) {
          // 신고하기 스포일러 결과가 잘 반영됐다는 메세지 보여줌.
          boolean isSpoiler = Boolean.valueOf(result.getData().isSpoilerCautioned());
          mComment.setIsSpoiler(isSpoiler);
          message = getResources().getString(
              isSpoiler ? R.string.report_spoiler_result : R.string.report_cancel_result);
        }

        if (result.getData().isImproperCautioned() != null) {
          // 신고하기 부적절한 표현 결과가 잘 반영됐다는 메세지 보여줌.
          boolean isImproper = Boolean.valueOf(result.getData().isImproperCautioned());
          mComment.setIsImproper(isImproper);
          message = getResources().getString(
              isImproper ? R.string.report_improper_result : R.string.report_cancel_result);
        }

        if (!TextUtils.isEmpty(message) && getContext() instanceof BaseActivity) {
          BaseActivity act = (BaseActivity) getContext();
          Toast.makeText(act, message, Toast.LENGTH_SHORT).show();
        }

        CacheManager.putComment(mComment);
      };
    }

    return mListener;
  }

  protected abstract void setLiked();

  protected abstract void setReplies();

  protected void showDialog() {
    String[] strs = getContext().getResources().getStringArray(R.array.deck_comment_dialog_msg);

    FakeActionBar header = new FakeActionBar(getContext());
    header.setTitle(mName + "님의 코멘트");
    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getContext(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getContext()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnDismissListener(new OnDismissListener() {
          @Override public void onDismiss(DialogPlus dialogPlus) {
            switch (itemClicked - 1) {
              case 0:
                if (mComment != null) {
                  ActivityStarter.with(getContext(),
                      "watcha://comments/" + mComment.getCode() + "/edit").start();
                }
                break;
              case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getResources().getString(R.string.want_to_delete))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                      @Override public void onClick(DialogInterface dialogInterface, int i) {
                        requestRating(ActionType.DELETE, true, null);
                      }
                    })
                    .setNegativeButton(R.string.no, null);
                builder.show();
                break;
            }
            itemClicked = -1;
          }
        })
        .setOnItemClickListener(new OnItemClickListener() {
          @Override public void onItemClick(DialogPlus dialogPlus, Object o, View view, int i) {
            dialogPlus.dismiss();
            itemClicked = i;
          }
        })

        .create();
    dialog.show();
  }

  protected void showReportDialog() {
    if (mComment == null) {
      return;
    }
    String[] strs = getContext().getResources().getStringArray(R.array.report_dialog_msg);

    String[] dialogItems = new String[2];

    if (mComment.isSpoiler()) {
      dialogItems[0] = strs[1];
    } else {
      dialogItems[0] = strs[0];
    }

    if (mComment.isImproper()) {
      dialogItems[1] = strs[3];
    } else {
      dialogItems[1] = strs[2];
    }

    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getContext(), R.layout.view_list_item_basic, R.id.text1, dialogItems);
    DialogPlus dialog = new DialogPlus.Builder(getContext()).setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnItemClickListener(new OnItemClickListener() {
          @Override public void onItemClick(DialogPlus dialogPlus, Object o, View view, int i) {
            dialogPlus.dismiss();
            switch (i) {
              case 0:
                requestSpoiler();
                break;
              case 1:
                requestImproper();
                break;
            }
          }
        })
        .create();
    dialog.show();
  }

  //protected BottomSheetLayout getReplyLayout() {
  //  if (mBottomSheetLayout == null) {
  //    mBottomSheetLayout = ((BaseActivity)getContext()).mBottomSheetLayout;
  //    mBottomSheetLayout.shouldPassPeek(true);
  //  }
  //
  //  return mBottomSheetLayout;
  //}

  //protected void showReplyLayout() {
  //  mBottomSheet = new ReplyCardBottomSheet(new CardBottomSheet.Builder(getContext()).setTitle("코멘트 댓글")
  //          .setQueryType(
  //          QueryType.GENERAL_CARDS.setApi("comments/" + mComment.getCode() + "/replies")));
  //
  //  mBottomSheet.setReplyType(ReplyCardBottomSheet.REPLY_TYPE.COMMENT);
  //  mBottomSheet.setCommentCode(mComment.getCode());
  //  mBottomSheet.setOnEditReplyListener(new ReplyCardBottomSheet.OnEditReplyListener() {
  //
  //    @Override public void onWrite() {
  //      mComment.addReply(true);
  //      setReplies();
  //
  //      CacheManager.putComment(mComment);
  //    }
  //  });
  //
  //  getReplyLayout().showWithSheetView(mBottomSheet);
  //
  //}

  protected void scaleSpringAnimation(final View view) {
    SpringSystem springSystem = SpringSystem.create();
    final Spring spring1 = springSystem.createSpring();
    spring1.setSpringConfig(new SpringConfig(250f, 17));
    spring1.addListener(new SimpleSpringListener() {
      @Override public void onSpringUpdate(Spring spring) {
        float value = (float) spring.getCurrentValue();
        float scale = 1f + (value * 0.4f);
        view.setScaleX(scale);
        view.setScaleY(scale);
      }
    });

    spring1.setEndValue(1);

    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      public void run() {
        if (getContext() == null) return;

        spring1.setEndValue(0);
      }
    }, 150);
  }

  protected void goReply(boolean shouldOpenKeyboard) {
    if (mComment != null) {
      ActivityStarter.with(getContext(), FragmentTask.REPLY)
          .addBundle(new BundleSet.Builder().putReplyType(ReplyFragment.ReplyType.COMMENT)
              .putPreviousScreenName(getScreenName())
              .putCommentCode(mComment.getCode())
              .putIsOpenKeyboard(shouldOpenKeyboard)
              .build()
              .getBundle())
          .start();
    }
  }
}
