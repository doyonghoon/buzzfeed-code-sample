package com.frograms.watcha.view.itemViews.abstracts;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.frograms.watcha.helpers.UserActionHelper.OnBaseRatingResponseListener;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.UserActionOperator.ActionType;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.response.data.RatingData;
import java.util.Collections;
import java.util.Date;
import org.apache.commons.lang3.math.NumberUtils;

public abstract class ItemAbsUserActionView<T extends Item> extends ItemView<T> {

  protected String mCategory, mCode;
  protected Date mReleasedAt;
  protected UserAction mUserAction;

  public ItemAbsUserActionView(Context context) {
    super(context);
  }

  public ItemAbsUserActionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  protected abstract void finishRating(RatingData data);

  protected void requestRating(Date releasedAt, UserActionOperator.ActionType actionType, @Nullable String ratingValue) {
    UserActionOperator.Builder builder =
        new UserActionOperator.Builder((Activity) getContext(), actionType).setContentCode(mCode)
            .setCategoryType(CategoryType.getCategory(mCategory))
            .setReleasedAt(releasedAt);
    if (NumberUtils.isNumber(ratingValue)) {
      builder.setParams(Collections.singletonMap("value", ratingValue));
    }
    builder.setRatingResponseListener(new OnBaseRatingResponseListener(getContext()));
    builder.build().request(this::finishRating);
  }

  protected void requestWish() {
    if (mUserAction == null || !mUserAction.isWished()) {
      requestRating(null, ActionType.WISH, null);
    } else {
      requestRating(null, ActionType.WISH_CANCEL, null);
    }
  }

  protected void requestMeh() {
    if (mUserAction == null || !mUserAction.isMehed()) {
      requestRating(null, ActionType.MEH, null);
    } else {
      requestRating(null, ActionType.MEH_CANCEL, null);
    }
  }

  protected void goDetailContent() {
    if (mCategory != null && mCode != null) {
      ActivityStarter.with(getContext(), FragmentTask.DETAIL_CONTENT)
          .addBundle(new BundleSet.Builder().putCategoryType(CategoryType.getCategory(mCategory))
              .putContentCode(mCode)
              .putPreviousScreenName(getScreenName())
              .build()
              .getBundle())
          .start();
    }
  }

  protected void goWriteComment() {
    BundleSet.Builder bundleSet = new BundleSet.Builder();

    if (mUserAction == null) {
      bundleSet.putContentCode(mCode);
    } else if (mUserAction.getComment() == null) {
      bundleSet.putContentCode(mCode);
    } else {
      bundleSet.putCommentCode(mUserAction.getComment().getCode());
    }

    ActivityStarter.with(getContext(), FragmentTask.WRITE_COMMENT)
        .addBundle(bundleSet.putPreviousScreenName(getScreenName()).build().getBundle())
        .start();
  }
}
