package com.frograms.watcha.view.itemViews.abstracts;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.GridDrawerAdapter;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.DeckActionHelper;
import com.frograms.watcha.helpers.DeckActionHelper.ActionType;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.model.response.data.ShareMessageData;
import com.frograms.watcha.models.ShareApp;
import com.frograms.watcha.retrofit.ApiResponseListener;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.ShareAppLoader;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.views.FakeActionBar;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import java.util.List;
import java.util.Map;

public abstract class ItemAbsDeckActionView<T extends Item> extends ItemView<T> {

  protected DeckBase mDeck;
  protected String mName;
  private static int itemClicked = -1;

  //private WBottomSheetLayout mBottomSheetLayout;
  //protected ReplyCardBottomSheet mBottomSheet;

  private ApiResponseListener<BaseResponse<CommentReportData>> mListener;

  public ItemAbsDeckActionView(Context context) {
    super(context);
  }

  public ItemAbsDeckActionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  protected void requestRating(ActionType actionType, boolean isRating,
      Map<String, String> params) {
    DeckActionHelper.requestDeck((BaseActivity) getContext(),
        DeckActionHelper.makeQueryType(actionType, isRating, mDeck.getCode()), mDeck.getCode(), params,
        getReportListener());
  }

  protected void requestLike() {
    if (mDeck == null) return;
    boolean isRequestLike = !mDeck.isLiked();
    requestRating(ActionType.LIKE, isRequestLike, null);
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL,
        isRequestLike ? AnalyticsActionType.LIKE_DECK
            : AnalyticsActionType.CANCEL_LIKE_DECK)
        .setLabel(getCardItemName())
        .build());
  }

  protected void requestDelete() {
    if (mDeck == null) return;

    requestRating(null, false, null);
  }

  protected void getShareUrl() {
    GridDrawerAdapter adapter = new GridDrawerAdapter(getContext());
    final ShareAppLoader loader = new ShareAppLoader(getContext());
    List<ShareApp> apps = loader.loadApps();
    adapter.addAll(apps);
    adapter.notifyDataSetChanged();
    FakeActionBar header = new FakeActionBar(getContext());
    header.setTitle(getResources().getString(R.string.do_share));
    DialogPlus dialog = new DialogPlus.Builder(getContext()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(adapter)
        .setGravity(DialogPlus.Gravity.BOTTOM)
        .setCancelable(true)
        .setOnItemClickListener((dialog1, item, view, position) -> {
          if (item instanceof ShareApp) {
            dialog1.dismiss();
            final ShareApp app = (ShareApp) item;
            WLog.i("item: " + app.getName());

            ApiResponseListener<BaseResponse<ShareMessageData>> listener = (queryType, result) -> {
              if (mDeck != null) {
                ShareMessageData data = result.getData();
                String message = String.format("%s #왓챠 %s", mDeck.getName(), data.getUrl());
                if (app.getPackageName().equals(ShareAppLoader.PACKAGE_FACEBOOK)) {
                  message = data.getUrl();
                }
                loader.goChosenApp(message, app);
              }
            };

            DataProvider<BaseResponse<ShareMessageData>> provider =
                new DataProvider<>((BaseActivity) getContext(), QueryType.DECK_SHARE_MESSAGE.setApi(mDeck
                    .getCode()), new ReferrerBuilder(getScreenName()).appendArgument(mDeck.getCode()));
            provider.withDialogMessage(getResources().getString(R.string.loading));
            provider.responseTo(listener);
            provider.request();
          }
        })
        .create();
    dialog.show();
    AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD, AnalyticsActionType.SHARE_DECK)
        .setLabel(getCardItemName())
        .build());
  }

  protected ApiResponseListener<BaseResponse<CommentReportData>> getReportListener() {
    if (mListener == null) {
      mListener = (queryType, result) -> {
        if (queryType.equals(QueryType.DECK_DELETE) && mDeck != null) {
          CacheManager.deleteDeck(mDeck.getCode());
          WatchaApp.getUser().addDecksCount(false);

          mDeck = null;

          if (onRemoveItemListener != null) {
            onRemoveItemListener.onRemove(getPosition());
          }
          return;
        }

        if (result.getData().isLiked() != null) {
          boolean islike = Boolean.valueOf(result.getData().isLiked());
          if (islike != mDeck.isLiked()) {
            mDeck.addLike(islike);
          }
          mDeck.setIsLiked(islike);
          setLiked();
        }

        CacheManager.putDeck(mDeck);
      };
    }

    return mListener;
  }

  protected abstract void setLiked();

  protected abstract void setReplies();

  protected void showDialog() {
    String[] strs = getContext().getResources().getStringArray(R.array.deck_comment_dialog_msg);

    FakeActionBar header = new FakeActionBar(getContext());
    header.setTitle(mName + "님의 컬렉션");
    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getContext(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getContext()).setHeader(header)
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnDismissListener(new OnDismissListener() {
          @Override public void onDismiss(DialogPlus dialogPlus) {
            switch (itemClicked - 1) {
              case 0:
                ActivityStarter.with(getContext(), FragmentTask.CREATE_DECK)
                    .addBundle(
                        new BundleSet.Builder().putDeckCode(mDeck.getCode()).build().getBundle())
                    .start();
                break;
              case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getResources().getString(R.string.want_to_delete))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                      @Override public void onClick(DialogInterface dialogInterface, int i) {
                        requestDelete();
                      }
                    })
                    .setNegativeButton(R.string.no, null);

                builder.show();
                break;
            }
            itemClicked = -1;
          }
        })
        .setOnItemClickListener(new OnItemClickListener() {
          @Override public void onItemClick(DialogPlus dialogPlus, Object o, View view, int i) {
            dialogPlus.dismiss();
            itemClicked = i;
          }
        })
        .create();
    dialog.show();
  }

  //protected BottomSheetLayout getReplyLayout() {
  //  if (mBottomSheetLayout == null) {
  //    mBottomSheetLayout = ((BaseActivity)getContext()).mBottomSheetLayout;
  //    mBottomSheetLayout.shouldPassPeek(true);
  //  }
  //
  //  return mBottomSheetLayout;
  //}
  //
  //protected void showReplyLayout() {
  //  mBottomSheet = new ReplyCardBottomSheet(new CardBottomSheet.Builder(getContext()).setTitle("컬렉션 댓글")
  //      .setQueryType(
  //          QueryType.GENERAL_CARDS.setApi("decks/" + mDeck.getCode() + "/replies")));
  //
  //  mBottomSheet.setReplyType(ReplyCardBottomSheet.REPLY_TYPE.DECK);
  //  mBottomSheet.setCommentCode(mDeck.getCode());
  //  mBottomSheet.setOnEditReplyListener(new ReplyCardBottomSheet.OnEditReplyListener() {
  //
  //    @Override public void onWrite() {
  //      mDeck.addReply(true);
  //      setReplies();
  //
  //      CacheManager.putDeck(mDeck);
  //    }
  //  });
  //
  //  getReplyLayout().showWithSheetView(mBottomSheet);
  //}

  protected void scaleSpringAnimation(final View view) {
    SpringSystem springSystem = SpringSystem.create();
    final Spring spring1 = springSystem.createSpring();
    spring1.setSpringConfig(new SpringConfig(250f, 17));
    spring1.addListener(new SimpleSpringListener() {
      @Override public void onSpringUpdate(Spring spring) {
        float value = (float) spring.getCurrentValue();
        float scale = 1f + (value * 0.4f);
        view.setScaleX(scale);
        view.setScaleY(scale);
      }
    });

    spring1.setEndValue(1);

    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      public void run() {
        if (getContext() == null) return;

        spring1.setEndValue(0);
      }
    }, 150);
  }

  protected void goReply(boolean shouldOpenKeyboard) {
    ActivityStarter.with(getContext(), FragmentTask.REPLY)
        .addBundle(new BundleSet.Builder().putReplyType(ReplyFragment.ReplyType.DECK)
            .putPreviousScreenName(getScreenName())
            .putDeckCode(mDeck.getCode())
            .putIsOpenKeyboard(shouldOpenKeyboard)
            .build()
            .getBundle())
        .start();
  }
}
