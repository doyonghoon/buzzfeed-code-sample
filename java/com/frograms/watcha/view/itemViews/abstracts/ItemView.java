package com.frograms.watcha.view.itemViews.abstracts;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.commonsware.cwac.anddown.AndDown;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.RecyclerAdapter;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.EventOperator;
import com.frograms.watcha.listeners.OnRemoveItemListener;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.viewHolders.ViewHolderMeta;

public abstract class ItemView<T extends Item> extends FrameLayout implements View.OnClickListener {

  protected Event mEvent = null;
  private static AndDown sAndDown = null;
  private RecyclerAdapter.OnClickCardItemListener mOnCardItemClickListener;
  protected OnRemoveItemListener onRemoveItemListener;

  public ItemView(Context context) {
    super(context);
    init();
  }

  public ItemView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public void setOnRemoveItemListener(OnRemoveItemListener listener) {
    this.onRemoveItemListener = listener;
  }

  protected int getPosition() {
    return getTag(R.id.TAG_POSITION) != null ? (Integer) getTag(R.id.TAG_POSITION) : 0;
  }

  protected void init() {
    if (getLayoutId() > 0) {
      inflate(getContext(), getLayoutId(), this);
    }
    setBackgroundResource(getBackgroundId());
    ButterKnife.bind(this);
    setOnClickListener(this);
  }

  protected RecyclerAdapter.OnClickCardItemListener getCardItemClickListener() {
    return mOnCardItemClickListener;
  }

  public boolean setItem(T item) {
    mEvent = item.getEvent();
    setBackgroundClickable(this);
    if (getChildCount() > 0 && getChildAt(0) instanceof ViewGroup) {
      setBackgroundClickable(getChildAt(0));
    }
    return item != null;
  }

  private void setBackgroundClickable(View v) {
    if (v != null) {
      v.setBackgroundResource((mEvent != null) ? R.drawable.bg_item : getBackgroundId());
    }
  }

  protected int getBackgroundId() {
    return R.color.white;
  }

  @LayoutRes abstract protected int getLayoutId();

  @Override public void onClick(View v) {
    if (mEvent != null && v instanceof ItemView && getContext() instanceof Activity) {
      new EventOperator.Builder(getContext(), mEvent).setPreviousScreenName(getScreenName())
          .setCardItem(this)
          .build()
          .start();
    }
  }

  protected static void setText(TextView textView, String string) {
    if (string == null) {
      textView.setText("");
    } else {
      textView.setText(string);
    }
  }

  /**
   * 마크다운 텍스트 얻기 위해서 사용 함.
   *
   * @param rawText 마크다운 포맷인 텍스트.
   * @return TextView에 바로 갖다 쓸 수 있게 String대신 Spanned를 리턴 함.
   */
  public static Spanned getMarkdownText(String rawText) {
    if (sAndDown == null) {
      sAndDown = new AndDown();
    }
    if (TextUtils.isEmpty(rawText)) {
      return null;
    } else {
      String text = sAndDown.markdownToHtml(rawText);
      text = text.replace("<p>", "");
      text = text.replace("</p>", "");
      return Html.fromHtml(text);
    }
  }

  public void setOnCardItemClickListener(
      RecyclerAdapter.OnClickCardItemListener onCardItemClickListener) {
    this.mOnCardItemClickListener = onCardItemClickListener;
  }

  @Nullable protected ListFragment getListFragment() {
    if (getContext() != null && getContext() instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) getContext();
      if (baseActivity.getFragment() instanceof ListFragment) {
        WLog.i("ready to return listFragment");
        return (ListFragment) baseActivity.getFragment();
      } else if (baseActivity.getFragment() instanceof AbsPagerFragment) {
        AbsPagerFragment pagerFragment = (AbsPagerFragment) baseActivity.getFragment();
        if (pagerFragment.mTabs != null && !pagerFragment.mTabs.isEmpty()) {
          BaseFragment fragment =
              pagerFragment.mTabs.get(pagerFragment.mCurrentPageItem).getFragment();
          if (fragment instanceof ListFragment) {
            WLog.i("ready to return listFragment");
            return (ListFragment) fragment;
          }
        }
      }
    }
    return null;
  }

  @Nullable protected AbsPagerFragment getPagerFragment() {
    if (getContext() != null && getContext() instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) getContext();
      if (baseActivity.getFragment() instanceof AbsPagerFragment) {
        return (AbsPagerFragment) baseActivity.getFragment();
      }
    }
    return null;
  }

  @Nullable
  @AnalyticsEvent.AnalyticsEventLabel
  protected String getCardItemName() {
    ViewHolderMeta m = ViewHolderMeta.getViewHolderMeta(getClass());
    if (m != null) {
      @AnalyticsEvent.AnalyticsEventLabel String name = m.name();
      return name;
    }
    return null;
  }

  @Nullable
  @AnalyticsManager.ScreenNameType
  protected String getScreenName() {
    String result = null;
    if (getContext() != null && getContext() instanceof BaseActivity) {
      BaseActivity baseActivity = (BaseActivity) getContext();
      BaseFragment baseFragment = baseActivity.getFragment();
      if (baseFragment != null) {
        if (baseFragment instanceof AbsPagerFragment) {
          AbsPagerFragment absPagerFragment = (AbsPagerFragment) baseFragment;
          result = absPagerFragment.getCurrentFragment().getCurrentScreenName();
        } else {
          result = baseFragment.getCurrentScreenName();
        }
      }
    }
    WLog.i("screenNameResult: " + result);
    return result;
  }
}
