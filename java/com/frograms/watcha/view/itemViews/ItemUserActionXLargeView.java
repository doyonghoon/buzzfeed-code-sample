package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.ContentUserAction;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.model.views.ItemUserActionView;
import com.frograms.watcha.utils.WLog;

public class ItemUserActionXLargeView extends ItemUserActionView<ContentUserAction> {

  private CategoryType mCategoryType;

  public ItemUserActionXLargeView(Context context) {
    super(context);
  }

  public ItemUserActionXLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void init() {
    super.init();

    getContentLayer().setVisibility(VISIBLE);
    getContentLayer().removePreRating();
    getContentLayer().setOnClickListener(this);
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      case R.id.content_layer:
        ActivityStarter.with(getContext(), FragmentTask.DETAIL_CONTENT).
            addBundle(new BundleSet.Builder().putCategoryType(mCategoryType)
                .putContentCode(mContentCode)
                .putPreviousScreenName(getScreenName())
                .build()
                .getBundle()).start();
        break;
    }
  }

  @Override public boolean setItem(ContentUserAction item) {
    super.setItem(item);

    mCategoryType = CategoryType.getCategory(item.getCategory());
    mContentCode = item.getCode();

    UserAction userAction = null;
    if (item.getDefaultUserCode().equals(WatchaApp.getUser().getCode())) {

      userAction = CacheManager.getMyUserAction(item.getCode());
      mComment = userAction.getComment();
    } else {

      userAction = item.getUserAction(item.getDefaultUserCode());
      if (userAction != null && userAction.getComment() != null) {
        mComment = CacheManager.getComment(userAction.getComment().getCode());
      } else {
        mComment = null;
      }
    }

    if (userAction == null) {
      WLog.e("second");
      return false;
    }

    if (!setUserActionItem(userAction)) {
      WLog.e("third");
      return false;
    }

    boolean check = getContentLayer().setItem(item);
    if (!check) {
      WLog.e("fourth");
    }
    return check;
  }
}
