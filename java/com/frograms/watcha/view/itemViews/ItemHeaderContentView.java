package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.items.MediaItem;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.ImageType;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemHeaderContentView extends ItemView<MediaItem> {

  @Bind(R.id.thumbnail) WImageView thumbnail;
  @Bind(R.id.subject) TextView subject;
  @Bind(R.id.minutes) TextView minutes;

  private boolean inSelectContent = false;

  public ItemHeaderContentView(Context context) {
    super(context);
  }

  public ItemHeaderContentView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    //ActivityStarter.with(getContext(), FragmentTask.WRITE_COMMENT)
    //    .addBundle(new BundleSet.Builder().putContentCode);
  }

  @Override protected void init() {
    super.init();
    FontHelper.Bold(subject);
    minutes.setVisibility(GONE);
    thumbnail.setImageType(ImageType.POSTER).setRatioType(RatioType.EQUALS);
    thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
  }

  @Override public boolean setItem(MediaItem item) {
    super.setItem(item);

    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;
    thumbnail.setRadius(true).load(url);

    subject.setText(getMarkdownText(item.getTitle()));

    if (item.getDescription() == null) {
      minutes.setVisibility(GONE);
    } else {
      minutes.setVisibility(VISIBLE);
      setText(minutes, item.getDescription());
    }

    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_header_content;
  }
}
