package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.WPopupRateManager;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.view.PreNAveRatingView;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsUserActionView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemContentSmallView extends ItemAbsUserActionView<Content> {

  @Bind(R.id.photo) WImageView mImage;
  @Bind(R.id.more_btn) FontIconTextView mMore;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.pre_ave_rating) PreNAveRatingView mPreAveRating;
  @Bind(R.id.description) TextView mDescription;

  private String mContentTitle, mContentSubtitle;

  public ItemContentSmallView(Context context) {
    super(context);
  }

  public ItemContentSmallView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void finishRating(RatingData data) {
    mUserAction = data.getUserAction();
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_small;
  }

  @Override protected void init() {
    super.init();
    mMore.setOnClickListener(this);
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.more_btn:
        showDialog();
        break;

      default:
        ActivityStarter.with(getContext(), FragmentTask.DETAIL_CONTENT)
            .addBundle(new BundleSet.Builder().putCategoryType(CategoryType.getCategory(mCategory))
                .putContentCode(mCode)
                .putPreviousScreenName(getScreenName())
                .build()
                .getBundle())
            .start();

        break;
    }
  }

  //private boolean mIsUnreleased;

  @Override public boolean setItem(Content item) {
    if (item == null) return false;
    mCategory = item.getCategory();
    mCode = item.getCode();
    mContentTitle = item.getTitle();
    mContentSubtitle = String.valueOf(item.getYear());
    //mIsUnreleased = item.isUnreleased();

    mUserAction = CacheManager.getMyUserAction(mCode);

    String url = (item.getPoster() != null) ? item.getPoster().getMedium() : null;
    mImage.load(url);

    mTitle.setText(item.getTitle());
    mPreAveRating.setRating(item.getPreRating(WatchaApp.getUser().getCode()),
        item.getAverageRating());
    if (item.getDescription() != null) {
      mDescription.setText(getMarkdownText(item.getDescription()));
    } else {
      mDescription.setText(
          getContext().getResources().getString(R.string.review) + " " + item.getCommentsCount());
    }

    if (item.isRequiredToShowDialog()) {
      item.setRequiredToShowDialog(false);
      ListFragment listFragment = getListFragment();
      AbsPagerFragment pagerFragment = getPagerFragment();
      if (pagerFragment != null) {
        pagerFragment.showDeckSelectableBottomSheetLayout(mCode);
      } else if (listFragment != null) {
        listFragment.showDeckSelectableBottomSheetLayout(mCode);
      }
    }

    return true;
  }

  private void showDialog() {
    WPopupRateManager popupRateManager =
        new WPopupRateManager.Builder(getContext(), WPopupType.CARD_ITEM_POPULAR).setTitle(
            mContentTitle)
            .setSubtitle(mContentSubtitle)
            .setUserAction(mUserAction)
            .setUserActionPopupListener(new UserActionPopupListener() {
              @Override public void onClickAddCollection() {
                super.onClickAddCollection();

                switch (CategoryType.getCategory(mCategory)) {
                  case MOVIES:
                    ListFragment listFragment = getListFragment();
                    AbsPagerFragment pagerFragment = getPagerFragment();
                    if (pagerFragment != null) {
                      pagerFragment.showDeckSelectableBottomSheetLayout(mCode);
                    } else if (listFragment != null) {
                      listFragment.showDeckSelectableBottomSheetLayout(mCode);
                    }
                    break;

                  case TV_SEASONS:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.add_only_movie_in_deck)
                        .setPositiveButton(R.string.ok, null);

                    builder.show();
                    break;
                }
              }

              @Override public void onChangedRating(float rating) {
                super.onChangedRating(rating);
                if (rating > 0) {
                  if (mUserAction != null && rating == mUserAction.getRating()) {
                    // 같은 별점을 클릭했으면 취소.
                    requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
                  } else {
                    // 별점 매김 혹은 변경.
                    requestRating(mReleasedAt, UserActionOperator.ActionType.RATING,
                        String.valueOf(rating));
                  }
                } else {
                  // 별 0은 취소.
                  requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
                }
              }

              @Override public void onClickComment() {
                super.onClickComment();
                goWriteComment();
              }

              @Override public void onClickWish() {
                super.onClickWish();
                requestWish();
              }

              @Override public void onClickMeh() {
                super.onClickMeh();
                requestMeh();
              }
            })
            .build();

    popupRateManager.show();
  }
}
