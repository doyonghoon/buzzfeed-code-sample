package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.fragment.DetailFragment;
import com.frograms.watcha.fragment.GalleryFragment;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.Media;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Gallery;
import com.frograms.watcha.view.MediaView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import java.util.List;

public class ItemGalleryView extends ItemView<Gallery> {

  @Bind(R.id.media_layer) LinearLayout mMediaLayer;

  public ItemGalleryView(Context context) {
    super(context);
  }

  public ItemGalleryView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public boolean setItem(@NonNull Gallery gallery) {
    super.setItem(gallery);
    if (gallery.getMedias() == null) {
      return false;
    }

    if (mMediaLayer.getChildCount() > 0) {
      mMediaLayer.removeAllViews();
    }

    final List<Media> medias = gallery.getMedias();
    for (int Loop1 = 0; Loop1 < medias.size(); Loop1++) {
      MediaView view =
          new MediaView(getContext(), medias.get(Loop1), new MediaView.OnClickMediaListener() {
            @Override public void onClickMedia(Media media) {
              int currentItemPosition = medias.indexOf(media);
              BundleSet.Builder builder = new BundleSet.Builder().putMediaJsonArray(medias)
                  .putSelectedTab(currentItemPosition)
                  .putGalleryType(GalleryFragment.GalleryType.PROFILE_COVER);
              CategoryType categoryType = getCategoryType(getContext());
              if (categoryType != null) {
                switch (categoryType) {
                  case MOVIES:
                    builder.putGalleryType(GalleryFragment.GalleryType.MOVIE);
                    break;
                  case TV_SEASONS:
                    builder.putGalleryType(GalleryFragment.GalleryType.DRAMA);
                    break;
                }
              }
              ActivityStarter.with(getContext(), FragmentTask.GALLERY)
                  .addBundle(builder.build().getBundle())
                  .start();
            }
          });
      if (Loop1 == 0) {
        view.setPadding(
            getContext().getResources().getDimensionPixelSize(R.dimen.gallery_left_margin), 0, 0,
            0);
      }

      mMediaLayer.addView(view);
    }

    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_gallery;
  }

  @Nullable
  private CategoryType getCategoryType(Context context) {
    if (context != null && context instanceof BaseActivity) {
      BaseActivity activity = (BaseActivity) context;
      if (activity.getFragment() instanceof DetailFragment) {
        DetailFragment detailFragment = (DetailFragment) activity.getFragment();
        return detailFragment.getCategoryType();
      }
    }
    return null;
  }
}