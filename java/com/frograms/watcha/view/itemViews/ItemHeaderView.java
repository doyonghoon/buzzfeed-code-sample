package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.items.Header;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import me.grantland.widget.AutofitTextView;

public class ItemHeaderView extends ItemView<Header> {

  @Bind(R.id.title) AutofitTextView mTitle;

  public ItemHeaderView(Context context) {
    super(context);
  }

  @Override protected void init() {
    super.init();
    if (mTitle != null) {
      FontHelper.RobotoMedium(mTitle);
    }
  }

  public ItemHeaderView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_header;
  }

  @Override public boolean setItem(@NonNull Header item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    setText(mTitle, item.getTitle());
    return true;
  }

}
