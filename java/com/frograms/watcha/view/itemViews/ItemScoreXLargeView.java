package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Score;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import java.util.Date;

public class ItemScoreXLargeView extends ItemView<Score> {

  @Bind(R.id.photo) GreatImageView mUserPhotoView;
  @Bind(R.id.title) TextView mUserNameView;
  @Bind(R.id.description) TextView mDescView;
  @Bind(R.id.more) FontIconTextView mMoreView;

  @Bind(R.id.score) TextView mScoreView;
  @Bind(R.id.category) TextView mCategoryView;

  public ItemScoreXLargeView(Context context) {
    super(context);
  }

  public ItemScoreXLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public int getLayoutId() {
    return R.layout.view_item_score;
  }

  @Override public void init() {
    super.init();
    //FontHelper.Bold(mUserNameView);

    mMoreView.setOnClickListener(this);
  }

  public boolean setItem(@NonNull Score item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    String category =
        getResources().getString(CategoryType.getCategory(item.getCategory()).getStringId());
    if (item.getUser() != null) {
      String url =
          (item.getUser().getPhoto() == null) ? null : item.getUser().getPhoto().getSmall();
      mUserPhotoView.load(url);

      mUserNameView.setText(getMarkdownText(
          String.format("**%s**님이 %s 평가수 **%d개**를 돌파했어요.", item.getUser().getName(), category,
              item.getScore())));
      mUserPhotoView.setOnClickListener(this);
    }

    if (item.getUpdatedAt() != null) {
      mDescView.setText(Util.calTime(item.getUpdatedAt().getTime(), new Date().getTime())
          + getContext().getString(R.string.before));
    } else {
      mDescView.setText("");
    }

    mScoreView.setText(item.getScore() + "");
    mCategoryView.setText(
        (item.getDescription() == null) ? "본 " + category : item.getDescription());
    return true;
  }
}
