package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.items.ButtonCompact;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.IconActionGridButton;

public class ItemButtonCompactView extends ItemView<ButtonCompact> {

  @Bind(R.id.btn) IconActionGridButton mButton;

  private Event mEvent;

  public ItemButtonCompactView(Context context) {
    super(context);
  }

  public ItemButtonCompactView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_button_compact;
  }

  @Override protected void init() {
    super.init();

    mButton.setOnClickListener(this);
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.btn:
        if (mEvent != null) {
          ActivityStarter.with(getContext(), mEvent.getScheme())
              .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                  .build()
                  .getBundle())
              .start();
        }
        break;
    }
  }

  @Override public boolean setItem(@NonNull ButtonCompact item) {
    super.setItem(item);
    if (item == null) return false;
    if (item.getButton() != null) {
      mButton.setText(item.getButton().getText());

      if (item.getButton().getEvent() != null) {
        mEvent = item.getButton().getEvent();
      }
    }
    return true;
  }
}
