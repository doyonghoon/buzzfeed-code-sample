package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.items.TextCollapsible;
import com.frograms.watcha.utils.ViewUtil;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemTextCollapsibleView extends ItemView<TextCollapsible> {

  @Bind(R.id.description) TextView mDescription1;
  @Bind(R.id.description2) TextView mDescription2;
  @Bind(R.id.story) TextView mStory;
  @Bind(R.id.expand_story) TextView mExpandStory;

  public ItemTextCollapsibleView(Context context) {
    super(context);
  }

  public ItemTextCollapsibleView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public boolean setItem(TextCollapsible item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    FontHelper.RobotoRegular(mDescription1);
    FontHelper.RobotoRegular(mDescription2);
    FontHelper.RobotoRegular(mStory);
    FontHelper.RobotoRegular(mExpandStory);

    mDescription1.setText(item.getText1());
    mDescription2.setText(item.getText2());
    mStory.setText(item.getText3());
    mDescription1.setVisibility(TextUtils.isEmpty(item.getText1()) ? View.GONE : View.VISIBLE);
    mDescription2.setVisibility(TextUtils.isEmpty(item.getText2()) ? View.GONE : View.VISIBLE);
    mStory.setVisibility(TextUtils.isEmpty(item.getText3()) ? View.GONE : View.VISIBLE);
    mExpandStory.setOnClickListener(this);
    ViewTreeObserver vto = mStory.getViewTreeObserver();
    vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override public void onGlobalLayout() {
        if (mExpandStory != null && mStory != null) {
          mExpandStory.getViewTreeObserver().removeOnGlobalLayoutListener(this);
          mExpandStory.setVisibility(isEllipsized(mStory) ? View.VISIBLE : View.GONE);
          mStory.setPadding(0, 0, 0, (int) ViewUtil.convertDpToPixel(getContext(), 13f));
        }
      }
    });

    return true;
  }

  public int getLayoutId() {
    return R.layout.view_item_text_collapsible;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      case R.id.expand_story:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT, AnalyticsActionType.EXPAND_STORY)
            .build());
        mStory.setMaxLines(Integer.MAX_VALUE);
        mStory.setPadding(0, 0, 0, (int) ViewUtil.convertDpToPixel(getContext(), 13f));
        mExpandStory.setVisibility(View.GONE);
        break;
    }
  }

  private boolean isEllipsized(TextView v) {
    Layout layout = v.getLayout();
    if (layout != null) {
      int lines = layout.getLineCount();
      if (lines > 0) {
        int ellipsisCount = layout.getEllipsisCount(lines - 1);

        if (ellipsisCount > 0) {
          return true;
        } else if (ellipsisCount == 0) { // Gpro에서 ellipsisCount의 값이 0이 뜨는 이슈로 인해 추가적인 조건 구문을 넣어주게 됨
          return layout.getText().toString().contains("…");
        }
      }
    }
    return false;
  }
}
