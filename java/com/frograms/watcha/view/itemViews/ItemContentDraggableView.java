package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemContentDraggableView extends ItemView<Content> {

  @Bind(R.id.photo) WImageView mPoster;
  @Bind(R.id.remove_item) FontIconTextView mRemoveItem;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.description) TextView mDescription;

  private Content mItem;

  public ItemContentDraggableView(Context context) {
    super(context);
  }

  public ItemContentDraggableView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void init() {
    super.init();
    setOnClickListener(this);
  }

  @Override public boolean setItem(Content item) {
    super.setItem(item);
    mItem = item;
    setBackgroundResource(R.drawable.bg_item);
    String url = (item.getPoster() != null) ? item.getPoster().getMedium() : null;
    mPoster.load(url);

    mTitle.setText(mItem.getTitle());
    mDescription.setText(mItem.getDescription());
    mRemoveItem.setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        if (getCardItemClickListener() != null) {
          getCardItemClickListener().onClickCardItem(ItemContentDraggableView.this, mItem);
        }
      }
    });
    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_draggable;
  }
}
