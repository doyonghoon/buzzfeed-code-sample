package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

public class ItemContentSelectableView extends ItemView<Content> {

  @Bind(R.id.photo) GreatImageView mPoster;
  @Bind(R.id.check) FontIconTextView mCheck;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.description) TextView mDescription;
  @Bind(R.id.content_selectable_layout) RelativeLayout mLayout;

  private Content mContent;

  public ItemContentSelectableView(Context context) {
    super(context);
  }

  public ItemContentSelectableView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public int getLayoutId() {
    return R.layout.view_item_content_selectable;
  }

  public boolean setItem(Content item) {
    super.setItem(item);
    setBackgroundResource(R.drawable.bg_item);
    mContent = item;
    String url = (item.getPoster() != null) ? item.getPoster().getMedium() : null;
    mPoster.load(url);

    FontHelper.RobotoRegular(mTitle);
    FontHelper.RobotoRegular(mDescription);
    mTitle.setText(item.getTitle());
    mDescription.setText(item.getDescription());

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
      mLayout.setBackground(getResources().getDrawable(R.drawable.bg_item));
    } else {
      mLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_item));
    }

    setIsChecked();
    return true;
  }

  private void setIsChecked() {
    if (mContent.isAdded()) {
      mCheck.setText(R.string.icon_check_on);
      mCheck.setTextColor(getResources().getColor(R.color.pink));
    } else {
      mCheck.setText(R.string.icon_check_off);
      mCheck.setTextColor(getResources().getColor(R.color.gray));
    }
  }

  @Override public void onClick(View v) {
    if (getCardItemClickListener() != null) {
      mContent.setAdded(!mContent.isAdded());
      setIsChecked();
      getCardItemClickListener().onClickCardItem(this, mContent);
    }
  }
}
