package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.fragment.SelectRatingCategoryFragment;
import com.frograms.watcha.model.items.EvaluateCategoryItem;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.WImageView;

/**
 * 영평늘에서 카테고리 선택할 때 보이는 아이템뷰.
 *
 * @see SelectRatingCategoryFragment#onClickCardItem(ItemView, Item)
 */
public class ItemEvaluateCategoryView extends ItemView<EvaluateCategoryItem> {

  @Bind(R.id.thumbnail) WImageView thumbnail;
  @Bind(R.id.subject) TextView subject;
  @Bind(R.id.minutes) TextView minutes;

  public ItemEvaluateCategoryView(Context context) {
    this(context, null);
  }

  public ItemEvaluateCategoryView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void init() {
    super.init();
    setOnClickListener(this);
    setBackgroundResource(R.drawable.bg_item);
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    if (getCardItemClickListener() != null && mItem != null) {
      getCardItemClickListener().onClickCardItem(this, mItem);
    }
  }

  private EvaluateCategoryItem mItem = null;

  @Override public boolean setItem(EvaluateCategoryItem item) {
    super.setItem(item);
    mItem = item;
    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;
    thumbnail.load(url);

    subject.setText(getMarkdownText(item.getTitle()));
    if (item.getDescription() == null) {
      minutes.setVisibility(GONE);
    } else {
      minutes.setVisibility(VISIBLE);
      setText(minutes, item.getDescription());
    }

    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_media_small;
  }
}
