package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.RatingTabFragment;
import com.frograms.watcha.fragment.TutorialFragment;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.UserActionOperator.ActionType;
import com.frograms.watcha.helpers.WPopupRateManager;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.ResizableRatingBar;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsUserActionView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.WImageView;
import java.util.Date;

public class ItemContentRatableView extends ItemAbsUserActionView<Content> {

  @Bind(R.id.photo) WImageView mImage;
  @Bind(R.id.more_btn) FontIconTextView mMore;
  @Bind(R.id.title) TextView mTitleView;
  @Bind(R.id.description) DefTextView mDescription;
  @Bind(R.id.rating) ResizableRatingBar mRatingBar;

  private static FontIconDrawable mWishDrawable, mMehDrawable;

  private String description;

  private String mTitle;
  private String mSubtitle;
  private Date mReleasedAt;

  public ItemContentRatableView(Context context) {
    super(context);
  }

  public ItemContentRatableView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void finishRating(RatingData data) {
    setUserAction();

    if (getContext() != null) {
      if (((BaseActivity) getContext()).getFragment() instanceof RatingTabFragment) {
        ((RatingTabFragment) ((BaseActivity) getContext()).getFragment()).setRatingsCount();
      } else if (((BaseActivity) getContext()).getFragment() instanceof TutorialFragment) {
        ((TutorialFragment) ((BaseActivity) getContext()).getFragment()).setRatingsCount();
      }
    }
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_ratable;
  }

  @Override protected void init() {
    super.init();

    if (mWishDrawable == null) {
      mWishDrawable = FontIconDrawable.inflate(getContext(), R.xml.icon_wish);
      mWishDrawable.setTextColor(getResources().getColor(R.color.pink));
      mWishDrawable.setTextSize(getResources().getDimension(R.dimen.small_desc_font));
    }

    if (mMehDrawable == null) {
      mMehDrawable = FontIconDrawable.inflate(getContext(), R.xml.icon_meh);
      mMehDrawable.setTextColor(getResources().getColor(R.color.pink));
      mMehDrawable.setTextSize(getResources().getDimension(R.dimen.small_desc_font));
    }

    mMore.setOnClickListener(this);
    setOnClickListener(this);

    mRatingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
      if (fromUser) {
        if (mUserAction != null) {
          if (mUserAction.getRating() == rating) {
            rating = 0.0f;
          }
        }

        if (rating > 0) {
          requestRating(mReleasedAt, ActionType.RATING, String.valueOf(rating));
        } else {
          requestRating(mReleasedAt, ActionType.RATING_CANCEL, null);
        }
      }
    });

    mRatingBar.setOnRatingBarClickListener((ratingBar, rating) -> {
      if (mUserAction == null) return;
      if (mUserAction.getRating() == rating) {
        requestRating(null, ActionType.RATING_CANCEL, null);
      }
    });
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.more_btn:
        //showDialog();
        showDialog(mTitle, mSubtitle);
        break;
    }
  }

  //private boolean mIsUnreleased;

  @Override public boolean setItem(@NonNull Content item) {
    if (item != null) {
      item.setEvent(new Event("watcha://" + item.getCategory() + "/" + item.getCode()));
    }

    super.setItem(item);
    if (item == null) {
      return false;
    }
    setBackgroundResource(R.drawable.bg_item);
    mCategory = item.getCategory();
    mCode = item.getCode();
    mReleasedAt = item.getReleasedAt();
    mTitle = item.getTitle();
    mSubtitle = String.valueOf(item.getYear());
    //mIsUnreleased = item.isUnreleased();

    String url = (item.getPoster() != null) ? item.getPoster().getMedium() : null;
    mImage.load(url);

    mTitleView.setText(item.getTitle());
    description = item.getDescription();
    setUserAction();

    return true;
  }

  private void setUserAction() {

    mUserAction = CacheManager.getMyUserAction(mCode);

    mDescription.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    mDescription.setText(description);

    if (mUserAction == null) {
      WLog.i("UserAction is Null");
      mRatingBar.setRating(0);
      return;
    }

    if (mUserAction.isWished()) {
      mDescription.setCompoundDrawables(mWishDrawable, null, null, null);
      mDescription.setText(getContext().getString(R.string.wish));
    }

    if (mUserAction.isMehed()) {
      mDescription.setCompoundDrawables(mMehDrawable, null, null, null);
      mDescription.setText(getContext().getString(R.string.ignore));
    }

    if (mUserAction.getRating() > 0) {
      mRatingBar.setRating(mUserAction.getRating());
    } else {
      mRatingBar.setRating(0);
    }
  }

  private void showDialog(String title, String subtitle) {
    WPopupRateManager popupRateManager =
        new WPopupRateManager.Builder(getContext(), WPopupType.CARD_ITEM_RATABLE).setTitle(title)
            .setSubtitle(subtitle)
            .setUserAction(mUserAction)
            .setUserActionPopupListener(new UserActionPopupListener() {
              @Override public void onClickAddCollection() {
                super.onClickAddCollection();
                if (mCategory == null) return;

                switch (CategoryType.getCategory(mCategory)) {
                  case MOVIES:
                    ListFragment listFragment = getListFragment();
                    AbsPagerFragment pagerFragment = getPagerFragment();
                    if (pagerFragment != null) {
                      pagerFragment.showDeckSelectableBottomSheetLayout(mCode);
                    } else if (listFragment != null) {
                      listFragment.showDeckSelectableBottomSheetLayout(mCode);
                    }
                    break;

                  case TV_SEASONS:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.add_only_movie_in_deck)
                        .setPositiveButton(R.string.ok, null);

                    builder.show();
                    break;
                }
              }

              @Override public void onChangedRating(float rating) {
                super.onChangedRating(rating);
                if (rating > 0) {
                  if (mUserAction != null && rating == mUserAction.getRating()) {
                    // 같은 별점을 클릭했으면 취소.
                    requestRating(null, UserActionOperator.ActionType.RATING, null);
                  } else {
                    // 별점 매김 혹은 변경.
                    requestRating(mReleasedAt, UserActionOperator.ActionType.RATING,
                        String.valueOf(rating));
                  }
                } else {
                  // 별 0은 취소.
                  requestRating(null, UserActionOperator.ActionType.RATING, null);
                }
              }

              @Override public void onClickComment() {
                super.onClickComment();
                goWriteComment();
              }

              @Override public void onClickWish() {
                super.onClickWish();
                requestWish();
              }

              @Override public void onClickMeh() {
                super.onClickMeh();
                requestMeh();
              }
            })
            .build();

    popupRateManager.show();
  }
}
