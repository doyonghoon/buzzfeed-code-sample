package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.chart.ValueLineChart;
import com.frograms.watcha.chart.model.ValueLinePoint;
import com.frograms.watcha.chart.model.ValueLineSeries;
import com.frograms.watcha.model.items.ChartLarge;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemChartLargeView extends ItemView<ChartLarge> {

  @Bind(R.id.linechart) ValueLineChart mValueLineChart;
  @Bind(R.id.avg_rating) TextView mAvgRating;
  @Bind(R.id.total_aud) TextView mTotalAud;
  @Bind(R.id.most_rating) TextView mMostRating;

  public ItemChartLargeView(Context context) {
    super(context);
  }

  public ItemChartLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_chart_large;
  }

  @Override public boolean setItem(@NonNull ChartLarge chart) {
    super.setItem(chart);

    int[] distributions = chart.getDistributions();

    ValueLineSeries series = new ValueLineSeries();
    series.setColor(getResources().getColor(R.color.gold));
    series.addPoint(new ValueLinePoint(null, 0));
    series.addPoint(new ValueLinePoint("0.5", distributions[0]));
    series.addPoint(new ValueLinePoint("1", distributions[1]));
    series.addPoint(new ValueLinePoint("1.5", distributions[2]));
    series.addPoint(new ValueLinePoint("2", distributions[3]));
    series.addPoint(new ValueLinePoint("2.5", distributions[4]));
    series.addPoint(new ValueLinePoint("3", distributions[5]));
    series.addPoint(new ValueLinePoint("3.5", distributions[6]));
    series.addPoint(new ValueLinePoint("4", distributions[7]));
    series.addPoint(new ValueLinePoint("4.5", distributions[8]));
    series.addPoint(new ValueLinePoint("5", distributions[9]));
    series.addPoint(new ValueLinePoint(null, 0));
    mValueLineChart.addSeries(series);
    mAvgRating.setText(chart.getAverageRating() + "");
    mTotalAud.setText(chart.getRatingCount() + "");
    mMostRating.setText(chart.getMostRating());

    return true;
  }
}