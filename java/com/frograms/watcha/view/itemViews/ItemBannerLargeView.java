package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.EventOperator;
import com.frograms.watcha.model.items.BannerLarge;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemBannerLargeView extends ItemView<BannerLarge> {

  @Bind(R.id.image) WImageView mImage;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.description) TextView mDesc;
  @Bind(R.id.button) TextView mParticipate;

  public ItemBannerLargeView(Context context) {
    super(context);
  }

  public ItemBannerLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_banner_large;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      case R.id.button:
        if (mEvent != null) {
          new EventOperator.Builder(getContext(), mEvent).setPreviousScreenName(getScreenName())
              .setCardItem(this)
              .build()
              .start();
        }
        break;
    }
  }

  @Override protected void init() {
    super.init();
    mParticipate.setOnClickListener(this);
  }

  @Override public boolean setItem(@NonNull BannerLarge item) {
    super.setItem(item);
    mImage.load(item.getImage());

    setText(mTitle, item.getTitle());
    setText(mDesc, item.getDesc());

    setText(mParticipate, item.getButtonText());
    return true;
  }
}
