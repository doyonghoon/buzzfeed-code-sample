package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.PartnerHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

public class ItemUserSelectableView extends ItemView<UserBase> {

  @Bind(R.id.photo) GreatImageView mPhoto;
  @Bind(R.id.name) NameTextView mName;
  @Bind(R.id.description) TextView mDescription;
  @Bind(R.id.button) DefTextView mButton;

  private UserBase mUser = null;
  private String mUserCode;
  private boolean mIsButtonOn;

  private PartnerHelper.OnBasePartnerResponseListener createPartnerResponseListener(Context context) {
    return new PartnerHelper.OnBasePartnerResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);
        if (getContext() instanceof BaseActivity) {
          BaseActivity act = (BaseActivity) getContext();
          mIsButtonOn = !mIsButtonOn;
          WatchaApp.getUser().setPartner(mIsButtonOn ? mUser : null);
          CacheManager.putUser(mUser);

          mButton.setText(mIsButtonOn ? R.string.icon_check_on : R.string.icon_check_off);
          mButton.setTextColor(
              getResources().getColor(mIsButtonOn ? R.color.pink : R.color.light_gray));

          final String message =
              mIsButtonOn ? mUser.getName() + " 님이 파트너로 설정 되었습니다!" : "파트너가 해제되었습니다";
          SnackbarManager.show(Snackbar.with(getContext())
              .text(message)
              .position(Snackbar.SnackbarPosition.BOTTOM)
              .type(SnackbarType.SINGLE_LINE), act);
          if (act.getFragment() instanceof ListFragment) {
            ListFragment frag = (ListFragment) act.getFragment();
            frag.getAdapter().notifyDataSetChanged();
          }
        } else {
          Toast.makeText(getContext(), "파트너 설정이 안됨..", Toast.LENGTH_SHORT).show();
        }
      }
    };
  }

  public ItemUserSelectableView(Context context) {
    super(context);
  }

  public ItemUserSelectableView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public int getLayoutId() {
    return R.layout.view_item_user_selectable;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      case R.id.button:
        PartnerHelper.setMoviePartner((BaseActivity) getContext(), new ReferrerBuilder(getScreenName())
            , mUserCode, !mIsButtonOn, createPartnerResponseListener(getContext()));
        break;

      default:
        if (!TextUtils.isEmpty(mUserCode)
            && WatchaApp.getUser() != null
            && !mUserCode.equals(WatchaApp.getUser().getCode())) {
          ActivityStarter.with(getContext(), FragmentTask.PROFILE)
              .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                  .putUserCode(mUserCode)
                  .build()
                  .getBundle())
              .start();
        }
        break;
    }
  }

  @Override protected void init() {
    super.init();
    mButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimension(R.dimen.button_font));

    mButton.setOnClickListener(this);
  }

  @Override public boolean setItem(UserBase item) {
    super.setItem(item);
    setBackgroundResource(R.drawable.bg_item);
    mUserCode = item.getCode();
    mUser = CacheManager.getUser(mUserCode);
    mName.setUser(item);

    mDescription.setText(
        getResources().getString(R.string.collection) + " " + item.getDecksCount() + "/ ");
    mDescription.append(
        getResources().getString(R.string.review) + " " + item.getCommentCounts() + "/ ");
    mDescription.append(getResources().getString(R.string.already) + " " + item.getRatingCounts());

    String url = (item.getPhoto() != null) ? item.getPhoto().getSmall() : null;
    mPhoto.load(url);

    mIsButtonOn = WatchaApp.getUser().getPartner() != null && mUserCode.equals(
        WatchaApp.getUser().getPartner().getCode());

    mButton.setText(mIsButtonOn ? R.string.icon_check_on : R.string.icon_check_off);
    mButton.setTextColor(getResources().getColor(mIsButtonOn ? R.color.pink : R.color.light_gray));
    //
    //mButton.setVisibility(mIsButtonOn ? VISIBLE : GONE);
    //ViewUtil.setBackground(mButton, getResources().getDrawable(R.drawable.bg_green_circle));
    //mButton.setText(R.string.icon_check_on);

    return true;
  }
}
