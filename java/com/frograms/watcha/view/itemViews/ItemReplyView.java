package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.items.Reply;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.textviews.RichTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.Date;

public class ItemReplyView extends ItemView<Reply> {

  @Bind(R.id.reply_more) FontIconTextView mMore;

  @Bind(R.id.reply_userimage) GreatImageView mUserImage;
  @Bind(R.id.user_name) NameTextView mUserName;
  @Bind(R.id.reply) RichTextView mText;

  @Bind(R.id.reply_time) TextView mTime;
  @Bind(R.id.reply_like) TextView mLike;
  @Bind(R.id.reply_likecount) DefTextView mLikeCount;

  private String mUserCode;
  private Reply mReply;

  private boolean isMine;

  public ItemReplyView(Context context) {
    super(context);
  }

  public ItemReplyView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public int getLayoutId() {
    return R.layout.view_item_reply;
  }

  @Override protected void init() {
    super.init();

    FontHelper.RobotoBold(mUserName);
    mMore.setOnClickListener(this);
    mLike.setOnClickListener(this);
    mLikeCount.setOnClickListener(this);
    ((FontIconDrawable) mLikeCount.getCompoundDrawables()[0]).setTextColor(
        getResources().getColor(R.color.gray));
  }

  private void request(QueryType queryType) {
    ReferrerBuilder referrerBuilder = new ReferrerBuilder(getScreenName());
    DataProvider<BaseResponse<CommentReportData>> provider = new DataProvider<BaseResponse<CommentReportData>>((BaseActivity)getContext(), queryType, referrerBuilder)
        .responseTo((queryType1, result) -> {
          if (queryType1.equals(QueryType.REPLY_DELETE)) {
            if (onRemoveItemListener != null) {
              onRemoveItemListener.onRemove(getPosition());
              //update Comment
              if (getContext() instanceof BaseActivity
                  && ((BaseActivity) getContext()).getFragment() instanceof ReplyFragment) {
                ((ReplyFragment) ((BaseActivity) getContext()).getFragment()).syncReplyCount(false);
              }

            }
          } else {
            if (result.getData().isLiked() != null) {
              boolean islike = Boolean.valueOf(result.getData().isLiked());
              if (islike != mReply.isLiked()) {
                mReply.addLike(islike);
              }
              mReply.setIsLiked(islike);
              setLiked();
            }
          }
        });

    provider.request();
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.reply_userimage:
        ActivityStarter.with(getContext(), FragmentTask.PROFILE)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                .putUserCode(mUserCode)
                .build()
                .getBundle())
            .start();
        break;

      case R.id.reply_more:
        showDialog();
        //request(QueryType.REPLY_DELETE.setApi(mReply.getCode()));
        break;

      case R.id.reply_like:
        if (mReply.isLiked())
          request(QueryType.REPLY_ACTION_CANCEL.setApi(mReply.getCode(), "like"));
        else
          request(QueryType.REPLY_ACTION.setApi(mReply.getCode(), "like"));

        break;

      case R.id.reply_likecount:
        ActivityStarter.with(getContext(), "watcha://replies/" + mReply.getCode() + "/likers")
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                .build()
                .getBundle())
            .start();
      break;
    }
  }

  protected void showDialog() {
    String[] strs = getContext().getResources().getStringArray(R.array.reply_dialog_msg);

    ArrayAdapter<String> simpleAdapter =
        new ArrayAdapter<>(getContext(), R.layout.view_list_item_basic, R.id.text1, strs);
    DialogPlus dialog = new DialogPlus.Builder(getContext())
        .setContentHolder(new ListHolder())
        .setAdapter(simpleAdapter)
        .setGravity(DialogPlus.Gravity.CENTER)
        .setCancelable(true)
        .setOnItemClickListener((dialogPlus, o, view, i) -> {
          dialogPlus.dismiss();
          switch (i) {
            case 0:
              AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
              builder.setMessage(getResources().getString(R.string.want_to_delete))
                  .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override public void onClick(DialogInterface dialogInterface, int i) {
                      request(QueryType.REPLY_DELETE.setApi(mReply.getCode()));
                    }
                  })
                  .setNegativeButton(R.string.no, null);
              builder.show();
              break;
          }
        })

        .create();
    dialog.show();
  }

  @Override public boolean setItem(Reply item) {
    super.setItem(item);

    if (item == null) {
      return false;
    }

    if (item.getUser() != null) {
      mUserCode = item.getUser().getCode();
      isMine = item.getUser().getCode().equals(WatchaApp.getUser().getCode());
      mMore.setVisibility((isMine || item.canRemove()) ? VISIBLE : GONE);

      String url =
          (item.getUser().getPhoto() != null) ? item.getUser().getPhoto().getSmall() : null;
      mUserImage.load(url);

      mUserName.setUser(item.getUser());

      mUserImage.setOnClickListener(this);
    }

    mReply = item;

    mText.setText(item.getMessage());
    setLiked();

    if (item.getCreatedAt() != null) {
      mTime.setText(Util.calTime(item.getCreatedAt().getTime(), new Date().getTime())
          + getContext().getString(R.string.before));
    } else {
      mTime.setText("");
    }

    return true;
  }

  protected void setLiked() {
    mLike.setText(getContext().getString(R.string.like));
    if (mReply.isLiked()) {
      mLike.append(" " + getContext().getString(R.string.cancel));
      mLike.setTextColor(getResources().getColor(R.color.pink));
    } else {
      mLike.setTextColor(getResources().getColor(R.color.gray));
    }

    if (mReply.getLikesCount() > 0) {
      mLikeCount.setVisibility(View.VISIBLE);
      mLikeCount.setText(mReply.getLikesCount() + "");
    } else {
      mLikeCount.setVisibility(View.GONE);
    }
  }
}
