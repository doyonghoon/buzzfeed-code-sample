package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.LoadMore;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemLoadMoreView extends ItemView<LoadMore> {

  @Bind(R.id.title) TextView mTitle;

  private LoadMore mLoadMore;

  public ItemLoadMoreView(Context context) {
    super(context);
  }

  public ItemLoadMoreView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_load_more;
  }

  @Override
  public void onClick(View v) {
    super.onClick(v);

    if (getCardItemClickListener() != null) {
      getCardItemClickListener().onClickCardItem(this, mLoadMore);
    }
  }

  @Override public boolean setItem(@NonNull LoadMore item) {
    super.setItem(item);
    if (item == null || !item.isVisible()) return false;

    setOnClickListener(this);
    mLoadMore = item;
    setText(mTitle, "이전댓글 보기...");
    return true;
  }
}
