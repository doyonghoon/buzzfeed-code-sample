package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.DeckSelectable;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemDeckSelectableView extends ItemView<DeckSelectable> {

  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.image) WImageView mImage;
  @Bind(R.id.check) FontIconTextView mCheck;

  private DeckSelectable mDeckSelectable;

  public ItemDeckSelectableView(Context context) {
    super(context);
  }

  public ItemDeckSelectableView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void init() {
    super.init();
    setOnClickListener(this);
  }

  public boolean setItem(DeckSelectable item) {
    super.setItem(item);
    mDeckSelectable = item;
    setBackgroundResource(R.drawable.bg_item);
    if (item != null) {
      mTitle.setText(item.getDeckName());
      if (item.getImage() != null && !TextUtils.isEmpty(item.getImage().getSmall())) {
        mImage.load(item.getImage().getSmall());
      } else {
        mImage.setImageDrawable(null);
      }

      mCheck.setText(item.isAdded() ? R.string.icon_check_on : R.string.icon_check_off);
      mCheck.setTextColor(
          getResources().getColor(item.isAdded() ? R.color.pink : R.color.light_gray));
      return true;
    }
    return false;
  }

  public int getLayoutId() {
    return R.layout.view_item_deck_selectable;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      default:
        if (getCardItemClickListener() != null && mDeckSelectable != null) {
          getCardItemClickListener().onClickCardItem(this, mDeckSelectable);
        }
        break;
    }
  }
}
