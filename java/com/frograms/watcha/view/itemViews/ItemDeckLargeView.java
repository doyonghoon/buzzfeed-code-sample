package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.DetailDeckFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsDeckActionView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.textviews.RichTextView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import java.util.Date;

public class ItemDeckLargeView extends ItemAbsDeckActionView<DeckBase> {

  @Bind(R.id.photo) GreatImageView mUserPhotoView;
  @Bind(R.id.title) NameTextView mUserNameView;
  @Bind(R.id.description) TextView mTimeView;
  @Bind(R.id.more) FontIconTextView mMoreView;

  @Bind(R.id.deck_title) RichTextView mTitleView;

  @Bind(R.id.deck_likecount) TextView mLikeCountView;
  @Bind(R.id.deck_repliescount) TextView mRepliesCountView;
  @Bind(R.id.like_grid) IconActionGridButton mLikeView;
  @Bind(R.id.reply_grid) IconActionGridButton mReplyView;
  @Bind(R.id.share_grid) IconActionGridButton mShareView;

  private String mUserCode;
  private boolean isMine;

  public ItemDeckLargeView(Context context) {
    super(context);
  }

  public ItemDeckLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public int getLayoutId() {
    return R.layout.view_item_deck_large;
  }

  @Override protected void init() {
    super.init();
    FontHelper.Bold(mUserNameView);

    mLikeCountView.setOnClickListener(this);
    mRepliesCountView.setOnClickListener(this);
    mLikeView.setOnClickListener(this);
    mReplyView.setOnClickListener(this);
    mShareView.setOnClickListener(this);
    mLikeView.setTextNormalColor(getResources().getColor(R.color.light_gray));
    mReplyView.setTextNormalColor(getResources().getColor(R.color.light_gray));
    mShareView.setTextNormalColor(getResources().getColor(R.color.light_gray));
    mLikeView.getIcon().setTextColor(getResources().getColor(R.color.light_gray));
    mReplyView.getIcon().setTextColor(getResources().getColor(R.color.light_gray));
    mShareView.getIcon().setTextColor(getResources().getColor(R.color.light_gray));
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.photo:
        ActivityStarter.with(getContext(), FragmentTask.PROFILE)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                .putUserCode(mUserCode)
                .build()
                .getBundle())
            .start();
        break;

      case R.id.more:
        if (isMine) showDialog();
        break;

      case R.id.deck_likecount:
        if (mDeck.getLikesCount() > 0) {
          ActivityStarter.with(getContext(), "watcha://decks/" + mDeck.getCode() + "/likers")
              .start();
        }
        break;

      case R.id.deck_repliescount:
        if (mDeck.getRepliesCount() > 0) {
          goReply(false);
        }
        break;

      case R.id.like_grid:
        requestLike();
        scaleSpringAnimation(mLikeView);
        break;

      case R.id.reply_grid:
        goReply(true);
        break;

      case R.id.share_grid:
        getShareUrl();
        break;

      default:
        if (mDeck != null
            && !(((BaseActivity) getContext()).getFragment() instanceof DetailDeckFragment)) {
          ActivityStarter.with(getContext(), "watcha://decks/" + mDeck.getCode()).start();
        }
        break;
    }
  }

  @Override public boolean setItem(DeckBase item) {
    super.setItem(item);

    item = CacheManager.getDeck(item.getCode());

    if (item == null) {
      return false;
    }

    if (WatchaApp.getUser().getCode().equals(item.getUser().getCode())) {
      mMoreView.setVisibility(VISIBLE);
      mMoreView.setOnClickListener(this);
    } else {
      mMoreView.setVisibility(GONE);
    }

    mDeck = item;

    if (mDeck != null) {
      setOnClickListener(this);
      if (item.getUser() != null) {
        String url =
            (item.getUser().getPhoto() == null) ? null : item.getUser().getPhoto().getSmall();
        mUserPhotoView.load(url);

        mUserNameView.setUser(item.getUser());

        mName = item.getUser().getName();
        isMine = item.getUser().getCode().equals(WatchaApp.getUser().getCode());

        mUserCode = item.getUser().getCode();
        mUserPhotoView.setOnClickListener(this);
      }

      if (isMine) {
        mMoreView.setVisibility(VISIBLE);
        mMoreView.setOnClickListener(this);
      } else {
        mMoreView.setVisibility(GONE);
      }

      mTitleView.setText("");
      mTitleView.toBoldText(item.getName(), getResources().getColor(R.color.black));
      if (item.getDesc() != null) {
        mTitleView.append("\n" + item.getDesc());
      }

      setLiked();
      setReplies();

      if (item.getUpdatedAt() != null) {
        mTimeView.setText(Util.calTime(item.getUpdatedAt().getTime(), new Date().getTime())
            + getContext().getString(R.string.before));
      } else {
        mTimeView.setText("");
      }
    } else {
      setOnClickListener(null);
    }

    return true;
  }

  @Override protected void setLiked() {
    mLikeView.setFocused(mDeck.isLiked());
    if (mDeck.getLikesCount() > 0) {
      mLikeCountView.setVisibility(VISIBLE);
      mLikeCountView.setText(getResources().getString(R.string.like)
          + " "
          + mDeck.getLikesCount()
          + getResources().getString(R.string.rating_unit));
    } else {
      mLikeCountView.setVisibility(GONE);
    }
  }

  @Override
  protected void setReplies() {
    if (mDeck.getRepliesCount() > 0) {
      mRepliesCountView.setVisibility(VISIBLE);
      mRepliesCountView.setText(getResources().getString(R.string.reply)
          + " "
          + mDeck.getRepliesCount()
          + getResources().getString(R.string.rating_unit));
    } else {
      mRepliesCountView.setVisibility(GONE);
    }
  }
}
