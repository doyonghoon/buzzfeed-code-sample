package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.items.Header;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import me.grantland.widget.AutofitTextView;

public class ItemHeaderCenterView extends ItemView<Header> {

  @Bind(R.id.title) AutofitTextView mTitle;

  public ItemHeaderCenterView(Context context) {
    super(context);
  }

  @Override protected void init() {
    super.init();
    if (mTitle != null) {
      FontHelper.RobotoMedium(mTitle);
      mTitle.setGravity(Gravity.CENTER);
      mTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,
          getResources().getDimension(R.dimen.header_center_font));
    }
  }

  @Override protected int getBackgroundId() {
    return R.color.background;
  }

  public ItemHeaderCenterView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_header_center;
  }

  @Override public boolean setItem(@NonNull Header item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }
    setText(mTitle, item.getTitle());
    return true;
  }
}
