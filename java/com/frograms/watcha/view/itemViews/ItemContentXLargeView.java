package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.widget.GradationPosterView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemContentXLargeView extends ItemView<Content> {

  @Bind(R.id.image) WImageView mImage;
  @Bind(R.id.gradation_poster) GradationPosterView mPosterView;
  @Bind(R.id.title_and_category) TextView mTitle;
  @Bind(R.id.circle1) ImageView mCircle1;
  @Bind(R.id.circle2) ImageView mCircle2;
  @Bind(R.id.prerating_text) TextView mPreRatingText;
  @Bind(R.id.prerating) DefTextView mPreRating;

  private String mCategory, mCode;

  public ItemContentXLargeView(Context context) {
    super(context);
  }

  public ItemContentXLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_xlarge;
  }

  @Override protected void init() {
    super.init();
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      default:
        ActivityStarter.with(getContext(), FragmentTask.DETAIL_CONTENT)
            .addBundle(new BundleSet.Builder().putCategoryType(CategoryType.getCategory(mCategory))
                .putContentCode(mCode)
                .putPreviousScreenName(getScreenName())
                .build()
                .getBundle())
            .start();
        break;
    }
  }

  public void removePreRating() {
    mCircle1.setVisibility(GONE);
    mCircle2.setVisibility(GONE);
    ((View) mPreRatingText.getParent()).setVisibility(GONE);
    ((View) mPreRating.getParent()).setVisibility(GONE);
  }

  public void showPreRating() {
    mCircle1.setVisibility(VISIBLE);
    mCircle2.setVisibility(VISIBLE);
    ((View) mPreRatingText.getParent()).setVisibility(VISIBLE);
    ((View) mPreRating.getParent()).setVisibility(VISIBLE);
  }

  @Override public boolean setItem(Content item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    setBackgroundResource(R.drawable.bg_item);
    mCategory = item.getCategory();
    mCode = item.getCode();

    boolean hasCover = false, hasPhoto = false;
    if (item.getStillCut() != null && item.getStillCut().getLarge() != null) hasCover = true;
    if (item.getPoster() != null && item.getPoster().getLarge() != null) hasPhoto = true;

    if (!hasCover && hasPhoto) {
      mImage.setVisibility(INVISIBLE);
      mPosterView.setVisibility(VISIBLE);

      String url = (item.getPoster() != null) ? item.getPoster().getLarge() : null;
      mPosterView.load(url);
    } else {
      mImage.setVisibility(VISIBLE);
      mPosterView.setVisibility(GONE);

      String url = (item.getStillCut() != null) ? item.getStillCut().getLarge() : null;
      mImage.load(url);
    }

    mTitle.setText(appendTitleAndCategory(item.getTitle(),
        getContext().getString(CategoryType.getCategory(item.getCategory()).getStringId())),
        TextView.BufferType.SPANNABLE);

    float preRating = item.getPreRating(WatchaApp.getUser().getCode());
    if (preRating > 0) {
      mPreRatingText.setText(R.string.predict);
      mPreRating.setText(item.getPreRating(WatchaApp.getUser().getCode()) + "");
      showPreRating();
    } else if (item.getAverageRating() > 0) {
      mPreRatingText.setText(R.string.average);
      mPreRating.setText(item.getAverageRating() + "");
      showPreRating();
    } else {
      removePreRating();
    }

    return true;
  }

  private SpannableString appendTitleAndCategory(String title, String category) {
    SpannableString t = new SpannableString(title + " " + category);
    t.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);
    t.setSpan(new TextAppearanceSpan(getContext(), R.style.CategorySpanStyle), title.length(),
        t.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    return t;
  }
}
