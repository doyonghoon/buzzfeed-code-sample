package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.Banner;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemBannerView extends ItemView<Banner> {

  @Bind(R.id.image) WImageView mImage;
  @Bind(R.id.title) TextView mTitle;

  public ItemBannerView(Context context) {
    super(context);
  }

  public ItemBannerView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  private Event mEvent;

  @Override protected int getLayoutId() {
    return R.layout.view_item_banner;
  }

  @Override protected void init() {
    super.init();
  }

  @Override public void onClick(View v) {
    super.onClick(v);
  }

  @Override public boolean setItem(Banner item) {
    super.setItem(item);
    if (item == null) return false;

    mEvent = item.getEvent();
    mImage.load(item.getImage());
    mTitle.setText(item.getTitle());

    return true;
  }
}
