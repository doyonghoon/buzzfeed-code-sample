package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.MediaItem;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

public class ItemMediaGridView extends ItemView<MediaItem> {

  @Bind(R.id.profile) GreatImageView mProfileView;
  @Bind(R.id.title) TextView mTitleView;
  @Bind(R.id.desc) TextView mDescView;

  public ItemMediaGridView(Context context) {
    super(context);
  }

  public ItemMediaGridView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_media_grid;
  }

  @Override public boolean setItem(MediaItem item) {
    super.setItem(item);
    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;
    mProfileView.load(url);

    setText(mTitleView, item.getTitle());
    setText(mDescView, item.getDescription());

    return true;
  }
}