package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.utils.RatingUtils;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsCommentActionView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.textviews.FeedRatingTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.textviews.RichTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import java.util.Date;

public class ItemUserActionSmallView extends ItemAbsCommentActionView<UserAction> {

  @Bind(R.id.comment_more) FontIconTextView mMore;

  @Bind(R.id.comment_userimage) GreatImageView mUserImage;
  @Bind(R.id.user_name) NameTextView mUserName;
  @Bind(R.id.user_action) FeedRatingTextView mAction;
  @Bind(R.id.comment) RichTextView mText;

  @Bind(R.id.comment_winner) ImageView mWinner;

  @Bind(R.id.comment_activity) RichTextView mActivity;
  @Bind(R.id.comment_time) TextView mTime;
  @Bind(R.id.comment_like) TextView mLike;
  @Bind(R.id.comment_likecount) DefTextView mLikeCount;
  @Bind(R.id.comment_reply) TextView mReply;
  @Bind(R.id.comment_repliescount) TextView mRepliesCountView;

  @Bind(R.id.blind_layer) View mBlindLayer;

  private String mUserCode, mCode;

  private boolean isMine;

  public ItemUserActionSmallView(Context context) {
    super(context);
  }

  public ItemUserActionSmallView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public int getLayoutId() {
    return R.layout.view_item_useraction_small;
  }

  @Override protected void init() {
    super.init();

    FontHelper.RobotoBold(mUserName);
    mMore.setOnClickListener(this);
    mLike.setOnClickListener(this);
    mLikeCount.setOnClickListener(this);
    mReply.setOnClickListener(this);
    mRepliesCountView.setOnClickListener(this);
    ((FontIconDrawable) mLikeCount.getCompoundDrawables()[0]).setTextColor(
        getResources().getColor(R.color.gray));
    ((FontIconDrawable) mRepliesCountView.getCompoundDrawables()[0]).setTextColor(
        getResources().getColor(R.color.gray));
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.comment_userimage:
        ActivityStarter.with(getContext(), FragmentTask.PROFILE)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                .putUserCode(mUserCode)
                .build()
                .getBundle())
            .start();
        break;

      case R.id.comment_more:
        if (isMine) {
          showDialog();
        } else {
          showReportDialog();
        }
        break;

      case R.id.comment_like:
        requestLike();
        scaleSpringAnimation(mLike);
        break;

      case R.id.comment_likecount:
        if (mComment.getLikesCount() > 0) {
          ActivityStarter.with(getContext(), "watcha://comments/" + mCode + "/likers").start();
        }
        break;

      case R.id.comment_reply:
        goReply(true);
        break;

      case R.id.comment_repliescount:
        if (mComment != null && mComment.getRepliesCount() > 0) {
          goReply(false);
        }
        break;

      case R.id.blind_layer:
        AlertDialog.Builder builder =
            new AlertDialog.Builder(getContext()).setMessage(R.string.open_spoil_comment)
                .setPositiveButton(R.string.spoiler_open_ok, (dialog, which) -> {
                  mBlindLayer.setVisibility(View.GONE);
                })
                .setNegativeButton(R.string.report_confirm_cancel, null);

        builder.show();
        break;

      default:
        if (mComment != null) {
          ActivityStarter.with(getContext(), "watcha://comments/" + mComment.getCode()).start();
        }
        break;
    }
  }

  @Override public boolean setItem(UserAction item) {
    super.setItem(item);

    mContentCode = item.getContentCode();
    if (item.getUser().getCode().equals(WatchaApp.getUser().getCode())) {

      item = CacheManager.getMyUserAction(item.getContentCode());
      mComment = item.getComment();
    } else {

      if (item.getComment() != null) {
        mComment = CacheManager.getComment(item.getComment().getCode());
      } else {
        mComment = null;
      }
    }

    if (item == null) {
      return false;
    }

    if (item.getUser() != null) {
      mUserCode = item.getUser().getCode();
      isMine = item.getUser().getCode().equals(WatchaApp.getUser().getCode());

      String url =
          (item.getUser().getPhoto() != null) ? item.getUser().getPhoto().getSmall() : null;
      mUserImage.load(url);

      mUserName.setUser(item.getUser());

      mUserImage.setOnClickListener(this);
      mName = item.getUser().getName();
    }

    mAction.setUserAction(item);

    mText.setText("");

    if (mComment != null) {
      setOnClickListener(this);
      mCode = mComment.getCode();

      if (mComment.isWinner()) {
        mText.toBoldText(getContext().getString(R.string.watcha_premiere) + "");
        mWinner.setVisibility(VISIBLE);
      } else {
        mText.setText("");
        mWinner.setVisibility(GONE);
      }
      mText.append(mComment.getText());

      if (mComment.getWatchedAt() == null) {
        mActivity.setVisibility(View.GONE);
      } else {
        mActivity.setVisibility(View.VISIBLE);

        Date watchedAt = mComment.getWatchedAt();

        int year, month, day;

        if (watchedAt != null) {
          //String[] array = watchedAt.split("-");
          //year = Integer.valueOf(array[0]);
          //month = Integer.valueOf(array[1]);
          //day = Integer.valueOf(array[2]);
          //
          //String activity = year + getContext().getString(R.string.year) + " "
          //    + month + getContext().getString(R.string.month) + " "
          //    + day + getContext().getString(R.string.day) + " "
          //    + "에 봄";

          mActivity.setText(RatingUtils.getHumanReadableDate(watchedAt) + "에 봄");
        }
      }

      mLike.setVisibility(View.VISIBLE);
      mRepliesCountView.setVisibility(VISIBLE);
      mReply.setVisibility(VISIBLE);

      setLiked();
      setReplies();

      UserAction action = CacheManager.getMyUserAction(mContentCode);
      boolean isCheck = false;
      if (action != null && action.getRating() > 0f) isCheck = true;

      if (mComment.isBlind() && !mUserCode.equals(WatchaApp.getUser().getCode()) && !isCheck) {
        mBlindLayer.setVisibility(View.VISIBLE);
        mBlindLayer.setOnClickListener(this);
      } else {
        mBlindLayer.setVisibility(View.GONE);
      }
    } else {
      setOnClickListener(null);
      mBlindLayer.setVisibility(View.GONE);

      mActivity.setVisibility(GONE);
      mLike.setVisibility(View.GONE);
      mLikeCount.setVisibility(View.GONE);
      mMore.setVisibility(GONE);
      mRepliesCountView.setVisibility(GONE);
      mReply.setVisibility(GONE);

      if (item.getRating() > 0) {
        mText.setText(getContext().getResources().getStringArray(R.array.rating_text)[Math.max(
            (int) item.getRating() - 1, 0)]);
      } else if (item.isWished()) {
        mText.setText(getContext().getString(R.string.wish));
      } else if (item.isMehed()) {
        mText.setText(getContext().getString(R.string.ignore));
      }
    }

    if (item.getUpdatedAt() != null) {
      mTime.setText(Util.calTime(item.getUpdatedAt().getTime(), new Date().getTime())
          + getContext().getString(R.string.before));
    } else {
      mTime.setText("");
    }

    return true;
  }

  @Override protected void setLiked() {
    mLike.setText(getContext().getString(R.string.like));
    if (mComment.isLiked()) {
      mLike.append(" " + getContext().getString(R.string.cancel));
      mLike.setTextColor(getResources().getColor(R.color.pink));
    } else {
      mLike.setTextColor(getResources().getColor(R.color.gray));
    }

    if (mComment.getLikesCount() > 0) {
      mLikeCount.setVisibility(View.VISIBLE);
      mLikeCount.setText(mComment.getLikesCount() + "");
    } else {
      mLikeCount.setVisibility(View.GONE);
    }
  }

  @Override
  protected void setReplies() {
    if (mComment.getRepliesCount() > 0) {
      mRepliesCountView.setVisibility(VISIBLE);
      mRepliesCountView.setText(mComment.getRepliesCount() + "");
    } else {
      mRepliesCountView.setVisibility(GONE);
    }
  }
}
