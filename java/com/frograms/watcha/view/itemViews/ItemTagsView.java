package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.response.data.TagItems;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.RowLayout;
import com.frograms.watcha.view.widget.TagTextView;
import java.util.ArrayList;
import java.util.List;

/**
 * 낚시 태그들..ㅉㅉ
 */
public class ItemTagsView extends ItemView<TagItems> {

  @Bind(R.id.tag_layout) RowLayout mRowLayout;
  private TagItems mTagItem;
  private List<TagItems.Tag> mTags = new ArrayList<>();

  public ItemTagsView(Context context) {
    super(context);
  }

  public ItemTagsView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public boolean setItem(TagItems item) {
    super.setItem(item);
    mTagItem = item;
    if (item != null && item.getTags() != null) {
      mTags = item.getTags();
    }
    if (mRowLayout.getChildCount() > 0) {
      mRowLayout.removeAllViews();
    }
    for (TagItems.Tag t : mTags) {
      mRowLayout.addView(createTagTextView(t));
    }
    return true;
  }

  private TagTextView createTagTextView(final TagItems.Tag tag) {
    TagTextView v = new TagTextView(getContext(), tag);
    v.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (getCardItemClickListener() != null) {
          setTag(tag.getId());
          getCardItemClickListener().onClickCardItem(ItemTagsView.this, mTagItem);
        }
      }
    });
    return v;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_tags;
  }
}
