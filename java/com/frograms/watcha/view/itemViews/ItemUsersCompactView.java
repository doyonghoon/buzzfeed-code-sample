package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.items.UsersCompact;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.ImageType;
import com.frograms.watcha.view.widget.wImages.WImageView;
import java.util.List;

public class ItemUsersCompactView extends ItemView<UsersCompact> {

  @Bind(R.id.header_friends_layer) LinearLayout mFriendsLayer;

  public ItemUsersCompactView(Context context) {
    super(context);
  }

  public ItemUsersCompactView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_diagram_friend_action;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    if (v.getTag(R.id.TAG_ID) != null) {
      String id = (String) v.getTag(R.id.TAG_ID);

      ActivityStarter.with(getContext(), FragmentTask.PROFILE)
          .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
              .putUserCode(id)
              .build()
              .getBundle())
          .start();
    }
  }

  @Override public boolean setItem(UsersCompact item) {
    super.setItem(item);

    if (item.getFriends() != null) {

      List<UserBase> friends = item.getFriends();
      if (friends.size() == 0) {
        mFriendsLayer.setVisibility(View.GONE);
      } else {
        mFriendsLayer.setVisibility(View.VISIBLE);
        mFriendsLayer.removeAllViews();

        for (int Loop1 = 0; Loop1 < Math.min(6, friends.size()); Loop1++) {
          WImageView imageView;

          if (mFriendsLayer.getChildCount() > Loop1) {
            imageView = (WImageView) mFriendsLayer.getChildAt(Loop1);
          } else {
            imageView =
                new WImageView(getContext()).setImageType(ImageType.PROFILE).setCircle(true);
            int padding =
                getContext().getResources().getDimensionPixelSize(R.dimen.diagram_image_margin);
            imageView.setPadding(padding, padding, padding, padding);
            mFriendsLayer.addView(imageView);

            int size =
                getContext().getResources().getDimensionPixelSize(R.dimen.diagram_image_width)
                    + getContext().getResources()
                    .getDimensionPixelSize(R.dimen.diagram_image_margin);
            imageView.getLayoutParams().width = size;
            imageView.getLayoutParams().height = size;
          }

          imageView.setTag(R.id.TAG_ID, friends.get(Loop1).getCode());
          String url =
              (friends.get(Loop1).getPhoto() != null) ? friends.get(Loop1).getPhoto().getSmall()
                  : null;
          imageView.load(url);

          imageView.setOnClickListener(this);
        }

        if (item.getFriendsCount() > 6) {

          if (mFriendsLayer.getChildCount() > 6) {
            //imageView = (WImageView) mFriendsLayer.getChildAt(7);
          } else {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.layer_profile, null);
            mFriendsLayer.addView(view);
            ((LinearLayout.LayoutParams) view.getLayoutParams()).gravity = Gravity.CENTER_VERTICAL;

            //imageView.setBackgroundResource(R.drawable.bg_placeholder_circle);
            //imageView.setText(R.string.icon_arrow_push);
            //imageView.setTextColor(getResources().getColor(R.color.white));
            //imageView.setGravity(Gravity.CENTER);
            //
            //int padding = getContext().getResources().getDimensionPixelSize(R.dimen.diagram_image_margin);
            //imageView.setPadding(padding, padding, padding, padding);
            //mFriendsLayer.addView(imageView);
            //
            //int size = getContext().getResources().getDimensionPixelSize(R.dimen.diagram_image_width) + getContext().getResources()
            //    .getDimensionPixelSize(R.dimen.diagram_image_margin);
            //imageView.getLayoutParams().width = size;
            //imageView.getLayoutParams().height = size;
          }
        }
      }
    } else {
      mFriendsLayer.setVisibility(View.GONE);
    }

    return true;
  }
}