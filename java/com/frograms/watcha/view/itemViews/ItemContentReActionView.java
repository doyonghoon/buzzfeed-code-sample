package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.WPopupRateManager;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.items.Reaction;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsUserActionView;
import com.frograms.watcha.view.widget.IconActionGridButton;

public class ItemContentReActionView extends ItemAbsUserActionView<Reaction> {

  @Bind(R.id.wish_grid) IconActionGridButton mWishButton;
  @Bind(R.id.rate_grid) IconActionGridButton mRateButton;
  @Bind(R.id.comment_grid) IconActionGridButton mCommentButton;

  //private boolean mIsUnreleased;

  public ItemContentReActionView(Context context) {
    super(context);
  }

  public ItemContentReActionView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void finishRating(RatingData data) {
    mUserAction = data.getUserAction();
    setUserAction();
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_reaction;
  }

  @Override protected void init() {
    super.init();
    mWishButton.setOnClickListener(this);
    mRateButton.setOnClickListener(this);
    mCommentButton.setOnClickListener(this);
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.wish_grid:
        requestWish();
        break;

      case R.id.rate_grid:
        showDialog();
        break;

      case R.id.comment_grid:
        goWriteComment();
        break;
    }
  }

  @Override public boolean setItem(Reaction item) {
    super.setItem(item);
    if (item == null) return false;
    if (item.getUserAction() == null) return false;
    //mIsUnreleased = item.isUnreleased();
    mCategory = item.getCategory();
    mCode = item.getUserAction().getContentCode();
    mReleasedAt = item.getReleasedAt();
    mUserAction = CacheManager.getMyUserAction(mCode);
    setUserAction();
    return true;
  }

  protected void setUserAction() {
    mWishButton.setFocused((mUserAction.isWished()) ? true : false);
    if (mUserAction.getRating() > 0) {
      mRateButton.setFocused(true);
      mRateButton.setText(mUserAction.getRating() + "줌");
    } else {
      mRateButton.setFocused(false);
      mRateButton.setText(getResources().getString(R.string.rating));
    }
    mCommentButton.setFocused((mUserAction.getComment() != null) ? true : false);
  }

  private void showDialog() {
    if (mUserAction != null) {
      WPopupRateManager popupRateManager =
          new WPopupRateManager.Builder(getContext(), WPopupType.DETAIL_RATING).setTitle(
              mUserAction.getContentTitle())
              .setSubtitle(null)
              .setUserAction(mUserAction)
              .setUserActionPopupListener(new UserActionPopupListener() {
                @Override public void onChangedRating(float rating) {
                  super.onChangedRating(rating);
                  if (rating > 0) {
                    if (mUserAction != null && rating == mUserAction.getRating()) {
                      // 같은 별점을 클릭했으면 취소.
                      requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
                    } else {
                      // 별점 매김 혹은 변경.
                      requestRating(mReleasedAt, UserActionOperator.ActionType.RATING,
                          String.valueOf(rating));
                    }
                  } else {
                    // 별 0은 취소.
                    requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
                  }
                }
              })
              .build();

      popupRateManager.show();
    }
  }
}
