package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.UserContentFragment;
import com.frograms.watcha.fragment.UserContentTabFragment;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.enums.UserActionType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.utils.WLog;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.CustomTypefaceSpan;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

public class ItemContentGridView extends ItemView<Content> {

  @Bind(R.id.photo) GreatImageView mImage;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.desc) TextView mDesc;

  private UserActionType mUserActionType;
  private String mContentRating;

  private String mCategory, mCode;

  private String mUserCode;

  private static SpannableStringBuilder mStar;

  public ItemContentGridView(Context context) {
    super(context);
  }

  public ItemContentGridView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_grid;
  }

  public SpannableStringBuilder getStar() {
    if (mStar == null) {
      mStar = new SpannableStringBuilder(getResources().getString(R.string.icon_rate_action));
      mStar.setSpan(new CustomTypefaceSpan("",
              Typeface.createFromAsset(WatchaApp.getInstance().getAssets(), "icon.ttf")), 0,
          mStar.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
      mStar.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.pink)), 0,
          mStar.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    return mStar;
  }

  @Override protected void init() {
    super.init();
    setOnClickListener(this);

    if (((BaseActivity) getContext()).getFragment() instanceof UserContentTabFragment) {
      mUserActionType =
          ((UserContentTabFragment) ((BaseActivity) getContext()).getFragment()).getUserActionType();
      mUserCode =
          ((UserContentTabFragment) ((BaseActivity) getContext()).getFragment()).getUserCode();
    }

    if (((BaseActivity) getContext()).getFragment() instanceof UserContentFragment) {
      mUserActionType =
          ((UserContentFragment) ((BaseActivity) getContext()).getFragment()).getUserActionType();
      mContentRating =
          ((UserContentFragment) ((BaseActivity) getContext()).getFragment()).getContentRating();
      mUserCode = ((UserContentFragment) ((BaseActivity) getContext()).getFragment()).getUserCode();
    }
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      default:
        ActivityStarter.with(getContext(), FragmentTask.DETAIL_CONTENT)
            .addBundle(new BundleSet.Builder().putCategoryType(CategoryType.getCategory(mCategory))
                .putContentCode(mCode)
                .putPreviousScreenName(getScreenName())
                .build()
                .getBundle())
            .start();
        break;
    }
  }

  @Override public boolean setItem(@NonNull Content item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }
    setBackgroundResource(R.drawable.bg_item);
    mCategory = item.getCategory();
    mCode = item.getCode();

    String url = (item.getPoster() == null) ? null : item.getPoster().getLarge();
    mImage.load(url);
    mTitle.setText(item.getTitle());

    mDesc.setVisibility(VISIBLE);

    UserAction action = CacheManager.getMyUserAction(mCode);

    if (mUserCode != null && mUserCode.equals(WatchaApp.getUser().getCode()) && action != null) {
      if (mUserActionType != null) {
        switch (mUserActionType) {
          case WISH:
            if (!action.isWished()) return false;
            break;

          case RATE:
            if (mContentRating != null) {
              WLog.e(mContentRating);
              float rating = Float.valueOf(mContentRating);
              if (action.getRating() < rating || action.getRating() > rating) {
                return false;
              }
            }
            break;

          case MEH:
            if (!action.isMehed()) return false;
            break;
        }
      }
    }

    if (item.getUserAction(WatchaApp.getUser().getCode()) != null &&
        action != null &&
        action.getRating() > 0f) {

      mDesc.setText(getStar());
      mDesc.setText(action.getRating() + " 줌");
    } else if (item.getPreRating(WatchaApp.getUser().getCode()) > 0) {

      mDesc.setText(R.string.predict);
      mDesc.append(" ");
      mDesc.append(getStar());
      mDesc.append(" " + item.getPreRating(WatchaApp.getUser().getCode()) + "");
    } else if (item.getAverageRating() > 0) {
      mDesc.setText(R.string.average);
      mDesc.append(" ");
      mDesc.append(getStar());
      mDesc.append(" " + item.getAverageRating() + "");
    } else {
      mDesc.setText("");
    }

    return true;
  }
}
