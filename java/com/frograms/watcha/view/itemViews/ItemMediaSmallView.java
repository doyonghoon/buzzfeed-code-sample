package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.MediaItem;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemMediaSmallView extends ItemView<MediaItem> {

  @Bind(R.id.thumbnail) WImageView thumbnail;
  @Bind(R.id.subject) TextView subject;
  @Bind(R.id.minutes) TextView minutes;

  private boolean inSelectContent = false;

  public ItemMediaSmallView(Context context) {
    super(context);
  }

  public ItemMediaSmallView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    //ActivityStarter.with(getContext(), FragmentTask.WRITE_COMMENT)
    //    .addBundle(new BundleSet.Builder().putContentCode);
  }

  //@Override
  //protected void init() {
  //  super.init();
  //  if (((BaseActivity)getContext()).getFragment() instanceof SelectContentPagerFragment) {
  //    inSelectContent = true;
  //  }
  //}

  @Override public boolean setItem(MediaItem item) {
    super.setItem(item);

    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;
    thumbnail.load(url);

    subject.setText(getMarkdownText(item.getTitle()));
    if (item.getDescription() == null) {
      minutes.setVisibility(GONE);
    } else {
      minutes.setVisibility(VISIBLE);
      setText(minutes, item.getDescription());
    }

    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_media_small;
  }
}
