package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.TextEdgedAligned;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemTextEdgeAlignedView extends ItemView<TextEdgedAligned> {

  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.description) TextView mDescription;

  public ItemTextEdgeAlignedView(Context context) {
    super(context);
  }

  public ItemTextEdgeAlignedView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_text_edge_aligned_office;
  }

  @Override public boolean setItem(@NonNull TextEdgedAligned item) {
    super.setItem(item);
    if (item == null) return false;

    setText(mTitle, item.getText1());
    setText(mDescription, item.getText2());
    return true;
  }
}
