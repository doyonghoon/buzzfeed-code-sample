package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.EventOperator;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.model.items.MediaItem;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

/**
 * {@link com.frograms.watcha.fragment.NotificationFragment}에 보이는 리스트의 카드 아이템뷰.
 */
public class ItemMediaView extends ItemView<MediaItem> {

  @Bind(R.id.thumbnail) GreatImageView thumbnail;
  @Bind(R.id.subject) TextView subject;
  @Bind(R.id.minutes) TextView minutes;

  private Event mCardEvent, mThumbnailEvent;

  public ItemMediaView(Context context) {
    super(context);
  }

  public ItemMediaView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void onClick(View v) {
    if (mCardEvent != null) {
      new EventOperator.Builder(getContext(), mCardEvent).setPreviousScreenName(getScreenName())
          .setCardItem(this)
          .build()
          .start();
    }
  }

  @Override protected void init() {
    super.init();

    setOnClickListener(this);
  }

  @Override public boolean setItem(@NonNull MediaItem item) {
    super.setItem(item);
    if (item == null) return false;

    if (item.getTitle() == null) return false;

    mCardEvent = item.getEvent();

    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;
    thumbnail.load(url);

    if (item.getThumbnail() != null) {
      mThumbnailEvent = item.getThumbnail().getEvent();
      thumbnail.setOnClickListener(v -> {
        if (mThumbnailEvent != null) {
          new EventOperator.Builder(getContext(), mThumbnailEvent).setCardItem(ItemMediaView.this)
              .setPreviousScreenName(getScreenName())
              .build()
              .start();
        }
      });
    }

    subject.setText(getMarkdownText(item.getTitle()));
    setText(minutes, item.getDescription());

    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_media;
  }
}
