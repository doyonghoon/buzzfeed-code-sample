package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.EventOperator;
import com.frograms.watcha.model.Button;
import com.frograms.watcha.model.items.HeaderButton;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemHeaderButtonView extends ItemView<HeaderButton> {

  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.button) TextView mButton;

  private Button mItem = null;

  public ItemHeaderButtonView(Context context) {
    super(context);
  }

  @Override protected void init() {
    super.init();
    setOnClickListener(this);
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    if (mItem != null && mItem.getEvent() != null) {
      new EventOperator.Builder(getContext(), mItem.getEvent()).setPreviousScreenName(getScreenName())
          .setCardItem(this)
          .build()
          .start();
    }
  }

  @Override public boolean setItem(HeaderButton item) {
    super.setItem(item);
    mTitle.setText(item.getTitle());
    mItem = item.getButton();
    if (mItem != null) {
      mButton.setText(mItem.getText());
    }
    setBackgroundResource(R.drawable.bg_item);
    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_header_button;
  }
}
