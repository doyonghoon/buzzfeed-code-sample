package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.PartnerHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsDeckActionView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.textviews.RichTextView;
import com.frograms.watcha.view.widget.ActionGridLayout;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.frograms.watcha.view.widget.wImages.ImageType;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.frograms.watcha.view.widget.wImages.WImageView;
import java.util.Date;
import java.util.List;

public class ItemDeckXLargeView extends ItemAbsDeckActionView<DeckBase> {

  @Bind(R.id.photo) GreatImageView mUserPhotoView;
  @Bind(R.id.title) NameTextView mUserNameView;
  @Bind(R.id.follow) TextView mFollowView;
  @Bind(R.id.description) TextView mTimeView;
  @Bind(R.id.more) FontIconTextView mMoreView;

  @Bind(R.id.deck_title) RichTextView mTitleView;

  @Bind(R.id.deck_container) ActionGridLayout mContentContainer;
  @Bind(R.id.label) TextView mLabelView;

  @Bind(R.id.deck_likecount) TextView mLikeCountView;
  @Bind(R.id.deck_repliescount) TextView mRepliesCountView;
  @Bind(R.id.like_grid) IconActionGridButton mLikeView;
  @Bind(R.id.reply_grid) IconActionGridButton mReplyView;
  @Bind(R.id.share_grid) IconActionGridButton mShareView;

  private List<Content> mContents;

  private String mUserCode;
  private boolean isMine;

  //private PartnerHelper.OnBaseFollowResponseListener onFollowListener = null;

  private PartnerHelper.OnBaseFollowResponseListener createFollowListener(Context context) {
    return new PartnerHelper.OnBaseFollowResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);

        UserBase user = CacheManager.getUser(mUserCode);
        user.setFriend(true);
        CacheManager.putUser(user);

        mFollowView.setVisibility(GONE);
      }
    };
  }

  public ItemDeckXLargeView(Context context) {
    super(context);
  }

  public ItemDeckXLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public int getLayoutId() {
    return R.layout.view_item_deck_large;
  }

  @Override protected void init() {
    super.init();

    ((View) mContentContainer.getParent()).setVisibility(VISIBLE);
    FontHelper.Bold(mUserNameView);
    mFollowView.setOnClickListener(this);

    mLikeCountView.setOnClickListener(this);
    mRepliesCountView.setOnClickListener(this);
    mLikeView.setOnClickListener(this);
    mReplyView.setOnClickListener(this);
    mShareView.setOnClickListener(this);
    mLikeView.setTextNormalColor(getResources().getColor(R.color.light_gray));
    mLikeView.setIconColor(getResources().getColor(R.color.light_gray));
    mShareView.setTextNormalColor(getResources().getColor(R.color.light_gray));
    mShareView.setIconColor(getResources().getColor(R.color.light_gray));
    mReplyView.setIconColor(getResources().getColor(R.color.light_gray));
    mReplyView.setTextNormalColor(getResources().getColor(R.color.light_gray));
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.photo:
        ActivityStarter.with(getContext(), FragmentTask.PROFILE)
            .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                .putUserCode(mUserCode)
                .build()
                .getBundle())
            .start();
        break;

      case R.id.follow:
        AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD, AnalyticsActionType.FOLLOW)
            .setLabel(getCardItemName())
            .build());
        PartnerHelper.setFollow((BaseActivity) getContext()
            , new ReferrerBuilder(getScreenName()).appendArgument(mUserCode)
            , mUserCode
            , true
            , createFollowListener(getContext()));
        break;

      case R.id.more:
        if (isMine) showDialog();
        break;

      case R.id.deck_likecount:
        if (mDeck != null && mDeck.getLikesCount() > 0) {
          ActivityStarter.with(getContext(), "watcha://decks/" + mDeck.getCode() + "/likers")
              .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                  .build()
                  .getBundle())
              .start();
        }
        break;

      case R.id.deck_repliescount:
        if (mDeck.getRepliesCount() > 0) {
          goReply(false);
        }
        break;

      case R.id.like_grid:
        requestLike();
        scaleSpringAnimation(mLikeView);
        break;

      case R.id.reply_grid:
        goReply(true);
        break;

      case R.id.share_grid:
        getShareUrl();
        break;

      default:
        if (mDeck != null) {
          ActivityStarter.with(getContext(), "watcha://decks/" + mDeck.getCode())
              .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                  .build()
                  .getBundle())
              .start();
        }
        break;
    }
  }

  @Override public boolean setItem(DeckBase item) {
    //super.setItem(item);

    item = CacheManager.getDeck(item.getCode());

    if (item == null) {
      return false;
    }

    mContents = item.getContents();

    if (WatchaApp.getUser() != null && item.getUser() != null && WatchaApp.getUser()
        .getCode()
        .equals(item.getUser().getCode())) {
      mMoreView.setVisibility(VISIBLE);
      mMoreView.setOnClickListener(this);
    } else {
      mMoreView.setVisibility(GONE);
    }

    if (mContents != null && !mContents.isEmpty()) {
      ((View) mContentContainer.getParent()).setVisibility(VISIBLE);
      int size = (mContents.size() > 3) ? 3 : mContents.size();
      for (int Loop1 = 0; Loop1 < size; Loop1++) {
        WImageView imageView;

        if (mContentContainer.getChildCount() > Loop1) {
          imageView = (WImageView) mContentContainer.getChildAt(Loop1);
        } else {
          imageView = new WImageView(getContext());
          imageView.setImageType(ImageType.POSTER).setRatioType(RatioType.NONE);
          imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

          mContentContainer.addView(imageView);
        }

        Content content = mContents.get(Loop1);
        if (size > 1) {
          String url = (content.getPoster() == null) ? null : content.getPoster().getXLarge();
          imageView.load(url);
        } else {
          String url = (content.getStillCut() == null) ? null : content.getStillCut().getLarge();
          imageView.load(url);
        }
      }

      if (size < mContentContainer.getChildCount()) {
        mContentContainer.removeViewsInLayout(size, mContentContainer.getChildCount() - size);
      }

      mLabelView.setText(item.getContentCount() + " 개");
    } else {
      ((View) mContentContainer.getParent()).setVisibility(GONE);
    }

    mDeck = item;

    if (mDeck != null) {
      setOnClickListener(this);
      if (item.getUser() != null) {

        String url =
            (item.getUser().getPhoto() == null) ? null : item.getUser().getPhoto().getSmall();
        mUserPhotoView.load(url);

        mUserNameView.setUser(item.getUser());

        mName = item.getUser().getName();
        isMine = item.getUser().getCode().equals(WatchaApp.getUser().getCode());

        mUserCode = item.getUser().getCode();
        mUserPhotoView.setOnClickListener(this);

        if (!WatchaApp.getUser().getCode().equals(mUserCode)) {
          UserBase user = CacheManager.getUser(mUserCode);
          if (user != null) item.getUser().setFriend(CacheManager.getUser(mUserCode).isFriend());

          mFollowView.setVisibility((item.getUser().isFriend()) ? GONE : VISIBLE);
        } else {
          mFollowView.setVisibility(GONE);
        }
      }

      mTitleView.setText("");
      mTitleView.toBoldText(item.getName(), getResources().getColor(R.color.black));
      if (item.getDesc() != null) {
        mTitleView.append("\n" + item.getDesc());
      }

      setLiked();
      setReplies();

      if (item.getUpdatedAt() != null) {
        mTimeView.setText(Util.calTime(item.getUpdatedAt().getTime(), new Date().getTime())
            + getContext().getString(R.string.before));
      } else {
        mTimeView.setText("");
      }
    } else {
      setOnClickListener(null);
    }

    return true;
  }

  @Override protected void setLiked() {
    mLikeView.setFocused(mDeck.isLiked());
    if (mDeck.getLikesCount() > 0) {
      mLikeCountView.setVisibility(VISIBLE);
      mLikeCountView.setText(getResources().getString(R.string.like)
          + " "
          + mDeck.getLikesCount()
          + getResources().getString(R.string.rating_unit));
    } else {
      mLikeCountView.setVisibility(GONE);
    }
  }

  @Override
  protected void setReplies() {
    if (mDeck.getRepliesCount() > 0) {
      mRepliesCountView.setVisibility(VISIBLE);
      mRepliesCountView.setText(getResources().getString(R.string.reply)
          + " "
          + mDeck.getRepliesCount()
          + getResources().getString(R.string.rating_unit));
    } else {
      mRepliesCountView.setVisibility(GONE);
    }
  }
}
