package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.items.Promotion;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

public class ItemPromotionXLargeView extends ItemView<Promotion> {

  @Bind(R.id.photo) GreatImageView mUserPhotoView;
  @Bind(R.id.title) TextView mUserNameView;
  @Bind(R.id.description) TextView mDescView;
  @Bind(R.id.more) FontIconTextView mMoreView;

  @Bind(R.id.banner_large) ItemBannerLargeView mBannerLargeView;

  public ItemPromotionXLargeView(Context context) {
    super(context);
  }

  public ItemPromotionXLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public int getLayoutId() {
    return R.layout.view_item_promotion_xlarge;
  }

  @Override public void init() {
    super.init();
    FontHelper.Bold(mUserNameView);

    mDescView.setVisibility(GONE);
    mMoreView.setVisibility(GONE);
    mUserPhotoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
  }

  @Override public boolean setItem(@NonNull Promotion item) {
    super.setItem(item);

    String url = (item.getImage() == null) ? null : item.getImage();
    mUserPhotoView.load(url);

    mUserNameView.setText(item.getTitle());

    mBannerLargeView.setItem(item.getBanner());

    return true;
  }
}
