package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.Header;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;

public class ItemHeaderBoldView extends ItemView<Header> {

  @Bind(R.id.title) TextView mTitle;

  public ItemHeaderBoldView(Context context) {
    super(context);
  }

  public ItemHeaderBoldView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_header_bold;
  }

  @Override public boolean setItem(@NonNull Header item) {
    super.setItem(item);
    if (item == null) return false;

    setText(mTitle, item.getTitle());
    return true;
  }
}
