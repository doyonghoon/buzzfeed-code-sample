package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.ListFragment;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.WPopupRateManager;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.ContentLarge;
import com.frograms.watcha.model.items.UserBase;
import com.frograms.watcha.model.response.data.RatingData;
import com.frograms.watcha.view.PreNAveRatingView;
import com.frograms.watcha.view.itemViews.abstracts.ItemAbsUserActionView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import java.util.Date;
import org.apache.commons.lang3.math.NumberUtils;

public class ItemContentLargeView extends ItemAbsUserActionView<ContentLarge> {

  @Bind(R.id.more_btn) FontIconTextView mMore;
  @Bind(R.id.photo) GreatImageView mImage;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.label) TextView mLabel;
  @Bind(R.id.pre_ave_rating) PreNAveRatingView mPreAveRating;
  @Bind(R.id.description) TextView mDescription;

  @Bind(R.id.friend_image) GreatImageView mFriendImageView;
  @Bind(R.id.friend_description) TextView mFriendDescription;
  @Bind(R.id.friend_layer) LinearLayout mFriendLayout;

  private String mContentTitle, mContentSubtitle;
  private Date mReleasedAt;

  public ItemContentLargeView(Context context) {
    super(context);
  }

  public ItemContentLargeView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void finishRating(RatingData data) {
    mUserAction = data.getUserAction();
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_content_large;
  }

  @Override protected void init() {
    super.init();
    mFriendLayout.setOnClickListener(this);
    mMore.setOnClickListener(this);
    setOnClickListener(this);
  }

  @Override public void onClick(View v) {
    super.onClick(v);

    switch (v.getId()) {
      case R.id.more_btn:
        showDialog();
        break;
      case R.id.friend_layer:
        ActivityStarter.with(getContext(), FragmentTask.GENERAL_CARDS)
            .addBundle(new BundleSet.Builder().putSchemeApi(
                String.format("/%s/%s/ratings/friends", mCategory, mCode)).build().getBundle())
            .start();
        break;
      default:
        ActivityStarter.with(getContext(), FragmentTask.DETAIL_CONTENT)
            .addBundle(new BundleSet.Builder().putCategoryType(CategoryType.getCategory(mCategory))
                .putContentCode(mCode)
                .putPreviousScreenName(getScreenName())
                .build()
                .getBundle())
            .start();
        break;
    }
  }

  //private Content mContent = null;

  @Override public boolean setItem(ContentLarge item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }
    setBackgroundResource(R.drawable.bg_item);
    mCategory = item.getCategory();
    mCode = item.getCode();
    mContentTitle = item.getTitle();
    mReleasedAt = item.getReleasedAt();
    mContentSubtitle = String.valueOf(item.getYear());

    mUserAction = CacheManager.getMyUserAction(mCode);

    String url = (item.getPoster() != null) ? item.getPoster().getMedium() : null;
    mImage.load(url);

    mTitle.setText(item.getTitle());
    mPreAveRating.setRating(item.getPreRating(WatchaApp.getUser().getCode()),
        item.getAverageRating());
    mDescription.setText(item.getDescription());

    if (item.getFriends() == null || item.getFriends().size() == 0) {
      mFriendImageView.setVisibility(View.GONE);
      mFriendDescription.setVisibility(View.GONE);
    } else {
      mFriendImageView.setVisibility(View.VISIBLE);
      mFriendDescription.setVisibility(View.VISIBLE);

      UserBase friend = item.getFriends().get(0);

      String friendUrl = (friend.getPhoto() != null) ? friend.getPhoto().getSmall() : null;
      mFriendImageView.load(friendUrl);
      mFriendDescription.setText(friend.getName() + "님" + ((item.getFriendsCount() == 1) ? "이"
          : " 외 " + (item.getFriendsCount() - 1) + "명이") + " 봤습니다");
    }

    if (item.isRequiredToShowDialog()) {
      item.setRequiredToShowDialog(false);
      ListFragment listFragment = getListFragment();
      AbsPagerFragment pagerFragment = getPagerFragment();
      if (pagerFragment != null) {
        pagerFragment.showDeckSelectableBottomSheetLayout(mCode);
      } else if (listFragment != null) {
        listFragment.showDeckSelectableBottomSheetLayout(mCode);
      }
    }

    if (!TextUtils.isEmpty(item.getLabel()) && NumberUtils.isNumber(item.getLabel())) {
      int ranking = NumberUtils.toInt(item.getLabel());
      mLabel.setText(item.getLabel() + "");
      if (ranking == 1) {
        mLabel.setBackgroundColor(getResources().getColor(R.color.gold));
      } else {
        mLabel.setBackgroundColor(getResources().getColor(R.color.black_alpha_80));
      }
      mLabel.setVisibility(View.VISIBLE);
    } else {
      mLabel.setVisibility(View.GONE);
    }

    return true;
  }

  private void showDialog() {
    WPopupRateManager popupRateManager =
        new WPopupRateManager.Builder(getContext(), WPopupType.CARD_ITEM_POPULAR).setTitle(
            mContentTitle)
            .setSubtitle(mContentSubtitle)
            .setUserAction(mUserAction)
            .setUserActionPopupListener(new UserActionPopupListener() {
              @Override public void onClickAddCollection() {
                super.onClickAddCollection();

                switch (CategoryType.getCategory(mCategory)) {
                  case MOVIES:
                    ListFragment listFragment = getListFragment();
                    AbsPagerFragment pagerFragment = getPagerFragment();
                    if (pagerFragment != null) {
                      pagerFragment.showDeckSelectableBottomSheetLayout(mCode);
                    } else if (listFragment != null) {
                      listFragment.showDeckSelectableBottomSheetLayout(mCode);
                    }
                    break;

                  case TV_SEASONS:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.add_only_movie_in_deck)
                        .setPositiveButton(R.string.ok, null);

                    builder.show();
                    break;
                }
              }

              @Override public void onChangedRating(float rating) {
                super.onChangedRating(rating);
                if (rating > 0) {
                  if (mUserAction != null && rating == mUserAction.getRating()) {
                    // 같은 별점을 클릭했으면 취소.
                    requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
                  } else {
                    // 별점 매김 혹은 변경.
                    requestRating(mReleasedAt, UserActionOperator.ActionType.RATING,
                        String.valueOf(rating));
                  }
                } else {
                  // 별 0은 취소.
                  requestRating(null, UserActionOperator.ActionType.RATING_CANCEL, null);
                }
              }

              @Override public void onClickComment() {
                super.onClickComment();
                goWriteComment();
              }

              @Override public void onClickWish() {
                super.onClickWish();
                requestWish();
              }

              @Override public void onClickMeh() {
                super.onClickMeh();
                requestMeh();
              }
            })
            .build();

    popupRateManager.show();
  }
}
