package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.Poster;
import com.frograms.watcha.model.items.bases.DeckBase;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.WImageView;

public class ItemDeckGridView extends ItemView<DeckBase> {

  private String mDeckCode;

  @Bind(R.id.back_image) WImageView mBackImage;
  @Bind(R.id.image) WImageView mImage;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.description) TextView mDesc;

  public ItemDeckGridView(Context context) {
    super(context);
  }

  public ItemDeckGridView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public boolean setItem(@NonNull DeckBase item) {
    super.setItem(item);

    mDeckCode = item.getCode();

    item = CacheManager.getDeck(item.getCode());

    if (item == null) {
      return false;
    }

    Poster photo = item.getImage();
    String url = (photo != null) ? photo.getXLarge() : null;
    mBackImage.load(url);
    mImage.load(url);

    mTitle.setText(item.getName());
    setOnClickListener(new OnClickListener() {
      @Override public void onClick(View v) {
        final String collectionCode = mDeckCode;
        final Bundle b = new BundleSet.Builder().putUserCode(WatchaApp.getUser().getCode())
            .putDeckCode(collectionCode)
            .build()
            .getBundle();
        ActivityStarter.with(getContext(), FragmentTask.DETAIL_DECK).addBundle(b).start();
      }
    });

    mDesc.setText(getResources().getString(R.string.like) + " " + item.getLikesCount());
    return true;
  }

  public int getLayoutId() {
    return R.layout.view_item_deck_grid;
  }
}
