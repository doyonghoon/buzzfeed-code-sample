package com.frograms.watcha.view.itemViews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.PrefHelper;
import com.frograms.watcha.model.items.MediaCompact;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.FeedRatingTextView;
import com.frograms.watcha.view.textviews.RobotoRegularView;
import com.frograms.watcha.view.widget.wImages.ImageType;
import com.frograms.watcha.view.widget.wImages.RatioType;
import com.frograms.watcha.view.widget.wImages.WImageView;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

public class ItemMediaCompactView extends ItemView<MediaCompact> {

  //@Bind(R.id.title) TextView mTitle;
  @Bind(R.id.thumbnail) WImageView mThumbnailView;
  @Bind(R.id.thumbnails_text_layout) LinearLayout mThumbnailsTextLayout;

  public ItemMediaCompactView(Context context) {
    this(context, null);
  }

  public ItemMediaCompactView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public void onClick(View v) {
    if (mCoachmarkHandler == null) {
      super.onClick(v);
    } else {
      mCoachmarkHandler.cleanUp();
      mCoachmarkHandler = null;
    }
  }

  @Override public boolean setItem(MediaCompact item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;
    if (item.getThumbnail() != null && item.getThumbnail().getType() != null) {
      switch (item.getThumbnail().getType()) {
        case POSTER:
          mThumbnailView.setImageType(ImageType.POSTER)
              .setRatioType(RatioType.EQUALS)
              .setScaleType(ImageView.ScaleType.CENTER_CROP);
          break;

        case PERSON:
          mThumbnailView.setImageType(ImageType.PROFILE);
          break;

        case TAG:
          break;
      }
    }
    mThumbnailView.load(url);

    if (mThumbnailsTextLayout.getChildCount() > 0) {
      mThumbnailsTextLayout.removeAllViews();
    }

    if (item.getDescriptions() != null) {
      for (MediaCompact.Description d : item.getDescriptions()) {
        TextView t = getDescText(d);
        mThumbnailsTextLayout.addView(t);

        if (getContext() instanceof Activity
            && d.getDescriptionType() == MediaCompact.Description.DescriptionType.PREDICTED_RATING
            && !PrefHelper.getBoolean(getContext(), PrefHelper.PrefType.COACHMARK, PrefHelper.PrefKey.COACHMARK_PRE_RATING
            .name())) {
          PrefHelper.setBoolean(getContext(), PrefHelper.PrefType.COACHMARK, PrefHelper.PrefKey.COACHMARK_PRE_RATING
              .name(), true);
          ToolTip tooltip = new ToolTip().setGravity(Gravity.TOP)
              .setTitleTextSize(21)
              .setDescriptionTextSize(20)
              .setTitle("예상별점이란 왓챠가 나의 취향을\n분석하여 몇 점을 줄지 예측한 점수예요")
              .setDescription("알겠어요!")
              .setBackgroundColor(Color.TRANSPARENT)
              .setTextColor(Color.WHITE)
              .setMargin(true)
              .setClickListener(new OnClickListener() {
                @Override public void onClick(View v) {
                  if (mCoachmarkHandler != null) {
                    mCoachmarkHandler.cleanUp();
                    mCoachmarkHandler = null;
                  }
                }
              })
              .setShadow(false);
          mCoachmarkHandler = TourGuide.init((Activity) getContext())
              .with(TourGuide.Technique.Click)
              .setToolTip(tooltip)
              .setOverlay(new Overlay().setBackgroundColor(getContext().getResources()
                  .getColor(R.color.skyblue_95)).setStyle(Overlay.Style.Rectangle))
              .playOn(this);
        }
      }
    }

    return true;
  }

  private TourGuide mCoachmarkHandler = null;

  /**
   * 논리 정보는 RATING, PLAIN 으로 나뉘는데 값 보고 맞는 타입으로 텍스트뷰를 리턴해줌.
   */
  private TextView getDescText(MediaCompact.Description d) {
    RobotoRegularView textView = new RobotoRegularView(getContext());
    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13.f);
    textView.setSingleLine();
    textView.setEllipsize(TextUtils.TruncateAt.END);
    LinearLayout.LayoutParams params =
        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    final int p = (int) getResources().getDimension(R.dimen.view_5dp) / 5;
    switch (d.getDescriptionType()) {
      case PREDICTED_RATING:
        FeedRatingTextView.getPreRatingText(textView, d.getText());
        break;

      case WISH:
        FeedRatingTextView.getWishText(textView);
        break;

      case MEH:
        FeedRatingTextView.getMehText(textView);
        break;

      case RATING:
        FeedRatingTextView.getRatingText(textView, d.getText());
        break;

      case PLAIN:
        textView.setText(getMarkdownText(d.getText()));
        params.setMargins(0, 0, p * 5, 0);
        textView.setBackgroundColor(Color.TRANSPARENT);
        textView.setTextColor(getResources().getColor(R.color.heavy_gray));
        break;
    }
    textView.setLayoutParams(params);
    return textView;
  }

  private Drawable getRatingDrawable(MediaCompact.Description d) {
    switch (d.getDescriptionType()) {
      case RATING:
        FontIconDrawable icon = FontIconDrawable.inflate(getContext(), R.xml.icon_rate_action);
        icon.setTextColor(getResources().getColor(R.color.pink));
        return icon;
    }
    return null;
  }

  public int getLayoutId() {
    return R.layout.view_item_media_compact;
  }
}
