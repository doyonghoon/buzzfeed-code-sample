package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.MediaItem;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

public class ItemArticleSmallView extends ItemView<MediaItem> {

  @Bind(R.id.thumbnail) GreatImageView thumbnail;
  @Bind(R.id.subject) TextView subject;
  @Bind(R.id.subtitle) TextView subtitle;

  public ItemArticleSmallView(Context context) {
    super(context);
  }

  public ItemArticleSmallView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected void init() {
    super.init();
    setOnClickListener(this);
  }

  @Override public boolean setItem(MediaItem item) {
    super.setItem(item);

    String url = (item.getThumbnail() != null) ? item.getThumbnail().getImage() : null;

    thumbnail.load(url);
    thumbnail.setOnClickListener(this);

    final String title = item.getTitle();
    if (!TextUtils.isEmpty(title)) {
      subject.setText(title);
      subject.setText(getMarkdownText(title));
    } else {
      subject.setText(null);
      subject.setVisibility(View.GONE);
    }

    final String description = item.getDescription();
    if (!TextUtils.isEmpty(description)) {
      subtitle.setText(description);
      subtitle.setText(getMarkdownText(description));
    } else {
      subtitle.setText(null);
      subtitle.setVisibility(View.GONE);
    }

    return true;
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_article_small;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
  }
}
