package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.helpers.EventOperator;
import com.frograms.watcha.helpers.SettingHelper;
import com.frograms.watcha.model.Button;
import com.frograms.watcha.model.items.Alert;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.widget.ActionGridLayout;
import com.frograms.watcha.view.widget.IconActionGridButton;

public class ItemAlertView extends ItemView<Alert> {

  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.description) TextView mDescription;
  @Bind(R.id.alert_button_layout) ActionGridLayout mButtonsLayout;

  public ItemAlertView(Context context) {
    super(context);
  }

  public ItemAlertView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected int getLayoutId() {
    return R.layout.view_item_edu;
  }

  @Override public boolean setItem(@NonNull Alert item) {
    super.setItem(item);
    if (item == null) {
      return false;
    }

    setText(mTitle, item.getTitle());
    setText(mDescription, item.getDescription());
    if (mButtonsLayout.getChildCount() > 0) {
      mButtonsLayout.removeAllViews();
    }
    if (item.getButtons() != null && !item.getButtons().isEmpty()) {
      for (Button b : item.getButtons()) {
        mButtonsLayout.addView(createButtonTextView(b));
      }
    }
    return true;
  }

  private IconActionGridButton createButtonTextView(@NonNull final Button button) {
    IconActionGridButton gridButton = new IconActionGridButton(getContext());
    gridButton.setText(button.getText());
    gridButton.setTextSize(getResources().getDimension(R.dimen.edu_btn_font));
    gridButton.setBackgroundColor(getResources().getColor(R.color.white));

    gridButton.setOnClickListener(v -> {
      if (button.getEvent() != null) {
        new EventOperator.Builder(getContext(), button.getEvent()).setPreviousScreenName(getScreenName())
            .setCardItem(ItemAlertView.this)
            .build()
            .start();
      }

      if (button.getDismiss() != null) {
        SettingHelper.alert((BaseActivity) getContext(), button.getDismiss());

        if (onRemoveItemListener != null) {
          onRemoveItemListener.onRemove(getPosition());
        }
      }
    });

    return gridButton;
  }
}
