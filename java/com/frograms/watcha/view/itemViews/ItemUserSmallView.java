package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.FollowFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.PartnerHelper;
import com.frograms.watcha.helpers.PartnerHelper.OnBaseFollowResponseListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.User;
import com.frograms.watcha.model.items.Event;
import com.frograms.watcha.model.items.UserSmall;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.ViewUtil;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;

public class ItemUserSmallView extends ItemView<UserSmall> {

  @Bind(R.id.photo) GreatImageView mPhoto;
  @Bind(R.id.name) NameTextView mName;
  @Bind(R.id.description) TextView mDescription;
  @Bind(R.id.button) TextView mButton;

  private UserSmall mUser = null;

  private String mUserCode;
  private boolean mIsButtonOn;

  private boolean mIsClickedFollow = false;

  //private OnBaseFollowResponseListener onFollowListener = null;

  private OnBaseFollowResponseListener createFollowResponseListener(Context context) {
    return new OnBaseFollowResponseListener(context) {
      @Override
      public void onSuccess(@NonNull QueryType queryType, @NonNull BaseResponse<User> result) {
        super.onSuccess(queryType, result);
        mIsClickedFollow = false;
        mIsButtonOn = !mIsButtonOn;
        mUser.setFriend(mIsButtonOn);
        CacheManager.putUser(mUser);
        ViewUtil.setBackground(mButton,
            getResources().getDrawable(mIsButtonOn ? R.drawable.bg_unfollow : R.drawable.bg_follow));
        mButton.setText(
            mIsButtonOn ? mUser.getButtonType().getIconOnId() : mUser.getButtonType().getIconOffId());
        mButton.setTextColor(getResources().getColor((mIsButtonOn) ? R.color.gray : R.color.white));
        if (getContext() instanceof BaseActivity
            && ((BaseActivity) getContext()).getFragment() != null
            && ((BaseActivity) getContext()).getFragment() instanceof FollowFragment) {
          ((FollowFragment) ((BaseActivity) getContext()).getFragment()).updateTabCount();
        }
      }
    };
  }

  public ItemUserSmallView(Context context) {
    super(context);
  }

  public ItemUserSmallView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public int getLayoutId() {
    return R.layout.view_item_user_small;
  }

  @Override public void onClick(View v) {
    super.onClick(v);
    switch (v.getId()) {
      case R.id.photo:
        if (!TextUtils.isEmpty(mUserCode) && WatchaApp.getUser() != null) {
          ActivityStarter.with(getContext(), FragmentTask.PROFILE)
              .addBundle(new BundleSet.Builder().putPreviousScreenName(getScreenName())
                  .putUserCode(mUserCode)
                  .build()
                  .getBundle())
              .start();
        }
        break;
    }
  }

  @Override protected void init() {
    super.init();
    FontHelper.Bold(mName);
    mButton.setOnClickListener(this);
    mPhoto.setOnClickListener(this);
  }

  public boolean setItem(UserSmall item) {
    if (item != null) {
      item.setEvent(new Event("watcha://users/" + item.getCode()));
    }

    super.setItem(item);
    mUserCode = item.getCode();
    mUser = item;
    mUser.setFriend(CacheManager.getUser(mUserCode).isFriend());
    mUser.setPartner(CacheManager.getUser(mUserCode).isPartner());

    mName.setUser(item);

    mDescription.setText(
        getResources().getString(R.string.collection) + " " + item.getDecksCount() + "/ ");
    mDescription.append(
        getResources().getString(R.string.review) + " " + item.getCommentCounts() + "/ ");
    mDescription.append(getResources().getString(R.string.already) + " " + item.getRatingCounts());

    String url = (item.getPhoto() != null) ? item.getPhoto().getSmall() : null;
    mPhoto.load(url);

    if (item.getButtonType() != null && !mUser.getCode().equals(WatchaApp.getUser().getCode())) {
      mButton.setVisibility(VISIBLE);
      switch (item.getButtonType()) {
        case INVITE:
          break;

        case FOLLOW:
        default:
          mIsButtonOn = mUser.isFriend();
          mButton.setOnClickListener(v -> {
            if (mIsClickedFollow) return;

            mIsClickedFollow = true;
            AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CARD,
                !mIsButtonOn ? AnalyticsActionType.FOLLOW : AnalyticsActionType.UNFOLLOW)
                .setLabel(getCardItemName())
                .build());
            PartnerHelper.setFollow((BaseActivity) getContext(), new ReferrerBuilder(getScreenName())
                , mUserCode, !mIsButtonOn, createFollowResponseListener(getContext()));
          });
          break;
      }

      ViewUtil.setBackground(mButton,
          getResources().getDrawable(mIsButtonOn ? R.drawable.bg_unfollow : R.drawable.bg_follow));
      mButton.setText(
          mIsButtonOn ? mUser.getButtonType().getIconOnId() : mUser.getButtonType().getIconOffId());
      mButton.setTextColor(getResources().getColor((mIsButtonOn) ? R.color.gray : R.color.white));
    } else {
      mButton.setVisibility(GONE);
    }

    return true;
  }
}
