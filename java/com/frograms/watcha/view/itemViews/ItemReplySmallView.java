package com.frograms.watcha.view.itemViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.Bind;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.fragment.ReplyFragment;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.ReferrerBuilder;
import com.frograms.watcha.model.items.Reply;
import com.frograms.watcha.model.response.BaseResponse;
import com.frograms.watcha.model.response.data.CommentReportData;
import com.frograms.watcha.retrofit.DataProvider;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.utils.Util;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.NameTextView;
import com.frograms.watcha.view.textviews.RichTextView;
import com.frograms.watcha.view.widget.wImages.GreatImageView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.Date;

public class ItemReplySmallView extends ItemView<Reply> {

  @Bind(R.id.reply_userimage) GreatImageView mUserImage;
  @Bind(R.id.user_name) NameTextView mUserName;
  @Bind(R.id.reply) RichTextView mText;

  public ItemReplySmallView(Context context) {
    super(context);
  }

  public ItemReplySmallView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public int getLayoutId() {
    return R.layout.view_item_reply_small;
  }

  @Override protected void init() {
    super.init();

    FontHelper.RobotoBold(mUserName);
  }

  @Override public boolean setItem(Reply item) {
    super.setItem(item);

    if (item == null) {
      return false;
    }

    if (item.getUser() != null) {
      String url =
          (item.getUser().getPhoto() != null) ? item.getUser().getPhoto().getSmall() : null;
      mUserImage.load(url);

      mUserName.setUser(item.getUser());

      mUserImage.setOnClickListener(this);
    }

    mText.setText(item.getMessage());
    return true;
  }
}
