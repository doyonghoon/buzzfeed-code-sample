package com.frograms.watcha.view;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import carbon.animation.AnimUtils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.frograms.watcha.R;
import com.frograms.watcha.activity.BaseActivity;
import com.frograms.watcha.adapters.VendorAdapter;
import com.frograms.watcha.database.CacheManager;
import com.frograms.watcha.fragment.GalleryFragment;
import com.frograms.watcha.fragment.WriteCommentFragment;
import com.frograms.watcha.fragment.abstracts.AbsPagerFragment;
import com.frograms.watcha.fragment.abstracts.BaseFragment;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.helpers.EventOperator;
import com.frograms.watcha.helpers.FontHelper;
import com.frograms.watcha.helpers.FontIconHelper;
import com.frograms.watcha.helpers.UserActionHelper;
import com.frograms.watcha.helpers.UserActionOperator;
import com.frograms.watcha.helpers.WPopupRateManager;
import com.frograms.watcha.helpers.WPopupType;
import com.frograms.watcha.listeners.BottomSheetDeckCardItemListener;
import com.frograms.watcha.listeners.BottomSheetDeckCreateButtonClickListener;
import com.frograms.watcha.listeners.UserActionPopupListener;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.Media;
import com.frograms.watcha.model.categories.CategoryType;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.model.items.UserAction;
import com.frograms.watcha.retrofit.QueryType;
import com.frograms.watcha.view.drawers.CardBottomSheet;
import com.frograms.watcha.view.fonticon.FontIconDrawable;
import com.frograms.watcha.view.textviews.DefTextView;
import com.frograms.watcha.view.widget.IconActionGridButton;
import com.frograms.watcha.view.widget.RatingActionGridButton;
import com.frograms.watcha.view.widget.wImages.WImageView;
import com.frograms.watcha.views.FakeActionBar;
import com.nineoldandroids.animation.Animator;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ListHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * 개영페 상단 헤더뷰
 */
public class DetailContentHeaderView extends LinearLayout implements View.OnClickListener {

  private Content mContent;
  //private UserActionHelper.OnBaseRatingResponseListener mUserActionResponseListener;

  @Bind(R.id.content_simple_header_layout) LinearLayout mContentLayout;
  @Bind(R.id.wish_grid) IconActionGridButton mWishButton;
  @Bind(R.id.rating_grid) RatingActionGridButton mRatingButton;
  @Bind(R.id.comment_grid) IconActionGridButton mCommentButton;
  @Bind(R.id.more_grid) IconActionGridButton mMoreButton;

  @Bind(R.id.cover_layout) carbon.widget.RelativeLayout mCoverLayout;
  @Bind(R.id.vendor_title) DefTextView mVendorTitle;
  @Bind(R.id.detail_hanmadi_text) TextView mHanmadiText;
  @Bind(R.id.detail_hanmadi_layout) LinearLayout mHanmadiLayout;
  @Bind(R.id.cover) WImageView mCover;
  @Bind(R.id.poster) WImageView mPoster;
  @Bind(R.id.poster_layout) View mPosterLayout;
  @Bind(R.id.title) TextView mTitle;
  @Bind(R.id.rating) TextView mRating;

  private UserAction mUserAction;

  public DetailContentHeaderView(Context context) {
    this(context, null, 0);
  }

  public DetailContentHeaderView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public DetailContentHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    inflate(getContext(), R.layout.view_detail_content_header, this);
    ButterKnife.bind(this);
    FontHelper.RobotoRegular(mTitle);
    FontHelper.RobotoRegular(mRating);
    FontHelper.RobotoRegular(mVendorTitle);
    // 데이터 로딩되는 시점엔 타이틀이나 평점 정보가 보여서는 안됨.
    mContentLayout.setAlpha(0);

    @ColorInt int heavyGrayColor = getResources().getColor(R.color.heavy_gray);
    mWishButton.setTextNormalColor(heavyGrayColor);
    mWishButton.setIconColor(heavyGrayColor);
    mCommentButton.setTextNormalColor(heavyGrayColor);
    mCommentButton.setIconColor(heavyGrayColor);
    mMoreButton.setTextNormalColor(heavyGrayColor);
    mMoreButton.setIconColor(heavyGrayColor);
  }

  private BottomSheetLayout mBottomSheetLayout;

  public void setBottomSheet(@NonNull BottomSheetLayout bottomSheetLayout) {
    mBottomSheetLayout = bottomSheetLayout;
  }

  /**
   * 뷰가 화면에 보여질때 평가영역 정보를 최신으로 데이터로 교체함.
   *
   * 특히 {@link WriteCommentFragment} 에서 돌아올때 대응하려고 오버라이드한 메소드.
   */
  @Override protected void onAttachedToWindow() {
    super.onAttachedToWindow();
  }

  public void refreshDeckList() {
    mMorePopupListener.onClickAddCollection();
  }

  public void updateHanmadiColor(@NonNull ContentToolbarView.SimpleSwatch swatch) {
    mHanmadiLayout.setVisibility(View.VISIBLE);
    mHanmadiText.setTextColor(swatch.getTextColor());
    flyIn(mHanmadiText);
    ObjectAnimator.ofObject(mHanmadiLayout, "backgroundColor", new ArgbEvaluator(),
        getResources().getColor(R.color.black), swatch.getBackgroundColor())
        .setDuration(400)
        .start();
  }

  /**
   * 새로운 상세 정보로 데이터를 채움.
   *
   * @param content 새로운 상세 정보 데이터.
   */
  public void setDetailData(Content content) {
    mContent = content;
    mUserAction = CacheManager.getMyUserAction(mContent.getCode());
    updateUi();
  }

  private List<Media> mMedias = new ArrayList<>();

  /**
   * 포스터를 클릭했을 때, 갤러리 화면으로 이동할 때 사용할 데이터.
   */
  public void setMedias(List<Media> medias) {
    if (medias != null) {
      mMedias = medias;
    }
  }

  private String getOneDecimalPlaces(float value) {
    return String.format("%.1f", value);
  }

  private String convertNumberToWords(int n) {
    if (n >= 100000000) {
      return (n / 100000000) + "억 명";
    }
    if (n >= 10000) {
      return (n / 10000) + "만 명";
    }
    String str = String.valueOf(n);
    if (str.length() > 3) {
      str =
          str.substring(0, str.length() - 3) + "," + str.substring(str.length() - 3, str.length());
    }
    //if (n >= 1000) {
    //  return (n / 1000) + "천 명";
    //}
    //if (n >= 100) {
    //  return (n / 100) + "백 명";
    //}
    //if (n >= 10) {
    //  return (n / 10) + "십 명";
    //}
    return str + " 명";
  }

  /**
   * {@link AnimUtils#flyIn(View, Animator.AnimatorListener)} 을 호출하는 건데, 그 전에 alpha 값이 0 이 아니면 0 으로
   * 맞추고 실행함.
   */
  private void flyIn(@NonNull final View v) {
    v.post(new Runnable() {
      @Override public void run() {
        if (v.getAlpha() != 0.0f) {
          v.setAlpha(0.0f);
        }
        AnimUtils.flyIn(v, null);
      }
    });
  }

  private FontIconDrawable getRatingDrawable() {
    return FontIconHelper.setFontIconDrawable(getContext(), R.xml.icon_rate_action)
        .setColor(Color.WHITE)
        .setSize(getResources().getDimension(R.dimen.banner_text_size))
        .getDrawable();
  }

  public void setPaletteSubscriber(Action1<Bitmap> subscriber) {
    mPaletteSubscriber = subscriber;
  }

  private Action1<Bitmap> mPaletteSubscriber = null;

  private void updateUi() {
    // 커버와 포스터 이미지를 로드하고, 커버가 높은 우선순위로 리턴해줌.
    BitmapLoadingObservableOnSubscribe coverLoadingSubscriber =
        new BitmapLoadingObservableOnSubscribe(mCover, mContent.getStillCut().getXLarge(),
            mContent.getHanmadiText());
    BitmapLoadingObservableOnSubscribe posterLoadingSubscriber =
        new BitmapLoadingObservableOnSubscribe(mPoster, mContent.getPoster().getLarge(),
            mContent.getHanmadiText());

    Observable.zip(Observable.create(coverLoadingSubscriber),
        Observable.create(posterLoadingSubscriber), new Func2<Bitmap, Bitmap, Bitmap>() {
          @Override public Bitmap call(Bitmap cover, Bitmap poster) {
            return cover != null ? cover : poster;
          }
        }).filter(new Func1<Bitmap, Boolean>() {
      @Override public Boolean call(Bitmap bitmap) {
        return !TextUtils.isEmpty(mContent.getHanmadiText());
      }
    }).subscribe(new Action1<Bitmap>() {
      @Override public void call(Bitmap bitmap) {
        if (mPaletteSubscriber != null) {
          mPaletteSubscriber.call(bitmap);
        }
      }
    });
    mCover.load(mContent.getStillCut().getXLarge());
    mPoster.load(mContent.getPoster().getLarge());

    mTitle.setText(mContent.getTitle());
    mRating.setText(getOneDecimalPlaces(mContent.getAverageRating()) + " (" + convertNumberToWords(
        mContent.getRatingCount()) + ")");
    mRating.setCompoundDrawablesWithIntrinsicBounds(getRatingDrawable(), null, null, null);

    mCover.setVisibility(View.VISIBLE);
    AnimUtils.brightnessSaturationFadeIn(mCover, null);
    mContent.getRatingCount();

    mWishButton.setOnClickListener(this);
    mRatingButton.setOnClickListener(this);
    mCommentButton.setOnClickListener(this);
    mMoreButton.setOnClickListener(this);

    updateUserAction();

    flyIn(mContentLayout);
    AnimUtils.popIn(mPosterLayout, null);
    updatePlayInfos(mContent);
    updateHanmadiText();
  }

  /**
   * 한마디 카드 영역 업데이트.
   */
  private void updateHanmadiText() {
    if (!TextUtils.isEmpty(mContent.getHanmadiText())) {
      mHanmadiText.setText(mContent.getHanmadiText());
    }
  }

  /**
   * 벤더 정보 영역 업데이트.
   */
  private void updatePlayInfos(Content content) {
    Content.Vod[] infos = content.getVods();
    if (infos != null && infos.length > 0) {
      mVendorTitle.setAlpha(0.0f);
      mVendorTitle.setVisibility(View.VISIBLE);
      flyIn(mVendorTitle);
      mVendorTitle.setOnClickListener(this);

      Content.Vod minInfo = infos[0];

      mVendorTitle.setText(minInfo.getPrice());
    }
  }

  /**
   * 평가정보 영역 업데이트.
   */
  private void updateUserAction() {
    if (mContent == null) return;

    mUserAction = CacheManager.getMyUserAction(mContent.getCode());
    if (mUserAction == null) return;

    float rating = mUserAction.getRating();
    boolean isWish = mUserAction.isWished();
    boolean isMeh = mUserAction.isMehed();
    boolean hasComment = mUserAction.getComment() != null;

    mCommentButton.setFocused(hasComment);
    mWishButton.setFocused(isWish);
    mRatingButton.setRating(rating);
  }

  @OnClick(R.id.poster) void onClickPoster() {
    if (mMedias != null) {
      CategoryType categoryType = CategoryType.getCategory(mContent.getCategory());
      ActivityStarter.with(getContext(), FragmentTask.GALLERY)
          .addBundle(new BundleSet.Builder().putMediaJsonArray(mMedias)
              .putSelectedTab(0)
              .putGalleryType(getGalleryType(categoryType))
              .build()
              .getBundle())
          .start();
    }
  }

  @Nullable
  private GalleryFragment.GalleryType getGalleryType(CategoryType categoryType) {
    if (categoryType != null) {
      switch (categoryType) {
        case MOVIES:
          return GalleryFragment.GalleryType.MOVIE;
        case TV_SEASONS:
          return GalleryFragment.GalleryType.DRAMA;
        default:
        case BOOKS:
          return null;
      }
    }
    return null;
  }

  @Override public void onClick(View v) {
    String title, subtitle;
    WPopupRateManager.Builder p;

    switch (v.getId()) {
      case R.id.wish_grid:
        requestWish();
        break;

      case R.id.rating_grid:
        title = mContent.getTitle();
        subtitle = String.valueOf(mContent.getYear());
        p = new WPopupRateManager.Builder(getContext(), WPopupType.DETAIL_RATING);
        p.setTitle(title)
            .setSubtitle(subtitle)
            .setUserAction(mUserAction)
            .setUserActionPopupListener(mRatingPopupListener);
        p.build().show();
        break;
      case R.id.comment_grid:
        //mCommentButton.setFocused(!mCommentButton.isFocused());
        ActivityStarter.with(getContext(), FragmentTask.WRITE_COMMENT)
            .addBundle(new BundleSet.Builder().putCategoryType(
                CategoryType.getCategory(mContent.getCategory()))
                .putContentCode(mContent.getCode())
                .putPreviousScreenName(getScreenName(getContext()))
                .build()
                .getBundle())
            .start();
        break;
      case R.id.more_grid:
        title = mContent.getTitle();
        subtitle = String.valueOf(mContent.getYear());
        p = new WPopupRateManager.Builder(getContext(), WPopupType.DETAIL_MORE);
        p.setTitle(title)
            .setSubtitle(subtitle)
            .setUserAction(mUserAction)
            .setUserActionPopupListener(mMorePopupListener);
        p.build().show();
        break;
      case R.id.vendor_title:
        if (mContent != null && mContent.getVods() != null && mContent.getVods().length > 0) {
          @AnalyticsEvent.AnalyticsEventLabel final String contentCode = mContent.getCode();
          AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.SOCIAL, AnalyticsActionType.WATCH)
              .setLabel(contentCode)
              .build());

          FakeActionBar header = new FakeActionBar(getContext());
          header.setTitle("감상하기");
          VendorAdapter adapter = new VendorAdapter(getContext());
          adapter.addAll(mContent.getVods());
          DialogPlus dialog = new DialogPlus.Builder(getContext()).setHeader(header)
              .setContentHolder(new ListHolder())
              .setAdapter(adapter)
              .setGravity(DialogPlus.Gravity.BOTTOM)
              .setCancelable(true)
              .setOnItemClickListener((dialogPlus, o, view, i) -> {
                Content.Vod info = mContent.getVods()[i - 1];
                if (info.getEvent() != null) {
                  EventOperator operator =
                      new EventOperator.Builder(getContext(), info.getEvent()).build();
                  operator.start();
                }
              })
              .create();
          dialog.show();
        }
        break;
    }
  }

  @Nullable private String getScreenName(@Nullable Context context) {
    if (context != null && context instanceof BaseActivity) {
      BaseFragment baseFragment = ((BaseActivity) context).getFragment();
      if (baseFragment instanceof AbsPagerFragment) {
        return ((AbsPagerFragment) baseFragment).getCurrentFragment().getCurrentScreenName();
      } else {
        return baseFragment.getCurrentScreenName();
      }
    }
    return null;
  }

  private void requestUserAction(UserActionOperator.ActionType type, @Nullable String ratingValue) {
    UserActionOperator.Builder builder =
        new UserActionOperator.Builder((BaseActivity) getContext(), type).setContentCode(
            mContent.getCode())
            .setCategoryType(CategoryType.getCategory(mContent.getCategory()))
            .setReleasedAt(mContent.getReleasedAt());
    if (NumberUtils.isNumber(ratingValue)) {
      builder.setParams(Collections.singletonMap("value", ratingValue));
    }
    builder.setRatingResponseListener(
        new UserActionHelper.OnBaseRatingResponseListener(getContext()));
    builder.build().request(ratingData -> updateUserAction());
  }

  public void requestWish() {
    UserActionOperator.ActionType type =
        (mUserAction == null || !mUserAction.isWished()) ? UserActionOperator.ActionType.WISH
            : UserActionOperator.ActionType.WISH_CANCEL;
    requestUserAction(type, null);
  }

  public void requestMeh() {
    UserActionOperator.ActionType type =
        (mUserAction == null || !mUserAction.isMehed()) ? UserActionOperator.ActionType.MEH
            : UserActionOperator.ActionType.MEH_CANCEL;
    requestUserAction(type, null);
  }

  private UserActionPopupListener mMorePopupListener = new UserActionPopupListener() {
    @Override public void onClickMeh() {
      super.onClickMeh();
      requestMeh();
    }

    @Override public void onClickWish() {
      super.onClickWish();
      requestWish();
    }

    @Override public void onClickAddCollection() {
      super.onClickAddCollection();
      switch (CategoryType.getCategory(mContent.getCategory())) {
        case MOVIES:
          Map<String, String> map = new HashMap<>();
          map.put("content_code", mContent.getCode());

          mBottomSheetLayout.showWithSheetView(
              new CardBottomSheet.Builder(getContext()).setTitle("컬렉션 선택")
                  .setQueryType(QueryType.DECK_MY_LIST)
                  .setParams(map)
                  .setButtonClickListener(
                      new BottomSheetDeckCreateButtonClickListener(mBottomSheetLayout,
                          mContent.getCode()))
                  .setOnCardItemClickListener(
                      new BottomSheetDeckCardItemListener(mBottomSheetLayout, mContent.getCode()))
                  .build());

          //mBottomSheetLayout.setOnSheetStateChangeListener(new BottomSheetLayout.OnSheetStateChangeListener() {
          //  @Override public void onSheetStateChanged(BottomSheetLayout.State state) {
          //    if (state.equals(BottomSheetLayout.State.PEEKED)) {
          //      mBottomSheetLayout.expandSheet();
          //      mBottomSheetLayout.setOnSheetStateChangeListener(null);
          //    }
          //  }
          //});
          break;

        case TV_SEASONS:
          AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
          builder.setMessage(R.string.add_only_movie_in_deck).setPositiveButton(R.string.ok, null);

          builder.show();
          break;
      }
    }
  };

  private UserActionPopupListener mRatingPopupListener = new UserActionPopupListener() {
    @Override public void onChangedRating(float rating) {
      super.onChangedRating(rating);
      if (mUserAction == null) {
        if (getContext() instanceof Activity) {
          SnackbarManager.show(Snackbar.with(getContext())
              .text("문제가 발견되어서 평가가 반영되지 않았습니다 ㅠ_ㅠ")
              .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
              .position(Snackbar.SnackbarPosition.BOTTOM)
              .type(SnackbarType.SINGLE_LINE), (Activity) getContext());
        }
        return;
      }
      if (rating > 0) {
        if (rating == mUserAction.getRating()) {
          // 같은 별점을 클릭했으면 취소.
          //requestRating(UserActionHelper.ActionType.RATING, false, null);
          requestUserAction(UserActionOperator.ActionType.RATING_CANCEL, null);
        } else {
          // 별점 매김 혹은 변경.
          //Map<String, String> params = new HashMap<>();
          //params.put("value", String.valueOf(rating));
          //requestRating(UserActionHelper.ActionType.RATING, true, params);
          requestUserAction(UserActionOperator.ActionType.RATING, String.valueOf(rating));
        }
      } else {
        // 별 0은 취소.
        //requestRating(UserActionHelper.ActionType.RATING, false, null);
        requestUserAction(UserActionOperator.ActionType.RATING_CANCEL, null);
      }
    }
  };

  private static class BitmapLoadingObservableOnSubscribe
      implements Observable.OnSubscribe<Bitmap> {

    private WImageView mView;
    private String mUrl;
    private String mHanmadiText;

    BitmapLoadingObservableOnSubscribe(WImageView v, String url, String hanmadiText) {
      mView = v;
      mUrl = url;
      mHanmadiText = hanmadiText;
    }

    @Override public void call(final Subscriber<? super Bitmap> subscriber) {
      mView.setOnFinishLoadingImageListener(new WImageView.OnFinishLoadingImageListener() {
        @Override public void onFinishLoadingImage(@Nullable Bitmap bitmap, WImageView view) {
          subscriber.onNext(!TextUtils.isEmpty(mHanmadiText) ? bitmap : null);
          subscriber.onCompleted();
        }
      });
      mView.load(mUrl);
    }
  }
}