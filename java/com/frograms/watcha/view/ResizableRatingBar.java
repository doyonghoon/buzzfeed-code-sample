package com.frograms.watcha.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RatingBar;
import com.frograms.watcha.utils.WLog;

public class ResizableRatingBar extends RatingBar {

  //	private boolean mInitialized = false;
  //	private int mResId;
  //	private int mHeight;
  private float startRating;
  private float beforeRating;
  private OnRatingBarClickListener mRatingBarClickListener = null;
  //	private int mWidth = 0;
  View.OnTouchListener mGestureListener;
  private GestureDetector mGestureDetector;

  private OnRatingDragListener mRatingDragListener;

  private boolean onScroll = false;
  //	private boolean onChange = false;

  private int mX, mY;

  private Context mContext;

  public static interface OnRatingBarClickListener {
    void onRatingClick(RatingBar ratingBar, float rating);
  }

  public interface OnRatingDragListener {
    public void onRatingTouchDown();

    public void onRatingCancel();

    public void onRatingTouchUp(float value);

    public void onRatingDrag(float value);
  }

  @Override public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    mGestureListener = null;
    mGestureDetector = null;
  }

  @Override public void onAttachedToWindow() {
    if (mContext != null) {
      mGestureDetector = new GestureDetector(mContext, new YScrollDetector());
    }
  }

  public ResizableRatingBar(Context context) {
    this(context, null);
    mContext = context;
  }

  public ResizableRatingBar(Context context, AttributeSet attrs) {
    this(context, attrs, android.R.attr.ratingBarStyle);
    mContext = context;
    //		mResId = attrs.getAttributeResourceValue(
    //				"http://schemas.android.com/apk/res/android",
    //				"progressDrawable", -1);
  }

  public ResizableRatingBar(Context context, AttributeSet attrs, int defStyle) {
    this(context, attrs, defStyle, 0);
    mContext = context;
    //		mResId = attrs.getAttributeResourceValue(
    //				"http://schemas.android.com/apk/res/android",
    //				"progressDrawable", -1);
  }

  public ResizableRatingBar(Context context, AttributeSet attrs, int defStyle, int styleRes) {
    super(context, attrs, defStyle);
    mContext = context;
    //		mResId = attrs.getAttributeResourceValue(
    //				"http://schemas.android.com/apk/res/android",
    //				"progressDrawable", -1);
  }

  public void setOnRatingBarDragListener(OnRatingDragListener listener) {
    mRatingDragListener = listener;
  }

  public void setOnRatingBarClickListener(OnRatingBarClickListener listener) {
    mRatingBarClickListener = listener;
  }

  //	public boolean onChange() {
  //		if (!onChange)
  //			return false;
  //		else
  //			return true;
  //	}

  @Override public boolean onTouchEvent(MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_DOWN) onScroll = false;

    if (onScroll) {
      //			WLog.i("OnScrolling......");
      return false;
    }
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        //			WLog.i("TouchEvent Down");
        mX = (int) event.getX();
        mY = (int) event.getY();
        break;

      case MotionEvent.ACTION_UP:
        //			WLog.i("TouchEvent Up");
        if ((Math.abs(mX - event.getX()) < Math.abs(mY - event.getY()))
            && Math.abs(mY - event.getY()) >= 10) {
          //				WLog.i("TouchEvent Up (Scoroll detected)");
          onScroll = true;
          setRating(beforeRating);
          return true;
        }
        break;

      case MotionEvent.ACTION_CANCEL:
      case MotionEvent.ACTION_OUTSIDE:
        if (mRatingDragListener != null) {
          mRatingDragListener.onRatingCancel();
        }
        break;
    }

    if (mGestureDetector == null) {
      //			WLog.i("mGestureDetector is null");
      return false;
    }

    if (mGestureDetector.onTouchEvent(event)) {
      onScroll = true;
      //			WLog.i("onTouchEvent Rating... :" + getRating());
      this.setRating(beforeRating);
      if (event.getAction() == MotionEvent.ACTION_DOWN) {
        //				WLog.i("TouchEvent Down : reutrn false");
        return false;
      } else {
        //				WLog.i("TouchEvent Down is Not : reutrn true");
        return true;
      }
    }

    if (mRatingBarClickListener == null) {
      WLog.i("mRatingBarClickListener is null");
      return super.onTouchEvent(event);
    }

    if (event.getAction() == MotionEvent.ACTION_DOWN) {
      //			WLog.i("TouchEvent Down, Set rating for click : " + getRating());
      beforeRating = getRating();
    }

    boolean ret = super.onTouchEvent(event);
    //		if (true) return false;
    // 이하 not fromUser
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        WLog.i("TouchEvent Down 2 : " + getRating());
        startRating = getRating();
        mX = (int) event.getX();
        mY = (int) event.getY();

        //			if (mRatingDragListener != null) {
        //        		mRatingDragListener.onRatingTouchDown();
        //        	}

        break;
      case MotionEvent.ACTION_MOVE:
        final int moveY = (int) event.getY();
        final int diffY = Math.abs(moveY - mY);

        if (mRatingDragListener != null
            && diffY < 20
            && getRating() > 0
            && startRating != getRating()) {
          mRatingDragListener.onRatingDrag(getRating());
        }

        WLog.i("TouchEvent Move 2 : " + getRating());
        break;

      case MotionEvent.ACTION_UP:
        //			WLog.i("TouchEvent Up 2 : " + getRating());
        if ((Math.abs(mX - event.getX()) < Math.abs(mY - event.getY()))
            && Math.abs(mY - event.getY()) >= 10) {
          //				WLog.i("TouchEvent Up 2(Scoroll detected)");
          return false;
        }

        WLog.i(beforeRating + " " + getRating());
        if (beforeRating == getRating()) {
          //    			WLog.i("TouchEvent Up 2 : " + getRating() + ", before : " + beforeRating);
          mRatingBarClickListener.onRatingClick(this, startRating);
        }
        break;

      case MotionEvent.ACTION_CANCEL:
      case MotionEvent.ACTION_OUTSIDE:
        if (mRatingDragListener != null) {
          //        		mRatingDragListener.onRatingTouchUp(getRating());
        }
        break;
    }

    return ret;
  }

  //	@Override
  //	public boolean onTouchEvent(MotionEvent event) {
  //		if (mGestureDetector.onTouchEvent(event)) {
  //			//event.setAction(MotionEvent.ACTION_UP);
  //			//super.onTouchEvent(event);
  //			Log.i("here2","?"+getRating());
  //			return false;
  //		}
  //
  //		if (mRatingBarClickListener == null) return super.onTouchEvent(event);
  //
  //		boolean ret = super.onTouchEvent(event);
  //
  //
  //        switch (event.getAction()) {
  //        case MotionEvent.ACTION_DOWN:
  //        	startRating = (int) getRating();
  //        	break;
  //        case MotionEvent.ACTION_UP:
  //        	if ((int)getRating() == mRating && (int)getRating() !=0) {
  //        		mRatingBarClickListener.onRatingClick(this, startRating);
  //        	}
  //        	mRating = (int)getRating();
  //        	break;
  //        case MotionEvent.ACTION_MOVE:
  //        	break;
  //        }
  //		return ret;
  //	}

  //	@Override
  //	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
  //		int width = MeasureSpec.getSize(widthMeasureSpec);
  //		if (width > 0) {
  //			if (!mInitialized || mWidth > width) {
  //				Drawable drawable = getResources().getDrawable(mResId);
  //				mWidth = width;
  //				if (drawable != null) {
  //					drawable = resize(drawable, width/5);
  //					drawable = tileify(drawable, false);
  ////					setProgressDrawable(drawable);
  //				}
  ////				setRating(this.getRating());
  //				mInitialized = true;
  //			}
  //			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  ////			setMeasuredDimension(resolveSize(width, widthMeasureSpec), mHeight);
  //		};// else super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  //	}
  //
  //	private Drawable resize(Drawable drawable, int width) {
  //
  //		if (drawable instanceof LayerDrawable) {
  //			LayerDrawable background = (LayerDrawable) drawable;
  //			final int N = background.getNumberOfLayers();
  //			Drawable[] outDrawables = new Drawable[N];
  //
  //			for (int i = 0; i < N; i++) {
  //				outDrawables[i] = resize(background.getDrawable(i), width);
  //			}
  //
  //			LayerDrawable newBg = new LayerDrawable(outDrawables);
  //
  //			for (int i = 0; i < N; i++) {
  //				newBg.setId(i, background.getXmlResId(i));
  //			}
  //
  //			return newBg;
  //
  //		} else if (drawable instanceof BitmapDrawable) {
  //			final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
  //			mHeight = (int) (width * ((float) bitmap.getHeight() / bitmap.getWidth()));
  //
  //			Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, width,
  //					mHeight, true);
  //			return new BitmapDrawable(getResources(), scaledBitmap);
  //		}
  //
  //		return drawable;
  //	}
  //
  //	private Drawable tileify(Drawable drawable, boolean clip) {
  //
  //		if (drawable instanceof LayerDrawable) {
  //			LayerDrawable background = (LayerDrawable) drawable;
  //			final int N = background.getNumberOfLayers();
  //			Drawable[] outDrawables = new Drawable[N];
  //
  //			for (int i = 0; i < N; i++) {
  //				int id = background.getXmlResId(i);
  //				outDrawables[i] = tileify(
  //						background.getDrawable(i),
  //						(id == android.R.id.progress || id == android.R.id.secondaryProgress));
  //			}
  //
  //			LayerDrawable newBg = new LayerDrawable(outDrawables);
  //
  //			for (int i = 0; i < N; i++) {
  //				newBg.setId(i, background.getXmlResId(i));
  //			}
  //
  //			return newBg;
  //
  //		} else if (drawable instanceof StateListDrawable) {
  //			StateListDrawable in = (StateListDrawable) drawable;
  //			StateListDrawable out = new StateListDrawable();
  //			in.getS
  //			int numStates = in.getState().length;
  //			for (int i = 0; i < numStates; i++) {
  //				out.addState(in.getState(, tileify(in.getStateDrawable(i), clip));
  //			}
  //			return out;
  //		} else if (drawable instanceof BitmapDrawable) {
  //			final Bitmap tileBitmap = ((BitmapDrawable) drawable).getBitmap();
  //
  //			final ShapeDrawable shapeDrawable = new ShapeDrawable(
  //					getDrawableShape());
  //
  //			final BitmapShader bitmapShader = new BitmapShader(tileBitmap, TileMode.REPEAT, TileMode.CLAMP);
  //			shapeDrawable.getPaint().setShader(bitmapShader);
  //
  //			return (clip) ? new ClipDrawable(shapeDrawable, Gravity.LEFT,
  //					ClipDrawable.HORIZONTAL) : shapeDrawable;
  //		}
  //		return drawable;
  //	}
  //
  //	Shape getDrawableShape() {
  //		final float[] roundedCorners = new float[] { 5, 5, 5, 5, 5, 5, 5, 5 };
  //		return new RoundRectShape(roundedCorners, null, null);
  //	}

  private class YScrollDetector extends SimpleOnGestureListener {

    @Override public boolean onSingleTapConfirmed(MotionEvent e) {
      WLog.i("event: " + e.getAction());
      if (mRatingDragListener != null) {
        mRatingDragListener.onRatingTouchDown();
      }
      return super.onSingleTapConfirmed(e);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
      //			Log.d("ResizableRatingBar", Math.abs(distanceX) + " " + Math.abs(distanceY));
      if (Math.abs(distanceY) > Math.abs(distanceX)) {
        return true;
      }

      if (mRatingDragListener != null) {
        mRatingDragListener.onRatingTouchUp(getRating());
      }

      return false;
    }

    @Override public boolean onFling(MotionEvent e1, MotionEvent e2, float vX, float vY) {
      //			Log.d("onFling", Math.abs(vX) + " " + Math.abs(vY));
      return false;
    }
  }
}
