package com.frograms.watcha.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.helpers.AnalyticsActionType;
import com.frograms.watcha.helpers.AnalyticsCategoryType;
import com.frograms.watcha.helpers.AnalyticsEvent;
import com.frograms.watcha.helpers.AnalyticsManager;
import com.frograms.watcha.model.ActivityStarter;
import com.frograms.watcha.model.BundleSet;
import com.frograms.watcha.model.FragmentTask;
import com.frograms.watcha.model.SearchSuggestionText;
import com.frograms.watcha.utils.ScreenNameUtils;
import com.frograms.watcha.view.IncredibleSearchTextLayout;
import java.util.ArrayList;
import java.util.List;
import rx.functions.Action1;

/**
 * 자동완성 검색어 결과.
 */
public class SearchAutocompleteAdapter extends ArrayAdapter<SearchSuggestionText>
    implements Filterable {

  private Handler mHandler = new Handler(Looper.getMainLooper());
  private final IncredibleSearchTextLayout mLayout;
  private Action1<String> mOnClickAction;

  public SearchAutocompleteAdapter(@NonNull Context context,
      @NonNull IncredibleSearchTextLayout layout, @Nullable Action1<String> onClickAction) {
    super(context, R.layout.item_autocomplete);
    setNotifyOnChange(true);
    mLayout = layout;
    mOnClickAction = onClickAction;
  }

  public void setSearchTexts(final SearchSuggestionText[] texts) {
    mHandler.postDelayed(() -> {
      clear();
      for (SearchSuggestionText t : texts) {
        add(t);
      }
    }, 1);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder;
    if (convertView == null) {
      convertView =
          LayoutInflater.from(getContext()).inflate(R.layout.item_autocomplete, parent, false);
      holder = new ViewHolder(convertView);
      convertView.setTag(holder);
    } else {
      holder = (ViewHolder) convertView.getTag();
    }

    boolean hasTitle = getItem(position).hasTitle();
    final boolean isCachedData = getItem(position).isFromLocal();
    holder.getTitle().setVisibility(hasTitle ? View.VISIBLE : View.GONE);
    final String value = this.getItem(position).getText();
    if (hasTitle) {
      holder.getTitle().setText(isCachedData ? "최근 검색어" : "자동 검색어");
    }
    holder.getText().setText(getItem(position).getText());

    if (mLayout != null) {
      convertView.setOnClickListener(v -> mLayout.canGoToSearchResult(value)
          .flatMap(aBoolean -> mLayout.cacheQueryObservable(value))
          .doOnSubscribe(() -> {
            mLayout.getSearchView().clearFocus();
            mLayout.requestFocus();
            AnalyticsManager.sendEvent(new AnalyticsEvent.Builder(AnalyticsCategoryType.CONTENT,
                isCachedData ? AnalyticsActionType.CLICK_AUTOCOMPLETE
                    : AnalyticsActionType.CLICK_LATEST_SEARCH_QUERY).build());
          })
          .subscribe(mOnClickAction == null ? query -> {
            final Bundle b = new BundleSet.Builder().putSearchQuery(query)
                .putPreviousScreenName(ScreenNameUtils.getScreenName(mLayout.getContext()))
                .build()
                .getBundle();
            ActivityStarter.with(getContext(), FragmentTask.SEARCH_RESULT)
                .addBundle(b)
                .setRequestCode(ActivityStarter.SEARCH_RESULT_REQUEST)
                .start();
          } : mOnClickAction));
    } else {
      convertView.setOnClickListener(null);
    }

    return convertView;
  }

  @Override public Filter getFilter() {
    return mFilter;
  }

  private Filter mFilter = new Filter() {
    @Override protected Filter.FilterResults performFiltering(CharSequence constraint) {
      Filter.FilterResults filterResults = new Filter.FilterResults();

      List<String> objects = new ArrayList<>();
      for (int i = 0; i < getCount(); i++) {
        if (!getItem(i).hasTitle()) {
          objects.add(getItem(i).getText());
        }
      }
      filterResults.values = objects;
      filterResults.count = getCount();
      return filterResults;
    }

    @Override protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
      if (results != null && results.count > 0) {
        notifyDataSetChanged();
      } else {
        notifyDataSetInvalidated();
      }
    }
  };

  public static class ViewHolder {

    final View mBase;

    @Bind(R.id.open) TextView mIcon;
    @Bind(R.id.title) TextView mTitle;
    @Bind(android.R.id.text1) TextView mText;

    ViewHolder(View base) {
      mBase = base;
      ButterKnife.bind(this, mBase);
    }

    TextView getIcon() {
      return mIcon;
    }

    TextView getTitle() {
      return mTitle;
    }

    TextView getText() {
      return mText;
    }
  }
}
