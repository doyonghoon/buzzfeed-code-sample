package com.frograms.watcha.adapters;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import com.frograms.watcha.view.widget.TouchImageView;

/**
 * 이미지 줌도되고 넘겨볼 수 있는 pager.
 */
public class GalleryViewPager extends ViewPager {

  public GalleryViewPager(Context context) {
    super(context);
  }

  public GalleryViewPager(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
    if (v instanceof TouchImageView) {
      //
      // canScrollHorizontally is not supported for Api < 14. To get around this issue,
      // ViewPager is extended and canScrollHorizontallyFroyo, a wrapper around
      // canScrollHorizontally supporting Api >= 8, is called.
      //
      return ((TouchImageView) v).canScrollHorizontallyFroyo(-dx);
    } else {
      return super.canScroll(v, checkV, dx, x, y);
    }
  }
}
