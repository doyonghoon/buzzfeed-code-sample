package com.frograms.watcha.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.model.items.Content;
import com.frograms.watcha.view.DetailContentHeaderView;
import com.frograms.watcha.view.textviews.FontIconTextView;
import com.frograms.watcha.view.textviews.RobotoRegularView;

/**
 * 감상정보 리스트.
 *
 * @see DetailContentHeaderView#updatePlayInfos(Content)
 */
public class VendorAdapter extends ArrayAdapter<Content.Vod> {

  public VendorAdapter(Context context) {
    super(context, R.layout.view_list_item_basic2);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    VendorViewHolder holder;
    if (convertView == null) {
      convertView =
          LayoutInflater.from(getContext()).inflate(R.layout.view_list_item_basic2, parent, false);
      holder = new VendorViewHolder(convertView);
      convertView.setTag(holder);
    } else {
      holder = (VendorViewHolder) convertView.getTag();
    }

    holder.getText1().setText(getItem(position).getVendorName());
    holder.getText2().setText(getItem(position).getQuality() + "|" + getItem(position).getPrice());
    if (getItem(position).getEvent() == null) {
      holder.getIcon().setVisibility(View.GONE);
    } else {
      holder.getIcon().setVisibility(View.VISIBLE);
    }

    return convertView;
  }

  public static class VendorViewHolder {

    @Bind(R.id.text1) RobotoRegularView mText1;
    @Bind(R.id.text2) RobotoRegularView mText2;
    @Bind(R.id.icon) FontIconTextView icon;

    public VendorViewHolder(View base) {
      ButterKnife.bind(this, base);
    }

    public RobotoRegularView getText1() {
      return mText1;
    }

    public RobotoRegularView getText2() {
      return mText2;
    }

    public FontIconTextView getIcon() {
      return icon;
    }
  }
}
