package com.frograms.watcha.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.frograms.watcha.models.Contact;
import com.frograms.watcha.utils.RecycleUtils;
import com.frograms.watcha.view.textviews.FontIconTextView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class FriendAdapter extends ArrayAdapter<Contact> {
  private ArrayList<Contact> item;
  private int[] location;

  private int resId;
  private int mFilterMode;

  private List<WeakReference<View>> mRecycleList = new ArrayList<WeakReference<View>>();
  private View.OnClickListener inviteClickListener;

  public void clearAdapter() {
    item = null;
    location = null;
    inviteClickListener = null;
    RecycleUtils.recursiveRecycle(mRecycleList);
  }

  public FriendAdapter(Context context, int resId, ArrayList<Contact> item) {
    super(context, resId, item);
    this.item = item;
    this.resId = resId;
    mFilterMode = 0;
  }

  public void recycle2() {
    item = null;
    RecycleUtils.recursiveRecycle(mRecycleList);
  }

  public void recycleHalf() {
    int halfSize = mRecycleList.size() / 2;
    List<WeakReference<View>> recycleHalfList = mRecycleList.subList(0, halfSize);
    RecycleUtils.recursiveRecycle(recycleHalfList);
    for (int i = 0; i < halfSize; i++)
      mRecycleList.remove(0);
  }

  public void setOnInviteClickListener(View.OnClickListener listener) {
    inviteClickListener = listener;
  }

  public void filter(int mode) {
    mFilterMode = mode;
  }

  private void setLocation() {
    location = new int[item.size()];
    for (int Loop1 = 0; Loop1 < item.size(); Loop1++) {
      location[Loop1] = Loop1;
    }
    if (mFilterMode == 0) return;
    for (int Loop1 = 0; Loop1 < item.size(); Loop1++) {
      for (int Loop2 = Loop1 + 1; Loop2 < item.size(); Loop2++) {
        //				switch(mFilterMode) {
        //				case 1:
        //					if (item.get(location[Loop1]).getEvalCount() < item.get(location[Loop2]).getEvalCount()) {
        //						int temp = location[Loop1];
        //						location[Loop1] = location[Loop2];
        //						location[Loop2] = temp;
        //					}
        //					break;
        //
        //				case 2:
        //					if (item.get(location[Loop1]).getWishCount() < item.get(location[Loop2]).getWishCount()) {
        //						int temp = location[Loop1];
        //						location[Loop1] = location[Loop2];
        //						location[Loop2] = temp;
        //					}
        //					break;
        //
        //				case 3:
        //					if (item.get(location[Loop1]).getReviewCount() < item.get(location[Loop2]).getReviewCount()) {
        //						int temp = location[Loop1];
        //						location[Loop1] = location[Loop2];
        //						location[Loop2] = temp;
        //					}
        //					break;
        //				}
      }
    }
  }

  @Override public void notifyDataSetChanged() {
    setLocation();
    super.notifyDataSetChanged();
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view = convertView;
    FriendViewWrapper viewWrapper;
    TextView name;
    FontIconTextView invite;

    if (view == null) {
      view = View.inflate(getContext(), resId, null);
      viewWrapper = new FriendViewWrapper(view);
      view.setTag(viewWrapper);

      name = viewWrapper.getName();
      invite = viewWrapper.getInviteBtn();
    } else {
      viewWrapper = (FriendViewWrapper) view.getTag();

      name = viewWrapper.getName();
      invite = viewWrapper.getInviteBtn();
    }

    Contact contact = item.get(location[position]);

    name.setText(contact.getName());

    invite.setTag(location[position]);
    invite.setOnClickListener(inviteClickListener);

    mRecycleList.add(new WeakReference<View>(view));
    return view;
  }
}