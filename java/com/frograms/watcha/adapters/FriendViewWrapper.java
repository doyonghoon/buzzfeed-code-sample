package com.frograms.watcha.adapters;

import android.view.View;
import android.widget.TextView;
import com.frograms.watcha.R;
import com.frograms.watcha.view.textviews.FontIconTextView;

public class FriendViewWrapper {

  private View base;
  private TextView mName = null;
  private FontIconTextView mInvite = null;

  public FriendViewWrapper(View base) {
    this.base = base;
  }

  public TextView getName() {
    if (mName == null) {
      mName = (TextView) base.findViewById(R.id.name);
    }
    return mName;
  }

  public FontIconTextView getInviteBtn() {
    if (mInvite == null) {
      mInvite = (FontIconTextView) base.findViewById(R.id.btn_invite);
    }
    return mInvite;
  }
}