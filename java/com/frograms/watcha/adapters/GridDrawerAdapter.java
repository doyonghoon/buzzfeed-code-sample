package com.frograms.watcha.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.frograms.watcha.R;
import com.frograms.watcha.models.ShareApp;

/**
 * Created by doyonghoon on 15. 3. 10..
 */
public class GridDrawerAdapter extends ArrayAdapter<ShareApp> {

  public GridDrawerAdapter(Context context) {
    super(context, R.layout.view_item_share_app);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
    View view = convertView;
    BaseViewHolder holder;
    if (view == null) {
      view = LayoutInflater.from(getContext()).inflate(R.layout.view_item_share_app, parent, false);
      holder = new BaseViewHolder(view);
      view.setTag(holder);
    } else {
      holder = (BaseViewHolder) view.getTag();
    }

    holder.getTextView().setText(getItem(position).getName());
    holder.getImageView().setImageDrawable(getItem(position).getIconDrawable());

    return view;
  }

  private static class BaseViewHolder {

    private View mBase;

    BaseViewHolder(View base) {
      mBase = base;
    }

    public TextView getTextView() {
      return (TextView) mBase.findViewById(R.id.share_name);
    }

    public ImageView getImageView() {
      return (ImageView) mBase.findViewById(R.id.share_icon);
    }
  }
}
