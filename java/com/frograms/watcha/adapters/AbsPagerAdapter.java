package com.frograms.watcha.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.model.Tab;
import java.util.List;

public class AbsPagerAdapter extends FragmentStatePagerAdapter {

  private List<Tab> mTabs;

  public void clearAdapter() {
    mTabs.clear();
  }

  @Override public void destroyItem(View pager, int position, Object view) {
    ((ViewPager) pager).removeView((View) view);
  }

  @Override public CharSequence getPageTitle(int position) {
    final String title = mTabs.get(position).getTitle();
    if (!TextUtils.isEmpty(title)) {
      return mTabs.get(position).getTitle();
    }
    return WatchaApp.getInstance().getString(mTabs.get(position).getIcon());
  }

  public boolean isText(int position) {
    if (mTabs.get(position).getTitle() != null) {
      return true;
    } else {
      return false;
    }
  }

  public AbsPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  public AbsPagerAdapter(FragmentManager fm, List<Tab> tabs) {
    super(fm);
    mTabs = tabs;
  }

  @Override public Fragment getItem(int position) {
    return mTabs.get(position).getFragment();
  }

  @Override public int getCount() {
    return mTabs.size();
  }
}
