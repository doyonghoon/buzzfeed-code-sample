package com.frograms.watcha.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import com.frograms.watcha.R;
import com.frograms.watcha.application.WatchaApp;
import com.frograms.watcha.listeners.OnRemoveItemListener;
import com.frograms.watcha.model.items.Item;
import com.frograms.watcha.model.views.ViewCard;
import com.frograms.watcha.utils.ViewUtil;
import com.frograms.watcha.view.itemViews.ItemBannerView;
import com.frograms.watcha.view.itemViews.abstracts.ItemView;
import com.frograms.watcha.viewHolders.ItemViewHolder;
import com.frograms.watcha.viewHolders.ViewHolderMeta;
import com.rey.material.widget.ProgressView;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    implements OnRemoveItemListener {

  public interface OnCreateFooterViewListener {
    void onCreateFooterView(FrameLayout footerView, FooterViewHolder footerViewHolder);
  }

  protected Class[] classes = new Class[1];
  protected Context mContext;
  protected List<ViewCard> mData;

  protected View mHeaderView;
  protected FrameLayout mLoadingFooterView;
  protected View mAdditionalFooterView;

  protected int mFooterViewBackgroundColor = Color.TRANSPARENT;
  protected int hasHeader;
  protected boolean hasNext = true;

  private OnClickCardItemListener mListener;
  private OnRemoveCardItemListener mRemoveListener;
  private OnCreateFooterViewListener mOnCreateFooterViewListener;

  public void setOnClickCardItemListener(OnClickCardItemListener listener) {
    mListener = listener;
  }

  public void setOnRemoveCardItemListener(OnRemoveCardItemListener listener) {
    mRemoveListener = listener;
  }

  public void setOnCreateFooterViewListener(OnCreateFooterViewListener listener) {
    mOnCreateFooterViewListener = listener;
  }

  @Override public void onRemove(int position) {
    mData.remove(position - hasHeader);

    if (mRemoveListener != null) {
      mRemoveListener.onRemoveCardItem(position);
    }
    notifyDataSetChanged();
  }

  public interface OnClickCardItemListener {
    void onClickCardItem(ItemView v, Item item);
  }

  public interface OnRemoveCardItemListener {
    void onRemoveCardItem(int position);
  }

  protected static class HeaderViewHolder extends RecyclerView.ViewHolder {
    public HeaderViewHolder(View view) {
      super(view);
    }
  }

  public static class FooterViewHolder extends RecyclerView.ViewHolder {

    private ProgressView mBar;
    //private View mEndIcon;

    public FooterViewHolder(View view) {
      super(view);
      mBar = ButterKnife.findById(view, R.id.progress_loading);
      //mEndIcon = ButterKnife.findById(view, R.id.loading_icon);
    }

    public void onLoading() {
      mBar.setVisibility(View.VISIBLE);
    }

    public void onEnd() {
      mBar.setVisibility(View.GONE);
    }

    //public void invisible() {
    //  mBar.stop();
    //  mBar.setVisibility(View.GONE);
    //}
  }

  public RecyclerAdapter(Context context, View headerView) {
    mContext = context;
    mHeaderView = headerView;

    hasHeader = (mHeaderView == null) ? 0 : 1;

    try {
      classes[0] = Class.forName("android.content.Context");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public void addFooterView(View view) {
    mAdditionalFooterView = view;
  }

  public void setHasNext(boolean hasNext) {
    this.hasNext = hasNext;
  }

  private boolean mReadyToShowLoadingFooterView = false;

  public void setOnReadyToShowLoadingFooterView(boolean isReady) {
    mReadyToShowLoadingFooterView = isReady;
  }

  public List<ViewCard> getData() {
    return mData;
  }

  public void clearItems() {
    mData = null;
    hasNext = true;
    setOnReadyToShowLoadingFooterView(false);
    notifyDataSetChanged();
  }

  public void addItems(List<ViewCard> data) {
    if (mData == null) mData = new ArrayList<>();

    if (data != null) {
      mData.addAll(data);
      notifyDataSetChanged();
    }
  }

  public void addItemsToTop(List<ViewCard> data) {
    if (mData == null) mData = new ArrayList<>();

    if (mData.size() > 0 && data != null && data.size() > 0 && data.get(0).isMerge()) {
      mData.get(0).setIsMerge(true);
      data.get(0).setIsMerge(false);
    }

    if (data != null) {
      mData.addAll(0, data);
      notifyDataSetChanged();
    }
  }

  public void addItem(ViewCard item, int position) {
    mData.add(position, item);
    notifyItemInserted(position + hasHeader);
  }

  //public void removeItem(ViewCard item) {
  //  int position = mData.indexOf(item);
  //  if (position < 0) return;
  //  mData.remove(item);
  //  notifyItemRemoved(position + hasHeader);
  //}

  public void removeItem(int position) {
    if (position < 0) return;
    mData.remove(position - hasHeader);
    notifyItemRemoved(position);
  }

  public FrameLayout getFooterView() {
    return mLoadingFooterView;
  }

  public FooterViewHolder getLoadingViewHolder() {
    return mFooterLoadingViewHolder;
  }

  @Override public int getItemCount() {

    if (mData == null) {
      return hasHeader + 1;/*Footer*/
    } else {
      return mData.size() + hasHeader + 1;/*Footer*/
    }
  }

  @Override public int getItemViewType(int position) {
    if (mHeaderView != null) {
      if (position == 0) {
        return 0;
      }
      position--;
    }

    if (mData == null || position >= mData.size()) {
      return -1;
    }

    ViewHolderMeta meta = mData.get(position).getCardItem().getItemType();
    if (meta == null) {
      return -1;
    }
    return meta.ordinal() + 1;
  }

  public void setDefaultLoadingVisibility(boolean isVisible) {
    mIsVisible = isVisible;
  }

  private boolean mIsVisible = true;
  private FooterViewHolder mFooterLoadingViewHolder = null;

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    if (viewType == 0) {
      return new HeaderViewHolder(mHeaderView);
    } else if (viewType == -1) {
      if (mLoadingFooterView == null) {
        mLoadingFooterView = (FrameLayout) LayoutInflater.from(mContext)
            .inflate(R.layout.view_loading, viewGroup, false);
        mLoadingFooterView.setForegroundGravity(Gravity.CENTER);
        mLoadingFooterView.setBackgroundColor(mFooterViewBackgroundColor);
        mLoadingFooterView.setVisibility(View.GONE);
      }
      mFooterLoadingViewHolder = new FooterViewHolder(mLoadingFooterView);
      mFooterLoadingViewHolder.onEnd();
      if (mOnCreateFooterViewListener != null) {
        mOnCreateFooterViewListener.onCreateFooterView(mLoadingFooterView,
            mFooterLoadingViewHolder);
      }
      return mFooterLoadingViewHolder;
    } else {
      ItemView itemView = null;

      ViewHolderMeta meta = ViewHolderMeta.values()[viewType - 1];

      LinearLayout view = (LinearLayout) LayoutInflater.from(mContext)
          .inflate(R.layout.view_frame, viewGroup, false);

      try {
        Constructor constructor = meta.getItemViewClass().getConstructor(classes);
        try {
          itemView = (ItemView) constructor.newInstance(mContext);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
          e.printStackTrace();
        }
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      }

      if (itemView != null) {
        if (itemView instanceof ItemBannerView) {
          view.findViewById(R.id.top_divider)
              .setBackgroundColor(
                  WatchaApp.getInstance().getResources().getColor(R.color.heavy_purple));
          view.findViewById(R.id.bot)
              .setBackgroundColor(
                  WatchaApp.getInstance().getResources().getColor(R.color.heavy_purple));
          view.findViewById(R.id.seperate)
              .setBackgroundColor(
                  WatchaApp.getInstance().getResources().getColor(R.color.heavy_purple));
        }
        view.addView(itemView, 1);
      }

      return new ItemViewHolder(view);
    }
  }

  private boolean isUseGrid = true;
  private boolean isUseGrid() {
    return isUseGrid;
  }

  public void setIsUseGrid(boolean isUseGrid) {
    this.isUseGrid = isUseGrid;
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
    View view = viewHolder.itemView;
    view.setTag(R.id.TAG_POSITION, position);

    StaggeredGridLayoutManager.LayoutParams lp = null;
    if (view != null && isUseGrid) {
      lp = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
    }

    switch (getItemViewType(position)) {
      case 0:
        if (lp != null) lp.setFullSpan(true);

        break;

      case -1:
        if (lp != null) lp.setFullSpan(true);

        if (hasNext && lp != null) {
          ((FrameLayout) view).removeView(mAdditionalFooterView);
          ((FooterViewHolder) viewHolder).onLoading();
        } else {
          if (mAdditionalFooterView != null) {
            ((FrameLayout) view).addView(mAdditionalFooterView);
            ((FooterViewHolder) viewHolder).onEnd();
          } else {
            ((FooterViewHolder) viewHolder).onEnd();
          }
        }
        break;

      default:
        ItemViewHolder holder = (ItemViewHolder) viewHolder;

        ViewCard viewCard = mData.get(position - hasHeader);

        int count = 0;
        switch (viewCard.getCardType()) {
          case LIST:
            count = 1;
            break;

          case GRID:
            count = 3;
            break;
        }

        for (int Loop1 = 1; Loop1 <= count; Loop1++) {
          if (mData.size() > position - hasHeader + Loop1) {
            if (mData.get(position - hasHeader + Loop1).isMerge()) {
              viewCard.setLocation(ViewCard.LOCATION.NONE);
            }
          }
        }

        if (viewCard.isMerge()) {
          switch(viewCard.getLocation()) {
            case ALL:
              viewCard.setLocation(ViewCard.LOCATION.BOT);
              break;

            default:
              viewCard.setLocation(ViewCard.LOCATION.NONE);
              break;
          }
        }

        //if (lp == null && !viewCard.isReversed()) {
        //  switch(viewCard.getLocation()) {
        //    case TOP:
        //      viewCard.setLocation(ViewCard.LOCATION.BOT);
        //      break;
        //    case BOT:
        //      viewCard.setLocation(ViewCard.LOCATION.TOP);
        //      break;
        //  }
        //
        //  viewCard.setIsReversed(true);
        //}

        switch (viewCard.getLocation()) {
          case ALL:
            holder.isDividerVisible(true, true);
            break;

          case TOP:
            holder.isDividerVisible(true, false);
            break;

          case BOT:
            holder.isDividerVisible(false, true);
            break;

          case NONE:
            holder.isDividerVisible(false, false);
            break;
        }

        switch (viewCard.getCardType()) {
          case LIST:
            if (lp != null) lp.setFullSpan(true);
            //mGridPosition = position;
            break;

          case GRID:
            if (lp != null) {
              lp.setFullSpan(false);
              View v = ((LinearLayout) view).getChildAt(1);
              if (v instanceof ItemView) {
                v.setPadding(getLeftGridMargin(), 0, getRightGridMargin(), 0);
              }
            }
            break;
        }

        Item item = viewCard.getCardItem().getItem();
        if (mListener != null) {
          holder.getItemView().setOnCardItemClickListener(mListener);
        }

        holder.getItemView().setTag(R.id.TAG_POSITION, position);
        holder.getItemView().setOnRemoveItemListener(this);
        if (!holder.getItemView().setItem(item)) {
          holder.remove();
        }
    }

    if (lp != null) {
      if (lp.isFullSpan()) {
        view.setTag(R.id.TAG_FULL_SPAN, true);
      } else {
        view.setTag(R.id.TAG_FULL_SPAN, false);
      }
    }
  }

  private int getLeftGridMargin() {
    return (int) ViewUtil.convertDpToPixel(mContext, 12);
  }

  private int getRightGridMargin() {
    return (int) ViewUtil.convertDpToPixel(mContext, 12);
  }
}